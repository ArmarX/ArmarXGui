/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    ArmarXGui::gui-plugins::ClockWidgetController
 * \author     Clemens Wallrath ( uagzs at student dot kit dot edu )
 * \date       2015
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ClockWidgetController.h"

#include <string>
#include <ArmarXCore/core/time/LocalTimeServer.h>
#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/core/application/Application.h>


namespace armarx
{
    ClockWidgetController::ClockWidgetController()
    {
        widget.setupUi(getWidget());
        updateTimer = new QTimer(this);
    }


    ClockWidgetController::~ClockWidgetController()
    {

    }


    void ClockWidgetController::loadSettings(QSettings* settings)
    {

    }

    void ClockWidgetController::saveSettings(QSettings* settings)
    {

    }


    void ClockWidgetController::onInitComponent()
    {
        if (Application::getInstance()->getProperty<bool>("UseTimeServer").getValue())
        {
            usingProxy(GLOBAL_TIMESERVER_NAME);
        }
    }


    void ClockWidgetController::onConnectComponent()
    {
        connect(updateTimer, SIGNAL(timeout()), this, SLOT(updateGui()));
        connect(widget.btnStart, SIGNAL(clicked()), this, SLOT(startButtonPressed()));
        connect(widget.btnStop, SIGNAL(clicked()), this, SLOT(stopButtonPressed()));
        connect(widget.btnStep, SIGNAL(clicked()), this, SLOT(stepButtonPressed()));
        connect(widget.spinBoxSpeed, SIGNAL(valueChanged(double)), this, SLOT(speedChanged(double)));

        iteration = 0;
        lastChangeSystemTime = IceUtil::Time::now();
        lastChangeTime = TimeUtil::GetTime();

        updateTimer->start(53);
        timeServerPtr = TimeUtil::GetTimeServer();

        if (timeServerPtr)
        {
            widget.btnStart->setEnabled(true);
            widget.btnStop->setEnabled(true);
            widget.btnStep->setEnabled(true);
            widget.spinBoxSpeed->setEnabled(true);
            widget.spinBoxStepCount->setEnabled(true);

            try
            {
                // sometimes the Timeserver is not fully connected at this time and this may fail
                widget.spinBoxSpeed->setValue(timeServerPtr->getSpeed());
            }
            catch (IceUtil::NullHandleException&)
            {
                widget.spinBoxSpeed->setValue(1);
            }
        }
    }

    void ClockWidgetController::onDisconnectComponent()
    {
        timeServerPtr = NULL;
        widget.btnStart->setEnabled(false);
        widget.btnStop->setEnabled(false);
        widget.btnStep->setEnabled(false);
        widget.spinBoxSpeed->setEnabled(false);
        widget.spinBoxStepCount->setEnabled(false);
    }

    void ClockWidgetController::updateGui()
    {
        IceUtil::Time time = TimeUtil::GetTime();
        QString timeStr;
        bool useTimeserver = Application::getInstance()->getProperty<bool>("UseTimeServer").getValue();

        if (useTimeserver)
        {
            // calculate actual speed factor
            if (iteration % 10 == 0) // ~2x per second
            {
                auto timeDiffVirtual = time - lastChangeTime;
                auto now = IceUtil::Time::now();
                auto timeDiffSystem = now - lastChangeSystemTime;
                if (timeDiffSystem.toMilliSeconds() > 0 && timeDiffVirtual.toMilliSeconds() >= 0) // timeDiffVirtual < 0 on timeserver restart
                {
                    float actualFactor = float(timeDiffVirtual.toMilliSeconds()) / timeDiffSystem.toMilliSeconds();
                    widget.lblActualSpeed->setText(QString("(%1)").arg(actualFactor, 0, 'f', 2));
                }
                if (timeDiffVirtual.toMilliSeconds() != 0)
                {
                    lastChangeSystemTime = now;
                    lastChangeTime = time;
                }
            }

            // update gui elements if values like speed where changed remotely
            if (timeServerPtr && iteration % 20 == 0) // ~1x per second
            {
                try
                {
                    widget.lblSteps->setText(QString("x %1ms").arg(timeServerPtr->getStepTimeMS()));
                    if (!widget.spinBoxSpeed->hasFocus())
                    {
                        widget.spinBoxSpeed->setValue(timeServerPtr->getSpeed());
                    }
                }
                catch (Ice::NotRegisteredException&)
                {
                    return;
                }
                catch (IceUtil::NullHandleException&)
                {
                    return;
                }
                catch (Ice::ConnectionRefusedException&)
                {
                    return;
                }

            }
        }

        if (useTimeserver)
        {
            timeStr = QString::fromStdString(time.toDuration());
        }
        else
        {
            timeStr = QString::fromStdString(time.toDateTime());
        }

        widget.lblTime->setText(timeStr);

        iteration++;
    }

    void ClockWidgetController::startButtonPressed()
    {
        if (timeServerPtr)
        {
            timeServerPtr->start();
        }
    }

    void ClockWidgetController::stepButtonPressed()
    {
        if (timeServerPtr)
        {
            for (int i = 0; i < widget.spinBoxStepCount->value() ; ++i)
            {
                timeServerPtr->step();
            }
        }
    }

    void ClockWidgetController::stopButtonPressed()
    {
        if (timeServerPtr)
        {
            timeServerPtr->stop();
        }
    }

    void ClockWidgetController::speedChanged(double newSpeed)
    {
        if (timeServerPtr)
        {
            timeServerPtr->setSpeed(newSpeed);
        }
    }
}
