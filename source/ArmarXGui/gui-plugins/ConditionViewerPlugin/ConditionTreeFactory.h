/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package   ArmarX::Gui::ConditionTreeFactory
* @author    Kai Welke ( welke at kit dot edu)
* @date
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include "TermNode.h"

#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/observers/condition/TermImpl.h>
#include <ArmarXCore/interface/observers/ConditionHandlerInterface.h>


namespace armarx
{
    class ARMARXCOMPONENT_IMPORT_EXPORT ConditionTreeFactory
    {
    public:
        static TermNodePtr createConditionTree(TermTreeGraphicsScene* scene, QTableWidget* checksTable, const TermImplPtr& root, int conditionId, IceManagerPtr manager, ConditionHandlerInterfacePrx handler)
        {
            // handle root node
            std::list<TermNodePtr> nodes;
            TermNodePtr rootNode(new TermNode(scene, checksTable, root, conditionId, manager, handler));
            nodes.push_back(rootNode);

            std::list<TermNodePtr>::iterator iterNodes = nodes.begin();

            while (iterNodes != nodes.end())
            {
                // retreive first element
                TermNodePtr node = nodes.front();
                nodes.pop_front();

                // add childs to list
                TermImplSequence childs = node->getImpl()->getChilds();
                TermImplSequence::iterator iterChilds = childs.begin();

                while (iterChilds != childs.end())
                {
                    TermNodePtr childNode(new TermNode(scene, checksTable, TermImplPtr::dynamicCast(*iterChilds), conditionId, manager, handler));
                    node->addChild(childNode);
                    nodes.push_back(childNode);

                    iterChilds++;
                }

                // next term
                iterNodes = nodes.begin();
            }

            return rootNode;
        }
    };
}

