/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package   ArmarX::Gui::TermNode
* @author    Kai Welke ( welke at kit dot edu)
* @date
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include "TreeNode.h"
#include "widgets/TermTreeGraphicsScene.h"

#include <ArmarXCore/core/IceManager.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/observers/condition/TermImpl.h>
#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <ArmarXCore/interface/observers/ConditionHandlerInterface.h>

#include <QTableWidget>

#include <mutex>

namespace armarx
{
    class ARMARXCOMPONENT_IMPORT_EXPORT TermNode;

    using TermNodePtr = std::shared_ptr<TermNode>;
    using TermNodeWeakPtr = std::weak_ptr<TermNode>;

    class TermNodeGraphicsItem;

    class ARMARXCOMPONENT_IMPORT_EXPORT TermNode : public QObject, public TreeNode,
        public Logging
    {
        Q_OBJECT
    private:
        // constants
        static const QSize LiteralNodeSize;
        static const QSize ExpressionNodeSize;
        static const QSize ConditionRootNodeSize;

    public:
        enum CheckTableColumns
        {
            eDataFieldCol,
            eCheckTypeCol,
            eParametersCol,
            eStateCol,
            eCurrentValueCol
        };

        // constructs a term node
        TermNode(TermTreeGraphicsScene* scene, QTableWidget* checksTable, TermImplPtr term, int conditionId, IceManagerPtr manager, ConditionHandlerInterfacePrx handler);
        ~TermNode() override;
        TermImplPtr getImpl()
        {
            return term;
        }

        QString getDatafieldValue();
        void setTermEvaluationState(TermImplPtr term);
    public slots:
        void graphicsItemClicked();
        void checksTableSelectionChanged();
        void updateValueString(const QString& valueString);
        void setTermEvaluationState(bool truthy);
    signals:
        void literalClicked(int);
        void newLiteralValue(bool truthy);
        void newValueString(QString newValue);
    protected:
        void drawNode(QRectF boundingBox) override;
        void drawEdge(QLineF line) override;

    private:
        void updateChecksTable(TermImplPtr term);

        // reference to term
        TermImplPtr term;
        int conditionId;

        // graphical items
        TermNodeGraphicsItem*    graphicsItem;
        TermTreeGraphicsScene* scene;
        QTableWidget* checksTable;
        int literalRow;
        std::string literaldatafieldName;
        DatafieldRefPtr curDatafield;
        std::recursive_mutex dataMutex;

        IceManagerPtr manager;
        ConditionHandlerInterfacePrx handler;
        // QObject interface
    protected:
        void timerEventRun();

        friend class ConditionViewerWidget;
    };
}

