/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package   ArmarX::Gui::TreeNode
* @author    Kai Welke ( welke at kit dot edu)
* @date
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "TreeNode.h"
#include <cfloat>

namespace armarx
{
    const QSize TreeNode::DefaultNodeHorizontalSeparator(10, 0);
    const QSize TreeNode::DefaultNodeVerticalSeparator(0, 50);
    const QSize TreeNode::DefaultNodeSize(30, 30);

    //----------------------------------------------------------------------
    // construction
    //----------------------------------------------------------------------
    TreeNode::TreeNode(QGraphicsScene* scene, QSize nodeSize)
    {
        this->size = nodeSize;

        nodeItem = new QGraphicsEllipseItem();
        scene->addItem(nodeItem);
        nodeItem->hide();
        edgeItem = new QGraphicsLineItem();
        scene->addItem(edgeItem);
        edgeItem->hide();
    }

    //----------------------------------------------------------------------
    // tree structure
    //----------------------------------------------------------------------
    void TreeNode::setParent(TreeNodeWeakPtr parent)
    {
        this->parent = parent;
        // maybe set parent of QGraphicsItems in order to allow transformations
    }

    void TreeNode::addChild(TreeNodePtr child)
    {
        child->setParent(TreeNodeWeakPtr(shared_from_this()));
        childs.push_back(child);
    }

    //----------------------------------------------------------------------
    // layout handling
    //----------------------------------------------------------------------
    void TreeNode::update(QPointF positionLeftTop)
    {
        // assure root node
        if (parent.lock())
        {
            return;
        }

        // recalculate size recursively
        calculateSubTreeSize();

        // update layout recursively
        updateLayout(positionLeftTop);
    }

    std::vector<TreeNodePtr> TreeNode::getChildren() const
    {
        return childs;
    }

    //----------------------------------------------------------------------
    // helper methods
    //----------------------------------------------------------------------
    void TreeNode::updateLayout(QPointF leftTop)
    {
        //  update of childs if necessary
        if (childs.size() > 0)
        {
            // init left top position of childs
            QPointF childLeftTop = leftTop;
            childLeftTop.ry() += TreeNode::DefaultNodeVerticalSeparator.height() + getSize().height();

            std::vector<TreeNodePtr>::iterator iter = childs.begin();

            while (iter != childs.end())
            {
                // set layout of children
                (*iter)->updateLayout(childLeftTop);

                // move to next left position
                QSize childSubTreeSize = (*iter)->getSubTreeSize();
                QPointF inc(childSubTreeSize.width() + TreeNode::DefaultNodeHorizontalSeparator.width(), 0);
                childLeftTop += inc;

                iter++;
            }
        }

        // calculate position of node and draw
        if (childs.size() == 0) // put node in center
        {
            // set geometry of node
            boundingBox = QRectF(leftTop.x() + getSubTreeSize().width() / 2.0f - getSize().width() / 2.0f, leftTop.y(), getSize().width(), getSize().height());
            drawNode(boundingBox);
        }
        else
        {
            // center nodes with respect to next layer
            QRectF childsBB = calculateChildsBoundingBox();

            // set geometry of node
            boundingBox = QRectF(childsBB.center().x() - getSize().width() / 2.0f, leftTop.y(), getSize().width(), getSize().height());
            drawNode(boundingBox);
        }

        // draw edge
        drawEdges();
    }

    QSize TreeNode::calculateSubTreeSize()
    {
        std::vector<TreeNodePtr>::iterator iter = childs.begin();

        // size of childs
        int width = 0;
        int height = 0;

        while (iter != childs.end())
        {
            // width of children is the sum
            QSize childSize = (*iter)->calculateSubTreeSize();
            width += childSize.width() + TreeNode::DefaultNodeHorizontalSeparator.width();

            // height is the maximum
            if (childSize.height() > height)
            {
                height = childSize.height();
            }

            iter++;
        }

        // adapt wize
        if (childs.size() > 0)
        {
            width -= TreeNode::DefaultNodeHorizontalSeparator.width();
            height += TreeNode::DefaultNodeVerticalSeparator.height();
        }
        else
        {
            width = getSize().width();
        }

        // adapt size in height
        height += getSize().height();

        this->subTreeSize = QSize(width, height);

        return subTreeSize;
    }

    QRectF TreeNode::calculateChildsBoundingBox()
    {
        // calculate boundingbox of childs
        float minX = FLT_MAX;
        float maxX = 0;

        float minY = FLT_MAX;
        float maxY = 0;

        std::vector<TreeNodePtr>::iterator iter = childs.begin();

        while (iter != childs.end())
        {
            QRectF childBB = (*iter)->getBoundingBox();

            if (childBB.left() < minX)
            {
                minX = childBB.left();
            }

            if (childBB.right() > maxX)
            {
                maxX = childBB.right();
            }

            if (childBB.top() < minY)
            {
                minY = childBB.top();
            }

            if (childBB.bottom() > maxY)
            {
                maxY = childBB.bottom();
            }

            iter++;
        }

        QRectF result(minX, minY, maxX - minX, maxY - minY);
        return result;
    }

    void TreeNode::drawEdges()
    {
        std::vector<TreeNodePtr>::iterator iter = childs.begin();

        while (iter != childs.end())
        {
            QPointF lineStart = (getBoundingBox().bottomLeft() + getBoundingBox().bottomRight()) / 2.0f;
            QPointF lineEnd = ((*iter)->getBoundingBox().topLeft() + (*iter)->getBoundingBox().topRight()) / 2.0f;
            QLineF line(lineStart, lineEnd);

            (*iter)->drawEdge(line);

            iter++;
        }
    }

    void TreeNode::drawEdge(QLineF line)
    {
        edgeItem->setLine(line);
        edgeItem->show();
    }

    void TreeNode::drawNode(QRectF boundingBox)
    {
        nodeItem->setRect(boundingBox);
        nodeItem->show();
    }
}
