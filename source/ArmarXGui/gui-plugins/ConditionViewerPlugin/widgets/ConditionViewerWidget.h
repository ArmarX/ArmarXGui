/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

// Qt
#include <ArmarXGui/gui-plugins/ConditionViewerPlugin/ui_ConditionViewerWidget.h>

#include "ConditionItemModel.h"
#include "TermTreeGraphicsScene.h"
#include "../TreeNode.h"
#include "../TermNode.h"

#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>

#include <QMainWindow>
#include <QWidget>

#include <string>
#include <mutex>
#include <map>


namespace armarx
{
    class ConditionViewerWidgetController;

    class ConditionViewerWidget : public QWidget
    {
        Q_OBJECT

    public:
        ConditionViewerWidget(ConditionViewerWidgetController* controller);
        ~ConditionViewerWidget() override;

    public slots:
        void onDisconnect();
        void onConnect();
        void activeConditionItemSelected(const QItemSelection&, const QItemSelection&);
        void pastConditionItemSelected(const QItemSelection&, const QItemSelection&);

    signals:
        void clearScene();

    private:
        ConditionRootPtr conditionFromItem(QStandardItem* selectedItem, const ConditionRegistry& registry);
        void updateCondition(int conditionId, ConditionRootPtr& condition);
        void updateLiterals();
        Ui::ConditionViewerWidget ui;
        ConditionViewerWidgetController* controller;
        TermNodePtr root;
        TermTreeGraphicsScene* scene;
        ConditionItemModel* activeConditionsModel;
        ConditionItemModel* pastConditionsModel;
        ConditionRegistry activeConditions;
        ConditionRegistry pastConditions;
        std::recursive_mutex dataMutex;

        PeriodicTask<ConditionViewerWidget>::pointer_type updateTask;
        int timerId;

        // QObject interface
    protected:
        void timerEvent(QTimerEvent* event) override;
    };
}

