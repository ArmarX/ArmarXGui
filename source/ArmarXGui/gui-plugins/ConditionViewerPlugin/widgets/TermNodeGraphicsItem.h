/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Gui
* @author     Kai Welke (welke@kit.edu)
* @copyright  2012 Humanoids Group, IAIM, IFA
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include "../TermNode.h"

#include <QGraphicsItem>
#include <QString>
#include <QPainter>

namespace armarx
{
    class TermNodeGraphicsItem : public QObject, public QGraphicsItem
    {
        Q_OBJECT

    public:
        // constructs a termnode graphics item
        TermNodeGraphicsItem(TermTreeGraphicsScene* scene, TermNode* node);

        // set the position of the node
        void setRect(QRectF boundingBox);
        void setText(const QString& text);
        void setPixmap(const QPixmap& pixmap);
        void setColor(const QColor& penColor);

        // pute virtual in QGraphicsItem
        QRectF boundingRect() const override;
        void paint(QPainter* painter, const QStyleOptionGraphicsItem* option,
                   QWidget* widget) override;

    signals:
        void nodeActivated();
        void nodeDeactivated();

    public slots:
        void updateNodeActivation(const QPointF&);
        void activateNode();
        void deactivateNode();
        void activateNodeSilent();

    private:
        TermNode* node;
        QString text;
        QRectF boundingBox;
        QPixmap pixmap;
        QColor penColor;
        bool pixmapSet;
        bool activated;
    };
}

