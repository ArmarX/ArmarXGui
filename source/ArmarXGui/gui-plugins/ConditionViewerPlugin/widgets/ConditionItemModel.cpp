/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Gui
* @author     Kai Welke (welke@kit.edu)
* @copyright  2012 Humanoids Group, IAIM, IFA
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "ConditionItemModel.h"

namespace armarx
{
    ConditionItemModel::ConditionItemModel()
        : QStandardItemModel()
    {
        reset();
    }

    void ConditionItemModel::update(const ConditionRegistry& registry)
    {
        // mark all items for delete
        markAllForDelete();

        // update iterm
        ConditionRegistry::const_iterator iter = registry.begin();

        while (iter != registry.end())
        {
            updateOrInsertCondition(iter->first, ConditionRootPtr::dynamicCast(iter->second));
            iter++;
        }

        // delete all unused items
        deleteUnusedItems();
    }

    void ConditionItemModel::reset()
    {
        clear();
        setRowCount(0);
        setColumnCount(6);

        // setup header
        setHorizontalHeaderItem(0, new QStandardItem("id"));
        setHorizontalHeaderItem(1, new QStandardItem("component"));
        setHorizontalHeaderItem(2, new QStandardItem("state"));
        setHorizontalHeaderItem(3, new QStandardItem("event"));
        setHorizontalHeaderItem(4, new QStandardItem("fired"));
        setHorizontalHeaderItem(5, new QStandardItem("description"));
    }

    void ConditionItemModel::updateOrInsertCondition(int id, const ConditionRootPtr& condition)
    {
        // loop through all rows
        bool update = false;
        int updateRow = 0;

        for (int r = 0 ; r < rowCount() ; r++)
        {
            QStandardItem* current = item(r, 0);

            if (current->data(CONDITION_ITEM_ID) == id)
            {
                update = true;
                updateRow = r;
            }
        }

        // check if new row is required
        if (!update)
        {
            insertRow(rowCount());
            updateRow = rowCount() - 1;

            // initial row content
            QString id_string;
            id_string.sprintf("%06d", id);

            QStandardItem* index = new QStandardItem(id_string);
            index->setData(QVariant(id), CONDITION_ITEM_ID);

            setItem(updateRow, 0, index);
            setItem(updateRow, 1, new QStandardItem("component"));
            setItem(updateRow, 2, new QStandardItem("state"));
            setItem(updateRow, 3, new QStandardItem("event"));
            setItem(updateRow, 4, new QStandardItem("false"));
            setItem(updateRow, 5, new QStandardItem("dsc"));
        }

        // update fields
        std::string component, event;
        extractFields(condition->event->eventName, component, event);

        item(updateRow, 0)->setData(QVariant(false), CONDITION_ITEM_DELETE);
        item(updateRow, 1)->setData(QVariant(component.c_str()), Qt::DisplayRole);
        item(updateRow, 2)->setData(QVariant(condition->event->eventReceiverName.c_str()), Qt::DisplayRole);
        item(updateRow, 3)->setData(QVariant(event.c_str()), Qt::DisplayRole);

        if (condition->fired)
        {
            item(updateRow, 4)->setData(QVariant("true"), Qt::DisplayRole);
            item(updateRow, 4)->setBackground(Qt::green);
        }
        else
        {
            item(updateRow, 4)->setData(QVariant("false"), Qt::DisplayRole);
            item(updateRow, 4)->setBackground(Qt::red);
        }

        item(updateRow, 5)->setData(QVariant(condition->description.c_str()), Qt::DisplayRole);
    }

    void ConditionItemModel::markAllForDelete()
    {
        // mark all elements for deletion
        for (int r = 0 ; r < rowCount() ; r++)
        {
            item(r, 0)->setData(QVariant(true), CONDITION_ITEM_DELETE);
        }
    }

    void ConditionItemModel::deleteUnusedItems()
    {
        // find unused rows
        std::vector<int> removeRows;

        for (int r = 0 ; r < rowCount() ; r++)
        {
            if (item(r, 0)->data(CONDITION_ITEM_DELETE).toBool())
            {
                removeRows.push_back(r);
            }
        }

        // remove unused rows
        std::vector<int>::iterator iter = removeRows.begin();

        while (iter != removeRows.end())
        {
            removeRow(*iter);
            iter++;
        }
    }

    void ConditionItemModel::extractFields(std::string in, std::string& componentName, std::string& eventName)
    {
        std::size_t pos = in.find("_EventDistributor");

        if (pos != std::string::npos)
        {
            componentName = in.substr(0, pos - 1);
        }

        std::size_t posEvent = in.find("__Event:");
        std::size_t posRecv = in.find("__EventRecv:");

        if ((posEvent == std::string::npos) || (posRecv == std::string::npos))
        {
            return;
        }

        posEvent += 8;
        posRecv += 12;

        eventName = in.substr(posEvent, posRecv - posEvent - 13);
    }
}
