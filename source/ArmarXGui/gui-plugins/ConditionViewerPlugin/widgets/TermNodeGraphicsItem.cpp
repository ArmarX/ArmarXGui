/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Gui
* @author     Kai Welke (welke@kit.edu)
* @copyright  2012 Humanoids Group, IAIM, IFA
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "TermNodeGraphicsItem.h"

namespace armarx
{
    TermNodeGraphicsItem::TermNodeGraphicsItem(TermTreeGraphicsScene* scene, TermNode* node)
    {
        this->node = node;
        activated = false;
        pixmapSet = false;

        // connections to / from TermNode
        connect(scene, SIGNAL(graphicsSceneClicked(const QPointF&)), this, SLOT(updateNodeActivation(const QPointF&)));
    }

    // setters
    void TermNodeGraphicsItem::setRect(QRectF boundingBox)
    {
        prepareGeometryChange();
        this->boundingBox = boundingBox;

        update();
    }

    void TermNodeGraphicsItem::setText(const QString& text)
    {
        this->text = text;

        update();
    }

    void TermNodeGraphicsItem::setColor(const QColor& penColor)
    {
        this->penColor = penColor;

        update();
    }

    void TermNodeGraphicsItem::setPixmap(const QPixmap& pixmap)
    {
        this->pixmap = pixmap;
        pixmapSet = true;

        update();
    }

    // pute virtual in QGraphicsItem
    QRectF TermNodeGraphicsItem::boundingRect() const
    {
        return boundingBox;
    }

    void TermNodeGraphicsItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option,
                                     QWidget* widget)
    {
        // draw ellipse
        {
            QBrush penBrush(penColor);
            QPen pen(penBrush, 3, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin);

            QBrush brush(Qt::lightGray);
            painter->setPen(pen);

            if (activated)
            {
                painter->setBrush(brush);
            }
            else
            {
                painter->setBrush(Qt::NoBrush);
            }

            painter->drawEllipse(boundingBox);
        }

        // draw text
        if (text.length() > 0)
        {
            QPen pen(Qt::black, 1, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin);
            painter->setPen(pen);
            painter->drawText(boundingBox, Qt::AlignCenter, text);
        }

        // draw pixmap
        pixmapSet = true;

        if (pixmapSet)
        {
            QRect bbPixmap(boundingBox.center().x() - boundingBox.width() / 4, boundingBox.center().y() - boundingBox.height() / 4, boundingBox.width() / 2, boundingBox.height() / 2);
            painter->drawPixmap(bbPixmap, pixmap);
        }
    }

    // slots
    void TermNodeGraphicsItem::activateNode()
    {
        activated = true;
        emit nodeActivated();
        update();
    }

    void TermNodeGraphicsItem::activateNodeSilent()
    {
        activated = true;
        update();
    }
    void TermNodeGraphicsItem::deactivateNode()
    {
        activated = false;
        emit nodeDeactivated();
        update();
    }

    void TermNodeGraphicsItem::updateNodeActivation(const QPointF& point)
    {
        // from scene pos to local pos
        QPointF inLocal = point - scenePos();

        if (contains(inLocal))
        {
            activateNode();
        }
        else
        {
            deactivateNode();
        }
    }
}
