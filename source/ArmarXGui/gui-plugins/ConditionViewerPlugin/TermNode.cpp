/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package   ArmarX::Gui::TermNode
* @author    Kai Welke ( welke at kit dot edu)
* @date
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "TermNode.h"
#include "widgets/TermNodeGraphicsItem.h"

#include <ArmarXCore/observers/condition/LiteralImpl.h>
#include <ArmarXCore/observers/condition/ConditionRoot.h>
#include <ArmarXCore/observers/condition/Operations.h>


namespace armarx
{
    const QSize TermNode::LiteralNodeSize(50, 50);
    const QSize TermNode::ExpressionNodeSize(30, 30);
    const QSize TermNode::ConditionRootNodeSize(50, 50);

    TermNode::TermNode(TermTreeGraphicsScene* scene, QTableWidget* checksTable, TermImplPtr term, int conditionId, IceManagerPtr manager, ConditionHandlerInterfacePrx handler)
        : TreeNode(scene),
          conditionId(conditionId),
          manager(manager),
          handler(handler)
    {
        this->term = term;
        this->checksTable = checksTable;
        this->scene = scene;

        graphicsItem = new TermNodeGraphicsItem(scene, this);

        // set size
        switch (term->getType())
        {
            case eConditionRoot:
            {
                setSize(ConditionRootNodeSize);
                graphicsItem->setPixmap(QPixmap(":/icons/arrow-right.png"));

                break;
            }

            case eOperation:
            {
                setSize(ExpressionNodeSize);
                graphicsItem->setText(OperationPtr::dynamicCast(term)->getOperationString().c_str());
                break;
            }

            case eLiteral:
            {
                setSize(LiteralNodeSize);
                graphicsItem->setPixmap(QPixmap(":/icons/wave.png"));

                break;
            }

            default:
                break;
        }

        // set tooltip
        std::stringstream ss;
        ss << term;
        graphicsItem->setToolTip(ss.str().c_str());

        // set color
        if (term->getValue())
        {
            graphicsItem->setColor(Qt::green);
        }
        else
        {
            graphicsItem->setColor(Qt::red);
        }


        // add to scene
        scene->addItem(graphicsItem);
        graphicsItem->hide();

        // update checks table for each literal
        if (checksTable && (term->getType() == eLiteral))
        {
            updateChecksTable(term);
        }

        connect(this, SIGNAL(newValueString(QString)), this, SLOT(updateValueString(QString)));
        connect(this, SIGNAL(newLiteralValue(bool)), this, SLOT(setTermEvaluationState(bool)));


    }

    TermNode::~TermNode()
    {
    }

    void TermNode::drawNode(QRectF boundingBox)
    {
        graphicsItem->setRect(boundingBox);
        graphicsItem->show();
    }

    void TermNode::drawEdge(QLineF line)
    {
        TreeNode::drawEdge(line);
    }

    QString TermNode::getDatafieldValue()
    {
        std::unique_lock lock(dataMutex);

        try
        {

            VariantPtr var = VariantPtr::dynamicCast(curDatafield->getDataField());
            auto valueString = var->getOutputValueOnly();
            return QString::fromStdString(valueString);
        }
        catch (std::exception& e)
        {
        }

        return "Retrieval Failed";

    }

    void TermNode::setTermEvaluationState(TermImplPtr term)
    {
        setTermEvaluationState(term->getValue());
    }

    void TermNode::setTermEvaluationState(bool truthy)
    {
        std::unique_lock lock(dataMutex);

        if (truthy)
        {
            QTableWidgetItem* item = new QTableWidgetItem("true");
            item->setBackground(Qt::green);
            checksTable->setItem(literalRow, eStateCol, item);
            graphicsItem->setColor(Qt::green);
        }
        else
        {
            QTableWidgetItem* item = new QTableWidgetItem("false");
            item->setBackground(Qt::red);
            checksTable->setItem(literalRow, eStateCol, item);
            graphicsItem->setColor(Qt::red);
        }

    }


    void TermNode::updateChecksTable(TermImplPtr term)
    {
        std::unique_lock lock(dataMutex);

        LiteralImplPtr literal = LiteralImplPtr::dynamicCast(term);
        CheckConfiguration config = literal->getCheckConfiguration();

        // insert new row
        checksTable->insertRow(checksTable->rowCount());
        literalRow = checksTable->rowCount() - 1;

        // set datafield value
        DataFieldIdentifierPtr dataFieldIdentifier = DataFieldIdentifierPtr::dynamicCast(config.dataFieldIdentifier);

        if (dataFieldIdentifier)
        {
            literaldatafieldName = dataFieldIdentifier->getIdentifierStr().c_str();
            checksTable->setItem(literalRow, eDataFieldCol, new QTableWidgetItem(dataFieldIdentifier->getIdentifierStr().c_str()));
            ObserverInterfacePrx prx = manager->getProxy<ObserverInterfacePrx>(dataFieldIdentifier->getObserverName());

            try
            {
                if (prx->existsChannel(dataFieldIdentifier->channelName))
                {
                    curDatafield = new DatafieldRef(prx, dataFieldIdentifier->channelName, dataFieldIdentifier->datafieldName);
                }
            }
            catch (std::exception& e)
            {
                ARMARX_INFO << e.what();
            }
        }
        else
        {
            curDatafield = NULL;
            checksTable->setItem(literalRow, eDataFieldCol, new QTableWidgetItem("NULL"));
            literaldatafieldName = "";
        }

        ARMARX_INFO << "updating " << dataFieldIdentifier->getIdentifierStr();

        // set check type value
        checksTable->setItem(literalRow, eCheckTypeCol, new QTableWidgetItem(config.checkName.c_str()));

        // set check parameters
        ParameterList::iterator iter = config.checkParameters.begin();
        std::stringstream parameters;

        while (iter != config.checkParameters.end())
        {
            VariantPtr var = VariantPtr::dynamicCast(*iter);
            parameters << var << ", ";
            iter++;
        }

        checksTable->setItem(literalRow, eParametersCol, new QTableWidgetItem(parameters.str().c_str()));

        // set check state
        setTermEvaluationState(term);

        // connections
        // from graphicsitem to table
        connect(graphicsItem, SIGNAL(nodeActivated()), this, SLOT(graphicsItemClicked()));
        connect(this, SIGNAL(literalClicked(int)), checksTable, SLOT(selectRow(int)));

        // from table to graphicsitem
        connect(checksTable, SIGNAL(itemSelectionChanged()), this, SLOT(checksTableSelectionChanged()));

        // proper size
        checksTable->resizeColumnsToContents();
    }

    void TermNode::timerEventRun()
    {
        bool truthy = false;
        {
            std::unique_lock lock(dataMutex);
            truthy = curDatafield && literaldatafieldName == curDatafield->getDataFieldIdentifier()->getIdentifierStr();
        }

        if (truthy)
        {
            auto valueString = getDatafieldValue();
            emit newValueString(valueString);
        }
    }

    void TermNode::graphicsItemClicked()
    {
        if (term->getType() == eLiteral)
        {
            emit literalClicked(literalRow);
        }
    }

    void TermNode::checksTableSelectionChanged()
    {
        QList<QTableWidgetItem*> selectedItems = checksTable->selectedItems();

        int size = selectedItems.size();

        if (size > 0)
        {
            QTableWidgetItem* widget = selectedItems.front();
            int row = checksTable->row(widget);

            if (row == literalRow)
            {
                scene->resetActivation();
                graphicsItem->activateNodeSilent();
            }
        }
    }

    void TermNode::updateValueString(const QString& valueString)
    {
        std::unique_lock lock(dataMutex);
        checksTable->setItem(literalRow, eCurrentValueCol, new QTableWidgetItem(valueString));
    }

}
