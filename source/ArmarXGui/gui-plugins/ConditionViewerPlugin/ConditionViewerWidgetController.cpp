/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package   ArmarX::Gui::ConditionViewerWidgetController
* @author    Kai Welke ( welke at kit dot edu)
* @date
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "ConditionViewerWidgetController.h"
#include <ArmarXCore/observers/ObserverObjectFactories.h>

#include <QAction>
#include <QToolBar>

namespace armarx
{
    ConditionViewerWidgetController::ConditionViewerWidgetController() :
        customToolbar(NULL)
    {

    }

    void ConditionViewerWidgetController::onInitComponent()
    {
        getWidget()->show();
        usingProxy("ConditionHandler");

    }

    void ConditionViewerWidgetController::onConnectComponent()
    {

        // get proxy of conditionhandler
        handler = getProxy<ConditionHandlerInterfacePrx>("ConditionHandler");
        if (handler)
        {
            std::unique_lock lock(dataMutex);
            QMetaObject::invokeMethod(__widget, "onConnect");
        }
        refreshAction->setChecked(true);

        qRegisterMetaType<Qt::Orientation>("Qt::Orientation");

        ARMARX_INFO << "Starting ConditionViewerGuiPlugin" << flush;
    }

    void ConditionViewerWidgetController::onDisconnectComponent()
    {
        ARMARX_INFO_S << "Stopping ConditionViewer";

        refreshAction->setChecked(false);
        __widget->onDisconnect();
    }

    void ConditionViewerWidgetController::onExitComponent()
    {
        if (updateTask)
        {
            updateTask->stop();
        }
    }

    QPointer<QWidget> ConditionViewerWidgetController::getWidget()
    {
        if (!__widget)
        {
            __widget = new ConditionViewerWidget(this);
        }

        return qobject_cast<ConditionViewerWidget*>(__widget);
    }

    ConditionRegistry ConditionViewerWidgetController::getActiveConditions()
    {
        std::unique_lock lock(dataMutex);
        return currentConditions;
    }

    ConditionRegistry ConditionViewerWidgetController::getPastConditions()
    {
        std::unique_lock lock(dataMutex);
        return pastConditions;
    }

    void ConditionViewerWidgetController::refresh(bool enableRefreshing)
    {
        if (enableRefreshing)
        {
            updateTask = new armarx::PeriodicTask<ConditionViewerWidgetController>(this, &ConditionViewerWidgetController::updateConditions, 2000, false, "ConditionsUpdateTask");
            updateTask->start();
        }
        else if (updateTask)
        {
            updateTask->stop();
        }
    }

    void ConditionViewerWidgetController::removeConditions()
    {
        try
        {
            if (handler)
            {
                auto removeResult = handler->begin_removeAllConditions();
                std::unique_lock lock(dataMutex);
                handler->end_removeAllConditions(removeResult);
            }
        }
        catch (Ice::NotRegisteredException&)
        {

        }
    }

    void ConditionViewerWidgetController::updateConditions()
    {
        try
        {
            if (handler)
            {
                auto pastResult = handler->begin_getPastConditions();
                auto activeResult = handler->begin_getActiveConditions();
                std::unique_lock lock(dataMutex);
                pastConditions = handler->end_getPastConditions(pastResult);
                currentConditions = handler->end_getActiveConditions(activeResult);
            }
        }
        catch (Ice::NotRegisteredException&)
        {

        }

    }
}


QPointer<QWidget> armarx::ConditionViewerWidgetController::getCustomTitlebarWidget(QWidget* parent)
{
    if (customToolbar)
    {
        if (parent != customToolbar->parent())
        {
            customToolbar->setParent(parent);
        }

        return qobject_cast<QToolBar*>(customToolbar);
    }

    customToolbar = new QToolBar(parent);
    customToolbar->setIconSize(QSize(16, 16));
    refreshAction = customToolbar->addAction(QIcon(":/icons/view-refresh-7.png"), "Refresh conditions");
    refreshAction->setCheckable(true);
    connect(refreshAction, SIGNAL(toggled(bool)), this, SLOT(refresh(bool)));

    QAction* removeAction = customToolbar->addAction(QIcon(":/icons/dialog-close.ico"), "Remove conditions");
    connect(removeAction, SIGNAL(triggered()), this, SLOT(removeConditions()));

    return qobject_cast<QToolBar*>(customToolbar);
}
