/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package   ArmarX::Gui::ConditionViewerWidgetController
* @author    Kai Welke ( welke at kit dot edu)
* @date
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/interface/observers/ConditionHandlerInterface.h>
#include <ArmarXCore/interface/observers/Event.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include "widgets/ConditionViewerWidget.h"

#include <mutex>

class QToolBar;

namespace armarx
{
    /** \page ArmarXGui-GuiPlugins-ConditionViewer ConditionViewer
    * \brief With this widget conditions can be visualized.
    * \image html ConditionViewer.png
    * The user can choose between displaying a history of all conditions or
    * the currently active conditions.
    * Gui Documentation \ref ConditionViewerWidgetController

    * \see ConditionViewerGuiPlugin
    */

    /** \class ConditionViewerWidgetController
    * \brief With this widget conditions can be visualized.
    * \see ConditionViewerGuiPlugin
    */
    class ARMARXCOMPONENT_IMPORT_EXPORT ConditionViewerWidgetController :
        public ArmarXComponentWidgetControllerTemplate<ConditionViewerWidgetController>
    {
        Q_OBJECT
    public:
        ConditionViewerWidgetController();

        // inherited from Component
        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;

        // inherited from ArmarXWidget
        QPointer<QWidget> getWidget() override;
        static QString GetWidgetName()
        {
            return "Observers.ConditionViewer";
        }
        void loadSettings(QSettings* settings) override {}
        void saveSettings(QSettings* settings) override {}
        QPointer<QWidget> getCustomTitlebarWidget(QWidget* parent) override;



        // helper methods for gui
        ConditionRegistry getActiveConditions();
        ConditionRegistry getPastConditions();
        ConditionHandlerInterfacePrx handler;
    public slots:
        void refresh(bool enableRefreshing);
        void removeConditions();


    private:
        void updateConditions();
        ConditionRegistry currentConditions;
        ConditionRegistry pastConditions;
        armarx::PeriodicTask<ConditionViewerWidgetController>::pointer_type updateTask;
        std::mutex dataMutex;
        QPointer<ConditionViewerWidget> __widget;
        QPointer<QToolBar> customToolbar;
        QAction* refreshAction;

    };
}

