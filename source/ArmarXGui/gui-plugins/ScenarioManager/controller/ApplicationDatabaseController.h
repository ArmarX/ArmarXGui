/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include "../gui/applicationdatabasemodel.h"
#include "../gui/filterabletreemodelsortfilterproxymodel.h"
#include "../gui/treeitem.h"
#include <ArmarXCore/util/ScenarioManagerCommon/data_structure/Package.h>
#include <ArmarXCore/util/ScenarioManagerCommon/executor/Executor.h>

#include <QObject>

#include <memory>
#include <vector>

namespace ScenarioManager::Controller
{
    /**
    * @class ApplicationDatabaseController
    * @ingroup controller
    * @brief This Controller manages the signals and model of the ApplicationDatabaseView.
    * Item click signals in the ApplicationDatabaseView need to be connected to the on_itemClicked() slot.
    * When an item gets clicked, this controller finds out whether an Application or a Package was clicked and
    * specifically which Application or Package it was. It then sends a signal with a pointer to the clicked object.
    */
    class ApplicationDatabaseController : public QObject
    {
        Q_OBJECT
    public:
        /**
        * Constructor that takes the data structure this controller operates on and optionally the parent object.
        * @param packages list of packages. Need to contain the Packages and Applications displayed in the ApplicationDatabaseView.
        * @param executor executor used to start, stop and update status of the Applications and Scenarios.
        * @param parent standard QT option to specify a parent
        */
        ApplicationDatabaseController(Data_Structure::PackageVectorPtr packages, Exec::ExecutorPtr executor,  QObject* parent = 0);
        ~ApplicationDatabaseController() override;

        /**
        * Returns the model used by the ApplicationDatabaseView and managed by this controller.
        * @return managed model
        */
        FilterableTreeModelSortFilterProxyModelPtr getModel();

    signals:
        /**
        * Gets emitted after an Application has been clicked in the ApplicationDatabaseView.
        * @param app clicked Application
        */
        void applicationClicked(Data_Structure::ApplicationPtr app);

        /**
        * Gets emitted after a Package gets clicked in the ApplicationDatabaseView.
        * @param package clicked Package
        */
        void packageClicked(Data_Structure::PackagePtr package);


        void modelUpdated(FilterableTreeModelSortFilterProxyModelPtr model);

    public slots:

        /**
        * Finds out whether an Application or a Scenario has been clicked and sends a signal with
        * the corresponding object.
        * @param index model-index of the clicked object
        */
        void on_itemClicked(const QModelIndex& index);

        /**
        * Updates the packages displayed in the ApplicationDatabaseView by reloading all packages
        * and applications into the model.
        */
        void updatePackages();

    private:
        ApplicationDatabaseModel treemodel;
        FilterableTreeModelSortFilterProxyModelPtr model;
        Data_Structure::PackageVectorPtr packages;
        Exec::ExecutorPtr executor;
    };
}


