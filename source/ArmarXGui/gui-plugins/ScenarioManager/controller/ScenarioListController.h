/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include "../gui/scenariomodel.h"
#include "../gui/scenariolistview.h"
#include "../gui/createscenarioview.h"
#include "../gui/filterabletreemodelsortfilterproxymodel.h"
#include <ArmarXCore/core/IceManager.h>
#include <ArmarXCore/core/IceGridAdmin.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>
#include <ArmarXCore/util/ScenarioManagerCommon/data_structure/Package.h>
#include <ArmarXCore/util/ScenarioManagerCommon/executor/Executor.h>
#include <ArmarXCore/util/ScenarioManagerCommon/data_structure/ApplicationInstance.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <QObject>

#include <memory>
#include <vector>
#include <mutex>

namespace ScenarioManager::Controller
{
    using ApplicationInstanceStatusMap = std::map<ApplicationInstancePtr, std::string>;
    /**
    * @class ScenarioListController
    * @ingroup controller
    * @brief Manages the signals and model of the ScenarioListView.
    * All signals emitted by the ScenarioListView need to be connected to their corresponding slots in this
    * controller. This controller periodically refreshes the status of the Applications and Scenarios
    * displayed in the ScenarioListView.
    */
    class ScenarioListController : public QObject,
        public armarx::Logging
    {
        Q_OBJECT

    public:
        /**
        * Constructor which sets the data structure, the executor and optionally the parent object.
        * @param packages list of packages. Need to contain the Scenarios and Applications displayed in the ScenarioListView.
        * @param executor executor used to start, stop and update status of the Applications and Scenarios.
        * @param parent standard QT option to specify a parent
        */
        ScenarioListController(Data_Structure::PackageVectorPtr packages, Exec::ExecutorPtr executor, QObject* parent = 0);

        /**
        * Destructor.
        */
        ~ScenarioListController() override;

        /**
        * Returns the model used by the ScenarioListView and managed by this controller.
        * @return managed model
        */
        FilterableTreeModelSortFilterProxyModelPtr getTreeModel();

        /**
         * @brief fetches application stati over their designated strategy.
         * This is potentially slow and should not be done in the GUI thread.
         */
        void fetchStati();

        static bool StartScenario(ScenarioManager::Data_Structure::ScenarioPtr scenario,
                                  ScenarioStartModes mode, Exec::ExecutorPtr executor, IceGrid::AdminPrx iceAdmin);

    signals:
        /**
        * Gets emitted after changes have been made to the data structure. Other controllers should update their models.
        */
        void updated();

        /**
         * Gets emitted after changes have been made to the States of the Applications. The DetailedApplicationView should update.
         */
        void statusUpdated();
        void statusFetched(ApplicationInstanceStatusMap stati);

        /**
        * Gets emitted after an ApplicationInstance has been clicked.
        * @param scenario clicked ApplicationInstance
        */
        void applicationInstanceClicked(Data_Structure::ApplicationInstancePtr appInstance, ScenarioItem* item);

        /**
        * Gets emitted after a Scenario has been clicked.
        * @param scenario clicked Scenario
        */
        void scenarioClicked(Data_Structure::ScenarioPtr scenario);

        void modelUpdated(FilterableTreeModelSortFilterProxyModelPtr model);

    public slots:
        /**
        * Shows a view that allows the user to create a new Scenario. This scenario will then be added
        * to the data structure and models.
        */
        void createScenario();

        /**
        * Updates the model by reloading all scenarios and applications.
        */
        void updateModel();

        /**
        * Updates the statuses of all Applications and Scenarios.
        */
        void updateStati();
        void updateStati(ApplicationInstanceStatusMap stati);


        /**
        * Removes an item from the model. Can either be an ApplicationInstance or an Scenario
        * @param index of the model to be removed
        */
        void removeItem(QModelIndex item);

        /**
        * Creates a new scenario in the package.
        * @param name name of the new scenario
        * @param package name of the package
        */
        void createdScenario(std::string name, std::string package);

        /**
        * Starts or stops the object in the specified location.
        * @param row row the object is in
        * @param column column the object is in
        * @param parent parent of the object. Needed to differentiate between scenarios and applications.
        */
        void start(int row, int column, QModelIndex parent, ScenarioStartModes mode);
        void stop(int row, int column, QModelIndex parent);
        void stopUpdateTask();

        /**
        * Restarts the object in the specified location.
        * @param row row the object is in
        * @param column column the object is in
        * @param parent parent of the object. Needed to differentiate between scenarios and applications.
        * @see start_stop(int row, int column, QModelIndex parent);
        */
        void restart(int row, int column, QModelIndex parent);

        /**
        * Adds applications to a scenario.
        * @param applications list of applications to be added
        * @param row
        * @param parent model-index of the scenario
        */
        void addApplicationsToScenario(QList<QPair<QString, ScenarioManager::Data_Structure::Application*> > applications,
                                       int row, const QModelIndex& parent);

        /**
        * Calculates the object at the given index and signals to show it.
        * @param index index of the object
        */
        void showApplication(const QModelIndex& index);

        void saveScenario(ScenarioManager::Data_Structure::ApplicationInstancePtr application);

        /**
         * @brief Set an IceAdmin for the controller. Needet to start an application via ice
         * @param IceGrid::AdminPrx
         */
        void setIceAdmin(IceGrid::AdminPrx iceAdmin);

    private:
        void startScenario(ScenarioItem* scenarioItem, ScenarioStartModes mode);
        int findScenario(ScenarioItem* rootItem, std::string name, std::string packageName);
        QModelIndex findSubScenarioModelIndex(std::string scenarioName, std::string packageName);
        QModelIndex findApplicationModelIndex(ScenarioManager::Data_Structure::ApplicationInstancePtr application);
        QModelIndex findSubScenarioModelIndexByScenarioIndex(QModelIndex scenarioIndex, std::string packageName);

        static void ShowWarningDialog(QString message, bool showOnce = false, QString messageId = "");

    private:
        ScenarioModel treemodel;
        FilterableTreeModelSortFilterProxyModelPtr model;
        Data_Structure::PackageVectorPtr packages;
        // used for fetching application stati, list set before every update by gui thread
        std::vector<ApplicationInstancePtr> applicationInstances;
        std::mutex applicationInstanceMutex;
        armarx::SimplePeriodicTask<>::pointer_type updateTask;
        Exec::ExecutorPtr executor;
        IceGrid::AdminPrx iceAdmin;
        CreateScenarioView createScenarioView;

    };
}


