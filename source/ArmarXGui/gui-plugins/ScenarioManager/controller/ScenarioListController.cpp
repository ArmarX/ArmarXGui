/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "ScenarioListController.h"

#include "../gui/namelocationview.h"
#include "../gui/scenarioitem.h"
#include <ArmarXCore/util/ScenarioManagerCommon/executor/StarterFactory.h>
#include <ArmarXCore/util/ScenarioManagerCommon/parser/PackageBuilder.h>
#include <ArmarXCore/util/ScenarioManagerCommon/parser/DependenciesGenerator.h>
#include <ArmarXCore/util/ScenarioManagerCommon/parser/XMLScenarioParser.h>
#include <ArmarXCore/util/ScenarioManagerCommon/parser/iceparser.h>
#include <ArmarXCore/util/ScenarioManagerCommon/parser/StatusManager.h>
#include <ArmarXCore/util/ScenarioManagerCommon/executor/StopStrategyFactory.h>
#include <ArmarXCore/util/ScenarioManagerCommon/executor/StopperFactory.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <QTimer>
#include <QSettings>
#include <QMessageBox>
#include <sstream>
#include <QApplication>
#include <SimoxUtility/algorithm/string/string_tools.h>

using namespace ScenarioManager;
using namespace Controller;
using namespace Data_Structure;
using namespace Parser;
using namespace Exec;

#define UPDATE_TIMER 500 //ms

ScenarioListController::ScenarioListController(PackageVectorPtr packages, ExecutorPtr executor, QObject* parent)
    : QObject(parent),
      treemodel(),
      model(new FilterableTreeModelSortFilterProxyModel()),
      packages(packages), executor(executor)
{
    qRegisterMetaType<ApplicationInstanceStatusMap>("ApplicationInstanceStatusMap");
    qRegisterMetaType<FilterableTreeModelSortFilterProxyModelPtr>("FilterableTreeModelSortFilterProxyModelPtr");

    QTimer* timer = new QTimer(this);
    QObject::connect(timer, SIGNAL(timeout()), this, SLOT(updateStati()));
    timer->start(UPDATE_TIMER);

    updateTask = new armarx::SimplePeriodicTask<>([&]
    {
        fetchStati();
    }, 100, false, "ScenarioManagerUpdateStatusTask");
    updateTask->start();

    model->setDynamicSortFilter(true);
    model->setSourceModel(&treemodel);

    QObject::connect(&createScenarioView, SIGNAL(created(std::string, std::string)),
                     this, SLOT(createdScenario(std::string, std::string)));

    QObject::connect(&treemodel, SIGNAL(applicationsDrop(QList<QPair<QString, ScenarioManager::Data_Structure::Application*>>, int, QModelIndex)),
                     this, SLOT(addApplicationsToScenario(QList<QPair<QString, ScenarioManager::Data_Structure::Application*>>, int, QModelIndex)));
    QObject::connect(this, SIGNAL(statusFetched(ApplicationInstanceStatusMap)), this, SLOT(updateStati(ApplicationInstanceStatusMap)));
}

ScenarioListController::~ScenarioListController()
{
    updateTask->stop();
}

FilterableTreeModelSortFilterProxyModelPtr ScenarioListController::getTreeModel()
{
    return model;
}

void ScenarioListController::stop(int row, int column, QModelIndex parent)
{
    ScenarioItem* item = model->data(model->index(row, column, parent), SCENARIOITEMSOURCE).value<ScenarioItem*>();
    if (item == nullptr)
    {
        return;
    }
    if (parent.isValid())
    {
        if (parent.parent().isValid())  //stop application
        {
            if (!item->isEnabled())
            {
                return;
            }
            ApplicationInstancePtr instance = item->getApplicationInstance();
            executor->stopApplication(instance);
        }
        else     //stop packageSubpackage
        {
            if (!item->isEnabled())
            {
                return;
            }
            for (const auto& app : *item->getApplicationInstances())
            {
                executor->stopApplication(app);
            }
        }

    }
    else   //stop scenario
    {
        ScenarioPtr scenario = item->getScenario();
        StatusManager statusManager;
        if (statusManager.isIceScenario(scenario))
        {
            executor->removeScenario(scenario);
        }
        else
        {
            executor->stopScenario(scenario);
        }
    }
}

void ScenarioListController::stopUpdateTask()
{
    updateTask->stop();
}

void ScenarioListController::start(int row, int column, QModelIndex parent, ScenarioStartModes mode)
{
    ScenarioItem* item = model->data(model->index(row, column, parent), SCENARIOITEMSOURCE).value<ScenarioItem*>();
    if (!item)
    {
        return;
    }
    if (parent.isValid())
    {
        auto deploy = [&](ScenarioPtr scenario)
        {
            ARMARX_CHECK_EXPRESSION(scenario);

            //            if (mode == IceStart || st)
            {
                executor->setStarter(StarterFactory::getFactory()->getIceStarter(iceAdmin), scenario);
                StatusManager statusManager;
                statusManager.setIceScenario(scenario, true);
                if (!executor->isScenarioDeployed(scenario))
                {
                    try
                    {
                        executor->deployScenario(scenario).get();
                    }
                    catch (IceGrid::DeploymentException& ex)
                    {
                        ShowWarningDialog(QString::fromStdString(ex.reason));
                        return;
                    }

                }
            }
        };


        if (parent.parent().isValid())  //start application
        {
            if (!item->isEnabled())
            {
                return;
            }
            ApplicationInstancePtr instance = item->getApplicationInstance();
            instance->updateFound();
            if (!instance->getFound())
            {
                ShowWarningDialog("The binary `" + QString::fromStdString(instance->getExecutableAbsPath()) +  "` for the application " + QString::fromStdString(instance->getName()) + " could not be found");
                treemodel.update();
                return;
            }
            auto scenario = instance->getScenario();
            StatusManager s;
            if (s.isIceScenario(scenario))
            {
                deploy(scenario);
            }
            executor->startApplication(instance);
        }
        else     // start packagesubtree
        {
            if (!item->isEnabled())
            {
                return;
            }
            ARMARX_CHECK_EXPRESSION(item->getApplicationInstances());
            if (!item->getApplicationInstances()->empty())
            {
                ARMARX_CHECK_EXPRESSION(item->getApplicationInstances()->at(0));
                StatusManager s;
                if (s.isIceScenario(item->getApplicationInstances()->at(0)->getScenario()))
                {
                    deploy(item->getApplicationInstances()->at(0)->getScenario());
                }
            }
            for (const auto& app : *item->getApplicationInstances())
            {
                app->updateFound();
                if (!app->getFound())
                {
                    ShowWarningDialog("The Binary for the Application " + QString::fromStdString(app->getName()) + " could not be found");
                    treemodel.update();
                    return;
                }

                executor->startApplication(app);
            }
        }
    }
    else   //start scenario
    {
        startScenario(item, mode);
    }
}

void ScenarioListController::startScenario(ScenarioItem* scenarioItem, ScenarioStartModes mode)
{
    ScenarioPtr scenario = scenarioItem->getScenario();
    StartScenario(scenario, mode, executor, iceAdmin);
}

void ScenarioListController::restart(int row, int column, QModelIndex parent)
{
    //hole entsprechende application aus dem package und uebergebe sie dem executor zum restarten & update model entsprechend
    ScenarioItem* item = model->data(model->index(row, column, parent), SCENARIOITEMSOURCE).value<ScenarioItem*>();
    if (item == nullptr || !item->isEnabled())
    {
        return;
    }
    if (parent.isValid())
    {
        if (parent.parent().isValid())
        {
            //start/stop application
            executor->restartApplication(item->getApplicationInstance());
        }
        else
        {
            //start/stop packageSubtree
            for (const auto& app : *item->getApplicationInstances())
            {
                executor->restartApplication(app);
            }
        }

    }
    else   //start/stop scenario
    {
        try
        {
            executor->restartScenario(item->getScenario());
        }
        catch (IceGrid::ServerStartException& ex)
        {
            ShowWarningDialog("Ice had an launching error. Please make sure your remote launch settings are correct");
        }
    }
}

void ScenarioListController::addApplicationsToScenario(
    QList<QPair<QString,
    ScenarioManager::Data_Structure::Application*>> applications,
    int row, const QModelIndex& parent)
{
    ScenarioItem* item;//reinterpret_cast<ScenarioItem*>(model->data(parent, SCENARIOITEM).data());
    QModelIndex scenarioIndex;

    if (parent.parent().isValid() && parent.parent().parent().isValid())
    {
        item = model->data(parent.parent().parent(), SCENARIOITEMPROXY).value<ScenarioItem*>();
        scenarioIndex = parent.parent().parent();
    }
    else if (parent.parent().isValid())
    {
        item = model->data(parent.parent(), SCENARIOITEMPROXY).value<ScenarioItem*>();
        scenarioIndex = parent.parent();
    }
    else
    {
        item = model->data(parent, SCENARIOITEMPROXY).value<ScenarioItem*>();
        scenarioIndex = parent;
    }

    if (item == nullptr)
    {
        return;
    }

    ScenarioPtr scenario = item->getScenario();
    if (!scenario->isScenarioFileWriteable())
    {
        QMessageBox::critical(0, "Adding application failed!", "The scenario '" + QString::fromStdString(scenario->getName()) + "' is read only. You cannot add an application to this scenario!");
        return;
    }
    Parser::DependenciesGenerator generator;
    std::vector<std::string> acceptedApps = generator.getDependencieTree(scenario->getPackage()->getName());


    for (const auto& pair : applications)
    {
        ScenarioManager::Data_Structure::Application* app = pair.second;
        PackagePtr appPackage;
        for (const auto& package : *packages)
        {
            for (const auto& pApp : *package->getApplications())
            {
                if (pApp->getName().compare(app->getName()) == 0)
                {
                    appPackage = package;
                }
            }
        }

        if (appPackage.get() == nullptr)
        {
            return;
        }

        if (std::find(acceptedApps.begin(), acceptedApps.end(), appPackage->getName()) == acceptedApps.end())
        {
            const std::string dependenciesStr = simox::alg::join(acceptedApps, ", ");

            QMessageBox box;
            QString message("You cannot drop an app from ");
            message.append(QString::fromStdString(appPackage->getName()));
            message.append(" into a scenario from ");
            message.append(QString::fromStdString(scenario->getPackage()->getName()));
            message.append(QString::fromStdString(". Package `" + scenario->getPackage()->getName() + "` depends on " + dependenciesStr));
            box.setText(message);
            box.exec();
            return;
        }


        const std::string appName = app->getName();

        int instanceCounter = 0;
        std::string instanceName = pair.first.toStdString();
        if (instanceName.empty())
        {
            for (const ApplicationInstancePtr& instances : *scenario->getApplications())
            {
                if (instances->getName().compare(appName) == 0)
                {
                    instanceCounter++;
                }
            }
            if (instanceCounter == 0)
            {
                instanceName = "";
            }
            else
            {
                instanceName = "instance" + std::to_string(instanceCounter);
            }

        }
        else
        {
            for (const ApplicationInstancePtr& instances : *scenario->getApplications())
            {
                if (instances->getInstanceName().compare(instanceName) == 0)
                {
                    instanceCounter++;
                }
            }
            if (instanceCounter > 0)
            {
                instanceName += std::to_string(instanceCounter);
            }
        }


        std::string configPath = scenario->getPath();
        configPath = configPath.substr(0, configPath.find_last_of('/'));
        configPath.append("/config/");


        configPath.append(app->getName());
        if (!instanceName.empty())
        {
            configPath.append(".");
            configPath.append(instanceName);
        }
        configPath.append(".cfg");

        std::ofstream out(configPath);
        out.close();

        ApplicationInstancePtr appInstance(new ApplicationInstance(app->getExecutableName(), app->getPathToExecutable(), instanceName, configPath, appPackage->getName(), scenario, "", true, false));

        std::string cacheDir = Parser::IceParser::getCacheDir();

        executor->loadAndSetCachedProperties(appInstance, cacheDir, true);
        appInstance->load(true);

        for (const auto& elem : app->getProperties()->getProperties()->getPropertiesForPrefix(""))
        {
            appInstance->modifyProperty(elem.first, elem.second);
            appInstance->setDefaultPropertyEnabled(elem.first, true);
        }

        appInstance->save();

        scenario->addApplication(appInstance);

        QModelIndex subScenarioIndex = findSubScenarioModelIndexByScenarioIndex(scenarioIndex, appPackage->getName());;

        //if this is unvalid than the user dropped an app out of an package that is not currently used inside the scenario
        //so create subscenario item
        //this behaviour was kind of buggy be careful while optimising
        if (!subScenarioIndex.isValid())
        {
            std::vector<ApplicationInstancePtr> list;

            ScenarioItem* subScenarioItem = new ScenarioItem(appPackage->getName(), list);

            treemodel.insertRow(0, subScenarioItem, scenarioIndex);

            subScenarioIndex = findSubScenarioModelIndexByScenarioIndex(scenarioIndex, appPackage->getName());

            treemodel.insertRow(0, new ScenarioItem(appInstance), subScenarioIndex);
        }
        else
        {
            treemodel.insertRow(0, new ScenarioItem(appInstance), subScenarioIndex);
        }
    }
    scenario->save();

}

void ScenarioListController::removeItem(QModelIndex item)
{
    ScenarioItem* scenItem;
    if (item.parent().isValid() && item.parent().parent().isValid())
    {
        scenItem = model->data(item.parent().parent(), SCENARIOITEMSOURCE).value<ScenarioItem*>();

        if (scenItem == nullptr)
        {
            return;
        }

        ScenarioItem* appItem = model->data(item, SCENARIOITEMSOURCE).value<ScenarioItem*>();

        ScenarioPtr scenario = scenItem->getScenario();
        scenario->removeApplication(appItem->getApplicationInstance());

        scenario->save();
        treemodel.removeRow(model->mapToSource(item).row(), model->mapToSource(item.parent()));
    }
    else if (item.parent().isValid())
    {
        scenItem = model->data(item.parent(), SCENARIOITEMSOURCE).value<ScenarioItem*>();

        if (scenItem == nullptr)
        {
            return;
        }

        ScenarioItem* packageItem = model->data(item, SCENARIOITEMSOURCE).value<ScenarioItem*>();

        ScenarioPtr scenario = scenItem->getScenario();

        for (const auto& app : *packageItem->getApplicationInstances())
        {
            scenario->removeApplication(app);
        }
        scenario->save();
        treemodel.removeRow(model->mapToSource(item).row(), model->mapToSource(item.parent()));
    }
    else
    {
        scenItem = model->data(item, SCENARIOITEMSOURCE).value<ScenarioItem*>();

        if (scenItem == nullptr)
        {
            return;
        }

        ScenarioPtr scen = scenItem->getScenario();

        QSettings settings("KIT", "ScenarioManager");
        QStringList scenarios = settings.value("scenarios").toStringList();
        scenarios.removeDuplicates();
        QString toFind(QString::fromStdString(scen->getName()));
        toFind.append("::Package::");
        toFind.append(QString::fromStdString(scen->getPackage()->getName()));
        scenarios.removeAt(scenarios.indexOf(toFind));
        settings.setValue("scenarios", scenarios);

        for (const auto& package : *packages)
        {
            for (auto it = package->getScenarios()->begin(); it != package->getScenarios()->end(); ++it)
            {
                if (scen.get() == it->get())
                {
                    package->getScenarios()->erase(it);
                    break;
                }
            }
        }
        treemodel.removeRow(model->mapToSource(item).row(), model->mapToSource(item.parent()));
        emit updated();
    }
}

void ScenarioListController::createScenario()
{
    ARMARX_TRACE;

    if (packages->size() != 0)
    {
        QVector<QPair<QString, bool> > packageNames;
        for (const auto& package : *packages)
        {
            packageNames << qMakePair(QString::fromStdString(package->getName()), package->isScenarioPathWritable());
        }
        createScenarioView.setPackages(packageNames);
        createScenarioView.exec();
    }
    else
    {
        QMessageBox messageBox;
        messageBox.setText("You have to have at least one Package open to create an Scenario");
        messageBox.exec();
    }

    ARMARX_TRACE;
}

void ScenarioListController::createdScenario(std::string name, std::string packageStr)
{
    XMLScenarioParser parser;

    PackagePtr package;
    for (unsigned int i = 0; i < packages->size(); i++)
    {
        if (packages->at(i)->getName().compare(packageStr) == 0)
        {
            package = packages->at(i);
            break;
        }
    }
    if (package.get() == nullptr)
    {
        return;
    }

    if (parser.isScenarioexistent(name, package))
    {
        QMessageBox box;
        QString message("The Scenario " + QString::fromStdString(name) + " already exists in Package " + QString::fromStdString(packageStr));
        box.setText(message);
        box.exec();
        return;
    }

    ScenarioPtr ptr = parser.createNewScenario(name, package);
    if (ptr.get() == nullptr)
    {
        return;
    }

    for (const auto& package : *packages)
    {
        if (package->getName().compare(packageStr) == 0)
        {
            package->addScenario(ptr);
            treemodel.insertRow(0, new ScenarioItem(ptr));
            break;
        }
    }

    QSettings settings("KIT", "ScenarioManager");
    QStringList scenarios = settings.value("scenarios").toStringList();
    scenarios.removeDuplicates();
    QString toAdd;
    toAdd.append(QString::fromStdString(ptr->getName()));
    toAdd.append("::Package::");
    toAdd.append(QString::fromStdString(package->getName()));
    scenarios.append(toAdd);
    settings.setValue("scenarios", scenarios);
    emit updated();
}

void ScenarioListController::showApplication(const QModelIndex& index)
{

    ScenarioItem* item = model->data(index, SCENARIOITEMSOURCE).value<ScenarioItem*>();
    if (index.column() > 0 || item == nullptr)
    {
        return;
    }

    if (index.parent().isValid())
    {
        if (index.parent().parent().isValid())
        {
            ApplicationInstancePtr appInstance = item->getApplicationInstance();
            appInstance->updateFound();
            if (!appInstance->getFound())
            {
                QMessageBox message;
                message.setText("Could not find Application " + item->data(0).toString() + " at " + QString::fromStdString(appInstance->getPathToExecutable()) + ".");
                message.exec();
                if (!appInstance->getFound())
                {
                    updateModel();
                    emit updated();
                }
                return;
            }

            emit applicationInstanceClicked(appInstance, item);
        }
        else
        {
            return;
        }
    }
    else
    {
        emit scenarioClicked(item->getScenario());
    }
}

void ScenarioListController::updateModel() //Scenarios don't have a setStatus-method, therefore UI should calculate Scenario status based on other
{
    ScenarioItem* rootItem = treemodel.getRootItem();

    for (const auto& package : *packages)
    {
        auto scenarios = package->getScenarios();
        for (const auto& scenario : *scenarios)
        {
            std::map<std::string, std::vector<ApplicationInstancePtr>> packageSubtrees;
            for (const auto& app : *scenario->getApplications())
            {
                std::string packageName = app->getPackageName();

                if (packageSubtrees.count(packageName) == 0)
                {
                    packageSubtrees[packageName] = std::vector<ApplicationInstancePtr>();
                }
                packageSubtrees[packageName].push_back(app);
            }

            ScenarioItem* scenarioItem;
            int index = findScenario(rootItem, scenario->getName(), scenario->getPackage()->getName());
            if (index == -1)
            {
                scenarioItem = new ScenarioItem(scenario);

                for (const auto& it : packageSubtrees)
                {
                    ScenarioItem* currentItem = new ScenarioItem(it.first, it.second);
                    scenarioItem->appendChild(currentItem);
                }
                treemodel.insertRow(0, scenarioItem);
            }
            else
            {
                scenarioItem = static_cast<ScenarioItem*>(rootItem->child(index));

                for (int i = 0; i < scenarioItem->childCount(); i++)
                {
                    ScenarioItem* subPackageItem = static_cast<ScenarioItem*>(scenarioItem->child(i));
                    std::string name = subPackageItem->data(0).toString().toStdString();

                    std::vector<ApplicationInstancePtr> currentBuildSubtree = packageSubtrees[name];
                    ApplicationInstanceVectorPtr currentTreeSubtree = subPackageItem->getApplicationInstances();
                    for (size_t k = 0; k < currentTreeSubtree->size(); k++)
                    {
                        bool found = false;
                        for (size_t j = 0; j < currentBuildSubtree.size(); j++)
                        {
                            if (currentTreeSubtree->at(k)->getName() == currentBuildSubtree[j]->getName()
                                && currentTreeSubtree->at(k)->getInstanceName() == currentBuildSubtree[j]->getInstanceName())
                            {
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                        {
                            treemodel.insertRow(0, new ScenarioItem(currentTreeSubtree->at(k)), findSubScenarioModelIndex(scenario->getName(), name));
                        }
                    }
                }
            }
        }
    }
}

QModelIndex ScenarioListController::findApplicationModelIndex(ScenarioManager::Data_Structure::ApplicationInstancePtr application)
{
    QModelIndex subScenarioModelIndex = findSubScenarioModelIndex(application->getScenario()->getName(), application->getPackageName());
    if (!subScenarioModelIndex.isValid())
    {
        return QModelIndex();
    }
    int count = 0;
    QModelIndex appIndex = subScenarioModelIndex.child(0, 0);
    while (true)
    {
        appIndex = appIndex.sibling(count, 0);
        if (!appIndex.isValid())
        {
            return QModelIndex();
        }
        if (application->getInstanceName().empty())
        {
            if (application->getName() == appIndex.data().toString().toStdString())
            {
                return appIndex;
            }
        }
        else
        {
            if (application->getInstanceName() + "." + application->getName() == appIndex.data().toString().toStdString())
            {
                return appIndex;
            }
        }

        count++;
    }
}

QModelIndex ScenarioListController::findSubScenarioModelIndex(std::string scenarioName, std::string packageName)
{
    QModelIndex scenarioIndex = treemodel.index(0, 0);

    bool scenarioFound = false;
    int count = 0;
    while (!scenarioFound)
    {
        scenarioIndex = scenarioIndex.sibling(count, 0);

        if (!scenarioIndex.isValid())
        {
            return QModelIndex();
        }
        if (scenarioName == scenarioIndex.data().toString().toStdString())
        {
            scenarioFound = true;
            break;
        }

        count++;
    }

    count = 0;
    while (true)
    {
        QModelIndex currentSubPackageIndex = scenarioIndex.child(count, 0);
        if (!currentSubPackageIndex.isValid())
        {
            return QModelIndex();
        }
        if (packageName == currentSubPackageIndex.data().toString().toStdString())
        {
            return currentSubPackageIndex;
        }
        count++;
    }

    return QModelIndex();
}

QModelIndex ScenarioListController::findSubScenarioModelIndexByScenarioIndex(QModelIndex scenarioIndex, std::string packageName)
{
    int count = 0;
    while (true)
    {
        QModelIndex currentSubPackageIndex = scenarioIndex.child(count, 0);
        if (packageName == currentSubPackageIndex.data().toString().toStdString())
        {
            return currentSubPackageIndex;
        }
        if (!currentSubPackageIndex.isValid())
        {
            return QModelIndex();
        }
        count++;
    }

    return QModelIndex();
}

//looks if the current scenario is already added
int ScenarioListController::findScenario(ScenarioItem* rootItem, std::string name, std::string packageName)
{
    for (int i = 0; i < rootItem->childCount(); i++)
    {
        if (static_cast<ScenarioItem*>(rootItem->child(i))->getScenario()->getName() == name
            && static_cast<ScenarioItem*>(rootItem->child(i))->getScenario()->getPackage()->getName() == packageName)
        {
            return i;
        }
    }

    return -1;
}


void ScenarioListController::fetchStati()
{
    std::vector<ApplicationInstancePtr> apps;
    {
        std::unique_lock lock(applicationInstanceMutex);
        apps.swap(applicationInstances);
    }
    if (apps.empty())
    {
        return;
    }
    ApplicationInstanceStatusMap stati;
    IceUtil::Time start = IceUtil::Time ::now();
    //update all app instances and scenarios
    for (const ApplicationInstancePtr&  appInst : apps)
    {
        std::string status = executor->getApplicationStatus(appInst);
        stati[appInst] = status;

    }
    ARMARX_DEBUG << deactivateSpam(5) << "Fetching scenario stati took " << (IceUtil::Time::now() - start).toMilliSecondsDouble() << " ms";
    emit statusFetched(stati);
}

bool ScenarioListController::StartScenario(ScenarioPtr scenario, ScenarioStartModes mode, ExecutorPtr executor, IceGrid::AdminPrx iceAdmin)
{
    StatusManager statusManager;
    StopStrategyFactory stopStrategyFactory;

    if (mode == LocalStart)
    {
        if (statusManager.isIceScenario(scenario) && scenario->getStatus() != ApplicationStatus::Stopped)
        {
            QMessageBox::StandardButton reply;
            reply = QMessageBox::question(nullptr, "", "The Scenario is currently running via Ice do you want to remove it ?", QMessageBox::Yes | QMessageBox::No);
            if (reply == QMessageBox::No)
            {
                return false;
            }
            else
            {
                executor->setStarter(StarterFactory::getFactory()->getIceStarter(iceAdmin), scenario);
                executor->setStopStrategy(stopStrategyFactory.getKillStrategy(StopperFactory::getFactory()->getIceStopper(iceAdmin)), scenario);

                statusManager.setIceScenario(scenario, true);
                executor->removeScenario(scenario);
                return false;
            }
        }
        executor->setStarter(StarterFactory::getFactory()->getStarter(), scenario);
        executor->setStopStrategy(executor->getDefaultStopStrategy(), scenario);

        statusManager.setIceScenario(scenario, false);

        if (!scenario->allApplicationsFound())
        {
            ShowWarningDialog("Not all binaries in the Scenario " + QString::fromStdString(scenario->getName()) + " could be found", true, QString::fromStdString(scenario->getName() + "BinariesMissing"));
            //            return false;
        }
        executor->startScenario(scenario);
    }
    else
    {
        if (!statusManager.isIceScenario(scenario) && scenario->getStatus() != ApplicationStatus::Stopped)
        {
            QMessageBox::StandardButton reply;
            reply = QMessageBox::question(nullptr, "", "The Scenario is currently running locally - do you want to stop it ?", QMessageBox::Yes | QMessageBox::No);
            if (reply == QMessageBox::No)
            {
                return false;
            }
            else
            {
                executor->setStarter(StarterFactory::getFactory()->getStarter(), scenario);
                executor->setStopStrategy(executor->getDefaultStopStrategy(), scenario);

                executor->stopScenario(scenario);
                return false;
            }
        }
        executor->setStarter(StarterFactory::getFactory()->getIceStarter(iceAdmin), scenario);
        executor->setStopStrategy(stopStrategyFactory.getKillStrategy(StopperFactory::getFactory()->getIceStopper(iceAdmin)), scenario);

        if (!scenario->allApplicationsFound())
        {
            ShowWarningDialog("Not all binaries in the Scenario " + QString::fromStdString(scenario->getName()) + " could be found", true, QString::fromStdString(scenario->getName() + "BinariesMissing"));
            //            updateStati();
            //            return true;
        }

        statusManager.setIceScenario(scenario, true);
        if (!executor->isScenarioDeployed(scenario))
        {
            try
            {
                executor->deployScenario(scenario).get();
            }
            catch (IceGrid::DeploymentException& ex)
            {
                ShowWarningDialog(QString::fromStdString(ex.reason));
                return false;
            }

        }
        //        else
        {
            try
            {
                executor->startScenario(scenario).get();
            }
            catch (IceGrid::ServerStartException& ex)
            {
                ShowWarningDialog("Ice had an launching error. Please make sure your remote launch settings are correct. Did you sync everything?\nId: " + QString::fromStdString(ex.id)
                                  + "\nReason: " + QString::fromStdString(ex.reason)) ;
                return false;
            }
        }
    }
    return true;
}


void ScenarioListController::updateStati()
{
    //update all app instances and scenarios
    // but this needs to be done async -> first get list of applications,
    // then send these to other thread
    std::vector<ApplicationInstancePtr> apps;
    for (ScenarioManager::Data_Structure::PackagePtr& package : *packages)
    {
        ScenarioVectorPtr scenarios = package->getScenarios();
        for (const ScenarioPtr& scenario : *scenarios)
        {
            ApplicationInstanceVectorPtr appInsts = scenario->getApplications();
            for (const ApplicationInstancePtr&  appInst : *appInsts)
            {
                apps.push_back(appInst);
            }
        }
    }

    {
        std::unique_lock lock(applicationInstanceMutex);
        apps.swap(applicationInstances);
    }
}

void ScenarioListController::updateStati(ApplicationInstanceStatusMap stati)
{
    bool changed = false;
    for (auto& pair : stati)
    {

        bool statusChanged = pair.first->setStatus(pair.second);
        changed |= statusChanged;
    }
    //    ARMARX_DEBUG << deactivateSpam(5) << "Updating scenarios took " << (IceUtil::Time::now() - start).toMilliSecondsDouble() << " ms";
    if (changed)
    {
        auto start = IceUtil::Time ::now();
        treemodel.update();
        ARMARX_DEBUG << deactivateSpam(5) << "Updating scenario Model took " << (IceUtil::Time::now() - start).toMilliSecondsDouble() << " ms";
    }

    emit statusUpdated();
}

void ScenarioListController::saveScenario(ScenarioManager::Data_Structure::ApplicationInstancePtr application)
{
    for (const auto& package : *packages)
    {
        for (const auto& scenario : *package->getScenarios())
        {
            if (application->getScenario()->getName() == scenario->getName())
            {
                std::string cacheDir = Parser::IceParser::getCacheDir();
                executor->loadAndSetCachedProperties(application, cacheDir, false, false);
                qApp->processEvents();

                scenario->save();
                return;
            }
        }
    }
}

void ScenarioListController::setIceAdmin(IceGrid::AdminPrx iceAdmin)
{
    this->iceAdmin = iceAdmin;
}

void ScenarioListController::ShowWarningDialog(QString message, bool showOnce, QString messageId)
{
    static std::set<QString> dialogIds;
    if (showOnce)
    {
        ARMARX_CHECK_EXPRESSION(!messageId.isEmpty());
        if (dialogIds.count(messageId))
        {
            return;
        }
        dialogIds.insert(messageId);
    }
    QMessageBox box;
    box.setText(message);
    box.exec();
}
