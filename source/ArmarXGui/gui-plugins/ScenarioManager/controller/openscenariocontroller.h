/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include "../gui/treemodel.h"
#include "../gui/openscenariomodel.h"
#include "../gui/openscenarioitem.h"
#include "../gui/openscenarioview.h"
#include <ArmarXCore/util/ScenarioManagerCommon/data_structure/Package.h>
#include <ArmarXCore/util/ScenarioManagerCommon/executor/Executor.h>
#include <QObject>
#include <QSet>
#include <QStringList>
#include <string>
#include <vector>

namespace ScenarioManager::Controller
{
    /**
    * @class OpenScenarioController
    * @ingroup controller
    * @brief Manages the signals and model of the OpenScenarioView.
    * This Controller manages opening and loading in a scenario.
    */
    class OpenScenarioController : public QObject
    {
        Q_OBJECT
    public:
        /**
        * Constructor that sets the data structure and optionally the parent object.
        * @param packages list of packages
        * @param parent standard optional QT parent object
        */
        OpenScenarioController(Data_Structure::PackageVectorPtr packages, Exec::ExecutorPtr exec, QObject* parent = 0);
        ~OpenScenarioController() override;


    signals:
        /**
        * This signal gets emitted if the data structure has changed.
        */
        void updated();
        void showPackageDialog();

    public slots:
        /**
        * Shows the OpenScenarioView which allows the User to open and load in a Scenario.
        */
        void showOpenScenarioView();
        void on_showAddPackageDialog();

        /**
        * Finds a scenario in the model, based on the given data and
        * updates the current model and data structure to include the given scenario.
        * @param row row the scenario is in
        * @param column column the scenario is in
        * @param parent parent index of the scenario
        */
        void on_openScenario(int row, int column, QModelIndex parent);

        /**
        * Reloads the packages into the model.
        */
        void updateModel();

    private:
        QStringList getAllClosedScenarios();
        QStringList getFavouriteScenarios(QStringList allClosedScenarios);


    private:
        const std::size_t favlistSize;

        Data_Structure::PackageVectorPtr packages;
        Exec::ExecutorPtr executor;
        OpenScenarioModelPtr treemodel;
        FilterableTreeModelSortFilterProxyModelPtr model;
        OpenScenarioView openScenarioView;
    };
}

