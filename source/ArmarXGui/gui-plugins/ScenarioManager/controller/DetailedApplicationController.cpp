/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DetailedApplicationController.h"
#include "ScenarioListController.h"

#include <ArmarXCore/util/ScenarioManagerCommon/data_structure/Scenario.h>
#include <ArmarXCore/util/ScenarioManagerCommon/parser/iceparser.h>
#include <ArmarXCore/util/ScenarioManagerCommon/executor/StopStrategyFactory.h>
#include <ArmarXCore/util/ScenarioManagerCommon/executor/StarterFactory.h>
#include <ArmarXCore/util/ScenarioManagerCommon/executor/StopperFactory.h>
#include <QDesktopServices>
#include <QUrl>
#include <QMessageBox>
#include <string>
#include <stdlib.h>


using namespace ScenarioManager;
using namespace Controller;
using namespace Exec;
using namespace Data_Structure;

DetailedApplicationController::DetailedApplicationController(ExecutorPtr executor, QObject* parent) : QObject(parent), executor(executor), showingStartable(false)
{
    QObject::connect(&propertyAdderView, SIGNAL(create(std::string, std::string)),
                     this, SLOT(setProperty(std::string, std::string)));

    QObject::connect(&iceEnvVariableAddView, SIGNAL(create(std::string, std::string)),
                     this, SLOT(setIceEnvVariable(std::string, std::string)));
}

DetailedApplicationController::~DetailedApplicationController()
{
}

void DetailedApplicationController::setDetailedApplicationView(DetailedApplicationView* ptr)
{
    view = ptr;
}

void DetailedApplicationController::showApplicationInstance(ApplicationInstancePtr application, ScenarioItem* item)
{
    if (application.get() == nullptr)
    {
        return;
    }
    showingStartable = true;
    currentApplication = application;
    currentScenario = ScenarioPtr();

    executor->loadAndSetCachedProperties(application, Parser::IceParser::getCacheDir(), false, false);
    application->load();
    view->setVisible(true);
    view->showApplicationInstance(application, item);
}

void DetailedApplicationController::showApplication(ScenarioManager::Data_Structure::ApplicationPtr application)
{
    if (application.get() == nullptr)
    {
        return;
    }
    showingStartable = false;
    currentApplication = ApplicationInstancePtr();
    currentScenario = ScenarioPtr();
    ScenarioManager::Data_Structure::ApplicationPtr app(new ScenarioManager::Data_Structure::Application(*application));
    executor->loadAndSetCachedProperties(app, Parser::IceParser::getCacheDir());

    view->setVisible(true);
    view->showApplication(app);
}

void DetailedApplicationController::showScenario(ScenarioPtr scenario)
{
    if (scenario.get() == nullptr)
    {
        return;
    }
    showingStartable = true;
    currentApplication = ApplicationInstancePtr();
    currentScenario = scenario;

    view->setVisible(true);
    view->showScenario(scenario);
}

void DetailedApplicationController::showPackage(PackagePtr package)
{
    if (package.get() == nullptr)
    {
        return;
    }
    showingStartable = false;
    currentApplication = ApplicationInstancePtr();
    currentScenario = ScenarioPtr();
    view->showPackage(package);
}

void DetailedApplicationController::showInStandardEditor()
{
    if (showingStartable)
    {
        std::string url = "file://";
        if (currentApplication.get() != nullptr)
        {
            url.append(currentApplication->getConfigPath());
        }
        else if (currentScenario.get() != nullptr)
        {
            url.append(currentScenario->getGlobalConfigPath());
        }
        QDesktopServices::openUrl(QUrl(url.c_str()));
    }
}

void DetailedApplicationController::setProperty(std::string name, std::string value)
{
    if (showingStartable)
    {
        if (currentApplication.get() != nullptr)
        {
            currentApplication->addProperty(name, value);
            currentApplication->save();
            showApplicationInstance(currentApplication, nullptr);
        }
        else if (currentScenario.get() != nullptr)
        {
            currentScenario->getGlobalConfig()->defineOptionalProperty<std::string>(name, "::NOT_DEFINED::", "Custom Property");
            currentScenario->getGlobalConfig()->getProperties()->setProperty(name, value);
            currentScenario->save();
            showScenario(currentScenario);
        }
    }
}

void DetailedApplicationController::setIceEnvVariable(std::string name, std::string value)
{
    if (currentScenario.get() != nullptr)
    {
        currentScenario->addIceEnviromentVariable(name, value);
        currentScenario->save();
        showScenario(currentScenario);
    }
}

void DetailedApplicationController::start()
{
    if (showingStartable)
    {
        if (currentApplication.get() != nullptr)
        {
            executor->startApplication(currentApplication);
        }
        else if (currentScenario.get() != nullptr)
        {
            try
            {
                startScenario(currentScenario);
            }
            catch (IceGrid::ServerStartException& ex)
            {
                showWarningDialog("Ice had an launching error. Please make sure your remote launch settings are correct");
            }
        }
    }
}

void DetailedApplicationController::startScenario(ScenarioPtr scenario)
{

    StatusManager statusManager;
    ScenarioStartModes mode = LocalStart;
    ScenarioListController::StartScenario(scenario, mode, executor, iceAdmin);

    //    StatusManager statusManager;
    //    StopStrategyFactory stopStrategyFactory;

    //    if (scenario->getScenarioDeploymentType() == ScenarioDeploymentType::Local)
    //    {
    //        executor->setStarter(StarterFactory::getFactory()->getStarter(), scenario);
    //        executor->setStopStrategy(executor->getDefaultStopStrategy(), scenario);
    //        executor->startScenario(scenario);
    //    }
    //    else
    //    {
    //        executor->setStarter(StarterFactory::getFactory()->getIceStarter(iceAdmin), scenario);
    //        executor->setStopStrategy(stopStrategyFactory.getKillStrategy(StopperFactory::getFactory()->getIceStopper(iceAdmin)), scenario);
    //        if (executor->isScenarioDeployed(scenario))
    //        {
    //            try
    //            {
    //                executor->startScenario(scenario).get();
    //            }
    //            catch (IceGrid::ServerStartException& ex)
    //            {
    //                showWarningDialog("Ice had an launching error. Please make sure your remote launch settings are correct");
    //            }
    //        }
    //        else
    //        {
    //            try
    //            {
    //                executor->deployScenario(scenario).get();
    //            }
    //            catch (IceGrid::DeploymentException& ex)
    //            {
    //                showWarningDialog(QString::fromStdString(ex.reason));
    //            }
    //        }
    //    }
}


void DetailedApplicationController::stop()
{
    if (showingStartable)
    {
        if (currentApplication.get() != nullptr)
        {
            executor->stopApplication(currentApplication);
        }
        else if (currentScenario.get() != nullptr)
        {
            if (currentScenario->getScenarioDeploymentType() == ScenarioDeploymentType::Local)
            {
                executor->stopScenario(currentScenario);
            }
            else
            {
                executor->removeScenario(currentScenario);
            }
        }
    }
}

void DetailedApplicationController::save()
{
    if (showingStartable)
    {
        if (currentApplication.get() != nullptr)
        {
            currentApplication->save();
            executor->restartApplication(currentApplication);
        }
        else if (currentScenario.get() != nullptr)
        {
            currentScenario->save();
            try
            {
                executor->restartScenario(currentScenario);
            }
            catch (IceGrid::ServerStartException& ex)
            {
                showWarningDialog("Ice had an launching error. Please make sure your remote launch settings are correct");
            }
        }
    }
}

void DetailedApplicationController::deleteProperty(std::string name)
{

}

void DetailedApplicationController::showPropertyAddView()
{
    propertyAdderView.exec();
}

void DetailedApplicationController::showIceEnvVariableAddView()
{
    iceEnvVariableAddView.exec();
}

void DetailedApplicationController::showWarningDialog(QString message)
{
    QMessageBox box;
    box.setText(message);
    box.exec();
}

void DetailedApplicationController::setIceAdmin(IceGrid::AdminPrx iceAdmin)
{
    this->iceAdmin = iceAdmin;
}
