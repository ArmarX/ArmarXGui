/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "openscenariocontroller.h"

#include <ArmarXCore/util/ScenarioManagerCommon/parser/PackageBuilder.h>
#include <ArmarXCore/util/ScenarioManagerCommon/executor/StopStrategyFactory.h>
#include <ArmarXCore/util/ScenarioManagerCommon/parser/XMLScenarioParser.h>
#include <ArmarXCore/util/ScenarioManagerCommon/parser/iceparser.h>
#include <ArmarXCore/util/ScenarioManagerCommon/data_structure/Package.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <QList>
#include <QVariant>
#include <QSettings>
#include <QMessageBox>
#include <algorithm>


using namespace ScenarioManager;
using namespace Controller;
using namespace Data_Structure;
using namespace Parser;
using namespace Exec;
using namespace armarx;

OpenScenarioController::OpenScenarioController(PackageVectorPtr packages, Exec::ExecutorPtr exec, QObject* parent)
    : QObject(parent), favlistSize(5), packages(packages), executor(exec),
      treemodel(new OpenScenarioModel()),
      model(new FilterableTreeModelSortFilterProxyModel())
{
    model->setSourceModel(treemodel.get());
    //    openScenarioView.setModel(model);

    QObject::connect(&openScenarioView, SIGNAL(openScenario(int, int, QModelIndex)),
                     this, SLOT(on_openScenario(int, int, QModelIndex)));

    QObject::connect(&openScenarioView, SIGNAL(showAddPackageDialog()),
                     this, SLOT(on_showAddPackageDialog()));
}

OpenScenarioController::~OpenScenarioController()
{

}

void OpenScenarioController::on_openScenario(int row, int column, QModelIndex parent)
{
    Parser::XMLScenarioParser parser;
    //ScenarioPtr scenario = parser.parseScenario(static_cast<OpenScenarioItem*>(treemodel->index(row, column, parent).internalPointer())->getScenarioPath());

    OpenScenarioItem* openScenarioItem = model->data(model->index(row, column, parent), OPENSCENARIOITEMSOURCE).value<OpenScenarioItem*>();
    if (openScenarioItem == nullptr)
    {
        return;
    }

    //get package matching the scenario string
    PackagePtr package;
    for (unsigned int i = 0; i < packages->size(); i++)
    {
        if (openScenarioItem->getScenarioPackage().compare(packages->at(i)->getName()) == 0)
        {
            package = packages->at(i);
            break;
        }
    }

    if (package.get() == nullptr)
    {
        return;
    }

    ScenarioPtr scenario;
    auto pos = openScenarioItem->getScenarioName().find("/");
    if (pos != std::string::npos)
    {
        scenario = parser.parseScenario(package, openScenarioItem->getScenarioName().substr(pos + 1), openScenarioItem->getScenarioName().substr(0, pos));
    }
    else
    {
        scenario = parser.parseScenario(package, openScenarioItem->getScenarioName());
    }

    std::string cacheDir = Parser::IceParser::getCacheDir();



    if (scenario.get() == nullptr)
    {
        QMessageBox message;
        std::string messageStr;
        messageStr << "Failed to load Scenario " << openScenarioItem->getScenarioName() << " in Package " << openScenarioItem->getScenarioPackage() << "\n";
        messageStr << "Either the File was not found or it has no valid scx formatting";
        message.setText(QString::fromStdString(messageStr));
        message.exec();
        return;
    }

    for (const auto& it : *scenario->getApplications())
    {
        executor->loadAndSetCachedProperties(it, cacheDir, false, false);
    }

    scenario->reloadAppInstances();

    package->addScenario(scenario);

    QSettings settings("KIT", "ScenarioManager");
    //save to openend scenarios
    QStringList settingsPackages = settings.value("scenarios").toStringList();
    settingsPackages.removeDuplicates();

    QString toAppend = QString::fromStdString(scenario->getName());
    toAppend.append("::Package::");
    toAppend.append(QString::fromStdString(scenario->getPackage()->getName()));

    settingsPackages.append(toAppend);
    settings.setValue("scenarios", settingsPackages);

    //Save opencount for favourites
    QMap<QString, int> favouriteCount;

    settings.beginGroup("favourites");
    QStringList keys = settings.childKeys();
    foreach (QString key, keys)
    {
        favouriteCount[key] = settings.value(key).toInt();
    }
    settings.endGroup();

    settings.beginGroup("favourites");
    QMap<QString, int>::const_iterator i = favouriteCount.constBegin();
    bool added = false;
    while (i != favouriteCount.constEnd())
    {
        if (i.key().compare(QString::fromStdString(scenario->getName() + "::Package::" + scenario->getPackage()->getName())) == 0)
        {
            settings.setValue(i.key(), i.value() + 1);
            added = true;
        }
        else
        {
            settings.setValue(i.key(), i.value());
        }
        ++i;
    }
    //value not in map yet
    if (!added)
    {
        settings.setValue(QString::fromStdString(scenario->getName() + "::Package::" + scenario->getPackage()->getName()), 1);
    }

    settings.endGroup();

    emit updated();
}

void OpenScenarioController::showOpenScenarioView()
{
    openScenarioView.exec();
}

void OpenScenarioController::on_showAddPackageDialog()
{
    emit showPackageDialog();
}

void OpenScenarioController::updateModel()
{
    treemodel.reset(new OpenScenarioModel());

    QStringList paths = getAllClosedScenarios();
    QStringList favourites = getFavouriteScenarios(paths);

    OpenScenarioItem* rootItem = treemodel->getRootItem();

    OpenScenarioItem* favItem = new OpenScenarioItem(QList<QVariant>({"Favourite Scenarios", "", "HIDE"}));
    OpenScenarioItem* allItem = new OpenScenarioItem(QList<QVariant>({"All Scenarios", "", "HIDE"}));

    for (int i = 0; i < favourites.size(); i++)
    {
        QStringList split = favourites.at(i).split("::Package::");
        OpenScenarioItem* scenarioItem = new OpenScenarioItem(QVariant(split.at(0)), QVariant(split.at(1)));
        favItem->appendChild(scenarioItem);
    }

    for (int i = 0; i < paths.size(); i++)
    {
        QStringList split = paths.at(i).split("::Package::");
        OpenScenarioItem* scenarioItem = new OpenScenarioItem(QVariant(split.at(0)), QVariant(split.at(1)));
        allItem->appendChild(scenarioItem);
    }

    rootItem->appendChild(favItem);
    rootItem->appendChild(allItem);

    //treemodel->update();
    model->setSourceModel(treemodel.get());
    openScenarioView.setModel(model);
}

QStringList OpenScenarioController::getFavouriteScenarios(QStringList allClosedScenarios)
{
    QSettings settings("KIT", "ScenarioManager");

    QMap<QString, int> favouriteCount;

    settings.beginGroup("favourites");
    QStringList keys = settings.childKeys();
    foreach (QString key, keys)
    {
        favouriteCount[key] = settings.value(key).toInt();
    }
    settings.endGroup();

    if (favouriteCount.size() == 0)
    {
        return QStringList();
    }

    QList<QPair<QString, int>> favouriteSort;

    for (const auto& key : favouriteCount.toStdMap())
    {
        favouriteSort.push_back(QPair<QString, int>(key.first, key.second));
    }

    std::sort(favouriteSort.begin(), favouriteSort.end(), [](const QPair<QString, int>& p1, const QPair<QString, int>& p2)
    {
        return p1.second > p2.second;
    });

    QStringList result;

    for (int i = 0; i < favouriteSort.count(); ++i)
    {
        //filter - only show not openend scenarios in favourites
        if (allClosedScenarios.contains(favouriteSort[i].first))
        {
            result.push_back(favouriteSort[i].first);
            if ((size_t)result.size() == favlistSize)
            {
                break;
            }
        }
    }

    return result;
}

QStringList OpenScenarioController::getAllClosedScenarios()
{
    QSettings settings("KIT", "ScenarioManager");
    auto packages = settings.value("packages").toStringList().toSet();
    auto openedScenarios = settings.value("scenarios").toStringList().toSet();

    QStringList result;
    for (const auto& package : packages)
        //    for (int i = 0; i < packages.size(); i++)
    {

        CMakePackageFinder parser = CMakePackageFinderCache::GlobalCache.findPackage(package.toStdString());
        XMLScenarioParser scenarioParser;
        std::vector<std::string> packageScenarios = scenarioParser.getScenariosFromFolder(parser.getScenariosDir());

        for (const auto& str : packageScenarios)
        {
            bool contains = false;
            for (const auto& opened : openedScenarios)
            {
                if (opened.compare("") == 0)
                {
                    continue;
                }
                QString name = opened.split("::Package::").at(0);
                QString p = opened.split("::Package::").at(1);
                if (name.compare(QString::fromStdString(str)) == 0 && p.compare(QString::fromStdString(package.toStdString())) == 0)
                {
                    contains = true;
                    break;
                }
            }
            if (!contains)
            {
                QString resultStr(QString::fromStdString(str));
                resultStr.append("::Package::");
                resultStr.append(package);
                result << resultStr;
            }
        }
    }

    return result;
}
