/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "SettingsController.h"

#include <ArmarXCore/util/ScenarioManagerCommon/executor/StopStrategyFactory.h>
#include <ArmarXCore/util/ScenarioManagerCommon/parser/iceparser.h>
#include <ArmarXCore/util/ScenarioManagerCommon/parser/StatusManager.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinderCache.h>

#include <QStringList>
#include <QMessageBox>
#include <QList>
#include <QVariant>
#include <QSettings>
#include <QSet>

using namespace ScenarioManager;
using namespace Controller;
using namespace Data_Structure;
using namespace Exec;
using namespace armarx;

SettingsController::SettingsController(PackageVectorPtr packages, ExecutorPtr executor, QObject* parent)
    : QObject(parent), treemodel(new SettingsModel()),
      model(new FilterableTreeModelSortFilterProxyModel()),
      packages(packages), executor(executor)
{
    model->setSourceModel(treemodel.get());
    settingsDialog.setModel(model);
    stopperFactory = StopperFactory::getFactory();
    starterFactory = StarterFactory::getFactory();

    using This = SettingsController;
    QObject::connect(&settingsDialog, SIGNAL(openPackageChooser()),
                     this, SLOT(showPackageAdderView()));

    QObject::connect(&settingsDialog, SIGNAL(removePackage(int, int, QModelIndex)),
                     this, SLOT(removePackage(int, int, QModelIndex)));

    QObject::connect(&packageAdderView, SIGNAL(created(std::string)),
                     this, SLOT(addPackage(std::string)));

    QObject::connect(&settingsDialog, SIGNAL(changeExecutorSettings(std::string, int, std::string)),
                     this, SLOT(setExecutorStopStrategy(std::string, int, std::string)));

    QObject::connect(this, SIGNAL(setExecutorState(int, int, int)),
                     &settingsDialog, SLOT(setExecutorState(int, int, int)));

    QObject::connect(&settingsDialog, SIGNAL(clearPidCache()),
                     this, SLOT(clearPidCache()));

    QObject::connect(&settingsDialog, SIGNAL(clearXmlCache()),
                     this, SLOT(clearXmlCache()));

    QObject::connect(&settingsDialog, SIGNAL(XmlCache()),
                     this, SLOT(clearXmlCache()));

    QObject::connect(&settingsDialog, &SettingsView::closeUnavailablePackages,
                     this, &This::closeUnavailablePackages);

    //updateModel();
}

SettingsController::~SettingsController()
{

}

void SettingsController::init()
{
    QSettings settings("KIT", "ScenarioManager");
    QStringList execSettings = settings.value("execSettings").toStringList();

    if (execSettings.size() < 3)
    {
        execSettings.clear();
        execSettings << "Stop & Kill" << "1000" << "By Id";
        settings.setValue("execSettings", execSettings);
        setExecutorStopStrategy(execSettings.at(0).toStdString(), execSettings.at(1).toInt(), execSettings.at(2).toStdString());
    }
    else
    {
        setExecutorStopStrategy(execSettings.at(0).toStdString(), execSettings.at(1).toInt(), execSettings.at(2).toStdString());
    }
}

void SettingsController::showSettings()
{

    QSettings settings("KIT", "ScenarioManager");
    QStringList execSettings = settings.value("execSettings").toStringList();

    if (execSettings.size() < 3)
    {
        execSettings.clear();
        execSettings << "Stop & Kill" << "1000" << "By Id";
        settings.setValue("execSettings", execSettings);
        setExecutorStopStrategy(execSettings.at(0).toStdString(), execSettings.at(1).toInt(), execSettings.at(2).toStdString());
    }
    else
    {
        setExecutorStopStrategy(execSettings.at(0).toStdString(), execSettings.at(1).toInt(), execSettings.at(2).toStdString());
    }

    settingsDialog.exec();
}

void SettingsController::showPackageAdderView()
{
    packageAdderView.exec();
}

void SettingsController::addPackage(std::string name)
{
    for (const auto& package : *packages)
    {
        if (package->getName().compare(name) == 0)
        {
            QMessageBox box;
            QString message(QString::fromStdString("Package " + name + " is already open"));
            box.setText(message);
            box.exec();
            return;
        }
    }

    QSettings settings("KIT", "ScenarioManager");
    QStringList packages = settings.value("packages").toStringList();
    packages.removeDuplicates();
    packages.append(QString::fromStdString(name));

    settings.setValue("packages", packages);

    emit packageAdded(name);
}

void SettingsController::removePackage(int row, int column, QModelIndex parent)
{
    //TODO finde index aus model heraus und loesche es aus qsettings
    //PackagePtr package = static_cast<SettingsItem*>(treemodel->getRootItem()->child(row))->getPackage();

    SettingsItem* settingsItem = model->data(model->index(row, column, parent), SETTINGSITEMSOURCE).value<SettingsItem*>();
    if (settingsItem == nullptr)
    {
        return;
    }

    PackagePtr package = settingsItem->getPackage();

    //Remove Package from QSettings
    QSettings settings("KIT", "ScenarioManager");
    QStringList settingsPackages = settings.value("packages").toStringList();
    settingsPackages.removeDuplicates();

    settingsPackages.removeAt(settingsPackages.indexOf(QString::fromStdString(package->getName())));

    settings.setValue("packages", settingsPackages);

    //Remove Package from Data Structure
    for (std::vector<PackagePtr>::iterator iter = packages->begin(); iter != packages->end(); ++iter)
    {
        if (*iter == package)
        {
            packages->erase(iter);
            break;
        }
    }

    //emit model updates
    emit packageRemoved();
}

void SettingsController::setExecutorStopStrategy(std::string killMethod, int delay, std::string stopMethod)
{
    StopStrategyPtr stopStrategy;
    ApplicationStopperPtr applicationStopper;
    int killIndex = 0;
    int stopStrategyIndex = 0;
    if (stopMethod.compare("By Id") == 0)
    {
        applicationStopper = stopperFactory->getPidStopper();
        killIndex = 0;
    }
    else if (stopMethod.compare("By Name") == 0)
    {
        applicationStopper = stopperFactory->getByNameStopper();
        killIndex = 1;
    }

    if (killMethod.compare("Stop only") == 0)
    {
        stopStrategy = stopStrategyFactory.getStopStrategy(applicationStopper);
        stopStrategyIndex = 1;
    }
    else if (killMethod.compare("Stop & Kill") == 0)
    {
        stopStrategy = stopStrategyFactory.getStopAndKillStrategy(applicationStopper, delay);
        stopStrategyIndex = 0;
    }

    if (stopStrategy.get() == nullptr)
    {
        ARMARX_INFO_S << "This should not happen";
        return;
    }
    executor->setDefaultStopStrategy(stopStrategy);

    QSettings settings("KIT", "ScenarioManager");
    QStringList execSettings = settings.value("execSettings").toStringList();
    execSettings.clear();

    execSettings << QString::fromStdString(killMethod) << QString::number(delay) << QString::fromStdString(stopMethod);
    settings.setValue("execSettings", execSettings);
    emit setExecutorState(killIndex, delay, stopStrategyIndex);
}

void SettingsController::updateModel()
{
    treemodel->clear();
    SettingsItem* rootItem = treemodel->getRootItem();
    for (const auto& package : *packages)
    {
        SettingsItem* packageItem = new SettingsItem(package);
        rootItem->appendChild(packageItem);
    }

    treemodel->update();
    model->setSourceModel(treemodel.get());
    settingsDialog.setModel(model);
}

void SettingsController::clearPidCache()
{
    StatusManager::clearCache();
}

void SettingsController::clearXmlCache()
{
    Parser::IceParser::clearXmlCacheDir();
}


void SettingsController::closeUnavailablePackages()
{
    QSettings settings("KIT", "ScenarioManager");
    QStringList packages = settings.value("packages").toStringList();
    packages.removeDuplicates();

    QStringList found;
    std::vector<std::string> foundStd;
    std::vector<std::string> unavailableStd;
    for (int i = 0; i < packages.size(); i++)
    {
        const QString name = packages.at(i);

        armarx::CMakePackageFinder pFinder =
                armarx::CMakePackageFinderCache::GlobalCache.findPackage(name.toStdString());
        if (pFinder.packageFound())
        {
            found.append(name);
            foundStd.push_back(name.toStdString());
        }
        else
        {
            unavailableStd.push_back(name.toStdString());
        }
    }

    ARMARX_INFO << "Removed " << unavailableStd.size() << " from open packages: \n" << unavailableStd
                << "\nFound available packages: \n" << foundStd;

    settings.setValue("packages", found);
}
