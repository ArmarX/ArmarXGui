/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <ArmarXCore/util/ScenarioManagerCommon/data_structure/ApplicationInstance.h>
#include <ArmarXCore/util/ScenarioManagerCommon/data_structure/Scenario.h>
#include <ArmarXCore/util/ScenarioManagerCommon/data_structure/Package.h>
#include <ArmarXCore/util/ScenarioManagerCommon/executor/Executor.h>
#include "../gui/detailedapplicationview.h"
#include "../gui/createpropertyview.h"
#include "../gui/scenarioitem.h"
#include "../gui/scenariolistview.h"

#include <ArmarXCore/core/IceManager.h>
#include <ArmarXCore/core/IceGridAdmin.h>
#include <QObject>
#include <memory>
#include <vector>

namespace ScenarioManager::Controller
{
    /**
    * @class DetailedApplicationController
    * @ingroup controller
    * @brief This Controller manages the signals and model of the DetailedApplicationView.
    * This controller gets notified by the ScenarioListController when a scenario or application
    * gets selected and displays it. It also starts and stops applications and scenarios using the Executor.
    */
    class DetailedApplicationController : public QObject
    {
        Q_OBJECT
    public:
        /**
        * Constructor that optionally sets the parent object.
        * @parent standard QT option to specify a parent
        */
        DetailedApplicationController(Exec::ExecutorPtr executor, QObject* parent = 0);

        /**
        * Destructor.
        */
        ~DetailedApplicationController() override;

        /**
        * Sets the view this controller manages.
        */
        void setDetailedApplicationView(DetailedApplicationView* ptr);

    public slots:
        /**
        * Shows an ApplicationInstance in the DetailedApplicationView.
        * @param application ApplicationInstance to show
        */
        void showApplicationInstance(Data_Structure::ApplicationInstancePtr application, ScenarioItem* item);

        /**
        * Shows an Application in the DetailedApplicationView.
        * @param application Application to show
        */
        void showApplication(Data_Structure::ApplicationPtr application);

        /**
        * Shows a Scenario in the DetailedApplicationView.
        * @param scenario Scenario to show
        */
        void showScenario(Data_Structure::ScenarioPtr scenario);

        /**
        * Shows a Package in the DetailedApplicationView.
        * @param package Package to show
        */
        void showPackage(Data_Structure::PackagePtr package);

        /**
        * Shows the configuration of the current ApplicationInstance in the standard editor.
        */
        void showInStandardEditor();

        /**
        * Sets a property of the current ApplicationInstance.
        * @param name key of the property
        * @param value new value of the property
        */
        void setProperty(std::string name, std::string value);
        void setIceEnvVariable(std::string name, std::string value);

        /**
        * Starts the current ApplicationInstance.
        */
        void start();

        /**
        * Stops the current ApplicationInstance.
        */
        void stop();

        /**
        * Saves the configuration of the current ApplicationInstance to the config file.
        */
        void save();

        /**
        * Deletes a property off the configuration file of the current ApplicationInstance.
        * @param name name of the property to be deleted
        */
        void deleteProperty(std::string name);

        void showPropertyAddView();
        void showIceEnvVariableAddView();


        /**
         * @brief Set an IceAdmin for the controller. Needet to start an application via ice
         * @param IceGrid::AdminPrx
         */
        void setIceAdmin(IceGrid::AdminPrx iceAdmin);
    private:
        void startScenario(ScenarioManager::Data_Structure::ScenarioPtr scenarioItem);
        void showWarningDialog(QString message);

    private:
        Exec::ExecutorPtr executor;

        bool showingStartable;
        Data_Structure::ApplicationInstancePtr currentApplication;
        Data_Structure::ScenarioPtr currentScenario;
        DetailedApplicationView* view;
        CreatePropertyView propertyAdderView;
        CreatePropertyView iceEnvVariableAddView;
        IceGrid::AdminPrx iceAdmin;
    };
}


