/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "packageadderview.h"
#include <ArmarXGui/gui-plugins/ScenarioManager/gui/ui_packageadderview.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <QMessageBox>

PackageAdderView::PackageAdderView(QWidget* parent) :
    QDialog(parent),
    ui(new Ui::PackageAdderView),
    found(false)
{
    ui->setupUi(this);
    ui->okButton->setDefault(true);
}

PackageAdderView::~PackageAdderView()
{
    delete ui;
}

void PackageAdderView::on_okButton_clicked()
{
    if (found)
    {
        emit created(ui->packageName->text().toStdString());
        accept();
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText("Please insert a valid package name");
        msgBox.exec();
    }
}

void PackageAdderView::on_cancelButton_clicked()
{
    reject();
}

void PackageAdderView::on_packageName_textChanged(const QString& text)
{
    armarx::CMakePackageFinder pFinder(text.toStdString());
    if (pFinder.packageFound())
    {
        ui->packagePath->setText(QString::fromStdString(pFinder.getConfigDir()));
        ui->foundPackageLabel->setPixmap(QPixmap(":/icons/user-online.svg"));
        found = true;
    }
    else
    {
        found = false;
        ui->packagePath->setText("Cannot find Package");
        ui->foundPackageLabel->setPixmap(QPixmap(":/icons/dialog-cancel-5.svg"));
    }
}

