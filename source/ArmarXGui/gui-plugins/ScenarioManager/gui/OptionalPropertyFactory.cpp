/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "OptionalPropertyFactory.h"
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <QComboBox>

using namespace armarx;

OptionalVariantFactory::~OptionalVariantFactory()
{
    QList<OptionalEdit*> editors = editorToProperty.keys();
    QListIterator<OptionalEdit*> it(editors);
    while (it.hasNext())
    {
        delete it.next();
    }
}

void OptionalVariantFactory::setElementName(const QString& name)
{
    elementName = name;
}

void OptionalVariantFactory::connectPropertyManager(QtVariantPropertyManager* manager)
{
    connect(manager, SIGNAL(valueChanged(QtProperty*, const QVariant&)),
            this, SLOT(slotPropertyChanged(QtProperty*, const QVariant&)));
    connect(manager, SIGNAL(attributeChanged(QtProperty*, const QString&, const QVariant&)),
            this, SLOT(slotPropertyAttributeChanged(QtProperty*, const QString&, const QVariant&)));
    QtVariantEditorFactory::connectPropertyManager(manager);
}

QWidget* OptionalVariantFactory::createEditor(QtVariantPropertyManager* manager,
        QtProperty* property, QWidget* parent)
{
    if (manager->propertyType(property) == OptionalVariantManager::optionalProprtyTypeId())
    {

        OptionalEdit* editor = new OptionalEdit(elementName, property->propertyName(), parent);
        auto varProp = dynamic_cast<QtVariantProperty*>(property);
        ARMARX_CHECK_EXPRESSION(varProp) << elementName.toStdString();
        if (varProp)
        {
            auto values = static_cast<OptionalVariantManager*>(manager)->attributeValue(varProp, QLatin1String("PossibleValues")).toStringList();

            editor->setPossibleValues(values);
        }
        editor->setValue(manager->value(property).toString());
        editor->setPropertyEnabled(static_cast<OptionalVariantManager*>(manager)->attributeValue(property, QLatin1String("enabled")).toBool());

        createdEditors[property].append(editor);
        editorToProperty[editor] = property;




        connect(editor, SIGNAL(valueChanged(QString)),
                this, SLOT(slotSetValue(const QString&)));
        connect(editor, SIGNAL(enabledChanged(bool)),
                this, SLOT(slotSetEnabled(const bool&)));
        connect(editor, SIGNAL(destroyed(QObject*)),
                this, SLOT(slotEditorDestroyed(QObject*)));
        return editor;
    }
    return QtVariantEditorFactory::createEditor(manager, property, parent);
}

void OptionalVariantFactory::disconnectPropertyManager(QtVariantPropertyManager* manager)
{
    disconnect(manager, SIGNAL(valueChanged(QtProperty*, const QVariant&)),
               this, SLOT(slotPropertyChanged(QtProperty*, const QVariant&)));
    disconnect(manager, SIGNAL(attributeChanged(QtProperty*, const QString&, const QVariant&)),
               this, SLOT(slotPropertyAttributeChanged(QtProperty*, const QString&, const QVariant&)));
    QtVariantEditorFactory::disconnectPropertyManager(manager);
}

void OptionalVariantFactory::slotPropertyChanged(QtProperty* property,
        const QVariant& value)
{
    if (!createdEditors.contains(property))
    {
        return;
    }

    QList<OptionalEdit*> editors = createdEditors[property];
    QListIterator<OptionalEdit*> itEditor(editors);
    while (itEditor.hasNext())
    {
        itEditor.next()->setValue(value.toString());
    }
}

void OptionalVariantFactory::slotPropertyAttributeChanged(QtProperty* property,
        const QString& attribute, const QVariant& value)
{
    if (!createdEditors.contains(property))
    {
        return;
    }

    if (attribute != QLatin1String("enabled"))
    {
        return;
    }

    QList<OptionalEdit*> editors = createdEditors[property];
    QListIterator<OptionalEdit*> itEditor(editors);
    while (itEditor.hasNext())
    {
        itEditor.next()->setPropertyEnabled(value.toBool());
    }
}

void OptionalVariantFactory::slotSetValue(const QString& value)
{
    QObject* object = sender();
    QMap<OptionalEdit*, QtProperty*>::ConstIterator itEditor =
        editorToProperty.constBegin();
    while (itEditor != editorToProperty.constEnd())
    {
        if (itEditor.key() == object)
        {
            QtProperty* property = itEditor.value();
            OptionalVariantManager* manager = static_cast<OptionalVariantManager*>(propertyManager(property));
            if (!manager)
            {
                return;
            }
            manager->setValue(property, value);
            return;
        }
        itEditor++;
    }
}

void OptionalVariantFactory::slotSetEnabled(const bool& value)
{
    QObject* object = sender();
    QMap<OptionalEdit*, QtProperty*>::ConstIterator itEditor =
        editorToProperty.constBegin();
    while (itEditor != editorToProperty.constEnd())
    {
        if (itEditor.key() == object)
        {
            QtProperty* property = itEditor.value();
            OptionalVariantManager* manager = static_cast<OptionalVariantManager*>(propertyManager(property));
            if (!manager)
            {
                return;
            }
            manager->setAttribute(property, QLatin1String("enabled"), value);
            return;
        }
        itEditor++;
    }
}

void OptionalVariantFactory::slotEditorDestroyed(QObject* object)
{
    QMap<OptionalEdit*, QtProperty*>::ConstIterator itEditor =
        editorToProperty.constBegin();
    while (itEditor != editorToProperty.constEnd())
    {
        if (itEditor.key() == object)
        {
            OptionalEdit* editor = itEditor.key();
            QtProperty* property = itEditor.value();
            editorToProperty.remove(editor);
            createdEditors[property].removeAll(editor);
            if (createdEditors[property].isEmpty())
            {
                createdEditors.remove(property);
            }
            return;
        }
        itEditor++;
    }
}
