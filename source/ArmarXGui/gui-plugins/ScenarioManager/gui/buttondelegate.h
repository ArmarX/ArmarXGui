/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include "scenariomodel.h"

#include <QStyledItemDelegate>
#include <QList>
#include <QFlags>
#include <QPixmap>
#include <map>

/**
* @class ButtonDelegate
* @brief Manages a button.
*/
class ButtonDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    /**
    * Constructor which doesn't do anything.
    */
    ButtonDelegate(QWidget* parent = 0);

    virtual QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const;
    virtual void setEditorData(QWidget* editor, const QModelIndex& index) const;
    virtual void setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const;

    /**
    * Sets button style and draws it.
    * @param painter Painter that does the drawing
    * @param option Describes the parameter used to draw the button
    * @param index Used to locate the button
    */
    void paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const override;

    /**
    * Calculates and returns the size of the button
    * @param option Used to get the height and width of the item
    * @param index Used to locate the button (not used currently)
    */
    QSize sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const override;


    int getScenarioIconSize() const;
    void setScenarioIconSize(int value);

    int getAppIconSize() const;
    void setAppIconSize(int value);

signals:
    void buttonClicked(int row, int column, QModelIndex parent);
    void comboBoxButtonClicked(int row, int column, QModelIndex parent, QString text);

protected:
    bool editorEvent(QEvent* event, QAbstractItemModel* model, const QStyleOptionViewItem& option, const QModelIndex& index) override;

private slots:
    void onComboBoxRelease(QString text, const QAbstractItemModel* model, const QString& scenarioName);

private:
    std::map<const QModelIndex, QFlags<QStyle::StateFlag>> buttonStates;
    QPixmap startPixmap;
    QPixmap stopPixmap;
    QPixmap restartPixmap;

    int scenarioIconSize = 24;
    int appIconSize = 16;
};


