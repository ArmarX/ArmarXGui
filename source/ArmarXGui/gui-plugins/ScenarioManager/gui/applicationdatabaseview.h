/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include "treemodel.h"
#include "filterabletreemodelsortfilterproxymodel.h"
#include "applicationdatabasemodel.h"

#include <QWidget>

#include <memory>
#include <mutex>

namespace Ui
{
    class ApplicationDatabaseView;
}

/**
* @class ApplicationDatabaseView
* @brief View containing the packages and their Applications. The user can select applications and drag them into another view.
*/
class ApplicationDatabaseView : public QWidget
{
    Q_OBJECT

public:
    /**
    * Constructor setting up the UI.
    * @param parent sets the parent of this view
    */
    explicit ApplicationDatabaseView(QWidget* parent = 0);
    ~ApplicationDatabaseView() override;

    std::mutex& getMutex();

signals:
    void testSignal();
    void openSettings();
    void itemClicked(const QModelIndex& index);

public slots:
    /**
    * Sets the model of this view.
    * @param model the underlying model
    */
    void setModel(FilterableTreeModelSortFilterProxyModelPtr model);

private slots:
    void on_lineEdit_textEdited(const QString& arg1);

    void on_treeView_clicked(const QModelIndex& index);

private:
    Ui::ApplicationDatabaseView* ui;
    FilterableTreeModelSortFilterProxyModelPtr model;

    std::mutex mutex;
};

