/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "scenariolistview.h"
#include <ArmarXGui/gui-plugins/ScenarioManager/gui/ui_scenariolistview.h>
#include "scenarioitem.h"
#include "treeitem.h"
#include "comboboxbutton.h"

#include <ArmarXCore/core/logging/Logging.h>

#include <QComboBox>

using namespace ScenarioManager;
using namespace Data_Structure;
using namespace armarx;


ScenarioListView::ScenarioListView(QWidget* parent) :
    QWidget(parent),
    ui(new Ui::ScenarioListView),
    startButtonDelegate(this),
    stopButtonDelegate(this),
    restartButtonDelegate(this),
    removeAction("Remove", &contextMenu)
{
    ui->setupUi(this);

    ui->treeView->setModel(model.get());
    ui->treeView->setSortingEnabled(true);
    ui->treeView->setItemDelegateForColumn(1, &startButtonDelegate);
    ui->treeView->setItemDelegateForColumn(2, &stopButtonDelegate);
    ui->treeView->setItemDelegateForColumn(3, &restartButtonDelegate);


    contextMenu.setParent(ui->treeView);
    ui->treeView->setContextMenuPolicy(Qt::CustomContextMenu);
    QObject::connect(ui->treeView, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(onCustomContextMenu(const QPoint&)));

    ui->treeView->addAction(&removeAction);

    ui->treeView->setDragEnabled(true);
    ui->treeView->setDefaultDropAction(Qt::CopyAction);
    ui->treeView->setAcceptDrops(true);
    ui->treeView->setDropIndicatorShown(true);

    QObject::connect(&removeAction, SIGNAL(triggered()),
                     this, SLOT(removeItemTriggered()));

    QObject::connect(&startButtonDelegate, SIGNAL(comboBoxButtonClicked(int, int, QModelIndex, QString)),
                     this, SLOT(startComboBoxClicked(int, int, QModelIndex, QString)));
    QObject::connect(&startButtonDelegate, SIGNAL(buttonClicked(int, int, QModelIndex)),
                     this, SLOT(startButtonClicked(int, int, QModelIndex)));


    QObject::connect(&stopButtonDelegate, SIGNAL(buttonClicked(int, int, QModelIndex)),
                     this, SLOT(stopButtonClicked(int, int, QModelIndex)));

    QObject::connect(&restartButtonDelegate, SIGNAL(buttonClicked(int, int, QModelIndex)),
                     this, SLOT(restartButtonClicked(int, int, QModelIndex)));
}

ScenarioListView::~ScenarioListView()
{
    delete ui;
}

void ScenarioListView::setModel(FilterableTreeModelSortFilterProxyModelPtr model)
{
    this->model = model;
    ui->treeView->setModel(model.get());
    QObject::connect(this->model.get(), SIGNAL(dataChanged(QModelIndex, QModelIndex)), this, SLOT(onModelDataChange(QModelIndex, QModelIndex)));

    ui->treeView->setColumnWidth(0, 200);
    ui->treeView->setColumnWidth(1, 90);
    ui->treeView->setColumnWidth(2, 50);
    ui->treeView->setColumnWidth(3, 60);
}


void ScenarioListView::on_searchBar_textEdited(const QString& text)
{
    model->setFilterRegExp(QRegExp(text, Qt::CaseInsensitive, QRegExp::FixedString));
    ui->treeView->expandAll();
}

void ScenarioListView::startComboBoxClicked(int row, int column, QModelIndex parent, QString text)
{
    emit startApplication(row, column, parent, LocalStart);
}

void ScenarioListView::startButtonClicked(int row, int column, QModelIndex parent)
{
    emit startApplication(row, column, parent, LocalStart);
}

void ScenarioListView::stopButtonClicked(int row, int column, QModelIndex parent)
{
    emit stopApplication(row, column, parent);
}

void ScenarioListView::restartButtonClicked(int row, int column, QModelIndex parent)
{
    emit restartApplication(row, column, parent);
}

void ScenarioListView::on_newButton_clicked()
{
    emit createScenario();
}

void ScenarioListView::on_openButton_clicked()
{
    emit showOpenDialog();
}

void ScenarioListView::updateScenarioIndices()
{
    for (int i = 0; i < this->model->rowCount(); ++i)
    {
        auto index = this->model->index(i, 1);
        ui->treeView->openPersistentEditor(index);
        //        auto w = ui->treeView->indexWidget(index);
        //        ComboBoxButton* cb = qobject_cast<ComboBoxButton*>(w);
        //        if (cb)
        //        {
        //            cb->setScenarioIndex(index);
        //        }
    }
}

void ScenarioListView::removeItemTriggered()
{
    QModelIndex index = ui->treeView->currentIndex();
    emit removeItem(index);
    updateScenarioIndices();

}

void ScenarioListView::on_treeView_clicked(const QModelIndex& index)
{
    emit itemClicked(index);
}

void ScenarioListView::onCustomContextMenu(const QPoint& point)
{
    QModelIndex index = ui->treeView->currentIndex();

    removeAction.setText("Remove " + index.data().toString());

    QList<QAction*> actions;
    actions.append(&removeAction);

    QMenu::exec(actions, ui->treeView->mapToGlobal(point));
}

void ScenarioListView::onModelDataChange(QModelIndex start, QModelIndex end)
{
    updateScenarioIndices();
}
