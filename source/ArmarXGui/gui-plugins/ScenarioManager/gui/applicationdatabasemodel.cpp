/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "applicationdatabasemodel.h"

#include <ArmarXCore/util/ScenarioManagerCommon/data_structure/ApplicationInstance.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <QColor>
#include <QMimeData>
#include <QtGlobal>
#include <QDataStream>

using namespace ScenarioManager;
using namespace Data_Structure;

ApplicationDatabaseModel::ApplicationDatabaseModel()
{
    rootItem = new ApplicationDatabaseItem(QString("Application"));
}

QVariant ApplicationDatabaseModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid())
    {
        return QVariant();
    }

    if (role == APPLICATIONITEMSOURCE)
    {
        return QVariant::fromValue(reinterpret_cast<ApplicationDatabaseItem*>(index.internalPointer()));
    }

    if (role != Qt::DisplayRole)
    {
        return QVariant();
    }

    ApplicationDatabaseItem* item = static_cast<ApplicationDatabaseItem*>(index.internalPointer());

    return item->data(index.column());
}


void ApplicationDatabaseModel::clear()
{
    delete rootItem;
    rootItem = new ApplicationDatabaseItem("Application");
    reset();
}

Qt::ItemFlags ApplicationDatabaseModel::flags(const QModelIndex& index) const
{
    if (!index.isValid())
    {
        return 0;
    }

    Qt::ItemFlags defaultFlags = TreeModel::flags(index);

    if (index.parent().isValid())
    {
        return Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled | defaultFlags;
    }
    else
    {
        return defaultFlags;
    }
}

Qt::DropActions ApplicationDatabaseModel::supportedDragActions() const
{
    return Qt::CopyAction;
}


QStringList ApplicationDatabaseModel::mimeTypes() const
{
    QStringList types;
    types << "application/pointer";
    return types;
}

QMimeData* ApplicationDatabaseModel::mimeData(const QModelIndexList& indexes) const
{
    QMimeData* mimeData = new QMimeData();
    QByteArray encodedData;

    QDataStream stream(&encodedData, QIODevice::WriteOnly);

    foreach (const QModelIndex& index, indexes)
    {
        if (index.isValid())
        {

            Data_Structure::ApplicationPtr item = static_cast<ApplicationDatabaseItem*>(index.internalPointer())->getApplication();

            stream <<  reinterpret_cast<quint64>(item.get());
            stream << QString(""); // instance name
        }
    }

    //mimeData->setData("application/pointer", QByteArray( (char*)item, sizeof(item) ));
    mimeData->setData("application/pointer", encodedData);
    return mimeData;
}

void ApplicationDatabaseModel::update()
{
    QModelIndex topLeft = index(0, 0);
    QModelIndex bottomRight = index(rowCount() - 1, columnCount() - 1);

    emit dataChanged(topLeft, bottomRight);
}

ApplicationDatabaseItem* ApplicationDatabaseModel::getRootItem()
{
    return static_cast<ApplicationDatabaseItem*>(rootItem);
}
