/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "namelocationview.h"
#include <ArmarXGui/gui-plugins/ScenarioManager/gui/ui_namelocationview.h>
#include <QFileDialog>

NameLocationView::NameLocationView(QWidget* parent) :
    QDialog(parent),
    ui(new Ui::NameLocationView)
{
    ui->setupUi(this);
    ui->okButton->setDefault(true);
}

NameLocationView::~NameLocationView()
{
    delete ui;
}

void NameLocationView::on_locationChooserButton_clicked()
{
    QDir directory(".");
    QString path = QFileDialog::getExistingDirectory(this, tr("Directory"), directory.path());
    if (path.isNull() == false)
    {
        this->path = path.toStdString();
    }
}

void NameLocationView::on_nameLineEdit_textChanged(const QString& text)
{
    name = text.toStdString();
}

void NameLocationView::on_okButton_clicked()
{
    if (name.length() != 0 && path.length() != 0)
    {
        emit created(name, path);
        accept();
    }
}

void NameLocationView::on_cancelButton_clicked()
{
    reject();
}

void NameLocationView::on_nameLineEdit_editingFinished()
{
}
