/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "OptionalPropertyManager.h"
#include "OptionalEdit.h"

class OptionalVariantFactory : public QtVariantEditorFactory
{
    Q_OBJECT
public:
    OptionalVariantFactory(QObject* parent = 0) : QtVariantEditorFactory(parent) { }
    ~OptionalVariantFactory() override;

    void setElementName(const QString& name);

protected:
    void connectPropertyManager(QtVariantPropertyManager* manager) override;
    QWidget* createEditor(QtVariantPropertyManager* manager, QtProperty* property,
                          QWidget* parent) override;
    void disconnectPropertyManager(QtVariantPropertyManager* manager) override;


private slots:
    void slotPropertyChanged(QtProperty* property, const QVariant& value);
    void slotPropertyAttributeChanged(QtProperty* property, const QString& attribute, const QVariant& value);
    void slotSetValue(const QString& value);
    void slotSetEnabled(const bool& value);
    void slotEditorDestroyed(QObject* object);
private:
    QString elementName;
    QMap<QtProperty*, QList<armarx::OptionalEdit*> > createdEditors;
    QMap<armarx::OptionalEdit*, QtProperty*> editorToProperty;
};


