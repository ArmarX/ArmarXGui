/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include "../gui/treemodel.h"
#include "openscenarioitem.h"
#include <memory>

/**
* @class OpenScenarioModel
* @brief Model used by the OpenScenarioView
*/
class OpenScenarioModel : public TreeModel
{
public:
    /**
    * Constructor that sets up the root of this model.
    */
    OpenScenarioModel();

    /**
    * Returns the data of the item at the given index of the given role.
    * @param index index of the item
    * @param role role of the item
    */
    QVariant data(const QModelIndex& index, int role) const override;

    /**
    * Deletes the current root and replaces it with a new one, as if this model was newly constructed.
    */
    void clear() override;

    /**
    * Updates this model.
    */
    void update();

    OpenScenarioItem* getRootItem();
};

using OpenScenarioModelPtr = std::shared_ptr<OpenScenarioModel>;


