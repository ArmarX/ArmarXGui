/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXGui/libraries/qtpropertybrowser/src/qtvariantproperty.h>
#include <QIcon>

class OptionalVariantManager : public QtVariantPropertyManager
{
    Q_OBJECT
public:
    OptionalVariantManager(QObject* parent = 0);

    QVariant value(const QtProperty* property) const override;
    int valueType(int propertyType) const override;
    bool isPropertyTypeSupported(int propertyType) const override;

    QStringList attributes(int propertyType) const override;
    int attributeType(int propertyType, const QString& attribute) const override;
    QVariant attributeValue(const QtProperty* property, const QString& attribute) const override;


    static int optionalProprtyTypeId();
public slots:
    void setValue(QtProperty* property, const QVariant& val) override;
    void setAttribute(QtProperty* property,
                      const QString& attribute, const QVariant& value) override;
protected:
    QString valueText(const QtProperty* property) const override;
    QIcon valueIcon(const QtProperty* property) const override;

    void initializeProperty(QtProperty* property) override;
    void uninitializeProperty(QtProperty* property) override;
private:
    struct Data
    {
        Data() {}
        QVariant variant;
    };
    QMap<const QtProperty*, QMap<QString, Data> > dataMap;

    QIcon checkedIcon;
    QIcon uncheckedIcon;
};



