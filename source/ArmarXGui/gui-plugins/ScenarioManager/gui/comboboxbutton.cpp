#include "comboboxbutton.h"
#include "scenariomodel.h"
#include <QMouseEvent>
#include <QStylePainter>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/util/ScenarioManagerCommon/parser/StatusManager.h>
#include <ArmarXGui/gui-plugins/ScenarioManager/gui/scenarioitem.h>


ComboBoxButton::ComboBoxButton(const QAbstractItemModel* model, const QString& scenarioName, QWidget* parent) :
    QComboBox(parent), model(model), buttonPress(false), scenarioName(scenarioName)
{
}

ComboBoxButton::~ComboBoxButton()
{
}


void ComboBoxButton::setIceStatesActive(bool active)
{
    if (active)
    {
        QObject::connect(this, SIGNAL(currentIndexChanged(QString)), this, SLOT(onIndexChange(QString)));
    }
    else
    {
        QObject::disconnect(this, SIGNAL(currentIndexChanged(QString)), this, SLOT(onIndexChange(QString)));
    }
}

void ComboBoxButton::paintEvent(QPaintEvent* e)
{
    if (buttonPress)
    {
        QStylePainter painter(this);
        QStyleOptionComboBox opt;
        opt.rect = e->rect();
        opt.currentIcon = itemIcon(currentIndex());
        opt.currentText = currentText();
        opt.iconSize = iconSize();
        opt.state |= QStyle::State_Sunken;
        painter.drawComplexControl(QStyle::CC_ComboBox, opt);
        painter.drawControl(QStyle::CE_ComboBoxLabel, opt);
    }
    else
    {
        QComboBox::paintEvent(e);
    }
}


void ComboBoxButton::mousePressEvent(QMouseEvent* e)
{
    if (e->x() < this->width() - 20)
    {
        buttonPress = true;
    }
    else
    {
        QComboBox::mousePressEvent(e);
    }
}

void ComboBoxButton::mouseReleaseEvent(QMouseEvent* e)
{
    if (e->x() < this->width() - 20)
    {
        emit released(this->currentText(), model, scenarioName);
    }
    else
    {
        QComboBox::mouseReleaseEvent(e);
    }
    buttonPress = false;
}

void ComboBoxButton::onIndexChange(const QString& text)
{
    ScenarioManager::StatusManager statusManager;

    ScenarioItem* item = ScenarioModel::FindScenario(model, scenarioName);
    ARMARX_CHECK_EXPRESSION(item) << scenarioName.toStdString();

    statusManager.setIceScenario(item->getScenario(), false);
}


