/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include "settingsmodel.h"
#include "filterabletreemodelsortfilterproxymodel.h"
#include "buttondelegate.h"
#include <QDialog>

namespace Ui
{
    class SettingsView;
}

class SettingsView : public QDialog
{
    Q_OBJECT

public:
    explicit SettingsView(QWidget* parent = 0);
    ~SettingsView() override;

    virtual void setModel(FilterableTreeModelSortFilterProxyModelPtr model);

signals:
    void openPackageChooser();
    void removePackage(int row, int column, QModelIndex parent);
    void changeExecutorSettings(std::string killMethod, int delay, std::string stopMethod);
    void clearPidCache();
    void clearXmlCache();
    void closeUnavailablePackages();

private slots:
    void on_lineEdit_textEdited(const QString& text);

    void removeButtonClicked(int row, int column, QModelIndex parent);

    void on_openButton_clicked();

    void on_applicationStopperChooser_currentIndexChanged(const QString& text);

    void on_stopMethodChooser_currentIndexChanged(const QString& text);

    void on_delayChooser_returnPressed();

    void on_delayChooser_editingFinished();

    void setExecutorState(int killIndex, int delay, int stopStrategyIndex);

    void on_clearPidCache_clicked();

    void on_clearXmlCache_clicked();

    void on_toolButton_clicked();

private:
    Ui::SettingsView* ui;
    FilterableTreeModelSortFilterProxyModelPtr model;
    ButtonDelegate deleteButtonDelegate;
};

