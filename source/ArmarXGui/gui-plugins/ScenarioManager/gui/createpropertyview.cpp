/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "createpropertyview.h"
#include <ArmarXGui/gui-plugins/ScenarioManager/gui/ui_createpropertyview.h>

#include <QMessageBox>

CreatePropertyView::CreatePropertyView(QWidget* parent) :
    QDialog(parent),
    ui(new Ui::CreatePropertyView)
{
    ui->setupUi(this);
    ui->okButton->setDefault(true);
}

CreatePropertyView::~CreatePropertyView()
{
    delete ui;
}

void CreatePropertyView::on_okButton_clicked()
{
    if (ui->nameLineEdit->text() != "")
    {
        emit create(ui->nameLineEdit->text().toStdString(), ui->valueLineEdit->text().toStdString());
        accept();
    }
    else
    {
        QMessageBox box;
        box.setText("Please set a name value");
        box.exec();
    }
}

void CreatePropertyView::on_cancelButton_clicked()
{
    reject();
}
