/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <QWidget>
#include <QLineEdit>
#include <QCheckBox>
#include <QHBoxLayout>
#include <QComboBox>

namespace armarx
{


    class CustomComboBox : public QComboBox
    {
        Q_OBJECT
    public:
        CustomComboBox(QWidget* parent) : QComboBox(parent)
        {

        }
    signals:
        void valueChanged(const QString& value);
        // QWidget interface
    protected:
        void focusInEvent(QFocusEvent* event) override
        {
            oldValue = currentText();
            QComboBox::focusInEvent(event);
        }
        void focusOutEvent(QFocusEvent* event) override;
        QString oldValue;
    };


    class OptionalEdit : public QWidget
    {
        Q_OBJECT
    public:
        explicit OptionalEdit(const QString& elementName, const QString& propertyName, QWidget* parent = 0);
        void setPossibleValues(const QStringList& values);
    signals:
        void enabledChanged(const bool& enabled);
        void valueChanged(const QString& value);

    public slots:
        void setPropertyEnabled(const bool& enabled = true);
        void setValue(const QString& value);
        void updateHistory(const QString& value);

    protected:
        void focusInEvent(QFocusEvent* e) override;
        void focusOutEvent(QFocusEvent* e) override;
        void keyPressEvent(QKeyEvent* e) override;
        void keyReleaseEvent(QKeyEvent* e) override;

    private slots:
        void slotEnabledChanged(int state);

    private:
        bool fixComboboxValues = false;
        QHBoxLayout* layout;
        //        QLineEdit* edit;
        QComboBox* combo;
        QCheckBox* checkbox;
        QString elementName;
        QString propertyName;
        QStringList valueList;
    };

}

