/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include <initializer_list>

#include "openscenariomodel.h"

OpenScenarioModel::OpenScenarioModel()
{
    rootItem = new OpenScenarioItem({"Scenarios", "Package", "Open"});
}

QVariant OpenScenarioModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid())
    {
        return QVariant();
    }

    //    if (role == Qt::BackgroundRole){
    //        if(index.column() == 1) {
    //            if(!index.data().toString().compare("Running")){
    //                return QColor(Qt::green);
    //            } else if(!index.data().toString().compare("Stopped")) {
    //                return QColor(Qt::red);
    //            } else if(!index.data().toString().compare("Waiting")) {
    //                return QColor(Qt::darkYellow);
    //            } else {
    //                return QColor(Qt::lightGray); //only for safety reasons
    //            }
    //        }
    //    } else if (role == Qt::DisplayRole) {
    //        if(index.column() == 2) {

    //        }
    //    } else if (role != Qt::DisplayRole){
    //        return QVariant();
    //    }
    if (role == OPENSCENARIOITEMSOURCE)
    {
        return QVariant::fromValue(reinterpret_cast<OpenScenarioItem*>(index.internalPointer()));
    }
    if (role != Qt::DisplayRole)
    {
        return QVariant();
    }

    OpenScenarioItem* item = static_cast<OpenScenarioItem*>(index.internalPointer());

    return item->data(index.column());
}


void OpenScenarioModel::clear()
{
    delete rootItem;
    rootItem = new OpenScenarioItem({"Scenarios", "Package", "Open"});
    reset();
}

void OpenScenarioModel::update()
{
    QModelIndex topLeft = index(0, 0);
    QModelIndex bottomRight = index(rowCount() - 1, columnCount() - 1);

    emit dataChanged(topLeft, bottomRight);
}


OpenScenarioItem* OpenScenarioModel::getRootItem()
{
    return static_cast<OpenScenarioItem*>(rootItem);
}
