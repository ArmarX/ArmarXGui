/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "OptionalEdit.h"

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/InfixCompleter.h>

#include <QFocusEvent>
#include <QAbstractItemView>
#include <QSettings>
#include <QApplication>

using namespace armarx;

OptionalEdit::OptionalEdit(const QString& elementName, const QString& propertyName, QWidget* parent) :
    QWidget(parent),
    elementName(elementName),
    propertyName(propertyName)
{
    layout = new QHBoxLayout;
    layout->setMargin(0);
    layout->setSpacing(0);

    QSettings settings("KIT", "ScenarioManager");
    valueList = settings.value(elementName + "/" + propertyName).toStringList();




    combo = new CustomComboBox(nullptr);
    combo->setEditable(true);
    combo->setAutoCompletion(true);
    combo->setAutoCompletionCaseSensitivity(Qt::CaseInsensitive);
    combo->addItems(valueList);

    checkbox = new QCheckBox(nullptr);
    checkbox->setToolTip("If checked, this property will be written into the config file");
    checkbox->setSizePolicy(QSizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred));

    setFocusPolicy(Qt::StrongFocus);
    setAttribute(Qt::WA_InputMethodEnabled);

    //    connect(edit, SIGNAL(textChanged(QString)), this, SIGNAL(valueChanged(QString)));
    connect(combo, SIGNAL(valueChanged(QString)), this, SIGNAL(valueChanged(QString)));
    connect(combo, SIGNAL(currentTextChanged(QString)), this, SLOT(setPropertyEnabled()));

    connect(combo, SIGNAL(valueChanged(QString)), this, SLOT(updateHistory(QString)));
    connect(checkbox, SIGNAL(stateChanged(int)), this, SLOT(slotEnabledChanged(int)));


    layout->addWidget(checkbox);
    layout->addWidget(combo);


    this->setLayout(layout);
}

void OptionalEdit::setPossibleValues(const QStringList& values)
{
    if (!values.empty())
    {
        fixComboboxValues = true;
        //        combo->setEditable(false);
        combo->clear();
        combo->addItems(values);
    }
}



void OptionalEdit::setPropertyEnabled(const bool& enabled)
{
    //    ARMARX_WARNING << "Property is enabled: " << enabled;
    auto changed = enabled != checkbox->isChecked();
    checkbox->setChecked(enabled);
    if (changed)
    {
        emit enabledChanged(checkbox->isChecked());
    }
}


void OptionalEdit::setValue(const QString& value)
{
    auto changed = value != combo->currentText();
    bool enabledState = checkbox->isChecked();
    //    ARMARX_INFO << VAROUT(value.toStdString()) << VAROUT(enabledState);
    combo->setCurrentText(value);

    if (checkbox->isChecked() != enabledState)
    {
        setPropertyEnabled(enabledState);
    }
    if (changed)
    {
        emit valueChanged(combo->currentText());
    }
}

void OptionalEdit::updateHistory(const QString& value)
{
    valueList.push_front(combo->currentText());
    valueList.removeDuplicates();
    if (!fixComboboxValues)
        //    ARMARX_INFO << "Adding " << combo->currentText().toStdString() << " to history";
    {
        combo->clear();
        combo->addItems(valueList);
    }


    QSettings settings("KIT", "ScenarioManager");
    settings.setValue(elementName + "/" + propertyName, QVariant(valueList));
}


void OptionalEdit::slotEnabledChanged(int value)
{
    emit enabledChanged(value != 0);
}

void OptionalEdit::focusInEvent(QFocusEvent* e)
{
    QWidget::focusInEvent(e);
}

void OptionalEdit::focusOutEvent(QFocusEvent* e)
{
    QWidget::focusOutEvent(e);
}

void OptionalEdit::keyPressEvent(QKeyEvent* e)
{

}

void OptionalEdit::keyReleaseEvent(QKeyEvent* e)
{

}


void CustomComboBox::focusOutEvent(QFocusEvent* event)
{
    //    ARMARX_INFO << "focus out - " << int(event->reason()) << " line focus: " << (lineEdit()->hasFocus() ? "true" : "false");

    if (event->reason() != Qt::FocusReason::PopupFocusReason)
    {
        //        ARMARX_INFO << "focus out - current text: " << currentText().toStdString() << " " << VAROUT(oldValue.toStdString());
        if (oldValue != currentText())
        {
            emit valueChanged(currentText());
        }
    }
    QComboBox::focusOutEvent(event);
}
