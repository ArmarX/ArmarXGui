/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include "scenarioitem.h"
#include "applicationdatabaseitem.h"
#include "openscenarioitem.h"
#include "settingsitem.h"
#include <QSortFilterProxyModel>
#include <QModelIndex>
#include <qmetatype.h>
#include <memory>

/**
* @class FilterableTreeModelSortFilterProxyModel
* @brief Model of the FilterableTreeView.
*/
class FilterableTreeModelSortFilterProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    /**
    * Constructor that registers the used meta-types.
    */
    FilterableTreeModelSortFilterProxyModel();

    /**
    * Returns the data stored at the specified location, based on the given role.
    * @param proxy_index Model-Index of the item
    * @param role describes what kind of data is requested
    */
    QVariant data(const QModelIndex& proxy_index, int role) const override;

    /**
    * Returns whether the filter accepts a row.
    * @param sourceRow index of the row
    * @param sourceParent index of the parent
    */
    bool filterAcceptsRow(int sourceRow, const QModelIndex& sourceParent) const override;
};

using FilterableTreeModelSortFilterProxyModelPtr = std::shared_ptr<FilterableTreeModelSortFilterProxyModel>;

