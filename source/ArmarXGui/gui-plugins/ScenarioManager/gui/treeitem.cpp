/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::gui
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "treeitem.h"

#include <QStringList>
#include <ArmarXCore/core/logging/Logging.h>
using namespace ScenarioManager;
using namespace Data_Structure;


TreeItem::TreeItem() : enabled(true), m_parentItem(nullptr)
{
}

TreeItem::~TreeItem()
{
    qDeleteAll(m_childItems);
}

void TreeItem::appendChild(TreeItem* item)
{
    item->m_parentItem = this;
    m_childItems.append(item);
}

TreeItem* TreeItem::child(int row)
{
    return m_childItems.value(row);
}

int TreeItem::childCount() const
{
    return m_childItems.count();
}

int TreeItem::columnCount() const
{
    return m_itemData.count();
}

QVariant TreeItem::data(int column) const
{
    return m_itemData.value(column);
}

TreeItem* TreeItem::parent()
{
    return m_parentItem;
}

int TreeItem::row() const
{
    if (m_parentItem)
    {
        return m_parentItem->m_childItems.indexOf(const_cast<TreeItem*>(this));
    }

    return 0;
}

void TreeItem::setEnabled(bool enabled)
{
    this->enabled = enabled;
}

bool TreeItem::isEnabled()
{
    return enabled;
}

bool TreeItem::setData(int column, const QVariant& value)
{
    if (column < 0 || column >= m_itemData.size())
    {
        return false;
    }

    m_itemData[column] = value;
    return true;
}

bool TreeItem::insertChild(int position, TreeItem* child)
{
    if (position < 0 || position > m_childItems.size())
    {
        return false;
    }

    child->m_parentItem = this;
    m_childItems.insert(position, child);

    return true;
}

bool TreeItem::removeChild(int position)
{
    if (position < 0 || position > m_childItems.size())
    {
        return false;
    }

    delete m_childItems.takeAt(position);

    return true;
}

bool TreeItem::insertColumn(int position, QVariant data)
{
    if (position < 0 || position > m_itemData.size())
    {
        return false;
    }

    m_itemData.insert(position, data);

    foreach (TreeItem* child, m_childItems)
    {
        child->insertColumn(position, QVariant());
    }

    return true;
}

bool TreeItem::removeColumn(int position)
{
    if (position < 0 || position > m_itemData.size())
    {
        return false;
    }

    m_itemData.remove(position);

    foreach (TreeItem* child, m_childItems)
    {
        child->removeColumn(position);
    }

    return true;
}
