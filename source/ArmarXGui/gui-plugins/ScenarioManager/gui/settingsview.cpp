/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "settingsview.h"
#include <ArmarXGui/gui-plugins/ScenarioManager/gui/ui_settingsview.h>

#include <ArmarXCore/util/ScenarioManagerCommon/generator/IceGridXmlGenerator.h>

#include <ArmarXCore/core/logging/Logging.h>
#include <QIntValidator>
#include <QString>
#include <QFileDialog>

SettingsView::SettingsView(QWidget* parent) :
    QDialog(parent),
    ui(new Ui::SettingsView)
    //deleteButtonDelegate(this)
{
    ui->setupUi(this);

    ui->treeView->setModel(model.get());
    ui->treeView->setSortingEnabled(true);

    ui->treeView->setItemDelegateForColumn(2, &deleteButtonDelegate);


    ui->delayChooser->setValidator(new QIntValidator());

    using This = SettingsView;
    QObject::connect(&deleteButtonDelegate, SIGNAL(buttonClicked(int, int, QModelIndex)),
                     this, SLOT(removeButtonClicked(int, int, QModelIndex)));

    QObject::connect(ui->closeUnavailablePackagesButton, &QPushButton::pressed,
                     this, &This::closeUnavailablePackages);

    ScenarioManager::Generator::IceGridXmlGenerator generator;
    ui->currentPath->setText(QString::fromStdString(generator.getDefaultSyncFile()));
}

SettingsView::~SettingsView()
{
    delete ui;
}

void SettingsView::setModel(FilterableTreeModelSortFilterProxyModelPtr model)
{
    this->model = model;
    ui->treeView->setModel(model.get());
    ui->treeView->setColumnWidth(0, 220);
    ui->treeView->setColumnWidth(1, 220);
}

void SettingsView::on_lineEdit_textEdited(const QString& text)
{
    model->setFilterRegExp(QRegExp(text, Qt::CaseInsensitive, QRegExp::FixedString));
    ui->treeView->expandAll();
}

void SettingsView::removeButtonClicked(int row, int column, QModelIndex parent)
{
    emit removePackage(row, column, parent);
}

void SettingsView::on_openButton_clicked()
{
    emit openPackageChooser();
}

void SettingsView::on_applicationStopperChooser_currentIndexChanged(const QString& text)
{
    if (!text.compare("Stop only"))
    {
        ui->delayChooser->setEnabled(false);
        ui->delayChooser->setText("1000");
    }
    else
    {
        ui->delayChooser->setEnabled(true);
    }

    emit changeExecutorSettings(ui->applicationStopperChooser->itemText(ui->applicationStopperChooser->currentIndex()).toStdString(),
                                ui->delayChooser->text().toInt(),
                                ui->stopMethodChooser->itemText(ui->stopMethodChooser->currentIndex()).toStdString());

    //TODO erstelle neue stopstrategy und emitte das event
}

void SettingsView::on_delayChooser_returnPressed()
{
    emit changeExecutorSettings(ui->applicationStopperChooser->itemText(ui->applicationStopperChooser->currentIndex()).toStdString(),
                                ui->delayChooser->text().toInt(),
                                ui->stopMethodChooser->itemText(ui->stopMethodChooser->currentIndex()).toStdString());
}

void SettingsView::on_delayChooser_editingFinished()
{
    emit changeExecutorSettings(ui->applicationStopperChooser->itemText(ui->applicationStopperChooser->currentIndex()).toStdString(),
                                ui->delayChooser->text().toInt(),
                                ui->stopMethodChooser->itemText(ui->stopMethodChooser->currentIndex()).toStdString());
}

void SettingsView::on_stopMethodChooser_currentIndexChanged(const QString& text)
{
    emit changeExecutorSettings(ui->applicationStopperChooser->itemText(ui->applicationStopperChooser->currentIndex()).toStdString(),
                                ui->delayChooser->text().toInt(),
                                ui->stopMethodChooser->itemText(ui->stopMethodChooser->currentIndex()).toStdString());
}

void SettingsView::setExecutorState(int killIndex, int delay, int stopStrategyIndex)
{
    ui->applicationStopperChooser->blockSignals(true);
    ui->stopMethodChooser->blockSignals(true);
    ui->applicationStopperChooser->setCurrentIndex(stopStrategyIndex);
    if (stopStrategyIndex == 1)
    {
        ui->delayChooser->setEnabled(false);
    }
    else
    {
        ui->delayChooser->setEnabled(true);
    }
    ui->delayChooser->setText(QString::number(delay));
    ui->stopMethodChooser->setCurrentIndex(killIndex);
    ui->applicationStopperChooser->blockSignals(false);
    ui->stopMethodChooser->blockSignals(false);
}

void SettingsView::on_clearPidCache_clicked()
{
    emit clearPidCache();
}

void SettingsView::on_clearXmlCache_clicked()
{
    emit clearXmlCache();
}

void SettingsView::on_toolButton_clicked()
{
    ScenarioManager::Generator::IceGridXmlGenerator generator;

    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                  QString::fromStdString(generator.getDefaultSyncFile()),
                  QFileDialog::ShowDirsOnly
                  | QFileDialog::DontResolveSymlinks);

    ARMARX_INFO_S << dir.toStdString();

    generator.setDefaultSyncFileDir(dir.toStdString());

    ui->currentPath->setText(QString::fromStdString(generator.getDefaultSyncFile()));
}
