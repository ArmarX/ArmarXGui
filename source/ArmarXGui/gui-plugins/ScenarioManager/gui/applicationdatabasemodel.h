/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include "treemodel.h"
#include "applicationdatabaseitem.h"
#include <memory>
#include <QStringList>

/**
* @class ApplicationDatabaseModel
* @brief Model underlying the ApplicationDatabaseView.
*/
class ApplicationDatabaseModel : public TreeModel
{
public:
    /**
    * Constructor that sets up the first item contained by this model.
    */
    ApplicationDatabaseModel();

    /**
    * Returns the data at the specified index, depending on the specified role.
    * @param index of the data
    * @param role role of the data
    * @return contained data
    */
    QVariant data(const QModelIndex& index, int role) const override;

    /**
    * Returns the behavioural flags of the object at the specified index.
    * @param index index of the object
    * @return behavioural flags
    */
    Qt::ItemFlags flags(const QModelIndex& index) const override;

    /**
    * Returns the supported drag action. In this case always Qt::CopyAction.
    */
    Qt::DropActions supportedDragActions() const override;

    /**
    * Returns supported MIME-types.
    * @return list of the supported MIME-types
    */
    QStringList mimeTypes() const override;

    /**
    * Returns the mimedata of the objects at the specified indexes.
    * @param indexes object indexes
    * @return data about the mime type of the objects
    */
    QMimeData* mimeData(const QModelIndexList& indexes) const override;

    /**
    * Updates the model.
    */
    void update();

    /**
    * Clears and resets this model as if it was newly constructed.
    */
    void clear() override;

    ApplicationDatabaseItem* getRootItem();
};

using ApplicationDatabaseModelPtr = std::shared_ptr<ApplicationDatabaseModel>;

