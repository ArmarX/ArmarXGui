/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include "../gui/treemodel.h"
#include "scenarioitem.h"
#include <ArmarXCore/util/ScenarioManagerCommon/data_structure/ApplicationInstance.h>
#include <memory>

/**
* @class ScenarioModel
* @brief Model defining how a Scenario gets displayed in the TreeView.
*/
class ScenarioModel : public TreeModel
{
    Q_OBJECT
public:
    /**
    * Sets up the model.
    */
    ScenarioModel();

    /**
    * Returns the data of the item at the given index based on the given role.
    * @param index index of the item
    * @param role role of the item
    */
    QVariant data(const QModelIndex& index, int role) const override;

    /**
    * Returns the drop-Actions supported by this model.
    * @return supported drop-actions
    */
    Qt::DropActions supportedDropActions() const override;

    /**
    * Returns the item flags of the item at the given index.
    * @param index item-index
    * @return item flags
    */
    Qt::ItemFlags flags(const QModelIndex& index) const override;

    /**
    *
    * @param data
    * @param action
    * @param row
    * @param column
    * @param parent
    */
    bool dropMimeData(const QMimeData* data, Qt::DropAction action,
                      int row, int column, const QModelIndex& parent) override;

    /**
    * Updates this model.
    */
    void update();

    /**
    *
    */
    QStringList mimeTypes() const override;

    /**
    * Clears and resets this model.
    */
    void clear() override;

    ScenarioItem* getRootItem();

    static ScenarioItem* FindScenario(const QAbstractItemModel* model, const QString& scenarioName);
    static int FindScenarioIndex(const QAbstractItemModel* model, const QString& scenarioName);

private:
    void treeUpdate(ScenarioItem* item);

signals:
    void applicationsDrop(QList<QPair<QString, ScenarioManager::Data_Structure::Application*>> applications,
                          int row, const QModelIndex& parent);
};

using ScenarioModelPtr = std::shared_ptr<ScenarioModel>;

