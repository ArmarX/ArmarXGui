/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "createscenarioview.h"
#include <ArmarXGui/gui-plugins/ScenarioManager/gui/ui_createscenarioview.h>

#include <QMessageBox>

CreateScenarioView::CreateScenarioView(QWidget* parent) :
    QDialog(parent),
    ui(new Ui::CreateScenarioView)
{
    ui->setupUi(this);
    ui->okButton->setDefault(true);
}

CreateScenarioView::~CreateScenarioView()
{
    delete ui;
}

void CreateScenarioView::setPackages(const QVector<QPair<QString, bool> >& packages)
{
    ui->packageBox->clear();
    int i = 0;
    for (QPair<QString, bool> const& pair : packages)
    {
        ui->packageBox->addItem(pair.first);
        if (!pair.second)
        {
            ui->packageBox->setItemText(i, pair.first + " (read-only)");
            // Get the index of the value to disable
            QModelIndex index = ui->packageBox->model()->index(i, 0);

            // This is the effective 'disable' flag
            QVariant v(0);

            // the magic
            ui->packageBox->model()->setData(index, v, Qt::UserRole - 1);
        }
        i++;
    }
}

void CreateScenarioView::on_okButton_clicked()
{
    if (ui->lineEdit->text() != "")
    {
        emit created(ui->lineEdit->text().toStdString(), ui->packageBox->itemText(ui->packageBox->currentIndex()).toStdString());
        accept();
    }
    else
    {
        QMessageBox messageBox;
        messageBox.setText("Please enter an Scenario name");
        messageBox.exec();
    }
}

void CreateScenarioView::on_cancelButton_clicked()
{
    reject();
}
