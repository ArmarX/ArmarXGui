/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "buttondelegate.h"
#include "filterabletreemodelsortfilterproxymodel.h"

#include "treeitem.h"
#include "comboboxbutton.h"
#include "scenarioitem.h"
#include <ArmarXCore/util/ScenarioManagerCommon/parser/StatusManager.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <QPainter>
#include <QStyleOptionViewItem>
#include <QModelIndex>
#include <QObject>
#include <QApplication>
#include <QMouseEvent>



ButtonDelegate::ButtonDelegate(QWidget* parent) : QStyledItemDelegate(parent),
    startPixmap(":/icons/media-playback-start.ico"),
    stopPixmap(":/icons/process-stop-7.ico"),
    restartPixmap(":/icons/view-refresh-7.png")
{
}

QWidget* ButtonDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    // Create the combobox and populate it
    auto model = index.model();
    ARMARX_CHECK_EXPRESSION(model);
    ComboBoxButton* cb = new ComboBoxButton(model, index.model()->index(index.row(), 0).data().toString(), parent);
    cb->setFrame(true);
    int size = appIconSize;
    //int row = index.row();
    if (!index.parent().isValid())
    {
        size = scenarioIconSize;
    }
    if (index.data().toString().compare("Start") == 0)
    {
        cb->addItem(startPixmap, "Local");

        // ICE START SUCKS HARD!!!!!!!!
        // IceStartQuickFix: TODO: This is just a very dirty fix. Either completely remove the Ice Start feature or fix it.
        //cb->addItem(startPixmap, "Ice");

        //ScenarioManager::StatusManager statusManager;
        //ScenarioItem* item = index.model()->data(index, SCENARIOITEMSOURCE).value<ScenarioItem*>();
        //if (statusManager.isIceScenario(item->getScenario()))
        //{
        //    cb->setCurrentIndex(1);
        //}
        cb->setIconSize(QSize(size, size));
        cb->setToolTip("Start");
        cb->setIceStatesActive(true);
    }
    else if (index.data().toString().compare("Stop") == 0)
    {
        cb->addItem(stopPixmap, "Stop");
        cb->setIconSize(QSize(size, size));
        cb->setToolTip("Stop");
    }
    else if (index.data().toString().compare("Restart") == 0)
    {
        cb->addItem(restartPixmap, "Restart");
        cb->setIconSize(QSize(size, size));
        cb->setToolTip("Restart");
    }
    else
    {
        cb->addItem(index.data().toString());
    }

    if (index.data().toString().compare("HIDE") == 0)
    {
        const_cast<ButtonDelegate*>(this)->buttonStates[index] = 0;
        cb->setVisible(false);
    }

    QObject::connect(cb, SIGNAL(released(QString, const QAbstractItemModel*, QString)), this,
                     SLOT(onComboBoxRelease(QString, const QAbstractItemModel*, QString)));

    return cb;
}

void ButtonDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const
{
    if (QComboBox* cb = qobject_cast<QComboBox*>(editor))
    {
        // get the index of the text in the combobox that matches the current value of the item
        QString currentText = index.data(Qt::EditRole).toString();
        int cbIndex = cb->findText(currentText);
        // if it is valid, adjust the combobox
        if (cbIndex >= 0)
        {
            cb->setCurrentIndex(cbIndex);
        }
    }
    else
    {
        QStyledItemDelegate::setEditorData(editor, index);
    }
}

void ButtonDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const
{
    if (ComboBoxButton* cb = qobject_cast<ComboBoxButton*>(editor))
    {
        model->setData(index, cb->currentText(), Qt::EditRole);
        //        cb->setScenarioIndex(index);
    }
    else
    {
        QStyledItemDelegate::setModelData(editor, model, index);
    }
}

void ButtonDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    QStyleOptionButton button;

    button.rect = option.rect;

    button.features = QStyleOptionButton::AutoDefaultButton;

    int size = appIconSize;
    if (!index.parent().isValid())
    {
        size = scenarioIconSize;
    }
    if (index.data().toString().compare("Start") == 0)
    {
        button.icon = startPixmap;

        button.iconSize = QSize(size, size);
    }
    else if (index.data().toString().compare("Stop") == 0)
    {
        button.icon = stopPixmap;
        button.iconSize = QSize(size, size);
    }
    else if (index.data().toString().compare("Restart") == 0)
    {
        button.icon = restartPixmap;
        button.iconSize = QSize(size, size);
    }
    else
    {
        button.text = index.data().toString();
    }

    if (index.data().toString().compare("HIDE") == 0)
    {
        const_cast<ButtonDelegate*>(this)->buttonStates[index] = 0;
        return;
    }

    if (const_cast<ButtonDelegate*>(this)->buttonStates[index] != 0)
    {
        button.state = const_cast<ButtonDelegate*>(this)->buttonStates[index];
    }
    else
    {
        const_cast<ButtonDelegate*>(this)->buttonStates[index] = QStyle::State_Raised | QStyle::State_Enabled;
        button.state = const_cast<ButtonDelegate*>(this)->buttonStates[index];
    }

    QApplication::style()->drawControl(QStyle::CE_PushButton, &button, painter, 0);

    //Draw 'Combobox' arrow if it is an Scenario Button
    //    if (!index.parent().isValid())
    //    {
    //        QStyleOptionHeader arrow;
    //        arrow.iconAlignment = Qt::AlignRight;
    //        arrow.rect = option.rect;
    //        arrow.rect.setLeft(arrow.rect.left() + arrow.rect.width() * 0.7);
    //        arrow.state = QStyle::State_DownArrow;
    //        QApplication::style()->drawPrimitive(QStyle::PE_IndicatorHeaderArrow, &arrow, painter);
    //    }
}

bool ButtonDelegate::editorEvent(QEvent* event, QAbstractItemModel* model, const QStyleOptionViewItem& option, const QModelIndex& index)
{
    //button should be hidden
    if ((buttonStates[index] & QStyle::State_Enabled) == 0)
    {
        return false;
    }

    if (event->type() == QEvent::MouseButtonPress)
    {
        buttonStates[index] = QStyle::State_Sunken | QStyle::State_Enabled;

        return true;
    }
    else if (event->type() == QEvent::MouseButtonRelease)
    {
        buttonStates[index] = QStyle::State_Raised | QStyle::State_Enabled;
        ARMARX_DEBUG << index.data().toString().toStdString() << ": editor event " << index.row() << " " << index.column();
        emit buttonClicked(index.row(), index.column(), index.parent());

        return true;
    }
    return false;
}

int ButtonDelegate::getAppIconSize() const
{
    return appIconSize;
}

void ButtonDelegate::setAppIconSize(int value)
{
    appIconSize = value;
}

int ButtonDelegate::getScenarioIconSize() const
{
    return scenarioIconSize;
}

void ButtonDelegate::setScenarioIconSize(int value)
{
    scenarioIconSize = value;
}

QSize ButtonDelegate::sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    int size = appIconSize;
    if (!index.parent().isValid())
    {
        size = scenarioIconSize;
    }
    return QSize(option.rect.width(), option.rect.height() + size + 6);
    //QItemDelegate::sizeHint(option,index);
}

void ButtonDelegate::onComboBoxRelease(QString text, const QAbstractItemModel* model, const QString& scenarioName)
{
    // find current row of scenario (might have changed due to reordering or insertion)
    int currentScenarioIndex = ScenarioModel::FindScenarioIndex(model, scenarioName);
    ARMARX_CHECK_GREATER(currentScenarioIndex, -1);
    auto index = model->index(currentScenarioIndex, 1);
    emit comboBoxButtonClicked(currentScenarioIndex, index.column(), index.parent(), text);
}
