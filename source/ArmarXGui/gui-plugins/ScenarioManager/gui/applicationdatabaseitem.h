/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include "treeitem.h"
#include <ArmarXCore/util/ScenarioManagerCommon/data_structure/Package.h>
/**
* @class ApplicationDatabaseItem
* @brief This class represents an item in the ApplicationDatabaseView.
*/
class ApplicationDatabaseItem : public TreeItem
{
public:
    /**
    * Constructor that only sets the text to be displayed.
    */
    ApplicationDatabaseItem(QString text);

    /**
    * Constructor that sets a package to be displayed.
    * @param package package to be displayed
    */
    ApplicationDatabaseItem(ScenarioManager::Data_Structure::PackagePtr package);

    /**
    * Constructor that sets an application to be displayed.
    * @param application application to be displayed
    */
    ApplicationDatabaseItem(ScenarioManager::Data_Structure::ApplicationPtr application);

    /**
    * Returns the Package displayed in this item.
    */
    ScenarioManager::Data_Structure::PackagePtr getPackage();

    /**
    * Returns the Application displayed in this item.
    */
    ScenarioManager::Data_Structure::ApplicationPtr getApplication();

private:
    ScenarioManager::Data_Structure::PackagePtr package;
    ScenarioManager::Data_Structure::ApplicationPtr application;
};

Q_DECLARE_METATYPE(ApplicationDatabaseItem*)
