#ifndef COMBOBOXBUTTON_H
#define COMBOBOXBUTTON_H


#include <QComboBox>

class ComboBoxButton : public QComboBox
{
    Q_OBJECT

public:
    explicit ComboBoxButton(const QAbstractItemModel* model, const QString& scenarioName, QWidget* parent = 0);
    ~ComboBoxButton();
    void setIceStatesActive(bool active);

signals:
    void released(QString, const QAbstractItemModel*, QString);


protected:
    virtual void mouseReleaseEvent(QMouseEvent* e);
    virtual void mousePressEvent(QMouseEvent* e);
    virtual void paintEvent(QPaintEvent* e);

private slots:
    void onIndexChange(const QString& text);


private:
    const QAbstractItemModel* model;
    bool buttonPress;
    QString scenarioName;
};

#endif // COMBOBOXBUTTON_H
