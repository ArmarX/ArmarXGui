/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "OptionalPropertyManager.h"

#include <ArmarXGui/libraries/qtpropertybrowser/src/qtpropertymanager.h>
#include <QStyleOptionButton>
#include <QApplication>
#include <QPainter>

class OptionalPropertyType
{
};
Q_DECLARE_METATYPE(OptionalPropertyType)

static QIcon drawCheckBox(bool value)
{
    QStyleOptionButton opt;
    opt.state |= value ? QStyle::State_On : QStyle::State_Off;
    opt.state |= QStyle::State_Enabled;
    const QStyle* style = QApplication::style();
    // Figure out size of an indicator and make sure it is not scaled down in a list view item
    // by making the pixmap as big as a list view icon and centering the indicator in it.
    // (if it is smaller, it can't be helped)
    const int indicatorWidth = style->pixelMetric(QStyle::PM_IndicatorWidth, &opt);
    const int indicatorHeight = style->pixelMetric(QStyle::PM_IndicatorHeight, &opt);
    const int listViewIconSize = indicatorWidth;
    const int pixmapWidth = indicatorWidth;
    const int pixmapHeight = qMax(indicatorHeight, listViewIconSize);

    opt.rect = QRect(0, 0, indicatorWidth, indicatorHeight);
    QPixmap pixmap = QPixmap(pixmapWidth, pixmapHeight);
    pixmap.fill(Qt::transparent);
    {
        // Center?
        const int xoff = (pixmapWidth  > indicatorWidth)  ? (pixmapWidth  - indicatorWidth)  / 2 : 0;
        const int yoff = (pixmapHeight > indicatorHeight) ? (pixmapHeight - indicatorHeight) / 2 : 0;
        QPainter painter(&pixmap);
        painter.translate(xoff, yoff);
        style->drawPrimitive(QStyle::PE_IndicatorCheckBox, &opt, &painter);
    }
    return QIcon(pixmap);
}


OptionalVariantManager::OptionalVariantManager(QObject* parent)
    : QtVariantPropertyManager(parent), checkedIcon(drawCheckBox(true)), uncheckedIcon(drawCheckBox(false))
{

}

int OptionalVariantManager::optionalProprtyTypeId()
{
    return qMetaTypeId<OptionalPropertyType>();
}

bool OptionalVariantManager::isPropertyTypeSupported(int propertyType) const
{
    if (propertyType == optionalProprtyTypeId())
    {
        return true;
    }
    return QtVariantPropertyManager::isPropertyTypeSupported(propertyType);
}

int OptionalVariantManager::valueType(int propertyType) const
{
    if (propertyType == optionalProprtyTypeId())
    {
        return QVariant::String;
    }
    return QtVariantPropertyManager::valueType(propertyType);
}

QVariant OptionalVariantManager::value(const QtProperty* property) const
{
    if (dataMap.contains(property))
    {
        return dataMap[property]["value"].variant;
    }
    return QtVariantPropertyManager::value(property);
}

QStringList OptionalVariantManager::attributes(int propertyType) const
{
    if (propertyType == optionalProprtyTypeId())
    {
        QStringList attr;
        attr << QLatin1String("enabled");
        return attr;
    }
    return QtVariantPropertyManager::attributes(propertyType);
}

int OptionalVariantManager::attributeType(int propertyType, const QString& attribute) const
{
    if (propertyType == optionalProprtyTypeId())
    {
        if (attribute == QLatin1String("enabled"))
        {
            return QVariant::Bool;
        }
        return 0;
    }
    return QtVariantPropertyManager::attributeType(propertyType, attribute);
}

QVariant OptionalVariantManager::attributeValue(const QtProperty* property, const QString& attribute) const
{
    if (dataMap.contains(property))
    {
        return dataMap[property][attribute].variant;
    }
    return QtVariantPropertyManager::attributeValue(property, attribute);
}

QString OptionalVariantManager::valueText(const QtProperty* property) const
{
    if (dataMap.contains(property))
    {
        return dataMap[property]["value"].variant.toString();
    }
    return QtVariantPropertyManager::valueText(property);
}

QIcon OptionalVariantManager::valueIcon(const QtProperty* property) const
{
    if (dataMap.contains(property))
    {
        auto it = dataMap.constFind(property);

        if (it == dataMap.constEnd())
        {
            return QIcon();
        }

        return (it.value()["enabled"].variant.isValid() && it.value()["enabled"].variant.toBool()) ? checkedIcon : uncheckedIcon;
    }
    return QtVariantPropertyManager::valueIcon(property);
}

void OptionalVariantManager::setValue(QtProperty* property, const QVariant& val)
{
    if (dataMap.contains(property))
    {
        if (val.type() != QVariant::String && !val.canConvert(QVariant::String))
        {
            return;
        }
        QString str = val.value<QString>();
        Data d = dataMap[property]["value"];
        if (d.variant.isValid() && d.variant.toString() == str)
        {
            return;
        }
        d.variant = str;
        dataMap[property]["value"] = d;
        emit propertyChanged(property);
        emit valueChanged(property, str);
        return;
    }
    QtVariantPropertyManager::setValue(property, val);
}

void OptionalVariantManager::setAttribute(QtProperty* property,
        const QString& attribute, const QVariant& val)
{
    if (dataMap.contains(property))
    {

        // special legacy case
        if (attribute == QLatin1String("enabled"))
        {
            if (val.type() != QVariant::Bool && !val.canConvert(QVariant::Bool))
            {
                return;
            }
            bool enabled = val.value<bool>();
            Data d = dataMap[property]["enabled"];
            if (d.variant.isValid() && d.variant.toBool() == enabled)
            {
                return;
            }
            dataMap[property]["enabled"].variant = val;
            emit propertyChanged(property);
            emit attributeChanged(property, attribute, enabled);
        }
        else
        {
            dataMap[property][attribute].variant = val;
            emit propertyChanged(property);
            emit attributeChanged(property, attribute, true);
        }
        return;

    }
    QtVariantPropertyManager::setAttribute(property, attribute, val);
}

void OptionalVariantManager::initializeProperty(QtProperty* property)
{
    if (propertyType(property) == optionalProprtyTypeId())
    {
        dataMap[property];
    }
    QtVariantPropertyManager::initializeProperty(property);
}

void OptionalVariantManager::uninitializeProperty(QtProperty* property)
{
    dataMap.remove(property);
    QtVariantPropertyManager::uninitializeProperty(property);
}
