/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include "filterabletreemodelsortfilterproxymodel.h"
#include "buttondelegate.h"
#include "openscenariomodel.h"
#include <QDialog>

namespace Ui
{
    class OpenScenarioView;
}

/**
* @class OpenScenarioView
* @brief View that allows to open a previously saved scenario.
*/
class OpenScenarioView : public QDialog
{
    Q_OBJECT

public:
    /**
    * Constructor that sets up the UI and delegates.
    * @param parent parent widget
    */
    explicit OpenScenarioView(QWidget* parent = 0);
    ~OpenScenarioView() override;

    /**
    * Sets the model of this view.
    * @param model new model
    */
    void setModel(FilterableTreeModelSortFilterProxyModelPtr model);

signals:
    void openScenario(int row, int column, QModelIndex parent);
    void showAddPackageDialog();

private slots:
    void openButtonClicked(int row, int column, QModelIndex parent);
    void on_lineEdit_textEdited(const QString& text);

    void on_treeView_doubleClicked(const QModelIndex& index);

    void on_openPackageButton_clicked();

private:
    Ui::OpenScenarioView* ui;
    FilterableTreeModelSortFilterProxyModelPtr model;
    ButtonDelegate openButtonDelegate;
};

