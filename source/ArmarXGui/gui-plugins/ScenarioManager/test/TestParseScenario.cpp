/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#define BOOST_TEST_MODULE ArmarX::ScenarioManager::ParseScenarioTest
#define ARMARX_BOOST_TEST

#include <ArmarXGui/Test.h>
#include <iostream>

#include <ArmarXCore/util/ScenarioManagerCommon/parser/PackageBuilder.h>
#include <ArmarXCore/util/ScenarioManagerCommon/data_structure/Application.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlWriter.h>

#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>
#include <ArmarXCore/core/application/properties/PropertyDefinitionContainerFormatter.h>
#include <ArmarXCore/core/application/properties/PropertyDefinitionFormatter.h>
#include <ArmarXCore/core/application/properties/PropertyDefinitionConfigFormatter.h>
#include <ArmarXCore/core/application/properties/IceProperties.h>
#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>

#include <Ice/Properties.h>

using namespace ScenarioManager;
using namespace Data_Structure;
using namespace Parser;
using namespace rapidxml;
using namespace armarx;

BOOST_AUTO_TEST_CASE(TestPackageThings)
{
    //ScenarioManager::Parser::CMakeParser parser;
    //ScenarioManager::Data_Structure::PackagePtr test = parser.parsePackage("Core");
    //std::cout << test.getName() << test.getPath();
}


BOOST_AUTO_TEST_CASE(testComponentInitialization)
{
    ApplicationPtr applicationptr(new Application("toller name", "./bla.ini", "ArmarXCore"));

    BOOST_CHECK_EQUAL(applicationptr->getName().c_str(), "toller name");
}

BOOST_AUTO_TEST_CASE(ParseIcePropertiesFromXml)
{
    IceProperties properties(IceProperties::create());

    xml_document<> doc;

    //Read xmlFile into vector
    //TODO XML MIT HIER REIN PACKEN
    std::ifstream file("path zu einer test xml");
    std::vector<char> buffer((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());
    buffer.push_back('\0');

    //Parse the buffer
    doc.parse<0>(&buffer[0]);

    PropertyDefinitionContainer container("NORMALERWEISE DER APP NAME");
    for (xml_node<>* property_node = doc.first_node("property"); property_node; property_node = property_node->next_sibling())
    {
        const std::string propertyName = property_node->first_attribute("name")->value();
        const std::string description = property_node->first_node("description")->value();
        xml_node<>* attributes_node = property_node->first_node("attributes");
        xml_node<>* first_attribute = attributes_node->first_node("attribute");
        xml_node<>* second_attribute = first_attribute->next_sibling();
        xml_node<>* third_attribute = second_attribute->next_sibling();

        std::string defaultValue = "";
        bool caseSensitive = false;
        bool required = false;

        if (std::string(first_attribute->first_attribute("name")->value()).find("Default") != std::string::npos)
        {
            defaultValue = first_attribute->value();
        }
        if (std::string(second_attribute->first_attribute("name")->value()).find("CaseSensitivity") != std::string::npos)
        {
            if (std::string(second_attribute->value()).compare("yes"))
            {
                caseSensitive = true;
            }
        }
        if (std::string(third_attribute->first_attribute("name")->value()).find("Required") != std::string::npos)
        {
            if (std::string(third_attribute->value()).compare("yes"))
            {
                required = true;
            }
        }

        if (required)
        {
            //funktioniert aus einem mir unersichtlichen grund nicht
            //PropertyDefinition<string> type = container.defineRequiredProperty(propertyName, description);
            //if(!caseSensitive)
            //  type = type.setCaseInsensitive(false);
        }
        else
        {
            PropertyDefinition<std::string> type = container.defineOptionalProperty(propertyName, defaultValue, description);
            if (!caseSensitive)
            {
                type = type.setCaseInsensitive(false);
            }
        }

    }

    PropertyDefinitionFormatter* defFormatter = new PropertyDefinitionConfigFormatter();
    PropertyDefinitionContainerFormatter*  pdcFormatter = new PropertyDefinitionContainerFormatter(*defFormatter);
    pdcFormatter->setProperties(properties.clone());

    //Das hier sollte in der wie in der email beschrieben gehen funktioniert aber nicht da ein PropertyDefinitionsPtr erwartet wird
    // std::string result = pdcFormatter->formatPropertyDefinitionContainer(container);
    //Versuch B war dann das hier waehre halt dann in der obigen schleife mit dabei geht aber auch nicht
    //std::string result = pdcFormatter->formatPropertyDefinitionContainer(container.getDefintion("ArmarX.Verbosity"));
}
