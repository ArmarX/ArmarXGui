/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#define BOOST_TEST_MODULE ArmarX::ScenarioManager::DataStructureTest
#define ARMARX_BOOST_TEST

#include <ArmarXGui/Test.h>
#include <iostream>

#include <ArmarXCore/util/ScenarioManagerCommon/data_structure/Application.h>
#include <ArmarXCore/util/ScenarioManagerCommon/data_structure/ApplicationInstance.h>
#include <ArmarXCore/util/ScenarioManagerCommon/data_structure/Package.h>
#include <ArmarXCore/util/ScenarioManagerCommon/data_structure/Scenario.h>
#include <ArmarXCore/util/ScenarioManagerCommon/parser/PackageBuilder.h>


#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/application/properties/IceProperties.h>

using namespace ScenarioManager;
using namespace Data_Structure;

PackagePtr p;
ApplicationVectorPtr apps;
ApplicationInstancePtr aip;
ScenarioPtr scenario1;
int applicationCounter = 0;

BOOST_AUTO_TEST_CASE(PackageInitialization)
{
    Parser::PackageBuilder cMParser = Parser::PackageBuilder();
    p = cMParser.parsePackage("ArmarXCore");

    BOOST_CHECK_EQUAL(p->getName(), "ArmarXCore");
}

BOOST_AUTO_TEST_CASE(ScenarioInitialization)
{
    scenario1 = ScenarioPtr(new Scenario("scenario1", "666666", "7777777", p));
    ScenarioPtr scenario2 = ScenarioPtr(new Scenario("scenario2", "1000", "7777777", p));
    ScenarioPtr scenario3 = ScenarioPtr(new Scenario("scenario3", "666666", "7777777", p));

    p->addScenario(scenario1);
    p->addScenario(scenario2);
    p->addScenario(scenario3);

    //BOOST_CHECK_EQUAL(p->getScenarios()->size(), 3);
    BOOST_CHECK_EQUAL(p->getScenarioByName("scenario2")->getCreationTime(), "1000");
}

BOOST_AUTO_TEST_CASE(ApplicationInitialization)
{
    int i = 0;
    apps = p->getApplications();
    int length = apps->size();
    ScenarioPtr s = p->getScenarioByName("scenario1");

    for (const auto& app : *apps)
    {
        i++;
        ApplicationInstancePtr ai = ApplicationInstancePtr(new ApplicationInstance((*app), "instance" + std::to_string(i), "", s, "NodeMain", false, true));
        if (i == 1)
        {
            aip = ai;
        }
        s->addApplication(ai);
    }
    applicationCounter = i;

    BOOST_CHECK_EQUAL(s->getApplications()->size(), length);
    BOOST_CHECK_EQUAL(p->getScenarioByName("scenario1")->getApplications(), s->getApplications());
}

BOOST_AUTO_TEST_CASE(ApplicationDataConsistency)
{
    ScenarioPtr s = p->getScenarioByName("scenario1");
    for (const auto& app : * (s->getApplications()))
    {
        BOOST_CHECK_EQUAL(app->getConfigPath(), "");


        if (app->getInstanceName().find("instance") == std::string::npos)
        {
            BOOST_ERROR("Naming inconsistency");
        }
    }
}


BOOST_AUTO_TEST_CASE(ApplicationSettersAndGetters)
{
    ApplicationInstancePtr ai = aip;

    ai->setInstanceName("newname");
    BOOST_CHECK_EQUAL(ai->getInstanceName(), "newname");

    ai->setConfigPath("newpath");
    BOOST_CHECK_EQUAL(ai->getConfigPath(), "newpath");

    ai->setStatus("newstatus");
    BOOST_CHECK_EQUAL(ai->getStatus(), "newstatus");

    //    ai->addProperty("newProperty", "newValue");
    //    BOOST_CHECK_EQUAL(ai->getProperties().getProperty("newProperty"), "newValue");

    //    ai->modifyProperty("newProperty", "newnewValue");
    //    BOOST_CHECK_EQUAL(ai->getProperties().getProperty("newProperty"), "newnewValue");

    //Just check if there are exceptions. No good way of using checks.
    ai->save();
    ai->load();
}

BOOST_AUTO_TEST_CASE(ScenarioSettersAndGetters)
{
    scenario1->setName("newname");
    BOOST_CHECK_EQUAL(scenario1->getName(), "newname");

    BOOST_CHECK_EQUAL(scenario1->getPath(), p->getScenarioPath().append("/").append(scenario1->getName()).append("/").append(scenario1->getName()).append(".scx"));

    BOOST_CHECK_EQUAL(scenario1->getPackage(), p);

    scenario1->removeApplication(aip);
    applicationCounter--;
    BOOST_CHECK_EQUAL(scenario1->getApplications()->size(), applicationCounter);
}
