/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Gui
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once


// QT
#include <ArmarXGui/gui-plugins/PlotterPlugin/ui_ArmarXPlotterDialog.h>

#include <ArmarXGui/gui-plugins/ObserverPropertiesPlugin/ObserverItemModel.h>

#include <ArmarXCore/core/IceManager.h>
#include <ArmarXCore/interface/observers/ConditionHandlerInterface.h>

#include <QDialog>
#include <QComboBox>
#include <QList>
#include <QMenu>
#include <QAction>
#include <QItemSelectionModel>
#include <QStandardItemModel>

#include <vector>


namespace armarx
{
    class InfixFilterModel;
    class ArmarXPlotter;
    class ArmarXPlotterDialog:
        public QDialog
    {
        Q_OBJECT
        QStringList availableChannelsList;
        ObserverItemModel* model;
        InfixFilterModel* proxyModel;
        std::string uuid;
        IceManagerPtr iceManager;
        int mdiId;


    public:
        ArmarXPlotterDialog(QWidget* parent, IceManagerPtr iceManager);
        ~ArmarXPlotterDialog() override;
        ConditionHandlerInterfacePrx handler;
        struct ChannelWidgetsEntry
        {
            QComboBox* comboBox;
            QLabel* label;
            QPushButton* deleteButton;
        };

        void setIceManager(IceManagerPtr iceManager);

        Ui::ArmarXPlotterDialog ui;
        std::vector< ChannelWidgetsEntry > sensorChannelList;
        QStringList getSelectedDatafields();


        //        // inherited from Component
        //        std::string getDefaultName() const;
        //        virtual void onInitComponent();
        //        virtual void onConnectComponent();
        //        void onExitComponent();
        /**
        * emits the closeRequest signal
        */
        void onCloseWidget(QCloseEvent* event);
    public slots:
        void ButtonAddSelectedChannelClicked();
        void ButtonRemoveChannelClicked();
        void ButtonRemoveChannelClicked(QModelIndex index);
        void updateObservers();
        void treeView_selected(const QItemSelection& selected, const QItemSelection& deselected);
        void treeView_doubleClick(const QModelIndex& proxyIndex);
        void destroyed(QObject*);
        void showEvent(QShowEvent*) override;
    private slots:
        void on_btnSelectLoggingDir_clicked();
    };
}
