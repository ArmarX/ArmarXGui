/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Gui
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "ArmarXPlotterDialog.h"
#include "ArmarXPlotter.h"

#include <IceUtil/UUID.h>

#include <QFileDialog>
#include <sstream>

#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/InfixFilterModel.h>


namespace armarx
{
    ArmarXPlotterDialog::ArmarXPlotterDialog(QWidget* parent, IceManagerPtr iceManager) :
        QDialog(parent),
        uuid(IceUtil::generateUUID()),
        iceManager(iceManager)
    {
        ui.setupUi(this);
        model = NULL;


        connect(ui.BTNAddSelectedChannels, SIGNAL(clicked()), this, SLOT(ButtonAddSelectedChannelClicked()));
        connect(ui.BTNRemoveSelected, SIGNAL(clicked()), this, SLOT(ButtonRemoveChannelClicked()));
        connect(ui.treeViewObservers, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(treeView_doubleClick(QModelIndex)));
        connect(ui.listWidget, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(ButtonRemoveChannelClicked(QModelIndex)));
        connect(ui.btnRefresh, SIGNAL(clicked()), this, SLOT(updateObservers()));


    }

    ArmarXPlotterDialog::~ArmarXPlotterDialog()
    {
        //    ARMARX_INFO << "~ArmarXPlotterDialog" ;
    }

    void ArmarXPlotterDialog::setIceManager(IceManagerPtr iceManager)
    {
        this->iceManager = iceManager;
    }



    //string ArmarXPlotterDialog::getDefaultName() const
    //{
    //    stringstream str;
    //    str << "ArmarXPlotterDialog" << uuid;
    //    return str.str();
    //}


    //void ArmarXPlotterDialog::onInitComponent()
    //{
    //    usingProxy("ConditionHandler");
    //}

    //void ArmarXPlotterDialog::onConnectComponent()
    //{
    //    // get proxy of conditionhandler
    //    handler = getProxy<ConditionHandlerInterfacePrx>("ConditionHandler");

    //    updateObservers();
    //}

    //void ArmarXPlotterDialog::onExitComponent()
    //{

    //}

    void ArmarXPlotterDialog::onCloseWidget(QCloseEvent* event)
    {

    }

    void ArmarXPlotterDialog::updateObservers()
    {
        if (!model)
        {
            model = new ObserverItemModel(iceManager, NULL);
            //        proxyModel = new InfixFilterModel(this);
            //        proxyModel->setSourceModel(model);
            //        ui.treeViewObservers->setModel(proxyModel);
            ui.treeViewObservers->setModel(model);

        }

        model->updateObservers();

        //    if(!iceManager)
        //        return;
        //    ObserverList observerList = iceManager->getIceGridSession()->getRegisteredObjectNames<ObserverInterfacePrx>("*Observer");
        //    ObserverList::iterator iter = observerList.begin();

        //    while(iter != observerList.end())
        //    {
        //        model->updateModel(*iter, iceManager->getProxy<ObserverInterfacePrx>(*iter)->getAvailableChannels(), StringConditionCheckMap());
        //        iter++;
        //    }
    }






    void ArmarXPlotterDialog::ButtonAddSelectedChannelClicked()
    {
        QItemSelectionModel* selectionModel = ui.treeViewObservers->selectionModel();

        //    auto selection = ui.treeViewObservers->getProxyModel()->mapSelectionToSource(selectionModel->selection());

        for (auto index : selectionModel->selection().indexes())
        {
            treeView_doubleClick(index);
        }

        ui.treeViewObservers->clearSelection();

    }

    void ArmarXPlotterDialog::ButtonRemoveChannelClicked()
    {
        QList<QListWidgetItem*> selectedItems = ui.listWidget->selectedItems();

        for (int i = 0; i < selectedItems.size(); ++i)
        {
            delete selectedItems.at(i);
        }
    }

    void ArmarXPlotterDialog::ButtonRemoveChannelClicked(QModelIndex index)
    {
        delete ui.listWidget->item(index.row());
    }

    QStringList ArmarXPlotterDialog::getSelectedDatafields()
    {
        QStringList result;
        QList <QListWidgetItem*> items = ui.listWidget->findItems(QString("*"), Qt::MatchWrap | Qt::MatchWildcard);
        foreach (QListWidgetItem* item, items)
        {
            result.append(item->text());
        }

        return result;
    }

    void ArmarXPlotterDialog::treeView_selected(const QItemSelection& selected, const QItemSelection& deselected)
    {
    }

    void ArmarXPlotterDialog::treeView_doubleClick(const QModelIndex& proxyIndex)
    {

        auto modelIndex = ui.treeViewObservers->getProxyModel()->mapToSource(proxyIndex);
        QStandardItem* item = model->itemFromIndex(modelIndex);
        QVariant id = item->data(OBSERVER_ITEM_ID);

        switch (item->data(OBSERVER_ITEM_TYPE).toInt())
        {
            case eDataFieldItem:
                if (ui.listWidget->findItems(id.toString(), Qt::MatchExactly).size() == 0)
                {
                    ui.listWidget->addItem(id.toString());
                }

                break;

        }
    }

    void ArmarXPlotterDialog::destroyed(QObject*)
    {
        setParent(0);
    }

    void ArmarXPlotterDialog::showEvent(QShowEvent*)
    {
        updateObservers();
    }


    void armarx::ArmarXPlotterDialog::on_btnSelectLoggingDir_clicked()
    {
        QString newLoggingDir = QFileDialog::getExistingDirectory(this, "Select a directory to which data should be logged",
                                ui.editLoggingDirectory->text());

        if (!newLoggingDir.isEmpty())
        {
            ui.editLoggingDirectory->setText(newLoggingDir);
        }
    }
}
