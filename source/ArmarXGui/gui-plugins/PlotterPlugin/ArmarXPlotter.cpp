/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Gui
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "ArmarXPlotter.h"
#include "ArmarXPlotterDialog.h"

#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/observers/ObserverObjectFactories.h>
#include <ArmarXCore/observers/variant/DataFieldIdentifier.h>
#include <ArmarXCore/observers/exceptions/local/InvalidChannelException.h>

#include <SimoxUtility/algorithm/string/string_tools.h>

//QT
#include <QTimer>
#include <QTime>
#include <QSettings>
#include <QDir>
#include <QStackedLayout>
#include <QTextEdit>


#include <IceUtil/IceUtil.h>

#include <filesystem>
#include <sstream>


namespace armarx
{
    ArmarXPlotter::ArmarXPlotter() :
        startUpTime(QDateTime::currentDateTime())
    {
        plotterController = new PlotterController {this};
        setTag("Plotter");
        ui.setupUi(getWidget());
        dialog = new ArmarXPlotterDialog(getWidget(), NULL);
        loggingDir = (QDir::currentPath());
        dialog->ui.editLoggingDirectory->setText(loggingDir);

        plotterController->setGraphStyle(ui.CBgraphStyle->currentText().toStdString());
        ui.verticalLayout->insertWidget(0, plotterController->getPlotterWidget());
    }

    ArmarXPlotter::~ArmarXPlotter()
    {
        //        if(dialog && this->getState() == eManagedIceObjectInitialized)
        //            getArmarXManager()->removeObjectNonBlocking(dialog);
        //        delete dialog;
    }

    void ArmarXPlotter::onInitComponent() {}

    void ArmarXPlotter::onConnectComponent()
    {
        ARMARX_VERBOSE << "ArmarXPlotter started" << flush;
        dialog->setIceManager(getIceManager());
        plotterController->setIceManager(getIceManager());
        connect(dialog, SIGNAL(accepted()), this, SLOT(configDone()));
        connect(ui.BTNEdit, SIGNAL(clicked()), this, SLOT(configDialog()));
        connect(ui.BTNPlotterStatus, SIGNAL(toggled(bool)), plotterController, SLOT(plottingPaused(bool)));

        connect(ui.BTNAutoScale, SIGNAL(toggled(bool)), plotterController, SLOT(autoScale(bool)));

        connect(ui.btnLogToFile, SIGNAL(toggled(bool)), this, SLOT(toggleLogging(bool)));

        connect(ui.CBgraphStyle, SIGNAL(currentTextChanged(QString)), this, SLOT(onGraphStyleChanged(QString)));

        connect(ui.btnClearHistory, SIGNAL(clicked()), plotterController, SLOT(clearHistory()));

        if (!QMetaObject::invokeMethod(plotterController, "setupCurves"))
        {
            ARMARX_WARNING << "Failed to invoke setupCurves";
        }

        usingTopic("TopicReplayerListener");
    }

    void ArmarXPlotter::onExitComponent()
    {
        //        unsubscribeFromTopic("TopicReplayerListener");
        plotterController->deleteLater();
    }

    void ArmarXPlotter::onCloseWidget(QCloseEvent* event)
    {
        ARMARX_VERBOSE << "closing" << flush;
        timer->stop();

        if (logstream.is_open())
        {
            logstream.close();
        }
    }

    void ArmarXPlotter::ButtonAddSensorChannelClicked()
    {
    }

    void ArmarXPlotter::configDialog()
    {
        if (!dialog)
        {
            return;
        }

        dialog->ui.spinBoxUpdateInterval->setValue(plotterController->getUpdateInterval());
        dialog->ui.spinBoxShownInterval->setValue(plotterController->getShownInterval());
        dialog->ui.syncDataLogging->setChecked(syncDataLogging);
        dialog->ui.listWidget->clear();
        dialog->ui.listWidget->addItems(plotterController->getSelectedDatafields());
        //        if(dialog->exec())
        //            configDone();
        dialog->setModal(true);
        dialog->show();
        //        dialog->raise();
        //        dialog->activateWindow();
        //        dialog->setParent(0);
    }

    void ArmarXPlotter::configDone()
    {
        {
            //            plotterController->clear();
            std::unique_lock lock(dataMutex);
            plotterController->setUpdateInterval(dialog->ui.spinBoxUpdateInterval->value());
            plotterController->setShownInterval(dialog->ui.spinBoxShownInterval->value());
            plotterController->setPollingInterval(dialog->ui.spinBoxPollingInterval->value());
            //            selectedChannels = dialog->getSelectedChannels();
            loggingDir = dialog->ui.editLoggingDirectory->text();
            syncDataLogging = dialog->ui.syncDataLogging->isChecked();
            ui.btnLogToFile->setChecked(false);
            ui.BTNPlotterStatus->setChecked(false);
            plotterController->setSelectedDatafields(dialog->getSelectedDatafields());
        }

        //        plotterController->setupCurves();
    }

    void ArmarXPlotter::toggleLogging(bool toggled)
    {
        std::lock_guard<std::mutex> lock(mutex);

        if (toggled)
        {
            std::filesystem::path outputPath;
            if (filename.empty())
            {
                outputPath = loggingDir.toStdString() + "/datalog.csv";

                std::string time = IceUtil::Time::now().toDateTime();
                time = simox::alg::replace_all(time, "/", "-");
                time = simox::alg::replace_all(time, " ", "_");
                time = simox::alg::replace_all(time, ":", "-");
                outputPath = outputPath.parent_path() / (outputPath.stem().string() + "-" + time + outputPath.extension().string());

            }
            else
            {
                std::filesystem::path path = filename;
                outputPath = loggingDir.toStdString() + "/" + path.stem().string() + ".csv";
            }

            ARMARX_INFO << "Logging to " << outputPath.string();
            logstream.open(outputPath.string());
            logStartTime = IceUtil::Time::now();

            if (!logstream.is_open())
            {
                ARMARX_ERROR << "Could not open file for logging: " << outputPath.string();
                ui.btnLogToFile->setChecked(false);
            }
            else
            {
                std::lock_guard<std::mutex> lock(fileMutex);

                csvHeader = plotterController->getSelectedDatafieldsKeys();

                logstream << "Timestamp";
                for (const auto& s : csvHeader)
                {
                    logstream <<  "," << s;
                }
                logstream << std::endl;

                connect(plotterController, SIGNAL(newDataAvailable(long, std::map<std::string, VariantPtr>)), this, SLOT(logToFile(long, std::map<std::string, VariantPtr>)));
            }

        }
        else
        {
            filename =  "";
            QObject::disconnect(plotterController, SIGNAL(newDataAvailable(long, std::map<std::string, VariantPtr>)), this, SLOT(logToFile(long, std::map<std::string, VariantPtr>)));

            std::lock_guard<std::mutex> lock(fileMutex);

            logstream.flush();
            ARMARX_INFO << "done writing log";
            logstream.close();

            csvHeader.clear();
        }
    }

    void ArmarXPlotter::onGraphStyleChanged(const QString& style)
    {
        const auto st = style.toStdString();
        plotterController->setGraphStyle(st);
        if (plotterController->getGraphStyle() != st)
        {
            if (style == "Bar chart")
            {
                ui.BTNAutoScale->setDisabled(true);
            }
            else
            {
                ui.BTNAutoScale->setDisabled(false);
            }
        }
    }

    void ArmarXPlotter::saveSettings(QSettings* settings)
    {
        std::unique_lock lock(dataMutex);
        settings->setValue("updateInterval", plotterController->getUpdateInterval());
        settings->setValue("shownInterval", plotterController->getShownInterval());
        settings->setValue("pollingInterval", plotterController->getPollingInterval());
        settings->setValue("selectedChannels", plotterController->getSelectedDatafields());
        settings->setValue("autoScaleActive", ui.BTNAutoScale->isChecked());
        settings->setValue("syncDataLogging", syncDataLogging);

    }

    void ArmarXPlotter::loadSettings(QSettings* settings)
    {
        std::unique_lock lock(dataMutex);
        plotterController->setUpdateInterval(settings->value("updateInterval", 100).toInt());
        plotterController->setShownInterval(settings->value("shownInterval", 60).toInt());
        plotterController->setPollingInterval(settings->value("pollingInterval", 33).toInt());
        plotterController->setSelectedDatafields(settings->value("selectedChannels", QStringList()).toStringList());
        ui.BTNAutoScale->setChecked(settings->value("autoScaleActive", true).toBool());
        syncDataLogging = settings->value("syncDataLogging", false).toBool();

        ARMARX_VERBOSE << "Settings loaded";
    }

    void ArmarXPlotter::logToFile(long timestamp, const std::map<std::string, VariantPtr>& dataMaptoAppend)
    {
        std::lock_guard<std::mutex> lock(fileMutex);

        if (!logstream.is_open() || !dataMaptoAppend.size())
        {
            return;
        }

        logstream << (IceUtil::Time::microSeconds(timestamp) - logStartTime).toMilliSecondsDouble();

        for (const std::string f : csvHeader)
        {
            logstream << ",";
            if (dataMaptoAppend.count(f))
            {
                logstream << dataMaptoAppend.at(f)->Variant::getOutputValueOnly();
            }
        }
        logstream  << std::endl;
    }
}
