/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX::Gui
 * @author     Jan Issac ( jan.issac at gmail dot com)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "ArmarXManagerItem.h"

namespace armarx
{
    ArmarXManagerItem::ArmarXManagerItem(const QString& name):
        QStandardItem(name)
    {
        data.name = name;
    }


    void ArmarXManagerItem::clear(bool clearProxy)
    {
        // ScopedLock lock(getMutex());

        while (this->rowCount() > 0)
        {
            this->removeRow(0);
        }

        data.objects.clear();

        if (clearProxy)
        {
            data.proxy = 0;
        }
        data.connection = nullptr;
        data.online = false;
    }


    QString ArmarXManagerItem::getName() const
    {
        return data.name;
    }


    bool ArmarXManagerItem::isOnline() const
    {
        return data.online;
    }


    ArmarXManagerInterfacePrx ArmarXManagerItem::getManagerProxy() const
    {
        return data.proxy;
    }

    Ice::ConnectionPtr ArmarXManagerItem::getConnection() const
    {
        return data.connection;
    }

    QString ArmarXManagerItem::getEndpointStr() const
    {
        return data.endpointStr;
    }


    ArmarXManagerItem::ObjectMap& ArmarXManagerItem::getObjects()
    {
        return data.objects;
    }


    void ArmarXManagerItem::setName(QString name)
    {
        // reset item if manager name has been changed
        if (getName() != name)
        {
            clear();

            // ScopedLock lock(getMutex());
            this->data.name = name;
        }
    }


    void ArmarXManagerItem::setOnline(bool online)
    {
        // ScopedLock lock(getMutex());

        this->data.online = online;
    }


    void ArmarXManagerItem::setManagerProxy(ArmarXManagerInterfacePrx proxy)
    {
        // ScopedLock lock(getMutex());

        this->data.proxy = proxy;
    }

    void ArmarXManagerItem::setAppProperties(const ObjectPropertyInfos& properties)
    {
        data.appProperties = properties;
    }

    void ArmarXManagerItem::setConnection(Ice::ConnectionPtr connection)
    {
        this->data.connection = connection;
    }

    void ArmarXManagerItem::setEndpointStr(const QString& endpointStr)
    {
        this->data.endpointStr = endpointStr;
    }

    const ObjectPropertyInfos& ArmarXManagerItem::getAppProperties() const
    {
        return data.appProperties;
    }


    std::mutex& ArmarXManagerItem::getMutex()
    {
        return mutex;
    }
}
