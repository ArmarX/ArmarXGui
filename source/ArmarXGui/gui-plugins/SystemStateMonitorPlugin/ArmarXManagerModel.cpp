/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX::Gui
 * @author     Jan Issac ( jan.issac at gmail dot com)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "ArmarXManagerModel.h"
#include "ManagedIceObjectItem.h"

#include <ArmarXGui/libraries/ArmarXGuiBase/QtUtil.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXWidgetController.h>
#include <ArmarXCore/observers/variant/Variant.h>

#include <SimoxUtility/algorithm/string/string_tools.h>

#include <Ice/Connection.h>

#include <QStandardItem>
#include <QFileSystemModel>

#include <fstream>

#define LiveMetaInfoDescriptionString "Live Meta Information"

namespace armarx
{
    ArmarXManagerModel::ArmarXManagerModel():
        iceManager(0)
    {
        connect(this, SIGNAL(itemChanged(QStandardItem*)), this, SLOT(updateItem(QStandardItem*)));
    }


    ArmarXManagerModel::~ArmarXManagerModel()
    {

    }


    ArmarXManagerItem* ArmarXManagerModel::getItem(int row, int column) const
    {
        return dynamic_cast<ArmarXManagerItem*>(QStandardItemModel::item(row, column));
    }


    ArmarXManagerItem* ArmarXManagerModel::getManagerItemByName(const QString& name)
    {
        CHECK_QT_THREAD(this);

        ArmarXManagerItem* item;

        for (int i = 0; i < this->rowCount(); i++)
        {
            item = this->getItem(i);

            if (item->getName().compare(name) == 0)
            {
                return item;
            }
        }

        return nullptr;
    }

    ManagerPrxMap ArmarXManagerModel::getManagerProxyMap() const
    {
        CHECK_QT_THREAD(this);

        ManagerPrxMap result;
        ArmarXManagerItem* item;
        //    ARMARX_IMPORTANT << this->rowCount();
        for (int i = 0; i < this->rowCount(); i++)
        {
            item = this->getItem(i);
            if (!item->getManagerProxy())
            {
                try
                {
                    ARMARX_DEBUG << item->getName().toStdString();
                    auto proxy = getIceManager()->getProxy<ArmarXManagerInterfacePrx>(item->getName().toStdString());
                    ARMARX_DEBUG << item->getName().toStdString() << " done";
                    item->setManagerProxy(ArmarXManagerInterfacePrx::uncheckedCast(proxy));

                }
                catch (...)
                {
                    armarx::handleExceptions();
                }
            }
            result[item->getName()] = item->getManagerProxy();
        }
        return result;
    }

    ManagerPrxMap ArmarXManagerModel::getManagerProxyMap(const QStringList& managerNames) const
    {
        ManagerPrxMap result;
        //    ARMARX_IMPORTANT << this->rowCount();
        for (auto& name : managerNames)
        {
            try
            {
                ARMARX_DEBUG << name.toStdString();
                auto proxy = getIceManager()->getProxy<ArmarXManagerInterfacePrx>(name.toStdString());
                ARMARX_DEBUG << name.toStdString() << " done";
                result[name] = proxy;
            }
            catch (...)
            {
                armarx::handleExceptions();
            }
        }


        return result;
    }


    void ArmarXManagerModel::updateManagerDetails(const  ArmarXManagerItem::ManagerDataMap& managerDataMap, bool deactivateIfMissing)
    {
        CHECK_QT_THREAD(this);
        dependenciesMap.clear();
        IceUtil::Time start = IceUtil::Time::now();

        std::map<std::string, std::vector<QString> > offererMap;
        std::map<std::string, std::vector<QString> > listenerMap;
        // iterate over all manager items
        for (int i = 0; i < this->rowCount(); i++)
        {
            ArmarXManagerItem* item = this->getItem(i);
            //        ARMARX_DEBUG << deactivateSpam(1, item->getName().toStdString()) << "Checking " << item->getName().toStdString();
            //        ARMARX_INFO << "Checking " << item->getName().toStdString();
            //        for (auto& element : item->getObjects())
            //        {
            //            DependencyMap depMap = element.connectivity.dependencies;
            //            //            ARMARX_INFO << "Checking sub item " << element.name.toStdString() << " with " << depMap.size() << " dependencies";
            //            if (depMap.size() > 0)
            //            {
            //                dependenciesMap[toStdString()] = depMap;
            //            }
            //        }

            auto it = managerDataMap.find(item->getName());
            if (it == managerDataMap.end())
            {
                ARMARX_INFO << deactivateSpam(15, item->getName().toStdString()) << "Didnt find " << item->getName().toStdString();
                if (deactivateIfMissing)
                {
                    item->setOnline(false);
                }
                continue;
            }
            else
            {
                item->setOnline(it->second.online);
            }

            item->setConnection(it->second.connection);
            item->setEndpointStr(it->second.endpointStr);
            //        ARMARX_DEBUG << deactivateSpam(1, item->getName().toStdString()) << "endpoint: " << it->second.endpointStr;

            if (!item->getManagerProxy() && !it->second.proxy)
            {
                ARMARX_INFO << "Proxy for " << item->getName().toStdString() << " is null";
                item->setOnline(false);
                continue;
            }
            if (it->second.proxy)
            {
                item->setManagerProxy(it->second.proxy);
            }
            item->setAppProperties(it->second.appProperties);

            const ArmarXManagerItem::ObjectMap& updates = it->second.objects;
            ArmarXManagerItem::ObjectMap& objects = item->getObjects();
            for (int j = 0; j < updates.size(); j++)
            {
                QString objName = updates.keys().at(j);
                const ManagedIceObjectItem& updateItem = updates[objName];
                ManagedIceObjectItem& objItem = objects[objName];
                objItem.connectivity = updateItem.connectivity;
                objItem.state = updateItem.state;
                objItem.properties = updateItem.properties;
                objItem.metaInfoMap = updateItem.metaInfoMap;
                for (const auto& topic : objItem.connectivity.offeredTopics)
                {
                    offererMap[topic].push_back(objName);
                }
                for (const auto& topic : objItem.connectivity.usedTopics)
                {
                    listenerMap[topic].push_back(objName);
                }
                if (objItem.connectivity.dependencies.size() > 0)
                {
                    dependenciesMap[objName.toStdString()] = objItem.connectivity.dependencies;
                }

            }
        }
        //    std::set<std::string> offeredTopics = armarx::getMapKeys(offererMap);
        //    ARMARX_INFO << deactivateSpam(10) << VAROUT(listenerMap) << "\n" << VAROUT(offererMap);
        // iterate over all manager items
        for (int i = 0; i < this->rowCount(); i++)
        {
            ArmarXManagerItem* item = this->getItem(i);
            item->setEditable(false);

            QBrush managerBrush;

            if (!item->isOnline())
            {
                managerBrush.setStyle(Qt::SolidPattern);
                managerBrush.setColor(Qt::lightGray);
                item->clear(false);
            }
            else
            {
                managerBrush.setColor(Qt::transparent);
            }

            item->setBackground(managerBrush);

            auto addPropertiesToItem = [](ObjectPropertyInfos const & properties, QStandardItem * item)
            {
                if (!properties.empty())
                {
                    QStandardItem* objectPropertiesItem = new QStandardItem("Properties");
                    objectPropertiesItem->setEditable(false);

                    QList<QStandardItem*> objectPropertiesRow;

                    for (auto property : properties)
                    {
                        QStandardItem* objectPropertyItem = new QStandardItem(QString("%1 %2: %3").arg(property.first.c_str())
                                .arg(property.second.constant ? "(constant)" : "")
                                .arg(property.second.value.c_str()));
                        objectPropertyItem->setEditable(!property.second.constant);
                        if (!property.second.constant)
                        {
                            objectPropertyItem->setToolTip("Double-click to edit this property and to send the updated property to the component.");
                        }
                        objectPropertiesRow.append(objectPropertyItem);
                    }

                    objectPropertiesItem->appendRows(objectPropertiesRow);
                    item->appendRow(objectPropertiesItem);
                }
            };

            if (item->rowCount() == 0 && item->isOnline()) // only when app is coming (back) online
            {
                auto applicationItem = new QStandardItem("Application Properties");
                applicationItem->setData((int)ApplicationRowType::ApplicationProperties, (int)UserDataRoles::RowType);
                applicationItem->setEditable(false);
                try
                {

                    auto& properties = item->getAppProperties();//item->getManagerProxy()->ice_timeout(1000)->getApplicationPropertyInfos();
                    addPropertiesToItem(properties, applicationItem);
                    item->appendRow(applicationItem);
                }
                catch (...) {}
            }

            ArmarXManagerItem::ObjectMap& objects = item->getObjects();
            // iterate over all ManagedIceObjectItems controlled by the manager
            for (int j = 0; j < objects.size(); j++)
            {
                QString objName = objects.keys().at(j);


                ManagedIceObjectItem& objItem = objects[objName];
                if (!objItem.item)
                {
                    objItem.item = new QStandardItem(objName);
                    objItem.item->setEditable(false);

                    item->appendRow(objItem.item);

                    auto& properties = objItem.properties;

                    // iterate over all properties and add them as a separate subentry

                    addPropertiesToItem(properties, objItem.item);

                    QStandardItem* metaInfoItem = new QStandardItem(LiveMetaInfoDescriptionString);
                    metaInfoItem->setEditable(false);
                    for (auto& pair : objItem.metaInfoMap)
                    {
                        auto name = QString::fromStdString(pair.first);
                        auto value = QString::fromStdString(VariantPtr::dynamicCast(pair.second)->getOutputValueOnly());
                        QStandardItem* infoItem = new QStandardItem(name + ": " + (value.size() > 100 ? (value.left(100) + "...") : value));
                        metaInfoItem->appendRow(infoItem);
                    }
                    objItem.item->appendRow(metaInfoItem);

                    DependencyMap depMap = objItem.connectivity.dependencies;
                    DependencyMap::iterator it = depMap.begin();

                    // iterate over proxy dependencies
                    while (it != depMap.end())
                    {
                        QStandardItem* depObjItem = new QStandardItem(it->second->getName().c_str());
                        depObjItem->setEditable(false);

                        if (it->second->getResolved())
                        {
                            depObjItem->setData(true, (int)UserDataRoles::ResolvedDependency);
                            depObjItem->setBackground(QBrush(QColor(50, 205, 50)));
                            depObjItem->setToolTip("Dependency resolved");
                        }
                        else
                        {
                            depObjItem->setData(false, (int)UserDataRoles::ResolvedDependency);
                            depObjItem->setBackground(QBrush(QColor(255, 106, 106)));
                            depObjItem->setToolTip("Dependency missing");
                        }
                        objItem.item->appendRow(depObjItem);

                        ++it;
                    }

                }
                else
                {

                    auto endpointItem = findItemByUserData(static_cast<Qt::ItemDataRole>(UserDataRoles::RowType), (int)ApplicationRowType::Endpoint, item);
                    auto endpointText = "Endpoint: " + item->getEndpointStr();
                    if (!endpointItem)
                    {
                        endpointItem = new QStandardItem(endpointText);
                        endpointItem->setEditable(false);
                        endpointItem->setData((int)ApplicationRowType::Endpoint, (int)UserDataRoles::RowType);
                        item->appendRow(endpointItem);
                    }
                    else
                    {
                        endpointItem->setText(endpointText);
                    }

                    DependencyMap depMap = objItem.connectivity.dependencies;
                    DependencyMap::iterator it = depMap.begin();

                    // iterate over proxy dependencies
                    while (it != depMap.end())
                    {
                        auto depObjItem = findItem(QString::fromStdString(it->second->getName()), objItem.item);


                        if (depObjItem)
                        {
                            depObjItem->setEditable(false);

                            if (it->second->getResolved())
                            {
                                depObjItem->setData(true, (int)UserDataRoles::ResolvedDependency);
                                depObjItem->setBackground(QBrush(QColor(50, 205, 50)));
                                depObjItem->setToolTip("Dependency resolved");
                            }
                            else
                            {
                                depObjItem->setData(false, (int)UserDataRoles::ResolvedDependency);
                                depObjItem->setBackground(QBrush(QColor(255, 106, 106)));
                                depObjItem->setToolTip("Dependency missing");
                            }
                        }


                        ++it;
                    }

                    if (objItem.connectivity.offeredTopics.size() > 0)
                    {
                        auto topicsItem = findItem(QString("Offered Topics"), objItem.item);
                        if (!topicsItem)
                        {
                            topicsItem = new QStandardItem(QString::fromStdString("Offered Topics"));
                            topicsItem->setEditable(false);
                            objItem.item->appendRow(topicsItem);
                        }

                        for (const auto& topic : objItem.connectivity.offeredTopics)
                        {
                            auto topicItem = findItem(QString::fromStdString(topic), topicsItem);
                            if (!topicItem)
                            {
                                topicItem = new QStandardItem(QString::fromStdString(topic));
                                topicItem->setEditable(false);
                                topicsItem->appendRow(topicItem);
                            }
                            auto& listeners = listenerMap[topic];
                            syncStringListChildren(topicItem, listeners);
                            //                        topicItem->removeRows(0, topicItem->rowCount());
                            //                        for (auto& listener : listeners)
                            //                        {
                            //                            QStandardItem* listenerItem = new QStandardItem(("Listener: " + listener));
                            //                            listenerItem->setEditable(false);
                            //                            topicItem->appendRow(listenerItem);
                            //                        }

                        }
                    }

                    if (objItem.connectivity.usedTopics.size() > 0)
                    {
                        auto topicsItem = findItem(QString("Used Topics"), objItem.item);
                        if (!topicsItem)
                        {
                            topicsItem = new QStandardItem(QString::fromStdString("Used Topics"));
                            topicsItem->setEditable(false);
                            objItem.item->appendRow(topicsItem);
                        }
                        if (topicsItem)
                        {
                            //                    QList<QStandardItem*> objectPropertiesRow;

                            for (const auto& topic : objItem.connectivity.usedTopics)
                            {
                                auto topicItem = findItem(QString::fromStdString(topic), topicsItem);
                                if (!topicItem)
                                {
                                    topicItem = new QStandardItem(QString::fromStdString(topic));
                                    topicItem->setEditable(false);
                                    topicsItem->appendRow(topicItem);
                                }
                                if (offererMap.count(topic) == 0)
                                {
                                    topicItem->setBackground(QBrush(QColor(255, 163, 33)));
                                    topicItem->setToolTip("The topic " + QString::fromStdString(topic) + " is not offered by any listed component - though not all components might be listed");
                                }
                                else
                                {
                                    topicItem->setBackground(QBrush());
                                    auto& offerers = offererMap[topic];
                                    syncStringListChildren(topicItem, offerers);
                                    //                                topicItem->removeRows(0, topicItem->rowCount());
                                    //                                for (auto& offerer : offerers)
                                    //                                {
                                    //                                    QStandardItem* offererItem = new QStandardItem(("Offerer: " + offerer));
                                    //                                    offererItem->setEditable(false);
                                    //                                    topicItem->appendRow(offererItem);
                                    //                                }

                                }
                                topicItem->setEditable(false);
                                //                        objectPropertiesRow.append(topicItem);
                            }
                        }
                    }

                    // update meta infos
                    auto metaInfoItem = findItem(QString(LiveMetaInfoDescriptionString), objItem.item);
                    if (metaInfoItem)
                    {
                        metaInfoItem->removeRows(0, metaInfoItem->rowCount());
                        for (auto& pair : objItem.metaInfoMap)
                        {
                            auto name = QString::fromStdString(pair.first);
                            auto value = QString::fromStdString(VariantPtr::dynamicCast(pair.second)->getOutputValueOnly());
                            QStandardItem* infoItem = new QStandardItem(name + ": " + (value.size() > 100 ? (value.left(100) + "...") : value));
                            metaInfoItem->appendRow(infoItem);
                        }
                    }
                }

                objItem.item->setBackground(getBrush(objItem.state));
                auto stateBool = objItem.state == eManagedIceObjectStarted;
                //            ARMARX_INFO << "Setting objItem: " << objItem.name.toStdString() << " with shown name " << objItem.item->text().toStdString() << " to " << stateBool;

                objItem.item->setToolTip("Component state: " + QString::fromStdString(ManagedIceObject::GetObjectStateAsString(objItem.state)));
                objItem.item->setData(stateBool, (int)UserDataRoles::ComponentStarted);
            }
        }

        ARMARX_DEBUG_S << "Updating SystemState model took " << (IceUtil::Time::now() - start).toMilliSecondsDouble() << " ms";

        std::string dotString = GenerateDependencyGraph(dependenciesMap);
        std::ofstream f("/tmp/dependencygraph.dot");
        f << dotString;
    }

    void ArmarXManagerModel::upsertManagerDetails(const ArmarXManagerItem::ManagerData& data)
    {
        CHECK_QT_THREAD(this);
        bool found = false;
        // iterate over all manager items
        for (int i = 0; i < this->rowCount(); i++)
        {
            ArmarXManagerItem* item = this->getItem(i);
            if (item->getName() == data.name)
            {
                found = true;
                break;
            }
        }
        if (!found)
        {
            ARMARX_DEBUG << "Inserting " << data.name.toStdString();
            this->appendRow(new ArmarXManagerItem(data.name));
        }
        updateManagerDetails({{data.name, data}}, false);
    }


    void ArmarXManagerModel::moveSelectionTo(QItemSelectionModel* selectionModel,
            ArmarXManagerModel* destinationModel)
    {
        CHECK_QT_THREAD(this);
        swapSelection(this, destinationModel, selectionModel);
    }


    void ArmarXManagerModel::takeSelectionFrom(QItemSelectionModel* selectionModel,
            ArmarXManagerModel* sourceModel)
    {
        CHECK_QT_THREAD(this);
        swapSelection(sourceModel, this, selectionModel);
    }


    void ArmarXManagerModel::swapSelection(ArmarXManagerModel* sourceModel,
                                           ArmarXManagerModel* destinationModel,
                                           QItemSelectionModel* selectionModel)
    {
        CHECK_QT_THREAD(this);
        QList<QPersistentModelIndex> persistentSelectionModelList =
            getPersistentModelIndex(selectionModel);

        // swap selections
        QList<QPersistentModelIndex>::Iterator persistentModelIndexIter;
        persistentModelIndexIter = persistentSelectionModelList.begin();

        while (persistentModelIndexIter != persistentSelectionModelList.end())
        {
            destinationModel->insertRow(destinationModel->rowCount(),
                                        sourceModel->takeRow(
                                            persistentModelIndexIter->row()));

            ++persistentModelIndexIter;
        }
    }


    void ArmarXManagerModel::deleteSelection(QItemSelectionModel* selectionModel)
    {
        CHECK_QT_THREAD(this);
        QList<QPersistentModelIndex> persistentSelectionModelList =
            getPersistentModelIndex(selectionModel);

        QList<QPersistentModelIndex>::Iterator persistentModelIndexIter;
        persistentModelIndexIter = persistentSelectionModelList.begin();

        while (persistentModelIndexIter != persistentSelectionModelList.end())
        {
            takeRow(persistentModelIndexIter->row());

            ++persistentModelIndexIter;
        }
    }


    QList<QPersistentModelIndex> ArmarXManagerModel::getPersistentModelIndex(
        QItemSelectionModel* selectionModel)
    {
        CHECK_QT_THREAD(this);
        QModelIndexList selection = selectionModel->selectedRows();

        // populate persistent selection model list
        QList<QPersistentModelIndex> persistentModelIndex;
        QModelIndexList::Iterator modelIndexIter;
        modelIndexIter = selection.begin();

        while (modelIndexIter != selection.end())
        {
            persistentModelIndex.append(*modelIndexIter);

            ++modelIndexIter;
        }

        return persistentModelIndex;
    }


    QVariant ArmarXManagerModel::headerData(int section, Qt::Orientation orientation, int role) const
    {
        CHECK_QT_THREAD(this);
        if (role == Qt::DisplayRole)
        {
            if (orientation == Qt::Horizontal)
            {
                switch (section)
                {
                    case 0:
                        return "Name";

                    case 1:
                        return "Object state";
                }
            }
        }

        return QVariant();
    }

    std::mutex& ArmarXManagerModel::getMutex()
    {
        return mutex;
    }


    void ArmarXManagerModel::updateItem(QStandardItem* item)
    {
        CHECK_QT_THREAD(this);
        ArmarXManagerItem* managerItem = dynamic_cast<ArmarXManagerItem*>(item);

        if (managerItem)
        {
            // std::unique_lock lock(getMutex());

            managerItem->setName(item->text());
        }
    }


    QStringList ArmarXManagerModel::toStringList() const
    {
        CHECK_QT_THREAD(this);
        QStringList managerList;

        for (int i = 0; i < this->rowCount(); i++)
        {
            managerList.append(this->getItem(i)->getName());
        }

        return managerList;
    }

    bool ArmarXManagerModel::empty() const
    {
        return rowCount() == 0;
    }


    void ArmarXManagerModel::populate(const QStringList& managerList)
    {
        CHECK_QT_THREAD(this);
        // std::unique_lock lock(getMutex());

        this->clear();

        for (int i = 0; i < managerList.size(); i++)
        {
            this->appendRow(new ArmarXManagerItem(managerList.at(i)));
        }
    }


    ArmarXManagerModel* ArmarXManagerModel::clone()
    {
        CHECK_QT_THREAD(this);
        ArmarXManagerModel* modelCopy = new ArmarXManagerModel();

        std::unique_lock lock(getMutex());

        for (int i = 0;  i < rowCount(); i++)
        {
            ArmarXManagerItem* itemClone = new ArmarXManagerItem(item(i)->text());

            modelCopy->appendRow(itemClone);
        }

        return modelCopy;
    }


    void ArmarXManagerModel::copyFrom(ArmarXManagerModel* source)
    {
        CHECK_QT_THREAD(this);
        // std::unique_lock lock(getMutex());
        // std::unique_lock lockSource(source->getMutex());

        this->clear();

        for (int i = 0;  i < source->rowCount(); i++)
        {
            ArmarXManagerItem* itemClone = new ArmarXManagerItem(source->item(i)->text());

            appendRow(itemClone);
        }
    }


    void ArmarXManagerModel::setIceManager(IceManagerPtr iceManager)
    {
        this->iceManager = iceManager;
    }


    IceManagerPtr ArmarXManagerModel::getIceManager() const
    {
        return iceManager;
    }


    QBrush ArmarXManagerModel::getBrush(armarx::ManagedIceObjectState state) const
    {
        QBrush brush;
        brush.setStyle(Qt::SolidPattern);

        switch (state)
        {
            case armarx::eManagedIceObjectCreated:
                brush.setColor(QColor(255, 196, 106));
                break;

            case armarx::eManagedIceObjectInitializing:
                brush.setColor(QColor(255, 255, 196));
                break;

            case armarx::eManagedIceObjectInitialized:
                brush.setColor(QColor(255, 255, 0));
                break;

            case armarx::eManagedIceObjectInitializationFailed:
                brush.setColor(QColor(255, 16, 16));
                break;

            case armarx::eManagedIceObjectStarting:
                brush.setColor(QColor(128, 196, 255));
                break;

            case armarx::eManagedIceObjectStarted:
                brush.setColor(QColor(128, 255, 128));
                break;

            case armarx::eManagedIceObjectStartingFailed:
                brush.setColor(QColor(255, 64, 64));
                break;

            case armarx::eManagedIceObjectExiting:
                brush.setColor(QColor(225, 225, 225));
                break;

            case armarx::eManagedIceObjectExited:
                brush.setColor(QColor(196, 196, 196));
                break;
        }

        return brush;
    }

    QStandardItem* ArmarXManagerModel::findItem(const QString& name, QStandardItem* item)
    {
        for (int i = 0; i < item->rowCount(); ++i)
        {
            QStandardItem* curItem = item->child(i);
            if (curItem->text() == name)
            {
                return curItem;
            }
        }
        return NULL;
    }

    QStandardItem* ArmarXManagerModel::findItemByUserData(Qt::ItemDataRole role, const QVariant& data, QStandardItem* item)
    {
        for (int i = 0; i < item->rowCount(); ++i)
        {
            QStandardItem* curItem = item->child(i);
            if (curItem->data(role) == data)
            {
                return curItem;
            }
        }
        return NULL;
    }

    void ArmarXManagerModel::syncStringListChildren(QStandardItem* item, const std::vector<QString>& strings)
    {
        for (int i = item->rowCount() - 1; i >= 0; i--)
        {
            QStandardItem* curItem = item->child(i);
            if (std::find(strings.begin(), strings.end(), curItem->text()) == strings.end())
            {
                item->removeRow(i);
            }
        }
        for (const QString& s : strings)
        {
            auto child = findItem(s, item);
            if (!child)
            {
                auto newChild = new QStandardItem(s);
                newChild->setEditable(false);
                item->appendRow(newChild);
            }
        }
    }

    std::string ArmarXManagerModel::GenerateDependencyGraph(const std::map<std::string, DependencyMap>& dependenciesMap)
    {
        std::stringstream ss;
        ss << "digraph ArmarXDependencyGraph\n"
           "{";
        for (auto& pair : dependenciesMap)
        {
            auto name = pair.first;
            for (auto& pair2 : pair.second)
            {
                auto name2 = pair2.first;
                name = simox::alg::replace_all(name, "-", "_");
                name2 = simox::alg::replace_all(name2, "-", "_");
                name = simox::alg::replace_all(name, ">", "_");
                name2 = simox::alg::replace_all(name2, ">", "_");
                ss << name << " -> " << name2 << ";\n";
            }
        }
        ss << "\n}";
        return ss.str();
    }
}
