/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX::Gui
 * @author     Jan Issac ( jan.issac at gmail dot com)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include "ManagedIceObjectItem.h"

#include <ArmarXCore/core/ArmarXManager.h>
#include <ArmarXCore/core/ManagedIceObject.h>
#include <ArmarXCore/core/ManagedIceObjectRegistryInterface.h>

#include <QObject>
#include <QStandardItem>

#include <mutex>

namespace armarx
{
    class ArmarXManagerItem : public QStandardItem
    {
    public:
        using ObjectMap = QMap<QString, ManagedIceObjectItem>;

        struct ManagerData
        {
            QString name;
            bool online = false;
            ArmarXManagerInterfacePrx proxy;
            ObjectMap objects;
            ObjectPropertyInfos appProperties;
            Ice::ConnectionPtr  connection;
            QString endpointStr;
        };
        using ManagerDataMap = std::map<QString, ManagerData>;

    public:
        explicit ArmarXManagerItem(const QString& name);
        ~ArmarXManagerItem() override { }

        QString getName() const;
        bool isOnline() const;
        ArmarXManagerInterfacePrx getManagerProxy() const;
        Ice::ConnectionPtr getConnection() const;
        QString getEndpointStr() const;
        ObjectMap& getObjects();

        void setName(QString name);
        void setOnline(bool online);
        void setManagerProxy(ArmarXManagerInterfacePrx proxy);
        void setAppProperties(const ObjectPropertyInfos& properties);
        void setConnection(Ice::ConnectionPtr connection);
        void setEndpointStr(const QString& endpointStr);
        const ObjectPropertyInfos& getAppProperties() const;
        /**
         * Clears the item data.
         */
        void clear(bool clearProxy = true);

        /**
         * Returns the access mutex
         *
         * @return Access mutex
         */
        std::mutex& getMutex();

    private:
        ManagerData data;
        std::mutex mutex;
    };
}

