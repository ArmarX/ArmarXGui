/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX::Gui
 * @author     Jan Issac ( jan.issac at gmail dot com )
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <ArmarXGui/gui-plugins/SystemStateMonitorPlugin/ui_SystemStateMonitorWidget.h>
#include "ArmarXManagerModel.h"
#include "ArmarXManagerRepositoryDialog.h"

#include <QTimer>
#include <QWidget>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/InfixFilterModel.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <mutex>

namespace Ui
{
    class SystemStateMonitorWidget;
}

namespace armarx
{

    class SystemStateMonitorFilterModel;
    /**
      \page ArmarXGui-GuiPlugins-SystemStateMonitor SystemStateMonitor
      \brief The SystemStateMonitor allows you to inspect the properties of the running ArmarX components.

      \image html SystemStateMonitor.png
      To inspect a component, press the button at the bottom with the wrench symbol.
      Select any component you wish to inspect from the appearing list.
      Every component that is waiting for dependencies or is currently starting is yellow. If it was successfully started, it will be green. The dependencies of components are shown in the subtree of each component. Available dependencies are green while missing dependencies are red.

      \see SystemStateMonitorWidget
      \see SystemStateMonitorPlugin
      */

    /**
      \class SystemStateMonitorWidget
      \brief The SystemStateMonitor allows you to inspect the properties of the running ArmarX components.
      \see SystemStateMonitorPlugin
      */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        SystemStateMonitorWidget:
        public ArmarXComponentWidgetControllerTemplate<SystemStateMonitorWidget>
    {
        Q_OBJECT

    public:

        explicit SystemStateMonitorWidget();
        ~SystemStateMonitorWidget() override;

        /**
         * View setup
         */
        void setupView();

        /**
         * Model setup
         */
        void setupModel();

        /**
         * Load stored manager models
         */
        void loadSettings(QSettings* settings) override;

        /**
         * Saves the manager models
         */
        void saveSettings(QSettings* settings) override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this.
         */
        static QString GetWidgetName()
        {
            return "Meta.SystemStateMonitor";
        }

        /**
         * @see armarx::Component::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::Component::onConnectComponent()
         */
        void onConnectComponent() override;

        void onDisconnectComponent() override;

        void onExitComponent() override;

        void retrieveManagerObjectsState(ArmarXManagerItem* item);

        bool retrieveManagerObjectsState(ArmarXManagerInterfacePrx prx, ArmarXManagerItem::ObjectMap& objectStates);
        bool retrieveManagerData(ArmarXManagerInterfacePrx prx, ArmarXManagerItem::ManagerData& managerData);

    signals:

        void updateManagerStatesSignal(const  ArmarXManagerItem::ManagerDataMap&);
    public slots:
        void  updateManagerObjectsState();
        /**
         * Updates the states of the managers stated in the monitored list
         */
        void updateManagerStates(const  ArmarXManagerItem::ManagerDataMap& managerData);

        /**
         * Accept config changes
         */
        void acceptConfig();

        /**
         * Retrieves the online managers
         */
        void retrieveOnlineManagers();

        /**
         * Opens the config dialog
         */
        void openManagerRepositoryDialog();
        void addArmarXManagers(QStringList managerNames);

    private slots:
        /**
         * Prefills the widgets view with all currently existing components
         */
        void prefillView();
        void addArmarXManagerEntries(ManagerPrxMap managers);
        void expandFilterSelection(QString filterStr);
        void delayedFilterExpansion();
        void on_btnProblematicOnly_toggled(bool checked);

    private:

        void fillLegendLayout(QHBoxLayout* layout, ArmarXManagerModel& model) const;

    private:

        /**
         * Active monitored managers model
         */
        ArmarXManagerModel* monitoredManagerModel;

        /**
         * Passive manager repository
         */
        ArmarXManagerModel* managerRepositoryModel;

        std::mutex managerPrxMapMutex;
        ManagerPrxMap currentManagerPrxMap;
        QTimer filterExpansionTimer;
        /**
         * Main widget
         */
        Ui::SystemStateMonitorWidget ui;

        /**
         * Config dialog
         */
        ArmarXManagerRepositoryDialog* managerRepositoryDialog;
        QPointer<InfixFilterModel> filterModel;

        /**
         * Monitored managers state update periodic task
         */
        PeriodicTask<SystemStateMonitorWidget>::pointer_type stateUpdateTask;

        /**
         * Fetches the online managers
         */
        QStringList fetchOnlineManagers();



        // ArmarXWidgetController interface
    public:
        static QIcon GetWidgetIcon()
        {
            return QIcon(":icons/activity_monitor.png");
        }
    };

    class SystemStateMonitorFilterModel : public InfixFilterModel
    {
    public:
        SystemStateMonitorFilterModel(QObject* parent) :
            InfixFilterModel(parent) {}

        // QSortFilterProxyModel interface
        bool getHideResolvedComponents() const;
        void setHideResolvedComponents(bool value);

    protected:
        bool filterAcceptsRow(int source_row, const QModelIndex& source_parent) const override
        {
            if (!InfixFilterModel::filterAcceptsRow(source_row, source_parent))
            {
                return false;
            }
            QModelIndex index0 = sourceModel()->index(source_row, 0, source_parent);
            if (hideResolvedComponents)
            {
                auto variant = index0.data((int)ArmarXManagerModel::UserDataRoles::ComponentStarted);
                if (
                    variant.isValid())
                {
                    //                ARMARX_INFO << "Checking entry: " << index0.data().toString() << " val: " << variant.toBool() << " param: " << hideResolvedComponents;
                    if (variant.toBool())
                    {
                        ARMARX_VERBOSE << "Hiding entry: " << index0.data().toString();
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }


            return true;
        }
        bool hideResolvedComponents = false;

    };

}

