/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX::Gui
 * @author     Jan Issac ( jan.issac at gmail dot com)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SystemStateMonitorWidget.h"

#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/InfixFilterModel.h>
#include <ArmarXGui/gui-plugins/SystemStateMonitorPlugin/ui_ArmarXManagerRepositoryDialog.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/QtUtil.h>

#include <ArmarXCore/core/IceGridAdmin.h>
#include <ArmarXCore/interface/core/ArmarXManagerInterface.h>
#include <ArmarXCore/core/CoreObjectFactories.h>

#include <QStyledItemDelegate>
#include <QApplication>
#include <QLineEdit>
#include <QVector>

#include <string>
#include <vector>


namespace armarx
{
    class PropertyEditingDelegate : public QStyledItemDelegate
    {
    public:
        PropertyEditingDelegate(armarx::ManagerPrxMap* managerPrxMap) : managerPrxMap(managerPrxMap)
        {
        }

        // QAbstractItemDelegate interface
        QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const override
        {
            return new QLineEdit(parent);
        }
        void setEditorData(QWidget* editor, const QModelIndex& index) const override
        {
            QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
            if (lineEdit)
            {
                QString propValue = index.data().toString();
                propValue.remove(0, propValue.indexOf(":") + 2);
                QString propName = index.data().toString();

                propName.truncate(propName.indexOf(":") - 1);
                lineEdit->setText(propValue);
                lineEdit->setProperty("propertyName", propName);
            }
        }
        void setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const override
        {
            QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
            if (lineEdit)
            {
                model->setData(index, lineEdit->property("propertyName").toString() +  " : " + lineEdit->text());
                auto it = managerPrxMap->find(index.parent().parent().parent().data().toString());
                if (it != managerPrxMap->end())
                {
                    armarx::ArmarXManagerInterfacePrx prx = it->second;
                    prx->getPropertiesAdmin()->ice_collocationOptimized(false)->setProperties({{
                            lineEdit->property("propertyName").toString().toStdString(),
                            lineEdit->text().toStdString()
                        }
                    });
                }
            }
        }
    private:
        armarx::ManagerPrxMap* managerPrxMap;
    };





    SystemStateMonitorWidget::SystemStateMonitorWidget()
    {
        setupModel();
        setupView();
        qRegisterMetaType<StateUpdateMap>("StateUpdateMap");
        qRegisterMetaType<ArmarXManagerItem::ManagerDataMap>("ArmarXManagerItem::ManagerDataMap");
        qRegisterMetaType<ArmarXManagerItem::ManagerData>("ArmarXManagerItem::ManagerData");
        qRegisterMetaType<ManagerPrxMap>("ManagerPrxMap");


        filterExpansionTimer.setSingleShot(true);
        connect(&filterExpansionTimer, SIGNAL(timeout()), this, SLOT(delayedFilterExpansion()));
        connect(ui.btnProblematicOnly, SIGNAL(toggled(bool)), this, SLOT(on_btnProblematicOnly_toggled(bool)), Qt::UniqueConnection);

        fillLegendLayout(ui.colorLegendLayout, *monitoredManagerModel);
    }


    SystemStateMonitorWidget::~SystemStateMonitorWidget()
    {
        //    stateUpdateTimer->stop();
        if (stateUpdateTask)
        {
            stateUpdateTask->stop();
        }

        delete managerRepositoryModel;
        delete monitoredManagerModel;
        //    delete stateUpdateTimer;
    }

    void SystemStateMonitorWidget::setupModel()
    {
        managerRepositoryModel = new ArmarXManagerModel();
        monitoredManagerModel = new ArmarXManagerModel();
    }


    void SystemStateMonitorWidget::setupView()
    {
        ui.setupUi(getWidget());

        managerRepositoryDialog = new ArmarXManagerRepositoryDialog(
            managerRepositoryModel,
            monitoredManagerModel,
            getWidget());
        managerRepositoryDialog->setModal(true);
        connect(managerRepositoryDialog, SIGNAL(accepted()), this, SLOT(acceptConfig()));
        connect(managerRepositoryDialog, SIGNAL(requestedManagerScan()), this, SLOT(retrieveOnlineManagers()));
        ui.monitoredManagersTree->setItemDelegate(new PropertyEditingDelegate(&currentManagerPrxMap));
        filterModel = new InfixFilterModel(this);
        filterModel->addCustomFilter([this](QAbstractItemModel * model, int source_row, const QModelIndex & source_parent)
        {
            QModelIndex index0 = model->index(source_row, 0, source_parent);
            if (this->ui.btnProblematicOnly->isChecked())
            {
                auto variant = index0.data((int)ArmarXManagerModel::UserDataRoles::ComponentStarted);
                auto variantDependency = index0.data((int)ArmarXManagerModel::UserDataRoles::ResolvedDependency);
                if (
                    variant.isValid())
                {
                    //                ARMARX_INFO << "Checking entry: " << index0.data().toString() << " val: " << variant.toBool() << " param: " << hideResolvedComponents;
                    if (variant.toBool())
                    {
                        //                    ARMARX_INFO << "Resolve: Hiding " << index0.data().toString();
                        return false;
                    }
                }
                if (variantDependency.isValid())
                {
                    if (variantDependency.toBool())
                    {
                        return false;
                    }
                }
                if (!variant.isValid() && !variantDependency.isValid())
                {
                    return false;
                }
                //            ARMARX_INFO << "Showing entry: " << index0.data().toString() << " state: " <<
                //                        (variantDependency.isValid() ? std::to_string(variantDependency.toBool()) : "none")
                //                        << " - " << (variant.isValid() ? std::to_string(variant.toBool()) : "none");
            }

            return true;
        });
        filterModel->setSourceModel(monitoredManagerModel);
        filterModel->setFilterCaseSensitivity(Qt::CaseInsensitive);
        ui.monitoredManagersTree->setModel(filterModel);

        connect(ui.lineEditFilter, SIGNAL(textChanged(QString)), filterModel, SLOT(setFilterFixedString(QString)));
        connect(ui.lineEditFilter, SIGNAL(textChanged(QString)), this, SLOT(expandFilterSelection(QString)), Qt::QueuedConnection);

        connect(ui.configureButton, SIGNAL(clicked()), this, SLOT(openManagerRepositoryDialog()));

        connect(this, SIGNAL(updateManagerStatesSignal(ArmarXManagerItem::ManagerDataMap)), this, SLOT(updateManagerStates(ArmarXManagerItem::ManagerDataMap)));

        //    stateUpdateTimer->start();
    }


    void SystemStateMonitorWidget::loadSettings(QSettings* settings)
    {
        managerRepositoryModel->populate(
            settings->value("ManagerRepository").toStringList());

        monitoredManagerModel->populate(
            settings->value("MonitoredManagers").toStringList());
        ui.monitoredManagersTree->expandToDepth(1);
    }


    void SystemStateMonitorWidget::saveSettings(QSettings* settings)
    {
        settings->setValue("ManagerRepository",
                           managerRepositoryModel->toStringList());

        settings->setValue("MonitoredManagers",
                           monitoredManagerModel->toStringList());
    }


    void SystemStateMonitorWidget::onInitComponent()
    {
        monitoredManagerModel->setIceManager(getIceManager());
        managerRepositoryModel->setIceManager(getIceManager());
    }


    void SystemStateMonitorWidget::onConnectComponent()
    {
        stateUpdateTask = new PeriodicTask<SystemStateMonitorWidget>(this, &SystemStateMonitorWidget::updateManagerObjectsState, 500, false, "SystemStateMonitorUpdate");
        stateUpdateTask->setDelayWarningTolerance(5000);
        stateUpdateTask->start();
        if (monitoredManagerModel->empty()) // only if fresh connect without preconfig
        {
            prefillView();
        }
        else
        {
            std::unique_lock lock(managerPrxMapMutex);
            currentManagerPrxMap = monitoredManagerModel->getManagerProxyMap();
        }
    }

    void SystemStateMonitorWidget::onDisconnectComponent()
    {
        if (stateUpdateTask)
        {
            stateUpdateTask->stop();
        }

    }

    void SystemStateMonitorWidget::onExitComponent()
    {
        if (stateUpdateTask)
        {
            stateUpdateTask->stop();
        }
    }


    void SystemStateMonitorWidget::updateManagerStates(const  ArmarXManagerItem::ManagerDataMap& managerData)
    {
        CHECK_QT_THREAD(getWidget());

        std::unique_lock lock(monitoredManagerModel->getMutex());

        monitoredManagerModel->updateManagerDetails(managerData);
        filterModel->invalidate();
    }

    bool SystemStateMonitorWidget::retrieveManagerData(ArmarXManagerInterfacePrx prx, ArmarXManagerItem::ManagerData& managerData)
    {
        CHECK_NOT_QT_THREAD(getWidget());
        bool result = false;
        try
        {
            managerData.proxy = prx;
            prx = prx->ice_timeout(1000);
            managerData.name = QString::fromStdString(prx->ice_getIdentity().name);
            managerData.appProperties = prx->getApplicationPropertyInfos();
            result &= retrieveManagerObjectsState(prx, managerData.objects);
            try
            {
                managerData.connection = prx->ice_collocationOptimized(false)->ice_getConnection();
                managerData.endpointStr = managerData.connection ? QString::fromStdString(managerData.connection->getEndpoint()->toString()) : "Endpoint: no connection data";
            }

            catch (const IceUtil::Exception& ex)
            {
                //            ARMARX_INFO << deactivateSpam(5, managerData.name.toStdString()) << "Failed to get connection info for " << managerData.name.toStdString();
                managerData.endpointStr = QString::fromStdString("? (" + std::string(ex.ice_id()) + ")");
            }
            managerData.online = true;
        }
        catch (...)
        {
            managerData.online = false;
            result = false;
        }
        return result;
    }

    bool SystemStateMonitorWidget::retrieveManagerObjectsState(ArmarXManagerInterfacePrx prx, ArmarXManagerItem::ObjectMap& objectStates)
    {
        CHECK_NOT_QT_THREAD(getWidget());
        // get proxy if not set
        // update existence state
        try
        {
            prx->ice_timeout(200)->ice_ping();
            //        item->setOnline(true);
        }
        catch (...)
        {
            return false;
        }

        // actual retrieval
        Ice::StringSeq objectNames = prx->getManagedObjectNames();

        for (auto& objectName : objectNames)
        {
            ManagedIceObjectItem objEntry;
            objEntry.name = objectName.c_str();
            objEntry.state = prx->getObjectState(objectName);
            objEntry.connectivity = prx->getObjectConnectivity(objectName);
            objEntry.properties = prx->getObjectPropertyInfos(objectName);
            try
            {
                objEntry.metaInfoMap = prx->getMetaInfo(objectName);
            }
            catch (...)
            {
                ARMARX_INFO << deactivateSpam(1000, objectName) << "Failed to get meta info for " << objectName;
            }
            objectStates[objEntry.name] = (objEntry);
        }

        return true;
    }

    void SystemStateMonitorWidget::updateManagerObjectsState()
    {

        //    std::unique_lock lock(monitoredManagerModel->getMutex());
        //    StateUpdateMap stateMap;
        ArmarXManagerItem::ManagerDataMap managerDataMap;
        {
            decltype(currentManagerPrxMap) proxies;
            {
                std::unique_lock lock(managerPrxMapMutex);
                proxies = currentManagerPrxMap;
            }
            IceUtil::Time start = IceUtil::Time::now();
            for (auto it = proxies.begin(); it != proxies.end(); it++)
            {
                ArmarXManagerItem::ObjectMap stateMap;
                bool online = retrieveManagerObjectsState(it->second, stateMap);
                managerDataMap[it->first].online = online;
                managerDataMap[it->first].name = it->first;
                managerDataMap[it->first].objects = stateMap;
                if (online)
                {
                    managerDataMap[it->first].appProperties = it->second->getApplicationPropertyInfos();
                }
                retrieveManagerData(it->second, managerDataMap[it->first]);
            }
            ARMARX_DEBUG << "update duration: " << (IceUtil::Time::now() - start).toMilliSecondsDouble();
        }

        emit updateManagerStatesSignal(managerDataMap);
    }

    void SystemStateMonitorWidget::acceptConfig()
    {
        this->managerRepositoryModel->copyFrom(
            managerRepositoryDialog->getManagerRepositoryModel());

        this->monitoredManagerModel->copyFrom(
            managerRepositoryDialog->getMonitoredManagersModel());
        ui.monitoredManagersTree->expandToDepth(1);
        std::unique_lock lock(managerPrxMapMutex);
        currentManagerPrxMap = monitoredManagerModel->getManagerProxyMap();
    }

    QStringList SystemStateMonitorWidget::fetchOnlineManagers()
    {
        enableMainWidgetAsync(false);
        auto admin = getIceManager()->getIceGridSession()->getAdmin();
        ARMARX_INFO << "Getting managers";
        IceGrid::ObjectInfoSeq objects = admin->getAllObjectInfos("*Manager");
        ARMARX_INFO << "Got new managers";
        IceGrid::ObjectInfoSeq::iterator iter = objects.begin();
        IceGrid::ObjectInfoSeq result;

        while (iter != objects.end())
        {
            Ice::ObjectPrx current = iter->proxy;

            ArmarXManagerInterfacePrx object;

            // if objects are hanging we might get connection refused
            try
            {
                object = ArmarXManagerInterfacePrx::checkedCast(current->ice_timeout(60));
            }
            catch (...)
            {
                ARMARX_INFO << current->ice_getIdentity().name << ": " << GetHandledExceptionString();
                // remove dead objects
                admin->removeObject(current->ice_getIdentity());
            }
            qApp->processEvents();

            if (object)
            {
                result.push_back(*iter);
            }

            ++iter;
        }
        ARMARX_INFO << "Iterated through online managers";
        enableMainWidgetAsync(true);

        QStringList managers;
        for (const IceGrid::ObjectInfo& info : result)
        {
            managers.append(info.proxy->ice_getIdentity().name.c_str());
        }

        return managers;
    }


    void SystemStateMonitorWidget::retrieveOnlineManagers()
    {
        managerRepositoryDialog->addOnlineManagers(fetchOnlineManagers());
    }


    void SystemStateMonitorWidget::openManagerRepositoryDialog()
    {
        managerRepositoryDialog->getManagerRepositoryModel()->copyFrom(
            managerRepositoryModel);

        managerRepositoryDialog->getMonitoredManagersModel()->copyFrom(
            monitoredManagerModel);

        retrieveOnlineManagers();

        managerRepositoryDialog->show();
    }


    void SystemStateMonitorWidget::prefillView()
    {
        managerRepositoryModel->clear();
        monitoredManagerModel->clear();

        QStringList managers = fetchOnlineManagers();
        //    enableMainWidgetAsync(false);
        //    ARMARX_INFO << "Got managers";
        //    QMetaObject::invokeMethod(this, "addArmarXManagers", Q_ARG(QStringList, managers));
        addArmarXManagers(managers);
        QMetaObject::invokeMethod(ui.monitoredManagersTree, "expandToDepth", Q_ARG(int, 0));
        std::unique_lock lock(managerPrxMapMutex);
        currentManagerPrxMap = monitoredManagerModel->getManagerProxyMap(managers);

    }


    void SystemStateMonitorWidget::addArmarXManagerEntries(ManagerPrxMap  managers)
    {
        CHECK_QT_THREAD(getWidget());

        ARMARX_INFO << "Adding managers";
        for (auto& pair : managers)
        {
            ArmarXManagerItem* item = new ArmarXManagerItem(pair.first);
            item->setManagerProxy(pair.second);
            monitoredManagerModel->appendRow(item);
        }
        ARMARX_INFO << "epanding managers";
        ui.monitoredManagersTree->expandToDepth(1);

    }

    void SystemStateMonitorWidget::addArmarXManagers(QStringList managerNames)
    {
        //    ARMARX_IMPORTANT << this->rowCount();
        for (auto& name : managerNames)
        {
            try
            {
                ArmarXManagerItem::ManagerData data;
                data.name = name;
                ARMARX_DEBUG << name.toStdString();
                auto proxy = getIceManager()->getProxy<ArmarXManagerInterfacePrx>(name.toStdString());
                retrieveManagerData(proxy, data);
                QMetaObject::invokeMethod(monitoredManagerModel, "upsertManagerDetails", Q_ARG(ArmarXManagerItem::ManagerData, data));
                ARMARX_DEBUG << name.toStdString() << " done";
            }
            catch (...)
            {
                armarx::handleExceptions();
            }
        }
    }

    void SystemStateMonitorWidget::expandFilterSelection(QString filterStr)
    {
        ARMARX_DEBUG_S << VAROUT(filterStr);
        if (filterStr.length() == 0)
        {
            ui.monitoredManagersTree->collapseAll();
            //        ui.monitoredManagersTree->expandToDepth(1);
        }
        else
        {
            filterExpansionTimer.start(500);
        }
    }

    void SystemStateMonitorWidget::delayedFilterExpansion()
    {
        CHECK_QT_THREAD(getWidget());

        InfixFilterModel::ExpandFilterResults(ui.monitoredManagersTree);
    }

    void armarx::SystemStateMonitorWidget::on_btnProblematicOnly_toggled(bool checked)
    {
        CHECK_QT_THREAD(getWidget());

        //    filterModel->setHideResolvedComponents(checked);
        filterModel->invalidate();
        InfixFilterModel::ExpandFilterResults(ui.monitoredManagersTree);
    }

    void SystemStateMonitorWidget::fillLegendLayout(QHBoxLayout* layout, ArmarXManagerModel& model) const
    {
        auto addLegendEntry = [layout, &model](ManagedIceObjectState state, const QString& text)
        {
            QLabel* label = new QLabel(text);
            {
                QBrush brush = model.getBrush(state);
                QPalette p = label->palette();
                p.setColor(label->backgroundRole(), brush.color());
                label->setPalette(p);
                label->setAutoFillBackground(true);
            }
            {
                QFont font = label->font();
                font.setPointSize(10);
                label->setFont(font);
            }
            layout->addWidget(label);
        };

        addLegendEntry(armarx::eManagedIceObjectCreated, "Created");
        addLegendEntry(armarx::eManagedIceObjectInitializing, "Initializing");
        addLegendEntry(armarx::eManagedIceObjectInitialized, "Initialized");
        addLegendEntry(armarx::eManagedIceObjectInitializationFailed, "Initialization Failed");
        addLegendEntry(armarx::eManagedIceObjectStarting, "Connecting");
        addLegendEntry(armarx::eManagedIceObjectStarted, "Connected");
        addLegendEntry(armarx::eManagedIceObjectStartingFailed, "Connecting Failed");
        addLegendEntry(armarx::eManagedIceObjectExiting, "Exiting");
        addLegendEntry(armarx::eManagedIceObjectExited, "Exited");
    }

    bool SystemStateMonitorFilterModel::getHideResolvedComponents() const
    {
        return hideResolvedComponents;
    }

    void SystemStateMonitorFilterModel::setHideResolvedComponents(bool value)
    {
        hideResolvedComponents = value;
    }
}
