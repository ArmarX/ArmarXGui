/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Adil Orhan ( ubdnw at student dot kit dot edu)
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <QAbstractItemModel>

#include <ArmarXCore/core/services/tasks/RunningTask.h>

#include <mutex>

namespace armarx
{

    class RunningTaskModel : public QAbstractTableModel
    {
        Q_OBJECT
    public:
        explicit RunningTaskModel(QObject* parent = 0);
        ~RunningTaskModel() override;

        // inherited from QAbstractItemModel
        //        QModelIndex index(int row, int column, const QModelIndex &parent) const;
        int rowCount(const QModelIndex& parent = QModelIndex()) const override;
        int columnCount(const QModelIndex& parent =  QModelIndex()) const override;
        QVariant data(const QModelIndex& index, int role) const override;
        QVariant headerData(int section, Qt::Orientation orientation, int role) const override;

        void setRunningTaskListData(const RunningTaskList& runningTaskList);
    signals:

    public slots:

    private:
        void run();

        RunningTaskList runningTaskList;

        mutable std::mutex taskListsMutex;
    };

}

