/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "ThreadViewerModel.h"

namespace armarx
{
    ThreadViewerModel::ThreadViewerModel(QObject* parent) :
        QAbstractTableModel(parent)
    {

    }

    ThreadViewerModel::~ThreadViewerModel()
    {
    }



    int ThreadViewerModel::rowCount(const QModelIndex& parent) const
    {
        return periodicTaskList.size();
    }

    int ThreadViewerModel::columnCount(const QModelIndex& parent) const
    {
        return 8;
    }

    QVariant ThreadViewerModel::data(const QModelIndex& index, int role) const
    {
        const unsigned int& row = index.row();
        const unsigned int& column = index.column();
        std::unique_lock lock(taskListsMutex);

        if (row >= periodicTaskList.size())
        {
            return QVariant();
        }

        const PeriodicTaskIceBase& entry = periodicTaskList.at(row);

        switch (role)
        {
            case Qt::DisplayRole:
            case Qt::ToolTipRole:
                switch (column)
                {
                    case 0:
                        return QString::fromStdString(entry.name);
                        break;

                    case 1:
                        if (entry.running)
                        {
                            return "Yes";
                        }
                        else
                        {
                            return "No";
                        }

                        break;

                    case 2:
                        return entry.intervalMs;
                        break;

                    case 3:
                        return *entry.workloadList.rbegin() * 100.0f;
                        break;

                    case 4:
                        return QString::number(0.001 * entry.lastCycleDuration, 'f', 1);
                        break;

                    case 5:
                    {
                        QString date = QString::fromStdString(IceUtil::Time::microSeconds(entry.startTime).toDateTime());
                        return date.remove(0, date.indexOf(' ') + 1); // remove date
                    }
                    break;

                    case 6:
                    {
                        QString date = QString::fromStdString(IceUtil::Time::microSeconds(entry.lastCycleStartTime).toDateTime());
                        return date.remove(0, date.indexOf(' ') + 1); // remove date
                    }
                    break;

                    case 7:
                    {
                        return QString::number(entry.threadId);
                    }
                    break;

                    default:
                        return QVariant();


                }

                break;

            default:

                return QVariant();
        }

        return QVariant();
    }

    QVariant ThreadViewerModel::headerData(int section, Qt::Orientation orientation, int role) const
    {
        switch (role)
        {
            case Qt::DisplayRole:
            case Qt::ToolTipRole:
                if (orientation == Qt::Horizontal)
                {
                    switch (section)
                    {
                        case 0:
                            return "ThreadName";

                        case 1:
                            return "Running?";

                        case 2:
                            return "Interval (ms)";

                        case 3:
                            return "Workload";

                        case 4:
                            return "Last cycle duration (ms)";

                        case 5:
                            return "Starttime";

                        case 6:
                            return "Last cycle start";

                        case 7:
                            return "Thread Id";

                    }
                }

                break;
        }

        return QAbstractTableModel::headerData(section, orientation, role);
    }

    void ThreadViewerModel::setPeriodicTaskListData(const PeriodicTaskList& periodicTaskList)
    {
        beginResetModel();

        std::unique_lock lock(taskListsMutex);
        this->periodicTaskList = periodicTaskList;
        endResetModel();
        emit dataChanged(index(0, 0), index(rowCount(), columnCount()));
    }
}
