/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Adil Orhan ( ubdnw at student dot kit dot edu)
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "RunningTaskModel.h"

namespace armarx
{
    RunningTaskModel::RunningTaskModel(QObject* parent) :
        QAbstractTableModel(parent)
    {

    }

    RunningTaskModel::~RunningTaskModel()
    {
    }



    int RunningTaskModel::rowCount(const QModelIndex& parent) const
    {
        return runningTaskList.size();
    }

    int RunningTaskModel::columnCount(const QModelIndex& parent) const
    {
        return 6;
    }

    QVariant RunningTaskModel::data(const QModelIndex& index, int role) const
    {
        const unsigned int& row = index.row();
        const unsigned int& column = index.column();
        std::unique_lock lock(taskListsMutex);

        if (row >= runningTaskList.size())
        {
            return QVariant();
        }

        const RunningTaskIceBase& entry = runningTaskList.at(row);

        switch (role)
        {
            case Qt::DisplayRole:
            case Qt::ToolTipRole:
                switch (column)
                {
                    case 0:
                        return QString::fromStdString(entry.name);
                        break;

                    case 1:
                        if (entry.running)
                        {
                            return "Yes";
                        }
                        else
                        {
                            return "No";
                        }

                        break;

                    case 2:
                        return entry.workload * 100.0f;
                        break;

                    case 3:
                    {
                        QString date = QString::fromStdString(IceUtil::Time::microSeconds(entry.startTime).toDateTime());
                        return date.remove(0, date.indexOf(' ') + 1); // remove date
                    }
                    break;

                    case 4:
                    {
                        QString date = QString::fromStdString(IceUtil::Time::microSeconds(entry.lastFeedbackTime).toDateTime());
                        return date.remove(0, date.indexOf(' ') + 1); // remove date
                    }
                    break;

                    case 5:
                    {
                        return QString::number(entry.threadId);
                    }
                    break;


                    default:
                        return QVariant();
                }

                break;

            default:

                return QVariant();
        }

        return QVariant();
    }

    QVariant RunningTaskModel::headerData(int section, Qt::Orientation orientation, int role) const
    {
        switch (role)
        {
            case Qt::DisplayRole:
            case Qt::ToolTipRole:
                if (orientation == Qt::Horizontal)
                {
                    switch (section)
                    {
                        case 0:
                            return "ThreadName";

                        case 1:
                            return "Running?";

                        case 2:
                            return "Workload";

                        case 3:
                            return "Starttime";

                        case 4:
                            return "lastFeedbackTime";

                        case 5:
                            return "Thread Id";

                    }
                }

                break;
        }

        return QAbstractTableModel::headerData(section, orientation, role);
    }

    void RunningTaskModel::setRunningTaskListData(const RunningTaskList& runningTaskList)
    {
        beginResetModel();

        std::unique_lock lock(taskListsMutex);
        this->runningTaskList = runningTaskList;
        endResetModel();
        emit dataChanged(index(0, 0), index(rowCount(), columnCount()));
    }
}
