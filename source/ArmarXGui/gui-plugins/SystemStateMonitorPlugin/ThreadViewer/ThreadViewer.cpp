/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "ThreadViewer.h"
#include <ArmarXGui/gui-plugins/SystemStateMonitorPlugin/ui_ThreadViewer.h>
#include "ThreadViewerModel.h"
#include "RunningTaskModel.h"
#include "ProgressbarDelegate.h"
#include <ArmarXCore/core/IceGridAdmin.h>

#include <ArmarXCore/core/services/tasks/ThreadList.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>


namespace armarx
{
    ThreadViewer::ThreadViewer() :
        periodicTaskModel(0),
        runningTaskModel(0),
        ui(new Ui::ThreadViewer)

    {
        ui->setupUi(getWidget());

        periodicTaskModel = new ThreadViewerModel(getWidget());

        ui->threadListView->setModel(periodicTaskModel);
        ui->threadListView->setItemDelegateForColumn(3, new ProgressbarDelegate());

        ui->threadListView->setColumnWidth(0, 150);
        ui->threadListView->setColumnWidth(1, 30);
        ui->threadListView->setColumnWidth(2, 50);
        ui->threadListView->setColumnWidth(3, 90);
        ui->threadListView->setColumnWidth(4, 100);
        ui->threadListView->setColumnWidth(5, 100);
        ui->threadListView->setColumnWidth(6, 80);
        ui->threadListView->setColumnWidth(7, 70);
        ui->threadListView->horizontalHeader()->setResizeMode(QHeaderView::Stretch);

        runningTaskModel = new RunningTaskModel(getWidget());

        ui->runningTaskView->setModel(runningTaskModel);
        ui->runningTaskView->setItemDelegateForColumn(2, new ProgressbarDelegate());

        ui->runningTaskView->setColumnWidth(0, 150);
        ui->runningTaskView->setColumnWidth(1, 30);
        ui->runningTaskView->setColumnWidth(2, 90);
        ui->runningTaskView->setColumnWidth(3, 100);
        ui->runningTaskView->setColumnWidth(4, 100);
        ui->runningTaskView->setColumnWidth(5, 70);
        ui->runningTaskView->horizontalHeader()->setResizeMode(QHeaderView::Stretch);

        QFont font;
        font.setPointSize(8);
        ui->threadListView->setFont(font);
        ui->runningTaskView->setFont(font);

        connect(this, SIGNAL(threadManagerListUpdated(QStringList)), this, SLOT(updateThreadManagerList(QStringList)));
        connect(this, SIGNAL(cpuUsageValueUpdated(int)), this, SLOT(cpuUsageValueSet(int)));
        connect(ui->cbThreadListManager, SIGNAL(currentIndexChanged(QString)), this, SLOT(managedSelectionChanged(QString)));
        connect(ui->btnRefresh, SIGNAL(clicked()), this, SLOT(triggerThreadManagerListUpdate()));
    }

    ThreadViewer::~ThreadViewer()
    {
        delete ui;
    }

    void ThreadViewer::loadSettings(QSettings* settings)
    {
    }

    void ThreadViewer::saveSettings(QSettings* settings)
    {
    }

    void ThreadViewer::onInitComponent()
    {

    }



    void ThreadViewer::onConnectComponent()
    {

        retrieveThreadManagerList();
        periodicTask = new  PeriodicTask<ThreadViewer>(this, &ThreadViewer::runThreadManagerUpdate, 500);
        periodicTask->start();
    }

    void ThreadViewer::onExitComponent()
    {
        if (periodicTask)
        {
            periodicTask->stop();
        }

        if (managerUpdateTask)
        {
            managerUpdateTask->stop();
        }
    }
    void ThreadViewer::triggerThreadManagerListUpdate()
    {
        if (managerUpdateTask && managerUpdateTask->isRunning())
        {
            return;
        }

        ui->btnRefresh->setEnabled(false);
        ui->btnRefresh->setToolTip("Refreshing manager list...please wait");
        managerUpdateTask = new RunningTask<ThreadViewer>(this, &ThreadViewer::retrieveThreadManagerList);
        managerUpdateTask->start();
    }


    void ThreadViewer::retrieveThreadManagerList()
    {
        std::vector<std::string> managerList;
        QStringList qManagerList;

        try
        {
            if (getIceManager() && getIceManager()->getIceGridSession())
                managerList = getIceManager()->getIceGridSession()
                              ->getRegisteredObjectNames< ThreadListInterfacePrx >("*ThreadList");

            for (unsigned int i = 0; i < managerList.size(); ++i)
            {
                qManagerList << QString::fromStdString(managerList.at(i));
            }
        }
        catch (...)
        {
            emit threadManagerListUpdated(qManagerList);
            throw;
        }

        emit threadManagerListUpdated(qManagerList);
    }

    void ThreadViewer::updateThreadManagerList(QStringList list)
    {
        QString lastSelectionText = ui->cbThreadListManager->currentText();
        ui->cbThreadListManager->clear();
        ui->cbThreadListManager->addItems(list);
        int index = ui->cbThreadListManager->findText(lastSelectionText);

        if (index > 0)
        {
            ui->cbThreadListManager->setCurrentIndex(index);
        }

        ui->btnRefresh->setEnabled(true);
        ui->btnRefresh->setToolTip("Refresh the thread manager list");

    }

    void ThreadViewer::managedSelectionChanged(QString selectedString)
    {
        if (selectedString.size() == 0)
        {
            return;
        }

        std::unique_lock lock(proxyMutex);
        threadManagerProxy = getProxy<ThreadListInterfacePrx>(selectedString.toStdString());
        ARMARX_VERBOSE << "new proxy for ThreadViewerModel set: " << selectedString;
    }

    void ThreadViewer::cpuUsageValueSet(int value)
    {

        ui->cpuUsageProgressBar->setValue(value);
    }

    void ThreadViewer::runThreadManagerUpdate()
    {
        std::unique_lock lock(proxyMutex);

        if (!threadManagerProxy)
        {
            return;
        }

        try
        {
            PeriodicTaskList periodicTaskList = threadManagerProxy->getPeriodicTasks();
            periodicTaskModel->setPeriodicTaskListData(periodicTaskList);

            RunningTaskList runningTaskList = threadManagerProxy->getRunningTasks();
            runningTaskModel->setRunningTaskListData(runningTaskList);

            Ice::Double cpuProcTotalTime = threadManagerProxy->getCpuUsage();
            int value = static_cast<int>(cpuProcTotalTime);
            emit cpuUsageValueUpdated(value);
        }
        catch (Ice::NotRegisteredException& e)
        {

        }
        catch (Ice::ObjectAdapterDeactivatedException& e)
        {

        }
        catch (Ice::UnknownLocalException& e)
        {

        }
    }
}
