/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Manfred Kroehnert ( manfred.kroehnert at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <IceGrid/Admin.h>

class QtNodeObserver :
    public IceGrid::NodeObserver
{
    armarx::IceGridViewer* viewer;
public:
    QtNodeObserver(armarx::IceGridViewer* viewer) :
        viewer(viewer)
    {
    }

    void nodeInit(const IceGrid::NodeDynamicInfoSeq& nodes, const ::Ice::Current& = Ice::emptyCurrent) override
    {
        viewer->gridNodeListUpdate(nodes);
    }

    void nodeUp(const IceGrid::NodeDynamicInfo& node, const ::Ice::Current& = Ice::emptyCurrent) override
    {
        viewer->gridNodeListAdd(node);
    }

    void nodeDown(const std::string& name, const ::Ice::Current& = Ice::emptyCurrent) override
    {
        viewer->gridNodeListRemove(QString::fromStdString(name));
    }

    void updateServer(const std::string& node, const IceGrid::ServerDynamicInfo& updatedInfo, const ::Ice::Current& = Ice::emptyCurrent) override
    {
        viewer->updateServerInfo(node, updatedInfo);
    }

    void updateAdapter(const std::string& node, const IceGrid::AdapterDynamicInfo& updatedInfo, const ::Ice::Current& = Ice::emptyCurrent) override
    {
        viewer->updateAdapterInfo(node, updatedInfo);
    }
};

using QtNodeObserverPtr = IceUtil::Handle<QtNodeObserver>;

