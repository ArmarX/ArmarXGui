/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Manfred Kroehnert ( manfred.kroehnert at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <QWidget>
#include <QStringListModel>

#include <IceGrid/Admin.h>

#include <mutex>

namespace Ui
{
    class IceGridViewer;
}

class NodeInfoModel;
class ServerInfoModel;
class AdapterInfoModel;

namespace armarx
{
    class IceGridViewerModel;

    /**
        \page ArmarXGui-GuiPlugins-IceGridViewer IceGridViewer
     * \brief The IceGridViewer shows the current state of a running IceGrid instance.
     * \image html IceGridViewer.png
     * The IceGridViewer shows the current state of a running IceGrid instance.
     * ArmarXGui Documentation \ref IceGridViewer
     * \see SystemStateMonitorPlugin
     */
    /**
     * \class IceGridViewer
     */
    class IceGridViewer :
        public ArmarXComponentWidgetControllerTemplate<IceGridViewer>
    {
        Q_OBJECT

    public:
        explicit IceGridViewer();
        ~IceGridViewer() override;

        /**
         * Load stored manager models
         */
        void loadSettings(QSettings* settings) override;

        /**
         * Saves the manager models
         */
        void saveSettings(QSettings* settings) override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this.
         */
        static QString GetWidgetName()
        {
            return "Meta.IceGridViewer";
        }

        /**
         * @see armarx::Component::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::Component::onConnectComponent()
         */
        void onConnectComponent() override;
        void onExitComponent() override;

    signals:

    public slots:
        void gridNodeListUpdate(const IceGrid::NodeDynamicInfoSeq& nodeSeq);
        void selectGridNode(int index);
        void gridNodeListAdd(const IceGrid::NodeDynamicInfo& node);
        void gridNodeListRemove(const QString& node);
        void updateServerInfo(const std::string& nodeName, const IceGrid::ServerDynamicInfo& serverUpdateInfo);
        void updateAdapterInfo(const std::string& nodeName, const IceGrid::AdapterDynamicInfo& adapterUpdateInfo);

    protected slots:
        void serverInfoChanged(IceGrid::ServerDynamicInfo serverInfo);

    private:
        mutable std::mutex gridNodeListMutex;
        Ui::IceGridViewer* ui;

        const std::string ADAPTER_NAME;
        armarx::IceGridAdminPtr iceGridAdmin;
        Ice::ObjectAdapterPtr observerAdapter;

        IceGrid::NodeDynamicInfoSeq gridNodeList;
        NodeInfoModel* nodeInfoModel;
        ServerInfoModel* serverInfoModel;
        AdapterInfoModel* adapterInfoModel;
        QStringListModel* selectorModel;
    };
}

