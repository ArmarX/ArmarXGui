/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Manfred Kroehnert ( Manfred.Kroehnert at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "IceGridViewer.h"
#include <ArmarXGui/gui-plugins/SystemStateMonitorPlugin/ui_IceGridViewer.h>
#include "NodeObserver.h"
#include "model/NodeInfoModel.h"
#include "model/ServerInfoModel.h"
#include "model/AdapterInfoModel.h"

#include <ArmarXGui/gui-plugins/SystemStateMonitorPlugin/ui_IceGridNodeView.h>
#include <ArmarXCore/core/IceGridAdmin.h>

#include <Ice/ObjectAdapter.h>

#include <algorithm>


namespace armarx
{
    IceGridViewer::IceGridViewer() :
        ui(new Ui::IceGridViewer),
        ADAPTER_NAME("IceGridNodeObserverGui")
    {
        ui->setupUi(getWidget());

        qRegisterMetaType<QVector<int>>("QVector<int>");
        qRegisterMetaType<IceGrid::ServerDynamicInfo>("IceGrid::ServerDynamicInfo");
        connect(ui->gridNodeSelector, SIGNAL(currentIndexChanged(int)), this, SLOT(selectGridNode(int)));

        Ui::IceGridNodeView* content = new Ui::IceGridNodeView();
        content->setupUi(ui->contentWidget);
        nodeInfoModel = new NodeInfoModel;
        content->NodeInfo->setModel(nodeInfoModel);
        serverInfoModel = new ServerInfoModel;
        content->ServerInfo->setModel(serverInfoModel);
        adapterInfoModel = new AdapterInfoModel;
        content->AdapterInfo->setModel(adapterInfoModel);

        connect(serverInfoModel, SIGNAL(serverInfoChanged(IceGrid::ServerDynamicInfo)), this, SLOT(serverInfoChanged(IceGrid::ServerDynamicInfo)));

        selectorModel = new QStringListModel;
        ui->gridNodeSelector->setModel(selectorModel);
    }


    IceGridViewer::~IceGridViewer()
    {
        delete ui;
    }


    void IceGridViewer::loadSettings(QSettings* settings)
    {
    }


    void IceGridViewer::saveSettings(QSettings* settings)
    {
    }


    void IceGridViewer::onInitComponent()
    {
        // create new IceGridAdmin which also creates a new AdminSession
        // Observers set on this new session won't interfer with the default observers
        // used for tracking ManagedIceObject dependencies
        iceGridAdmin = IceGridAdmin::Create(getIceManager()->getCommunicator(), ADAPTER_NAME);
        // remove default observers which are not required and used here
        iceGridAdmin->removeObservers();
    }


    void IceGridViewer::onConnectComponent()
    {
        IceGrid::AdminSessionPrx adminSession = iceGridAdmin->adminSession();

        QtNodeObserverPtr observer = new QtNodeObserver(this);
        Ice::ObjectPrx objectProxy = iceGridAdmin->registerObjectWithNewAdapter(observer, ADAPTER_NAME, observerAdapter);
        IceGrid::NodeObserverPrx nodeObserver = IceGrid::NodeObserverPrx::checkedCast(objectProxy);

        adminSession->setObservers(NULL, NULL, NULL, NULL, NULL);
        adminSession->setObservers(NULL,
                                   nodeObserver,
                                   NULL,
                                   NULL,
                                   NULL);
    }

    void IceGridViewer::onExitComponent()
    {
        // stop updating the AdminSession so it can be removed
        iceGridAdmin->stop();
        // remove observers so the IceGridAmin does not hold any more references to them
        iceGridAdmin->adminSession()->setObservers(NULL, NULL, NULL, NULL, NULL);
        // destroy the object adapter to remove the observer objects
        observerAdapter->destroy();
    }


    void IceGridViewer::gridNodeListUpdate(const IceGrid::NodeDynamicInfoSeq& nodeSeq)
    {
        QString lastSelectionText = ui->gridNodeSelector->currentText();
        {
            std::unique_lock guard(gridNodeListMutex);
            gridNodeList = nodeSeq;
            QStringList nodeNames;

            for (size_t i = 0; i < gridNodeList.size(); ++i)
            {
                nodeNames.append(QString::fromStdString(gridNodeList[i].info.name));
            }

            selectorModel->setStringList(nodeNames);
        }

        int index = ui->gridNodeSelector->findText(lastSelectionText);
        index = std::max(0, index);

        ui->gridNodeSelector->setCurrentIndex(index);
        selectGridNode(index);
    }


    void IceGridViewer::gridNodeListAdd(const IceGrid::NodeDynamicInfo& node)
    {
        std::unique_lock guard(gridNodeListMutex);
        QStringList currentEntries = selectorModel->stringList();
        currentEntries.append(QString::fromStdString(node.info.name));
        gridNodeList.push_back(node);
        selectorModel->setStringList(currentEntries);
    }


    void IceGridViewer::gridNodeListRemove(const QString& node)
    {
        QString lastSelectionText = ui->gridNodeSelector->currentText();
        {
            std::unique_lock guard(gridNodeListMutex);

            QStringList currentEntries = selectorModel->stringList();
            currentEntries.removeOne(node);

            for (IceGrid::NodeDynamicInfoSeq::iterator nodeInfo = gridNodeList.begin(); nodeInfo != gridNodeList.end(); nodeInfo++)
            {
                if (nodeInfo->info.name == node.toStdString())
                {
                    nodeInfo = gridNodeList.erase(nodeInfo);
                }
            }

            selectorModel->setStringList(currentEntries);

        }
        int index = std::max<int>(0, ui->gridNodeSelector->findText(lastSelectionText));
        ui->gridNodeSelector->setCurrentIndex(index);
        selectGridNode(index);
    }


    void IceGridViewer::updateServerInfo(const std::string& nodeName, const IceGrid::ServerDynamicInfo& serverUpdateInfo)
    {
        {
            std::unique_lock guard(gridNodeListMutex);
            IceGrid::NodeDynamicInfoSeq::iterator nodeEntry =
                std::find_if(gridNodeList.begin(), gridNodeList.end(), [nodeName](IceGrid::NodeDynamicInfo nodeInfo)
            {
                return nodeName == nodeInfo.info.name;
            });

            if (nodeEntry == gridNodeList.end())
            {
                return;
            }

            IceGrid::ServerDynamicInfoSeq::iterator serverEntry =
                std::find_if(nodeEntry->servers.begin(), nodeEntry->servers.end(), [serverUpdateInfo](IceGrid::ServerDynamicInfo serverInfo)
            {
                return serverUpdateInfo.id == serverInfo.id;
            });

            if (serverEntry == nodeEntry->servers.end())
            {
                nodeEntry->servers.push_back(serverUpdateInfo);
            }
            else
            {
                if (serverUpdateInfo.pid == 0)
                {
                    nodeEntry->servers.erase(serverEntry);
                }
                else
                {
                    (*serverEntry) = serverUpdateInfo;
                }
            }
        }

        selectGridNode(ui->gridNodeSelector->currentIndex());
    }


    void IceGridViewer::updateAdapterInfo(const std::string& nodeName, const IceGrid::AdapterDynamicInfo& adapterUpdateInfo)
    {
        {
            std::unique_lock guard(gridNodeListMutex);
            IceGrid::NodeDynamicInfoSeq::iterator nodeEntry =
                std::find_if(gridNodeList.begin(), gridNodeList.end(), [nodeName](IceGrid::NodeDynamicInfo nodeInfo)
            {
                return nodeName == nodeInfo.info.name;
            });

            if (nodeEntry == gridNodeList.end())
            {
                return;
            }

            IceGrid::AdapterDynamicInfoSeq::iterator adapterEntry =
                std::find_if(nodeEntry->adapters.begin(), nodeEntry->adapters.end(), [adapterUpdateInfo](IceGrid::AdapterDynamicInfo adapterInfo)
            {
                return adapterUpdateInfo.id == adapterInfo.id;
            });

            if (adapterEntry == nodeEntry->adapters.end())
            {
                nodeEntry->adapters.push_back(adapterUpdateInfo);
            }
            else
            {
                if (!adapterUpdateInfo.proxy)
                {
                    nodeEntry->adapters.erase(adapterEntry);
                }
                else
                {
                    (*adapterEntry) = adapterUpdateInfo;
                }
            }
        }

        selectGridNode(ui->gridNodeSelector->currentIndex());
    }


    void IceGridViewer::serverInfoChanged(IceGrid::ServerDynamicInfo serverInfo)
    {
        iceGridAdmin->getAdmin()->enableServer(serverInfo.id, serverInfo.enabled);
    }


    void IceGridViewer::selectGridNode(int index)
    {
        if (index >= (int)gridNodeList.size() || index < 0)
        {
            return;
        }

        std::unique_lock guard(gridNodeListMutex);
        nodeInfoModel->setData(gridNodeList[index].info);
        serverInfoModel->setData(gridNodeList[index].servers);
        adapterInfoModel->setData(gridNodeList[index].adapters);
    }
}
