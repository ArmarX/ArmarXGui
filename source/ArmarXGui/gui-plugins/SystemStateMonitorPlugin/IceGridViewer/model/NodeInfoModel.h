/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Manfred Kroehnert ( manfred.kroehnert at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <QAbstractTableModel>
#include <IceGrid/Admin.h>

#include <mutex>

class NodeInfoModel :
    public QAbstractTableModel
{
    Q_OBJECT
public:
    NodeInfoModel()
    {
    }

    NodeInfoModel(IceGrid::NodeInfo nodeInfo) :
        nodeInfo(nodeInfo)
    {
    }

    int rowCount(const QModelIndex& parent = QModelIndex()) const override
    {
        return 1;
    }

    int columnCount(const QModelIndex& parent = QModelIndex()) const override
    {
        return 8;
    }

    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override
    {
        if (!index.isValid())
        {
            return QVariant();
        }

        if (index.row() >= rowCount() || index.column() >= columnCount())
        {
            return QVariant();
        }

        if (role == Qt::DisplayRole)
        {
            std::unique_lock guard(nodeInfoMutex);

            switch (index.column())
            {
                case 0:
                    return QString::fromStdString(nodeInfo.name);
                    break;

                case 1:
                    return QString::fromStdString(nodeInfo.os);
                    break;

                case 2:
                    return QString::fromStdString(nodeInfo.hostname);
                    break;

                case 3:
                    return QString::fromStdString(nodeInfo.release);
                    break;

                case 4:
                    return QString::fromStdString(nodeInfo.version);
                    break;

                case 5:
                    return QString::fromStdString(nodeInfo.machine);
                    break;

                case 6:
                    return QString::number(nodeInfo.nProcessors);
                    break;

                case 7:
                    return QString::fromStdString(nodeInfo.dataDir);
                    break;

                default:
                    return QVariant();
            }
        }

        return QVariant();
    }

    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override
    {
        if (role != Qt::DisplayRole)
        {
            return QVariant();
        }

        if (orientation == Qt::Horizontal)
        {
            switch (section)
            {
                case 0:
                    return QString("Name");
                    break;

                case 1:
                    return QString("OS");
                    break;

                case 2:
                    return QString("Hostname");
                    break;

                case 3:
                    return QString("Release");
                    break;

                case 4:
                    return QString("Version");
                    break;

                case 5:
                    return QString("Machine");
                    break;

                case 6:
                    return QString("#CPUs");
                    break;

                case 7:
                    return QString("DataDir");
                    break;

                default:
                    return QString("");
                    break;
            }
        }

        return QVariant();
    }

    Qt::ItemFlags flags(const QModelIndex& index) const override
    {
        return Qt::ItemFlags(0);
    }

    bool setData(IceGrid::NodeInfo newInfo)
    {
        std::unique_lock guard(nodeInfoMutex);
        beginResetModel();
        nodeInfo = newInfo;
        endResetModel();
        emit dataChanged(index(0, 0), index(rowCount(), columnCount()));
        return true;
    }

protected:
    IceGrid::NodeInfo nodeInfo;
    mutable std::mutex nodeInfoMutex;
};

