/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "IceGridModel.h"
#include <QApplication>
#include <QPainter>
#include <QProgressBar>

namespace armarx
{
    IceGridModel::IceGridModel(QObject* parent) :
        QAbstractTableModel(parent)
    {
    }


    IceGridModel::~IceGridModel()
    {
    }


    int IceGridModel::rowCount(const QModelIndex& parent) const
    {
        return gridNodeItemsList.size();
    }


    int IceGridModel::columnCount(const QModelIndex& parent) const
    {
        return 2;
    }


    QVariant IceGridModel::data(const QModelIndex& index, int role) const
    {
        const unsigned int& row = index.row();
        const unsigned int& column = index.column();
        std::unique_lock lock(gridNodeListsMutex);

        if (row >= gridNodeItemsList.size())
        {
            return QVariant();
        }

        switch (role)
        {
            case Qt::DisplayRole:
            case Qt::ToolTipRole:
                switch (column)
                {
                    case 0:
                        return QString(row);
                        break;

                    case 1:
                        return QString::fromStdString(gridNodeItemsList.at(row));
                        break;

                    default:
                        return QVariant();
                }

                break;

            default:
                return QVariant();
        }

        return QVariant();
    }


    QVariant IceGridModel::headerData(int section, Qt::Orientation orientation, int role) const
    {
        switch (role)
        {
            case Qt::DisplayRole:
            case Qt::ToolTipRole:
                if (orientation == Qt::Horizontal)
                {
                    switch (section)
                    {
                        case 0:
                            return "Item";

                        case 1:
                            return "Name";
                    }
                }

                break;
        }

        return QAbstractTableModel::headerData(section, orientation, role);
    }


    void IceGridModel::setData(const std::vector<std::string>& gridNodeItemsList)
    {
        beginResetModel();
        std::unique_lock lock(gridNodeListsMutex);
        this->gridNodeItemsList = gridNodeItemsList;
        endResetModel();
        emit dataChanged(index(0, 0), index(rowCount(), columnCount()));
    }
}
