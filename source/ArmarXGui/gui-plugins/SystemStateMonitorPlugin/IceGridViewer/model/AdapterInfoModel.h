/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Manfred Kroehnert ( manfred.kroehnert at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <QAbstractTableModel>
#include <IceGrid/Admin.h>

#include <mutex>

class AdapterInfoModel :
    public QAbstractTableModel
{
    Q_OBJECT
public:
    AdapterInfoModel()
    {
    }


    AdapterInfoModel(IceGrid::AdapterDynamicInfoSeq adapterInfo) :
        adapterInfo(adapterInfo)
    {
    }


    int rowCount(const QModelIndex& parent = QModelIndex()) const override
    {
        return adapterInfo.size();
    }


    int columnCount(const QModelIndex& parent = QModelIndex()) const override
    {
        return 2;
    }


    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override
    {
        if (!index.isValid())
        {
            return QVariant();
        }

        if (index.row() >= (int)adapterInfo.size())
        {
            return QVariant();
        }

        if (role == Qt::DisplayRole)
        {
            std::unique_lock guard(adapterInfoMutex);

            switch (index.column())
            {
                case 0:
                    return QString::fromStdString(adapterInfo.at(index.row()).id);
                    break;

                case 1:
                    return QString::fromStdString(adapterInfo.at(index.row()).proxy->ice_toString());
                    break;

                default:
                    return QVariant();
            }
        }

        return QVariant();
    }


    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override
    {
        if (role != Qt::DisplayRole)
        {
            return QVariant();
        }

        if (orientation == Qt::Horizontal)
        {
            switch (section)
            {
                case 0:
                    return QString("ID");
                    break;

                case 1:
                    return QString("Proxy");
                    break;

                default:
                    return QString("");
                    break;
            }
        }

        return QVariant();
    }


    Qt::ItemFlags flags(const QModelIndex& index) const override
    {
        return Qt::ItemFlags(0);
    }


    bool setData(IceGrid::AdapterDynamicInfoSeq newInfo)
    {
        std::unique_lock guard(adapterInfoMutex);
        beginResetModel();
        adapterInfo = newInfo;
        endResetModel();
        emit dataChanged(index(0, 0), index(rowCount(), columnCount()));
        return true;
    }

protected:
    IceGrid::AdapterDynamicInfoSeq adapterInfo;
    mutable std::mutex adapterInfoMutex;
};

