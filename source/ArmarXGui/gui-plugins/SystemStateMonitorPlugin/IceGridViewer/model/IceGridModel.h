/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <QAbstractItemModel>
#include <QStyledItemDelegate>

#include <mutex>

namespace armarx
{

    class IceGridModel :
        public QAbstractTableModel
    {
        Q_OBJECT
    public:
        explicit IceGridModel(QObject* parent = 0);
        ~IceGridModel() override;

        // inherited from QAbstractItemModel
        int rowCount(const QModelIndex& parent = QModelIndex()) const override;
        int columnCount(const QModelIndex& parent =  QModelIndex()) const override;
        QVariant data(const QModelIndex& index, int role) const override;
        QVariant headerData(int section, Qt::Orientation orientation, int role) const override;

        void setData(const std::vector<std::string>& gridNodeItemsList);
    signals:

    public slots:

    private:
        void run();

        std::vector<std::string> gridNodeItemsList;
        mutable std::mutex gridNodeListsMutex;
    };
}

