/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX::Gui
 * @author     Jan Issac ( jan.issac at gmail dot com)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include "ArmarXManagerItem.h"

#include <QMap>
#include <QThread>
#include <QStringList>
#include <QStandardItemModel>
#include <QItemSelectionModel>
#include <QPointer>

#include <ArmarXCore/core/ArmarXManager.h>
#include <ArmarXCore/core/ManagedIceObject.h>
#include <ArmarXCore/core/ManagedIceObjectRegistryInterface.h>

#include <mutex>

namespace armarx
{
    using StateUpdateMap = std::map<QString, QPair<bool, ArmarXManagerItem::ObjectMap> >;
    using ManagerPrxMap = std::map<QString, ArmarXManagerInterfacePrx>;
    class ArmarXManagerModel:
        public QStandardItemModel
    {
        Q_OBJECT

    public:
        /**
         * Constructs an ArmarXManagerModel
         */
        ArmarXManagerModel();

        /**
         * Model destructor
         */
        ~ArmarXManagerModel() override;

    public slots:
        /**
         *
         */
        void updateManagerDetails(const ArmarXManagerItem::ManagerDataMap& managerDataMap, bool deactivateIfMissing = true);
        void upsertManagerDetails(const ArmarXManagerItem::ManagerData& data);
        /**
         * Takes the specified selected rows via the selection model from this
         * and appends the rows to the given destination model.
         *
         * @param selectionModel    The selected rows
         * @param destinationModel  Manager destination model
         */
        void moveSelectionTo(QItemSelectionModel* selectionModel,
                             ArmarXManagerModel* destinationModel);

        /**
         * Takes the specified selected rows via the selection model from the
         * source model and appends the rows to this one.
         *
         * @param selectionModel    The selected rows
         * @param sourceModel       Manager source model
         */
        void takeSelectionFrom(QItemSelectionModel* selectionModel,
                               ArmarXManagerModel* sourceModel);

        /**
         * Deletes the selected set of rows from this model.
         *
         * @param selectionModel    The selected rows
         */
        void deleteSelection(QItemSelectionModel* selectionModel);

    public:
        /**
         * Returns a requested Model item
         *
         * @param row       The row index of the item
         * @param column    The column index of the item
         *
         * @param Pointer to the ArmarXManagerItem
         */
        ArmarXManagerItem* getItem(int row, int column = 0) const;

        /**
         * Returns the requested manager item by its name
         *
         * @param name  Requested manager item name
         *
         * @return pointer to the manager item if exists, NULL otherwise
         */
        ArmarXManagerItem* getManagerItemByName(const QString& name);

        ManagerPrxMap getManagerProxyMap() const;
        ManagerPrxMap getManagerProxyMap(const QStringList& managerNames) const;

        /**
         * Returns the model as a string list containing the manager names
         *
         * @return Manager names as string list
         */
        QStringList toStringList() const;
        bool empty() const;

        /**
         * Populates this model with the given manager name list
         *
         * @param managerList   The manager names to populate this model with.
         */
        void populate(const QStringList& managerList);

        /**
         * Returns a clone of this ArmarXManagerModel
         *
         * @return pointer to a clone of this model
         */
        ArmarXManagerModel* clone();

        /**
         * Copies the source model content to this one. After the copy, this
         * model is basically a clone of the source model
         *
         * @param source    Source model to copy all content from
         */
        void copyFrom(ArmarXManagerModel* source);

        /**
         * Returns the data header
         */
        QVariant headerData(int section, Qt::Orientation orientation, int role) const override;

        /**
         * Returns the access mutex
         */
        std::mutex& getMutex();

        void setIceManager(IceManagerPtr iceManager);
        IceManagerPtr getIceManager() const;
        enum class UserDataRoles : int
        {
            ComponentStarted = Qt::UserRole + 1,
            ResolvedDependency,
            RowType,
            End
        };
        enum class ApplicationRowType
        {
            Endpoint,
            ApplicationProperties,
            Components
        };

    public slots:
        void updateItem(QStandardItem* item);

    public:

        /// Returns an instance of state dependent brush.
        QBrush getBrush(armarx::ManagedIceObjectState state) const;


    private:

        /**
         * Util function: Swaps a selection of rows between a source and a
         * destination model
         *
         * @param sourceModel       The source model providing the rows
         * @param destinationModel  The destination model which takes the
         *                          selected rows
         * @param selectionModel    Model of the selected rows
         */
        void swapSelection(ArmarXManagerModel* sourceModel,
                           ArmarXManagerModel* destinationModel,
                           QItemSelectionModel* selectionModel);

        /**
         * Util function: Converts a selection model into a persistent index
         * model used for swapping and deletion of multiple items.
         *
         * @param selectionModel        The selection model to convert into
         *                              a persistent model index
         *
         * @return A list of persistent model index
         */
        QList<QPersistentModelIndex> getPersistentModelIndex(
            QItemSelectionModel* selectionModel);


        QStandardItem* findItem(const QString& name, QStandardItem* item);
        QStandardItem* findItemByUserData(Qt::ItemDataRole role, const QVariant& data, QStandardItem* item);
        void syncStringListChildren(QStandardItem* item, const std::vector<QString>&  strings);

        static std::string GenerateDependencyGraph(const std::map<std::string, DependencyMap>& dependenciesMap);
    private:
        /**
         * Modification access mutex
         */
        std::mutex mutex;

        IceManagerPtr iceManager;
        std::map<std::string, DependencyMap> dependenciesMap;
    };


}

