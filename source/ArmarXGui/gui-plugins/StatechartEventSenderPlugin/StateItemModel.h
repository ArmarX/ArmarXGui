/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Gui
* @author     Kai Welke (welke@kit.edu)
* @copyright  2012 Humanoids Group, IAIM, IFA
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include "../ObserverPropertiesPlugin/widgets/properties/PropertiesWidget.h"

#include <ArmarXCore/statechart/Statechart.h>

#include <QStandardItemModel>

#include <string>




#define STATE_ITEM_TYPE Qt::UserRole+1
//#define STATE_UNIQUEID Qt::UserRole+2
#define STATE_COMPONENT_NAME Qt::UserRole+3
#define STATE_ITEM_DELETE Qt::UserRole+4
#define STATE_TYPE Qt::UserRole+5
#define STATE_GLOBALSTATEID Qt::UserRole+6

#define ARMARXGUI_BASEINSTANCE_STR "Base Instances"
#define ARMARXGUI_REQUESTEDINSTANCE_STR "Requested Instances"


namespace armarx
{

    class StateItemModel : public QStandardItemModel
    {
        Q_OBJECT
    public:
        StateItemModel(QObject* parent = 0);
        ~StateItemModel() override {}

        //! Enum that is used to store the type of a treeEntry
        enum eItemType
        {
            eComponentItem,
            ePlaceHolderItem,
            eBaseStateItem,
            eInstanceStateItem,
            eInputItem
        };
        void updateModel(const std::string& componentName,  const std::vector<StateIceBasePtr>& topLevelBaseStates, const std::vector<StateIceBasePtr>& topLevelRequestedStates);
        StateIceBasePtr getState(std::string componentName, const std::string& globalStateIdStr);
        void findSubstate(StateIceBasePtr state, const std::string& globalStateIdStr, StateIceBasePtr& resultState);

        //        QString getSearchString(){ return searchString;}

        // QAbstractItemModel interface
        bool setData(const QModelIndex& index, const QVariant& value, int role) override;
    public slots:
        //        void setSearchString(QString search){ searchString = search;}
    private:
        std::map<std::string, std::vector< StateIceBasePtr > > requestedInstanceList;

        QStandardItem* updateComponent(std::string componentName);
        void updateStates(QStandardItem* componentItem, const std::vector<StateIceBasePtr>& topLevelStates, std::string componentName, eItemType itemType);

        void markAllForDelete(QStandardItem* stateItem);
        void deleteUnusedItems(QStandardItem* stateItem);
        void insertSubstates(QStandardItem* parentStateItem, StateIceBasePtr state, std::string componentName, eItemType itemType);
        QStandardItem* updateOrInsertItem(QStandardItem* parent, QStandardItem* insert);


        QVariant data(const QModelIndex& index, int role) const override;

        //        QString searchString;



        //        // QAbstractItemModel interface
        //    public:
        //        Qt::ItemFlags flags(const QModelIndex &index) const;
    };
}

