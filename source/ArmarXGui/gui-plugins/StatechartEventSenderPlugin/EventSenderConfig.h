/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

/* ArmarX headers */

#include "TreeBox.h"

/* Qt headers */
#include <QTableWidget>
#include <QWidget>
#include <QComboBox>
#include <QList>
#include <QSettings>
#include <QDialog>

/* ArmarX Headers */
#include <ArmarXCore/core/Component.h>
#include "StateItemModel.h"

namespace Ui
{
    class EventSenderConfigDialog;
}


namespace armarx
{
    class StateParameterEditor;

    struct EventSenderConfig
    {
        QString eventSenderName;
        QString description;
        QString componentName;
        EventPtr event;
        std::string globalStateIdentifier;


        EventSenderConfig();
        EventSenderConfig(const EventSenderConfig& source)
        {
            eventSenderName = source.eventSenderName;
            description = source.description;
            componentName = source.componentName;
            globalStateIdentifier = source.globalStateIdentifier;


            if (source.event)
            {
                event = source.event->clone();
            }
            else
            {
                event = new Event(EVENTTOALL, "dummyEvent");
            }
        }
        EventSenderConfig& operator=(const EventSenderConfig&) = default;

        void saveSettings(QSettings* settings);
        void loadSettings(QSettings* settings);

    };

    class EventSenderConfigDialog:
        public QDialog,
        virtual public Logging
    //            public Component

    {
        Q_OBJECT

    public:

        Ui::EventSenderConfigDialog* ui;

        EventSenderConfigDialog(QWidget* parent = 0);
        ~EventSenderConfigDialog() override;




        void setEventSender(QString first, QString second);

        QStringList getPossibleEvents(StateIceBasePtr state);

        // data retrieval functions
        QString getSelectedComponentName();
        QString getSelectedState();
        EventPtr getSelectedEvent();
        void setConfig(const EventSenderConfig& config);
        const QModelIndex getRowOfItem(QStandardItemModel* model, QString searchString, int userDataRole = 0, QVariant userData = QVariant()) const;
        const QModelIndex getRowOfItem(QStandardItem* item, QString searchString, int userDataRole = 0, QVariant userData = QVariant()) const;
        void setIceManager(IceManagerPtr iceManager);
    public slots:
        void send();
        void eventBox(int index);
        void saveEventSender();
        void populateStateTreeList();
        void stateSelected(int index);
    protected:
        void setupUi();


        void getTopLevelStates(std::string componentName,  std::vector<StateIceBasePtr>& baseStateList, std::vector<StateIceBasePtr>& instanceStateList);
    private:
        int mdiId;
        TreeBox* treeBox;
        StateItemModel* model;
        IceManagerPtr iceManager;
        //        StateParameterEditor* paramEditor;


    };
    using EventSenderPtr = IceUtil::Handle<EventSenderConfigDialog>;

}


