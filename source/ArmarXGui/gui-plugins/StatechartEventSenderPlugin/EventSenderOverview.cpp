/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Gui
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "EventSenderOverview.h"
#include <ArmarXGui/gui-plugins/StatechartEventSenderPlugin/ui_EventSenderOverview.h>

#include "EventSenderComponent.h"

// ArmarX
#include <ArmarXCore/statechart/Statechart.h>

using namespace armarx;


EventSenderOverview::EventSenderOverview() :
    ui(new Ui::EventSenderOverview())

{
    lastColumnInsertion = 0;
    lastRowInsertion = 0;
    ui->setupUi(getWidget());

    connect(ui->btnAddEventSender, SIGNAL(clicked()), this, SLOT(addEventSender()));
}
EventSenderOverview::~EventSenderOverview()
{
    delete ui;
}

void EventSenderOverview::loadSettings(QSettings* settings)
{
    int size = settings->beginReadArray("EventSender");

    for (int i = 0; i < size; ++i)
    {
        settings->setArrayIndex(i);
        EventSenderConfig config;
        config.loadSettings(settings);
        addEventSender(config);
    }

    settings->endArray();
}

void EventSenderOverview::saveSettings(QSettings* settings)
{

    settings->beginWriteArray("EventSender");
    int size = ui->scrollAreaWidgetContents->children().size();
    int settingsIndex = 0;

    for (int i = 0; i < size; ++i)
    {
        EventSenderComponent* comp  =  qobject_cast<EventSenderComponent*>(ui->scrollAreaWidgetContents->children().at(i));

        if (!comp)
        {
            continue;
        }

        settings->setArrayIndex(settingsIndex);
        comp->config.saveSettings(settings);
        settingsIndex++;
    }

    settings->endArray();
}

void EventSenderOverview::onInitComponent()
{
    int size = ui->scrollAreaWidgetContents->children().size();

    for (int i = 0; i < size; ++i)
    {
        EventSenderComponent* comp  =  qobject_cast<EventSenderComponent*>(ui->scrollAreaWidgetContents->children().at(i));

        if (!comp)
        {
            continue;
        }

        comp->setIceManager(getIceManager());
    }
}

EventSenderComponent* EventSenderOverview::addEventSender(const EventSenderConfig& config)
{
    if (config.eventSenderName.length() == 0)
    {
        return NULL;
    }

    // check if eventsender by this name exists
    int size = ui->scrollAreaWidgetContents->children().size();

    for (int i = 0; i < size; ++i)
    {
        EventSenderComponent* comp  =  qobject_cast<EventSenderComponent*>(ui->scrollAreaWidgetContents->children().at(i));

        if (!comp)
        {
            continue;
        }

        if (ui->edtEventSenderName->text() == comp->config.eventSenderName)
        {
            return NULL;
        }
    }

    ARMARX_LOG << "Adding EventSender " << config.eventSenderName.toStdString() << flush;
    EventSenderComponent* comp = new EventSenderComponent(config.eventSenderName,  ui->scrollAreaWidgetContents);
    comp->setIceManager(getIceManager());
    //    comp->setAttribute(Qt::WA_DeleteOnClose,false);

    comp->setConfig(config);
    ui->scrollAreaVerticalLayout->removeItem(ui->verticalSpacer);

    ui->scrollAreaVerticalLayout->addWidget(comp);
    ui->scrollAreaVerticalLayout->addItem(ui->verticalSpacer);
    connect(comp, SIGNAL(sendEvent(EventSenderConfig)), this, SLOT(sendEvent(EventSenderConfig)));
    return comp;
}

void EventSenderOverview::addEventSender()
{
    EventSenderConfig config;
    config.eventSenderName = ui->edtEventSenderName->text();
    addEventSender(config);
}

void EventSenderOverview::sendEvent(const EventSenderConfig& config)
{
    try
    {
        RemoteStateOffererInterfacePrx statePrx;
        statePrx = getProxy<RemoteStateOffererInterfacePrx>(config.componentName.toStdString());

        if (!statePrx)
        {
            ARMARX_WARNING << "Proxy is NULL" << flush;
            return;
        }

        statePrx->begin_issueEventWithGlobalIdStr(config.globalStateIdentifier, config.event);
        ARMARX_INFO << "sent event " << config.event->eventName << " to state " << config.globalStateIdentifier << " in component " << config.componentName.toStdString() << flush;
        ARMARX_INFO << "event parameter count: " << config.event->properties.size() << flush;
        ARMARX_INFO << "Event dictionary: " << StateUtilFunctions::getDictionaryString(config.event->properties);
    }
    catch (Ice::NotRegisteredException& e)
    {
        ARMARX_WARNING << "Cannot send event - the entered component with name '" << config.componentName.toStdString() << "' was not found - maybe you forgot to start it?" << flush;
    }
    catch (armarx::UserException& e)
    {
        ARMARX_WARNING << "Cannot send event - Found component but cannot communicate correctly with component - probably it is a component of another type\nreason: " << e.reason << flush;
    }
    catch (LocalException& e)
    {
        ARMARX_WARNING << "Cannot send event - the entered component with name '" << config.componentName.toStdString() << "' was not found - maybe you forgot to start it?" << flush;
    }
    catch (std::exception& e)
    {
        ARMARX_WARNING << "Cannot send event - caught exception:\n" << e.what();
    }
}
