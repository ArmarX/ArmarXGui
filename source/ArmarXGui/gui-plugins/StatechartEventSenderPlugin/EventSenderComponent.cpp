/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Gui
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "EventSenderComponent.h"
#include <ArmarXGui/gui-plugins/StatechartEventSenderPlugin/ui_EventSenderConfig.h>

#include <IceUtil/IceUtil.h>

#include <QSpacerItem>

using namespace armarx;


EventSenderComponent::EventSenderComponent(QString eventSenderName, QWidget* parent) :
    dialog(0)
{

    config.eventSenderName = eventSenderName;
    setupUi();
}

EventSenderComponent::~EventSenderComponent()
{
    //    ARMARX_INFO << "~EventSenderComponent" << flush;
    delete dialog;
}





//QWidget *EventSenderComponent::getMainWidget()
//{
//    return this;
//}

void EventSenderComponent::setupUi()
{
    if (dialog)
    {
        return;
    }

    //    gbEventSender = new QGroupBox(this);
    this->setObjectName(QString::fromUtf8("this"));
    this->setMinimumSize(QSize(0, 40));
    horizontalLayout = new QHBoxLayout(this);
    horizontalLayout->setSpacing(2);
    horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
    horizontalLayout->setContentsMargins(2, 3, 2, -1);
    tbSend = new QToolButton(this);
    tbSend->setObjectName(QString::fromUtf8("tbSend"));
    tbSend->setMinimumSize(QSize(60, 20));
    QIcon icon;
    icon.addFile(QString::fromUtf8(":/icons/media-playback-start.ico"), QSize(), QIcon::Normal, QIcon::Off);
    tbSend->setIcon(icon);
    tbSend->setIconSize(QSize(16, 16));
    tbSend->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    horizontalLayout->addWidget(tbSend);

    tbConfig = new QToolButton(this);
    tbConfig->setObjectName(QString::fromUtf8("tbConfig"));
    tbConfig->setMinimumSize(QSize(20, 20));

    QIcon icon2;
    icon2.addFile(QString::fromUtf8(":/icons/configure-3.png"), QSize(), QIcon::Normal, QIcon::Off);
    tbConfig->setIcon(icon2);
    tbConfig->setIconSize(QSize(16, 16));
    horizontalLayout->addWidget(tbConfig);

    tbDelete = new QToolButton(this);
    tbDelete->setObjectName(QString::fromUtf8("tbDelete"));
    tbDelete->setMinimumSize(QSize(20, 20));

    QIcon iconClose;
    iconClose.addFile(QString::fromUtf8(":/icons/dialog-close.ico"), QSize(), QIcon::Normal, QIcon::Off);
    tbDelete->setIcon(iconClose);
    tbDelete->setIconSize(QSize(16, 16));
    tbDelete->setToolButtonStyle(Qt::ToolButtonIconOnly);
    horizontalLayout->addWidget(tbDelete);

    QSpacerItem* spacer = new QSpacerItem(1, 20, QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);
    horizontalLayout->addItem(spacer);

    this->setTitle(QApplication::translate("EventSenderOverview", config.eventSenderName.toStdString().c_str(), 0));
#ifndef QT_NO_TOOLTIP
    tbSend->setToolTip(QApplication::translate("EventSenderOverview", "Send Event", 0));
    tbDelete->setToolTip(QApplication::translate("EventSenderOverview", "Delete this EventSender", 0));
    tbConfig->setToolTip(QApplication::translate("EventSenderOverview", "Configure this EventSender", 0));
#endif // QT_NO_TOOLTIP
    tbSend->setText(QApplication::translate("EventSenderOverview", "Send", 0));
    tbConfig->setText(QApplication::translate("EventSenderOverview", "...", 0));


    dialog = new EventSenderConfigDialog(this);



    // slots and signals
    connect(tbSend, SIGNAL(clicked()), this, SLOT(sendClicked()));
    connect(tbConfig, SIGNAL(clicked()), this, SLOT(openConfig()));
    connect(tbDelete, SIGNAL(clicked()), this, SLOT(deleteClicked()));

    connect(dialog, SIGNAL(accepted()), this, SLOT(configDone()));
}

void EventSenderComponent::setConfig(const EventSenderConfig& conf)
{
    this->config = EventSenderConfig(conf);

    if (conf.event)
    {
        this->config.event = EventPtr::dynamicCast(conf.event->clone());
    }

    this->setTitle(conf.eventSenderName);

}

void EventSenderComponent::setIceManager(IceManagerPtr iceMan)
{
    if (dialog)
    {
        dialog->setIceManager(iceMan);
    }

    this->iceManager = iceMan;

}




void EventSenderComponent::sendClicked()
{
    emit sendEvent(config);
}

void EventSenderComponent::openConfig()
{
    dialog->setConfig(config);
    dialog->setModal(true);
    dialog->show();
    dialog->raise();
    dialog->activateWindow();
    dialog->setParent(0);


    //ARMARX_INFO_S << config.componentName.toStdString()  << ", " << config.globalStateIdentifier << ", " << config.event->eventName << flush;

}

void EventSenderComponent::deleteClicked()
{
    throw exceptions::user::NotImplementedYetException("EventSenderDeletion");
}

void EventSenderComponent::configDone()
{
    config.componentName = dialog->getSelectedComponentName();
    config.globalStateIdentifier = dialog->getSelectedState().toStdString();
    config.event = dialog->getSelectedEvent();
    config.description = dialog->ui->edtDescription->text();
    config.eventSenderName = dialog->ui->edtEventSenderName->text();
    this->setTitle(config.eventSenderName);
    setToolTip(config.description);
}
