/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::GuiComponent::EventSender
* @author     Mirko Waechter
* @copyright  2012 Mirko Waechter
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "EventSenderConfig.h"
#include "StateParameterEditor.h"
#include <ArmarXGui/gui-plugins/StatechartEventSenderPlugin/ui_EventSenderConfig.h>


// ArmarX
#include <ArmarXCore/statechart/Statechart.h>
#include <ArmarXCore/statechart/StatechartObjectFactories.h>
#include <ArmarXCore/core/CoreObjectFactories.h>
#include <ArmarXCore/core/exceptions/user/NotImplementedYetException.h>
#include <ArmarXCore/observers/ObserverObjectFactories.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXWidgetController.h>
#include <ArmarXCore/core/IceGridAdmin.h>

//Ice
#include <IceUtil/IceUtil.h>

// Qt headers
#include <Qt>
#include <QtGlobal>
#include <QComboBox>
#include <QScrollArea>
#include <QPushButton>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QFile>
#include <QFileDialog>
#include <QMessageBox>
#include <QTableWidgetItem>
#include <QValidator>
#include <QTreeView>

// System
#include <cstdio>
#include <string>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <cmath>


using namespace armarx;

EventSenderConfig::EventSenderConfig()
{
    event = new Event(EVENTTOALL, "dummyEvent");
}


void EventSenderConfig::saveSettings(QSettings* settings)
{
    settings->setValue("eventSenderName", eventSenderName);
    settings->setValue("description", description);
    settings->setValue("componentName", componentName);
    settings->setValue("globalStateIdentifier", globalStateIdentifier.c_str());

    if (event)
    {
        settings->setValue("eventName", event->eventName.c_str());
        settings->setValue("eventReceiverName", event->eventReceiverName.c_str());
        settings->beginWriteArray("parameters");
        StringVariantContainerBaseMap::const_iterator it = event->properties.begin();
        int i = 0;

        for (; it != event->properties.end(); ++it, i++)
        {
            SingleVariantPtr sVar = SingleVariantPtr::dynamicCast(it->second);
            settings->setArrayIndex(i);
            settings->setValue("key", it->first.c_str());
            settings->setValue("VariantTypeId", sVar->get()->getType());
            settings->setValue("value", sVar->get()->getOutputValueOnly().c_str());
        }

        settings->endArray();
    }

}

void EventSenderConfig::loadSettings(QSettings* settings)
{
    eventSenderName = settings->value("eventSenderName").toString();
    description = settings->value("description").toString();
    componentName = settings->value("componentName").toString();
    event = new Event(settings->value("eventReceiverName").toString().toStdString(),
                      settings->value("eventName").toString().toStdString());
    globalStateIdentifier = settings->value("globalStateIdentifier").toString().toStdString();
    int size = settings->beginReadArray("parameters");

    for (int i = 0; i < size; ++i)
    {
        settings->setArrayIndex(i);
        SingleVariantPtr sVar = new SingleVariant();
        VariantPtr var = new Variant();
        var->setType(settings->value("VariantTypeId").toInt());
        QVariant data = settings->value("value");
        VariantTypeId type = var->getType();

        if (type == VariantType::String)
        {
            var->setString(data.toString().toStdString());
        }
        else if (type == VariantType::Int)
        {
            var->setInt(data.toInt());
        }
        else if (type == VariantType::Long)
        {
            var->setLong(data.toLongLong());
        }
        else if (type == VariantType::Float)
        {
            var->setFloat(data.toFloat());
        }
        else if (type == VariantType::Double)
        {
            var->setDouble(data.toDouble());
        }
        else if (type == VariantType::Bool)
        {
            var->setBool(data.toBool());
        }
        else
        {
            throw exceptions::user::NotImplementedYetException("VariantParser");
        }

        sVar->setVariant(var);
        std::string key = settings->value("key").toString().toStdString();
        event->properties[key] = sVar;
        ARMARX_VERBOSE_S << "Reading parameter " << key << ": " << *sVar->get() << flush;
    }

    settings->endArray();
}


EventSenderConfigDialog::EventSenderConfigDialog(QWidget* parent) :
    ui(new Ui::EventSenderConfigDialog())
{
    ui->setupUi(this);
    setupUi();
}

EventSenderConfigDialog::~EventSenderConfigDialog()
{
    delete ui;
    //    ARMARX_INFO << "~EventSenderConfigDialog" << flush;
}





void EventSenderConfigDialog::eventBox(int index)
{
    switch (index)
    {
        default:
            //cases not defined yet
            break;
    }
}




void EventSenderConfigDialog::send()
{
    throw NotImplementedYetException("Sending event");
    //    QModelIndex index = treeBox->view()->currentIndex();
    //    if(treeBox->model()->itemData(index)[STATE_ITEM_TYPE] != StateItemModel::eInstanceStateItem){
    //        ARMARX_INFO << "You cannot send an event to this type of state! You can only send events to 'Requested Instances'." << flush;
    //        return;
    //    }

    //    QVariant varStateId =getSelectedState();
    //    QVariant varComponentName =getSelectedComponentName();
    //    ARMARX_INFO << "event receiver: " << varComponentName.toString().toStdString() << ":" << varStateId.toString().toStdString() <<  flush;
    //    RemoteStateOffererInterfacePrx statePrx;
    //    EventPtr event = getSelectedEvent();
    //    if(!event)
    //        return;
    //    statePrx = getProxy<RemoteStateOffererInterfacePrx>(varComponentName.toString().toStdString());
    //    if(!statePrx){
    //        ARMARX_WARNING << "Proxy is NULL" << flush;
    //        return;
    //    }

    //    statePrx->begin_issueEventWithGlobalIdStr(varStateId.toString().toStdString(), event);
    //    ARMARX_INFO << "sent event " << event->eventName << " to " << varComponentName.toString().toStdString() << flush;
    //    ARMARX_INFO << "event parameter count: " << event->properties.size() << flush;
}

EventPtr EventSenderConfigDialog::getSelectedEvent()
{
    QString currentEventString = ui->CBEvent->currentText();
    EventPtr event = new Event(EVENTTOALL, currentEventString.toStdString());
    event->properties = ui->paramEditor->getStringValueMap();
    //    for(int row=0; row < ui->paramEditor->rowCount(); ++row)
    //    {


    //        if(!ui->paramEditor->item(row,0)){
    //            ArmarXWidgetController::showMessageBox("Please enter a key for the parameter with id #" + QString::number(row+1));
    //            return NULL;
    //        }
    //        std::string key = ui->paramEditor->item(row,0)->text().toStdString();
    //        QComboBox* CBvalueType = qobject_cast<QComboBox*>(ui->paramEditor->cellWidget(row,1));

    //        if(!CBvalueType){
    //            ARMARX_WARNING << "CBvalueType pointer is NULL" << flush;
    //            continue;
    //        }
    //        int type = CBvalueType->itemData(CBvalueType->currentIndex()).toInt();
    //        QLineEdit* valueEdit = qobject_cast<QLineEdit*>(ui->paramEditor->cellWidget(row,2));

    //        if( type == VariantType::Int)
    //            event->add(key, valueEdit->text().toInt());

    //        else if( type == VariantType::Bool)
    //            if(valueEdit->text() == "true" || valueEdit->text() == "1")
    //                event->add(key, Variant(true));
    //            else
    //                event->add(key, Variant(false));

    //        else if( type == VariantType::Float)
    //            event->add(key, valueEdit->text().toFloat());

    //        else if( type == VariantType::String)
    //            event->add(key, valueEdit->text().toStdString());

    ////            else if( type == VariantType::DataFieldIdentifier)
    ////            {
    ////                DataFieldIdentifierBasePtr  id = new DataFieldIdentifier(valueEdit->text().toStdString());
    ////                event->add(key, id);

    ////            }
    ////            else if( eLinkedPosition:
    ////            {
    ////                LinkedPosition pos(valueEdit->text().toStdString());
    ////                event->add(key, id);
    ////                break;
    ////            }
    //        else
    //            ARMARX_WARNING << "Datatype not implemented yet!" << flush;

    //    }


    return event;
}

void EventSenderConfigDialog::setConfig(const EventSenderConfig& config)
{
    //    EventSenderConfig config;
    //    // set test data
    //    config.componentName = "RobotStatechart";
    //    config.globalStateIdentifier = "RobotStatechart->RobotControl->Functional";
    //    config.event = new Event(EVENTTOALL, "EvLoadScenario");
    //    config.event->add("test", std::string("Visual"));

    ui->edtEventSenderName->setText(config.eventSenderName);
    ui->edtDescription->setText(config.description);

    if (iceManager)
    {
        populateStateTreeList();
    }

    ui->edtSelectedState->setText(config.globalStateIdentifier.c_str());
    QModelIndex index = getRowOfItem(model, "", STATE_GLOBALSTATEID, QString(config.globalStateIdentifier.c_str()));

    if (index.isValid())
    {
        // we need to set the current index this way because QComboBox does
        // not work well with hierarchical states
        QModelIndex oldIndex = treeBox->rootModelIndex();
        treeBox->setRootModelIndex(index.parent());
        treeBox->setCurrentIndex(index.row());
        treeBox->setRootModelIndex(oldIndex);
        treeBox->view()->setCurrentIndex(index);

        stateSelected(-1); // update event list
    }

    ui->CBEvent->setCurrentIndex(ui->CBEvent->findText(config.event->eventName.c_str()));  // set correct event as selected

    // set parameters

    ui->paramEditor->setRowCount(0);
    StringVariantContainerBaseMap::const_iterator it = config.event->properties.begin();

    for (; it != config.event->properties.end(); ++it)
    {
        SingleVariantPtr sVar = SingleVariantPtr::dynamicCast(it->second);
        ui->paramEditor->addParameterRow(it->first.c_str(), QString::fromStdString(VariantContainerType::allTypesToString(sVar->getContainerType()))
                                         ,  sVar->get()->getOutputValueOnly().c_str());
    }


}

const QModelIndex  EventSenderConfigDialog::getRowOfItem(QStandardItemModel* model, QString searchString, int userDataRole, QVariant userData) const
{
    QStandardItem* item = model->invisibleRootItem();
    return getRowOfItem(item, searchString, userDataRole, userData);
}

const QModelIndex  EventSenderConfigDialog::getRowOfItem(QStandardItem* item, QString searchString, int userDataRole, QVariant userData) const
{
    if (userDataRole == 0 && searchString.length() == 0)
    {
        throw LocalException("Either searchString or userDataRole must be given!");
    }

    int i = -1;
    QStandardItem* child;

    while (i++, child = item->child(i), child != NULL)
    {
        if (child->text() == ARMARXGUI_BASEINSTANCE_STR) // base instances are no valid event receivers
        {
            continue;
        }

        if ((searchString.length() == 0 || child->text() == searchString)
            && (userDataRole == 0 || child->data(userDataRole) == userData))
        {
            return child->index();
        }

        QModelIndex result = getRowOfItem(child, searchString, userDataRole, userData);

        if (result.isValid())
        {
            return result;
        }

    }

    return QModelIndex();
}



void EventSenderConfigDialog::setIceManager(IceManagerPtr iceManager)
{
    this->iceManager = iceManager;

    if (iceManager)
    {
        ui->paramEditor->setCommunicator(iceManager->getCommunicator());
    }
}

QStringList EventSenderConfigDialog::getPossibleEvents(StateIceBasePtr state)
{
    QStringList events;

    for (unsigned int i = 0; i < state->transitions.size(); i++)
    {
        TransitionIceBase& t = state->transitions[i];

        if (!events.contains(QString(t.evt->eventName.c_str())))
        {
            events.push_back(QString(t.evt->eventName.c_str()));
        }
    }

    return events;
}

void EventSenderConfigDialog::setEventSender(QString first, QString second)
{
    QSettings settings(first, second);
    //ui->eventString->setText(settings.value("LineEdit").toString());
    //    for(int i = 0; i < settings.beginReadArray("ComboBox"); i++)
    //    {
    //        ui->eventComboBox->setItemText(i,settings.value("Event").toString());
    //    }
    settings.endArray();
    int rows = settings.beginReadArray("TableWidget");
    ui->paramEditor->setRowCount(rows);

    for (int i = 0; i < rows; i++)
    {
        settings.setArrayIndex(i);
        int columns = settings.beginReadArray("Column");
        ui->paramEditor->setColumnCount(columns);

        for (int j = 0; j < columns; j++)
        {
            settings.setArrayIndex(j);

            if (j == 0)
            {
                ui->paramEditor->item(i, j)->setText(settings.value("Cell").toString());
            }

            if (j == 1)
            {
                QComboBox* tempBox = new QComboBox;
                int comboLength = settings.beginReadArray("ComboBox");

                for (int index = 0; index < comboLength; index++)
                {
                    QString cell;
                    cell.setNum(index);
                    tempBox->insertItem(index, settings.value(cell).toString());
                }

                ui->paramEditor->setCellWidget(i, j, tempBox);
                //                if(valueboxes.contains(tempBox->objectName()))
                //                {
                //                    valueboxes.removeOne(tempBox->objectName);
                //                    valueboxes.append(tempBox);
                //                }
                settings.endArray();
            }

            if (j == 2)
            {
                QLineEdit* tempEdit = new QLineEdit;
                tempEdit->setText(settings.value("Cell").toString());
                ui->paramEditor->setCellWidget(i, j, tempEdit);
            }
        }

        settings.endArray();
    }

    settings.endArray();
}



void EventSenderConfigDialog::saveEventSender()
{
    QSettings settings("armarx", "eventSender");
    //settings.setValue("LineEdit", ui->eventString->text());
    settings.beginWriteArray("ComboBox");
    //    for(int i = 0; i < ui->eventComboBox->count(); i++)
    //    {
    //        settings.setValue("Event", ui->eventComboBox->itemText(i));
    //    }
    settings.endArray();
    settings.beginWriteArray("TableWidget");

    for (int i = 0; i < ui->paramEditor->rowCount(); i++)
    {
        settings.setArrayIndex(i);
        settings.beginWriteArray("Column");

        for (int j = 0; j < ui->paramEditor->columnCount(); j++)
        {
            settings.setArrayIndex(j);

            if (j == 0)
            {
                settings.setValue("Cell", ui->paramEditor->item(i, j)->text());
            }

            if (j == 1)
            {
                QComboBox* tempBox = qobject_cast<QComboBox*>(ui->paramEditor->cellWidget(i, j));
                settings.beginWriteArray("ComboBox");

                for (int index = 0; index < tempBox->count(); index++)
                {
                    settings.setArrayIndex(index);
                    QString cell;
                    cell.setNum(index);
                    settings.setValue(cell, tempBox->itemText(index));
                }

                settings.endArray();
            }

            if (j == 2)
            {
                QLineEdit* tempEdit = qobject_cast<QLineEdit*>(ui->paramEditor->cellWidget(i, j));
                settings.setValue("Cell", tempEdit->text());
            }

        }

        settings.endArray();
    }

    settings.endArray();
}

void EventSenderConfigDialog::populateStateTreeList()
{
    ARMARX_VERBOSE << "Updating State tree" << flush;
    std::vector<StateIceBasePtr> baseStateList;
    std::vector<StateIceBasePtr> instanceStateList;
    std::string stateSearchMask = ui->edtStateSearchMask->text().toStdString();
    std::vector<std::string> remoteStateOffererList = iceManager->getIceGridSession()->getRegisteredObjectNames<RemoteStateOffererInterfacePrx>(stateSearchMask);
    //    RemoteStateOffererInterface::ice_staticId()
    QStringList componentNames;// = ui->EDTComponentName->text().split(';');

    for (unsigned int i = 0; i < remoteStateOffererList.size(); ++i)
    {
        componentNames << QString::fromStdString(remoteStateOffererList.at(i));
    }

    if (componentNames.size() == 0)
    {
        ARMARX_INFO << "No RemoteStateOfferer found.";
        return;
    }

    foreach (QString componentNameQStr, componentNames)
    {
        std::string componentName = componentNameQStr.trimmed().toStdString();

        try
        {
            getTopLevelStates(componentName, baseStateList, instanceStateList);
            model->updateModel(componentName, baseStateList, instanceStateList);
        }
        catch (Ice::NotRegisteredException& e)
        {
            ARMARX_WARNING << "The entered component with name '" << componentName << "' was not found - maybe you forgot to start it?" << flush;
        }
        catch (IceUtil::NullHandleException& e)
        {
            ARMARX_ERROR << "IceUtil::NullHandleException: " << e.what() << "\nFile: " << e. ice_file() << e.ice_line() << "\nBacktrace:\n" << e.ice_stackTrace()  << flush;
        }
        catch (armarx::UserException& e)
        {
            ARMARX_WARNING << "Found component but cannot communicate with component - probably it is a component of another type\nreason: " << e.reason << flush;
        }
    }
}



void
EventSenderConfigDialog::getTopLevelStates(std::string componentName, std::vector<StateIceBasePtr>& baseStateList, std::vector<StateIceBasePtr>& instanceStateList)
{
    RemoteStateOffererInterfacePrx statePrx;
    statePrx = iceManager->getProxy< armarx::RemoteStateOffererInterfacePrx >(componentName);

    if (!statePrx)
    {
        ARMARX_WARNING << "Proxy is NULL" << flush;
        return;
    }

    Ice::StringSeq availableStates = statePrx->getAvailableStates();
    baseStateList.clear();

    for (unsigned int i = 0; i < availableStates.size(); ++i)
    {
        baseStateList.push_back(statePrx->getStatechart(availableStates[i]));
    }

    StateIdNameMap availableStateInstances = statePrx->getAvailableStateInstances();
    instanceStateList.clear();
    StateIdNameMap::iterator it = availableStateInstances.begin();

    for (; it != availableStateInstances.end(); ++it)
    {
        instanceStateList.push_back(statePrx->getStatechartInstance(it->first));
    }

}

void EventSenderConfigDialog::stateSelected(int index)
{
    //send event
    QModelIndex modelIndex = treeBox->view()->currentIndex();
    //    ARMARX_LOG << "row : " << modelIndex.row() << flush;
    std::string globalStateId = treeBox->model()->itemData(modelIndex)[STATE_GLOBALSTATEID].toString().toStdString();
    std::string componentName = treeBox->model()->itemData(modelIndex)[STATE_COMPONENT_NAME].toString().toStdString();

    //    ARMARX_WARNING<< "State selection changed to "<< globalStateId << flush;
    if (treeBox->model()->itemData(modelIndex)[STATE_ITEM_TYPE] != StateItemModel::eInstanceStateItem)
    {
        ui->CBEvent->clear();
        ui->edtSelectedState->setText("");
        return;
    }

    StateIceBasePtr statePtr = model->getState(componentName, globalStateId);

    if (!statePtr)
    {
        ARMARX_WARNING << "Could not find state with id " << globalStateId << " of component '" << componentName << "'" << flush;
        return;
    }

    ui->edtSelectedState->setText(globalStateId.c_str());
    ui->CBEvent->clear();
    QStringList eventList = getPossibleEvents(statePtr);
    ui->CBEvent->addItems(eventList);


}

void EventSenderConfigDialog::setupUi()
{
    QObject::connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    QObject::connect(ui->buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

    model = new StateItemModel();
    treeBox = new TreeBox(model, true, this);
    ui->gridLayout->addWidget(treeBox, 3, 4, 1, 1);
    ui->paramEditor->setColumnHidden(StateParameterEditor::eOptional, true);
    ui->paramEditor->setColumnHidden(StateParameterEditor::eProvideDefaultValue, true);
    ui->paramEditor->setDefaultValueState(Qt::Checked);
    //    ui->paramEditor = new StateParameterEditor(ui->groupBox);
    //    ui->verticalLayout->addWidget(ui->paramEditor);

    //    addParameterRow();
    //    addParameterRow();
    //    if(ui->paramEditor->rowCount() < 2)
    //        return;
    //    if(!ui->paramEditor->item(0,0))
    //        return;
    //    ui->paramEditor->item(0,0)->setText("proxyName");
    //    ui->paramEditor->item(1,0)->setText("stateName");
    //    qobject_cast<QLineEdit*>(ui->paramEditor->cellWidget(0,2))->setText("VisualServoingExample");
    //    qobject_cast<QLineEdit*>(ui->paramEditor->cellWidget(1,2))->setText("VisualServoingExample");

    //    QComboBox* cbox = qobject_cast<QComboBox*>(ui->paramEditor->cellWidget(0, 1));
    //    cbox->setCurrentIndex(8);
    //    cbox = qobject_cast<QComboBox*>(ui->paramEditor->cellWidget(1, 1));
    //    cbox->setCurrentIndex(8);



    // setup eventsender slots
    connect(ui->addRowButton, SIGNAL(clicked()), ui->paramEditor, SLOT(addParameterRow()));
    connect(ui->sendButton, SIGNAL(clicked()), this, SLOT(send()));

    connect(ui->actionSaveAs, SIGNAL(triggered()), this, SLOT(saveEventSender()));
    connect(ui->BTNUpdateTreeView, SIGNAL(clicked()), this, SLOT(populateStateTreeList()));
    connect(treeBox, SIGNAL(currentIndexChanged(int)), this, SLOT(stateSelected(int)));
    ui->buttonBox->button(QDialogButtonBox::Ok)->setDefault(true);
    setConfig(EventSenderConfig());
}

QString EventSenderConfigDialog::getSelectedComponentName()
{
    QModelIndex index = treeBox->view()->currentIndex();
    return treeBox->model()->itemData(index)[STATE_COMPONENT_NAME].toString();
}

QString EventSenderConfigDialog::getSelectedState()
{
    QModelIndex index = treeBox->view()->currentIndex();
    return treeBox->model()->itemData(index)[STATE_GLOBALSTATEID].toString();
}




