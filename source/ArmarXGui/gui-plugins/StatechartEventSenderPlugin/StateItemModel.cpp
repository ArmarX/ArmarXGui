/* * This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Gui
* @author     Mirko Waechter (waechter@kit.edu)
* @copyright  2012 Humanoids Group, IAIM, IFA
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/



#include "StateItemModel.h"

#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/FilterableTreeView.h>


using namespace armarx;

StateItemModel::StateItemModel(QObject* parent)
    : QStandardItemModel(0, 1, parent)
{
    //    QStandardItem* item = new QStandardItem(FilterableTreeView::DefaultFilterStr);
    //    appendRow(item);
}

void StateItemModel::updateModel(const std::string& componentName, const std::vector<StateIceBasePtr>& topLevelBaseStates, const std::vector<StateIceBasePtr>& topLevelRequestedStates)
{
    if (componentName.empty())
    {
        return;
    }

    requestedInstanceList[componentName] = topLevelRequestedStates;

    // mark all elements for deletion
    markAllForDelete(invisibleRootItem());

    // insert observer
    QStandardItem* componentItem = updateComponent(componentName);

    // update states
    updateStates(componentItem->child(0), topLevelBaseStates, componentName, eBaseStateItem);
    updateStates(componentItem->child(1), topLevelRequestedStates, componentName, eInstanceStateItem);



    // delete not updated items
    deleteUnusedItems(componentItem);
}

StateIceBasePtr StateItemModel::getState(std::string componentName, const std::string&   globalStateIdStr)
{
    std::map<std::string, std::vector< StateIceBasePtr > > ::iterator it = requestedInstanceList.find(componentName);

    if (it == requestedInstanceList.end())
    {
        return NULL;
    }

    StateIceBasePtr result = NULL;

    for (unsigned int i = 0; i < it->second.size(); i++)
    {
        if (it->second[i]->globalStateIdentifier == globalStateIdStr)
        {
            return it->second[i];
        }

        findSubstate(it->second[i], globalStateIdStr, result);

        if (result)
        {
            return result;
        }
    }

    return result;
}


void StateItemModel::findSubstate(StateIceBasePtr state, const std::string&   globalStateIdStr, StateIceBasePtr& resultState)
{
    for (unsigned int i = 0; i < state->subStateList.size(); ++i)
    {
        StateIceBasePtr curState = StateIceBasePtr::dynamicCast(state->subStateList[i]);

        if (curState->globalStateIdentifier == globalStateIdStr)
        {
            resultState = curState;
            return;
        }

        findSubstate(curState, globalStateIdStr, resultState);

        if (resultState)
        {
            return;
        }
    }
}


QStandardItem* StateItemModel::updateComponent(std::string componentName)
{
    QStandardItem* componentItem = new QStandardItem(QString(componentName.c_str()));
    componentItem->setData(QVariant(eComponentItem), STATE_ITEM_TYPE);
    componentItem->setData(QVariant(componentName.c_str()), STATE_GLOBALSTATEID);
    componentItem->setData(QVariant(eUndefined), STATE_TYPE);

    componentItem = updateOrInsertItem(invisibleRootItem(), componentItem);

    // insert item for BaseStates
    QStandardItem* baseStateItem = new QStandardItem(QString(ARMARXGUI_BASEINSTANCE_STR));
    baseStateItem->setData(QVariant(ePlaceHolderItem), STATE_ITEM_TYPE);
    baseStateItem->setData(QVariant(ARMARXGUI_BASEINSTANCE_STR), STATE_GLOBALSTATEID);
    baseStateItem->setData(QVariant(eUndefined), STATE_TYPE);
    baseStateItem = updateOrInsertItem(componentItem, baseStateItem);

    // insert item for the requested state instances
    QStandardItem* requestedStateItem = new QStandardItem(QString(ARMARXGUI_REQUESTEDINSTANCE_STR));
    requestedStateItem->setData(QVariant(ePlaceHolderItem), STATE_ITEM_TYPE);
    requestedStateItem->setData(QVariant(ARMARXGUI_REQUESTEDINSTANCE_STR), STATE_GLOBALSTATEID);
    requestedStateItem->setData(QVariant(eUndefined), STATE_TYPE);
    requestedStateItem = updateOrInsertItem(componentItem, requestedStateItem);

    return componentItem;

}

void StateItemModel::updateStates(QStandardItem* componentItem, const std::vector<StateIceBasePtr>& topLevelStates, std::string componentName, eItemType itemType)
{

    for (unsigned int i = 0; i < topLevelStates.size(); ++i)
    {


        insertSubstates(componentItem, topLevelStates[i], componentName, itemType);
    }
}





QStandardItem* StateItemModel::updateOrInsertItem(QStandardItem* parent, QStandardItem* insert)
{
    // loop through childs and compare data
    for (int r = 0 ; r < parent->rowCount() ; r++)
    {
        QStandardItem* item = parent->child(r);

        //ARMARX_LOG_S << "insert data: " << insert->data(STATE_GLOBALSTATEID).toString().toStdString();
        if (item->data(STATE_GLOBALSTATEID) == insert->data(STATE_GLOBALSTATEID))
        {
            delete insert;
            item->setData(QVariant(false), STATE_ITEM_DELETE);
            return item;
        }
    }

    parent->appendRow(insert);

    insert->setData(QVariant(false), STATE_ITEM_DELETE);
    return insert;
}


QVariant StateItemModel::data(const QModelIndex& index, int role) const
{



    if (role == Qt::BackgroundColorRole)
    {
        if (not(itemData(index)[STATE_ITEM_TYPE].toInt() == eInstanceStateItem ||
                itemData(index)[STATE_ITEM_TYPE].toInt() == eBaseStateItem))
        {
            return QStandardItemModel::data(index, role);
        }

        QBrush brush;
        eStateType type = eStateType(itemData(index)[STATE_TYPE].toInt());

        switch (type)
        {
            case eNormalState:
                brush.setColor(Qt::white);
                break;

            case eRemoteState:
                brush.setColor(Qt::blue);
                break;

            case eDynamicRemoteState:
                brush.setColor(Qt::blue);
                brush.setColor(brush.color().lighter(120));
                break;

            case eFinalState:
                brush.setColor(Qt::yellow);
                break;

            default:
                brush.setColor(Qt::white);
        }

        brush.setColor(brush.color().lighter(170));

        return qVariantFromValue(brush.color());
    }
    else if (role == Qt::ToolTipRole)
    {
        eStateType type = eStateType(itemData(index)[STATE_TYPE].toInt());
        QString componentName = itemData(index)[STATE_COMPONENT_NAME].toString();
        QString globalId = itemData(index)[STATE_GLOBALSTATEID].toString();

        QString description;

        switch (type)
        {
            case eNormalState:
                description = "Normal State";
                break;

            case eRemoteState:
                description = "Remote State";
                break;

            case eDynamicRemoteState:
                description = "Dynamic Remote State";
                break;

            case eFinalState:
                description = "Final State";
                break;

            case eUndefined:
                description = "";
                break;

            default:
                description = "Unknown State Type";
        }

        if (description.length() > 0)
        {
            description += "; Id: " + globalId + "; Component Name: " + componentName;
        }

        return  qVariantFromValue(description);
    }
    else
    {
        return QStandardItemModel::data(index, role);
    }
}

// mark all elements for deletion
void StateItemModel::markAllForDelete(QStandardItem* stateItem)
{
    int nCount = 0;
    std::list<QStandardItem*> markList;
    markList.push_back(stateItem);

    while (markList.size() > 0)
    {
        QStandardItem* current = markList.front();
        markList.pop_front();
        current->setData(QVariant(true), STATE_ITEM_DELETE);
        nCount++;

        if (current->hasChildren())
        {
            for (int r = 0 ; r < current->rowCount() ; r++)
            {
                markList.push_back(current->child(r));
            }
        }
    }
}

void StateItemModel::deleteUnusedItems(QStandardItem* stateItem)
{
    std::list<QStandardItem*> searchList;
    std::vector<int> removeRows;

    searchList.push_back(stateItem);

    while (searchList.size() > 0)
    {
        QStandardItem* current = searchList.front();
        searchList.pop_front();

        if (current->hasChildren())
        {
            removeRows.clear();

            for (int r = 0 ; r < current->rowCount() ; r++)
            {
                if (current->child(r)->data(STATE_ITEM_DELETE).toBool())
                {
                    removeRows.push_back(r);
                }
                else
                {
                    searchList.push_back(current->child(r));
                }
            }

            // remove unused rows
            std::vector<int>::iterator iter = removeRows.begin();

            while (iter != removeRows.end())
            {
                current->removeRow(*iter);
                iter++;
            }
        }
    }
}

void StateItemModel::insertSubstates(QStandardItem* parentStateItem, StateIceBasePtr state, std::string componentName, eItemType itemType)
{

    // create item for substate
    QStandardItem* stateItem = new QStandardItem(QString(state->stateName.c_str()));
    //ARMARX_INFO_S << "Inserting " << state->stateName << " with " << state->globalStateIdentifier << flush;
    stateItem->setData(QVariant(itemType), STATE_ITEM_TYPE);
    stateItem->setData(QVariant(QString(state->globalStateIdentifier.c_str())), STATE_GLOBALSTATEID);
    stateItem->setData(QVariant(state->stateType), STATE_TYPE);
    RemoteStatePtr remoteState = RemoteStatePtr::dynamicCast(state);

    if (remoteState)
    {
        //        ARMARX_INFO_S << remoteState->stateName << " is remote state" << flush;
        componentName = remoteState->proxyName;
        //        ARMARX_INFO_S << "substatelist size: " << remoteState->subStateList.size() << flush;
    }

    stateItem->setData(QVariant(componentName.c_str()), STATE_COMPONENT_NAME);

    // insert state item into treeview
    stateItem = updateOrInsertItem(parentStateItem, stateItem);

    for (unsigned int i = 0; i < state->subStateList.size(); ++i)
    {
        StateIceBasePtr curState = StateIceBasePtr::dynamicCast(state->subStateList[i]);
        insertSubstates(stateItem, curState, componentName, itemType);
    }
}



bool armarx::StateItemModel::setData(const QModelIndex& index, const QVariant& value, int role)
{
    if (index.row() == 0 && index.parent() == invisibleRootItem()->index())
    {
        if (role == Qt::EditRole)
        {
            //            if(value.toString().length() > 0)
            //                searchString = value.toString();
            //            else
            //                searchString = DEFAULT_FILTER_CONTENT;
            //            emit dataChanged(index, index);
        }

    }

    return true;
}


//Qt::ItemFlags armarx::StateItemModel::flags(const QModelIndex &index) const
//{
//    if(index.row() == 0 && index.parent() == invisibleRootItem()->index() )
//    {
//        return Qt::ItemIsEnabled | Qt::ItemIsEditable;
//    }
//    else
//        return Qt::ItemIsEnabled | Qt::ItemIsSelectable ;
//    //QStandardItemModel::flags(index);

//}
