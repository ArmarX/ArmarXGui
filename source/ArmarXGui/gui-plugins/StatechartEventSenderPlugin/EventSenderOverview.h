/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Gui
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include "EventSenderConfig.h"

#include "EventSenderComponent.h"

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

// Qt
#include <QWidget>

/** ArmarX Headers */


namespace Ui
{
    class EventSenderOverview;
}
namespace armarx
{
    /*!
     \page ArmarXGui-GuiPlugins-EventSender EventSender
     \brief This widget allows the user to create event senders.
     \image html EventSender.png
     For an event the user can specify the receiving state and parameters.
        Every state that is added to a RemoteStateOfferer and instantiated
        can be controlled via Ice. <br/>
        This control is one-way, so you dont get any information back.
        To make this easier, there is a Gui-Widget that offers
        a general interface to such states. It is called EventSender.<br/>
        The control is event based. Like mentioned in \ref Eventprocessing
        state transitions are caused by events and the exact transitions are
        specified in a transition table. So to control a state, one has to
        send events with parameters to it.<br/>
        Usually such events are automatically send, if condition becomes true,
        that was installed by a state.<br/>
        But e.g. for debugging purposes or loading a new robot-program it is needed
        to send this events manually.
        To do this, you have to load the EventSender-Widget. In this widget,
        you can specify control-configurations. These configurations contain:
        - a name of the control-action
        - a description of the control-action
        - the receiving state
        - the event
        - a dictionary of event parameters
        \image html EventSenderGui.png "EventSenderGui"

        In details these mean:
        - The name and description are arbitrary and should only be a
        human readable description
        - The receiving state is the state that has the transition table
        with this event. In the field Component Names you have to enter
        the name of the component where the state is located in. If you
        want to load a robot-program into the robot-mainstatechart this is
        'RobotStatechart'. Next press the update-button besides
        the label 'Receiving State'. Now all found RemoteStateOfferers
        appear in the ComboBox next to this button. These
        RemoteStateOfferers contain 2 lists of states: 'Base instances'
        and 'Requested instances'. The base instances are the initial
        instances of states from which the requsted instances are copied
        from. They are only display to show the structure of the states
        and one cannot communicate with them.
        The requested instances are the important ones. These instances
        are created, if a state is called/entered. So in the beginning
        there are none, except for the robot-mainstatechart. The RobotStatechart
        creates automatically an instance, that is usable ("Requested Instance").
        In the tree under "Requested Instances" one can selected the desired instance
        and then the state to which the event should be sent. Once a state
        is selected, the event-combobox beneath is updated with corresponding
        events.
        \image html EventSenderGui-ComboBoxOpen.png "EventSenderGui-ComboBoxOpen"
        - In the event-combobox the user can selected the specific event,
        that should be sent to the statechart.
        - In the tableview the dictionary, that should be sent with the event,
        can be specified. The user can add an arbitrary number of parameters here.

        \subsection LoadingRobotProgram How to load a robot program
        To load a robot program (i.e. a statechart offered with a
        RemoteStateOfferer) into the running robot main statechart
        configure an EventSender like this:
        \image html RobotProgramLoading.png "RobotProgramLoading"
        The RobotStatechart must be in the states RobotStatechart->Functional->RobotInitialized
        (it is automatically in this state after scenario start, but you can re-enter
         this state by restarting the statechart, see below). <br/>
         The parameter field 'proxyName' has to contain the component-name
         of the RemoteStateOfferer, where the state resides in.<br/>
         The parameter field 'stateName' has to be the name of the state,
         that should be loaded into the Robot-main-statechart. This state is
         then automatically entered, when the event was sent.

        \subsection RestartingRobotStatechart How to restart the RobotStatechart
        To restart the RobotStatechart send an Event EvStopRobot (see image below) and an Event EvStartRobot respectively.
        \image html EvStopRobot.png "EvStopRobot"

        For detailed explanations see the class-documentations themself.



     \see  \ref EventSenderOverview
     \see \ref ArmarXGui-GuiPlugins-StatechartViewer
    */
    /*!
     * \class EventSenderOverview
     * \see StatechartEventSenderPlugin
     */
    class EventSenderOverview :
        public ArmarXComponentWidgetControllerTemplate<EventSenderOverview>

    {
        Q_OBJECT
        int lastRowInsertion;
        int lastColumnInsertion;
    public:
        explicit EventSenderOverview();
        ~EventSenderOverview() override;

        //inherited from ArmarXWidget
        static QString GetWidgetName()
        {
            return "Statecharts.EventSender";
        }

        void loadSettings(QSettings* settings) override;
        void saveSettings(QSettings* settings) override;
        // inherited from Component
        void onInitComponent() override;
        void onConnectComponent() override {}
        // end of inherited from Component
        // end of inherited from Component
        EventSenderComponent* addEventSender(const EventSenderConfig& config);
    public slots:
        void addEventSender();
        void sendEvent(const EventSenderConfig& config);
    private:
        Ui::EventSenderOverview* ui;

        //        std::vector<EventSenderComponent*> eventSenders;
    };
}
