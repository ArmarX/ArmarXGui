/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Gui
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include "EventSenderConfig.h"

#include <ArmarXCore/core/exceptions/user/NotImplementedYetException.h>

#include <QGridLayout>
#include <QGroupBox>
#include <QToolButton>
#include <QObject>

namespace armarx
{
    class EventSenderComponent :
        public QGroupBox,
        virtual public Logging
    //            virtual public Component

    {
        Q_OBJECT
    public:
        EventSenderConfig config;

        EventSenderComponent(QString eventSenderName,   QWidget* parent = 0);
        ~EventSenderComponent() override;



        // end of inherited from Component
        //        QWidget* getMainWidget();
        void setupUi();
        void setConfig(const EventSenderConfig& conf);
        void setIceManager(IceManagerPtr iceMan);

    signals:
        void sendEvent(const EventSenderConfig& config);
    public slots:
        void sendClicked();
        void openConfig();
        void deleteClicked();
        void configDone();

    private:

        //        QGroupBox *gbEventSender;
        QHBoxLayout* horizontalLayout;
        QToolButton* tbSend;
        QToolButton* tbConfig;
        QToolButton* tbDelete;
        EventSenderConfigDialog* dialog;

        IceManagerPtr iceManager;
        int mdiId;
    };
}
