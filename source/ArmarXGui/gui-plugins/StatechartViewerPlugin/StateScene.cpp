/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "StateScene.h"

#include "view/StateItem.h"
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXWidgetController.h>
#include "model/State.h"
#include "model/stateinstance/LocalState.h"
#include <QMenu>
#include <QPainter>
#include <QtSvg/QSvgGenerator>


namespace armarx
{
    StateScene::StateScene(QObject* parent) :
        QGraphicsScene(parent)
    {
        setItemIndexMethod(QGraphicsScene::ItemIndexMethod::NoIndex);
    }

    statechartmodel::StateInstancePtr StateScene::getStateInstance() const
    {
        return toplevelStateInstance;
    }

    void StateScene::setToplevelState(statechartmodel::StatePtr toplevelState)
    {
        clear();
        statechartmodel::StateInstancePtr instance(new statechartmodel::LocalState(toplevelState, ""));
        this->toplevelStateInstance = instance;
        StateItem* toplevelItem = new StateItem(instance);
        addItem(toplevelItem);
        topLevelStateItem = toplevelItem;
    }

    void StateScene::saveSceneToSVG(QString path, int width)
    {
        QImage img(width, width * sceneRect().height() / sceneRect().width(), QImage::Format_ARGB32);
        img.fill(Qt::transparent);


        QPainter painter(&img);
        painter.setRenderHint(QPainter::Antialiasing);
        render(&painter);
        img.save(path);
    }

    void StateScene::clearActiveSubstates()
    {
        std::function<void(StateItem*)> clearRecursive;
        clearRecursive = [&](StateItem * item)
        {
            if (!item)
            {
                return;
            }
            for (auto substateItem : item->getSubstateItems())
            {
                clearRecursive(substateItem);
            }
            item->clearActiveSubstate();
        };

        if (topLevelStateItem)
        {
            clearRecursive(topLevelStateItem);
        }
    }

    QMap<QString, StateInstanceData> StateScene::getStateInstanceData() const
    {
        QMap<QString, StateInstanceData> stateInstanceData;

        std::function<QMap<QString, StateInstanceData>(StateItem*)> getChildren;
        getChildren = [&](StateItem * stateItem)
        {
            QMap<QString, StateInstanceData> stateInstanceData;
            for (StateItem* substateItem : stateItem->getSubstateItems())
            {
                auto fullpath = substateItem->getFullStatePath();
                StateInstanceData data;
                data.active = substateItem->isActive();
                data.fullStatePath = fullpath;
                data.stateInstance = substateItem->getStateInstance();
                stateInstanceData[fullpath] = data;
                auto map = getChildren(substateItem);
                for (auto& key : map.keys())
                {
                    stateInstanceData[key] = map[key];
                }
            }
            return stateInstanceData;
        };

        if (topLevelStateItem)
        {
            stateInstanceData = getChildren(topLevelStateItem);
            auto fullpath = topLevelStateItem->getFullStatePath();
            StateInstanceData data;
            data.active = topLevelStateItem->isActive();
            data.fullStatePath = fullpath;
            data.stateInstance = topLevelStateItem->getStateInstance();
            stateInstanceData[fullpath] = data;
        }

        return stateInstanceData;
    }

    StateItem* StateScene::getTopLevelStateItem() const
    {
        return topLevelStateItem;
    }

}
