/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX
* @author     Mirko Waechter( mirko.waechter at kit dot edu)
* @date       2016
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#include "ActiveStateFollower.h"

#include <ArmarXGui/gui-plugins/StatechartViewerPlugin/view/StateItem.h>

namespace armarx
{

    ActiveStateFollower::ActiveStateFollower(StatechartView* statechartView, QWidget* parent) :
        QVariantAnimation(parent),
        statechartView(statechartView)
    {
        duration = 1000;
        connect(&activeStateCheckerTimer, SIGNAL(timeout()), this, SLOT(checkActiveState()));
        activeStateCheckerTimer.setInterval(1000);
        //        activeStateCheckerTimer.start();
    }

    void ActiveStateFollower::toggle(bool on)
    {
        if (on)
        {
            startFollowing();
        }
        else
        {
            stopFollowing();
        }
    }

    void ActiveStateFollower::startFollowing()
    {
        activeStateCheckerTimer.start();
    }

    void ActiveStateFollower::stopFollowing()
    {
        activeStateCheckerTimer.stop();
    }

    void ActiveStateFollower::checkActiveState()
    {
        if (!statechartView)
        {
            return;
        }
        auto state = statechartView->getStateInstance();
        if (!state || !state->getStateClass())
        {
            return;
        }
        statechartmodel::StateInstancePtr activeState = state->getStateClass()->getActiveSubstate();
        while (activeState)
        {
            if (!activeState->getStateClass() || !activeState->getStateClass()->getActiveSubstate())
            {
                break;
            }
            activeState = activeState->getStateClass()->getActiveSubstate();
        }

        if (!activeState || activeState == currentActiveState)
        {
            return;
        }
        auto items = statechartView->getScene()->items();
        QPointer<StateItem> stateItem;
        for (QGraphicsItem* item : items)
        {
            stateItem = dynamic_cast<StateItem*>(item);
            if (stateItem && activeState == stateItem->getStateInstance())
            {
                break;
            }
        }
        if (currentActiveStateItem == stateItem)
        {
            return;
        }
        stop();
        currentActiveStateItem = stateItem;
        currentTargetPos = stateItem->mapToScene(stateItem->scenePos());
        startPos = statechartView->getGraphicsView()->mapToScene(statechartView->getGraphicsView()->viewport()->rect()).boundingRect().center();
        if (statechartView->getGraphicsView()->viewport()->rect().adjusted(50, 50, -50, -50).
            contains(statechartView->getGraphicsView()->mapFromScene(currentTargetPos)))
        {
            ARMARX_DEBUG_S << "already visible: " << currentTargetPos;
            return;
        }
        ARMARX_INFO_S << "travel distance: " << (startPos - currentTargetPos).manhattanLength();
        //        if ((startPos - currentTargetPos).manhattanLength() < 1) // dont do marginal motions
        //        {
        //            return;
        //        }
        startTime = IceUtil::Time::now();
        setStartValue(startPos);
        setEndValue(currentTargetPos);
        setEasingCurve(QEasingCurve::InOutQuad);
        setDuration(duration);
        start();
    }

    StatechartView* ActiveStateFollower::getStatechartView() const
    {
        return statechartView;
    }

    void ActiveStateFollower::setStatechartView(StatechartView* value)
    {
        statechartView = value;
    }

} // namespace armarx


void armarx::ActiveStateFollower::updatePos(QVariant value)
{


}

void armarx::ActiveStateFollower::centerOnCurrentState(bool toggle)
{
    ARMARX_INFO_S << "Manually triggered centering on state";
    currentActiveStateItem = NULL;
    checkActiveState();
}


void armarx::ActiveStateFollower::updateCurrentValue(const QVariant& value)
{
    //    ARMARX_INFO_S << "cur ani value : " << value.toPointF();
    statechartView->getGraphicsView()->centerOn(value.toPointF());
    //    statechartView->getGraphicsView()->ensureVisible(QRectF(value.toPointF(), QSizeF(1, 1)));

}
