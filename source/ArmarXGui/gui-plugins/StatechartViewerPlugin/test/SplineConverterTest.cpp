/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#define BOOST_TEST_MODULE ArmarX::Gui::SplineConverterTest
#define ARMARX_BOOST_TEST

#include <graphviz/types.h>
#include <graphviz/graph.h>
#include <graphviz/gvc.h>

#include <Eigen/Core>

#include <QMainWindow>
#include <QPainter>
#include <QPainterPath>
#include <QPointF>
#include <QApplication>
#include <QWidget>

#include <cassert>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include <Gui/Test.h>

#include "Gui/gui_plugins/StatechartViewerPlugin/SplineConverter.h"
#include "../splines/Beziercurve.h"
#include "../splines/BSpline.h"
#include "../splines/Spline.h"
#include "../splines/SplineFitting.h"

std::vector<float> pointsToFloat(std::vector<QPointF> points)
{
    std::vector<float> floats;

    for (auto point : points)
    {
        floats.push_back(point.x());
        floats.push_back(point.y());
    }

    return floats;
}

BOOST_AUTO_TEST_CASE(fullSplineTokenizeTest)
{
    std::string graphVizSpline = "e,5,5 s,0,0 1,1 2,2 3,3 4,4";
    armarx::SplineConverter converter {};
    armarx::SplineType spline = converter.convert(graphVizSpline);

    armarx::SplineType referenceSpline;
    referenceSpline.startPoint = std::make_shared<QPointF>(QPointF(0, 0));
    referenceSpline.endPoint = std::make_shared<QPointF>(QPointF(5, 5));
    referenceSpline.controlPoints.push_back(QPointF(1, 1));
    referenceSpline.controlPoints.push_back(QPointF(2, 2));
    referenceSpline.controlPoints.push_back(QPointF(3, 3));
    referenceSpline.controlPoints.push_back(QPointF(4, 4));

    BOOST_CHECK_EQUAL(referenceSpline.startPoint->x(), spline.startPoint->x());
    BOOST_CHECK_EQUAL(referenceSpline.startPoint->y(), spline.startPoint->y());
    BOOST_CHECK_EQUAL(referenceSpline.endPoint->x(), spline.endPoint->x());
    BOOST_CHECK_EQUAL(referenceSpline.endPoint->y(), spline.endPoint->y());

    std::vector<float> referenceControlPoints = pointsToFloat(referenceSpline.controlPoints);
    std::vector<float> controlPoints = pointsToFloat(spline.controlPoints);
    BOOST_CHECK_EQUAL_COLLECTIONS(referenceControlPoints.begin(), referenceControlPoints.end(),
                                  controlPoints.begin(), controlPoints.end());
}

class PaintWidget : public QWidget
{
public:
    PaintWidget(std::string graphVizSpline) :
        QWidget(),
        stretch(3),
        renderPrecicision(0.0005)
    {
        m_path = convertToPath(graphVizSpline);
    }

    void paintEvent(QPaintEvent*)
    {
        QPainter painter(this);
        painter.setPen(QColor("black"));
        painter.drawPath(m_path);
    }

    void addPath(std::string newPath)
    {
        m_path.addPath(convertToPath(newPath));
    }

private:
    QPainterPath m_path;
    size_t stretch;
    float renderPrecicision;

    typedef Eigen::Spline<float, 2> Spline2f;

    QPainterPath convertToPath(std::string graphVizSpline)
    {
        armarx::SplineConverter converter {};
        armarx::SplineType spline = converter.convert(graphVizSpline);

        //Punkte umkehren
        armarx::QPointVector flippedControls;

        for (QPointF point : spline.controlPoints)
        {
            point.setY(130 - point.y());
            flippedControls.push_back(point);
        }

        spline.controlPoints = flippedControls;

        if (spline.startPoint)
        {
            spline.startPoint->setY(130 - (spline.startPoint->y()));
        }

        if (spline.endPoint)
        {
            spline.endPoint->setY(130 - spline.endPoint->y());
        }

        //Punkte strecken
        armarx::QPointVector stretchedControls;

        for (QPointF point : spline.controlPoints)
        {
            point = stretch * point;
            stretchedControls.push_back(point);
        }

        spline.controlPoints = stretchedControls;

        if (spline.startPoint)
        {
            spline.startPoint = std::make_shared<QPointF>(stretch * (*(spline.startPoint)));
        }

        if (spline.endPoint)
        {
            spline.endPoint = std::make_shared<QPointF>(stretch * (*(spline.endPoint)));
        }

        printArmarxSpline(spline);

        /* for using Eigen
        const Spline2f interpolatedSpline = calcEigenSpline(spline);

        //print spline values
        printEigenSpline(interpolatedSpline);

        //rebuild as QPainterPath
        constructPath(interpolatedSpline);
        */

        // using Beziercurves
        const armarx::BSpline bezierSpline = calcBezierSpline(spline);

        //construct QPainterPath
        QPainterPath path = constructPath(bezierSpline);

        /*
                //with SplineConverter and QPainterPath.cubicTo()
                QPainterPath path = converter.calcSingleSpline(spline);
        */
        //print circles on control points
        for (QPointF controlPoint : spline.controlPoints)
        {
            path.addEllipse(controlPoint, 5, 5);
        }

        if (spline.startPoint)
        {
            path.addEllipse(*(spline.startPoint), 5, 5);
        }

        if (spline.endPoint)
        {
            path.addEllipse(*(spline.endPoint), 5, 5);
        }

        //print supposed tangents
        for (size_t i = 3; i < spline.controlPoints.size() - 1; i += 3)
        {
            path.moveTo(spline.controlPoints.at(i - 1));
            path.lineTo(spline.controlPoints.at(i + 1));
        }

        return path;
    }

    const armarx::BSpline calcBezierSpline(armarx::SplineType spline)
    {
        armarx::pointVector controls;

        for (QPointF point : spline.controlPoints)
        {
            Eigen::VectorXf v(2);
            v << point.x(), point.y();
            controls.push_back(v);
        }

        return armarx::BSpline(controls, 3);
    }

    const Spline2f calcEigenSpline(armarx::SplineType spline)
    {
        size_t splineSize = spline.controlPoints.size();

        Eigen::MatrixXf points(2, splineSize);

        for (size_t i = 0; i < splineSize; i++)
        {
            points(0, i) = spline.controlPoints.at(i).x();
            points(1, i) = spline.controlPoints.at(i).y();
        }

        //calculate knot vector
        Spline2f::KnotVectorType knots(7);
        knots << 0, 0.1, 0.25, 0.5, 0.75, 0.9, 1;

        return Eigen::SplineFitting<Spline2f>::Interpolate(points, 3, knots);
    }

    QPainterPath constructPath(const armarx::BSpline bezierSpline)
    {
        QPainterPath path;
        Eigen::VectorXf value = bezierSpline.evaluate(0);
        assert(value.innerSize() == 2);
        path.moveTo(value(0), value(1));

        for (float i = 0; i < bezierSpline.maxEvalFloat(); i += renderPrecicision)
        {
            value = bezierSpline.evaluate(i);
            assert(value.innerSize() == 2);
            path.lineTo(value(0), value(1));
        }

        return path;
    }

    QPainterPath constructPath(const Spline2f interpolatedSpline)
    {
        QPainterPath path;
        Eigen::Vector2f value = interpolatedSpline(0);
        path.moveTo(value(0), value(1));

        for (float i = 1; i < 1 + renderPrecicision; i += renderPrecicision)
        {
            value = interpolatedSpline(i);
            path.lineTo(value(0), value(1));
        }

        return path;
    }

    void printEigenSpline(const Spline2f eigenSpline)
    {
        for (float i = 0; i <= 1; i += 0.05)
        {
            Eigen::Vector2f value = eigenSpline(i);
            std::cout << "i = " << i << " : (x,y) = " << value(0) << ", " << value(1) << std::endl;
        }
    }

    void printArmarxSpline(const armarx::SplineType spline)
    {
        std::cout << "Interpolated points:" << std::endl;

        for (QPointF point : spline.controlPoints)
        {
            std::cout << "(x,y) = (" << point.x() << ", " << point.y() << ")" << std::endl;
        }
    }
};
/*
class ArrowWidget : public QWidget
{
public:
    ArrowWidget(QPointF start, QPointF destination)
        : QWidget()
    {
        armarx::SplineConverter converter{};
        m_arrow = converter.calcArrowhead(start, destination);

        m_helpPath.moveTo(start);
        m_helpPath.lineTo(destination);
        m_helpPath.addEllipse(start, 5, 5);
        m_helpPath.addEllipse(destination, 5, 5);
    }

    void paintEvent(QPaintEvent*)
    {
        QPainter painter(this);
        painter.setPen(QColor("black"));
        painter.drawPath(m_arrow);

        QPainter helpPainter(this);
        helpPainter.setPen(QColor("red"));
        helpPainter.drawPath(m_helpPath);
    }

private:
    QPainterPath m_arrow;
    QPainterPath m_helpPath;
};
*/
BOOST_AUTO_TEST_CASE(EigenSplineTest)
{
    //create QApplication
    char* args[1];
    args[0] = "SplineConverterTest";
    int argc = 1;
    QApplication app {argc, args};

    PaintWidget* myWidget {new PaintWidget("e,142.11,37.618 171.81,98.477 162.14,92.518 152.28,84.389 146.8,74 142.61,66.046 141.37,56.503 141.47,47.615")};
    myWidget->addPath("e,110.97,20.932 162.81,109.36 112.79,106.23 26.203,97.494 6.8049,74 -21.485,39.738 50.232,26.704 100.85,21.828");
    myWidget->addPath("e,349.61,37.267 233.98,106.78 260.02,102.22 295.26,92.879 320.8,74 330.58,66.772 338.59,56.142 344.63,46.149");
    myWidget->addPath("e,142.11,37.618 171.81,98.477 162.14,92.518 152.28,84.389 146.8,74 142.61,66.046 141.37,56.503 141.47,47.615");
    myWidget->addPath("e,217.76,94.791 179.45,24.687 197.66,29.665 218.66,38.915 229.8,56 236.26,65.898 231.78,77.214 224.43,86.998");
    myWidget->show();

    myWidget->update();
    /*
        ArrowWidget* widget{new ArrowWidget(QPointF(100,100), QPointF(200,200))};
        widget->show();
    */
    app.exec();
}

BOOST_AUTO_TEST_CASE(splineTest)
{
    Agraph_t* g;
    Agnode_t* l, *m, *r;
    Agedge_t* e1, *e2, *straight, *anti;
    GVC_t* gvc = gvContext();

    /* Create a simple digraph */
    g = agopen("g", AGDIGRAPH);
    l = agnode(g, "left");
    r = agnode(g, "right");
    m = agnode(g, "middle");
    e1 = agedge(g, l, r);
    e2 = agedge(g, l, r);
    straight = agedge(g, l, m);
    anti = agedge(g, r, l);

    //Set node positions
    agsafeset(l, "pos", "35,19", "0,0");
    agsafeset(m, "pos", "135,19", "0,0");
    agsafeset(r, "pos", "235,19", "0,0");


    agsafeset(l, "size", "1", "0,0");
    agsafeset(m, "size", "1", "0,0");
    agsafeset(r, "size", "1", "0,0");

    agsafeset(l, "width", "1", "0,0");
    agsafeset(m, "size", "1", "0,0");
    agsafeset(r, "size", "1", "0,0");

    agsafeset(e1, "label", "Hier ist ein Label", "leeres Label");

    /*get control points from graphviz*/
    FILE* delFile = fopen("/home/SMBAD/scherer/home/armarx/Gui/positionsBeforeLayouting.txt", "w+");
    fclose(delFile);
    std::fstream otherFileStream {"/home/SMBAD/scherer/home/armarx/Gui/positionsBeforeLayouting.txt"};
    otherFileStream << "Node left: " << agget(l, "pos") << std::endl;
    otherFileStream << "Node middle: " << agget(m, "pos") << std::endl;
    otherFileStream << "Node right: " << agget(r, "pos") << std::endl;

    /*Layout and render the graph*/
    gvLayout(gvc, g, "dot");
    gvRender(gvc, g, "dot", NULL);

    gvRenderFilename(gvc, g, "png", "/home/SMBAD/scherer/home/armarx/Gui/testGraph.png");
    gvRenderFilename(gvc, g, "dot", "/home/SMBAD/scherer/home/armarx/Gui/testGraph.dot");

    /*get control points from graphviz*/
    delFile = fopen("/home/SMBAD/scherer/home/armarx/Gui/positions.txt", "w+");
    fclose(delFile);
    std::fstream fileStream {"/home/SMBAD/scherer/home/armarx/Gui/positions.txt"};
    fileStream << "Node left: " << agget(l, "pos") << std::endl;
    fileStream << "Node middle: " << agget(m, "pos") << std::endl;
    fileStream << "Node right: " << agget(r, "pos") << std::endl;
    //    fileStream << "Edge e1: " << agget(e1, "pos") << std::endl;
    fileStream << "Edge e2: " << ((agget(e2, "pos") == 0) ? "no postion provided" : agget(e2, "pos")) << std::endl;
    fileStream << "Edge straight: " << agget(straight, "pos") << std::endl;
    fileStream << "Edge anti: " << agget(anti, "pos") << std::endl;

    /* Free layout data */
    gvFreeLayout(gvc, g);

    /* Free graph structures */
    agclose(g);

    /* close output file, free context, and return number of errors */
    gvFreeContext(gvc);
}
