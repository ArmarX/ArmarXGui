/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include <cassert>
#include <limits>

#include "Beziercurve.h"

armarx::Beziercurve::Beziercurve(pointVector controlPoints, size_t degree, size_t index)
    : m_degree(degree),
      m_index(index),
      m_controlPoints(controlPoints)
{}

armarx::Beziercurve::Beziercurve(armarx::pointVector controlPoints)
    : m_degree(controlPoints.size() - 1),
      m_index(0),
      m_controlPoints(controlPoints)
{}

Eigen::VectorXf armarx::Beziercurve::evaluate(float t) const
{
    assert((t >= 0) && (t <= 1.0 + std::numeric_limits<float>::epsilon()));

    if (m_degree == 0)
    {
        return m_controlPoints.at(m_index);
    }

    Beziercurve left {m_controlPoints, m_degree - 1, m_index};
    Beziercurve right {m_controlPoints, m_degree - 1, m_index + 1};

    assert((m_degree > 0) && (m_degree < m_controlPoints.size()));
    assert(m_index < m_controlPoints.size() - m_degree);

    return (1 - t) * left.evaluate(t) + t * right.evaluate(t);
}
