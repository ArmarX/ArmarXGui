/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <vector>

#include <Eigen/Core>

namespace armarx
{

    using pointVector = std::vector<Eigen::VectorXf>;

    class Beziercurve
    {
    public:
        Beziercurve(pointVector controlPoints);

        Eigen::VectorXf evaluate(float t) const;

    private:
        Beziercurve(pointVector controlPoints, size_t degree, size_t index);

        /**
         * @brief m_degree Degree of the Beziercurve, that means (number of control points) + 1.
         */
        size_t m_degree;

        /**
         * @brief m_index Index of the Bezier curve. For B(i,j) i is the index and j is the degree.
         */
        size_t m_index;

        pointVector m_controlPoints;
    };

} //namespace armarx
