/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "../model/SignalType.h"
#include "../model/State.h"
#include "../model/stateinstance/StateInstance.h"
#include "../model/Transition.h"
#include "GraphvizConverter.h"
#include "Layout.h"
#include "StateModelLayoutMediator.h"

#include <QList>
#include <QObject>
#include <QPointF>
#include <QSizeF>
#include <QString>

#include <map>
#include <memory>
#include <string>

namespace armarx
{

    using NodeMap = std::map<statechartmodel::StateInstancePtr, std::string>;
    using EdgeList = QList<statechartmodel::TransitionCPtr>;

    class StateModelLayoutMediator : public QObject
    {
        Q_OBJECT

    public:
        /**
         * @brief LayoutWorker Sets state and id as specified. Initializes an empty Graph.
         * @param state The state which this worker layouts.
         * @param id The mediator's ID for communication with the LayoutController.
         */
        StateModelLayoutMediator(statechartmodel::StatePtr state, size_t mediator_id);

        /**
         * @brief getState Returns the state associated with this mediator.
         * @return The state that is layouted by the mediator's worker.
         */
        statechartmodel::StatePtr getState() const;
        /**
         * @brief getID Returns this mediator's id.
         * @return ID for communication with the LayoutController
         */
        size_t getID() const;

        /**
         * @brief isConnected Indicates whether this mediator is connected to a worker.
         * @return True if it is connected to a worker.
         */
        bool isConnected();

        template <typename ElementType>
        bool hasGraphAttribute(ElementType* element, const std::string& attributeName) const
        {
            ARMARX_CHECK_EXPRESSION(element);
            char* value = agget(element, const_cast<char*>(attributeName.c_str()));
            return value != NULL;
        }

        template <typename ElementType>
        std::string getGraphAttribute(ElementType* element, const std::string& attributeName) const
        {
            ARMARX_CHECK_EXPRESSION(element);
            char* value = agget(element, const_cast<char*>(attributeName.c_str()));
            ARMARX_CHECK_EXPRESSION(value) << "Could not get attribute '" << attributeName << "'' from element";
            return std::string(value);
        }

        template <typename ElementType, typename ValueType>
        int setGraphAttribute(ElementType* element, const std::string& attributeName, const ValueType& value) const
        {
            ARMARX_CHECK_EXPRESSION(element);
            //            ARMARX_INFO_S   << "Setting " << attributeName << " to " << ValueToString(value);
            return agsafeset(element, const_cast<char*>(attributeName.c_str()), const_cast<char*>(ValueToString(value).c_str()), const_cast<char*>(""));
        }
        QRectF boundingRectForAgraph(Agraph_t* graph) const;
    signals:
        /**
         * @brief supportPointsChanged Notifies of changed support points of the given transition.
         * @param transition Transition whose support points have changed.
         * @param suppPoints New support points.
         */
        void supportPointsChanged(statechartmodel::TransitionCPtr transition, const SupportPoints& suppPoints, const QPointPtr& labelCenterPoint, const FloatPtr& labelFontPointSize);

        /**
         * @brief layout Tells worker to layout the graph.
         * @param layoutAll True: layout nodes and edgese. False: layout only edges.
         */
        void layout(bool layoutAll);
        /**
         * @brief layoutingFinished Notifies that the latest layout task was finished.
         */
        void layoutingFinished();

        /**
         * @brief scheduleMe To be called when its graph should be layouted. Is meant for the Controller
         * to schedule this mediator in its layout queue.
         * @param id This mediator's id.
         * @param layoutAll Indicates whether the whole graph (including nodes) should be layouted or only
         * the edges.
         */
        void scheduleMe(size_t id, bool layoutAll);

        /**
         * @brief deleteMe To be emitted when this mediator and its worker ought to be deleted because
         * their corresponding state was deleted.
         * @param id This mediator's id.
         */
        void deleteMe(size_t id);
        /**
         * @brief mediatorDeleted Tells worker that is has to be deleted.
         */
        void mediatorDeleted();

        /**
         * @brief substateFound To be emitted when a substate is found when building up the graph for the
         * first time.
         * @param substate The substate that was discovered.
         * @param signalType Type of change to the substate. Should be eAdded.
         */
        void substateFound(statechartmodel::StateInstancePtr substate, statechartmodel::SignalType signalType);

    public slots:
        /**
         * @brief buildUpGraph To be called when the corresponding worker initialized the graph. Traverses
         * the state and adds all transitions and states to the graph as well as to EdgeList and NodeMap.
         * @param graph The graphviz graph that shall represent the state.
         */
        void buildUpGraph(LockableGraphPtr graph);


        void stateChanged(statechartmodel::SignalType signalType);

        /**
         * @brief substateAdded To be called when a substate of state has changed.
         * @param substate The changed substate.
         * @param signalType The type of signal (added, removed, moved etc.).
         */
        void substateChanged(statechartmodel::StateInstancePtr substate, statechartmodel::SignalType signalType);

        /**
         * @brief transitionAdded To be called when a transition of state has changed.
         * @param transition The changed transition.
         * @param signalType The type of signal (added, removed, moved etc.).
         */
        void transitionChanged(statechartmodel::TransitionCPtr transition, statechartmodel::SignalType signalType);

        /**
         * @brief layout Tells the worker to layout edges and, if layoutAlgo is true, nodes of the graph.
         * Signals changes of positions to the state.
         * @param layoutAll If true, nodes and edges are layouted, if false, node positions are assumed
         * to be fixed and only edges are routed.
         */
        void startLayouting(bool layoutAll, int counter);

        /**
         * @brief workerFinishedLayouting To be called when the worker is finished layouting.
         * Notifies the controller.
         */
        void workerFinishedLayouting();

        /**
         * @brief stateDeleted To be called when the corresponding state is deleted.
         */
        void stateDeleted();

    private:

        bool updateInitialTransition(statechartmodel::TransitionCPtr transition, statechartmodel::SignalType signalType);


        QPointF convertPositionFromGraphViz(QPointF gvPos) const;
        float convertScaleFromGraphViz() const;
        QPointF convertPositionToGraphViz(QPointF statePos) const;

        /**
         * @brief IsConnectedToWorker Indicates whether this mediator is already connected to a worker.
         * Necessary for the createAllWorkers() function of LayoutController.
         */
        bool isConnectedToWorker;
        /**
         * @brief id ID of this mediator. Used for communication with the LayoutController.
         */
        size_t id;

        /**
         * @brief state The state whose subgraph is layouted by this mediator's worker.
         */
        statechartmodel::StatePtr m_state;

        /**
         * @brief converter Converts strings from graphviz to the corresponding point or splineType.
         */
        //TODO: Funktionen aus GraphvizConverter allein stehend machen. Converter hier und in SplinePath
        //      eliminieren
        GraphvizConverter converter;

        /**
         * @brief graph Graphviz graph that represents the subgraph of state and a mutex to lock it.
         */
        LockableGraphPtr m_graph;
        /**
         * @brief substateMap maps the substates of state to the name of their corresponding graphviz node.
         * Necessary for changing the name of a node.
         */
        NodeMap substateMap;
        /**
         * @brief transitionMap maps the transitions of state to the label of their corresponding graphviz
         * edge. Necessary for informing the state of all transition changes.
         */
        EdgeList transitionList;

        NodePtr invisibleStartNode;
        EdgePtr initialTransition;

        QRectF graphVizBB;

        QSizeF dpi;
        int layoutRepetition;

        /**
         * @brief addNode Adds a node with the given name to the graph and to substateMap.
         * @param substate The substate for which a node is added.
         */
        void addNode(statechartmodel::StateInstancePtr substate);
        /**
         * @brief removeNode Removes a node and its adjacent edges from the graph and from substateMap/
         * transitionMap.
         * @param substate The substate for which the node is removed.
         */
        void removeNode(statechartmodel::StateInstancePtr substate);
        /**
         * @brief setNodeAttribute Sets the attribute of the node belonging to substate.
         * @param substate Substate that is represented by the node whose attribute is to be set.
         * @param attributeName Name of the attribute, e.g. "pos".
         * @param attributeValue Value of the attribute.
         */
        void setNodeAttribute(statechartmodel::StateInstancePtr substate, const std::string& attributeName,
                              const std::string& attributeValue);
        /**
         * @brief getNodeAttribute Returns the attribute of the node belonging to substate.
         * @param substate Substate that is represented by the node whose attribute is returned.
         * @param attributeName Name of the attribute, e.g. "pos".
         * @return Value of the attribute.
         */
        std::string getNodeAttribute(statechartmodel::StateInstancePtr substate, const std::string& attributeName);
        bool hasNodeAttribute(armarx::statechartmodel::StateInstancePtr substate, const std::string& attributeName);

        /**
         * @brief getNodeName Returns the name of the given node. NOT THREAD-SAFE! LOCK GRAPH BEFORE USAGE!
         * @param node A node of the graph.
         * @return
         */
        std::string getNodeName(NodePtr node);
        /**
         * @brief getNode Return the node with the given name. NOT THREAD-SAFE! LOCK GRAPH BEFORE USAGE!
         * @param name Name of the node.
         * @return Returns the node with the given name or NULL if no such node exists.
         */
        NodePtr getNode(const std::string& name);
        /**
         * @brief getNode Return the node belonging to the substate. NOT THREAD-SAFE! LOCK GRAPH BEFORE USAGE!
         * @param substate Substate whose node is returned.
         * @return Returns the node belonging to substate or NULL if no such node exists.
         */
        NodePtr getNode(statechartmodel::StateInstancePtr substate);


        /**
         * @brief addEdge Adds a node to the graph with source, destination and name as specified by
         * transition and to transitionList.
         * @param transition The transition to be represented by the edge
         * @return Pointer to the newly created edge
         */
        void addEdge(statechartmodel::TransitionCPtr transition);
        /**
         * @brief removeEdge Removes the node corresponding to transition from the graph and transitionList.
         * @param transition Transition for which the edge is removed.
         */
        void removeEdge(statechartmodel::TransitionCPtr transition);
        /**
         * @brief setEdgeAttribute Sets the attribute of the edge belonging to transition.
         * @param transition Transition represented by the edge whose attribute is set.
         * @param attributeName Name of the attribute, e.g. "pos".
         * @param attributeValue Value of the attribute.
         */
        void setEdgeAttribute(statechartmodel::TransitionCPtr transition, std::string attributeName,
                              std::string attributeValue);
        /**
         * @brief getEdgeAttribute Gets the attribute of the edge belonging to transition.
         * @param transition Transition represented by the edge whose attribute is returned.
         * @param attributeName Name of the attribute, e.g. "pos".
         * @return Value of the attribute.
         */
        std::string getEdgeAttribute(statechartmodel::TransitionCPtr transition, std::string attributeName);
        bool hasEdgeAttribute(statechartmodel::TransitionCPtr transition, std::string attributeName);
        /**
         * @brief getEdge Return the edge belonging to the transition. NOT THREAD-SAFE! LOCK GRAPH BEFORE USAGE!
         * @param transition Transition whose edge is returned.
         * @return Returns the edge belonging to transition or NULL if none exists.
         */
        EdgePtr getEdge(statechartmodel::TransitionCPtr transition);


        /**
         * @brief updatePositionAndDimension Compares the stored and the new position of substate and
         * updates it if necessary.
         * @param substate Substate whose position is updated.
         */
        bool updateNodePositionAndDimension(statechartmodel::StateInstancePtr substate);
        /**
         * @brief updateName Compares the stored and the new name of substate and updates it
         * if necessary.
         * @param substate Substate whose name is updated.
         */
        void updateNodeName(statechartmodel::StateInstancePtr substate);
        /**
         * @brief updateLabel If necessary, updates the label of the edge belonging to this transition.
         * @param transition Transitions whose name changed.
         */
        void updateEdgeLabel(statechartmodel::TransitionCPtr transition);
        /**
         * @brief updateTargetAndSource Compares the old and the new target and source nodes of the transition
         * and updates them if necessary.
         * @param transition Transition whose target or source state might have changed.
         */
        void updateEdgeTargetAndSource(statechartmodel::TransitionCPtr transition);
        /**
         * @brief updateEdgePosition If necessary updates the position of the edge belonging to
         * transition.
         * @param transition Transition whose position might have changed.
         */
        void updateEdgePosition(statechartmodel::TransitionCPtr transition);

        /**
         * @brief broadcastSubstatePositions Sends signals to the state for every substate containing their
         * present position
         */
        void broadcastSubstatePositions();
        /**
         * @brief broadcastTransitionSupportPoints Sends signals to state for every transition containing
         * their present support points.
         */
        void broadcastTransitionSupportPoints();

        /**
         * @brief transitionRepresentable Returns true if transition can be represented in a graphviz graph.
         * @param transition The transition who shall be represented.
         * @return True if transition has a source and destination state. 'Detached' edges don't exist
         * in graphviz.
         */
        bool transitionRepresentable(statechartmodel::TransitionCPtr transition);
    };
} //namespace armarx

