/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "../model/State.h"
#include "../model/Transition.h"
#include "../model/stateinstance/StateInstance.h"
#include "GraphvizConverter.h"
#include "Layout.h"
#include "StateModelLayoutMediator.h"

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXWidgetController.h>
#include <QSizeF>

#include <cassert> //for testing only
#include <map>
#include <memory>
#include <mutex>
#include <string>
#include <QApplication>
#include <QDesktopWidget>


#define INITIAL_TRANSITION_NAME ""
const qreal DotDefaultDPI = 72;


armarx::StateModelLayoutMediator::StateModelLayoutMediator(armarx::statechartmodel::StatePtr state, size_t mediator_id)
    : isConnectedToWorker(false),
      id(mediator_id),
      m_state(state)
{
    dpi.setWidth(qApp->desktop()->logicalDpiX());
    dpi.setHeight(qApp->desktop()->logicalDpiY());
    dpi.setWidth(92);
    dpi.setHeight(92);

    initialTransition = NULL;
    invisibleStartNode = NULL;
    m_graph = std::make_shared<LockableGraph>();
}

armarx::statechartmodel::StatePtr armarx::StateModelLayoutMediator::getState() const
{
    return m_state;
}

size_t armarx::StateModelLayoutMediator::getID() const
{
    return id;
}



void armarx::StateModelLayoutMediator::buildUpGraph(LockableGraphPtr graph)
{
    if (!graph || !graph->graph)
    {
        ARMARX_WARNING_S << "Graph pointer is NULL!";
        return;
    }

    if (!m_state)
    {
        ARMARX_WARNING_S << "state pointer is NULL!";
        return;
    }

    m_graph = graph;
    isConnectedToWorker = true;

    //add all substates to the graph
    foreach (statechartmodel::StateInstancePtr substate, m_state->getSubstates())
    {
        addNode(substate);

        emit substateFound(substate, statechartmodel::eAdded);
    }
    statechartmodel::StateCPtr state = m_state;
    statechartmodel::TransitionCPtr init = state->getStartTransition();
    if (init)
    {
        updateInitialTransition(init, statechartmodel::eAdded);
    }


    //add all transitions
    for (statechartmodel::TransitionCPtr transition : m_state->getTransitions())
    {
        addEdge(transition);
    }


    stateChanged(statechartmodel::eAdded);
    emit scheduleMe(id, true);
}

void armarx::StateModelLayoutMediator::stateChanged(armarx::statechartmodel::SignalType signalType)
{
    return;// State changes do not matter at the moment
    /** comment to prevent warnings
        if (!isConnectedToWorker)
        {
            return;
        }


        switch (signalType)
        {
            case (statechartmodel::eUnchanged):
            case (statechartmodel::eActivated):
                //nothing to do
                return;
            case (statechartmodel::eAdded):
            case (statechartmodel::eRemoved):
            case (statechartmodel::eChanged):
                //            auto sizeString = GraphvizConverter::convertFromPoint(QPointF(m_state->getSize().width() / dpi.width(), m_state->getSize().height() / dpi.height()));
                ARMARX_INFO_S << "state changed for" << m_state->getStateName();
            //        ARMARX_INFO_S << "Graphviz graph size: " << sizeString;
            //            setGraphAttribute(m_graph->graph, "size", sizeString);
            //            setGraphAttribute(m_graph->graph, "ratio", m_state->getSize().height() / m_state->getSize().width());
            default:
                ARMARX_WARNING << "CASE NOT HANDLED " << signalType;
        }

        emit scheduleMe(id, true);
        */
}

void armarx::StateModelLayoutMediator::substateChanged(armarx::statechartmodel::StateInstancePtr substate, armarx::statechartmodel::SignalType signalType)
{
    if (!isConnectedToWorker)
    {
        return;
    }

    bool layoutAll = false;

    switch (signalType)
    {
        case (statechartmodel::eAdded):
        {
            addNode(substate);
            ARMARX_INFO_S << "new substate " << substate->getInstanceName();
            layoutAll = true;
        }
        break;

        case (statechartmodel::eRemoved):
        {
            ARMARX_INFO_S << "removed substate " << substate->getInstanceName();
            removeNode(substate);
        }
        break;

        case (statechartmodel::eActivated):
        case (statechartmodel::eUnchanged):
            //nothing to do
            return;

        case (statechartmodel::eChanged):
        {
            //            ARMARX_INFO_S << substate->getInstanceName() << " changed ";
            //herausfinden ob Name geändert, ggf. Name im Graph ändern
            updateNodeName(substate);
            //herausfinden ob Position geändert, wenn ja neue Pos im Graph eintragen
            if (!updateNodePositionAndDimension(substate))
            {
                return;
            }
            //TODO: Muss noch irgendeine andere Änderung beachtet werden?
            //            ARMARX_INFO_S << "substate changed for" << substate->getInstanceName();
            ARMARX_INFO_S << "state changed " << substate->getInstanceName();
        }
        break;
        default:
        {
            ARMARX_WARNING << "CASE NOT HANDLED " << signalType;
        }
    }

    emit scheduleMe(id, layoutAll);
}

void armarx::StateModelLayoutMediator::transitionChanged(armarx::statechartmodel::TransitionCPtr transition, armarx::statechartmodel::SignalType signalType)
{
    if (!isConnectedToWorker)
    {
        return;
    }

    if (!transition->sourceState && transition->destinationState)
    {
        if (!updateInitialTransition(transition, signalType))
        {
            return;    //No layouting need
        }
    }
    else
    {
        switch (signalType)
        {
            case (statechartmodel::eAdded):
                addEdge(transition);
                break;

            case (statechartmodel::eRemoved):
                removeEdge(transition);
                break;

            case (statechartmodel::eActivated):
            case (statechartmodel::eUnchanged):
                //nothing to do
                return;

            case (statechartmodel::eChanged):
                //            ARMARX_INFO_S << transition->eventName << " changed ";

                //mapping changed -> not interesting for us
                //event changed
                updateEdgeLabel(transition);
                //target/source state changed
                updateEdgeTargetAndSource(transition);
                //support point changed/added/removed
                updateEdgePosition(transition);
                //            ARMARX_INFO_S << "transition changed for" << m_state->getStateName();
                break;
            default:
                ARMARX_WARNING << "CASE NOT HANDLED " << signalType;

        }
    }

    emit scheduleMe(id, false);
}

void armarx::StateModelLayoutMediator::startLayouting(bool layoutAll, int counter)
{
    //    ARMARX_INFO_S << "Layouting " << armarx::LocalException::generateBacktrace();
    counter--;
    layoutRepetition = counter;
    emit layout(layoutAll);
}

QRectF  armarx::StateModelLayoutMediator::boundingRectForAgraph(Agraph_t* graph) const
{
    //    const qreal dpi = 96;
    const qreal left = GD_bb(graph).LL.x * (dpi.width() / DotDefaultDPI);
    const qreal top = (GD_bb(graph).UR.y - GD_bb(graph).LL.y) * (dpi.height() / DotDefaultDPI);
    const qreal right = GD_bb(graph).UR.x * (dpi.width() / DotDefaultDPI);
    const qreal bottom = (GD_bb(graph).UR.y - GD_bb(graph).UR.y) * (dpi.height() / DotDefaultDPI);
    return QRectF(left, top, right - left, bottom - top);
}

void armarx::StateModelLayoutMediator::workerFinishedLayouting()
{
    //    const qreal dpi = 96;
    graphVizBB = GraphvizConverter::convertToRectangle(getGraphAttribute(m_graph->graph, "bb"));
    //    ARMARX_INFO_S << graphVizBB.width() << "," << graphVizBB.height();
    m_state->setSubstateAreaSize(QSizeF(graphVizBB.width() * (dpi.width() / DotDefaultDPI), graphVizBB.height() * (dpi.height() / DotDefaultDPI)));
    broadcastSubstatePositions();
    broadcastTransitionSupportPoints();
    if (layoutRepetition > 0)
    {
        startLayouting(false, layoutRepetition);
    }
    else
    {
        emit layoutingFinished();
    }
}

void armarx::StateModelLayoutMediator::stateDeleted()
{
    //disconnect some slots to ensure that no queueing or layouting or graph changing is done after
    //the state was deleted
    disconnect(SLOT(substateChanged(statechartmodel::StateInstancePtr, statechartmodel::SignalType)));
    disconnect(SLOT(transitionChanged(statechartmodel::TransitionCPtr, statechartmodel::SignalType)));
    disconnect(SLOT(startLayouting(bool)));

    //ensure that controller is not stuck waiting for our worker to finish although this worker is
    //deleted
    emit layoutingFinished();

    //tell worker to let itself be deleted by the LayoutWorkerCreator
    emit mediatorDeleted();

    //tell controller to delete this mediator
    emit deleteMe(id);
}

bool armarx::StateModelLayoutMediator::updateInitialTransition(armarx::statechartmodel::TransitionCPtr transition, armarx::statechartmodel::SignalType signalType)
{
    std::unique_lock lock(m_graph->mutex);
    if (transition->sourceState)
    {
        return false;
    }
    if (!transition->destinationState)
    {
        return false;
    }

    auto addFakeStartNode = [&, this]()
    {
        if (!invisibleStartNode)
        {
            invisibleStartNode = agnode(m_graph->graph, const_cast<char*>(""), TRUE);
            std::string posString = converter.convertFromPoint(QPointF(0, 0));
            setGraphAttribute(invisibleStartNode, "pos", posString + "!");
            setGraphAttribute(invisibleStartNode, "shape", "circle");
            setGraphAttribute(invisibleStartNode, "width", "0.4");
            setGraphAttribute(invisibleStartNode, "height", "0.4");
        }
    };

    switch (signalType)
    {
        case (statechartmodel::eAdded):
        {
            addFakeStartNode();

            auto destinationNode = getNode(transition->destinationState);
            if (!initialTransition && invisibleStartNode && destinationNode)
            {
                initialTransition = agedge(m_graph->graph, invisibleStartNode, destinationNode, const_cast<char*>(INITIAL_TRANSITION_NAME), TRUE);
            }
        }

        break;

        case (statechartmodel::eRemoved):
            if (invisibleStartNode)
            {
                agdelete(m_graph->graph, invisibleStartNode);
            }

            if (initialTransition)
            {
                agdelete(m_graph->graph, initialTransition);
            }

            initialTransition = NULL;
            invisibleStartNode = NULL;
            break;

        case (statechartmodel::eActivated):
        case (statechartmodel::eUnchanged):
            //nothing to do
            return false;

        case (statechartmodel::eChanged):
        {
            //mapping changed -> not interesting for us
            //event changed
            addFakeStartNode();

            if (initialTransition)
            {
                agdelete(m_graph->graph, initialTransition);
                initialTransition = NULL;
            }
            auto node = getNode(transition->destinationState);
            if (!node)
            {
                return false;
            }
            ARMARX_CHECK_EXPRESSION(node) << "could not find node for " << transition->destinationState->getInstanceName();
            initialTransition = agedge(m_graph->graph, invisibleStartNode, node, const_cast<char*>("NULL"), TRUE);
        }
        break;
        default:
            ARMARX_WARNING << "CASE NOT HANDLED " << signalType;
    }
    return true;
}


QPointF armarx::StateModelLayoutMediator::convertPositionFromGraphViz(QPointF gvPos) const
{
    float fac = convertScaleFromGraphViz();

    gvPos.setY(graphVizBB.height() - gvPos.y()); // graphviz's y value starts at bottom, so invert it
    gvPos /= fac;
    gvPos += m_state->margin.topLeft();
    return gvPos;
}

float armarx::StateModelLayoutMediator::convertScaleFromGraphViz() const
{
    return DotDefaultDPI / dpi.width();
    const QSizeF& size = getState()->getSize();

    float widthFac = graphVizBB.width() / (size.width() - m_state->margin.left());
    float heightFac = graphVizBB.height() / (size.height() - m_state->margin.top());
    float fac = std::max(widthFac, heightFac);
    return fac;
}

QPointF armarx::StateModelLayoutMediator::convertPositionToGraphViz(QPointF statePos) const
{
    float fac = convertScaleFromGraphViz();

    statePos -= m_state->margin.topLeft();
    statePos *= fac;
    statePos.setY(graphVizBB.height() - statePos.y());
    return statePos;
}

void armarx::StateModelLayoutMediator::addNode(armarx::statechartmodel::StateInstancePtr substate)
{
    std::unique_lock lock(m_graph->mutex);

    //Sometimes the eAdded signal for a node reaches the mediator after an eChanged signal. Test whether
    //there already is a node for the given substate and only add one if there isn't one already.
    NodePtr node = getNode(substate);

    if (!node)
    {
        std::string name;

        if (!substate->getInstanceName().isEmpty())
        {
            name = substate->getInstanceName().toStdString();
        }
        else if (substate->getStateClass())
        {
            name = substate->getStateClass()->getStateName().toStdString();
        }
        else
        {
            name = "NoStateClass";
        }

        agnode(m_graph->graph, const_cast<char*>(name.c_str()), TRUE);
        substateMap.insert(
            std::pair<statechartmodel::StateInstancePtr, std::string>(substate, name));
        updateNodePositionAndDimension(substate);
    }
}

void armarx::StateModelLayoutMediator::removeNode(armarx::statechartmodel::StateInstancePtr substate)
{
    std::unique_lock lock(m_graph->mutex);

    NodePtr node = getNode(substate);
    if (!substate || !substate->getStateClass())
    {
        return;
    }

    //remove node and all adjacent edges from the graph
    if (node)
    {
        ARMARX_INFO_S << "Removing node " << substate->getInstanceName();
        agdelete(m_graph->graph, node);
    }
}

void armarx::StateModelLayoutMediator::setNodeAttribute(armarx::statechartmodel::StateInstancePtr substate,
        const std::string& attributeName, const std::string& attributeValue)
{
    std::unique_lock lock(m_graph->mutex);

    NodePtr node = getNode(substate);

    if (!node)
    {
        ARMARX_WARNING_S << "Node null for state " << substate->getInstanceName() << " - Could not set attribute";
        return;
    }

    setGraphAttribute(node, attributeName, attributeValue);
}

std::string armarx::StateModelLayoutMediator::getNodeAttribute(armarx::statechartmodel::StateInstancePtr substate, const std::string& attributeName)
{
    std::unique_lock lock(m_graph->mutex);

    NodePtr node = getNode(substate);

    if (!node)
    {
        return std::string();
    }

    std::string value = getGraphAttribute(node, attributeName);
    return value;
}
bool armarx::StateModelLayoutMediator::hasNodeAttribute(armarx::statechartmodel::StateInstancePtr substate, const std::string& attributeName)
{
    std::unique_lock lock(m_graph->mutex);

    NodePtr node = getNode(substate);

    if (!node)
    {
        return false;
    }

    return hasGraphAttribute(node, attributeName);
}

std::string armarx::StateModelLayoutMediator::getNodeName(armarx::NodePtr node)
{
    if (!node)
    {
        return std::string();
    }

    if (node->base.data->name)
    {
        return node->base.data->name;
    }
    else
    {
        return "";
    }
}

armarx::NodePtr armarx::StateModelLayoutMediator::getNode(const std::string& name)
{
    NodePtr node;
    node = agnode(m_graph->graph, const_cast<char*>(name.c_str()), TRUE);

    if (!node)
    {
        node = NULL;
    }

    return node;
}

//not thread-safe!! Always make sure to lock m_graph->mutex before calling this function!
armarx::NodePtr armarx::StateModelLayoutMediator::getNode(armarx::statechartmodel::StateInstancePtr substate)
{
    NodeMap::iterator iter = substateMap.find(substate);

    if (iter != substateMap.end())
    {
        NodePtr node = NULL;
        node = getNode(iter->second);

        if (!node)
        {
            ARMARX_ERROR_S << "substateMap contains reference to non-exixtant node";
        }
        return node;
    }

    return NULL;
}

void armarx::StateModelLayoutMediator::addEdge(armarx::statechartmodel::TransitionCPtr transition)
{
    std::unique_lock lock(m_graph->mutex);

    if (!transitionRepresentable(transition))
    {
        return;
    }

    NodePtr source = getNode(transition->sourceState);
    NodePtr destination = getNode(transition->destinationState);
    std::string name = transition->eventName.toStdString();

    if (destination && source)
    {
        //add edge to graph
        EdgePtr edge = agedge(m_graph->graph, source, destination, const_cast<char*>(name.c_str()), TRUE);
        setGraphAttribute(edge, const_cast<char*>("label"), const_cast<char*>(name.c_str()));

        //add edge to transitionList
        transitionList.append(transition);
    }
    else
    {
        ARMARX_ERROR_S << "The nodes requested for this transition don't exist in the graph";
    }
}

void armarx::StateModelLayoutMediator::removeEdge(armarx::statechartmodel::TransitionCPtr transition)
{
    std::unique_lock lock(m_graph->mutex);

    EdgePtr edge = getEdge(transition);

    //remove from transitionList
    transitionList.removeAll(transition);

    //remove from graph
    if (edge)
    {
        agdelete(m_graph->graph, edge);
    }
}

void armarx::StateModelLayoutMediator::setEdgeAttribute(armarx::statechartmodel::TransitionCPtr transition, std::string attributeName, std::string attributeValue)
{
    std::unique_lock lock(m_graph->mutex);

    EdgePtr edge = getEdge(transition);

    if (!edge)
    {
        return;
    }

    agset(edge, const_cast<char*>(attributeName.c_str()), const_cast<char*>(attributeValue.c_str()));
}

std::string armarx::StateModelLayoutMediator::getEdgeAttribute(armarx::statechartmodel::TransitionCPtr transition, std::string attributeName)
{
    std::unique_lock lock(m_graph->mutex);

    EdgePtr edge = getEdge(transition);

    if (!edge)
    {
        return std::string();
    }

    std::string value {getGraphAttribute(edge, attributeName)};
    return value;
}

bool armarx::StateModelLayoutMediator::hasEdgeAttribute(armarx::statechartmodel::TransitionCPtr transition, std::string attributeName)
{
    std::unique_lock lock(m_graph->mutex);

    EdgePtr edge = getEdge(transition);

    if (!edge)
    {
        return false;
    }

    return hasGraphAttribute(edge, attributeName);
}

//not thread-safe!! Always make sure to lock m_graph->mutex before calling this function!
armarx::EdgePtr armarx::StateModelLayoutMediator::getEdge(armarx::statechartmodel::TransitionCPtr transition)
{
    EdgePtr edge = NULL;

    if (transitionRepresentable(transition))
    {
        QString transName = transition->eventName;
        std::string name;

        if (!(transName.isEmpty()))
        {
            name = transName.toStdString();
        }

        NodePtr sourceNode = getNode(transition->sourceState);
        NodePtr destNode = getNode(transition->destinationState);

        if (sourceNode)
        {
            //iterate over all edges incident to sourceNode
            for (EdgePtr e = agfstedge(m_graph->graph, sourceNode); e; e = agnxtedge(m_graph->graph, e, sourceNode))
            {
                std::string currentName {getGraphAttribute(e, "label")};

                //test whether the name fits and whether it is an out-edge of sourceNode
                if ((currentName == name) && (agtail(e) == sourceNode))
                {
                    edge = e;
                    break;
                }
            }
        }
        else if (destNode) // for initial transition
        {
            for (EdgePtr e = agfstedge(m_graph->graph, destNode); e; e = agnxtedge(m_graph->graph, e, destNode))
            {
                std::string currentName {getGraphAttribute(e, "label")};

                //test whether the name fits and whether it is an out-edge of sourceNode
                if ((currentName == name) && (aghead(e) == destNode))
                {
                    edge = e;
                    break;
                }
            }
        }
    }

    return edge;
}

bool armarx::StateModelLayoutMediator::updateNodePositionAndDimension(armarx::statechartmodel::StateInstancePtr substate)
{
    //TODO: Conversion from Qt positions to graphviz positions and sizes neccessary?
    bool changed = false;
    auto size = substate->getClassSize() * substate->getScale();
    QPointF middlePosition = convertPositionToGraphViz(substate->getTopLeft() + QPointF(size.width(), size.height()) * 0.5);
    if (hasNodeAttribute(substate, "pos"))
    {
        auto posString = getNodeAttribute(substate, "pos");
        if (posString.empty())
        {
            changed |= true;
        }
        else
        {
            auto l = (converter.convertToPoint(posString) - middlePosition).manhattanLength();
            changed |= l  > 3;
        }
    }
    else
    {
        changed = true;
    }


    auto posString = converter.convertFromPoint(middlePosition);
    setNodeAttribute(substate, "pos", posString + "!");
    setNodeAttribute(substate, (char*)"shape", (char*)"box");
    //change node dimensions

    //    if (substate->getStateClass())
    {
        //        float multiplicator = sqrt(substate->getStateClass()->getSubstates().size());
        //        multiplicator = std::min(4.f, multiplicator);
        size = substate->getClassSize() * substate->getScale();
        //        ARMARX_INFO_S << substate->getInstanceName() << ": " << VAROUT(multiplicator) << VAROUT(size);
    }
    //    else
    //    {

    //        size = QSizeF(substate->getBoundingSquareSize(), substate->getBoundingSquareSize());
    //        //        ARMARX_INFO_S << "no state class for  " << substate->getInstanceName();
    //    }


    if (hasNodeAttribute(substate, "width"))
    {
        auto oldWidthString = getNodeAttribute(substate, "width");
        auto ow = size.width() / dpi.width();
        changed |= oldWidthString.empty() || fabs(converter.convertToFloat(oldWidthString) - ow) > 0.01;
    }
    else
    {
        changed = true;
    }
    std::string widthString {converter.convertFromFloat(size.width() / dpi.width())};
    setNodeAttribute(substate, "width", widthString);

    if (hasNodeAttribute(substate, "height"))
    {
        auto oldheightString = getNodeAttribute(substate, "height");
        changed |= oldheightString.empty() || fabs(converter.convertToFloat(oldheightString) - size.height() / dpi.height()) > 0.01;
    }
    else
    {
        changed = true;
    }

    std::string heightString {converter.convertFromFloat(size.height() / dpi.height())};
    //    ARMARX_INFO_S << m_graph->graph << ": " << substate->getInstanceName() << ": " << VAROUT(widthString) << VAROUT(heightString);
    setNodeAttribute(substate, "height", heightString);

    setNodeAttribute(substate, "pin", "true");
    return changed;
}

void armarx::StateModelLayoutMediator::updateNodeName(armarx::statechartmodel::StateInstancePtr substate)
{
    std::string newName;

    if (!substate->getInstanceName().isEmpty())
    {
        newName = substate->getInstanceName().toStdString();
    }
    else if (substate->getStateClass())
    {
        newName = substate->getStateClass()->getStateName().toStdString();
    }
    else
    {
        newName = "NoStateClass";
    }

    std::string oldName;
    auto iter = substateMap.find(substate);

    if (iter != substateMap.end())
    {
        oldName = iter->second;

        if (newName != oldName)
        {
            std::unique_lock lock(m_graph->mutex);

            NodePtr node = getNode(oldName);
            setGraphAttribute(node, "label", newName);
        }
    }
    else
    {
        ARMARX_ERROR_S << "The substate " << newName << " is not in the graph";
        addNode(substate);
    }
}

void armarx::StateModelLayoutMediator::updateEdgeLabel(armarx::statechartmodel::TransitionCPtr transition)
{
    std::string oldLabel {getEdgeAttribute(transition, "label")};
    std::string newLabel {transition->eventName.toStdString()};

    if ((!(oldLabel.empty())) && (oldLabel != newLabel))
    {
        setEdgeAttribute(transition, "label", newLabel);
    }
}

void armarx::StateModelLayoutMediator::updateEdgeTargetAndSource(armarx::statechartmodel::TransitionCPtr transition)
{
    std::unique_lock lock(m_graph->mutex);

    if (transitionRepresentable(transition))
    {
        //the transition can be represented in the graph
        EdgePtr edge = getEdge(transition);

        if (edge) // the transition has an edge
        {
            std::string oldSourceName = getNodeName(agtail(edge));
            std::string oldDestinationName = getNodeName(aghead(edge));

            if ((!oldSourceName.empty()) && (!oldDestinationName.empty()))
                //the source and destination state have nodes
            {
                std::string newSourceName = transition->sourceState->getInstanceName().toStdString();
                std::string newDestName = transition->destinationState->getInstanceName().toStdString();

                if ((oldSourceName != newSourceName) || (oldDestinationName != newDestName))
                    //the names of the source and/or destination state changed
                {
                    removeEdge(transition);
                    addEdge(transition);
                }
            }
            else
            {
                ARMARX_ERROR_S << "Edge has invalid tail or head node";
            }
        }
        else   //the transition has no edge yet
        {
            addEdge(transition);
        }
    }
    else
    {
        ARMARX_INFO_S << "Removing edge " << transition->eventName;
        removeEdge(transition);
    }
}

void armarx::StateModelLayoutMediator::updateEdgePosition(armarx::statechartmodel::TransitionCPtr transition)
{
    //TODO: wie wird Pfeilposition umgesetzt? Momentan werden alle support points als Kontrollpunkte
    //angesehen
    if (transitionRepresentable(transition))
    {
        SupportPoints spline;
        spline = transition->supportPoints;
        std::string posString = converter.convertFromSpline(spline);
        setEdgeAttribute(transition, "pos", posString);
    }
}

void armarx::StateModelLayoutMediator::broadcastSubstatePositions()
{
    //    float scale = convertScaleFromGraphViz();

    for (auto substate : substateMap)
    {
        try
        {
            std::string currentPosString = getNodeAttribute(substate.first, "pos");
            //        ARMARX_INFO_S << "Position of " << substate.second << ": " <<currentPosString;
            std::string widthString = getNodeAttribute(substate.first, "width");
            std::string heightString = getNodeAttribute(substate.first, "height");
            float width = converter.convertToFloat(widthString) * dpi.width();
            float height = converter.convertToFloat(heightString) * dpi.height();
            QPointF position = converter.convertToPoint(currentPosString);
            position = convertPositionFromGraphViz(position);
            //        ARMARX_INFO_S << "Position of " << substate.second << " in QT: " <<position;
            statechartmodel::StateInstancePtr instance = substate.first;
            instance->setBoundingBox(std::max(width, height)
                                    );// * (DotDefaultDPI / dpi.width()));
            instance->setPosition(position - QPoint(width, height) * 0.5);
        }
        catch (...)
        {
            scheduleMe(getID(), true);
        }
    }
}

void armarx::StateModelLayoutMediator::broadcastTransitionSupportPoints()
{
    auto tmpList = transitionList;
    if (initialTransition)
    {
        const auto* state = m_state.get();
        statechartmodel::TransitionCPtr t(state->getStartTransition());
        if (t)
        {
            tmpList.push_back(t);
        }
    }
    for (auto& transition : tmpList)
    {
        try
        {


            std::string currentSplineString = getEdgeAttribute(transition, "pos");
            SupportPoints supportPoints = GraphvizConverter::convertToSpline(currentSplineString);

            for (auto& p : supportPoints.controlPoints)
            {
                p = convertPositionFromGraphViz(p);
            }

            if (supportPoints.endPoint)
            {
                *supportPoints.endPoint  = convertPositionFromGraphViz(*supportPoints.endPoint);
            }

            if (supportPoints.startPoint)
            {
                *supportPoints.startPoint  = convertPositionFromGraphViz(*supportPoints.startPoint);
            }
            QPointPtr labelCenter;
            if (hasEdgeAttribute(transition, "lp"))
            {
                std::string labelPosString = getEdgeAttribute(transition, "lp");
                if (!labelPosString.empty())
                {
                    QPointF labelPosition = converter.convertToPoint(labelPosString);
                    labelPosition = convertPositionFromGraphViz(labelPosition);
                    labelCenter = std::make_shared<QPointF>(labelPosition);
                }
            }
            FloatPtr labelFontPointSize;
            if (hasEdgeAttribute(transition, "fontsize"))
            {
                std::string labelFontPointSizeString = getEdgeAttribute(transition, "fontsize");
                labelFontPointSize = std::make_shared<float>(converter.convertToFloat(labelFontPointSizeString) * 0.3);
            }

            //TODO: ist toPointList richtig? sollten nicht eher nur controlPoints genommen werden?
            //wie geht View damit um, wenn start oder end point noch mit dazugegeben?
            //TODO: wie wird Pfeilposition umgesetzt?
            emit supportPointsChanged(transition, supportPoints.controlPointList(), labelCenter, labelFontPointSize);
        }
        catch (...)
        {
            ARMARX_INFO_S << "Exception during transition update:\n" << GetHandledExceptionString();
            emit scheduleMe(getID(), true);
        }
    }
}

bool armarx::StateModelLayoutMediator::transitionRepresentable(armarx::statechartmodel::TransitionCPtr transition)
{
    return (transition->sourceState && transition->destinationState) || (!transition->sourceState && transition->destinationState);
}
