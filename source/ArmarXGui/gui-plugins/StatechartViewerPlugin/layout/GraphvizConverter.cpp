/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "GraphvizConverter.h"

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/util/StringHelpers.h>

#include <SimoxUtility/algorithm/string/string_tools.h>

#include <QPainterPath>
#include <QPointF>

#include <boost/lexical_cast.hpp>

#include <cassert>
//#include <iomanip> //setprecision
#include <stdexcept>
//#include <sstream>
#include <string>
#include <vector>

armarx::GraphvizConverter::GraphvizConverter()
{
}

//TODO: ggf. diese beiden Funktionen und deren includes löschen
float armarx::GraphvizConverter::convertToFloat(const std::string& graphvizNumber)
{
    /*
    //replace ',' by '.'
    size_t pointPos = graphvizNumber.find_first_of(",");
    graphvizNumber.replace(pointPos, 1, ".");
    */
    try
    {
        return boost::lexical_cast<float>(graphvizNumber);
    }
    catch (boost::bad_lexical_cast&)
    {
        ARMARX_WARNING_S << " bad lexical cast to float - value:" << graphvizNumber;
        throw;
    }
}

std::string armarx::GraphvizConverter::convertFromFloat(float number)
{
    std::string graphvizNumber = to_string(number);

    /*
    //replace '.' by ','
    size_t pointPos = graphvizNumber.find_first_of(".");
    graphvizNumber.replace(pointPos, 1, ",");
    */

    return graphvizNumber;
}

QPointF armarx::GraphvizConverter::convertToPoint(std::string graphvizPoint)
{
    auto pos = graphvizPoint.find('!');
    if (pos != std::string::npos)
    {
        graphvizPoint.erase(pos, 1);
    }
    tokenVector tokens = splitBy(graphvizPoint, ",");
    ARMARX_CHECK_EXPRESSION(tokens.size() == 2) << graphvizPoint;

    try
    {
        float y = boost::lexical_cast<float>(tokens.at(1));
        return QPointF {boost::lexical_cast<float>(tokens.at(0)), y};
    }
    catch (boost::bad_lexical_cast&)
    {
        ARMARX_WARNING_S << " bad lexical cast to point - value:" << graphvizPoint;
        throw;
    }
    catch (const std::invalid_argument& ia)
    {
        ARMARX_ERROR_S << "Invalid argument for float conversion: " << ia.what() << " " << tokens.at(0) << ", " << tokens.at(1);
    }
    catch (const std::out_of_range& e)
    {
        ARMARX_ERROR_S << "Out of range: " << e.what();
    }

    return QPointF(0, 0);
}

std::string armarx::GraphvizConverter::convertFromPoint(QPointF point)
{
    std::string graphvizPoint = to_string(point.x());
    graphvizPoint += ",";
    graphvizPoint += to_string(point.y());

    /*
     * might be neccessary to use this version because graphviz wants a comma instead of a
     * decimal point
     *
    std::stringstream ss;
    // setprecision + fixed that 4 post decimal positions are shown
    ss << std::setprecision(4) << std::fixed;
    ss << point.x() << "," << point.y();
    std::string graphvizPoint = ss.str();

    //replace '.' by ','
    size_t pointPos = graphvizPoint.find_first_of(".");
    while(pointPos != std::string::npos)
    {
        graphvizPoint.replace(pointPos, 1, ",");
        pointPos = graphvizPoint.find_first_of(".", pointPos + 1);
    }
    */

    return graphvizPoint;
}

QRectF armarx::GraphvizConverter::convertToRectangle(const std::string& graphvizPoint)
{
    tokenVector tokens = splitBy(graphvizPoint, ",");
    ARMARX_CHECK_EXPRESSION(tokens.size() == 4);

    try
    {
        float y = boost::lexical_cast<float>(tokens.at(1));
        float y2 = boost::lexical_cast<float>(tokens.at(3));
        return QRectF {boost::lexical_cast<float>(tokens.at(0)), y,
                       boost::lexical_cast<float>(tokens.at(2)), y2
                      };
    }
    catch (boost::bad_lexical_cast&)
    {
        ARMARX_WARNING_S << " bad lexical cast to float - point:" << graphvizPoint;
        throw;
    }
    catch (const std::invalid_argument& ia)
    {
        ARMARX_ERROR_S << "Invalid argument for float conversion: " << ia.what() << " " << tokens.at(0) << ", " << tokens.at(1);
    }
    catch (const std::out_of_range& e)
    {
        ARMARX_ERROR_S << "Out of range: " << e.what();
    }

    return QRectF(0, 0, 0, 0);

}

armarx::SupportPoints armarx::GraphvizConverter::convertToSpline(const std::string& graphVizSplineType)
{
    splineVector splines;

    tokenVector splineStrings = splitBy(graphVizSplineType, ";");
    ARMARX_CHECK_EXPRESSION(splineStrings.size() > 0);

    for (auto spline : splineStrings)
    {
        splines.push_back(tokenize(spline));
    }

    return mergeSplines(splines);
}

std::string armarx::GraphvizConverter::convertFromSpline(armarx::SupportPoints spline)
{
    //assert(spline.controlPoints.size() % 3 == 1);

    std::string graphvizSpline;

    if (spline.endPoint)
    {
        graphvizSpline += "e,";
        graphvizSpline += convertFromPoint(*(spline.endPoint));
        graphvizSpline += " ";
    }

    if (spline.startPoint)
    {
        graphvizSpline += "s,";
        graphvizSpline += convertFromPoint(*(spline.startPoint));
        graphvizSpline += " ";
    }

    for (auto point : spline.controlPoints)
    {
        graphvizSpline += convertFromPoint(point);
        graphvizSpline += " ";
    }

    ARMARX_CHECK_EXPRESSION(graphvizSpline.size() > 0);
    simox::alg::trim(graphvizSpline); //remove trailing whitespace

    return graphvizSpline;
}


armarx::SupportPoints armarx::GraphvizConverter::tokenize(std::string graphVizSpline)
{
    tokenVector points = splitBy(graphVizSpline, " ");
    /* TODO: de-comment
    assert(points.size() >= 4); //there has to be a starting control point and a triple of control points
    */
    SupportPoints spline;

    for (auto point : points)
    {
        tokenVector tokens = splitBy(point, ",");

        if (tokens.size() == 3)
        {
            ARMARX_CHECK_EXPRESSION((tokens.at(0) == "s") || (tokens.at(0) == "e"));

            try
            {
                QPointF floatPoint {boost::lexical_cast<float>(tokens.at(1)), boost::lexical_cast<float>(tokens.at(2))};

                if (tokens.at(0) == "s")
                {
                    spline.startPoint = std::make_shared<QPointF>(floatPoint);
                }
                else
                {
                    spline.endPoint = std::make_shared<QPointF>(floatPoint);
                }
            }
            catch (boost::bad_lexical_cast&)
            {
                ARMARX_WARNING_S << " bad lexical cast to pointlist - value:" << graphVizSpline;
                throw;
            }
            catch (const std::invalid_argument& ia)
            {
                ARMARX_ERROR_S << "Invalid argument for float conversion: " << ia.what();
            }
            catch (const std::out_of_range& e)
            {
                ARMARX_ERROR_S << "Out of range: " << e.what();
            }
        }
        else if (tokens.size() == 2)
        {
            try
            {
                QPointF p {boost::lexical_cast<float>(tokens.at(0)), boost::lexical_cast<float>(tokens.at(1))};
                spline.controlPoints.push_back(p);
            }
            catch (const std::invalid_argument& ia)
            {
                ARMARX_ERROR_S << "Invalid argument for float conversion: " << ia.what() << " " << tokens.at(0) << ", " << tokens.at(1);
            }
            catch (const std::out_of_range& e)
            {
                ARMARX_ERROR_S << "Out of range: " << e.what();
            }
        }
        else
        {
            ARMARX_ERROR_S << "Not a valid string to represent a spline control point: " << point;
        }
    }

    //assert(spline.controlPoints.size() % 3 == 1);

    return spline;
}

armarx::tokenVector armarx::GraphvizConverter::splitBy(std::string toSplit, std::string divider)
{
    tokenVector tokenVec;
    size_t dividerPos = 0;

    while (dividerPos != std::string::npos)
    {
        dividerPos = toSplit.find_first_of(divider);
        std::string token = toSplit.substr(0, dividerPos);
        toSplit = toSplit.substr(dividerPos + 1);

        tokenVec.push_back(token);
    }

    return tokenVec;
}

armarx::SupportPoints armarx::GraphvizConverter::mergeSplines(armarx::splineVector toMerge)
{
    if (toMerge.size() >= 1)
    {
        return toMerge.front();
    }
    SupportPoints merged;
    merged.startPoint = toMerge.front().startPoint;
    merged.endPoint = toMerge.back().endPoint;

    for (SupportPoints otherSpline : toMerge)
    {
        merged.controlPoints.append(otherSpline.controlPoints);
    }

    return merged;
}

/*
QPainterPath armarx::SplineConverter::calcArrowhead(QPointF start, QPointF destination)
{
    QPointF directionVector = destination - start;
    QPointF baseVector {- directionVector.y(), directionVector.x()}; //vector orthogonal to directionVector

    QPointF leftWingEnd = start + 0.5 * baseVector;
    QPointF rightWingEnd = start - 0.5 * baseVector;

    QPolygonF triangle;
    triangle << leftWingEnd << rightWingEnd << destination;

    QPainterPath arrowhead;
    arrowhead.addPolygon(triangle);
    arrowhead.closeSubpath();

    return arrowhead;
}
*/


QList<QPointF> armarx::SupportPoints::toPointList() const
{
    QList<QPointF> list;

    if (startPoint)
    {
        list.append(*startPoint.get());
    }

    list.append(controlPointList());

    if (endPoint)
    {
        list.append(*endPoint.get());
    }

    return list;
}

QList<QPointF> armarx::SupportPoints::controlPointList() const
{
    QList<QPointF> list;

    for (QPointF point : controlPoints)
    {
        list.append(point);
    }

    return list;
}

void armarx::SupportPoints::setControlPoints(QList<QPointF> list)
{
    controlPoints.clear();

    for (auto point : list)
    {
        controlPoints.push_back(point);
    }
}

void armarx::SupportPoints::append(const QPointF& point)
{
    controlPoints.append(point);
}
