/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "../model/State.h"
#include "LayoutController.h"
#include "LayoutWorkerCreator.h"

#include <QThread>

namespace armarx
{

    class LayoutThread : public QObject
    {
        Q_OBJECT

    public:
        explicit LayoutThread(armarx::statechartmodel::StatePtr state = statechartmodel::StatePtr(new statechartmodel::State()), bool startEnabled = true);
        ~LayoutThread() override;

        /**
         * @brief setState Stops the event loop and resets the top state. All layouting done before is
         * nullified by this. run() has to be called afterwards to resume layouting.
         * @param state The new top state.
         */
        void setState(armarx::statechartmodel::StatePtr state);

        //protected:
        void run();

        LayoutController& getController()
        {
            return controller;
        }

    private:
        /**
         * @brief topState Topstate of all the graphs.
         */
        statechartmodel::StatePtr topState;

        /**
         * @brief controller The LayoutController that schedules the workers that run in the other thread.
         */
        LayoutController controller;

        LayoutWorkerCreatorPtr workerCreator;
    };
} //namespace armarx
