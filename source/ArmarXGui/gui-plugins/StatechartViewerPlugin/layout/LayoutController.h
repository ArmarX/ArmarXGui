/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "../model/State.h"
#include "StateModelLayoutMediator.h"

#include <QList>
#include <QObject>
#include <QTimer>

#include <map>
#include <memory>
#include <utility>
#include <string>

namespace armarx
{

    typedef std::shared_ptr<StateModelLayoutMediator>MediatorPtr;
    using MediatorMap = std::map<size_t, MediatorPtr>;
    using MediatorLayoutOptionPair = std::pair<size_t, bool>;

    class LayoutController : public QObject
    {
        Q_OBJECT

    public:
        LayoutController(armarx::statechartmodel::StatePtr topState, bool startEnabled = true);

        /**
         * @brief setTopState Resets all workers i.e. deletes all old workers and sets topState and its
         * worker as only member of workers.
         * @param topState The new top state.
         */
        void setTopState(statechartmodel::StatePtr topState);

        /**
         * @brief createAllWorkers Creates workers for all mediators
         */
        void createAllWorkers();

        size_t getStateId(armarx::statechartmodel::StatePtr state) const;

    signals:
        /**
         * @brief createWorker Tells the LayoutWorkerCreator to create a new worker for this mediator.
         * @param mediator Manages the communication between the new worker and the associated state.
         * @param id The mediator's and worker's id.
         * @param name Name of mediator's state.
         */
        void createWorker(MediatorPtr mediator, size_t id, QString name);

        /**
         * @brief reset Notifies the LayoutWorkerCreator that the topState has been reset.
         */
        void reset();

    public slots:
        void enableLayouting(bool enable = true);

        /**
         * @brief scheduleMediator Put the mediator in the layoutQueue.
         * @param mediatorId ID of the StateModelLayoutMediator that should be scheduled.
         * @param layoutAll Indicates whether nodes and edges (true) or only edges shall be layouted (false).
         */
        void scheduleMediator(size_t mediatorId, bool layoutAll);
        bool layoutNow(size_t mediatorId, bool layoutAll);


        /**
         * @brief startNextLayouting The last worker that layouted is finished and the next should be
         * started.
         */
        void startNextLayouting();

        /**
         * @brief deleteMediator Call the destructor of mediator and tell LayoutWorkerCreator to delete
         * the corresponding worker.
         * @param mediatorId The id of the mediator that needs to be deleted.
         */
        void deleteMediator(size_t mediatorId);

        /**
         * @brief potentialStateAdded If a substate is added it might mean that there is a new state
         * needing a LayoutWorker. Creates a new LayoutWorker if necessary.
         * @param substate The substate that was changed.
         * @param signalType Type of change to the substate.
         */
        void potentialStateAdded(statechartmodel::StateInstancePtr substate, statechartmodel::SignalType signalType);

    private:
        /**
         * @brief layoutQueue Queue of all workers that need to layout their graph.
         */
        QList<MediatorLayoutOptionPair> layoutQueue;

        /**
         * @brief workers List of all LayoutWorkers.
         */
        MediatorMap mediators;
        /**
         * @brief idCounter Value is the next unused ID.
         */
        size_t idCounter;

        /**
         * @brief states List of all states that already have a LayoutWorker.
         */
        QList<statechartmodel::StatePtr> states;

        /**
         * @brief timer Timeouts are used when the layoutQueue is empty in order to start layouting when
         * a mediator is scheduled. Prevents starvation.
         */
        QTimer timer;


        bool layoutingDisabled;
        /**
         * @brief createMediator Create mediator and connect it to state via signals and slots.
         * @param state The state for which a StateModelLayoutMediator is to be created.
         */
        void createMediator(statechartmodel::StatePtr state);
    };
} //namespace armarx
