/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "Layout.h"
#include "StateModelLayoutMediator.h"

#include <string>

namespace armarx
{

    class LayoutWorker : public QObject
    {
        Q_OBJECT

    public:
        /**
         * @brief LayoutWorker Sets state and id as specified. Initializes an empty Graph.
         * @param state The state which this worker layouts.
         * @param id The worker's ID for communication with the LayoutController.
         */
        LayoutWorker(size_t workerId, QString name, GvcPtr gvContext);
        ~LayoutWorker() override;

    signals:
        /**
         * @brief buildGraph Tells the StateModelLayoutMediator to build the first version of this graph
         * @param graph The graph to be filled.
         */
        void buildGraph(LockableGraphPtr graph);

        /**
         * @brief layoutingFinished Indicates that this worker finished its layout task.
         */
        void layoutingFinished();

        /**
         * @brief deleteMe To be emitted when this worker ought to be deleted because
         * its corresponding mediator was deleted.
         * @param id This worker's id.
         */
        void deleteMe(size_t id);

    public slots:
        /**
         * @brief stateDeleted The corresponding state was deleted.
         */
        void stateDeleted();
        /**
         * @brief layout Starts layouting.
         * @param layoutAll If true, layout edges and nodes, if false only layout edges.
         */
        //TODO: stat immer layoutAll mitzuschleppen könnte es vielleicht möglich sein immer nur
        //neato auszuführen. Dann müssen allerdings alle schon mal positioniertennodes gepinnt sein.
        //Außerdem muss neato -n benutzt werden damit positionen in points sind.
        void layout(bool layoutAll);

        /**
         * @brief isConnected Notifies that worker with id workerId is now connected to its mediator.
         * If workerId is this worker's id, send a pointer to the graphviz graph to the mediator.
         * @param workerId The id of the worker who is connected.
         */
        void isConnected(size_t workerId);

    private:
        /**
         * @brief id ID of this worker. Used for communication with the LayoutController.
         */
        size_t id;

        /**
         * @brief graph Graph that is layouted by this worker and mutex to lock it.
         */
        LockableGraphPtr m_graph;

        /**
         * @brief graphvizContext Context needed for layouting. Shared with all other workers.
         */
        GvcPtr graphvizContext;


        /**
         * @brief layout Layout the graph using the specified algorithm (e.g. dot, neato etc.).
         * @param algorithm The layout algorithm.
         */
        void layoutWithAlgo(std::string algorithm);
        QString name;
    };

} //namespace armarx

