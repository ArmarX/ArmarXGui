/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "../model/State.h"
#include "../model/Transition.h"
#include "GraphvizConverter.h"
#include "Layout.h"
#include "LayoutWorker.h"

#include <ArmarXCore/core/logging/Logging.h>

#include <cassert> //for testing only
#include <map>
#include <memory>
#include <mutex>
#include <string>
#include <utility>

#include <QApplication>
#include <QDesktopWidget>

armarx::LayoutWorker::LayoutWorker(size_t workerId, QString name, armarx::GvcPtr gvContext)
    : QObject(),
      id(workerId),
      graphvizContext(gvContext),
      name(name)
{
    m_graph = std::make_shared<LockableGraph>();
    std::unique_lock lock(m_graph->mutex);
    m_graph->graph = agopen(name.toLatin1().data(), Agdirected, NULL);
    ARMARX_INFO_S << m_graph->graph << " Creating Graph with name " << name.toLatin1().data();

    int dpiX = qApp->desktop()->logicalDpiX();
    auto dpi = GraphvizConverter::convertFromFloat(dpiX);
    //Standardparameter setzen
    agattr(m_graph->graph, AGRAPH, (char*)"dpi", (char*)"72");//needs to be 72 because graphviz's bounding box is always calculated iwht 72dpi
    agattr(m_graph->graph, AGRAPH, (char*)"bb", (char*)"0,0,1210,744");
    agattr(m_graph->graph, AGRAPH, (char*)"size", (char*)"5,5");
    agattr(m_graph->graph, AGRAPH, (char*)"overlap", (char*)"prism");
    agattr(m_graph->graph, AGRAPH, (char*)"splines", (char*)"true");
    agattr(m_graph->graph, AGRAPH, (char*)"pad", (char*)"0.2");
    agattr(m_graph->graph, AGRAPH, (char*)"nodesep", (char*)"0.4");
    agattr(m_graph->graph, AGRAPH, (char*)"sep", (char*)"1");
    agattr(m_graph->graph, AGRAPH, (char*)"overlap_shrink", (char*)"true");
    agattr(m_graph->graph, AGRAPH, (char*)"rankdir", (char*)"LR");
    agattr(m_graph->graph, AGRAPH, (char*)"ratio", (char*)"compress");

    //    agraphattr(m_graph->graph, (char*)"ratio", (char*)"fill");
    //    agraphattr(m_graph->graph, (char*)"orientation", (char*)"landscape");
    //    agraphattr(m_graph->graph, (char*)"notranslate", (char*)"false");


    //Set default attributes for the future nodes

    //    agattr(m_graph->graph, AGNODE, (char*)"fixedsize", (char*)"true");
    //    agnodeattr(m_graph->graph, (char*)"orientation", (char*)"1,0");
    //    agattr(m_graph->graph, AGNODE, (char*)"shape", (char*)"box");
    agattr(m_graph->graph, AGNODE, (char*)"margin", (char*)"0,0");
    agattr(m_graph->graph, AGNODE, (char*)"fontsize", (char*)"7");
    agattr(m_graph->graph, AGEDGE, (char*)"fontsize", (char*)"14");

    //Divide the wanted width by the DPI to get the value in points
    //TODO: wirklich benötigt?
    double test1 = atof(agget(m_graph->graph, (char*)"dpi"));
    qreal node_size = 50;
    QString nodePtsWidth = QString("%1").arg(node_size / test1);

    //GV uses , instead of . for the separator in floats
    //    agnodeattr(m_graph->graph, (char*)"width", (char*)(nodePtsWidth.replace('.', ",")).toLatin1().data());

    //Set default attributes for future edges
    agattr(m_graph->graph, AGEDGE, const_cast<char*>("label"), const_cast<char*>(""));
    agattr(m_graph->graph, AGEDGE, const_cast<char*>("fontsize"), const_cast<char*>("25"));
    //TODO: pos als Attribut zu node und edge hinzufügen?
}

armarx::LayoutWorker::~LayoutWorker()
{
    std::unique_lock lock(m_graph->mutex);
    agclose(m_graph->graph);
}

void armarx::LayoutWorker::stateDeleted()
{
    emit deleteMe(id);
}

void armarx::LayoutWorker::layout(bool layoutAll)
{
    auto start = IceUtil::Time::now();

    if (true || layoutAll)
    {
        //        ARMARX_INFO_S << "Layouting with dot";
        layoutWithAlgo("dot");
        //broadcast changed substate positions
    }
    else
    {
        ARMARX_INFO_S << "Layouting with only transitions";
        layoutWithAlgo("nop"); // neato nop nop2 fdp
    }

    ARMARX_INFO_S << name << ": Layouting took " << (IceUtil::Time::now() - start).toMilliSecondsDouble();
    //broadcast changed transition support points
    emit layoutingFinished();
}

void armarx::LayoutWorker::isConnected(size_t workerId)
{
    if (id == workerId)
    {
        emit buildGraph(m_graph);
    }
}

void armarx::LayoutWorker::layoutWithAlgo(std::string algorithm)
{
    std::unique_lock lock(m_graph->mutex);
    //    ARMARX_INFO_S << "Layouting with graph ptr " << m_graph->graph;
    //    int nodeCard = agnnodes(m_graph->graph);
    //    ARMARX_INFO_S << "State " /*<< m_graph->graph->name*/ << " with " << nodeCard << " substates is to be layouted";
    //    agwrite(m_graph->graph, stdout);
    gvLayout(graphvizContext, m_graph->graph, algorithm.c_str());
    //    ARMARX_INFO_S /*<< m_graph->graph->name*/ << " succesfully layouted";
    //    gvRenderContext(graphvizContext, m_graph->graph, "dot", NULL);
    //    std::iostream str;
    attach_attrs(m_graph->graph);
    //    gvRender(graphvizContext, m_graph->graph, "dot", stdout);
    //    gvRenderFilename(graphvizContext, m_graph->graph, "svg", "./graphviz.svg");
    gvFreeLayout(graphvizContext, m_graph->graph);

}
