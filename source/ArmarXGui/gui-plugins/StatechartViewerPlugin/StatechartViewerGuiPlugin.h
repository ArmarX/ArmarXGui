/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    Mirko Waechter
* @author     (waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <QPointer>

#include <string>

#include <ArmarXCore/statechart/Statechart.h>
#include <ArmarXCore/interface/statechart/StatechartIce.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>


namespace armarx
{
    /**
        \page ArmarXGui-GuiPlugins-StatechartViewer StatechartViewer
        The StatechartViewer is a GUI to inspect currently running statecharts.
        Any state that is running can be loaded and inspected on all hierarchy levels.
        Additionally, the input-, local- and output-parameters are shown if a state was atleast visited once.
        Transitions of *active* states can be triggered by right-clicking on the transitions and selected the action in the context-menu.
        The following video shows the StatechartViewer briefly:<br/>
          \htmlonly
        <video width="700" controls>
        <source src="images/StatechartViewer-2016-12-30_18.44.56.mp4" type="video/mp4">
        Your browser does not support HTML5 video.
        </video>
          \endhtmlonly

        \see \ref Statechart
        \see \ref ArmarXGui-GuiPlugins-StatechartEditorController



      \class StatechartViewerGuiPlugin
      \see StatechartViewerController
      */
    class StatechartViewerGuiPlugin :
        public ArmarXGuiPlugin
    {
        Q_OBJECT
        Q_INTERFACES(ArmarXGuiInterface)
        Q_PLUGIN_METADATA(IID "ArmarXGuiInterface/1.00")
    public:
        StatechartViewerGuiPlugin();
    };




}

