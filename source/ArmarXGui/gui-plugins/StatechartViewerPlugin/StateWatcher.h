/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    Mirko Waechter
* @author     (waechter at kit dot edu)
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#pragma once
#include <ArmarXCore/core/ManagedIceObject.h>

#include <ArmarXGui/gui-plugins/StatechartViewerPlugin/model/State.h>
#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <ArmarXCore/interface/observers/ProfilerObserverInterface.h>
#include <ArmarXGui/gui-plugins/StatechartViewerPlugin/view/StateItem.h>

#include <mutex>

namespace armarx
{

    class StateWatcher :
        public armarx::ManagedIceObject,
        public ProfilerListener
    //        public EventListenerInterface
    {
    public:
        struct StateListenerData
        {
            StateIceBasePtr iceState;
            std::set<statechartmodel::StatePtr> states;
            std::set<StateItem*> stateItems;
            std::map<std::string, ::armarx::StateParameterMap> parameters;


        };

        using StateListenerDataMap = std::map<std::string, StateListenerData> ;

        StateWatcher();
        bool subscribeToState(StateIceBasePtr iceState, StateItem* state);
        bool subscribeToState(StateIceBasePtr iceState, statechartmodel::StatePtr state);
        bool unsubscribeState(StateItem* state);
        VariantPtr getStateParameter(statechartmodel::StatePtr state, const std::string paramMapType) const;
        ::armarx::StateParameterMap getStateParameterMap(statechartmodel::StatePtr state, const std::string paramMapType) const;

        void clear();

        static std::string ExtractStateName(const std::string globalIdString);
        // ManagedIceObject interface
        std::string getDefaultName() const override;
    protected:
        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;

        void subscriptionFunction();

        StateListenerDataMap stateMap;
        mutable std::mutex mapMutex;
        ConditionHandlerInterfacePrx conditionHandler;
        ProfilerObserverInterfacePrx profilerObserver;
        using SubscriptionMap = std::vector<std::pair<StateIceBasePtr, std::pair<statechartmodel::StatePtr, StateItem*>> >;
        SubscriptionMap subscriptionQueue;
        PeriodicTask<StateWatcher>::pointer_type subscriptionTask;


        // ProfilerListener interface
    public:
        void reportNetworkTraffic(const std::string&, const std::string&, Ice::Int, Ice::Int, const Ice::Current&) override;
        void reportEvent(const ProfilerEvent&, const Ice::Current&) override;
        void reportStatechartTransition(const ProfilerStatechartTransition& event, const Ice::Current&) override;
        void reportStatechartInputParameters(const ProfilerStatechartParameters& event, const Ice::Current&) override;
        void reportStatechartLocalParameters(const ProfilerStatechartParameters& event, const Ice::Current&) override;
        void reportStatechartOutputParameters(const ProfilerStatechartParameters&, const Ice::Current&) override;
        void reportProcessCpuUsage(const ProfilerProcessCpuUsage&, const Ice::Current&) override;
        void reportProcessMemoryUsage(const ProfilerProcessMemoryUsage&, const Ice::Current&) override;
        void reportEventList(const ProfilerEventList&, const Ice::Current&) override;
        void reportStatechartTransitionList(const ProfilerStatechartTransitionList&, const Ice::Current&) override;
        void reportStatechartInputParametersList(const ProfilerStatechartParametersList& data, const Ice::Current&) override;
        void reportStatechartLocalParametersList(const ProfilerStatechartParametersList&, const Ice::Current&) override;
        void reportStatechartOutputParametersList(const ProfilerStatechartParametersList&, const Ice::Current&) override;
        void reportProcessCpuUsageList(const ProfilerProcessCpuUsageList&, const Ice::Current&) override;
        void reportProcessMemoryUsageList(const ProfilerProcessMemoryUsageList&, const Ice::Current&) override;

        void reportStatechartTransitionWithParameters(const ProfilerStatechartTransitionWithParameters&, const Ice::Current&) override {}
        void reportStatechartTransitionWithParametersList(const ProfilerStatechartTransitionWithParametersList&, const Ice::Current&) override {}
    };
    using StateWatcherPtr = IceInternal::Handle<StateWatcher>;
}

