/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "TransitionItem.h"

#include "../model/Transition.h"
#include "../model/stateinstance/StateInstance.h"
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <QGraphicsSceneMouseEvent>
#include <QMenu>
#include <QPainter>
#include <QGraphicsScene>
#include <QPen>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXWidgetController.h>
#include "StateItem.h"



#include <QGraphicsView>
#include <QPropertyAnimation>
#include <QStyleOptionGraphicsItem>
#include "SplinePath.h"

#include <memory>
#include <cmath>


#define INITIAL_CIRCLE_RADIUS 10

namespace armarx
{

    TransitionItem::TransitionItem(statechartmodel::TransitionCPtr transition, QGraphicsItem* parent) :
        QGraphicsPathItem(parent),
        MorphingItem(this),
        transition(transition),
        transitionLabel(NULL),
        pointAttachedToMouse(-1),
        highlightColor(255, 0, 0),
        normalColor(Qt::black),
        animation(this, "color")
    {
        setCacheMode(DeviceCoordinateCache);
        color = Qt::black;
        setPen(QPen(color, 2, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
        setBrush(Qt::Dense3Pattern);
        setFlag(QGraphicsItem::ItemIsSelectable, true);
        setAcceptHoverEvents(true);
        if (transition)
        {
            transitionLabel = new TransitionLabel("", this);

            transitionLabel->setZValue(100000);
            updateLabel();
        }
        setZValue(100000);
    }

    IceUtil::Time TransitionItem::getLastHighlightTimestamp() const
    {
        return highlightAnimationStartTime;
    }

    QPainterPath TransitionItem::shape() const
    {

        QPainterPath path;

        if (isInitialTransition())
        {
            path.addEllipse(initialStateCircle);
        }

        path.addPolygon(shapePolygon);
        path.setFillRule(Qt::WindingFill);
        return path;
    }


    QRectF TransitionItem::boundingRect() const
    {


        if (isInitialTransition())
        {
            return QGraphicsPathItem::boundingRect().adjusted(-2 - INITIAL_CIRCLE_RADIUS, -2 - INITIAL_CIRCLE_RADIUS, 2, 2) | arrowHead.boundingRect();
        }
        else
        {
            return QGraphicsPathItem::boundingRect().adjusted(-2, -2, 2, 2) | arrowHead.boundingRect();
        }
    }

    bool TransitionItem::isInitialTransition() const
    {
        if (transition && !transition->sourceState)
        {
            return true;
        }

        return false;
    }

    void TransitionItem::setEditable(bool editable)
    {
        // TODO
    }

    void TransitionItem::recalcPath()
    {
        ARMARX_CHECK_EXPRESSION(transition != NULL);

        pointAttachedToMouse = -1;
        SupportPoints points = transition->supportPoints;
        //        ARMARX_DEBUG << points.controlPoints.size()  << " points at start";
        //adapt the first point
        if (transition->sourceState)
        {
            if (points.startPoint)
            {
                points.startPoint = std::make_shared<QPointF>(calcIntersection(transition->sourceState, *(points.startPoint)));
            }
            else if (points.controlPoints.size() > 1)
            {
                //points.startPoint = std::make_shared<QPointF>(calcIntersection(transition->sourceState, points.controlPoints.first()));
                points.controlPoints.replace(0, calcIntersection(transition->sourceState, points.controlPoints.first()));
            }
            else if (points.controlPoints.size() == 1)
            {
                //                ARMARX_DEBUG << "setting startpoint straight between sourceState and sfirst controlpoint";
                points.startPoint = std::make_shared<QPointF>(calcIntersection(transition->sourceState, points.controlPoints.first()));
                //points.controlPoints.replace(0, calcIntersection(transition->sourceState, points.controlPoints.first()));
            }
            else if (transition->destinationState) // calc start point so that it lies on the edge of the source state
            {
                //                ARMARX_DEBUG << "adding controlpoint straight between dest state and source on edge of source";
                points.startPoint = std::make_shared<QPointF>(calcIntersection(transition->sourceState, transition->destinationState->getBounds().center()));
                //points.controlPoints.push_front(calcIntersection(transition->sourceState, transition->destinationState->getBounds().center()));
            }
            else if (points.endPoint)
            {
                points.startPoint = std::make_shared<QPointF>(calcIntersection(transition->sourceState, *points.endPoint));
                //                points.controlPoints.push_front(calcIntersection(transition->sourceState, *points.endPoint));
            }

        }
        else
        {
            // this is the start transition
            //        if(transition->destinationState)
            //            points.controlPoints.push_back(transition->destinationState->getBounds().center());
            if (points.controlPoints.size() == 0)
            {
                points.controlPoints.push_back(QPointF(50, 80));
            }
            assert(!(points.startPoint));
        }

        //adapt the last point
        if (transition->destinationState)
        {
            if (points.endPoint)
            {
                ARMARX_DEBUG << "Adjusting start point";
                points.endPoint = std::make_shared<QPointF>(calcIntersection(transition->destinationState, *(points.endPoint)));
            }
            else if (points.controlPoints.size() > 2)
            {
                ARMARX_DEBUG << "Adjusting first control point";
                //replace the last control point
                points.controlPoints.replace(points.controlPoints.size() - 1,
                                             calcIntersection(transition->destinationState, points.controlPoints.last()));
            }
            else if (points.controlPoints.size() >= 1)
            {
                ARMARX_DEBUG << "inserting control point between last controlpoint and destination state";
                points.controlPoints.push_back(calcIntersection(transition->destinationState, points.controlPoints.last()));
            }
            else if (points.startPoint)
            {
                ARMARX_DEBUG << "inserting control point between startPoint and destination state";
                points.controlPoints.push_back(calcIntersection(transition->destinationState, *points.startPoint));
            }
        }
        else
        {
            //this is a detached transition
            //just keep control points as they were calculated otherwhere
        }

        // Not working - why?
        //    if(transition->sourceState && points.controlPoints.size() > 1)
        //    {
        //        points.controlPoints.replace(0, calcIntersection(transition->sourceState, points.controlPoints.first()));
        //    }

        //calculate path using the spline class
        auto pointList = points.toPointList();
        if (pointList.size() <= 1)
        {
            //this is the start transition in the initial creation
            pointList.push_front(QPointF(100, 400));
        }
        QPainterPath path = SplinePath::simplePath(pointList);
        //TODO: simplePath geht davon aus, dass immer 2 oder n = 1 mod 3 Kontrollpunkte gegeben sind.
        //      Ist das immer gegeben?




        if (isInitialTransition() && path.elementCount() > 0)
        {
            // draw initial start circle
            int r = 10;
            initialStateCircle.setTopLeft(QPointF(path.elementAt(0).x - r, path.elementAt(0).y - r));
            initialStateCircle.setWidth(r * 2);
            initialStateCircle.setHeight(r * 2);
        }

        setPath(path);
        recalcLabelPose();

        recalcShape();

    }

    void TransitionItem::recalcShape()
    {
        shapePolygon = calcHull();
    }

    void TransitionItem::setSelected(bool selected)
    {
        QGraphicsItem::setSelected(selected);
    }

    void TransitionItem::attachToMouse(int supportPointIndex)
    {
        pointAttachedToMouse = supportPointIndex;
    }

    void TransitionItem::highlightAnimation(int duration)
    {
        animation.stop();
        highlightAnimationStartTime = IceUtil::Time::now();
        animation.setDuration(duration);
        animation.setKeyValueAt(0, highlightColor);
        //        animation.setKeyValueAt(0.3, normalColor);
        //        animation.setKeyValueAt(0.99, QColor(255, 0, 0));
        animation.setKeyValueAt(1, normalColor);

        animation.setEasingCurve(QEasingCurve::InOutQuad);
        animation.start();
    }

    void TransitionItem::updateLabel()
    {
        if (transitionLabel)
        {
            auto labelText = transition->eventName + (transition->transitionUserCode ? " (C)" : "");
            transitionLabel->setText(labelText);
            transitionLabel->update();
            QString eventName = transition->eventName;
            if (!transition->sourceState)
            {
                eventName = "Initial Transition";
            }
            setToolTip("Event: " + eventName + " - " + (transition->transitionUserCode ? "Transition with user code" : "Transition without user code"));
        }
    }


    QPolygonF TransitionItem::calcHull(QPainter* painter) const
    {
        auto path = this->path();
        QPolygonF hull;

        auto addHull = [&](bool forward, float width)
        {
            float step = 0.0499999;
            float start = 0;
            float end = 1;
            float offsetAngle = M_PI / 2;

            if (!forward)
            {
                offsetAngle = -M_PI / 2;
            }

            QPointF p2;

            if (path.length() == 0)
            {
                return;
            }

            for (float percent = start; percent <= end; percent += step)
            {
                p2 = path.pointAtPercent(forward ? percent : 1 - percent);
                float angle = path.angleAtPercent(forward ? percent : 1 - percent) / 180 * M_PI;
                //            float angle = getAngle(p,p2);

                float x = width;
                float y = 0;
                float newAngle = angle + offsetAngle;
                newAngle *= -1;
                float x2 = cos(newAngle) * x - sin(newAngle) * y;
                float y2 = sin(newAngle) * x  + cos(newAngle) * y;
                hull.append(QPointF(x2, y2) + p2);

                if (painter)
                {
                    painter->drawEllipse(hull.last(), 2, 2);
                }

            }
        };

        if (painter)
        {
            painter->setPen(QPen(Qt::red, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
            painter->setBrush(QBrush(Qt::red, Qt::NoBrush));
        }

        addHull(true, 5);

        if (painter)
        {
            painter->setPen(QPen(Qt::green, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
            painter->setBrush(QBrush(Qt::red, Qt::NoBrush));
        }

        addHull(false, 5);


        return hull;
    }

    QColor TransitionItem::getColor() const
    {
        return this->color;
    }

    void TransitionItem::setColor(const QColor& color)
    {
        this->color = color;
        update();
    }

    QColor TransitionItem::getHighlightColor() const
    {
        return highlightColor;
    }

    void TransitionItem::setHighlightColor(const QColor& color)
    {
        this->highlightColor = color;

    }

    QPointF TransitionItem::calcIntersection(statechartmodel::StateInstancePtr state, const QPointF& supportPoint) const
    {
        QLineF centerLine(state->getBounds().center(), supportPoint);
        return calcIntersection(state, centerLine, supportPoint);
    }

    QPointF TransitionItem::calcIntersection(statechartmodel::StateInstancePtr state, QLineF centerLine, const QPointF& supportPoint) const
    {

        QPolygonF statePolygon(state->getBounds());
        QPointF p1 = statePolygon.first();
        QPointF p2;
        QPointF intersectPoint;
        QLineF polyLine;
        bool found = false;

        for (int i = 1; i < statePolygon.count(); ++i)
        {
            p2 = statePolygon.at(i);
            polyLine = QLineF(p1, p2);
            QLineF::IntersectType intersectType =
                polyLine.intersect(centerLine, &intersectPoint);

            if (intersectType == QLineF::BoundedIntersection)
            {
                found = true;
                break;
            }

            p1 = p2;
        }


        if (!found)
        {
            // support point is IN the state
            float minLength = std::numeric_limits<float>::max();

            for (int i = 1; i < statePolygon.count(); ++i)
            {
                QPointF curIntersectPoint;
                p2 = statePolygon.at(i);
                polyLine = QLineF(p1, p2);
                QLineF::IntersectType intersectType =
                    polyLine.intersect(centerLine, &curIntersectPoint);

                if (intersectType == QLineF::UnboundedIntersection)
                {
                    float length = QLineF(supportPoint, curIntersectPoint).length();

                    if (length < minLength)
                    {
                        intersectPoint = curIntersectPoint;
                        minLength = length;
                    }
                }

                p1 = p2;
            }
        }

        return intersectPoint;
    }

    QPolygonF TransitionItem::calcArrowHead(const QLineF& line)
    {
        qreal arrowSize = 15;

        double angle = ::acos(line.dx() / line.length());

        if (line.dy() >= 0)
        {
            angle = (M_PI * 2) - angle;
        }

        QPointF arrowP1 = line.p1() + QPointF(sin(angle + M_PI / 3) * arrowSize,
                                              cos(angle + M_PI / 3) * arrowSize);
        QPointF arrowP2 = line.p1() + QPointF(sin(angle + M_PI - M_PI / 3) * arrowSize,
                                              cos(angle + M_PI - M_PI / 3) * arrowSize);

        QPolygonF arrowHead;
        arrowHead <<  line.p1() << arrowP1 << arrowP2 ;

        return arrowHead;
    }

    void TransitionItem::recalcLabelPose()
    {
        if (!transitionLabel)
        {
            return;
        }

        QPainterPath curPath = path();

        if (curPath.elementCount() < 2)
        {
            return;
        }

        QPointF startPoint = curPath.elementAt(0);
        QPointF endPoint = curPath.elementAt(curPath.elementCount() - 1);
        QRectF transitionLabelRect = transitionLabel->boundingRect();

        float transitionLength = calcLengthOfLine(startPoint, endPoint);
        ARMARX_CHECK_EXPRESSION(transitionLength == transitionLength);

        if (transitionLength == 0)
        {
            transitionLength = 1;
        }
        float labelAngle = 0;
        //set position and angle of the transition label
        QPointF newPos;
        if (transition->labelCenterPosition)
        {
            //            ARMARX_INFO_S << "Using  precalculated label position";
            if (transition->labelFontPointSize)
            {
                //                ARMARX_INFO_S << "Using  precalculated label font size";
                transitionLabel->setBaseFontPointSize(*transition->labelFontPointSize);
                transitionLabel->setFontPointSize(*transition->labelFontPointSize);
            }
            if (transitionLabel->rotation() != 0)
            {
                transitionLabel->setRotation(0);
            }
            newPos = *transition->labelCenterPosition - QPointF(transitionLabel->boundingRect().width() * 0.5, transitionLabel->boundingRect().height() * 0.5);
        }
        else
        {
            //angle between x direction and edge
            float diff = (endPoint.x() - startPoint.x()) / transitionLength;
            diff = std::max(-1.0f, diff);
            diff = std::min(1.0f, diff);
            labelAngle = acos(diff);

            if (labelAngle != labelAngle)
            {
                ARMARX_WARNING << VAROUT(diff) << "; " << VAROUT(acos(diff));
            }

            int arg = 1;

            if (endPoint.y() - startPoint.y() < 0)
            {
                labelAngle = -labelAngle; //angle is negative if end is lower than start
            }

            //adjust angle if end is to the left of start
            if (endPoint.x() - startPoint.x() < 0)
            {
                labelAngle = M_PI + labelAngle;
                arg = -1;
            }
            newPos.setX(path().pointAtPercent(0.5).x() - arg * transitionLabelRect.width() * 0.5 * (endPoint.x() - startPoint.x()) / transitionLength);
            newPos.setY(path().pointAtPercent(0.5).y() - arg * transitionLabelRect.width() * 0.5 * (endPoint.y() - startPoint.y()) / transitionLength);
            double newAngle = labelAngle * 180.0 / M_PI;
            if (newAngle == newAngle)
            {
                if (transitionLabel->rotation() != newAngle)
                {
                    transitionLabel->setRotation(newAngle);
                }
            }
            else
            {
                ARMARX_WARNING << "Angle is NaN";
            }
        }

        transitionLabel->setPos(newPos);



    }

    float TransitionItem::calcLengthOfLine(QPointF start, QPointF end)
    {
        float xDiff = end.x() - start.x();
        float yDiff = end.y() - start.y();
        return sqrt(pow(xDiff, 2) + pow(yDiff, 2));
    }

    statechartmodel::TransitionCPtr TransitionItem::getTransition() const
    {
        return transition;
    }

    void TransitionItem::hoverEnterEvent(QGraphicsSceneHoverEvent* event)
    {
        //        ARMARX_INFO << "hoverEnterEvent " << transition->eventName;
        //        QFont font = transitionLabel->font();
        //        font.setPointSizeF(font.pointSizeF() * 2);
        //        transitionLabel->setFont(font);
        QGraphicsView* view = getView(event);
        if (view)
        {
            QRectF sceneScale = mapToScene(0.0f, 0.0f, 100.0f, 100.0f).boundingRect();
            QRectF globalScale = view->mapFromScene(sceneScale).boundingRect();
            float scale = globalScale.width();
            float newFontSize = transitionLabel->baseFontPointSize() / (scale / 100);
            transitionLabel->setEnlargedFontPointSize(newFontSize);
        }
        setColor(Qt::gray);
        update();
        transitionLabel->setHovering(true);
        transitionLabel->update();
        if (view && arrowHead.containsPoint(event->pos(), Qt::OddEvenFill))
        {
            view->viewport()->setCursor(Qt::OpenHandCursor);
        }
    }

    void TransitionItem::hoverMoveEvent(QGraphicsSceneHoverEvent* event)
    {
        QGraphicsView* view = getView(event);
        if (view)
        {
            if (arrowHead.containsPoint(event->pos(), Qt::OddEvenFill))
            {
                view->viewport()->setCursor(Qt::OpenHandCursor);
            }
            else
            {
                view->viewport()->setCursor(Qt::ArrowCursor);
            }
        }
    }

    void TransitionItem::hoverLeaveEvent(QGraphicsSceneHoverEvent* event)
    {
        //        ARMARX_INFO << "hoverLeaveEvent " << transition->eventName;
        //        QFont font = transitionLabel->font();
        //        font.setPointSizeF(font.pointSizeF() / 2);
        //        transitionLabel->setFont(font);
        setColor(normalColor);
        update();
        transitionLabel->setHovering(false);

        transitionLabel->update();
        QGraphicsView* view = getView(event);
        if (view)
        {
            view->viewport()->setCursor(Qt::ArrowCursor);
        }

    }
    void TransitionItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
    {
        //    ARMARX_INFO << "painting transition";
        if (!transition || (!transition->sourceState && !transition->destinationState))
        {
            return;
        }

        StateItem* parentState = qgraphicsitem_cast<StateItem*>(parentItem());
        LevelOfDetail detail = getLevelOfDetailByParent(painter, parentState);

        if (detail == eHidden)
            // only use scale of parent so that all sub elements of parent (states, transitions) are hidden at once and not one after the other
        {
            return;
        }

        painter->setBrush(QBrush(Qt::black, Qt::NoBrush));
        painter->setPen(QPen(Qt::black, 2, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
        recalcLabelPose();
        QColor tempColor = color;

        if (isSelected())
        {
            tempColor = Qt::darkGray;
            //        tempColor = tempColor.darker();
            //        QPainterPath curPath = path();
            //        for(int i = 1; i < curPath.elementCount()-1; i++)
            //        {
            //            painter->drawEllipse(curPath.elementAt(i), 1,1);
            //        }

        }
        //        else if (option->state & QStyle::State_MouseOver)
        //        {
        //            ARMARX_INFO << "Mouse over transition";
        //        }
        else if (transition && !transition->destinationState)
        {
            tempColor = Qt::darkRed;
        }


        QPen myPen = pen();
        myPen.setColor(tempColor);

        painter->setPen(myPen);
        painter->setBrush(QBrush(tempColor, Qt::NoBrush));
        setBrush(QBrush(tempColor, Qt::NoBrush));
        setPen(QPen(tempColor, 2, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
        painter->drawPath(path());
        //    QGraphicsPathItem::paint(painter, option, widget);
        //    QPen myPen = pen();

        myPen.setColor(tempColor);

        painter->setPen(myPen);
        painter->setBrush(tempColor);
        QPainterPath p = path();

        if (p.elementCount() < 2)
        {
            return;
        }

        QPointF p1 = p.elementAt(p.elementCount() - 1);
        QPointF p2 = p.elementAt(p.elementCount() - 2);
        QLineF line(p1, p2);

        arrowHead = calcArrowHead(line);
        painter->drawPolygon(arrowHead);


        if (!transition->sourceState) // circle for start state transition
        {
            painter->drawEllipse(initialStateCircle);
        }

        //     auto hull = calcHull(0);
        if (isSelected())
        {
            painter->setPen(QPen(Qt::gray, 1, Qt::DotLine, Qt::RoundCap, Qt::RoundJoin));
            painter->setBrush(QBrush(Qt::red, Qt::NoBrush));
            painter->drawPolygon(shapePolygon);
        }


    }


    void TransitionItem::mousePressEvent(QGraphicsSceneMouseEvent* event)
    {
        //    ARMARX_INFO_S << "mouse pos: " << event->pos();
        if (event->button() == Qt::LeftButton)
        {
            if (arrowHead.containsPoint(event->pos(), Qt::OddEvenFill))
            {
                pointAttachedToMouse = path().elementCount() - 1;
            }
            else
            {
                QPainterPath curPath = path();

                for (int i = 1; i < curPath.elementCount() - 1; i++)
                {
                    QPointF point = curPath.elementAt(i);
                    QRectF rect(point.x() - 5, point.y() - 5, 10, 10);

                    if (rect.contains(event->pos()))
                    {
                        pointAttachedToMouse = i;
                        ARMARX_INFO_S << VAROUT(rect) << " attached to point " << pointAttachedToMouse << "/" << curPath.elementCount();
                        break;
                    }
                }
            }
        }
        else if (event->button() == Qt::RightButton)
        {
            event->accept();
            scene()->clearSelection();
            QMetaObject::invokeMethod(this, "setSelected", Qt::QueuedConnection, Q_ARG(bool, true));
        }
        else
        {
            QGraphicsPathItem::mousePressEvent(event);
        }

        //    ARMARX_INFO_S << VAROUT(pointAttachedToMouse);
    }

    void TransitionItem::mouseMoveEvent(QGraphicsSceneMouseEvent* event)
    {
        //    ARMARX_INFO_S  << "buttonDownPos dist: " << (event->buttonDownPos(Qt::LeftButton)-event->pos()).manhattanLength() ;
        if ((event->buttonDownPos(Qt::LeftButton) - event->pos()).manhattanLength() > 10)
        {

            //    ARMARX_INFO_S << VAROUT(event->pos()) << ": " << VAROUT(pointAttachedToMouse);
            if (pointAttachedToMouse >= 0 && pointAttachedToMouse < path().elementCount())
            {
                //        ARMARX_INFO_S << VAROUT(path().elementCount()) << ": " << VAROUT(pointAttachedToMouse);
                QPainterPath p = path();
                p.setElementPositionAt(pointAttachedToMouse, event->pos().x(), event->pos().y());
                setPath(p);
                recalcLabelPose();
                recalcShape();
            }
        }

        QGraphicsPathItem::mouseMoveEvent(event);
    }

    void TransitionItem::mouseReleaseEvent(QGraphicsSceneMouseEvent* event)
    {

        if ((event->buttonDownPos(Qt::LeftButton) - event->pos()).manhattanLength() > 10 &&
            pointAttachedToMouse == path().elementCount() - 1)
        {
            emit transitionEndPlaced(transition, event->pos());
            recalcLabelPose();
        }

        pointAttachedToMouse = -1;
        QGraphicsPathItem::mouseReleaseEvent(event);
    }

    void TransitionItem::contextMenuEvent(QGraphicsSceneContextMenuEvent* event)
    {
        emit transitionContextMenuRequested(transition, event->screenPos(), event->pos());
    }




    TransitionLabel::TransitionLabel(QString labelText, QGraphicsItem* parent) :
        QGraphicsSimpleTextItem(labelText, parent),
        MorphingItem(this),
        animation(this, "fontPointSize")
    {
        setBaseFontPointSize(12);
        //        QFont currentFont = font();
        //        currentFont.setPointSize(fontBasePointSize);
        setFontPointSize(fontBasePointSize);
        hovering = false;
    }

    void TransitionLabel::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
    {
        if (parentItem())
        {
            StateItem* parentState = qgraphicsitem_cast<StateItem*>(parentItem()->parentItem());
            LevelOfDetail detail = getLevelOfDetailByParent(painter, parentState);

            // only use scale of parent so that all sub elements of parent (states, transitions) are hidden at once and not one after the other
            if (detail == eHidden)
            {
                return;
            }
        }
        //        float lod = QStyleOptionGraphicsItem::levelOfDetailFromTransform(painter->worldTransform());
        //        ARMARX_INFO_S << text().toStdString() << " LOD: " << lod << VAROUT(hovering);
        //        QFont currentFont = font();
        //        if (hovering)
        //        {
        //            float fontSize = currentFont.pointSizeF() * 2;
        //            if (fontSize * lod > fontMaxSize)
        //            {
        //                fontSize = fontMaxSize;
        //            }
        //            currentFont.setPointSizeF(fontSize);
        //        }
        //        else
        //        {
        //            currentFont.setPointSizeF(fontBasePointSize);
        //        }
        //        setFont(currentFont);
        QGraphicsSimpleTextItem::paint(painter, option, widget);
    }

    qreal TransitionLabel::fontPointSize() const
    {
        return font().pointSizeF();
    }

    qreal TransitionLabel::baseFontPointSize() const
    {
        return fontBasePointSize;
    }

    void TransitionLabel::setBaseFontPointSize(qreal newFontSize)
    {
        fontBasePointSize = newFontSize;
        fontEnlargedPointSize = fontBasePointSize * 2;
    }

    void TransitionLabel::setFontPointSize(qreal newFontSize)
    {
        QFont currentFont = font();
        currentFont.setPointSizeF(newFontSize);
        setFont(currentFont);
    }

    void TransitionLabel::setEnlargedFontPointSize(qreal newFontSize)
    {
        fontEnlargedPointSize = std::max(newFontSize, fontBasePointSize);
    }

    void TransitionLabel::setHovering(bool hovering)
    {
        animation.stop();
        animation.setDuration(200);
        if (hovering)
        {
            animation.setKeyValueAt(0, fontPointSize());
            animation.setKeyValueAt(1, fontEnlargedPointSize);
        }
        else
        {
            animation.setKeyValueAt(0, fontPointSize());
            animation.setKeyValueAt(1, fontBasePointSize);
        }

        animation.setEasingCurve(QEasingCurve::InOutQuad);
        animation.start();
        this->hovering = hovering;
    }

    bool TransitionLabel::getHovering() const
    {
        return hovering;
    }

}






