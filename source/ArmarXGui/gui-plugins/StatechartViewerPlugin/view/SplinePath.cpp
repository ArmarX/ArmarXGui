/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include <cassert>
#include <cmath>

#include <ArmarXCore/core/logging/Logging.h>
#include "SplinePath.h"

armarx::SplinePath::SplinePath()
{
}


QPainterPath armarx::SplinePath::simplePath(armarx::QPointList simpleControlPoints)
{
    //assert((simpleControlPoints.size() == 2) || (simpleControlPoints.size() % 3 == 1));

    QPainterPath path;

    if (simpleControlPoints.size() == 2)
    {
        //        ARMARX_DEBUG_S << "Direct Line";
        path.moveTo(simpleControlPoints.first());
        path.lineTo(simpleControlPoints.last());
    }
    else if (simpleControlPoints.size() == 3)
    {
        //        ARMARX_DEBUG_S << "Quad. path";
        path.moveTo(simpleControlPoints.first());
        path.quadTo(simpleControlPoints.at(1),
                    simpleControlPoints.last());
    }
    else
    {
        //        ARMARX_DEBUG_S << "complex path";
        path = complexPath(simpleControlPoints);
    }

    return path;

    /*
     * this doesn't work
     *
    QPointVector fullControlPoints;

    if(simpleControlPoints.size() > 1)
    {
        //add first given simpleControlPoint also as first complexControlPoint
        auto iter = simpleControlPoints.begin();
        fullControlPoints.push_back(*iter);

        //create point between the first two simpleControlPoints
        QPoint2Array linePoints = line(*iter, *(iter + 1));
        iter++;
        //add the new point and the second simpleControlPoint to the fullControlPoints
        fullControlPoints.insert(fullControlPoints.end(), linePoints.begin(), linePoints.end());

        while(iter < simpleControlPoints.end() - 1)
        {
            QPoint2Array linePoints = line(*iter, *(iter + 1));
            iter++;
            fullControlPoints.insert(fullControlPoints.end(), linePoints.begin(), linePoints.end());

            if(iter < simpleControlPoints.end() - 2)
            {
                QPoint4Array transitionPoints = smoothTransition(*iter, *(iter + 1), *(iter + 2));
                iter = iter + 2;
                fullControlPoints.insert(fullControlPoints.end(), transitionPoints.begin(), transitionPoints.end());
            }
        }
    }

    //ensure that there are enough control points to create a cubic Bezierspline
    while((fullControlPoints.size() % 3 != 1) || (fullControlPoints.size() < 4))
    {
        fullControlPoints.insert(fullControlPoints.begin(), simpleControlPoints.front());
    }

    return complexPath(fullControlPoints);
    */
}

QPainterPath armarx::SplinePath::complexPath(armarx::QPointList fullControlPoints)
{
    assert((fullControlPoints.size() % 3 == 1) && (fullControlPoints.size() > 3));

    QPainterPath path {fullControlPoints.at(0)};

    for (int i = 1; i < fullControlPoints.size(); i += 3)
    {
        assert(i + 2 < fullControlPoints.size()); //size of controlPoints == 1 mod 3

        path.cubicTo(fullControlPoints.at(i), fullControlPoints.at(i + 1), fullControlPoints.at(i + 2));
    }

    /*
    //print circles on control points
    for(QPointF controlPoint : fullControlPoints)
    {
        path.addEllipse(controlPoint, 5, 5);
    }
    */

    //only for debugging
    //pathToString(path);
    return path;
}

armarx::QPoint2Array armarx::SplinePath::line(QPointF start, QPointF end)
{
    QPointF direction = end - start; //vector from start to end
    QPointF middle = start + 0.5 * direction;

    QPoint2Array array {{middle, end}};
    return array;
}

armarx::QPoint4Array armarx::SplinePath::smoothTransition(QPointF start, QPointF middle, QPointF end)
{
    QPointF parallelLine = end - start; //vector from start to end

    //put the two new points on the line parallel to parallelLine that goes through middle.
    QPointF new1 = middle - relativeDistance * parallelLine;
    QPointF new2 = middle + relativeDistance * parallelLine;

    QPoint4Array array {{new1, middle, new2, end}};
    return array;
}

void armarx::SplinePath::pathToString(QPainterPath path)
{
    //    ARMARX_LOG_S << "New path begins:";
    for (size_t i = 0; i < static_cast<size_t>(path.elementCount()); i++)
    {
        QPainterPath::Element elem = path.elementAt(i);

        std::string type;

        switch (elem.type)
        {
            case QPainterPath::MoveToElement:
            {
                type = "Move";
                break;
            }

            case QPainterPath::LineToElement:
            {
                type = "Line";
                break;
            }

            case QPainterPath::CurveToElement:
            {
                type = "Curve";
                break;
            }

            default:
                break;
        }

        if (elem.type != QPainterPath::CurveToDataElement)
        {
            //            ARMARX_LOG_S << type << " at (" << elem.x << ", " << elem.y << ")";
        }
    }
}
