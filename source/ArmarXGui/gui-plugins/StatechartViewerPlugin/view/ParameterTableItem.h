/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <QGraphicsGridLayout>
#include <QGraphicsItem>
#include <QGraphicsWidget>

#include <memory>

#include "../model/StateParameter.h"
#include "../StateScene.h"

namespace armarx::statechartmodel
{
    class StateParameter;
    using StateParameterPtr = std::shared_ptr<StateParameter>;
    using StateParameterMap = QMap<QString, StateParameterPtr>;
}

namespace armarx
{
    class ParameterTableItem :
        public QGraphicsWidget
    {
    public:
        ParameterTableItem(statechartmodel::StateParameterMap parameterMap, QGraphicsItem* parent = 0);
        ~ParameterTableItem() override;

    private:
        void addStateParameterToLayout(statechartmodel::StateParameterPtr param, QGraphicsGridLayout* layout, int row);
        void setHeadRow(QGraphicsGridLayout* layout);

        statechartmodel::StateParameterMap paramMap;
        QGraphicsScene* parentScene;
    };

} //namespace armarx

