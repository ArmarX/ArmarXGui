/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once


#include <graphviz/types.h>
#include <graphviz/graph.h>
#include <graphviz/gvc.h>

#include <QGraphicsScene>
#include <QGraphicsEllipseItem>
#include <QGraphicsSimpleTextItem>
#include <QGraphicsPolygonItem>
#include <QPolygonF>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QFont>
#include <QGraphicsSceneMouseEvent>

#include <utility>

using floatPair = std::pair<float, float>;

class StateChartGraphEdge : public QGraphicsPathItem
{

public:

    /*
     * @brief StateChartGraphEdge copy constructor
     * It's supposed to be a copy constructor but not implemented as such
     */
    StateChartGraphEdge(const StateChartGraphEdge&);
    /*
     * @brief StateChartGraphEdge constructs "empty" StateChartGraphEdge as child of scene
     * @param scene GraphicsScene the new object belongs to
     */
    StateChartGraphEdge(QGraphicsScene* scene = 0);
    /*
     * @brief StateChartGraphEdge constructs an StateChartGraphEdge from gvGraphNode
     * @param gvGraphNode the graphviz node this node represents
     * @param m_transitionName name of transition that is represented by this edge
     * @param scene GraphicsScene the edge belongs to
     * @param dpi dpi value needed to paint the edge
     */
    StateChartGraphEdge(Agedge_t* gvGraphNode, const QString& transitionName, QGraphicsScene* scene = 0, float dpi = 72.0f);
    ~StateChartGraphEdge();
    StateChartGraphEdge& operator=(const StateChartGraphEdge&);

    void setTransitionName(const QString& name);
    QString getTransitionName()
    {
        return m_transitionName;
    }

    /*
     * @brief m_Path the edge's path
     * Approximated by Bezier curves.
     */
    QPainterPath m_Path;
    /*
     * @brief headNode the node from which the edge starts.
     */
    QString m_headNode;
    /*
     * @brief tailNode the node to which the edge leads.
     */
    QString m_tailNode;

    /*
     * @brief startControlPoint start point of the edge
     */
    floatPair m_startControlPoint;
    /*
     * @brief endControlPoint end point of the edge
     */
    floatPair m_endControlPoint;

    bool m_bInfoBoxUnfolded;

    bool infoBoxClicked(const QPointF& point);
    void setLineTo(QPointF newEndPoint);
    void setLineFrom(QPointF newStartPoint);
    void unfoldInfoBox();
    void foldInfoBox();
    QPointF getEndPoint();
    QPointF getStartPoint();
private:
    /*
     * @brief m_Arrow the edge's end arrow
     */
    QGraphicsPolygonItem* m_Arrow; //it's only set but never used

    /*
     * @brief m_Cross
     */
    QGraphicsPathItem* m_Cross; //TODO: find out what it's there for

    /*
     * @brief m_InfoText text of m_InfoBox
     */
    QGraphicsSimpleTextItem* m_InfoText;

    /*
     * @brief m_InfoBox box containing edge information
     */
    QGraphicsRectItem* m_InfoBox;

    QPainterPath m_CrossPainter;
    QGraphicsSimpleTextItem* m_pTransitionLabel;
    QGraphicsEllipseItem* m_pTransitionInfoPoint; //TODO: find out what it's there for
    float m_InfoPointRadius;
    float m_ArrowScale;

    /*
     * @brief m_transitionName the name of the transition represented by this edge.
     */
    QString m_transitionName;

    //private functions

    void setInfoPointPosition(QPointF newPosition, double labelAngle);

    /*
     * @brief StateChartGraphEdge::setArrowHeadPosition sets the arrowheads position
     * The arrowhead's shape also is calculated.
     * @param headLineSegmentStart start of arrow segment
     * @param headLineSegmentEnd point of arrowhead
     */
    void setArrowHeadPosition(QPointF headLineSegmentStart, QPointF headLineSegmentEnd);

    /*
     * @brief calcLengthOfLine calculates length of line between start and end
     * @param start start of line
     * @param end end of line
     * @return length of line between start and end
     */
    float calcLengthOfLine(floatPair start, floatPair end);
    /*
     * @brief calcLengthOfLine calculates length of line between start and end
     * @param start start of line
     * @param end end of line
     * @return length of line between start and end
     */
    float calcLengthOfLine(QPointF start, QPointF end);
    /*
     * @brief calcControlPoints calculates all the control points of the edge.
     * @param gvGraphEdge the edge whose control points are to be calculated
     * @return vector of controlPoints starting with startpoint and ending with endpoint
     */
    std::vector<floatPair> calcControlPoints(Agedge_t* gvGraphEdge);
    /*
     * @brief calcPath construct path with the given controlPoints
     * @param controlPoints control points for Bezier polygon
     */
    void calcPath(std::vector<floatPair> controlPoints);

    friend class GVGraphStateChart;
};

