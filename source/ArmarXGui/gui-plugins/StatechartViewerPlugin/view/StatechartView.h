/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once


#include "GraphicsViewZoom.h"
#include "../model/State.h"
#include "../model/stateinstance/StateInstance.h"
#include "../StateScene.h"
#include <QGraphicsScene>
#include <QWidget>

#include <ArmarXGui/gui-plugins/StatechartViewerPlugin/layout/LayoutController.h>
#include <ArmarXGui/gui-plugins/StatechartViewerPlugin/layout/LayoutThread.h>

namespace Ui
{
    class StatechartView;
}

namespace armarx
{

    class StatechartView: public QWidget
    {
        Q_OBJECT

    public:
        explicit StatechartView(statechartmodel::StatePtr state = statechartmodel::StatePtr(new statechartmodel::State()), bool enableLayouting = true, QWidget* parent = 0);
        ~StatechartView() override;

        static statechartmodel::StatePtr CreateTestStates();
        statechartmodel::StateInstancePtr getStateInstance() const;
        StateScene* getScene() const
        {
            return stateScene;
        }
        QGraphicsView* getGraphicsView() const;
        Graphics_view_zoom* getGraphicsViewZoomer() const;

        LayoutController& getLayoutController();
    signals:
        void selectedStateChanged(statechartmodel::StateInstancePtr selectedStateInstance);
    public slots:
        void setState(statechartmodel::StatePtr state);
        void setOriginalZoom();
        void deleteSelectedStates();
        void viewAll();
        void onItemChange();
        void showSubSubstates(bool show = true);
    private:
        //        QList<statechartmodel::StatePtr> stateClasses;
        ::Ui::StatechartView* ui;
        StateScene* stateScene;
        Graphics_view_zoom* zoomer;
        //        LayoutController layoutController;
        LayoutThread layoutThread;
        // QWidget interface
    public:
        QSize sizeHint() const override;
    };
}

