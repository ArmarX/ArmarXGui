/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "SceneOverview.h"

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXWidgetController.h>

namespace armarx
{

    SceneOverview::SceneOverview(QWidget* parent) :
        QGraphicsView(parent),
        graphicsViewZoomer(NULL)

    {

    }

    void SceneOverview::setZoomer(Graphics_view_zoom* zoomer)
    {
        disconnect(this);
        graphicsViewZoomer = zoomer;
        connect(graphicsViewZoomer, SIGNAL(moved()), this, SLOT(retrieveUpdate()));
        connect(graphicsViewZoomer, SIGNAL(zoomed()), this, SLOT(retrieveUpdate()));
    }

    void SceneOverview::retrieveUpdate()
    {
        if (!graphicsViewZoomer || !graphicsViewZoomer->getView())
        {
            return;
        }

        selectionRect = graphicsViewZoomer->getView()->mapToScene(graphicsViewZoomer->getView()->rect()).boundingRect();

        if (scene())
        {
            fitInView(scene()->itemsBoundingRect().adjusted(-30, -30, 30, 30), Qt::KeepAspectRatio);
        }

        //    ARMARX_INFO_S << "overview update: " << selectionRect;
        update();
    }

}




void armarx::SceneOverview::drawForeground(QPainter* painter, const QRectF& rect)
{
    QPen pen(QBrush(Qt::red), 5);
    painter->setPen(pen);
    painter->drawRect(selectionRect);
    QGraphicsView::drawForeground(painter, rect);
    //    ARMARX_INFO_S << "drawing foreground";
}


void armarx::SceneOverview::resizeEvent(QResizeEvent*)
{
    retrieveUpdate();
}
