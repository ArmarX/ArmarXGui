/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "StateItem.h"
#include "TransitionItem.h"

#include "../StateScene.h"
#include "../model/State.h"
#include "../model/Transition.h"
#include "../model/stateinstance/StateInstance.h"
#include "../model/StateMimeData.h"

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXWidgetController.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <QGraphicsProxyWidget>
#include <QMenu>
#include <QPainter>
#include <QtGui>
#include <QStyleOptionGraphicsItem>
#include <QGraphicsSceneContextMenuEvent>
#include <QMessageBox>


#define MAXFONTSIZE 300.0f



namespace armarx
{
    StateItem::StateItem(statechartmodel::StateInstancePtr state, QGraphicsItem* parent) :
        RoundRectItem(QRectF(0, 0, state->getClassSize().width(), state->getClassSize().height()),
                      //                  QColor(Qt::blue).lighter(190),
                      QColor(),
                      parent),
        MorphingItem(this),
        state(state)
        //    QGraphicsWidget(parent)
    {
        //    if(state && state->getStateClass())
        setAcceptDrops(true);
        //    stateScene = dynamic_cast<StateScene*>(scene());
        setup(parent);
        connectSlots();
        initialCreation();
        setStateColor();
        setRimColor();
        setZValue(10000);
        updateToolTip();
        if (state->getStateClass())
        {
            setEditable(state->getStateClass()->isEditable());
        }
    }

    StateItem::~StateItem()
    {
        //        ARMARX_INFO << state->getInstanceName();
    }

    void StateItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option,
                          QWidget* widget)
    {
        StateItem* parentState = qgraphicsitem_cast<StateItem*>(parentItem());
        LevelOfDetail detail = getLevelOfDetailByParent(painter, parentState);

        if (detail == eHidden) // only use scale of parent so that all sub elements of parent (states, transitions) are hidden at once and not one after the other
        {
            return;
        }



        statechartmodel::StatePtr stateClass = state->getStateClass();

        // Draw state rect
        RoundRectItem::paint(painter, option, widget);

        // draw substate rect for debug purposes
        //        if(state->getStateClass())
        //            painter->drawRect(QRectF(state->getStateClass()->margin.topLeft(),
        //                                     QPointF(state->getStateClass()->getSize().width()-state->getStateClass()->margin.width(),
        //                                             state->getStateClass()->getSize().height()-state->getStateClass()->margin.height())));
        /*
         * There are three different cases which must be differentiated:
         * 1. Class known & substates: Show both names at top
         * 2. Class known & no substates: Show both names centered (instance label centered, class name below)
         * 3. Class unknown & no substates: Show class name centered
         * (Class unknown & substates: Not possible, substates are deduced from state class)
         */

        bool showSubstates = detail != eNoSubstates && stateClass && !stateClass->getSubstates().empty();

        bool showStateClassName = stateClass && (instanceName != stateClass->getStateName() /*|| !showSubstates*/);

        const int renderFlags = Qt::TextSingleLine | Qt::AlignTop | Qt::AlignHCenter;

        QRectF textRect = bounds;

        float instanceNameWidthRelative = showSubstates ? 0.5f : 0.8f;


        float instanceNameScaleFactor = textRect.width() * instanceNameWidthRelative / painter->fontMetrics().width(instanceName);

        if (showSubstates)
        {
            if (showStateClassName)
            {
                textRect.adjust(0, 10, 0, 0);
            }
            else
            {
                textRect.adjust(0, 20, 0, 0);
            }
        }
        else
        {
            textRect.adjust(0, (bounds.height() - painter->fontMetrics().height() * instanceNameScaleFactor) / 2, 0, 0);

        }

        qreal lodT = QStyleOptionGraphicsItem::levelOfDetailFromTransform(painter->worldTransform());
        QFont f = painter->font();
        QRectF maxFsize(0, 0, 0, MAXFONTSIZE);
        f.setPointSizeF(std::min<float>(f.pointSizeF() * instanceNameScaleFactor, (maxFsize).height() / lodT));
        painter->setFont(f);
        float textHeightScale = 0.8;


        if (showSubstates && state->getStateClass() && painter->fontMetrics().height() > 0.5 * state->getStateClass()->margin.top())
        {
            float margin = state->getStateClass()->margin.top();
            float height = painter->fontMetrics().height();
            float size = f.pointSizeF();
            textHeightScale = margin / height;
            if (showStateClassName)
            {
                textHeightScale *= 0.45;
            }
            else
            {
                textHeightScale *= 0.7;
            }
            f.setPointSizeF(size * textHeightScale);
            painter->setFont(f);
            //            ARMARX_INFO << "Text to high: " << textHeightScale;
        }

        painter->setFont(f);
        painter->drawText(textRect, renderFlags, instanceName);

        if (showStateClassName && !stateClass->getStateName().isEmpty())
        {
            QString classNameText = "[state: " + stateClass->getStateName() + "]";
            float instanceNamePointSize = f.pointSizeF();
            float classNameScaleFactor = textRect.width() * 0.8f / painter->fontMetrics().width(classNameText);



            textRect.adjust(0, painter->fontMetrics().height() * 1.0, 0, 0);

            lodT = QStyleOptionGraphicsItem::levelOfDetailFromTransform(painter->worldTransform());
            QRectF maxFsize(0, 0, 0, MAXFONTSIZE);
            float classNamePointSize = textHeightScale * std::min<float>(f.pointSizeF() * classNameScaleFactor, (maxFsize).height() / lodT);
            classNamePointSize = std::min(classNamePointSize, instanceNamePointSize); // never bigger than instance name
            f.setPointSizeF(classNamePointSize);
            painter->setFont(f);

            painter->drawText(textRect, renderFlags, classNameText);
        }
    }

    void StateItem::notifyStateChanged(statechartmodel::SignalType signalType)
    {
        ARMARX_CHECK_EXPRESSION(state != NULL);

        if (signalType == statechartmodel::eChanged)
        {
            updateToolTip();
            setRimColor();
            update();
            setSize(state->getClassSize());
            setInstanceName(state->getInstanceName());
            //            ARMARX_WARNING_S << "Resizing " << state->getInstanceName() << VAROUT(state->getClassSize());
            update(bounds);

        }
    }

    void StateItem::notifySubstateChanged(statechartmodel::StateInstancePtr substate, statechartmodel::SignalType signalType)
    {

        switch (signalType)
        {
            case statechartmodel::eAdded:
            {
                //                ARMARX_INFO << "substate added: " << substate->getInstanceName();
                StateInstanceItemMap::iterator it = subStates.find(substate);

                if (it != subStates.end())
                {
                    ARMARX_WARNING << "substate already in list";
                    return;
                }

                StateItem* stateItem = new StateItem(substate, this);
                subStates.insert(substate, stateItem);
                connect(stateItem, SIGNAL(stateItemMoved(QPointF)), substate.get(), SLOT(setPosition(QPointF)));
                connect(stateItem, SIGNAL(stateItemBoundingBoxChanged(float)), substate.get(), SLOT(setBoundingBox(float)));
                stateItem->setInstanceName(substate->getInstanceName());
                stateItem->setScale(substate->getScale());
                //        ARMARX_INFO_S << "topleft: "  << substate->getTopLeft();
                stateItem->setPos(substate->getTopLeft());
                stateItem->setSize(substate->getClassSize());


                stateItem->adjustSize();
                update();
                //        ARMARX_INFO << "Added state " << stateName << " to Scene";
            }
            break;

            case statechartmodel::eChanged:
            {
                //        ARMARX_INFO << "substate changed: " << substate->getInstanceName();
                StateInstanceItemMap::iterator it = subStates.find(substate);

                if (it == subStates.end())
                {
                    break;
                }

                StateItem* substateItem = it.value();
                substateItem->setPos(substate->getTopLeft());
                substateItem->setSize(substate->getClassSize());
                substateItem->setScale(substate->getScale());
                substateItem->setInstanceName(substate->getInstanceName());
                substateItem->setStateColor();
                substateItem->setRimColor();
                notifyTransitionsChanged(substate, statechartmodel::eChanged);

            }
            break;

            case statechartmodel::eUnchanged:
            {

            }
            break;

            case statechartmodel::eRemoved:
            {
                //        ARMARX_INFO << "Removing state " << substate->getInstanceName() << " from Scene";
                StateInstanceItemMap::iterator it = subStates.find(substate);

                if (it == subStates.end())
                {
                    //            ARMARX_INFO << "Could not find substate";
                    break;
                }

                QPointer<StateItem> substateItem = it.value();
                subStates.remove(substate);
                if (substateItem)
                {
                    substateItem->prepareGeometryChange();
                    QObject::disconnect(substateItem);
                    delete substateItem.data();
                }
            }
            break;

            default:
                //        ARMARX_INFO << "Unhandled state changed";
                break;
        }

    }

    void StateItem::notifyTransitionChanged(statechartmodel::TransitionCPtr transition, statechartmodel::SignalType signalType)
    {
        //    ARMARX_INFO << "Transition Changed";
        switch (signalType)
        {
            case statechartmodel::eAdded:
            {
                TransitionItem* t = new TransitionItem(transition, this);
                transitions.insert(transition, t);
                t->recalcPath();
                connect(t, SIGNAL(transitionEndPlaced(statechartmodel::TransitionCPtr, QPointF)), this, SLOT(transitionMoved(statechartmodel::TransitionCPtr, QPointF)));
                connect(t, SIGNAL(transitionContextMenuRequested(statechartmodel::TransitionCPtr, QPoint, QPointF)), this, SLOT(forwardTransitionContextMenuRequested(statechartmodel::TransitionCPtr, QPoint, QPointF)));
            }
            break;

            case statechartmodel::eChanged:
            {
                TransitionMap::iterator it = transitions.find(transition);

                if (it == transitions.end())
                {
                    break;
                }
                TransitionItem* t = it.value();
                t->updateLabel();
                t->recalcPath();
            }
            break;

            case statechartmodel::eRemoved:
            {
                TransitionMap::iterator it = transitions.find(transition);

                if (it == transitions.end())
                {
                    break;
                }
                //                ARMARX_DEBUG << "Removing transition " << transition->eventName;
                TransitionItem* t = it.value();
                QObject::disconnect(t);
                transitions.remove(transition);
                //                t->prepareGeometryChange();
                delete t;

            }
            break;
            case statechartmodel::eActivated:
            {
                TransitionMap::iterator it = transitions.find(transition);

                if (it == transitions.end())
                {
                    break;
                }
                int highlightDuration = 10000;
                TransitionItem* t = it.value();
                t->highlightAnimation(highlightDuration);
            }
            break;
            default:

                break;
        }
    }

    void StateItem::notifyTransitionsChanged(statechartmodel::StateInstancePtr state, statechartmodel::SignalType signalType)
    {
        TransitionMap::iterator it = transitions.begin();

        for (; it != transitions.end(); it++)
        {
            auto t = it.key();

            if (t->sourceState == state  || t->destinationState == state)
            {
                notifyTransitionChanged(t, signalType);
            }
        }
    }

    void StateItem::forwardTransitionContextMenuRequested(statechartmodel::TransitionCPtr transition, QPoint mouseScreenPos, QPointF mouseItemPos)
    {
        emit transitionContextMenuRequested(transition, state->getStateClass(), mouseScreenPos, mouseItemPos);
    }

    void StateItem::transitionMoved(statechartmodel::TransitionCPtr transition, QPointF dropPoint)
    {
        TransitionMap::iterator it = transitions.find(transition);
        ARMARX_CHECK_EXPRESSION(it != transitions.end());

        StateInstanceItemMap::iterator itState = subStates.begin();

        for (; itState != subStates.end(); itState++)
        {
            StateItem* item = itState.value();
            QPointF dropPointMapped = item->mapFromParent(dropPoint);

            if (item->contains(dropPointMapped))
            {

                ARMARX_IMPORTANT << "dropped on " << item->getStateInstance()->getInstanceName();
                QList<QPointF> suppPoint;
                suppPoint.push_back(dropPoint);
                state->getStateClass()->updateTransitionDestination(transition, item->getStateInstance(), suppPoint);
                return;
            }
        }

        state->getStateClass()->detachTransitionDestination(transition, dropPoint);

    }



    void StateItem::viewAll()
    {
        //    ui.substatesView->fitInView( substateScene->sceneRect(), Qt::KeepAspectRatio );
    }

    void StateItem::adjustSize()
    {
        if (parentItem())
        {
            //        ARMARX_INFO << "adjusting size";
            //        state->setBoundingBox(100);
            //        bounds.setWidth(parentItem()->boundingRect().width()*0.3);
            //        bounds.setHeight(parentItem()->boundingRect().height()*0.3);
        }
    }



    void StateItem::setup(QGraphicsItem* parent)
    {




    }

    void StateItem::connectSlots()
    {
        ARMARX_CHECK_EXPRESSION(state != NULL);

        if (state->getStateClass())
        {
            connect(state->getStateClass().get(), SIGNAL(stateChanged(statechartmodel::SignalType)), this, SLOT(notifyStateChanged(statechartmodel::SignalType)));
            connect(state->getStateClass().get(), SIGNAL(substateChanged(statechartmodel::StateInstancePtr, statechartmodel::SignalType)), this, SLOT(notifySubstateChanged(statechartmodel::StateInstancePtr, statechartmodel::SignalType)));
            connect(state->getStateClass().get(), SIGNAL(transitionChanged(statechartmodel::TransitionCPtr, statechartmodel::SignalType)), this, SLOT(notifyTransitionChanged(statechartmodel::TransitionCPtr, statechartmodel::SignalType)));
            connect(this, SIGNAL(stateItemResized(QSizeF)), state->getStateClass().get(), SLOT(setSize(QSizeF)));

        }

        if (scene())
        {
            QVariant v;
            v.setValue(scene());
            itemChange(ItemSceneChange, v);
        }

    }

    void StateItem::initialCreation()
    {
        ARMARX_CHECK_EXPRESSION(state != NULL);
        notifyStateChanged(statechartmodel::eChanged);

        if (!state->getStateClass())
        {
            return;
        }

        const auto& ts = state->getStateClass()->getTransitions(true);

        for (int i = 0; i < ts.size(); i++)
        {
            notifyTransitionChanged(ts.at(i), statechartmodel::eAdded);
        }

        const auto& sl =  state->getStateClass()->getSubstates();

        for (auto i = sl.begin(); i != sl.end(); i++)
        {
            notifySubstateChanged(i.value(), statechartmodel::eAdded);
        }


    }



    void StateItem::setStateColor()
    {
        if (!state)
        {
            return;
        }
        int level = getInstanceLevel();
        switch (state->getType())
        {
            case eNormalState:
            {
                QColor color(52, 152, 219);
                if (level % 2 == 0)
                {
                    color = color.lighter(130);
                }
                else
                {
                    color = color.lighter(110);
                }
                setColor(color);
            }
            break;

            case eRemoteState:
            {
                QColor color(26, 188, 156);
                if (level % 2 == 0)
                {
                    color = color.lighter(130);
                }
                else
                {
                    color = color.lighter(110);
                }
                setColor(color);
            }
            break;

            case eDynamicRemoteState:
                setColor(QColor(155, 89, 182));
                break;

            case eFinalState:
            {
                QColor color(241, 196, 15);
                color = color.lighter(120);
                setColor(color);
            }


            default:
                break;

        }
    }

    void StateItem::setRimColor()
    {
        if ((state
             && state->isActive()) || active)
        {
            setRimPen(QPen(Qt::red, 10));
        }
        else
        {
            setRimPen(QPen(Qt::black, 2));
        }
        update();
    }

    void StateItem::updateToolTip()
    {
        QString tip;
        if (state)
        {
            tip = "StateType: " + statechartmodel::State::StateTypeToString(state->getType());
            if (state->getStateClass())
            {
                auto desc = state->getStateClass()->getDescription();
                if (!desc.isEmpty())
                {
                    tip += "\n" + desc;
                }
            }
        }
        setToolTip(tip);
    }

    void StateItem::removeChildren()
    {
        auto curScene = scene();
        if (curScene)
        {
            for (auto& t : transitions)
            {
                //                t->prepareGeometryChange();
                curScene->removeItem(t);
            }
        }
        for (auto& s : subStates)
        {
            s->prepareGeometryChange();
            s->removeChildren();
            if (curScene)
            {
                curScene->removeItem(s);
            }
        }
    }

    int StateItem::getInstanceLevel() const
    {
        int level = 0;
        const QGraphicsItem* parent = this;
        while (dynamic_cast<StateItem*>(parent->parentItem()))
        {
            parent = parent->parentItem();
            level++;
        }
        return level;
    }

    QPointF StateItem::adjustPosition(QPointF& newPos)
    {
        if (state)
        {
            return state->adjustPosition(newPos);
        }
        ARMARX_INFO << "state ptr is null";
        return newPos;
    }

    int StateItem::getMaxShownSubstateLevel() const
    {
        return maxShownSubstateLevel;
    }

    void StateItem::setMaxShownSubstateLevel(int value)
    {
        maxShownSubstateLevel = value;
        for (auto& substate : subStates)
        {
            substate->setMaxShownSubstateLevel(value);
        }
        for (auto& item : childItems())
        {
            item->update();
        }
    }

    void StateItem::setActiveSubstate(const QString& substateName)
    {
        auto substates = getSubstateItems();
        for (auto substate : substates)
        {
            bool active = (substate->getInstanceName() == substateName);
            substate->setActiveState(active);
        }
    }

    void StateItem::clearActiveSubstate()
    {
        auto substates = getSubstateItems();
        for (auto substate : substates)
        {
            substate->setActiveState(false);
        }
    }

    QString StateItem::getFullStatePath() const
    {
        QString path = instanceName;
        StateItem* parent = qgraphicsitem_cast<StateItem*>(parentItem());
        while (parent)
        {
            path = parent->getInstanceName() + "/" + path;
            parent = qgraphicsitem_cast<StateItem*>(parent->parentItem());
        }
        return path;
    }

    StateItem* StateItem::getParentStateItem() const
    {
        StateItem* parent = qgraphicsitem_cast<StateItem*>(parentItem());
        return parent;
    }

    QVector<StateItem*> StateItem::getSubstateItems() const
    {
        QVector<StateItem*> result;
        for (QGraphicsItem* item : this->childItems())
        {
            StateItem* state = dynamic_cast<StateItem*>(item);
            if (state)
            {
                result.push_back(state);
            }
        }
        return result;
    }

    QVector<TransitionItem*> StateItem::getTransitionItems() const
    {
        QVector<TransitionItem*> result;
        for (QGraphicsItem* item : this->childItems())
        {
            TransitionItem* transition = dynamic_cast<TransitionItem*>(item);
            if (transition)
            {
                result.push_back(transition);
            }
        }
        return result;
    }

    QString StateItem::getInstanceName() const
    {
        return instanceName;
    }

    void StateItem::setInstanceName(const QString& value)
    {
        if (value != instanceName)
        {
            instanceName = value;
            update();
        }
    }

    void StateItem::setActiveState(bool active)
    {
        this->active = active;
        setRimColor();
    }

    bool StateItem::itemResizing(const QRectF& oldSize, QRectF& proposedSize)
    {
        if (!state->getStateClass())
        {
            return false;
        }

        return true;
    }



    void StateItem::itemResized(const QRectF& oldSize, const QRectF& newSize)
    {
        if (oldSize != newSize)
        {
            emit stateItemResized(newSize.size());
        }
    }

    void StateItem::itemMoved(const QPointF& oldPos, const QPointF& newPos)
    {
        //    ARMARX_INFO << VAROUT(oldPos) << " " << VAROUT(newPos);
        if (oldPos != newPos)
        {
            //        ARMARX_INFO << "Changed";
            emit stateItemMoved(newPos);

        }
    }

    void StateItem::itemBoundingBoxChanged(float oldSize, float size)
    {

        if (oldSize != size)
        {
            //        ARMARX_INFO << VAROUT(oldSize) << " newBB: " << size;
            emit stateItemBoundingBoxChanged(size);
        }
    }


    void StateItem::contextMenuEvent(QGraphicsSceneContextMenuEvent* event)
    {
        if (isLevelOfDetailLow(event))
        {
            event->ignore();
            ARMARX_DEBUG << "Ignoring context menu event on " << state->getInstanceName();
            return;
        }
        if (scene())
        {
            scene()->clearSelection();
        }

        this->setSelected(true);
        emit stateContextMenuRequested(state, event->screenPos(), event->pos());
        event->accept();
    }


    QVariant StateItem::itemChange(GraphicsItemChange change, const QVariant& value)
    {
        if (change == ItemSelectedHasChanged && value.toBool() == true)
        {
            //        ARMARX_IMPORTANT << state->getInstanceName() << " was selected with scale " << scale();
            //        setScale(scale()*1.1);
        }
        else if (change == ItemSelectedHasChanged && value.toBool() == false)
        {
            //        ARMARX_IMPORTANT << state->getInstanceName() << " was UNselected";
            //        setScale(scale()*0.9);
        }
        else if (change == ItemScaleHasChanged)
        {
            emit stateItemBoundingBoxChanged(std::max(bounds.width(), bounds.height())*value.toFloat());
            //        itemBoundingBoxChanged(std::max(oldRect.width(), oldRect.height()),
            //                               std::max(bounds.width(), bounds.height()));
            //        emit stateItemScaled(value.toFloat());
        }
        else if (change == ItemSceneChange)
        {
            StateScene* oldStateScene = dynamic_cast<StateScene*>(scene());

            if (oldStateScene)
            {
                disconnect(this, SIGNAL(stateContextMenuRequested(statechartmodel::StateInstancePtr, QPoint, QPointF)), oldStateScene, SIGNAL(stateContextMenuRequested(statechartmodel::StateInstancePtr, QPoint, QPointF)));
                disconnect(this, SIGNAL(transitionContextMenuRequested(statechartmodel::TransitionCPtr, statechartmodel::StatePtr, QPoint, QPointF)), oldStateScene, SIGNAL(transitionContextMenuRequested(statechartmodel::TransitionCPtr, statechartmodel::StatePtr, QPoint, QPointF)));
            }

            StateScene* stateScene = dynamic_cast<StateScene*>(value.value<QGraphicsScene*>());

            if (stateScene)
            {
                //            ARMARX_INFO_S << "scene OF " << state->getInstanceName() << " has changed - StateScene";
                connect(this, SIGNAL(stateContextMenuRequested(statechartmodel::StateInstancePtr, QPoint, QPointF)),
                        stateScene, SIGNAL(stateContextMenuRequested(statechartmodel::StateInstancePtr, QPoint, QPointF)));
                connect(this, SIGNAL(transitionContextMenuRequested(statechartmodel::TransitionCPtr, statechartmodel::StatePtr, QPoint, QPointF)),
                        stateScene, SIGNAL(transitionContextMenuRequested(statechartmodel::TransitionCPtr, statechartmodel::StatePtr, QPoint, QPointF)));
            }

            //        else
            //            ARMARX_INFO_S << "scene OF " << state->getInstanceName() << " has changed - noScene";
        }

        return RoundRectItem::itemChange(change, value);
    }

    void StateItem::dragEnterEvent(QGraphicsSceneDragDropEvent* event)
    {
        //    ARMARX_INFO << "Drag Enter event " << state->getInstanceName();
        const AbstractStateMimeData* data =  qobject_cast<const AbstractStateMimeData*>(event->mimeData());

        if (data && state && state->getStateClass() && data->getState() && state->getType() == eNormalState)
        {
            event->setDropAction(Qt::LinkAction);
        }
        else
        {
            event->setDropAction(Qt::IgnoreAction);
        }

        event->accept();
    }

    void StateItem::dragMoveEvent(QGraphicsSceneDragDropEvent* event)
    {
        dragEnterEvent(event);
    }



    void StateItem::dropEvent(QGraphicsSceneDragDropEvent* event)
    {
        //    ARMARX_INFO << "drop event " << state->getInstanceName();
        if (event->proposedAction() == Qt::IgnoreAction)
        {
            return;
        }

        const AbstractStateMimeData* data =  qobject_cast<const AbstractStateMimeData*>(event->mimeData());
        bool accept = true;
        if (data && state && state->getStateClass() && data->getState()
            && (state->getType() == eNormalState || state->getType() == eRemoteState))
        {
            int i = 2;
            const QString newStateNameBase = data->getState()->getStateName();
            QString newStateName = newStateNameBase;

            while (state->getStateClass()->getSubstates().find(newStateName) != state->getStateClass()->getSubstates().end())
            {
                newStateName = newStateNameBase + "_" + QString::number(i);
                i++;
            }

            if (hasAncestor(data->getState()) || data->getState()->hasDescendant(state->getStateClass()))
            {
                QMessageBox::warning(event->source(), "State drag'n'drop error", "State cycle detected - you must not insert a state which is also a parent state of the current state");
                accept = false;
            }
            else if (data->getState()->getType() == eDynamicRemoteState && !data->isInSameGroup(state->getStateClass()))
            {
                QMessageBox::warning(event->source(), "State drag'n'drop error", "Dynamic Remote States can only be added in the same group.");
                accept = false;
            }
            else if (data->isInSameGroup(state->getStateClass()))
            {
                if (data->getState()->getType() == eDynamicRemoteState)
                {
                    state->getStateClass()->addDynamicRemoteSubstate(data->getState(), newStateName, event->pos());
                }
                else
                {
                    state->getStateClass()->addSubstate(data->getState(), newStateName, event->pos());
                }
            }
            else if (data->isPublic())
            {
                state->getStateClass()->addRemoteSubstate(data->getState(), data->getProxyName(), newStateName, event->pos());
            }
            else
            {
                QMessageBox::warning(event->source(), "State drag'n'drop error", "Only public states can be added as a Remote State.");
                accept = false;
            }


        }
        else
        {
            accept = false;
        }


        if (accept)
        {
            event->setDropAction(Qt::LinkAction);
            event->accept();
        }
        else
        {
            event->setDropAction(Qt::IgnoreAction);
            event->accept();
        }
    }

    bool StateItem::hasAncestor(statechartmodel::StatePtr sC) const
    {
        if (!sC)
        {
            return false;
        }

        StateItem* state = qgraphicsitem_cast<StateItem*>(parentItem());
        if (!state)
        {
            return false;
        }
        if (!state->getStateInstance() || !state->getStateInstance()->getStateClass())
        {
            return false;
        }

        if (state->getStateInstance()->getStateClass()->getUUID() == sC->getUUID())
        {
            return true;
        }

        return state->hasAncestor(sC);
    }

    bool StateItem::hasDescendant(statechartmodel::StatePtr sC) const
    {
        if (!sC || !state->getStateClass())
        {
            return false;
        }
        return state->getStateClass()->hasDescendant(sC);


    }
    LevelOfDetail StateItem::getLevelOfDetail(float levelOfDetail) const
    {
        if (maxShownSubstateLevel >= 0) // only show X levels of substates
        {
            int level = getInstanceLevel();
            if (level > maxShownSubstateLevel)
            {
                return eHidden;
            }
            else if (level >= maxShownSubstateLevel)
            {
                return eNoSubstates;
            }
        }
        return MorphingItem::getLevelOfDetail(levelOfDetail);
    }
}
