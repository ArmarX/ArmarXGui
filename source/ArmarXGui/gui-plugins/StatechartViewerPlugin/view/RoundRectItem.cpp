/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "RoundRectItem.h"
#include "MorphingItem.h"
#include <QtGui>
#include <QGraphicsView>
#include <QGraphicsSceneMouseEvent>
#include <QApplication>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXWidgetController.h>
#define RESIZE_RECT 30
#define ROUNDEDGESIZE 50


RoundRectItem::RoundRectItem(const QRectF& bounds, const QColor& color,
                             QGraphicsItem* parent)
    : QGraphicsObject(parent),
      rimPen(Qt::black, 1),
      bounds(bounds)
{
    setCursor = false;
    scalingActive = false;
    editable = true;
    resizingMode = eNone;
    setCacheMode(DeviceCoordinateCache);
    setColor(color);
    setFlag(QGraphicsItem::ItemIsSelectable, true);
    setFlag(QGraphicsItem::ItemIsMovable, true);
    //    setFlag(QGraphicsItem::ItemUsesExtendedStyleOption, true);
    setFlag(QGraphicsItem::ItemSendsGeometryChanges, true);
    setAcceptHoverEvents(true);


    float curScale = scale();
    //    adjustScale(curScale);
    setScale(curScale);
    QPointF curPos = pos();
    adjustPosition(curPos);
    setPos(curPos);
}

void RoundRectItem::setColor(QColor newColor)
{
    color = newColor;
    fillGradient.setStart(bounds.topLeft());
    fillGradient.setFinalStop(bounds.bottomRight());
    fillGradient.setColorAt(0, newColor);
    fillGradient.setColorAt(1, newColor.darker(100));
    update();
}

void RoundRectItem::setRimPen(QPen newPen)
{
    rimPen = newPen;
}

QRectF RoundRectItem::boundingRect() const
{
    return bounds.adjusted(0, 0, 2, 2);
}

void RoundRectItem::setBounds(QRectF newBounds)
{
    if (bounds == newBounds)
    {
        return;
    }

    prepareGeometryChange();
    QRectF oldBounds = bounds;
    bounds = newBounds;
    itemResized(oldBounds, bounds);
}

void RoundRectItem::setSize(const QSizeF& newSize)
{
    if (newSize == bounds.size())
    {
        return;
    }

    QRectF oldBounds = bounds;
    prepareGeometryChange();
    bounds.setSize(newSize);
    setColor(color);
    itemResized(oldBounds, bounds);
}

void RoundRectItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option,
                          QWidget* widget)
{


    painter->setPen(Qt::NoPen);
    painter->setBrush(QColor(0, 0, 0, 64));
    painter->drawRoundedRect(bounds.translated(2, 2), ROUNDEDGESIZE, ROUNDEDGESIZE);

    QLinearGradient tempGradient = fillGradient;
    QPen tmpRimPen = rimPen;
    if (isSelected())
    {
        tempGradient.setColorAt(1, color.lighter(130));
        tmpRimPen.setStyle(Qt::DashLine);
        //        tmpRimPen.setWidthF(tmpRimPen.widthF() * 2);
    }

    painter->setBrush(tempGradient);
    painter->setPen(tmpRimPen);
    painter->drawRoundedRect(bounds, ROUNDEDGESIZE, ROUNDEDGESIZE);

    painter->setPen(QPen(Qt::black, 1));

    //    QGraphicsObject::paint(painter, option, widget);
}


void RoundRectItem::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
    RoundRectItem* parent = dynamic_cast<RoundRectItem*>(parentItem());
    if (event->modifiers().testFlag(Qt::ShiftModifier)
        && (!parent || parent->isEditable()))
    {
        setFlag(QGraphicsItem::ItemIsMovable, true);
    }
    else
    {
        setFlag(QGraphicsItem::ItemIsMovable, false);
    }

    if (isLevelOfDetailLow(event))
    {
        event->ignore();
        return;
    }

    //    std::cout << "item pos : " << pos().x() << ", " << pos().y() << std::endl;
    //    std::cout << "item scene pos : " << this->mapToScene(pos()).x() << ", " << this->mapToScene(pos()).y()<< std::endl;
    QPointF pos = event->pos();
    //    ARMARX_INFO_S << "pos: " << pos;
    QRectF oldRect = boundingRect();
    QRectF rect = boundingRect();
    //    rect = rect.size().scale(scale());
    //    ARMARX_INFO_S << "rect: " << rect.width() << ", " << rect.height();
    rect.setTopLeft(QPointF(rect.bottomRight().rx() - RESIZE_RECT, rect.bottomRight().ry() - RESIZE_RECT));

    //    ARMARX_INFO_S << "rect: " << rect.topLeft() << ", " << rect.bottomRight();
    if (rect.contains(pos))
    {
        scalingActive = true;
        prepareGeometryChange();
        //        bounds.setBottomRight(QPointF(pos.x()+2, pos.y()+2));
        itemResized(oldRect, bounds);
        //        update(/*bounds.adjusted(-20,-20,20,20)*/);
        //        if(parentItem())
        //            parentItem()->update(mapRectToParent(bounds.adjusted(-20,-20,20,20)));
    }
    else if (getBottomResizeBB().contains(pos))
    {
        resizingMode = eBottomResizing;
    }
    else if (getTopResizeBB().contains(pos))
    {
        resizingMode = eTopResizing;

    }
    else if (getLeftResizeBB().contains(pos))
    {
        resizingMode = eLeftResizing;

    }
    else if (getRightResizeBB().contains(pos))
    {
        resizingMode = eRightResizing;

    }
    else
    {
        QGraphicsObject::mousePressEvent(event);
    }
}

void RoundRectItem::mouseReleaseEvent(QGraphicsSceneMouseEvent* event)
{
    //    ARMARX_INFO_S << "pos: " << event->pos();
    if (setCursor)
    {
        QApplication::restoreOverrideCursor();
    }

    setCursor = false;
    scalingActive = false;
    resizingMode = eNone;
    QGraphicsItem::mouseReleaseEvent(event);
}

void RoundRectItem::mouseMoveEvent(QGraphicsSceneMouseEvent* event)
{
    QPointF pos = event->pos();

    if (scalingActive)
    {
        QRectF oldRect = boundingRect();
        prepareGeometryChange();
        QRectF newRect = oldRect;
        newRect.setBottomRight(QPointF(pos.x() + 2, pos.y() + 2));

        //        if(parentItem())
        //        {
        //            QRectF boundsP = mapRectFromParent(parentItem()->boundingRect());
        //            float xRatio = newRect.width()/boundsP.width();
        //            float yRatio = newRect.height()/boundsP.height();
        //            float maxRatio = std::max(xRatio,yRatio);
        ////            ARMARX_INFO << VAROUT(ratio) << " " << newRect.width() << ", " << newRect.height() << " --- " << boundsP.width() << ", " << boundsP.height();
        //            if(maxRatio > 0.4)
        //            {
        //                newRect.setWidth(newRect.width() * 0.4/maxRatio);
        //                newRect.setHeight(newRect.height() * 0.4/maxRatio);
        //            }
        //        }

        float xScale = newRect.width() / oldRect.width();
        float yScale = newRect.height() / oldRect.height();



        float resultScalefactor = std::max(xScale, yScale);
        resultScalefactor *= scale();
        resultScalefactor = std::max(resultScalefactor, 0.01f);
        //        ARMARX_INFO << resultScalefactor;

        setScale(resultScalefactor);
    }
    else if (resizingMode != eNone)
    {
        QRectF oldRect = boundingRect();
        prepareGeometryChange();
        QRectF newBounds = bounds;

        switch (resizingMode)
        {
            // TODO: Fix top and left state rezising
            //            case eTopResizing:
            //                bounds.setTop(pos.y()-2);
            //                break;
            case eBottomResizing:
                newBounds.setBottom(pos.y() + 2);
                break;

            //            case eLeftResizing:
            //                bounds.setLeft(pos.x()-2);
            //                break;
            case eRightResizing:
                newBounds.setRight(pos.x() + 2);
                break;

            default:
                break;
        }

        newBounds.setWidth(std::max(50.0, newBounds.width()));
        newBounds.setHeight(std::max(50.0, newBounds.height()));

        QRectF bbInP = mapRectToParent(newBounds);
        QRectF obbInP = mapRectToParent(oldRect);

        if (itemResizing(oldRect, newBounds))
        {
            bounds = newBounds;
            itemBoundingBoxChanged(std::max(obbInP.width(), obbInP.height()),
                                   std::max(bbInP.width(), bbInP.height()));
            itemResized(oldRect, newBounds);
        }
    }
    else
    {
        QGraphicsItem::mouseMoveEvent(event);
    }
}

void RoundRectItem::adjustCursor(Qt::CursorShape shape)
{
    if (!setCursor || (QApplication::overrideCursor() && QApplication::overrideCursor()->shape() != shape))
    {

        if (QApplication::overrideCursor() && QApplication::overrideCursor()->shape() != shape)
        {
            QApplication::changeOverrideCursor(QCursor(shape));
        }
        else
        {
            QApplication::setOverrideCursor(QCursor(shape));
        }

        setCursor = true;
    }
}

void RoundRectItem::hoverMoveEvent(QGraphicsSceneHoverEvent* event)
{
    QPointF pos = event->pos();
    QRectF rect = boundingRect();
    rect.setTopLeft(QPointF(rect.bottomRight().rx() - RESIZE_RECT, rect.bottomRight().ry() - RESIZE_RECT));

    if (isLevelOfDetailLow(event))
    {
        event->ignore();
        return;
    }

    if (rect.contains(pos))
    {
        adjustCursor(Qt::SizeFDiagCursor);
    }
    else if (getBottomResizeBB().contains(pos))
    {
        adjustCursor(Qt::SizeVerCursor);
    }
    //    else if(getTopResizeBB().contains(pos))
    //    {
    //        adjustCursor(Qt::SizeVerCursor);
    //    }
    //    else if(getLeftResizeBB().contains(pos))
    //    {
    //        adjustCursor(Qt::SizeHorCursor);
    //    }
    else if (getRightResizeBB().contains(pos))
    {
        adjustCursor(Qt::SizeHorCursor);
    }
    else if (QApplication::overrideCursor())
    {
        QApplication::restoreOverrideCursor();
        setCursor = false;
    }

}

void RoundRectItem::hoverEnterEvent(QGraphicsSceneHoverEvent* event)
{

}

void RoundRectItem::hoverLeaveEvent(QGraphicsSceneHoverEvent* event)
{

    if (setCursor)
    {
        QApplication::restoreOverrideCursor();
    }

    setCursor = false;
}

QRectF RoundRectItem::getBottomResizeBB() const
{
    QRectF result = boundingRect();
    result.setTop(result.bottom());
    return result.adjusted(0, -RESIZE_RECT, 0, 0);
}

QRectF RoundRectItem::getTopResizeBB() const
{
    QRectF result = boundingRect();
    result.setBottom(result.top());
    return result.adjusted(0, 0, 0, RESIZE_RECT);
}

QRectF RoundRectItem::getLeftResizeBB() const
{
    QRectF result = boundingRect();
    result.setRight(result.left());
    return result.adjusted(0, 0, RESIZE_RECT, 0);
}

QRectF RoundRectItem::getRightResizeBB() const
{
    QRectF result = boundingRect();
    result.setLeft(result.right());
    return result.adjusted(-RESIZE_RECT, 0, 0, 0);
}


void RoundRectItem::adjustScale(float& resultScalefactor)
{
    if (!parentItem())
    {
        return;
    }

    QRectF rectP = (parentItem()->boundingRect());
    QRectF rect = boundingRect();
    float areaP = rectP.width() * rectP.height();
    float area = rect.width() * rect.height() * resultScalefactor;
    ARMARX_INFO << VAROUT(rect) << "," << VAROUT(rectP);
    ARMARX_INFO << VAROUT(area) << ", " << VAROUT(areaP);

    if (area / areaP > 0.4)
    {
        resultScalefactor = std::max(rect.width(), rect.height()) / std::max(rectP.width(), rectP.height());
        ARMARX_IMPORTANT << "bigger scale prohibited";
    }
}

QPointF RoundRectItem::adjustPosition(QPointF& newPos)
{
    if (!parentItem())
    {
        return newPos;
    }

    QRectF rect = parentItem()->boundingRect();
    QPointF oldPos = boundingRect().topLeft(); //TODO: Bounding rect is wrong here

    newPos.setX(qMin(rect.right() - boundingRect().width()*scale() - rect.width() /** itemParent->scale()*/ * 0.03,
                     qMax(newPos.x(),
                          rect.left() + rect.width() /** itemParent->scale()*/ * 0.03)));
    newPos.setY(qMin(rect.bottom() - boundingRect().height()*scale() - rect.height()/* * itemParent->scale()*/ * 0.03,
                     qMax(newPos.y(),
                          rect.top() + rect.height() /* * itemParent->scale()*/ * 0.20)));

    return oldPos;
}

bool RoundRectItem::isLevelOfDetailLow(QGraphicsSceneEvent* event) const
{
    if (event->widget())
    {
        QGraphicsView* view = qobject_cast<QGraphicsView*>(event->widget()->parent());

        if (parentItem())
        {
            auto bbparent = view->mapFromScene(parentItem()->mapToScene(parentItem()->boundingRect())).boundingRect();
            //            auto bb = view->mapFromScene(mapToScene(parentItem()->boundingRect())).boundingRect();
            //            auto qlod = QStyleOptionGraphicsItem::levelOfDetailFromTransform((view->transform() * this->sceneTransform()));
            //            float lod = bb.width() / this->boundingRect().width();

            //            if(view)
            //                ARMARX_INFO << "View " << view->metaObject()->className() << VAROUT(lod) << VAROUT(qlod)<< VAROUT(bbparent.width()) << VAROUT(bb.width());
            //            else
            //                ARMARX_INFO << "NoView";
            if (bbparent.width() < armarx::MorphingItem::minSizeToShowSubstates)
            {
                //                ARMARX_INFO << "lod low -> no interaction";

                return true;
            }
        }
        else
        {
            //            ARMARX_INFO << "no Parent";
        }
    }

    return false;
}

bool RoundRectItem::isEditable() const
{
    return editable;
}

void RoundRectItem::setEditable(bool editable)
{
    this->editable = editable;
}

QVariant RoundRectItem::itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant& value)
{
    if (change == ItemPositionChange)
    {
        // value is the new position.
        QPointF newPos = value.toPointF();
        RoundRectItem* itemParent = qgraphicsitem_cast<RoundRectItem*>(parentItem());

        if (itemParent)
        {
            /* QPointF oldPos = */adjustPosition(newPos);

            //            ARMARX_INFO << "bounds " << boundingRect() << " newPos " << newPos;
            itemMoved(boundingRect().topLeft(), newPos/*+QPointF(boundingRect().width()/2, boundingRect().height()/2)*/);
            return newPos;
        }
    }
    else if (change == ItemScaleChange)
    {
        if (parentItem())
        {

            float resultScalefactor = value.toFloat();
            //            adjustScale(resultScalefactor);
            //            ARMARX_INFO << "new scale: " << resultScalefactor << " proposed scale: " << value.toFloat();
            return resultScalefactor;
        }
    }

    return QGraphicsItem::itemChange(change, value);
}




void RoundRectItem::mouseDoubleClickEvent(QGraphicsSceneMouseEvent* event)
{

}
