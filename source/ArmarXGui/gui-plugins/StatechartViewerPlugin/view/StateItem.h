/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "RoundRectItem.h"
#include "MorphingItem.h"
#include "../model/SignalType.h"

#include <ArmarXCore/core/logging/Logging.h>

#include <QGraphicsWidget>
#include <QPointer>

#include <memory>

namespace armarx
{
    class StateScene;
}

namespace armarx::statechartmodel
{
    class State;
    using StatePtr = std::shared_ptr<State>;
    class Transition;
    using TransitionPtr = std::shared_ptr<Transition>;
    using TransitionCPtr = std::shared_ptr<const Transition>;
    class StateInstance;
    using StateInstancePtr = std::shared_ptr<StateInstance>;
}

namespace armarx
{
    class StateItem;
    class TransitionItem;

    class StateItem :
        public RoundRectItem,
        public MorphingItem
    {
        Q_OBJECT
    public:



        explicit StateItem(statechartmodel::StateInstancePtr state, QGraphicsItem* parent = 0);
        ~StateItem() override;

        statechartmodel::StateInstancePtr getStateInstance() const
        {
            return state;
        }



        QString getInstanceName() const;

        bool hasAncestor(statechartmodel::StatePtr state) const;
        bool hasDescendant(statechartmodel::StatePtr state) const;
        int getMaxShownSubstateLevel() const;
        void setMaxShownSubstateLevel(int value);

        void clearActiveSubstate();
        StateItem* getParentStateItem() const;

        QString getFullStatePath() const;
        QVector<StateItem*> getSubstateItems() const;
        QVector<TransitionItem*> getTransitionItems() const;
    signals:
        void stateItemResized(QSizeF newSize);
        void stateItemMoved(QPointF newPosition);
        void stateItemBoundingBoxChanged(float newSquareBoundingBoxSize);
        void stateContextMenuRequested(statechartmodel::StateInstancePtr stateInstace, QPoint mouseScreenPos, QPointF mouseItemPos);
        void transitionContextMenuRequested(statechartmodel::TransitionCPtr transition, statechartmodel::StatePtr state, QPoint mouseScreenPos, QPointF mouseItemPos);
    public slots:
        void notifyStateChanged(statechartmodel::SignalType signalType);
        void notifySubstateChanged(statechartmodel::StateInstancePtr substate, statechartmodel::SignalType signalType);
        void notifyTransitionChanged(statechartmodel::TransitionCPtr transition, statechartmodel::SignalType signalType);
        void notifyTransitionsChanged(statechartmodel::StateInstancePtr transition, statechartmodel::SignalType signalType);

        void forwardTransitionContextMenuRequested(statechartmodel::TransitionCPtr transition, QPoint mouseScreenPos, QPointF mouseItemPos);
        void transitionMoved(statechartmodel::TransitionCPtr transition, QPointF dropPoint);
        void viewAll();
        void adjustSize();
        void setInstanceName(const QString& value);
        void setActiveState(bool active);
        void setActiveSubstate(const QString& substateName);


    protected:
        void setup(QGraphicsItem* parent);
        void connectSlots();
        void initialCreation();
        void setStateColor();
        void setRimColor();
        void updateToolTip();
        void removeChildren();
        int getInstanceLevel() const;
        QPointF adjustPosition(QPointF& newPos) override;

        //    Ui_StateWidget ui;
        //    QGraphicsProxyWidget* prx;
        QWidget* subWidget;
        StateScene* stateScene;
        statechartmodel::StateInstancePtr state;
        using StateInstanceItemMap = QMap<statechartmodel::StateInstancePtr, QPointer<StateItem>>;
        StateInstanceItemMap subStates;
        QString instanceName;
        using TransitionMap = QMap<statechartmodel::TransitionCPtr, QPointer<TransitionItem>>;
        TransitionMap transitions;
        int maxShownSubstateLevel = -1;
        bool active = false;


        // RoundRectItem interface
    protected:
        bool itemResizing(const QRectF& oldSize, QRectF& proposedSize) override;
        void itemResized(const QRectF& oldSize, const QRectF& newSize) override;
        void itemMoved(const QPointF& oldPos, const QPointF& newPos) override;
        void itemBoundingBoxChanged(float oldSize, float size) override;

        void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) override;

        // QGraphicsItem interface
    protected:
        void contextMenuEvent(QGraphicsSceneContextMenuEvent* event) override;

        // QGraphicsItem interface
    protected:
        QVariant itemChange(GraphicsItemChange change, const QVariant& value) override;

        // QGraphicsItem interface
    protected:
        void dragEnterEvent(QGraphicsSceneDragDropEvent* event) override;
        void dragMoveEvent(QGraphicsSceneDragDropEvent* event) override;
        void dropEvent(QGraphicsSceneDragDropEvent* event) override;

        // MorphingItem interface
    public:
        LevelOfDetail getLevelOfDetail(float levelOfDetail) const override;
    };

}


