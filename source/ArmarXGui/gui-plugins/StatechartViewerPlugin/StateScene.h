/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <QGraphicsScene>

#include <QGraphicsRectItem>
#include <QGraphicsLineItem>

#include <ArmarXCore/core/logging/Logging.h>

namespace armarx::statechartmodel
{
    class State;
    using StatePtr = std::shared_ptr<State>;
    class StateInstance;
    using StateInstancePtr = std::shared_ptr<StateInstance>;
    class Transition;
    using TransitionCPtr = std::shared_ptr<const Transition>;
}

namespace armarx
{
    class StateItem;

    struct StateInstanceData
    {
        bool active = false;
        QString fullStatePath;
        statechartmodel::StateInstancePtr stateInstance;
    };

    class StateScene :
        public QGraphicsScene,
        public Logging
    {
        Q_OBJECT
    public:
        explicit StateScene(QObject* parent = 0);
        statechartmodel::StateInstancePtr getStateInstance() const;
        StateItem* getTopLevelStateItem() const;



        QMap<QString, StateInstanceData> getStateInstanceData() const;

    signals:
        void stateContextMenuRequested(statechartmodel::StateInstancePtr state, QPoint mouseScreenPos, QPointF mouseItemPos);
        void transitionContextMenuRequested(statechartmodel::TransitionCPtr transition, statechartmodel::StatePtr state, QPoint mouseScreenPos,  QPointF mouseItemPos);
    public slots:
        void setToplevelState(statechartmodel::StatePtr toplevelStateInstance);
        void saveSceneToSVG(QString path = "/tmp/statechart.png", int width = 6000);
        void clearActiveSubstates();
    private:
        statechartmodel::StateInstancePtr toplevelStateInstance;
        StateItem* topLevelStateItem;
        //        QMap<QString, StateItem*> subStates;
        //        QList<QGraphicsLineItem*> transitions;

    };

}

