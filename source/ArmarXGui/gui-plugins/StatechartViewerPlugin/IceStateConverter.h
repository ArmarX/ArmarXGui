/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Clara Scherer
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <string>
#include <vector>

#include <ArmarXCore/interface/statechart/StatechartIce.h>

#include "model/State.h"
#include "model/Transition.h"
#include "StateWatcher.h"

namespace armarx
{
    class IceStateConverter
    {
    public:
        /**
         * @brief IceStateConverter Creates a converter whose model's top state is state.
         * @param state The top state of the internal model
         */
        IceStateConverter(statechartmodel::StatePtr state = statechartmodel::StatePtr(new statechartmodel::State()));
        ~IceStateConverter();

        /**
         * @brief Converts the given ice model into a statechartmodel. This is then accesible via
         *  getTopState().
         * @param iceBase The StateIceBase that is the top state of the ice model that shall be converted
         */
        void convert(StateIceBasePtr iceBase);

        /**
         * @brief getTopState Returns the top state of the internal model that was converted from an ice model.
         * @return The top state of the internal model.
         */
        statechartmodel::StateInstancePtr getTopState();

        void setStateWatcher(StateWatcherPtr watcher);

        std::map<std::string, std::pair<statechartmodel::StateInstancePtr, StateIceBasePtr> > getCompleteStateMap() const;

    private:
        /**
         * @brief topState The top state of the model.
         */
        statechartmodel::StateInstancePtr topState;

        /**
         * @brief Map with all states with their global state id, for quick access
         */
        std::map<std::string, std::pair<armarx::statechartmodel::StateInstancePtr, armarx::StateIceBasePtr>> completeStateMap;

        /**
         * @brief convert Converts iceBase into a statechartmodel state and saves the conversion result in modelState.
         * @param iceBase A state from the ice model
         * @param modelState The corresponding state from the statechartmodel
         */
        void convert(StateIceBasePtr iceBase, statechartmodel::StateInstancePtr modelState);

        /**
         * @brief updateState Updates the state in the model (incrementally if possible). Requires both states to be equal.
         * @param iceBase The state maintained by ice.
         * @param modelState The state contained by the model.
         */
        void updateState(StateIceBasePtr iceBase, statechartmodel::StateInstancePtr modelState);

        /**
         * @brief resetState Completely resets modelState to fit iceBase.
         * @param iceBase The state maintained by ice.
         * @param modelState The state contained by the model.
         */
        void resetState(StateIceBasePtr iceBase, statechartmodel::StateInstancePtr modelState);

        /**
         * @brief stateEqual Checks whether the states are equal. Two states are equal if they have
         *                   the same name or, if not, have the same transitions
         * @param iceBase The state maintained by ice.
         * @param modelState The state contained by the model.
         * @return Returns true if both states are equal.
         */
        bool stateEqual(StateIceBasePtr iceBase, statechartmodel::StateInstancePtr modelState);


        /**
         * @brief transitionEqual Tests whether two transitions are equal. Two transitions are considered
         *                        considered equal if the are triggered by the same event.
         * @param iceTransition The transition delivered by ice.
         * @param modelTransition The transition contained in the statechartmodel.
         * @return true if both transitions are equal.
         */
        bool transitionEqual(TransitionIceBase iceTransition, statechartmodel::TransitionPtr modelTransition);
        /**
         * @brief sameTransitions Checks whether the ice transitions are the same as the model transitions, even if they are in
         *                        a different order.
         * @param iceTransitions The vector of transitions belonging to the iceBaseState.
         * @param modelTransitions The list of transitions belonging to the state in the model.
         * @return
         */
        bool sameTransitions(StateIceBasePtr iceBase, statechartmodel::StatePtr modelState);



        /**
         * @brief updateTransitions (Incrementally) updates the model state's transitions to match those of the iceBase.
         */
        void updateTransitions(StateIceBasePtr iceBase, statechartmodel::StatePtr modelState);
        /**
         * @brief updateSubstates (Incrementally) updates modelState's substates to match those of iceBase.
         */
        void updateSubstates(StateIceBasePtr iceBase, statechartmodel::StatePtr modelState);
        /**
         * @brief updateStartState (Incrementally) updates modelState's start state to match the one of iceBase.
         */
        void updateStartState(StateIceBasePtr iceBase, statechartmodel::StatePtr modelState);
        /**
         * @brief updateActiveSubstate (Incrementally) updates the modelState's active substate to match the one of iceBase.
         */
        void updateActiveSubstate(StateIceBasePtr iceBase, statechartmodel::StatePtr modelState);
        /**
         * @brief updateStateName (Incrementally) updates modelState's state name to match the one of iceBase.
         */
        void updateStateAttributes(StateIceBasePtr iceBase, statechartmodel::StateInstancePtr modelState);

        void updateTransitionFromAll(TransitionIceBase newTrans, statechartmodel::StatePtr modelState);
        void updateSingleTransition(TransitionIceBase newTrans, statechartmodel::StatePtr modelState);
        //evtl. (bei allen/manchen) update Funktionen bool zurückgeben um anzuzeigen,
        //ob das einfach so geändert werden kann oder alles neu übergeben werden muss?

        /**
         * @brief resetTransitions Resets modelState's transitions to match those of iceBase.
         */
        void resetTransitions(StateIceBasePtr iceBase, statechartmodel::StatePtr modelState);
        /**
         * @brief resetSubstates Resets modelState's substates to match those of iceBase.
         */
        void resetSubstates(StateIceBasePtr iceBase, statechartmodel::StatePtr modelState);
        /**
         * @brief resetParameters Resets modelState's parameters to match those of iceBase.
         */
        void resetParameters(StateIceBasePtr iceBase, statechartmodel::StatePtr modelState);
        /**
         * @brief resetStartState Resets modelState's start state to match the one of iceBase.
         */
        void resetStartState(StateIceBasePtr iceBase, statechartmodel::StatePtr modelState);
        /**
         * @brief resetActiveSubstate Resets modelState's active substate to match the one of iceBase.
         */
        void resetActiveSubstate(StateIceBasePtr iceBase, statechartmodel::StatePtr modelState);
        /**
         * @brief resetStateName Resets modelState's state name to match the one of iceBase.
         */
        void resetStateAttributes(StateIceBasePtr iceBase, statechartmodel::StateInstancePtr modelState);


        statechartmodel::StateParameterMap convertToModelParameterMap(armarx::StateParameterMap iceMap);
        using stringVector = std::vector<std::string>;
        /**
         * @brief sortTransitionNames Sorts the names of ice and statechartmodel transitions alphabetically.
         * @param iceTransitions A List of ice transitions
         * @param modelTransitions A List of statechartmodel transitions
         * @return A pair of string vectors.
         * The first Component are the names of the ice transitions sorted alphabetically.
         * The second component are the names of the statechartmodel transitions sorted alphabetically.
         */
        std::pair<stringVector, stringVector> sortTransitionNames(TransitionTable iceTransitions, statechartmodel::CTransitionList modelTransitions);
        /**
         * @brief compareIceStates Operator '<' on the states' names.
         * @param l A statechartmodel state
         * @param r Another statechartmodel state
         * @return l->stateName < r->stateName
         */
        static bool compareIceStates(armarx::AbstractStateIceBasePtr l, armarx::AbstractStateIceBasePtr r);

        /**
         * @brief findSubstateByName Finds the substate of state with name 'name'.
         * @param state The parent of the sought substate
         * @param name The name of the sought substate
         * @return The InstantancePtr to the corresponding substate.
         */
        statechartmodel::StateInstancePtr findSubstateByName(statechartmodel::StatePtr state, QString name);
    private:
        StateWatcherPtr watcher;
    };

    using IceStateConverterPtr = std::shared_ptr<IceStateConverter>;

}
