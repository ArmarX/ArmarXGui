/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "StateMimeData.h"

namespace armarx
{


    AbstractStateMimeData::AbstractStateMimeData(statechartmodel::StatePtr state) :
        state(state)
    {

    }

    bool AbstractStateMimeData::hasFormat(const QString& mimetype) const
    {
        if (mimetype == formats().first())
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    QStringList AbstractStateMimeData::formats() const
    {
        return QStringList() << "application/x-State";
    }

    QVariant AbstractStateMimeData::retrieveData(const QString& mimetype, QVariant::Type preferredType) const
    {
        ARMARX_INFO_S << "retrieveData: " << mimetype.toStdString();

        if (mimetype == "application/x-State")
        {
            ARMARX_INFO_S << "Generating state variant";
            QVariant v;
            v.setValue(state);
            statechartmodel::StatePtr s = v.value<statechartmodel::StatePtr>();
            return v;
        }
        else
        {
            return QMimeData::retrieveData(mimetype, preferredType);
        }
    }
    const QString& AbstractStateMimeData::getProxyName() const
    {
        return proxyName;
    }

    void AbstractStateMimeData::setProxyName(const QString& value)
    {
        proxyName = value;
    }

    statechartmodel::StatePtr AbstractStateMimeData::getState() const
    {
        return state;
    }

    void AbstractStateMimeData::setState(const statechartmodel::StatePtr& value)
    {
        state = value;
    }




}




