/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <memory>

#include <QString>
#include <QPointF>
#include <QList>
#include <QMetaType>


#include "stateinstance/StateInstance.h"
#include "ParameterMapping.h"

namespace armarx
{

    using QPointList = QList<QPointF>;
    using FloatPtr = std::shared_ptr<float>;
    using QPointPtr = std::shared_ptr<QPointF>;

    struct SupportPoints
    {
        SupportPoints()
            : startPoint(),
              endPoint(),
              controlPoints()
        {}

        SupportPoints(QPointList list)
            : startPoint(),
              endPoint(),
              controlPoints(list)
        {}

        QPointPtr startPoint;
        QPointPtr endPoint;
        QPointList controlPoints;

        QList<QPointF> toPointList() const;

        QList<QPointF> controlPointList() const;

        void setControlPoints(QList<QPointF> list);

        void append(const QPointF& point);
    };
}

namespace armarx::statechartmodel
{
    class Transition
    {
    public slots:
        void detachFromDestination();
        ParameterMappingPtr findMapping(const QString& destinationKey, const ParameterMappingList& mapping) const;
    public:
        Transition();
        QString eventName;
        StateInstancePtr sourceState;
        StateInstancePtr destinationState;
        ParameterMappingList mappingToNextStatesInput;
        ParameterMappingList mappingToParentStatesLocal;
        ParameterMappingList mappingToParentStatesOutput;
        SupportPoints supportPoints;
        QPointPtr labelCenterPosition;
        FloatPtr labelFontPointSize;
        IceUtil::Time lastActivationTimestamp;
        bool transitionUserCode = false;

    };
    using TransitionPtr = std::shared_ptr<Transition>;
    using TransitionCPtr = std::shared_ptr<const Transition>;
}

Q_DECLARE_METATYPE(armarx::statechartmodel::TransitionPtr)
