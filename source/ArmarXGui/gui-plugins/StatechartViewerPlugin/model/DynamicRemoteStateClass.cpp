/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "DynamicRemoteStateClass.h"

namespace armarx::statechartmodel
{
    DynamicRemoteStateClass::DynamicRemoteStateClass(QString uuid) : State(uuid)
    {
        setStateName("DynamicRemoteState");

        StateParameterMap input;
        StateParameterPtr proxyName(new StateParameter());
        proxyName->optional = false;
        proxyName->type = QString::fromStdString(Variant::typeToString(VariantType::String));
        input["proxyName"] = proxyName;
        StateParameterPtr stateName(new StateParameter());
        stateName->optional = false;
        stateName->type = QString::fromStdString(Variant::typeToString(VariantType::String));
        input["stateName"] = stateName;

        setInputParameters(input);

        EventList events;
        statechartmodel::EventPtr event(new statechartmodel::Event());
        event->name = "LoadingFailed";
        event->description = "The connecting to the desired remote state failed";
        events.push_back(event);
        this->outgoingTransitions = events;
    }

    DynamicRemoteStateClass::~DynamicRemoteStateClass()
    {

    }

    eStateType DynamicRemoteStateClass::getType() const
    {
        return eDynamicRemoteState;
    }
}
