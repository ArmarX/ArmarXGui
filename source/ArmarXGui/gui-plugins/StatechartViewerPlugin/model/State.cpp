/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "DynamicRemoteStateClass.h"
#include "State.h"
#include "stateinstance/RemoteState.h"
#include "stateinstance/LocalState.h"
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXWidgetController.h>
#include "stateinstance/DynamicRemoteState.h"

#include <IceUtil/UUID.h>

#include <cmath>

namespace armarx::statechartmodel
{
    State::State(const QString& uuid) :
        QObject(),
        margin(30, 140, 30, 30),
        size(StateInstance::StateDefaultSize),
        dirty(false),
        editable(true),
        active(false)
    {
        UUID = uuid.isEmpty() ? QString::fromStdString(IceUtil::generateUUID()) : uuid;
        transitions.append(TransitionPtr(new Transition()));
        connect(this, SIGNAL(stateChanged(statechartmodel::SignalType)), this, SLOT(setDirty(statechartmodel::SignalType)));
        connect(this, SIGNAL(substateChanged(statechartmodel::StateInstancePtr, statechartmodel::SignalType)), this, SLOT(setDirty(statechartmodel::StateInstancePtr, statechartmodel::SignalType)));
        connect(this, SIGNAL(transitionChanged(statechartmodel::TransitionCPtr, statechartmodel::SignalType)), this, SLOT(setDirty(statechartmodel::TransitionCPtr, statechartmodel::SignalType)));
    }
    State::~State()
    {
        emit stateDeleted();
    }

    void State::addReferences(const QMap<QString, StatePtr>& uuidStateMap)
    {
        StateInstanceMap substates = getSubstates();

        for (StateInstanceMap::const_iterator i = substates.begin(); i != substates.end(); ++i)
        {
            std::shared_ptr<RegularState> regularState = std::dynamic_pointer_cast<RegularState>(*i);  // ugly

            if (regularState && !regularState->getStateClass())
            {
                const QString& substateUUID = regularState->getClassUUID();

                if (uuidStateMap.contains(substateUUID))
                {
                    regularState->setStateClass(uuidStateMap[substateUUID]);
                }
            }
        }

        connectToSubclasses();
        // TODO: Now we can add all detached transitions to state instances
    }

    const StateParameterMap State::getInputAndLocalParameters() const
    {
        StateParameterMap result = inputParameters;

        for (auto p : localParameters.toStdMap())
        {
            result.insert(p.first, p.second);
        }

        return result;
    }

    CTransitionList State::getTransitions(bool withStartTransition) const
    {
        CTransitionList result;
        foreach (TransitionPtr t, transitions)
        {
            if (withStartTransition || t->sourceState)
            {
                result.append(t);
            }
        }
        return result;
    }

    void State::setStateName(const QString& newName)
    {
        if (stateName == newName)
        {
            emit stateChanged(eUnchanged);
        }
        else
        {
            stateName = newName;
            emit stateChanged(eChanged);
        }
    }

    StateInstancePtr State::getStartState() const
    {
        TransitionCPtr t = getStartTransition();
        return t ? t->destinationState : StateInstancePtr();
    }

    ParameterMappingList State::getStartStateInputMapping() const
    {
        TransitionCPtr t = getStartTransition();
        return t ? t->mappingToNextStatesInput : ParameterMappingList();
    }

    EventList State::getAllEvents() const
    {
        auto events = getOutgoingEvents();
        for (const StateInstancePtr& substate : getSubstates())
        {
            if (substate->getType() == eFinalState)
            {
                bool found = false;
                for (EventPtr& event : events)
                {
                    if (event->name == substate->getInstanceName())
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    EventPtr evt(new Event());
                    evt->name = substate->getInstanceName();
                    events.push_back(evt);
                }
            }
        }
        return events;
    }

    TransitionPtr State::getStartTransition()
    {
        foreach (TransitionPtr t, transitions)
        {
            if (!t->sourceState)
            {
                return t;
            }
        }

        return TransitionPtr();
    }

    TransitionCPtr State::getStartTransition() const
    {
        foreach (TransitionPtr t, transitions)
        {
            if (!t->sourceState)
            {
                return t;
            }
        }

        return TransitionPtr();
    }



    void State::connectToSubclasses()
    {
        foreach (StateInstancePtr instance, substates)
        {
            if (instance->getStateClass())
            {
                connect(instance->getStateClass().get(), SIGNAL(outgoingTransitionChanged(QString, statechartmodel::StatePtr, statechartmodel::SignalType)), this, SLOT(updateTransition(QString, statechartmodel::StatePtr, statechartmodel::SignalType)), Qt::UniqueConnection);
            }
        }

    }

    bool State::isEditable() const
    {
        return editable;
    }

    void State::setDescription(const QString& newDescription)
    {
        if (description == newDescription)
        {
            emit stateChanged(eUnchanged);
        }
        else
        {
            description = newDescription;
            emit stateChanged(eChanged);
        }
    }

    void State::setSize(const QSizeF& newSize)
    {

        //    ARMARX_INFO_S << "new size of " << stateName << ": " << newSize.width() << ", " << newSize.height() ;
        if (size == newSize)
        {
            emit stateChanged(eUnchanged);
        }
        else
        {
            size = newSize;
            emit stateChanged(eChanged);
            emit resized();
        }

    }

    void State::setSubstateAreaSize(const QSizeF& newSize)
    {
        QSizeF adjustedNewSize(newSize.width() + margin.left() + margin.width(),
                               newSize.height() + margin.top() + margin.height());
        setSize(adjustedNewSize);
    }

    void State::setEditable(bool editable)
    {
        this->editable = editable;
    }

    StateInstancePtr State::addSubstate(StatePtr newSubstate, QString instanceName, const QPointF& pos)
    {
        ARMARX_CHECK_EXPRESSION(newSubstate);

        if (!checkSubstate(newSubstate))
        {
            return StateInstancePtr();
        }

        if (instanceName.length() == 0)
        {
            instanceName = newSubstate->getStateName();
        }

        if (substates.find(instanceName) != substates.end())
        {
            return StateInstancePtr();
        }

        StateInstancePtr instance(new LocalState(newSubstate, instanceName, shared_from_this()));

        instance = addSubstate(instance);

        if (instance)
        {
            if (pos.x() != 0 && pos.y() != 0)
            {
                instance->setPosition(pos);
            }

            connect(newSubstate.get(), SIGNAL(outgoingTransitionChanged(QString, statechartmodel::StatePtr, statechartmodel::SignalType)), this, SLOT(updateTransition(QString, statechartmodel::StatePtr, statechartmodel::SignalType)), Qt::UniqueConnection);
            addDetachedTransitions(instance);
        }

        return instance;
    }

    StateInstancePtr State::addRemoteSubstate(StatePtr newRemoteSubstate, const QString& remoteStateOffererName, QString instanceName, const QPointF& pos)
    {
        ARMARX_CHECK_EXPRESSION(newRemoteSubstate);

        if (!checkSubstate(newRemoteSubstate))
        {
            return StateInstancePtr();
        }

        if (instanceName.length() == 0)
        {
            instanceName = newRemoteSubstate->getStateName();
        }

        StateInstancePtr instance(new RemoteState(newRemoteSubstate, instanceName, remoteStateOffererName, shared_from_this()));



        instance = addSubstate(instance);

        if (instance)
        {
            if (pos.x() != 0 && pos.y() != 0)
            {
                instance->setPosition(pos);
            }

            connect(newRemoteSubstate.get(), SIGNAL(outgoingTransitionChanged(QString, statechartmodel::StatePtr, statechartmodel::SignalType)), this, SLOT(updateTransition(QString, statechartmodel::StatePtr, statechartmodel::SignalType)), Qt::UniqueConnection);
            addDetachedTransitions(instance);
        }

        return instance;
    }

    StateInstancePtr State::addDynamicRemoteSubstate(StatePtr state, QString instanceName, const QPointF& pos)

    {

        if (!checkSubstate(state))
        {
            return StateInstancePtr();
        }

        if (instanceName.length() == 0)
        {
            instanceName = state->getStateName();
        }

        if (substates.find(instanceName) != substates.end())
        {
            return StateInstancePtr();
        }

        StateInstancePtr newstateInstance(new DynamicRemoteState(state, instanceName, shared_from_this()));

        newstateInstance = addSubstate(newstateInstance);

        if (newstateInstance)
        {

            if (pos.x() != 0 && pos.y() != 0)
            {
                newstateInstance->setPosition(pos);
            }

            connect(newstateInstance->getStateClass().get(), SIGNAL(outgoingTransitionChanged(QString, StatePtr, SignalType)), this, SLOT(updateTransition(QString, StatePtr, SignalType)), Qt::UniqueConnection);

            addDetachedTransitions(newstateInstance);
        }

        return newstateInstance;

    }

    StateInstancePtr State::addEndSubstate(const QString& endstateName, const QString& eventName, const QPointF& pos)
    {
        StateInstancePtr instance(new EndState(endstateName, eventName, shared_from_this()));
        instance = addSubstate(instance);

        if (instance)
        {
            if (pos.x() != 0 && pos.y() != 0)
            {
                instance->setPosition(pos);
            }

            emit outgoingTransitionChanged(eventName, shared_from_this(), eAdded);
        }

        return instance;
    }

    StateInstancePtr State::addSubstate(StateInstancePtr stateInstance)
    {
        if (substates.find(stateInstance->getInstanceName()) != substates.end())
        {
            return StateInstancePtr();
        }
        if (stateInstance->getStateClass() && !checkSubstate(stateInstance->getStateClass()))
        {
            return StateInstancePtr();
        }

        substates.insert(stateInstance->getInstanceName(), stateInstance);



        emit substateChanged(stateInstance, eAdded);

        if (substates.size() == 1) //needs to go after statechanged-> otherwise crash
        {
            setStartState(stateInstance);
        }

        return stateInstance;
    }


    void State::setStartState(StateInstancePtr newStartState)
    {
        TransitionPtr t = getStartTransition();

        if (!t)
        {
            ARMARX_WARNING << "No start transition found!";
            return;
        }

        if (t->destinationState == newStartState)
        {
            emit transitionChanged(getStartTransition(), eUnchanged);
        }
        else
        {
            t->destinationState = newStartState;
            t->supportPoints.controlPoints.clear();
            t->supportPoints.endPoint.reset();
            t->supportPoints.startPoint.reset();
            emit transitionChanged(getStartTransition(), eChanged);
        }
    }

    void State::setStartStateInputMapping(const ParameterMappingList& newStartStateInputMapping)
    {
        TransitionPtr t = getStartTransition();

        if (!t)
        {
            ARMARX_WARNING << "No start transition found!";
            return;
        }

        if (t->mappingToNextStatesInput == newStartStateInputMapping)
        {
            emit stateChanged(eUnchanged);
        }
        else
        {
            t->mappingToNextStatesInput = newStartStateInputMapping;
            emit stateChanged(eChanged);
        }
    }

    bool State::renameSubstate(QString oldName, QString newName)
    {
        if (substates.find(newName) != substates.end())
        {
            return false;
        }

        StateInstanceMap::iterator it = substates.find(oldName);

        if (it != substates.end())
        {
            EndStatePtr endState = std::dynamic_pointer_cast<EndState>(it.value());

            if (endState)
            {
                emit outgoingTransitionChanged(endState->getEventName(), shared_from_this(), eRemoved);
            }

            it.value()->setInstanceName(newName);
            StateInstanceMap::iterator newIt = substates.insert(newName, it.value());
            substates.remove(oldName);

            if (endState)
            {
                emit outgoingTransitionChanged(endState->getEventName(), shared_from_this(), eAdded);
            }

            emit substateChanged(newIt.value(), eChanged);
        }

        return true;
    }

    bool State::removeSubstate(StateInstancePtr substate)
    {
        if (!substate)
        {
            return false;
        }

        return removeSubstate(substate->getInstanceName());
    }

    bool containsOutgoingEvent(const QString& eventName, const EventList& events)
    {
        for (const EventPtr& evt : events)
        {
            if (evt->name == eventName)
            {
                return true;
            }
        }
        return false;
    }

    bool State::removeSubstate(QString stateInstanceName)
    {
        StateInstanceMap::iterator it = substates.find(stateInstanceName);

        if (it != substates.end())
        {
            foreach (TransitionPtr t, transitions)
            {
                if (t->sourceState
                    && t->sourceState == it.value() // reflexive edges should not be detached but removed
                    && t->destinationState == it.value()
                   )
                {
                    removeTransition(t);
                }
                else if (t->destinationState == it.value() && t->sourceState)
                {
                    detachTransitionDestination(t, calcDetachedTransitionLastControlPoint(t->sourceState));
                }
                else if (t->destinationState == it.value())
                {
                    setStartState(StateInstancePtr());
                }
                else if (t->sourceState == it.value())
                {
                    removeTransition(t);
                }

            }
            EndStatePtr endState = std::dynamic_pointer_cast<EndState>(it.value());

            if (endState)
            {
                emit outgoingTransitionChanged(endState->getEventName(), shared_from_this(), eRemoved);
            }

            substates.erase(it);
            emit substateChanged(it.value(), eRemoved);
            return true;
        }
        else
        {
            return false;
        }

    }

    StateInstancePtr State::replaceSubstate(QString stateInstanceName, StateInstancePtr newInstance)
    {
        StateInstanceMap::iterator it = substates.find(stateInstanceName);

        if (it != substates.end())
        {
            // TODO: This should also try to keep all transitions
            if (newInstance->getStateClass() && !checkSubstate(newInstance->getStateClass()))
            {
                return StateInstancePtr();
            }
            removeSubstate(stateInstanceName);
            newInstance->setPosition(it.value()->getTopLeft());
            return addSubstate(newInstance);
        }

        return StateInstancePtr();
    }

    void State::replaceSubstates(StateInstanceMap newSubstateList)
    {
        while (!substates.empty())
        {
            removeSubstate(substates.begin().key());
        }

        substates = newSubstateList;

        foreach (StateInstancePtr instance, substates)
        {
            if (instance->getStateClass())
            {
                connect(instance->getStateClass().get(), SIGNAL(outgoingTransitionChanged(QString, statechartmodel::StatePtr, statechartmodel::SignalType)), this, SLOT(updateTransition(QString, statechartmodel::StatePtr, statechartmodel::SignalType)), Qt::UniqueConnection);
            }

            emit substateChanged(instance, eAdded);
            addDetachedTransitions(instance);
        }
    }

    void State::setActiveSubstate(StateInstancePtr newActiveState)
    {
        if (activeSubstate == newActiveState)
        {
            emit stateChanged(eUnchanged);
        }
        else
        {
            //        ARMARX_INFO << "Setting new substate of state " << getStateName();
            auto tempState = activeSubstate;
            activeSubstate = newActiveState;
            if (tempState)
            {
                emit substateChanged(tempState, eChanged);
            }
            emit stateChanged(eChanged);
            if (newActiveState)
            {
                emit substateChanged(newActiveState, eChanged);
            }
        }
    }

    void State::clearActiveSubstates()
    {
        if (activeSubstate && activeSubstate->getStateClass())
        {
            activeSubstate->getStateClass()->clearActiveSubstates();
        }
        auto tempState = activeSubstate;
        activeSubstate.reset();
        if (tempState)
        {
            //        tempState->setActive(false);
            emit substateChanged(tempState, eChanged);
        }
        emit stateChanged(eChanged);

    }


    void State::setInputParameters(const StateParameterMap& newInputParameters)
    {
        // TODO: This should allow for a more fine-granular set operation and emit the signal more intelligently
        auto keys = newInputParameters.keys();

        for (const auto& key : keys)
        {
            if (localParameters.count(key) > 0)
            {
                throw LocalException("Key ") << key << " is already used in localParameters in state " << stateName;
            }
        }

        inputParameters = newInputParameters;
        emit stateChanged(eChanged);
    }

    void State::setLocalParameters(const StateParameterMap& newLocalParameters)
    {
        // TODO: This should allow for a more fine-granular set operation and emit the signal more intelligently
        auto keys = newLocalParameters.keys();

        for (const auto& key : keys)
        {
            if (inputParameters.count(key) > 0)
            {
                throw LocalException("Key ") << key << " is already used in inputParameters in state " << stateName;
            }
        }

        localParameters = newLocalParameters;
        emit stateChanged(eChanged);
    }

    void State::setOutputParameters(const StateParameterMap& newOutputParameters)
    {
        // TODO: This should allow for a more fine-granular set operation and emit the signal more intelligently
        outputParameters = newOutputParameters;
        emit stateChanged(eChanged);
    }


    void State::addTransition(TransitionPtr newTransition)
    {
        if (!checkTransition(newTransition))
        {
            return;
        }

        transitions.push_back(newTransition);
        emit transitionChanged(newTransition, eAdded);
    }

    void State::setTransitionMapping(TransitionCPtr transition, const ParameterMappingList& mappingToNextStateInput
                                     , const ParameterMappingList& mappingToParentLocal
                                     , const ParameterMappingList& mappingToParentOutput
                                    )
    {
        TransitionPtr t = findTransition(transition);

        if (t)
        {
            t->mappingToNextStatesInput = mappingToNextStateInput;
            t->mappingToParentStatesLocal = mappingToParentLocal;
            t->mappingToParentStatesOutput = mappingToParentOutput;
            emit transitionChanged(t, eChanged);
        }
        else
        {
            throw LocalException("Could not find transition in state");
        }
    }

    void State::setTransitionSupportPoints(TransitionCPtr transition, const SupportPoints& points, const QPointPtr& labelCenterPosition, const FloatPtr& labelFontPointSize)
    {
        TransitionPtr t = findTransition(transition);

        if (t)
        {
            auto oldPoints = t->supportPoints.toPointList();
            auto newPoints = points.toPointList();
            bool changed = newPoints.size() != oldPoints.size();

            if (!changed)
            {
                int size = oldPoints.size();

                for (int i = 0; i < size; ++i)
                {
                    if ((oldPoints.at(i) - newPoints.at(i)).manhattanLength() > 2)
                    {
                        changed = true;
                        break;
                    }
                }

            }
            t->labelCenterPosition = labelCenterPosition;
            t->labelFontPointSize = labelFontPointSize;
            t->supportPoints = points;
            emit transitionChanged(t, changed ? eChanged : eUnchanged);
        }
        else
        {
            throw LocalException("Could not find transition in state");
        }
    }

    void State::setTransitionActivated(TransitionCPtr transition)
    {
        emit transitionChanged(transition, eActivated);
    }

    void State::setTransitionUserCodeEnabled(TransitionCPtr transition, bool enabled)
    {
        TransitionPtr t = findTransition(transition);

        if (t)
        {
            t->transitionUserCode = enabled;
            emit transitionChanged(t, eChanged);
        }
        else
        {
            throw LocalException("Could not find transition in state");
        }
    }

    void State::addDetachedTransition(const QString& eventName, StateInstancePtr sourceState)
    {
        if (!sourceState)
        {
            return;
        }

        TransitionPtr t(new Transition());
        t->sourceState = sourceState;
        t->eventName = eventName;

        QPointF lastControlPoint = calcDetachedTransitionLastControlPoint(sourceState);
        t->supportPoints.controlPoints.append(lastControlPoint);
        t->supportPoints.endPoint = std::make_shared<QPointF>(calcDetachedTransitionEndPoint(sourceState, lastControlPoint));
        addTransition(t);
    }

    void State::updateTransitionDestination(TransitionCPtr transition, StateInstancePtr newDest, QPointList newSupportPoints)
    {
        foreach (TransitionPtr t, transitions)
        {
            if (t.get() == transition.get())
            {
                if (transition->destinationState == newDest)
                {
                    return;
                }
                //            if(!t->destinationState)
                t->supportPoints = SupportPoints(newSupportPoints);
                t->destinationState = newDest;

                if (t->destinationState == t->sourceState) // reflexive edge
                {
                    QPointF p = calcDetachedTransitionLastControlPoint(t->sourceState);
                    QPointList controlPoints;
                    controlPoints.push_back(p - QPointF(30, 30));
                    controlPoints.push_back(p);
                    controlPoints.push_back(p + QPointF(30, 30));

                    t->supportPoints.controlPoints = controlPoints;
                }
                else
                {
                    t->supportPoints.controlPoints.clear();
                    t->supportPoints.endPoint.reset();
                    t->supportPoints.startPoint.reset();
                    bendTransition(t, 50, (rand() % 20) - 10);
                }

                if (!t->destinationState->getStateClass())
                {
                    setTransitionUserCodeEnabled(t, false);
                }

                emit transitionChanged(t, eChanged);
            }
        }

    }

    void State::detachTransitionDestination(TransitionCPtr transition, QPointF floatingEndPoint)
    {
        //    if(!transition->destinationState)
        //        return;
        TransitionPtr t = findTransition(transition);

        if (t)
        {
            ARMARX_INFO << "Detaching transition " << t->eventName;
            t->supportPoints = SupportPoints(QPointList({floatingEndPoint}));
            if (t->sourceState)
            {
                t->supportPoints.endPoint = std::make_shared<QPointF>(calcDetachedTransitionEndPoint(t->sourceState, floatingEndPoint));
            }
            t->destinationState.reset();
            t->labelCenterPosition.reset();
            emit transitionChanged(t, eChanged);
        }
    }


    void State::detachTransitionDestination(TransitionCPtr transition)
    {
        if (transition->sourceState)
        {
            detachTransitionDestination(transition, calcDetachedTransitionLastControlPoint(transition->sourceState));
        }
    }

    void State::replaceTransitions(TransitionList newTransitionList)
    {
        TransitionPtr startT = getStartTransition();

        while (!transitions.empty())
        {
            removeTransition(transitions.last());
        }

        transitions = newTransitionList;
        transitions.push_front(startT ? startT : TransitionPtr(new Transition()));

        foreach (TransitionPtr t, transitions)
        {
            emit transitionChanged(t, eAdded);
        }
    }

    void State::addSupportPoint(TransitionCPtr transition, QPointF supportPoint)
    {
        ARMARX_ERROR_S << "NYI";
        //    auto t = findTransition(transition);
        //    float minDist = std::numeric_limits<float>::max();
        //    int index = -1;
        //    QList<QPointF> list = t->supportPoints;
        //    if(t->sourceState)
        //        list.push_front(t->sourceState->getBounds().center());
        //    if(t->destinationState)
        //        list.push_back(t->destinationState->getBounds().center());
        //    for(int i = 0; i < t->supportPoints.size(); i++)
        //    {
        //        const QPointF& curP = list.at(i);
        //        float d = (curP-supportPoint).manhattanLength();

        //        if(d < minDist)
        //        {
        //            minDist = d;
        //            index = i;
        //        }
        //        ARMARX_INFO_S << VAROUT(index) << " " << VAROUT(d);
        //    }
        //    float prevDistance = index> 0 ? (list.at(index-1)-supportPoint).manhattanLength() : std::numeric_limits<float>::max();
        //    float distanceToNext = index< list.size()-1 ? (list.at(index+1)-supportPoint).manhattanLength() : std::numeric_limits<float>::max();
        //    if(prevDistance < distanceToNext)
        //        list.insert(index, supportPoint);
        //    else
        //        list.insert(index+1, supportPoint);
        //    if(t->sourceState)
        //        list.pop_front();
        //    if(t->destinationState)
        //        list.pop_back();
        //    t->supportPoints = list;
        //    emit transitionChanged(t, eChanged);
    }

    void State::setOutgoingEvents(const EventList& outgoingEvents)
    {
        auto checkEventExistsInSubstates = [&, this](EventPtr & e)
        {
            for (const StateInstancePtr& substate : substates)
            {
                if (substate->getType() == eFinalState)
                {
                    EndStateCPtr endstate = std::dynamic_pointer_cast<const EndState>(substate);

                    if (endstate && endstate->getEventName() == e->name)
                    {
                        return true;
                    }
                }
            }

            return false;
        };

        if (this->outgoingTransitions == outgoingEvents)
        {
            emit stateChanged(eUnchanged);
        }
        else
        {
            auto oldTransitions = outgoingTransitions;
            this->outgoingTransitions = outgoingEvents;
            // notify for new events
            foreach (EventPtr ev, outgoingEvents)
            {
                bool found = false;
                foreach (EventPtr evOld, oldTransitions)
                {
                    if (evOld->name == ev->name)
                    {
                        found = true;
                    }
                }

                if (!found && !checkEventExistsInSubstates(ev))
                {
                    emit outgoingTransitionChanged(ev->name, shared_from_this(), eAdded);
                }
            }
            // notify for removed events
            foreach (EventPtr evOld, oldTransitions)
            {
                bool found = false;
                foreach (EventPtr ev, outgoingEvents)
                {
                    if (evOld->name == ev->name)
                    {
                        found = true;
                    }
                }

                if (!found && !checkEventExistsInSubstates(evOld))
                {
                    emit outgoingTransitionChanged(evOld->name, shared_from_this(), eRemoved);
                }
            }


            emit stateChanged(eChanged);
        }
    }

    void State::updateTransition(const QString& eventName, StatePtr stateClass, SignalType signalType)
    {
        ARMARX_INFO_S << getStateName() << " was updated by substate " << stateClass->getStateName();

        // TODO: Rename transitions is buggy
        switch (signalType)
        {
            case eAdded:
            {
                QList<StateInstancePtr>  instances = getInstances(stateClass);
                foreach (StateInstancePtr instance, instances)
                {
                    addDetachedTransition(eventName, instance);
                }
            }
            break;

            case eRemoved:
            {
                ARMARX_INFO_S << "Removing transitions for event " << eventName;
                QList<StateInstancePtr>  instances = getInstances(stateClass);
                foreach (StateInstancePtr instance, instances)
                {
                    TransitionPtr t = getTransition(eventName, instance);

                    if (t && !containsOutgoingEvent(eventName, stateClass->getOutgoingEvents()))
                    {
                        removeTransition(t);
                    }
                }
            }

            break;

            default:

                break;
        }

    }

    void State::setDirty(SignalType signalType)
    {
        if (signalType != eUnchanged)
        {
            setDirty(true);
        }
    }

    void State::setDirty(StateInstancePtr substate, SignalType signalType)
    {
        if (signalType != eUnchanged)
        {
            setDirty(true);
        }
    }

    void State::setDirty(TransitionCPtr transition, SignalType signalType)
    {
        if (signalType != eUnchanged)
        {
            setDirty(true);
        }
    }

    void State::setDirty(bool dirty)
    {
        if (this->dirty != dirty)
        {
            emit dirtyStatusChanged(dirty);
        }

        this->dirty = dirty;
    }

    QList<StateInstancePtr> State::getInstances(StatePtr stateClass) const
    {
        QList<StateInstancePtr> result;

        for (StateInstanceMap::const_iterator s = substates.begin(); s != substates.end(); s++)
        {
            if (s.value()->getStateClass() == stateClass)
            {
                result.push_back(s.value());
            }
        }

        return result;
    }

    TransitionPtr State::getTransition(const QString& eventName, StateInstancePtr sourceInstance) const
    {
        foreach (TransitionPtr t, transitions)
        {
            if (t->eventName == eventName && t->sourceState == sourceInstance)
            {
                return t;
            }
        }
        return TransitionPtr();
    }



    bool State::removeTransition(TransitionPtr transition)
    {
        int index = transitions.indexOf(transition);

        if (index != -1)
        {
            //        ARMARX_INFO_S << "Removing transitions for event " << transition->eventName
            //                      << " from " << (transition->sourceState ? transition->sourceState->getInstanceName() : "None")
            //                      << " to " << (transition->destinationState ? transition->destinationState->getInstanceName() : "None");
            emit transitionChanged(transition, eRemoved);
            transitions.erase(transitions.begin() + index);
            return true;
        }

        return false;
    }

    bool State::checkTransition(TransitionPtr transition) const
    {
        if (!transition)
        {
            ARMARX_WARNING << "Transition must not be NULL";
            return false;
        }

        TransitionPtr t = getTransition(transition->eventName, transition->sourceState);

        if (t)
        {
            ARMARX_WARNING << "Transition with event " << transition->eventName << " for state " << transition->sourceState->getInstanceName() << " already exists";
            return false;
        }

        if (!transition->destinationState && transition->supportPoints.controlPoints.size() == 0)
        {
            ARMARX_WARNING << "If no destination is set for a transition, it must have 1 support point";
            return false;
        }

        if (!transition->sourceState)
        {
            ARMARX_WARNING << "A Transition must have a source start";
            return false;
        }

        return true;
    }

    bool State::checkSubstate(StatePtr newState) const
    {
        ARMARX_INFO << "UUID parent: " << getUUID() << " new: " << newState->getUUID();

        if (getUUID() == newState->getUUID())
        {
            return false;
        }
        return true;
    }



    TransitionPtr State::findTransition(TransitionCPtr t) const
    {
        for (int i = 0 ; i < transitions.size(); i++)
        {
            if (t.get() == transitions.at(i).get())
            {
                return transitions.at(i);
            }
        }

        return TransitionPtr();
    }

    TransitionPtr State::findTransition(const QString& eventName, const QString& transitionSourceName, const QString& transitionDestinationName) const
    {
        for (const TransitionPtr& t : transitions)
        {
            if (t->eventName == eventName && t->sourceState->getInstanceName() == transitionSourceName && t->destinationState->getInstanceName() == transitionDestinationName)
            {
                return t;
            }
        }
        return TransitionPtr();
    }

    bool State::hasDescendant(StatePtr sC) const
    {
        if (!sC)
        {
            return false;
        }

        for (auto it = substates.begin(); it != substates.end(); it++)
        {
            StateInstancePtr state = (it.value());
            if (!state)
            {
                continue;
            }
            if (!state->getStateClass())
            {
                continue;
            }

            if (state->getStateClass()->getUUID() == sC->getUUID())
            {
                return true;
            }

            if (state->getStateClass()->hasDescendant(sC))
            {
                return true;
            }
        }
        return false;

    }

    QString State::StateTypeToString(eStateType type)
    {
        switch (type)
        {
            case eNormalState:
                return "Normal State";
                break;

            case eRemoteState:
                return "Remote State";
                break;

            case eDynamicRemoteState:
                return "Dynamic Remote State";
                break;

            case eFinalState:
                return "Final State";
                break;

            case eUndefined:
                return "";
                break;

            default:
                return "Unknown State Type";
        }
    }

    void State::addDetachedTransitions(StateInstancePtr instance)
    {
        if (!instance->getStateClass())
        {
            return;    // This not only happens for end states but also the class-reference is not existing during loading
        }

        QList<EventPtr> events = instance->getStateClass()->getAllEvents();

        float angleStep = 20;
        float length = instance->getClassSize().width() * instance->getScale() * 0.8f;
        QPointF center = instance->getBounds().center();
        int count = events.size();
        float startAngle = angleStep * (count - 1) / 2.0f;

        float angle = startAngle;

        foreach (EventPtr event, events)
        {
            TransitionPtr t(new Transition());
            t->sourceState = instance;
            t->eventName = event->name;
            t->supportPoints.append(center + QPointF(cos(angle / 180.0 * M_PI) * length, sin(angle / 180.0 * M_PI) * length));
            addTransition(t);
            angle -= angleStep;
        }
    }

    QPointF State::calcDetachedTransitionLastControlPoint(StateInstancePtr instance) const
    {
        QPointF p;
        float angle = ((double)(rand() % 360));
        float length = std::max(instance->getClassSize().width(), instance->getClassSize().height());
        length *= instance->getScale() * 0.8;
        //    ARMARX_INFO << VAROUT(angle) << " " << VAROUT(length);
        p = instance->getBounds().center();
        //    ARMARX_INFO << VAROUT(p);
        //        p.setX(instance->getBounds().center().x);
        p.setX(p.x() + cos(angle / 180.0 * M_PI) * length);
        p.setY(p.y() + sin(angle / 180.0 * M_PI) * length);
        return p;
    }

    QPointF State::calcDetachedTransitionEndPoint(StateInstancePtr instance, const QPointF& lastControlPoint) const
    {
        QPointF line = lastControlPoint - instance->getBounds().center();
        QPointF p = line * 0.1 + lastControlPoint;

        return p;
    }

    void State::bendTransition(TransitionCPtr transition, int u, int v)
    {
        TransitionPtr t = findTransition(transition);

        if (t && t->sourceState && t->destinationState)
        {
            QPointList supportPoints;
            QPointF p1 = t->sourceState->getBounds().center();
            QPointF p2 = t->destinationState->getBounds().center();
            //        t->supportPoints.startPoint = std::make_shared<QPointF>(p1);
            //        t->supportPoints.endPoint = std::make_shared<QPointF>(p2);
            QPointF dirU = p2 - p1;
            QPointF dirV = QPointF(dirU.y(), -dirU.x());
            supportPoints.push_back(p1 + dirU * u / 100 + dirV * v / 100);
            t->supportPoints.setControlPoints(supportPoints);
            emit transitionChanged(t, eChanged);
        }
    }
}
