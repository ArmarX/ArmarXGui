/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/interface/statechart/StatechartIce.h>

#include <Ice/Handle.h>

#include <QPair>
#include <QString>
#include <QMap>

#include <memory>

namespace armarx
{
    class Variant;
    using VariantPtr = IceInternal::Handle<Variant>;
}

namespace armarx::statechartmodel
{
    class StateParameter;
    using StateParameterPtr = std::shared_ptr<StateParameter>;
    using StateParameterMap = QMap<QString, StateParameterPtr>;
    using StateParameterProfileDefaultValueMap = QMap<QString, QPair<VariantContainerBasePtr, QString>>;

    class StateParameter
    {
    public:
        StateParameter();
        void setDefaultValue(const VariantContainerBasePtr& value);
        VariantContainerBasePtr getDefaultValue();
        void setDefaultValueJson(const QString& valueJson);
        QString getDefaultValueJson();
        static statechartmodel::StateParameterPtr FromIceStateParameter(armarx::StateParameterIceBasePtr param);
        static armarx::StateParameterIceBasePtr ToIceStateParameter(statechartmodel::StateParameterPtr param);
        static armarx::StateParameterMap ToIceStateParameterMap(statechartmodel::StateParameterMap params);

        QString type;
        //QString dataType;

        //default values are also part of profileDefaultValues under the name StatechartProfiles::GetRootName()
        //            VariantContainerBasePtr defaultValue;
        //            QString defaultValueJSON;  //! Used to store the default value in case the JSON string could not be deserialized
        bool optional;
        QString description;
        StateParameterProfileDefaultValueMap profileDefaultValues;  //! Second item in value pair: store the default value in case the JSON string could not be deserialized
    };

}
