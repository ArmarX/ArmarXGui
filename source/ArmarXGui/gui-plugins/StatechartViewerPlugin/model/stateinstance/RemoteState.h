/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#pragma once

#include "RegularState.h"

namespace armarx::statechartmodel
{
    class RemoteState :
        public RegularState
    {
    public:
        RemoteState(StatePtr stateClass, const QString& instanceName, const QString& proxyName, StatePtr parentState);
        RemoteState(const QString& stateUUID, const QString& instanceName, StatePtr parentState = StatePtr());
        QString proxyName;

        void accept(Visitor& visitor) const override;

        // StateInstance interface
    public:
        eStateType getType() const override;
    };

    using RemoteStatePtr = std::shared_ptr<RemoteState>;
    using RemoteStateCPtr = std::shared_ptr<const RemoteState>;
}
