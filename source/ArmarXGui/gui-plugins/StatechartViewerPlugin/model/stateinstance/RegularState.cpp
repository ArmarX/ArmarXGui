/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "../State.h"
#include "RegularState.h"
#include "Visitor.h"

namespace armarx::statechartmodel
{

    RegularState::RegularState(StatePtr stateClass, const QString& instanceName, StatePtr parentState) :
        StateInstance(instanceName, parentState)
        , stateClass(stateClass)
    {
        if (stateClass)
        {
            stateClassUUID = stateClass->getUUID();
            setStateClass(stateClass);
        }
    }

    RegularState::RegularState(const QString& stateClassUUID, const QString& instanceName, StatePtr parentState) :
        StateInstance(instanceName, parentState),
        stateClassUUID(stateClassUUID)
    {}

    StatePtr RegularState::getStateClass() const
    {
        return stateClass;
    }

    void RegularState::setStateClass(StatePtr newStateClass)
    {
        if (newStateClass)
        {
            if (stateClass)
            {
                // TODO check if disconnect works
                disconnect(stateClass.get(), SIGNAL(resized()), this, SLOT(updateScale()));
            }

            stateClass = newStateClass;
            connect(stateClass.get(), SIGNAL(resized()), this, SLOT(updateScale()));
        }
    }
}
