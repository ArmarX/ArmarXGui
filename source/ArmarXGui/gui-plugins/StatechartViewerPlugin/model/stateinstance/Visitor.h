/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Christian Mandery (christian.mandery at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <memory>

namespace armarx::statechartmodel
{
    class LocalState;
    class RemoteState;
    class EndState;
    class DynamicRemoteState;

    using LocalStateCPtr = std::shared_ptr<const LocalState>;
    using RemoteStateCPtr = std::shared_ptr<const RemoteState>;
    using EndStateCPtr = std::shared_ptr<const EndState>;
    using DynamicRemoteStateCPtr = std::shared_ptr<const DynamicRemoteState>;

    class Visitor
    {
        friend class LocalState;
        friend class RemoteState;
        friend class EndState;
        friend class DynamicRemoteState;

    public:
        virtual ~Visitor() {}

    protected:
        virtual void visitLocalState(LocalStateCPtr localState) = 0;
        virtual void visitRemoteState(RemoteStateCPtr remoteState) = 0;
        virtual void visitEndState(EndStateCPtr endState) = 0;
        virtual void visitDynamicRemoteState(DynamicRemoteStateCPtr dynamicRemoteState) = 0;
    };

    using VisitorPtr = std::shared_ptr<Visitor>;
    using VisitorCPtr = std::shared_ptr<const Visitor>;
}
