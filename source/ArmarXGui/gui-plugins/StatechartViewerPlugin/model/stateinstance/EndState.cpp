/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "EndState.h"
#include "Visitor.h"

namespace armarx::statechartmodel
{
    EndState::EndState(QString instanceName, QString eventName, StatePtr parentState) :
        StateInstance(instanceName, parentState)
        , eventName(eventName)
    {

    }

    void EndState::setInstanceName(const QString& value)
    {
        StateInstance::setInstanceName(value);
        setEventName(value);
    }


    eStateType EndState::getType() const
    {
        return eFinalState;
    }

    QString EndState::getEventName() const
    {
        return eventName;
    }

    void EndState::setEventName(const QString& newEventName)
    {
        eventName = newEventName;
    }

    void EndState::accept(Visitor& visitor) const
    {
        std::shared_ptr<const StateInstance> siPtr = shared_from_this();
        visitor.visitEndState(std::static_pointer_cast<const EndState>(siPtr));
    }
}
