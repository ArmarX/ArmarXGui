/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "RemoteState.h"
#include "Visitor.h"

namespace armarx::statechartmodel
{
    RemoteState::RemoteState(StatePtr stateClass, const QString& instanceName, const QString& proxyName, StatePtr parentState) :
        RegularState(stateClass, instanceName, parentState)
        , proxyName(proxyName)
    {
    }

    RemoteState::RemoteState(const QString& stateUUID, const QString& instanceName, StatePtr parentState)
        : RegularState(stateUUID, instanceName, parentState)
    {
    }

    eStateType RemoteState::getType() const
    {
        return eRemoteState;
    }

    void RemoteState::accept(Visitor& visitor) const
    {
        std::shared_ptr<const StateInstance> siPtr = shared_from_this();
        visitor.visitRemoteState(std::static_pointer_cast<const RemoteState>(siPtr));
    }
}
