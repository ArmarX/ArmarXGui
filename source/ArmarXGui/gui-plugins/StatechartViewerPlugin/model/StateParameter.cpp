/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "StateParameter.h"

#include <ArmarXCore/util/json/JSONObject.h>

#include <ArmarXCore/statechart/xmlstates/profiles/StatechartProfiles.h>
#include <ArmarXCore/observers/variant/VariantContainer.h>
#include <ArmarXCore/statechart/StateParameter.h>
#include <ArmarXCore/core/logging/Logging.h>

namespace armarx::statechartmodel
{

    StateParameter::StateParameter() :
        optional(false)
    {
    }

    void StateParameter::setDefaultValue(const VariantContainerBasePtr& value)
    {
        profileDefaultValues[QString::fromUtf8(StatechartProfiles::GetRootName().data())].first = value;
    }

    VariantContainerBasePtr StateParameter::getDefaultValue()
    {
        return profileDefaultValues[QString::fromUtf8(StatechartProfiles::GetRootName().data())].first;
    }

    void StateParameter::setDefaultValueJson(const QString& valueJson)
    {
        profileDefaultValues[QString::fromUtf8(StatechartProfiles::GetRootName().data())].second = valueJson;
    }

    QString StateParameter::getDefaultValueJson()
    {
        return profileDefaultValues[QString::fromUtf8(StatechartProfiles::GetRootName().data())].second;
    }


    statechartmodel::StateParameterPtr StateParameter::FromIceStateParameter(armarx::StateParameterIceBasePtr param)
    {
        statechartmodel::StateParameterPtr p(new statechartmodel::StateParameter());
        p->setDefaultValue(param->defaultValue);
        JSONObjectPtr json = new JSONObject();
        std::string typeName;
        try
        {
            if (param->defaultValue)
            {
                typeName = VariantContainerType::allTypesToString(param->defaultValue->getContainerType());
                //std::string dataType = Variant::getDataType(Variant::getTypeId(typeName));
                p->type = QString::fromStdString(typeName);
                //p->dataType.fromStdString(dataType);
                json->serializeIceObject(param->defaultValue);
                p->setDefaultValueJson(QString::fromStdString(json->asString(false)));
            }
        }
        catch (std::exception& e)
        {
            ARMARX_INFO_S << "Default value serialization failed: " << typeName << " - " << e.what();
        }

        p->optional = param->optionalParam;

        return p;
    }

    StateParameterIceBasePtr StateParameter::ToIceStateParameter(StateParameterPtr param)
    {
        StateParameterIceBasePtr r = armarx::StateParameter::create();
        r->defaultValue = param->getDefaultValue();
        r->optionalParam = param->optional;
        r->set = false;
        return r;
    }

    armarx::StateParameterMap StateParameter::ToIceStateParameterMap(StateParameterMap params)
    {
        armarx::StateParameterMap result;
        StateParameterMap::const_iterator it = params.begin();
        for (; it != params.end(); it++)
        {
            result[it.key().toStdString()] = ToIceStateParameter(it.value());
        }
        return result;
    }

}
