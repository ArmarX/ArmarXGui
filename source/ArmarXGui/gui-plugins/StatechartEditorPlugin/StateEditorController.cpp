/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "StateEditorController.h"
#include "StateTreeController.h"
#include <ArmarXGui/gui-plugins/StatechartViewerPlugin/view/StateItem.h>
#include <ArmarXGui/gui-plugins/StatechartViewerPlugin/view/StateTabWidget.h>
#include <ArmarXGui/gui-plugins/StatechartViewerPlugin/view/TransitionItem.h>
#include "view/dialogs/StateDialog.h"
#include "view/dialogs/TransitionDialog.h"
#include "../StatechartViewerPlugin/view/StatechartView.h"
#include "../StatechartViewerPlugin/model/stateinstance/RemoteState.h"
#include "../StatechartViewerPlugin/model/stateinstance/DynamicRemoteState.h"
#include <QMenu>
#include <QMessageBox>
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/view/dialogs/SetSupportPointDialog.h>

namespace armarx
{

    StateEditorController::StateEditorController(StatechartEditorMainWindow* mainWindow, StateTreeControllerPtr treeController, Ice::CommunicatorPtr communicator, VariantInfoPtr variantInfo, StatechartProfilePtr currentProfile, QPointer<TipDialog> tipDialog, QObject* parent) :
        QObject(parent),
        mainWindow(mainWindow),
        treeController(treeController),
        communicator(communicator),
        variantInfo(variantInfo),
        currentProfile(currentProfile),
        tipDialog(tipDialog)
    {
        if (mainWindow)
        {
            connect(mainWindow->getStateTabWidget(), SIGNAL(currentChanged(int)), this, SLOT(connectToStateTab(int)));
        }
        else
        {
            ARMARX_ERROR_S << "mainwindow ptr must not be NULL";
        }

        editState = menu.addAction(QIcon(":/icons/accessories-text-editor-6.ico"), "");
        deleteState = menu.addAction(QIcon(":/icons/delete.ico"), "Delete State");
        layoutState = menu.addAction(QIcon(":/icons/magic-wand-icon.png"), "Layout State");
        setstartstate = menu.addAction("Set as initial State");

        newEndstate = menu.addAction(QIcon(":/statechart-editor/new-endstate.svg"), "Insert new Endstate");
        replaceState = menu.addAction("Replace State");
        openCode = menu.addAction(QIcon(":/icons/cpp.svg"), "Open C++ Code");
        findStateInTree = menu.addAction(QIcon(":/icons/search.svg"), "Find State in Tree");
        for (QAction* a : menu.actions())
        {
            a->setIconVisibleInMenu(true);
        }
        connect(mainWindow->ui->actionEdit_State_Properties, SIGNAL(triggered(bool)), this, SLOT(editSelectedState()));
    }


    void StateEditorController::stateDialogAccepted(statechartmodel::StateInstancePtr stateInstance, const StateDialog& d)
    {
        statechartmodel::StatePtr state = stateInstance->getStateClass();

        if (stateInstance->getParent())
        {
            stateInstance->getParent()->renameSubstate(stateInstance->getInstanceName(), d.getStateInstanceName());
        }

        if (state)
        {
            //                ARMARX_INFO_S << "setting class params";
            //                state->setStateName(d.getStateName());

            state->setDescription(d.getDescription());
            state->setOutgoingEvents(d.getOutgoingEvents());
            state->setInputParameters(d.getInputParams());
            state->setLocalParameters(d.getLocalParams());
            state->setOutputParameters(d.getOutputParams());
        }

        statechartmodel::RemoteStatePtr remoteState = std::dynamic_pointer_cast<statechartmodel::RemoteState>(stateInstance);

        if (remoteState)
        {
            remoteState->proxyName = d.getProxyName();
        }

        statechartmodel::EndStatePtr endstate = std::dynamic_pointer_cast<statechartmodel::EndState>(stateInstance);

        if (endstate)
        {
            endstate->setEventName(d.getStateInstanceName());
        }
    }

    void StateEditorController::showStateContextMenu(statechartmodel::StateInstancePtr stateInstance, QPoint mouseScreenPos, QPointF mouseItemPos)
    {
        if (!stateInstance)
        {
            return;
        }
        bool editable = true;
        if (stateInstance &&
            stateInstance->getStateClass() &&
            !stateInstance->getStateClass()->isEditable())
        {
            editable = false;
        }



        statechartmodel::StatePtr parentState = stateInstance->getParent();
        StateTreeNodePtr node = treeController->getNodeByState(stateInstance->getStateClass());
        editState->setText("Edit State " + stateInstance->getInstanceName());
        deleteState->setEnabled(!!parentState && parentState->isEditable());
        setstartstate->setEnabled(!!parentState && parentState->isEditable());

        replaceState->setEnabled(editable);


        openCode->setEnabled(!(!stateInstance->getStateClass() || (node && !node->getCppExists())));


        newEndstate->setEnabled(!(stateInstance->getType() == eFinalState || stateInstance->getType() == eDynamicRemoteState)
                                && stateInstance->getStateClass()
                                && stateInstance->getStateClass()->isEditable());


        layoutState->setEnabled(!(stateInstance->getType() == eFinalState
                                  || stateInstance->getType() == eDynamicRemoteState
                                  || !stateInstance->getStateClass()
                                  || stateInstance->getStateClass()->getSubstates().size() <= 1));


        replaceState->setEnabled(false);

        QAction* result = menu.exec(mouseScreenPos);

        if (result == editState)
        {
            StateDialog d(stateInstance, communicator, variantInfo, currentProfile, lockRemoteStates && stateInstance->getType() == eRemoteState ? true : false, !editable, tipDialog);

            if (d.exec() == QDialog::Accepted)
            {
                stateDialogAccepted(stateInstance, d);
            }
        }
        else if (result == deleteState)
        {
            if (QMessageBox::question(0, "State deletion", "Do you really want to delete state '" + stateInstance->getInstanceName() + "'?", QMessageBox::Yes, QMessageBox::No) == QMessageBox::Yes)
            {
                parentState->removeSubstate(stateInstance);
            }
        }
        else if (result == layoutState)
        {
            mainWindow->layoutState();
        }
        else if (result == setstartstate)
        {
            parentState->setStartState(stateInstance);
        }
        else if (result == newEndstate)
        {
            ARMARX_INFO_S << "Inserting endstate";
            statechartmodel::StatePtr state = stateInstance->getStateClass();
            int i = 2;
            const QString newStateNameBase = "MyEndState";
            QString newStateName = newStateNameBase;

            while (!state->addEndSubstate(newStateName, newStateName, mouseItemPos))
            {
                newStateName = newStateNameBase + "_" + QString::number(i);
                i++;
            }

        }
        else if (result == openCode && stateInstance->getStateClass())
        {
            StateTreeNodePtr node = treeController->getNodeByState(stateInstance->getStateClass());
            treeController->openStateCPP(node);
        }
        else if (result == findStateInTree && stateInstance->getStateClass())
        {
            treeController->selectNodeByState(stateInstance->getStateClass());
        }

    }

    void StateEditorController::showTransitionContextMenu(statechartmodel::TransitionCPtr transition, statechartmodel::StatePtr state, QPoint mouseScreenPos, QPointF mouseItemPos)
    {
        ARMARX_DEBUG_S << "pos: " << mouseItemPos;
        QMenu menu;
        QAction* editTransition = menu.addAction("Edit Transition " + transition->eventName);
        QAction* enableTransitionCode = menu.addAction("Enable Transition Hook for " + transition->eventName);
        enableTransitionCode->setCheckable(true);
        enableTransitionCode->setChecked(transition->transitionUserCode);

        StateTreeNodePtr node = treeController->getNodeByState(state);


        if (!transition->destinationState || !transition->destinationState->getStateClass()
            || !node || !node->getCppExists() || !transition->sourceState)
        {
            if (!node->getCppExists())
            {
                enableTransitionCode->setText(enableTransitionCode->text() + " (no parent cpp)");
            }
            else if (!transition->sourceState)
            {
                enableTransitionCode->setText("Transition code unavailable for initial transition");
            }
            else if (transition->destinationState && !transition->destinationState->getStateClass())
            {
                enableTransitionCode->setText(enableTransitionCode->text() + " (final or unloaded destination state)");
            }
            enableTransitionCode->setEnabled(false);
        }

        QAction* detach = menu.addAction("Detach Transition");
        //QAction* insertPoint = menu.addAction("Insert Support Point");
        //insertPoint->setEnabled(false);

        if (!transition->sourceState || !transition->destinationState)
        {
            detach->setEnabled(false);
        }

        QAction* setSupportPoint {nullptr};
        std::map<QAction*, int> bendMap;

        if (transition->sourceState && transition->destinationState)
        {
            menu.addSeparator();
            bendMap.insert(std::make_pair(menu.addAction("Straighten"), 0));
            QMenu* bend = menu.addMenu("Bend");
            bendMap.insert(std::make_pair(bend->addAction("Bend Left 10%"), 10));
            bendMap.insert(std::make_pair(bend->addAction("Bend Left 25%"), 25));
            bendMap.insert(std::make_pair(bend->addAction("Bend Left 50%"), 50));
            bendMap.insert(std::make_pair(bend->addAction("Bend Left 100%"), 100));
            bendMap.insert(std::make_pair(bend->addAction("Bend Right 10%"), -10));
            bendMap.insert(std::make_pair(bend->addAction("Bend Right 25%"), -25));
            bendMap.insert(std::make_pair(bend->addAction("Bend Right 50%"), -50));
            bendMap.insert(std::make_pair(bend->addAction("Bend Right 100%"), -100));
            setSupportPoint = menu.addAction("Set Support Point");
        }

        QAction* result = menu.exec(mouseScreenPos);




        if (result == editTransition)
        {
            if (transition->destinationState &&
                transition->destinationState->getType() != eFinalState &&
                !transition->destinationState->getStateClass())
            {
                QMessageBox::warning(mainWindow, "Transition Editing Error", "Transition editing not possible because destination state is not loaded - load statechart group of destination state.");
            }
            else
            {

                TransitionDialog d(transition, state, getRelevantProfiles(), communicator, variantInfo);

                if (d.exec() == QDialog::Accepted)
                {
                    state->setTransitionMapping(transition, d.getMappingToNextStateInput(),
                                                d.getMappingToParentStateLocal(),
                                                d.getMappingToParentStateOutput());
                }
            }
        }
        else if (result == detach)
        {
            state->detachTransitionDestination(transition);
        }
        //    else if(result == insertPoint)
        //    {
        //        state->addSupportPoint(transition, mouseItemPos);
        //    }
        else if (bendMap.find(result) != bendMap.end())
        {
            state->bendTransition(transition, 50, bendMap[result]);
        }
        else if (result == setSupportPoint)
        {
            SetSupportPointDialog d;

            if (d.exec() == QDialog::Accepted)
            {
                state->bendTransition(transition, d.getU(), d.getV());
            }
        }
        else if (result == enableTransitionCode)
        {
            state->setTransitionUserCodeEnabled(transition, result->isChecked());
            if (result->isChecked())
            {
                tipDialog->showMessage("Transition functions enable you to execute user code during a transition. This is designated for parameter conversion or minor calculations. Do NOT execute long running operations in transition functions.\n"
                                       "On next saving of the statecharts a pure virtual function will be generated in the state of this transition. You need to implement this function. Otherwise a compilation error will occur.", "Transition Function", "TransitionFunction");
            }
        }
    }

    QList<QString> StateEditorController::getRelevantProfiles() const
    {
        if (!currentProfile)
        {
            return QList<QString>();
        }

        QList<QString> result {QString::fromUtf8(currentProfile->getName().c_str())};
        auto profile = currentProfile;

        while ((profile = profile->getParent()))
        {
            result.push_back(QString::fromUtf8(profile->getName().c_str()));
        }

        return result;
    }

    void StateEditorController::editSelectedState()
    {
        if (mainWindow->getStateTabWidget()->currentStateview()
            && mainWindow->getStateTabWidget()->currentStateview()->getScene()
            && mainWindow->getStateTabWidget()->currentStateview()->getScene()->selectedItems().size() > 0)
        {
            StateItem* item = dynamic_cast<StateItem*>(*mainWindow->getStateTabWidget()->currentStateview()->getScene()->selectedItems().begin());

            if (item)
            {
                StateDialog d(item->getStateInstance(), communicator, variantInfo, currentProfile, item->getStateInstance()->getType() == eRemoteState ? true : false, !item->isEditable(), tipDialog);
                if (d.exec() == QDialog::Accepted)
                {
                    stateDialogAccepted(item->getStateInstance(), d);
                }

            }
            else
            {
                TransitionItem* transitionItem = dynamic_cast<TransitionItem*>(*mainWindow->getStateTabWidget()->currentStateview()->getScene()->selectedItems().begin());
                item = dynamic_cast<StateItem*>(transitionItem->parentItem());
                if (transitionItem && item)
                {
                    TransitionDialog d(transitionItem->getTransition(), item->getStateInstance()->getStateClass(),
                                       getRelevantProfiles(),  communicator, variantInfo);
                    if (d.exec() == QDialog::Accepted)
                    {
                        item->getStateInstance()->getStateClass()->setTransitionMapping(transitionItem->getTransition(), d.getMappingToNextStateInput(),
                                d.getMappingToParentStateLocal(),
                                d.getMappingToParentStateOutput());
                    }
                }
            }

        }
    }


    void StateEditorController::setLockRemoteStatesByDefault(bool lock)
    {
        lockRemoteStates = lock;
    }


    void StateEditorController::connectToStateTab(int index)
    {
        if (index != -1 && mainWindow->getStateTabWidget()->currentStateview() && mainWindow->getStateTabWidget()->currentStateview()->getScene())
        {
            connect(mainWindow->getStateTabWidget()->currentStateview()->getScene(), SIGNAL(stateContextMenuRequested(statechartmodel::StateInstancePtr, QPoint, QPointF)), this, SLOT(showStateContextMenu(statechartmodel::StateInstancePtr, QPoint, QPointF)),  Qt::UniqueConnection);
            connect(mainWindow->getStateTabWidget()->currentStateview()->getScene(), SIGNAL(transitionContextMenuRequested(statechartmodel::TransitionCPtr, statechartmodel::StatePtr, QPoint, QPointF)), this, SLOT(showTransitionContextMenu(statechartmodel::TransitionCPtr, statechartmodel::StatePtr, QPoint, QPointF)),  Qt::UniqueConnection);
            connect(mainWindow->ui->actionShow_Subsubstates,
                    SIGNAL(toggled(bool)),
                    mainWindow->getStateTabWidget()->currentStateview(),
                    SLOT(showSubSubstates(bool)));

        }
    }

}
