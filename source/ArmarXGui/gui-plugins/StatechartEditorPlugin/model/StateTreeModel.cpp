/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "StateTreeModel.h"

#include "../io/GroupXmlReader.h"
#include "../io/GroupXmlWriter.h"
#include "../io/XmlWriter.h"

#include <ArmarXCore/core/ArmarXManager.h>
#include <ArmarXCore/core/exceptions/local/FileIOException.h>
#include <ArmarXCore/core/system/DynamicLibrary.h>
#include <ArmarXCore/core/system/FactoryCollectionBase.h>
#include <ArmarXCore/statechart/xmlstates/StateGroupGenerator.h>
#include <ArmarXCore/statechart/xmlstates/baseclassgenerator/XmlContextBaseClassGenerator.h>
#include <ArmarXCore/statechart/xmlstates/baseclassgenerator/XmlStateBaseClassGenerator.h>
#include <QMessageBox>
#include <omp.h>

#include <filesystem>


using namespace armarx;
using namespace statechartio;
using namespace statechartmodel;

StateTreeModel::StateTreeModel(Ice::CommunicatorPtr ic, VariantInfoPtr variantInfo) :
    iceCommunicator(ic),
    variantInfo(variantInfo)
{
    StateTreeNodePtr nullNode;
    StatechartGroupPtr nullGroup;
    this->rootNode.reset(new StateTreeNode(QString(), "", StateTreeNode::Folder, nullNode, nullGroup, false));
}

void StateTreeModel::loadAllStates()
{
    QList<statechartmodel::StatePtr> statesToAnnotate;

    for (int i = 0; i < groups.count(); i++)
    {
        StatechartGroupPtr group = groups.at(i);
        ARMARX_INFO_S << "loading group " << group->getName();

        for (int j = 0; j < group->getRootNode()->getChildren().count(); j++)
        {
            loadNode(group->getRootNode()->getChildren().at(j), group->getBoostGroupPath(), statesToAnnotate);
        }
    }

    annotateAllStateRerefences();
}

void StateTreeModel::loadGroupStates(StatechartGroupPtr group)
{
    QList<statechartmodel::StatePtr> statesToAnnotate;

    for (int j = 0; j < group->getRootNode()->getChildren().count(); j++)
    {
        loadNode(group->getRootNode()->getChildren().at(j), group->getBoostGroupPath(), statesToAnnotate);
    }

    annotateAllStateRerefences();
}

void StateTreeModel::annotateStateRerefences(const QList<statechartmodel::StatePtr>& statesToAnnotate)
{
    QMap<QString, statechartmodel::StatePtr> referenceMap;
    QMapIterator<QString, StateTreeNodePtr> i(uuidToNodeMap);

    while (i.hasNext())
    {
        i.next();
        referenceMap.insert(i.key(), i.value()->getState());
    }

    for (int i = 0; i < statesToAnnotate.count(); i++)
    {
        statesToAnnotate.at(i)->addReferences(referenceMap);
    }
}

void StateTreeModel::annotateAllStateRerefences()
{
    QMap<QString, statechartmodel::StatePtr> referenceMap;
    QMapIterator<QString, StateTreeNodePtr> i(uuidToNodeMap);

    while (i.hasNext())
    {
        i.next();
        referenceMap.insert(i.key(), i.value()->getState());
    }

    for (auto i : uuidToNodeMap)
    {
        i->getState()->addReferences(referenceMap);
    }
}

void StateTreeModel::loadNode(StateTreeNodePtr node, std::filesystem::path parentPath, QList<statechartmodel::StatePtr>& allTheStates)
{
    std::filesystem::path nodePath = node->getBoostPath(parentPath);

    if (node->getNodeType() == StateTreeNode::State)
    {
        try
        {
            QString xml = GuiStatechartGroupXmlReader::ReadFileContents(QString::fromUtf8(nodePath.string().c_str()));
            std::shared_ptr<XmlReader> reader(new XmlReader(iceCommunicator, variantInfo));
            reader->parseXml(xml);
            statechartmodel::StatePtr state = reader->getLoadedState();
            state->setEditable(node->getGroup()->getWriteAccess() == StatechartGroup::eWritable);
            setNodeState(node, state);
            uuidToNodeMap.insert(state->getUUID(), node);
            allTheStates.append(state);
        }
        catch (armarx::exceptions::local::FileOpenException& ex)
        {
            ARMARX_ERROR_S << ex.what();
        }

    }
    else if (node->getNodeType() == StateTreeNode::Folder)
    {
        for (int i = 0; i < node->getChildren().count(); i++)
        {
            loadNode(node->getChildren().at(i), nodePath, allTheStates);
        }
    }
}

StatechartGroupPtr StateTreeModel::loadGroup(QString groupDefinitionFile, StateTreeNodePtr rootNode)
{
    StatechartGroupPtr group = GuiStatechartGroupXmlReader::ReadXml(groupDefinitionFile, rootNode);

    if (groupExists(group->getBoostGroupPath()))
    {
        return StatechartGroupPtr();
    }
    group->getPackageName();
    loadVariantInfos(group->getPackageName().toStdString());
    groups.append(group);
    return group;
}

void StateTreeModel::clear()
{
    groups.clear();
    uuidToNodeMap.clear();
}

void StateTreeModel::addGroup(StatechartGroupPtr group)
{
    groups.append(group);
}

void StateTreeModel::notifyNewStateTreeNode(StateTreeNodePtr node)
{
    if (node->getNodeType() == StateTreeNode::State)
    {
        uuidToNodeMap.insert(node->getState()->getUUID(), node);
    }
}

void StateTreeModel::notifyDeleteStateTreeNode(StateTreeNodePtr node)
{
    if (node->getNodeType() == StateTreeNode::State && node->getState())
    {
        uuidToNodeMap.remove(node->getState()->getUUID());
    }
}

void StateTreeModel::saveAll(QPointer<QProgressDialog> progressDialog)
{
    if (progressDialog)
    {
        progressDialog->setMinimum(0);
        progressDialog->setMaximum(groups.count());
        progressDialog->setLabelText("Saving statechart groups");
    }
    auto startTime = IceUtil::Time::now();
    #pragma omp parallel for schedule(static,1)
    for (int i = 0; i < groups.count(); i++)
    {
        StatechartGroupPtr group = groups.at(i);
        if (group->getWriteAccess() == StatechartGroup::eReadOnly)
        {
            continue;
        }
        std::filesystem::create_directories(group->getBoostDefinitionFilePath().parent_path());
        GroupXmlWriter::WriteXml(group, group->getDefinitionFilePath(), true);

        for (int j = 0; j < group->getRootNode()->getChildren().count(); j++)
        {
            saveNode(group->getRootNode()->getChildren().at(j), group->getBoostGroupPath());
        }
        if (omp_get_thread_num() == 0)
        {
            if (progressDialog && (IceUtil::Time::now() - startTime).toMilliSeconds() > 1000)
            {
                progressDialog->setValue(i + 1);
                //                QMetaObject::invokeMethod(progressDialog, "setValue", Q_ARG(int, (i + 1)));
            }
        }
    }
}

void StateTreeModel::saveNode(StateTreeNodePtr node, std::filesystem::path parentPath)
{
    std::filesystem::path nodePath = node->getBoostPath(parentPath);

    if (node->getGroup()->getWriteAccess() == StatechartGroup::eReadOnly)
    {
        return;
    }

    if (node->getNodeType() == StateTreeNode::State)
    {
        try
        {
            if (node->getState())
            {
                XmlWriter writer(variantInfo);
                writer.serialize(node->getState(), uuidToNodeMap);
                //GroupXmlWriter::WriteFileContents(QString::fromUtf8(nodePath.string().c_str()), writer.getXmlString(true));
                if (StatechartGroupGenerator::writeFileContentsIfChanged(nodePath.string(), writer.getXmlString(true).toUtf8().data()))
                {
                    ARMARX_INFO_S << "Writing new state definition to " << nodePath.string();
                }
            }
        }
        catch (armarx::exceptions::local::FileOpenException& ex)
        {
            ARMARX_ERROR_S << ex.what();
        }
        catch (std::exception& e)
        {
            QMetaObject::invokeMethod(this, "showWarning", Q_ARG(QString, "Saving state failed!"), Q_ARG(QString, QString::fromStdString(std::string(e.what())).split("Backtrace:").first()));
        }

    }
    else if (node->getNodeType() == StateTreeNode::Folder)
    {

        std::filesystem::create_directory(nodePath);

        for (int i = 0; i < node->getChildren().count(); i++)
        {
            saveNode(node->getChildren().at(i), nodePath);
        }
    }
}

void StateTreeModel::generateGroupDocString(StatechartGroupPtr group, QString doxyGroupPath)
{
    QString doxString = "/**\n";
    doxString += "@defgroup " + group->getPackageName() +  "-" + group->getName() + " " + group->getName() + "";
    doxString += "\n@ingroup Statecharts\n";
    doxString += group->getDescription();
    doxString += "@defgroup " + group->getPackageName() +  "-" + group->getName() + "-Substates " + group->getName() + " Substates\n";
    doxString += "@ingroup " + group->getPackageName() +  "-" + group->getName();
    doxString += "\n*/";

    //    GroupXmlWriter::WriteFileContents(doxyGroupPath, doxString);
}

const CMakePackageFinder& StateTreeModel::getPackageFinder(const QString& packageName)
{
    std::unique_lock lock(findersMutex);
    auto it = finders.find(packageName);
    if (it == finders.end())
    {
        finders.insert(std::make_pair(packageName, CMakePackageFinder(packageName.toStdString())));
        return finders.at(packageName);
    }
    else
    {
        return it->second;
    }

}

void StateTreeModel::loadVariantInfos(const std::string& packageName)
{
    //ARMARX_INFO << "loadVariantInfos: " << packageName;
    auto finder = getPackageFinder(QString::fromStdString(packageName));
    bool loaded = variantInfo->isPackageLoaded(packageName);
    if (!finder.packageFound())
    {
        if (!!loaded)
        {
            ARMARX_WARNING_S << "Could not find package " << packageName << " - generated code might be incomplete";
        }
    }
    else
    {
        if (!loaded)
        {
            ARMARX_INFO_S << "Loading variant info for " << packageName << " and its dependencies";
            variantInfo = VariantInfo::ReadInfoFilesRecursive(packageName, finder.getConfigDir(), false, variantInfo);
        }
        auto contains = [](const std::map<VariantTypeId, std::string>& map, const std::string & value)
        {
            for (const auto& v : map)
            {
                if (v.second == value)
                {
                    return true;
                }
            }
            return false;
        };


        for (VariantInfo::LibEntryPtr lib : variantInfo->getLibs())
        {
            if (lib->getPackageName() != packageName)
            {
                continue;
            }
            for (VariantInfo::VariantEntryPtr var : lib->getVariants())
            {

                if (!contains(Variant::getTypes(), var->getBaseTypeName()))
                {
                    //ARMARX_IMPORTANT << VAROUT(Variant::getTypes());
                    //ARMARX_INFO << "loading lib for " << var->getBaseTypeName();
                    DynamicLibraryPtr lib = variantInfo->loadLibraryOfVariant(var->getBaseTypeName());
                    ARMARX_CHECK_EXPRESSION(lib) << var->getBaseTypeName();
                    lib->setUnloadOnDestruct(false);

                    ArmarXManager::RegisterKnownObjectFactoriesWithIce(iceCommunicator);

                    Variant::addTypeName(var->getBaseTypeName());
                    //                    ARMARX_IMPORTANT << VAROUT(Variant::getTypes());
                    if (iceCommunicator->getValueFactoryManager()->find(var->getBaseTypeName()))
                    {
                        ARMARX_INFO << "Loaded variant " << var->getBaseTypeName();
                    }
                    else
                    {
                        ARMARX_WARNING << "variant " << var->getBaseTypeName() << " could not be loaded. Adding Variant TypeName only.";
                    }
                }
            }
        }

        for (auto& dep : finder.getDependencies())
        {
            loadVariantInfos(dep);
        }
    }
}

void StateTreeModel::generateAllBaseClasses(QPointer<QProgressDialog> progressDialog)
{
    if (progressDialog)
    {
        progressDialog->setMinimum(0);
        progressDialog->setMaximum(groups.count());
        progressDialog->setLabelText("Generating code for statechart groups");
    }
    auto startTime = IceUtil::Time::now();
    #pragma omp parallel for schedule(static,1)
    for (int i = 0; i < groups.size(); ++i)
        //    for (const StatechartGroupPtr& group : groups)
    {
        const StatechartGroupPtr& group = groups.at(i);
        generateBaseClasses(group);
        if (omp_get_thread_num() == 0)
        {
            if (progressDialog && (IceUtil::Time::now() - startTime).toMilliSeconds() > 1000)
            {
                progressDialog->setValue(i + 1);
                //                QMetaObject::invokeMethod(progressDialog, "setValue", Q_ARG(int, (i + 1)));
            }
        }
        else
        {
            if (progressDialog && (IceUtil::Time::now() - startTime).toMilliSeconds() > 1000)
            {
                QMetaObject::invokeMethod(progressDialog, "setValue", Q_ARG(int, (i + 1)));
            }
        }
    }
}

std::set<std::string> StateTreeModel::getVariantOfStatesWithNoCpp(const StatechartGroupPtr& group)
{
    std::set<std::string> variantTypes;
    std::set<std::string> alreadyLinkedTypes;
    for (StateTreeNodePtr& stateNode : group->getAllNodes())
    {
        std::set<std::string>* currentTypeSet = &variantTypes;

        if (stateNode->checkCppExists())
        {
            // only need if no cpp exists
            currentTypeSet = &alreadyLinkedTypes;
        }
        auto state = stateNode->getState();
        if (!state)
        {
            continue;
        }
        for (auto param : state->getInputAndLocalParameters())
        {
            QString type = param->type;
            currentTypeSet->insert(type.toStdString());
        }
        for (auto param : state->getOutputParameters())
        {
            QString type = param->type;
            currentTypeSet->insert(type.toStdString());
        }
    }


    std::set<std::string> resultingTypes;
    std::set_difference(variantTypes.begin(), variantTypes.end(),
                        alreadyLinkedTypes.begin(), alreadyLinkedTypes.end(),
                        std::inserter(resultingTypes, resultingTypes.end()));

    std::set<std::string> innerVariantTypes;
    for (auto& type : resultingTypes)
    {
        auto containerType = VariantContainerType::FromString(type);
        while (containerType->subType)
        {
            containerType = containerType->subType;
        }
        if (!variantInfo->isBasic(containerType->typeId))
        {
            innerVariantTypes.insert(containerType->typeId);
        }
    }

    return innerVariantTypes;
}

void StateTreeModel::generateBaseClasses(const StatechartGroupPtr& group)
{
    /*std::filesystem::create_directories(group->getBoostDefinitionFilePath().parent_path());
    std::vector<std::string> namespaces;
    namespaces.push_back("armarx");
    namespaces.push_back(group->getName().toUtf8().data());*/
    auto checkPackage = [&](const CMakePackageFinder & finder)
    {
        if (!finder.packageFound())
        {
            QMetaObject::invokeMethod(this, "showWarning", Q_ARG(QString, "State Generation Error"), Q_ARG(QString, "Could not find the package '" + QString::fromStdString(finder.getName()) + "' - Did you run cmake for that package already? Skipping state generation of stategroup " + group->getName()));
            return false;
        }
        else
        {
            return true;
        }
    };


    std::map<QString, CMakePackageFinder>::const_iterator it;
    {
        std::unique_lock lock(findersMutex);
        it = finders.find(group->getPackageName());



        if (it == finders.end())
        {
            CMakePackageFinder finder(group->getPackageName().toStdString());

            if (!checkPackage(finder))
            {
                return;
            }

            it = finders.insert(std::make_pair(group->getPackageName(), finder)).first;
        }
    }

    CMakePackageFinder packageFinder = it->second;

    if (!checkPackage(packageFinder))
    {
        return;
    }
    else if (packageFinder.getBuildDir().empty())
    {
        ARMARX_WARNING_S << "Build dir for package '" << packageFinder.getName() << "' is empty! Skipping generating files.";
        return;
    }


    std::filesystem::path doxyFile = packageFinder.getBuildDir();

    if (!doxyFile.string().empty())
    {
        doxyFile /= "doxygen";
    }
    else
    {
        doxyFile = ".";
    }

    doxyFile /= group->getName().toStdString() + ".dox";
    generateGroupDocString(group, QString::fromStdString(doxyFile.string()));

    std::set<std::string> proxies;
    proxies.insert("ArmarXCoreInterfaces.systemObserver");
    proxies.insert("ArmarXCoreInterfaces.conditionHandler");
    for (QString p : group->getProxies())
    {
        proxies.insert(p.toUtf8().data());
    }
    Ice::StringSeq proxyVec(proxies.begin(), proxies.end());
    if (group->contextGenerationEnabled())
    {
        std::set<std::string> innerVariantTypes = getVariantOfStatesWithNoCpp(group);
        StatechartGroupGenerator::generateStatechartContextFile(packageFinder.getBuildDir(),
                group->getPackageName().toUtf8().data(),
                group->getName().toUtf8().data(),
                proxyVec,
                variantInfo,
                innerVariantTypes,
                false);
        //std::string cpp = XmlContextBaseClassGenerator::GenerateCpp(namespaces, proxies, group->getName().toUtf8().data(), variantInfo);
        //writeIfChanged((group->getBoostGroupPath() / (std::string(group->getName().toUtf8().data()) + "StatechartContext.generated.h")).c_str(), cpp);
    }

    for (StateTreeNodePtr node : group->getRootNode()->getChildren())
    {
        generateBaseClass(node, packageFinder.getBuildDir(), variantInfo, proxyVec);
    }
}

void StateTreeModel::generateBaseClass(StateTreeNodePtr node, std::filesystem::path buildDir, VariantInfoPtr variantInfo, std::vector<std::string> proxies)
{
    //std::filesystem::path nodePath = node->getBoostPath(parentPath);
    if (node->getNodeType() == StateTreeNode::State)
    {
        try
        {
            if (node->getState() && node->getCppExists())
            {
                XmlWriter writer(variantInfo);
                writer.serialize(node->getState(), uuidToNodeMap);
                std::string xml = writer.getXmlString(false).toUtf8().data();
                StatechartGroupGenerator::generateStateFile(node->getState()->getStateName().toUtf8().data(), RapidXmlReader::FromXmlString(xml), buildDir, node->getGroup()->getPackageName().toUtf8().data(), node->getGroup()->getName().toUtf8().data(), proxies, node->getGroup()->contextGenerationEnabled(), variantInfo, false);
                //std::string cpp = XmlStateBaseClassGenerator::GenerateCpp(namespaces, RapidXmlReader::FromXmlString(xml), proxies, node->getGroup()->contextGenerationEnabled(), node->getGroup()->getName().toUtf8().data(), variantInfo);
                //std::string targetPath(nodePath.replace_extension().string() + ".generated.h");
                //writeIfChanged(targetPath, cpp);
            }
        }
        catch (armarx::exceptions::local::FileOpenException& ex)
        {
            ARMARX_ERROR_S << ex.what();
        }
        catch (std::exception& e)
        {
            QMessageBox::warning(0, "Saving state failed!", QString::fromStdString(std::string(e.what())).split("Backtrace:").first());
        }

    }
    else if (node->getNodeType() == StateTreeNode::Folder)
    {
        //std::filesystem::create_directory(nodePath);
        for (StateTreeNodePtr child : node->getChildren())
        {
            generateBaseClass(child, buildDir, variantInfo, proxies);
        }
    }
}

void StateTreeModel::writeIfChanged(std::string targetPath, std::string contents)
{
    bool identical = false;

    if (std::filesystem::exists(std::filesystem::path(targetPath)))
    {
        std::string currentContents = RapidXmlReader::ReadFileContents(targetPath);
        identical = contents == currentContents;
    }

    if (!identical)
    {
        QString qtargetPath = QString::fromUtf8(targetPath.data());
        //QMessageBox::information(NULL, "test", qtargetPath);
        ARMARX_LOG_S << "Generated file " << targetPath;
        GroupXmlWriter::WriteFileContents(qtargetPath, QString::fromUtf8(contents.data()));
    }
    else
    {
        ARMARX_LOG_S << "Skipping identical file " << targetPath;
        //QMessageBox::information(NULL, "test", "current file and generated output are identical.");
    }
}

StateTreeNodePtr StateTreeModel::getRootNode() const
{
    return rootNode;
}

QList<StatechartGroupPtr> StateTreeModel::getGroups() const
{
    return QList<StatechartGroupPtr>(groups);
}

StatechartGroupPtr StateTreeModel::getGroupByName(const QString& groupName) const
{
    auto it = std::find_if(groups.constBegin(), groups.constEnd(), [&](const StatechartGroupPtr & g)
    {
        return g->getName() == groupName;
    });

    if (it != groups.constEnd())
    {
        return *it;
    }
    else
    {
        return StatechartGroupPtr();
    }
}

StateTreeNodePtr StateTreeModel::getNodeByState(statechartmodel::StatePtr state) const
{
    if (!state)
    {
        return StateTreeNodePtr();
    }

    return getNodeByUuid(state->getUUID());
}

StateTreeNodePtr StateTreeModel::getNodeByUuid(const QString& uuid) const
{
    QMap<QString, StateTreeNodePtr>::const_iterator it = uuidToNodeMap.find(uuid);

    if (it != uuidToNodeMap.end())
    {
        return it.value();
    }
    else
    {
        return StateTreeNodePtr();
    }
}

void StateTreeModel::setNodeState(StateTreeNodePtr node, statechartmodel::StatePtr state)
{
    node->state = state;
    node->checkCppExists();
    node->display = state->getStateName();
    connect(state.get(), SIGNAL(substateChanged(statechartmodel::StateInstancePtr, statechartmodel::SignalType)), SLOT(notifySubstateChanged(statechartmodel::StateInstancePtr, statechartmodel::SignalType)));
}

QList<StateInstancePtr> StateTreeModel::findStateUsages(StateTreeNodePtr node)
{
    QList<StateInstancePtr> instances;
    QMapIterator<QString, StateTreeNodePtr> i(uuidToNodeMap);

    while (i.hasNext())
    {
        i.next();
        QMapIterator<QString, StateInstancePtr> itSubstate = i.value()->getState()->getSubstates();

        while (itSubstate.hasNext())
        {
            itSubstate.next();
            statechartmodel::StatePtr state = itSubstate.value()->getStateClass();

            if (state && node->getState() == state)
            {
                instances.append(itSubstate.value());
            }
        }
    }

    return instances;
}

bool StateTreeModel::groupExists(StatechartGroupPtr group)
{
    return groupExists(group->getBoostGroupPath());
}

bool StateTreeModel::groupExists(std::filesystem::path groupPath)
{
    foreach (StatechartGroupPtr g, groups)
    {
        if (g->getBoostGroupPath() == groupPath)
        {
            return true;
        }
    }
    return false;
}

void StateTreeModel::notifySubstateChanged(StateInstancePtr substate, SignalType signalType)
{
    if (signalType == eAdded || signalType == eRemoved)
    {
        stateAddedOrRemoved();
    }
}

void StateTreeModel::showWarning(QString title, QString message)
{
    QMessageBox::warning(0, title, message);
}




/*
StatechartGroupTreeNodePtr StateTreeModel::createNewState(QString name, StatechartGroupTreeNodePtr parent)
{
    statechartmodel::StatePtr state(new State());
    state->setStateName(name);
    StatechartGroupTreeNodePtr node(new StatechartGroupTreeNode(name, name + ".xml", StatechartGroupTreeNode::State, parent, parent->getGroup()));
    node->setState(state);
    parent->appendChild(node);
    uuidToNodeMap.insert(state->getUUID(), node);
    return node;
}*/
