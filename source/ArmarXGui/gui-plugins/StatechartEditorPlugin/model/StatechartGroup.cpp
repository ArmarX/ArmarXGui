/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "StatechartGroup.h"

#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>

#include "StateTreeNode.h"

using namespace armarx;

StatechartGroup::StatechartGroup(QString groupDefinitionFilePath, QString groupPath, QString name, QString description, QString packageName, const QList<QString>& proxies, bool generateContext, const QMap<QString, QString>& statechartGroupConfigurations, WriteAccess writable)
{
    this->definitionFilePath = groupDefinitionFilePath;
    this->groupPath = groupPath;
    this->name = name;
    this->description = description;
    this->packageName = packageName;
    this->proxies = proxies;
    this->generateContext = generateContext;
    this->statechartGroupConfigurations = statechartGroupConfigurations;
    this->writable = writable;
}

StateTreeNodePtr StatechartGroup::getRootNode()
{
    return rootNode;
}
QString StatechartGroup::getName() const
{
    return name;
}
QString StatechartGroup::getDefinitionFilePath() const
{
    return definitionFilePath;
}

QString StatechartGroup::getGroupPath() const
{
    return groupPath;
}

std::filesystem::path StatechartGroup::getBoostGroupPath() const
{
    return std::filesystem::path(groupPath.toUtf8().data());
}

std::filesystem::path StatechartGroup::getBoostDefinitionFilePath() const
{
    return std::filesystem::path(definitionFilePath.toUtf8().data());
}
QString StatechartGroup::getDescription() const
{
    return description;
}

const QMap<QString, QString>& StatechartGroup::getConfigurations() const
{
    return statechartGroupConfigurations;
}

QList<QString> StatechartGroup::getProxies() const
{
    return proxies;
}

bool StatechartGroup::contextGenerationEnabled() const
{
    return generateContext;
}

void StatechartGroup::setContextGeneration(bool generateContext)
{
    this->generateContext = generateContext;
}

bool StatechartGroup::existsCMakeLists() const
{
    return std::filesystem::exists(getBoostGroupPath() / "CMakeLists.txt");
}

void StatechartGroup::setRootNode(StateTreeNodePtr rootNode)
{
    this->rootNode = rootNode;
}

armarx::StateTreeNodePtr armarx::StatechartGroup::findNodeByStateName(QString name)
{
    return rootNode->findNodeByStateName(name);
}

QVector<StateTreeNodePtr> StatechartGroup::getAllNodes() const
{
    QVector<StateTreeNodePtr> result = rootNode->getAllSubNodesRecursively();
    result.push_front(rootNode);
    return result;
}

QVector<statechartmodel::StatePtr> StatechartGroup::getAllStates(bool localOnly) const
{
    return rootNode->getAllStatesRecursively(localOnly);
}

QString StatechartGroup::getPackageName() const
{
    return packageName;
}

QString StatechartGroup::getPackagePath() const
{
    CMakePackageFinder finder(getPackageName().toStdString());
    return QString::fromStdString(finder.getPackageDir());
}

void StatechartGroup::setDescription(const QString& description)
{
    this->description = description;
}

void StatechartGroup::setProxies(const QList<QString> proxies)
{
    this->proxies = proxies;
}

void StatechartGroup::setConfigurations(const QMap<QString, QString>& statechartGroupConfigurations)
{
    this->statechartGroupConfigurations = statechartGroupConfigurations;
}

StatechartGroup::WriteAccess StatechartGroup::getWriteAccess()
{
    return writable;
}

