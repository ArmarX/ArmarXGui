/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "StatechartGroupDefs.h"
#include "StatechartGroup.h"

#include "../../StatechartViewerPlugin/model/State.h"

#include <QVariant>

#include <memory>
#include <filesystem>

namespace armarx
{

    class StateTreeModel;
    class StateTreeNode
        : public std::enable_shared_from_this<StateTreeNode>
    {
        friend class StateTreeModel;
    public:
        enum NodeType { Group, Folder, State };
        enum class Path
        {
            Absolute, RelativeToGroup
        };

        StateTreeNode(QString display, QString basename, NodeType nodeType, StateTreeNodePtr parentNode, StatechartGroupPtr parentGroup, bool stateIsPublic);
        ~StateTreeNode();

        void appendChild(StateTreeNodePtr child);
        void sortChildren();
        void removeChild(StateTreeNodePtr child);

        StateTreeNodePtr child(int row);
        int childCount() const;
        int columnCount() const;
        QString getDisplay() const;
        int row() const;
        StateTreeNodePtr getParent() const;
        StatechartGroupPtr getGroup();
        QList<StateTreeNodePtr> getChildren() const;
        bool isPublic();
        void setPublic(bool isPublic);

        NodeType getNodeType();

        QString getBasename() const;
        std::filesystem::path getBoostPath(std::filesystem::path parentPath) const;
        std::filesystem::path getBoostPathRelativeToGroup() const;
        std::filesystem::path getAbsoluteBoostPath() const;
        std::filesystem::path getBoostXmlFilePath(Path pathType = Path::Absolute) const;
        std::filesystem::path getBoostCppFilePath(Path pathType = Path::Absolute) const;
        std::filesystem::path getBoostHFilePath(Path pathType = Path::Absolute) const;
        std::filesystem::path getBoostGeneratedHFilePath(Path pathType = Path::Absolute) const;
        bool getCppExists() const;
        bool checkCppExists();

        armarx::statechartmodel::StatePtr getState() const;
        //void setState(const armarx::statechartmodel::StatePtr &value);

        bool isState() const;
        bool isFolderOrGroup() const;
        bool isGroup() const;
        bool isFolder() const;
        StateTreeNodePtr getClosestParentFolderOrGroupNode();

        StateTreeNodePtr findNodeByStateName(QString name);
        QVector<StateTreeNodePtr> getAllSubNodesRecursively() const;
        QVector<statechartmodel::StatePtr> getAllStatesRecursively(bool localOnly) const;

        int getNestingLevelRelativeToGroup();

        static bool CheckNodeName(QString name);

    private:
        static int NodeTypeToSortIndex(NodeType nodeType);
        static bool CompareChildren(const StateTreeNodePtr& a, const StateTreeNodePtr& b);
        void GetAllSubstatesRecursively(const statechartmodel::StatePtr& state, QVector<statechartmodel::StatePtr>& result, bool localOnly) const;

        QList<StateTreeNodePtr> children;
        QList<QVariant> itemData;
        std::weak_ptr<StateTreeNode> parentNode;
        std::weak_ptr<StatechartGroup> parentGroup;
        NodeType nodeType;
        QString basename;
        QString display;
        armarx::statechartmodel::StatePtr state;
        bool stateIsPublic;
        bool cppExists;
    };

}

