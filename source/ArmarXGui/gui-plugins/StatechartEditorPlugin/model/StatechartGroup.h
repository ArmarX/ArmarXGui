/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <memory>
#include <QString>
#include <QList>
#include "StatechartGroupDefs.h"
#include <filesystem>

#include <ArmarXGui/gui-plugins/StatechartViewerPlugin/model/State.h>

namespace armarx
{

    class StatechartGroup
    {
    public:
        enum WriteAccess { eReadOnly = 0, eWritable = 1 };
        StatechartGroup(QString definitionFilePath, QString groupPath, QString name, QString description, QString packageName, const QList<QString>& proxies, bool generateContext, const QMap<QString, QString>& statechartGroupConfigurations, WriteAccess writable);
        StateTreeNodePtr getRootNode();

        QString getName() const;
        QString getDefinitionFilePath() const;
        QString getGroupPath() const;
        std::filesystem::path getBoostGroupPath() const;
        std::filesystem::path getBoostDefinitionFilePath() const;
        QString getDescription() const;
        const QMap<QString, QString>& getConfigurations() const;
        QList<QString> getProxies() const;
        bool contextGenerationEnabled() const;
        void setContextGeneration(bool generateContext);
        bool existsCMakeLists() const;
        void setRootNode(StateTreeNodePtr rootNode);
        StateTreeNodePtr findNodeByStateName(QString name);
        QVector<StateTreeNodePtr> getAllNodes() const;
        QVector<statechartmodel::StatePtr> getAllStates(bool localOnly = true) const;

        QString getPackageName() const;
        QString getPackagePath() const;

        void setDescription(const QString& description);
        void setProxies(const QList<QString> proxies);
        void setConfigurations(const QMap<QString, QString>& statechartGroupConfigurations);
        WriteAccess getWriteAccess();

    private:
        QString definitionFilePath;
        QString groupPath;
        QString name;
        QString description;
        QString packageName;
        StateTreeNodePtr rootNode;
        QList<QString> proxies;
        bool generateContext;
        WriteAccess writable;
        QMap<QString, QString> statechartGroupConfigurations;
    };

}
