/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "StateTreeNode.h"

#include <boost/regex.hpp>

using namespace armarx;

StateTreeNode::StateTreeNode(QString display, QString basename, NodeType nodeType, StateTreeNodePtr parentNode, StatechartGroupPtr parentGroup, bool stateIsPublic)
{
    this->display = display;
    this->basename = basename;
    this->nodeType = nodeType;
    this->parentNode = parentNode;
    this->parentGroup = parentGroup;
    this->stateIsPublic = stateIsPublic;
    this->cppExists = false;
    checkCppExists();
}

StateTreeNode::~StateTreeNode()
{
    //qDeleteAll(children);
}

void StateTreeNode::appendChild(StateTreeNodePtr child)
{
    children.append(child);
}

int StateTreeNode::NodeTypeToSortIndex(NodeType nodeType)
{
    switch (nodeType)
    {
        case Group:
            return 0;

        case Folder:
            return 1;

        case State:
            return 2;

        default:
            return 999;
    }
}

bool StateTreeNode::CompareChildren(const StateTreeNodePtr& a, const StateTreeNodePtr& b)
{
    int aTypeIndex = NodeTypeToSortIndex(a->nodeType);
    int bTypeIndex = NodeTypeToSortIndex(b->nodeType);

    if (aTypeIndex != bTypeIndex)
    {
        return aTypeIndex < bTypeIndex;
    }

    return a->display.toLower().localeAwareCompare(b->display.toLower()) < 0;
}

void StateTreeNode::sortChildren()
{
    qStableSort(children.begin(), children.end(), CompareChildren);
}

void StateTreeNode::removeChild(StateTreeNodePtr child)
{
    children.removeOne(child);
}

StateTreeNodePtr StateTreeNode::child(int row)
{
    return children.value(row);
}

int StateTreeNode::childCount() const
{
    return children.count();
}

int StateTreeNode::columnCount() const
{
    return 1;
}

QString StateTreeNode::getDisplay() const
{
    return display;
}

int StateTreeNode::row() const
{
    StateTreeNodePtr parent = parentNode.lock();

    if (parent)
    {
        //return parentNode->children.indexOf(const_cast<StatechartGroupTreeNode*>(this));

        //NOT WORKING:
        //return parent->children.indexOf(const_cast<StatechartGroupTreeNodePtr>(shared_from_this()));
        for (int i = 0; i < parent->children.count(); i++)
        {
            if (parent->children.at(i).get() == this)
            {
                return i;
            }
        }

        return -1;
    }

    return 0;
}

StateTreeNodePtr StateTreeNode::getParent() const
{
    return parentNode.lock();
}

StatechartGroupPtr StateTreeNode::getGroup()
{
    return parentGroup.lock();
}

StateTreeNode::NodeType StateTreeNode::getNodeType()
{
    return nodeType;
}
QString StateTreeNode::getBasename() const
{
    return basename;
}

std::filesystem::path StateTreeNode::getAbsoluteBoostPath() const
{
    return parentGroup.lock()->getBoostGroupPath() / getBoostPathRelativeToGroup();
}

std::filesystem::path StateTreeNode::getBoostXmlFilePath(StateTreeNode::Path pathType) const
{
    if (!state)
    {
        return std::filesystem::path();
    }

    return (pathType == Path::Absolute) ? getAbsoluteBoostPath() : getBoostPathRelativeToGroup();
}

std::filesystem::path StateTreeNode::getBoostCppFilePath(StateTreeNode::Path pathType) const
{
    if (!state)
    {
        return std::filesystem::path();
    }

    std::string filename = std::string(state->getStateName().toUtf8().data()) + ".cpp";
    auto path = (pathType == Path::Absolute) ? getAbsoluteBoostPath() : getBoostPathRelativeToGroup();
    return (path.remove_filename() / filename);
}

std::filesystem::path StateTreeNode::getBoostHFilePath(StateTreeNode::Path pathType) const
{
    if (!state)
    {
        return std::filesystem::path();
    }

    std::string filename = std::string(state->getStateName().toUtf8().data()) + ".h";
    auto path = (pathType == Path::Absolute) ? getAbsoluteBoostPath() : getBoostPathRelativeToGroup();
    return (path.remove_filename() / filename);
}

std::filesystem::path StateTreeNode::getBoostGeneratedHFilePath(StateTreeNode::Path pathType) const
{
    if (!state)
    {
        return std::filesystem::path();
    }

    std::string filename = std::string(state->getStateName().toUtf8().data()) + ".generated.h";
    auto path = (pathType == Path::Absolute) ? getAbsoluteBoostPath() : getBoostPathRelativeToGroup();
    return (path.remove_filename() / filename);
}

bool StateTreeNode::getCppExists() const
{
    if (!state)
    {
        return false;
    }

    return cppExists;
}

bool StateTreeNode::checkCppExists()
{
    if (!state)
    {
        return false;
    }

    cppExists = std::filesystem::exists(getBoostCppFilePath());
    return cppExists;
}

std::filesystem::path StateTreeNode::getBoostPath(std::filesystem::path parentPath) const
{
    return parentPath / basename.toUtf8().data();
}

std::filesystem::path StateTreeNode::getBoostPathRelativeToGroup() const
{
    std::filesystem::path result = basename.toUtf8().data();
    StateTreeNodePtr p = getParent();

    while (p && !p->isGroup())
    {
        result = std::filesystem::path(p->getBasename().toUtf8().data()) / result;
        p = p->getParent();
    }

    return result;
}
armarx::statechartmodel::StatePtr StateTreeNode::getState() const
{
    return state;
}


bool StateTreeNode::isState() const
{
    return nodeType == State;
}

bool StateTreeNode::isFolderOrGroup() const
{
    return nodeType == Folder || nodeType == Group;
}

bool StateTreeNode::isGroup() const
{
    return nodeType == Group;
}

bool StateTreeNode::isFolder() const
{
    return nodeType == Folder;
}

StateTreeNodePtr StateTreeNode::getClosestParentFolderOrGroupNode()
{
    StateTreeNodePtr node = shared_from_this();

    if (isState())
    {
        node = node->getParent();
    }

    ARMARX_CHECK_EXPRESSION(node->isFolderOrGroup()) << "State Tree is broken.";
    return node;
}

StateTreeNodePtr StateTreeNode::findNodeByStateName(QString name)
{
    if (state && state->getStateName() == name)
    {
        return shared_from_this();
    }

    for (int i = 0; i < children.count(); i++)
    {
        StateTreeNodePtr match = children.at(i)->findNodeByStateName(name);

        if (match)
        {
            return match;
        }
    }

    return StateTreeNodePtr();
}

QVector<StateTreeNodePtr> StateTreeNode::getAllSubNodesRecursively() const
{
    QVector<StateTreeNodePtr> result;

    for (const StateTreeNodePtr& node : children)
    {
        result.push_back(node);

        for (const auto& n : node->getAllSubNodesRecursively())
        {
            result.push_back(n);
        }
    }

    return result;
}

QVector<statechartmodel::StatePtr> StateTreeNode::getAllStatesRecursively(bool localOnly) const
{
    QVector<statechartmodel::StatePtr> result;

    if (state)
    {
        result.push_back(state);
        GetAllSubstatesRecursively(state, result, localOnly);
    }
    else
    {
        for (const auto& child : children)
        {
            for (const auto& state : child->getAllStatesRecursively(localOnly))
            {
                result.push_back(state);
            }
        }
    }

    return result;
}

void StateTreeNode::GetAllSubstatesRecursively(const statechartmodel::StatePtr& state, QVector<statechartmodel::StatePtr>& result, bool localOnly) const
{
    for (const auto& substate : state->getSubstates())
    {
        const auto& stateClass = substate->getStateClass();

        if (stateClass && (!localOnly || substate->getType() != eRemoteState))
        {
            result.push_back(stateClass);
            GetAllSubstatesRecursively(stateClass, result, localOnly);
        }
    }
}

int StateTreeNode::getNestingLevelRelativeToGroup()
{
    if (isState() || isFolder())
    {
        return parentNode.lock()->getNestingLevelRelativeToGroup() + 1;
    }

    return 0;
}

bool StateTreeNode::CheckNodeName(QString name)
{
    std::string str = name.toUtf8().data();
    boost::regex nameRegex("[a-zA-Z][a-zA-Z0-9]*");
    boost::smatch what;
    return boost::regex_match(str, what, nameRegex);
}



QList<StateTreeNodePtr> StateTreeNode::getChildren() const
{
    return children;
}

bool StateTreeNode::isPublic()
{
    return stateIsPublic;
}

void StateTreeNode::setPublic(bool isPublic)
{
    stateIsPublic = isPublic;
}



