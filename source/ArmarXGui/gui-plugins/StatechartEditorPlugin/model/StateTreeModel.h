/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "StateTreeNode.h"
#include "StatechartGroup.h"

#include "../io/XmlReader.h"

#include <ArmarXCore/observers/variant/VariantInfo.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>

#include <QMap>
#include <QProgressDialog>

#include <filesystem>
#include <mutex>

namespace armarx
{

    class StateTreeModel;
    using StateTreeModelPtr = std::shared_ptr<StateTreeModel>;

    class StateTreeModel : public QObject
    {
        Q_OBJECT

    public:
        StateTreeModel(Ice::CommunicatorPtr ic, VariantInfoPtr variantInfo);

        void loadAllStates();
        void loadGroupStates(StatechartGroupPtr group);
        void annotateStateRerefences(const QList<statechartmodel::StatePtr>& statesToAnnotate);
        void annotateAllStateRerefences();

        void loadNode(StateTreeNodePtr node, std::filesystem::path parentPath, QList<statechartmodel::StatePtr>& allTheReaders);
        StatechartGroupPtr loadGroup(QString groupDefinitionFile, StateTreeNodePtr rootNode);
        void clear();
        void addGroup(StatechartGroupPtr group);

        //StatechartGroupTreeNodePtr createNewState(QString name, StatechartGroupTreeNodePtr parent);
        void notifyNewStateTreeNode(StateTreeNodePtr node);
        void notifyDeleteStateTreeNode(StateTreeNodePtr node);

        void saveAll(QPointer<QProgressDialog> progressDialog = NULL);
        void saveNode(StateTreeNodePtr node, std::filesystem::path parentPath);
        void generateAllBaseClasses(QPointer<QProgressDialog> progressDialog = NULL);
        void generateBaseClasses(const StatechartGroupPtr& group);
        void generateBaseClass(StateTreeNodePtr node, std::filesystem::path buildDir, VariantInfoPtr variantInfo, std::vector<std::string> proxies);
        void writeIfChanged(std::string targetPath, std::string contents);

        StateTreeNodePtr getRootNode() const;

        QList<StatechartGroupPtr> getGroups() const;
        StatechartGroupPtr getGroupByName(const QString& groupName) const;
        StateTreeNodePtr getNodeByState(statechartmodel::StatePtr state) const;
        StateTreeNodePtr getNodeByUuid(const QString& uuid) const;

        void setNodeState(StateTreeNodePtr node, statechartmodel::StatePtr state);
        QList<statechartmodel::StateInstancePtr> findStateUsages(StateTreeNodePtr node);
        bool groupExists(StatechartGroupPtr group);
        bool groupExists(std::filesystem::path groupPath);

        void generateGroupDocString(StatechartGroupPtr group, QString doxyGroupPath);
        const CMakePackageFinder& getPackageFinder(const QString& packageName);
        void loadVariantInfos(const std::string& packageName);
    private slots:
        void notifySubstateChanged(statechartmodel::StateInstancePtr substate, statechartmodel::SignalType signalType);
        void showWarning(QString title, QString message);

    signals:
        void stateAddedOrRemoved();

    private:
        QMap<QString, StateTreeNodePtr> uuidToNodeMap;
        QList<StatechartGroupPtr> groups;
        Ice::CommunicatorPtr iceCommunicator;
        StateTreeNodePtr rootNode;
        VariantInfoPtr variantInfo;
        std::mutex findersMutex;
        std::map<QString, CMakePackageFinder> finders;
        std::set<std::string> getVariantOfStatesWithNoCpp(const StatechartGroupPtr& group);
    };
}

