/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "StateTreeController.h"

#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/editorfileopener.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXWidgetController.h>


#include "io/GroupXmlWriter.h"
#include "io/XmlWriter.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/statechart/xmlstates/XMLStateComponent.h>
#include <ArmarXCore/statechart/xmlstates/baseclassgenerator/XmlStateBaseClassGenerator.h>
#include "io/GroupXmlReader.h"
#include "io/GroupXmlWriter.h"
#include "../StatechartViewerPlugin/model/DynamicRemoteStateClass.h"
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/view/dialogs/CloneGroupDialog.h>
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/view/dialogs/RenameGroupDialog.h>
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/view/dialogs/RenameStateDialog.h>
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/view/dialogs/EditStatechartGroupDialog.h>
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/renaming/StateRenamer.h>
#include <QIcon>
#include <QInputDialog>
#include <QMessageBox>
#include <QtGui>
#include <QProgressDialog>
#include <QMenu>
#include <QFileDialog>
#include <QLabel>
#include <QDialogButtonBox>
#include <QCheckBox>
#include <QBoxLayout>

#include <filesystem>

using namespace armarx;
using namespace statechartio;
using namespace statechartmodel;

StateTreeController::StateTreeController(Ice::CommunicatorPtr ic, VariantInfoPtr variantInfo, QList<QVariant> headerInfo, QTreeView* treeView, QLineEdit* filterLineEdit, const ArmarXPackageToolInterfacePtr& packageTool, const StatechartProfilesPtr& statechartProfiles, StatechartProfilePtr currentProfile, QObject* parent)
    : QAbstractItemModel(parent)
{
    qRegisterMetaType<StateTreeNodePtr>("StateTreeNodePtr");
    CMakePackageFinder finder("ArmarXCore");
    this->appForDrag.reset(
        new ScenarioManager::Data_Structure::Application(
            "XMLRemoteStateOfferer",
            finder.getBinaryDir(),
            "ArmarXCore"
        ));
    this->variantInfo = variantInfo;
    this->profiles = statechartProfiles;
    this->currentProfile = currentProfile;
    this->packageTool = packageTool;
    this->headerInfo = headerInfo;
    this->treeView = treeView;
    this->stateTreeModel.reset(new StateTreeModel(ic, variantInfo));
    this->rootNode = this->stateTreeModel->getRootNode();
    this->groupCloner = std::make_shared<GroupCloner>(stateTreeModel, packageTool);
    this->groupRenamer = std::make_shared<GroupRenamer>(groupCloner);
    this->communicator = ic;


    proxyModel = new InfixFilterModel(this);
    proxyModel->setSourceModel(this);
    proxyModel->setFilterCaseSensitivity(Qt::CaseInsensitive);
    treeView->setModel(proxyModel);


    contextMenuGroup = new QMenu(treeView);
    contextMenuFolder = new QMenu(treeView);
    contextMenuState = new QMenu(treeView);
    treeView->setContextMenuPolicy(Qt::CustomContextMenu);

    actionDeleteNode = new QAction(QIcon(":/icons/delete.ico"), "Delete", treeView);
    actionMakeStatePublic = new QAction("Public State", treeView);
    actionMakeStatePublic->setCheckable(true);
    actionNewFolder = new QAction(QIcon(":/icons/folder-new.svg"), "Add new Folder", treeView);
    actionNewStateDefinition = new QAction(QIcon(":/icons/images/add-state.png"), "Add new State Definition", treeView);
    actionNewDynamicRemoteStateDefinition = new QAction("Add new Dynamic Remote State Definition", treeView);
    actionFindStateUsages = new QAction(QIcon(":/icons/search.svg"), "Find State Usages", treeView);
    actionRenameState = new QAction(QIcon(":/icons/rename.svg"), "Rename State", treeView);
    actionOpenCMakeProject = new QAction(QIcon(":/icons/qt-small.png"), "Open CMake Project", treeView);
    actionShowFullPath = new QAction(QIcon(":/icons/search.svg"), "Show Full Path", treeView);
    actionOpenStateCPP = new QAction(QIcon(":/icons/cpp.svg"), "Open C++ Code", treeView);
    actionExecuteGroup = new QAction(QIcon(":/icons/run.svg"), "Execute Statechart Group", treeView);
    //actionGenerateStateBaseClass = new QAction(QIcon(":/icons/document-properties.svg"), "Generate Base Class", treeView);
    //actionGenerateRsoBaseClass = new QAction(QIcon(":/icons/document-properties.svg"), "Generate RSO Base Class", treeView);
    actionGenerateStateCPP = new QAction(QIcon(":/icons/document-properties.svg"), "Generate State C++ Files", treeView);
    actionRenameGroup = new QAction(QIcon(":/icons/rename.svg"), "Rename Group", treeView);
    actionCloneGroup = new QAction(QIcon(":/icons/clone.svg"), "Clone Group", treeView);
    actionGroupProperties = new QAction(QIcon(":/icons/document-properties.svg"), "Group Properties", treeView);

    QList<QAction*> groupActions, folderActions, stateActions;

    groupActions.append(actionNewStateDefinition);
    groupActions.append(actionNewDynamicRemoteStateDefinition);
    groupActions.append(actionNewFolder);
    groupActions.append(actionDeleteNode);
    groupActions.append(actionOpenCMakeProject);
    groupActions.append(actionShowFullPath);
    groupActions.append(actionExecuteGroup);
    //groupActions.append(actionGenerateRsoBaseClass);
    groupActions.append(actionRenameGroup);
    groupActions.append(actionCloneGroup);
    groupActions.append(actionGroupProperties);

    folderActions.append(actionNewStateDefinition);
    folderActions.append(actionNewDynamicRemoteStateDefinition);
    folderActions.append(actionNewFolder);
    folderActions.append(actionDeleteNode);

    stateActions.append(actionMakeStatePublic);
    stateActions.append(actionFindStateUsages);
    stateActions.append(actionRenameState);
    stateActions.append(actionDeleteNode);
    stateActions.append(actionOpenStateCPP);
    //stateActions.append(actionGenerateStateBaseClass);
    stateActions.append(actionGenerateStateCPP);

    for (QAction* a : groupActions)
    {
        a->setIconVisibleInMenu(true);
    }

    for (QAction* a : folderActions)
    {
        a->setIconVisibleInMenu(true);
    }

    for (QAction* a : stateActions)
    {
        a->setIconVisibleInMenu(true);
    }

    contextMenuGroup->addActions(groupActions);
    contextMenuFolder->addActions(folderActions);
    contextMenuState->addActions(stateActions);


    connect(stateTreeModel.get(), SIGNAL(stateAddedOrRemoved()), SLOT(stateAddedOrRemoved()));
    connect(treeView, SIGNAL(customContextMenuRequested(QPoint)), SLOT(onContextMenu(QPoint)));

    connect(actionNewStateDefinition, SIGNAL(triggered()), SLOT(onNewStateDefinition()));
    connect(actionNewDynamicRemoteStateDefinition, SIGNAL(triggered()), SLOT(onNewDynamicRemoteStateDefinition()));
    connect(actionDeleteNode, SIGNAL(triggered()), SLOT(onDeleteNode()));
    connect(actionMakeStatePublic, SIGNAL(triggered(bool)), SLOT(onMakeStatePublic(bool)));
    connect(actionNewFolder, SIGNAL(triggered()), SLOT(onNewFolder()));
    connect(actionFindStateUsages, SIGNAL(triggered()), SLOT(onFindStateUsages()));
    connect(actionRenameState, SIGNAL(triggered()), SLOT(onRenameState()));
    connect(actionOpenCMakeProject, SIGNAL(triggered()), this, SLOT(onOpenCMakeProject()));
    connect(actionShowFullPath, SIGNAL(triggered()), this, SLOT(onShowFullPath()));
    connect(actionOpenStateCPP, SIGNAL(triggered()), this, SLOT(onOpenStateCPP()));
    connect(actionExecuteGroup, SIGNAL(triggered()), this, SLOT(onExecuteGroup()));
    //connect(actionGenerateStateBaseClass, SIGNAL(triggered()), this, SLOT(onGenerateStateBaseClass()));
    connect(actionGenerateStateCPP, SIGNAL(triggered()), this, SLOT(onGenerateStateCppFiles()));
    //connect(actionGenerateRsoBaseClass, SIGNAL(triggered()), this, SLOT(onGenerateRsoBaseClass()));
    connect(actionRenameGroup, SIGNAL(triggered()), this, SLOT(onRenameGroup()));
    connect(actionCloneGroup, SIGNAL(triggered()), this, SLOT(onCloneGroup()));
    connect(actionGroupProperties, SIGNAL(triggered()), this, SLOT(onGroupProperties()));

    connect(filterLineEdit, SIGNAL(textChanged(QString)), proxyModel, SLOT(setFilterFixedString(QString)));
    connect(filterLineEdit, SIGNAL(textChanged(QString)), this, SLOT(expandFilterResults(QString)));
}

StateTreeController::~StateTreeController()
{
    //delete rootNode;
    ARMARX_INFO << "Killing " << processes.size() << " statechart processes";
    for (auto& process : processes)
    {
        ARMARX_INFO << "stopping " << process->pid();
        process-> terminate();
    }
    for (auto& process : processes)
    {
        ARMARX_INFO << "Waiting for process " << process->pid();
        process->waitForFinished(2000);
    }
    for (auto& process : processes)
    {
        if (process->pid() > 0)
        {
            ARMARX_INFO << "killing " << process->pid();
            process->kill();
        }
    }

}

QVariant StateTreeController::data(const QModelIndex& index, int role) const
{
    if (!index.isValid())
    {
        return QVariant();
    }

    StateTreeNodePtr node = getNode(index); //static_cast<StateTreeNode*>(index.internalPointer());

    switch (role)
    {
        case Qt::DisplayRole:
            if (node->getNodeType() == StateTreeNode::State)
            {
                return node->getDisplay()
                       + (node->getState() ? "" : " [no .xml]")
                       + (node->getCppExists() ? "" : " [no .cpp]")
                       + (node->isPublic() ? " (public)" : "")
                       + (node->getGroup()->getWriteAccess() == StatechartGroup::eReadOnly ? " (read-only)" : "");
            }
            else
            {
                return node->getDisplay();
            }

        case Qt::DecorationRole:
            if (node->getNodeType() == StateTreeNode::Folder || node->getNodeType() == StateTreeNode::Group)
            {
                if (node->getGroup()->getWriteAccess() == StatechartGroup::eReadOnly)
                {
                    return getIcon(":/icons/folder-locked.svg");
                }
                if (treeView->isExpanded(index))
                {
                    return getIcon(":/icons/folder-open.svg");
                }
                else
                {
                    return getIcon(":/icons/folder.svg");
                }
            }
            else if (node->getNodeType() == StateTreeNode::State)
            {
                if (node->getState())
                {
                    if (node->getState()->getSubstates().count() > 0)
                    {
                        return getIcon(node->isPublic() ? ":/statechart-editor/states-public.svg" : ":/statechart-editor/states.svg");
                    }
                    else
                    {
                        return getIcon(node->isPublic() ? ":/statechart-editor/state-public.svg" : ":/statechart-editor/state.svg");
                    }
                }
                else
                {
                    return getIcon(":/statechart-editor/state-broken.svg");
                }
            }

            return QVariant();

        case Qt::ToolTipRole:

            return QString::fromStdString(node->getAbsoluteBoostPath().string());

        default:
            return QVariant();
    }

}

Qt::ItemFlags StateTreeController::flags(const QModelIndex& index) const
{
    if (!index.isValid())
    {
        return 0;
    }

    Qt::ItemFlags flags;
    StateTreeNodePtr node = getNode(index);
    flags = Qt::ItemIsEnabled | Qt::ItemIsSelectable;

    if (node)
    {
        if (node->getState() || node->isGroup())
        {
            flags |= Qt::ItemIsDragEnabled;
        }
    }

    return flags;
}

QVariant StateTreeController::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
    {
        return headerInfo.value(section);
    }

    return QVariant();
}

QModelIndex StateTreeController::index(int row, int column, const QModelIndex& parentIndex) const
{
    if (!hasIndex(row, column, parentIndex))
    {
        return QModelIndex();
    }

    StateTreeNode* parentNode;

    if (!parentIndex.isValid())
    {
        parentNode = this->rootNode.get();
    }
    else
    {
        parentNode = static_cast<StateTreeNode*>(parentIndex.internalPointer());
    }

    StateTreeNode* childNode = parentNode->child(row).get();

    if (childNode)
    {
        return createIndex(row, column, childNode);
    }
    else
    {
        return QModelIndex();
    }
}

QModelIndex StateTreeController::parent(const QModelIndex& index) const
{
    if (!index.isValid())
    {
        return QModelIndex();
    }

    StateTreeNode* childNode = static_cast<StateTreeNode*>(index.internalPointer());
    StateTreeNodePtr parentNode = childNode->getParent();

    if (parentNode == rootNode)
    {
        return QModelIndex();
    }

    return createIndex(parentNode->row(), 0, parentNode.get());
}

int StateTreeController::rowCount(const QModelIndex& parent) const
{
    //StatechartGroupTreeNode *parentNode;
    if (parent.column() > 0)
    {
        return 0;
    }

    if (!parent.isValid())
    {
        return rootNode->childCount();
    }
    else
    {
        ARMARX_CHECK_EXPRESSION(parent.internalPointer()) << "The internal pointer of a tree node is zero";
        return static_cast<StateTreeNode*>(parent.internalPointer())->childCount();
    }

    //return parentNode->childCount();
}

int StateTreeController::columnCount(const QModelIndex& parent) const
{
    if (parent.isValid())
    {
        return static_cast<StateTreeNode*>(parent.internalPointer())->columnCount();
    }
    else
    {
        return rootNode->columnCount();
    }
}

StatechartGroupPtr StateTreeController::loadAndAddGroup(QString groupDefinitionFile)
{
    bool load = !groupDefinitionFile.isEmpty();
    if (!load)
    {
        QFileDialog selectGroupFile(treeView, "Open Statechart Group");
        //        selectFolder.setOption(QFileDialog::ShowDirsOnly, true);
        selectGroupFile.setOption(QFileDialog::ReadOnly, true);
        selectGroupFile.setOption(QFileDialog::HideNameFilterDetails, false);
        selectGroupFile.setFileMode(QFileDialog::ExistingFile);
        QList<QUrl> urls;
        urls << QUrl::fromLocalFile(QDesktopServices::storageLocation(QDesktopServices::HomeLocation))
             << QUrl::fromLocalFile(QDesktopServices::storageLocation(QDesktopServices::DesktopLocation));

        if (!ArmarXDataPath::getHomePath().empty())
        {
            urls << QUrl::fromLocalFile(QString::fromStdString(ArmarXDataPath::getHomePath()));
        }

        selectGroupFile.setSidebarUrls(urls);
        QStringList filters;
        filters << "Statechart Group (*.scgxml)";
        filters << "All Files(*.*)";
        selectGroupFile.setNameFilters(filters);
        load = selectGroupFile.exec() == QDialog::Accepted;
        if (load && groupDefinitionFile.isEmpty())
        {
            groupDefinitionFile = selectGroupFile.selectedFiles().first();
        }
    }

    StatechartGroupPtr group;
    if (load)
    {
        try
        {
            group = loadGroup(groupDefinitionFile);

            if (!group)
            {
                return group;
            }

            try
            {
                stateTreeModel->loadGroupStates(group);
            }
            catch (std::exception& e)
            {
                QString reason(QString::fromStdString(e.what()));
                reason.truncate(200);
                ARMARX_WARNING_S << "Exception caught: " << e.what();
                QMessageBox::warning(0, "Error while loading States", QString("An error while loading states - Reason:\n" + reason));
            }
        }
        catch (std::exception& e)
        {
            QString reason = QString::fromStdString(e.what());
            reason.truncate(200);
            QMessageBox::warning(0, "Error while loading Statechart Group", QString("An error while loading the statechart group file '") + groupDefinitionFile + "' - Reason:\n" + reason);
        }
    }

    return group;
}

StatechartGroupPtr StateTreeController::loadGroup(QString groupDefinitionFile)
{
    //StatechartGroupTreeNodePtr root = treeController->getRoot();
    //beginInsertRows(getIndex(rootNode), rowCount(), rowCount());

    this->layoutAboutToBeChanged();
    StatechartGroupPtr group = stateTreeModel->loadGroup(groupDefinitionFile, rootNode);

    if (!group)
    {
        return group;
    }

    rootNode->appendChild(group->getRootNode());
    rootNode->sortChildren();
    //endInsertRows();
    this->layoutChanged();

    StateTreeNodePtr groupNode = group->getRootNode();
    treeView->expand(getIndex(groupNode));
    treeView->setCurrentIndex(getIndex(groupNode));

    return group;
}

StatechartGroupPtr StateTreeController::addNewGroup(const QString& groupName, const QString& groupPath, const QString& description, const QString& packageName, const QList<QString> proxies, bool generateContext, const QMap<QString, QString>& statechartGroupConfigurations)
{
    beginInsertRows(getIndex(rootNode), rowCount(), rowCount());
    std::filesystem::path defPath = groupPath.toUtf8().data();
    defPath /= (groupName + ".scgxml").toUtf8().data();
    StatechartGroupPtr group(new StatechartGroup(QString::fromUtf8(defPath.c_str()), groupPath, groupName, description, packageName, proxies, generateContext, statechartGroupConfigurations, StatechartGroup::eWritable));
    StateTreeNodePtr groupRootNode(new StateTreeNode(group->getName(), "", StateTreeNode::Group, rootNode, group, false));
    group->setRootNode(groupRootNode);

    stateTreeModel->addGroup(group);
    rootNode->appendChild(group->getRootNode());
    endInsertRows();
    rootNode->sortChildren();

    //    emit dataChanged(getIndex(group->getRootNode()),getIndex(group->getRootNode()));

    QMetaObject::invokeMethod(this, "selectNode", Qt::QueuedConnection, Q_ARG(StateTreeNodePtr, group->getRootNode()));

    return group;
}

void StateTreeController::loadAllStates()
{
    stateTreeModel->loadAllStates();
}

StateTreeNodePtr StateTreeController::getNodeByState(statechartmodel::StatePtr state)
{
    return stateTreeModel->getNodeByState(state);
}

StateTreeNodePtr StateTreeController::getSelectedNode()
{
    QModelIndex selected = treeView->currentIndex();

    if (!selected.isValid())
    {
        return StateTreeNodePtr();
    }

    return getNode(selected);
}

StateTreeNodePtr StateTreeController::getClosestParentFolderOrGroupNode(StateTreeNodePtr node)
{
    return node ? node->getClosestParentFolderOrGroupNode() : StateTreeNodePtr();
}

bool StateTreeController::nodeIsState(StateTreeNodePtr node)
{
    return node ? node->getNodeType() == StateTreeNode::State : false;
}

bool StateTreeController::nodeIsFolder(StateTreeNodePtr node)
{
    return node ? node->getNodeType() == StateTreeNode::Folder : false;
}

bool StateTreeController::nodeIsGroup(StateTreeNodePtr node)
{
    return node ? node->getNodeType() == StateTreeNode::Group : false;
}

bool StateTreeController::nodeIsFolderOrGroup(StateTreeNodePtr node)
{
    return node ? node->getNodeType() == StateTreeNode::Folder || node->getNodeType() == StateTreeNode::Group : false;
}

bool StateTreeController::selectedNodeIsState()
{
    return nodeIsState(getSelectedNode());
}

bool StateTreeController::selectedNodeIsFolder()
{
    return nodeIsFolder(getSelectedNode());
}

bool StateTreeController::selectedNodeIsGroup()
{
    return nodeIsGroup(getSelectedNode());
}

bool StateTreeController::selectedNodeIsFolderOrGroup()
{
    return nodeIsFolderOrGroup(getSelectedNode());
}

StateTreeNodePtr StateTreeController::tryCreateNewState(QString name)
{
    StateTreeNodePtr node = getSelectedNode();

    if (!nodeIsFolderOrGroup(node))
    {
        return StateTreeNodePtr();
    }

    return createNewState(name, node);
}

void StateTreeController::tryDeleteState()
{
    StateTreeNodePtr node = getSelectedNode();

    if (!nodeIsState(node))
    {
        return;
    }

    QList<StateInstancePtr> instances = stateTreeModel->findStateUsages(node);

    if (instances.count() > 0)
    {
        QString message = QString("Cannot delete state, it is still being used by %1 state(s):\n%2")
                          .arg(instances.count()).arg(buildStateUsageString(instances));
        QMessageBox::warning(NULL, "Cannot delete State", message);
        return;
    }

    if (node->getState())
    {
        QString question = QString("You are about to delete the State Definition '%1'.\nDo you also want to permanently remove the underlying file %2?")
                           .arg(node->getDisplay(), node->getBasename());
        QMessageBox::StandardButton reply = QMessageBox::question(NULL, "Delete State Definition File?", question, QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);

        if (reply == QMessageBox::Cancel)
        {
            return;
        }
        else if (reply == QMessageBox::Yes)
        {
            std::filesystem::remove(node->getAbsoluteBoostPath());
            if (node->getCppExists())
            {
                std::filesystem::remove(node->getBoostCppFilePath());
                std::filesystem::remove(node->getBoostHFilePath());
                std::filesystem::remove(node->getBoostGeneratedHFilePath());
            }
        }
    }
    else
    {
        QString question = QString("Remove State Definition '%1'?").arg(node->getDisplay());
        QMessageBox::StandardButton reply = QMessageBox::question(NULL, "Delete State Definition?", question, QMessageBox::Ok | QMessageBox::Cancel);

        if (reply == QMessageBox::Cancel)
        {
            return;
        }
    }

    removeState(node);
    GroupXmlWriter::WriteXml(node->getGroup(), QString::fromUtf8((node->getGroup()->getBoostDefinitionFilePath()).c_str()));
}

StateTreeNodePtr StateTreeController::createNewState(QString name, StateTreeNodePtr parent, bool createDynamicRemoteState)
{
    this->layoutAboutToBeChanged();

    statechartmodel::StatePtr state;

    if (!createDynamicRemoteState)
    {
        state.reset(new statechartmodel::State());
        state->addEndSubstate("Success", "Success");
        state->addEndSubstate("Failure", "Failure");
        statechartmodel::EventPtr evt(new statechartmodel::Event());
        evt->name = "Failure";
        evt->description = "Event for statechart-internal failures or optionally user-code failures";
        state->setOutgoingEvents(EventList {evt});
    }
    else
    {
        state.reset(new statechartmodel::DynamicRemoteStateClass());
    }


    state->setStateName(name);
    state->setEditable(parent->getGroup()->getWriteAccess() == StatechartGroup::eWritable);
    StateTreeNodePtr node(new StateTreeNode(name, name + ".xml", StateTreeNode::State, parent, parent->getGroup(), false));
    stateTreeModel->setNodeState(node, state);
    parent->appendChild(node);
    parent->sortChildren();
    stateTreeModel->notifyNewStateTreeNode(node);
    this->layoutChanged();
    treeView->sortByColumn(0, Qt::AscendingOrder);

    QMetaObject::invokeMethod(this, "selectNode", Qt::QueuedConnection, Q_ARG(StateTreeNodePtr, node));

    return node;
}

StateTreeNodePtr StateTreeController::createNewFolder(QString name, StateTreeNodePtr parent)
{
    this->layoutAboutToBeChanged();
    StateTreeNodePtr node(new StateTreeNode(name, name, StateTreeNode::Folder, parent, parent->getGroup(), false));
    parent->appendChild(node);
    parent->sortChildren();

    this->layoutChanged();

    treeView->expand(getIndex(parent));
    treeView->setCurrentIndex(getIndex(node));

    return node;
}

StateTreeNodePtr StateTreeController::getNode(QModelIndex index) const
{
    if (!index.isValid())
    {
        return rootNode;
    }
    else
    {
        if (index.model() == proxyModel)
        {
            index = proxyModel->mapToSource(index);
        }
        return static_cast<StateTreeNode*>(index.internalPointer())->shared_from_this();
    }
}

void StateTreeController::selectNodeByState(statechartmodel::StatePtr state)
{
    StateTreeNodePtr node = stateTreeModel->getNodeByState(state);

    if (node)
    {
        treeView->setCurrentIndex(proxyModel->mapFromSource(getIndex(node)));
    }
}

void StateTreeController::onContextMenu(const QPoint& point)
{
    QModelIndex index = treeView->indexAt(point);
    //    index = this->index(index.row(), index.column(), index.parent());
    //    if(!index.isValid())
    //    {
    //        return;
    //    }
    //    StateTreeNodePtr node = static_cast<StateTreeNode*>(index.internalPointer())->shared_from_this();
    StateTreeNodePtr node = getNode(index);

    // reset to initial state
    actionMakeStatePublic->setEnabled(true);
    actionRenameState->setEnabled(true);
    actionRenameGroup->setEnabled(true);
    actionGenerateStateCPP->setEnabled(true);
    actionNewFolder->setEnabled(true);
    actionNewStateDefinition->setEnabled(true);
    actionNewDynamicRemoteStateDefinition->setEnabled(true);
    actionOpenCMakeProject->setEnabled(true);
    actionOpenStateCPP->setEnabled(true);
    actionGroupProperties->setEnabled(true);
    actionDeleteNode->setEnabled(true);

    auto applyReadOnlyStatusToMenu = [&, this]()
    {

        bool writable = node->getGroup() && node->getGroup()->getWriteAccess() == StatechartGroup::eWritable;

        if (!writable)
        {
            actionMakeStatePublic->setEnabled(writable);
            actionRenameState->setEnabled(writable);
            actionRenameGroup->setEnabled(writable);
            actionGenerateStateCPP->setEnabled(writable);
            actionNewFolder->setEnabled(writable);
            actionNewStateDefinition->setEnabled(writable);
            actionNewDynamicRemoteStateDefinition->setEnabled(writable);
            actionOpenCMakeProject->setEnabled(writable);
            actionOpenStateCPP->setEnabled(writable);
            actionGroupProperties->setEnabled(writable);
            actionDeleteNode->setEnabled(writable);
        }
    };


    if (node->isState())
    {
        actionMakeStatePublic->setEnabled(node->getState() != NULL);
        actionMakeStatePublic->setChecked(node->isPublic());
        actionFindStateUsages->setEnabled(node->getState() != NULL);
        actionRenameState->setEnabled(node->isState());
        actionGenerateStateCPP->setEnabled(!node->getCppExists() && node->getState()->getType() == eNormalState);
        actionOpenStateCPP->setEnabled(node->getCppExists());
        applyReadOnlyStatusToMenu();
        contextMenuState->exec(treeView->mapToGlobal(point));
    }
    else if (node->isGroup())
    {
        // @@@ Buggy implementation: if process is stopped while context menu is open a new process is started. Better: use 2 different context menu entries and 2 differend handler functions.
        QMap<QString, QProcess*>::const_iterator it;

        if (node->getGroup() // @@@ check not needed, every node has a group
            && (it = processes.find(node->getGroup()->getGroupPath())) != processes.end()
            && it.value()->state() != QProcess::NotRunning)
        {
            actionExecuteGroup->setText("Stop and Execute Statechart Group");
        }
        else
        {
            actionExecuteGroup->setText("Execute Statechart Group");
        }
        applyReadOnlyStatusToMenu();
        contextMenuGroup->exec(treeView->mapToGlobal(point));
    }
    else if (node->isFolder() && node != rootNode)
    {
        applyReadOnlyStatusToMenu();
        contextMenuFolder->exec(treeView->mapToGlobal(point));
    }


}

struct StateCreationDialog : public QDialog
{
    QCheckBox* checkbox;
    QLabel* label;
    QLineEdit* editField;
    QDialogButtonBox* buttonBox;
    StateCreationDialog(QWidget* parent = 0, Qt::WindowFlags flags = 0) : QDialog(parent, flags)
    {
        label = new QLabel("Enter the state name", this);
        editField = new QLineEdit(this);
        QRegExp rx("^([a-zA-Z_]{1})([a-zA-Z_0-9]+)$");
        QValidator* validator = new QRegExpValidator(rx, this);
        editField->setValidator(validator);
        setWindowTitle("New State Definition");
        resize(250, 120);
        checkbox = new QCheckBox("Create C++ Files", this);
        QBoxLayout* layout = new QBoxLayout(QBoxLayout::TopToBottom, this);
        buttonBox = new QDialogButtonBox(this);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel | QDialogButtonBox::Ok);
        layout->addWidget(label);
        layout->addWidget(editField);
        layout->addWidget(checkbox);
        layout->addWidget(buttonBox);
        QObject::connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

    }
};

bool StateTreeController::retrieveNewStateName(QString& newStateName, StateTreeNodePtr& node, bool& createCPPFiles, bool askForCPPFiles)
{
    node = getSelectedNode();

    if (!node)
    {
        QMessageBox::warning(NULL, "Nothing selected", "Error: No state, group or folder selected.");
        return false;
    }

    node = node->getClosestParentFolderOrGroupNode();

    StateCreationDialog dialog;
    dialog.checkbox->setEnabled(askForCPPFiles);

    if (dialog.exec() == QDialog::Rejected)
    {
        return false;
    }

    newStateName = dialog.editField->text();
    createCPPFiles = dialog.checkbox->isChecked();

    if (!StateTreeNode::CheckNodeName(newStateName))
    {
        QMessageBox::warning(NULL, "Invalid name", QString("Error: Provided name '%1' does not meet requirements: Has to start with a letter, followed by letters and digits. Sorry.").arg(newStateName));
        return false;
    }

    if (node->getGroup()->findNodeByStateName(newStateName))
    {
        QMessageBox::warning(NULL, "Duplicate State Name", QString("Error: State with name '%1' exists already. Sorry.").arg(newStateName));
        return false;
    }

    if (node->getGroup()->getName() == newStateName)
    {
        QMessageBox::warning(NULL, "Name Conflict", QString("Error: State cannot have same name as Group. Sorry."));
        return false;
    }

    return true;
}

QIcon StateTreeController::getIcon(const QString& path) const
{
    if (!icons.contains(path))
    {
        icons[path] = QIcon(path);
    }
    return icons[path];
}

void StateTreeController::onNewStateDefinition()
{
    bool createCPPFiles;
    StateTreeNodePtr node;
    QString newStateName;

    if (!retrieveNewStateName(newStateName, node, createCPPFiles, true))
    {
        return;
    }


    StateTreeNodePtr newState = createNewState(newStateName, node);

    ARMARX_CHECK_EXPRESSION(packageTool);

    if (createCPPFiles)
    {
        if (!packageTool->addFullXmlStatechart(newState->getGroup()->getName().toUtf8().data(),
                                               newState->getBoostPathRelativeToGroup().replace_extension().string(),
                                               newState->getGroup()->getPackagePath().toUtf8().data()))
        {
            ARMARX_WARNING_S << "Creating source files for state " << newStateName << " failed";
        }
    }
    else
    {
        if (!packageTool->addXmlOnlyXmlStatechart(newState->getGroup()->getName().toUtf8().data(),
                newState->getBoostPathRelativeToGroup().replace_extension().string(),
                newState->getGroup()->getPackagePath().toUtf8().data()))
        {
            ARMARX_WARNING_S << "Creating source files for state " << newStateName << " failed";
        }
    }

    newState->checkCppExists();
    saveAll();

}

void StateTreeController::onNewDynamicRemoteStateDefinition()
{
    bool createCPPFiles;
    StateTreeNodePtr node;
    QString newStateName;

    if (!retrieveNewStateName(newStateName, node, createCPPFiles, false))
    {
        return;
    }


    createNewState(newStateName, node, true);
    saveAll();
}

void StateTreeController::onNewFolder()
{
    StateTreeNodePtr node = getSelectedNode();

    if (!node)
    {
        QMessageBox::warning(NULL, "Nothing selected", "Error: No state, group or folder selected.");
        return;
    }

    if (!nodeIsFolderOrGroup(node))
    {
        QMessageBox::warning(NULL, "Internal error", "Internal error.");
        return;
    }

    bool ok;
    QString name = QInputDialog::getText(NULL, QString("New Folder"), QString("Enter name"), QLineEdit::Normal, QString(), &ok);

    if (!ok)
    {
        return;
    }

    StateTreeNodePtr newFolderNode = createNewFolder(name, node);
    std::filesystem::create_directory(newFolderNode->getAbsoluteBoostPath());

}

void StateTreeController::onDeleteNode()
{
    if (selectedNodeIsState())
    {
        tryDeleteState();
        return;
    }

    if (selectedNodeIsFolder())
    {
        tryDeleteFolder();
        return;
    }

    if (selectedNodeIsGroup())
    {
        tryDeleteGroup();
        return;
    }

    QMessageBox msgBox;
    msgBox.setText("Error: No state selected.");
    msgBox.exec();
    return;
}

void StateTreeController::onMakeStatePublic(bool enable)
{
    getSelectedNode()->setPublic(enable);
    treeView->repaint();
}



void StateTreeController::onFindStateUsages()
{
    QList<StateInstancePtr> instances = stateTreeModel->findStateUsages(getSelectedNode());
    QMessageBox msgBox;
    QString message("Usage count: ");
    message += QString::number(instances.count());
    message += "\n" + buildStateUsageString(instances);

    msgBox.setText(message);
    msgBox.exec();
}

void StateTreeController::onRenameState()
{
    auto node = getSelectedNode();

    if (node && node->isState())
    {
        statechartmodel::StatePtr state = node->getState();
        StatechartGroupPtr group = stateTreeModel->getNodeByState(state)->getGroup();

        RenameStateDialog d(stateTreeModel, group, state);

        if (d.exec() == QDialog::Accepted)
        {
            if (d.isSaveAllRequested())
            {
                ARMARX_VERBOSE_S << "Saving all changes first...";
                saveAll();
            }

            ARMARX_VERBOSE_S << "closing tabs";
            emit closeAllTabsRequested();

            ARMARX_VERBOSE_S << "removing groups from tree view";
            auto groups = stateTreeModel->getGroups();
            stateTreeModel->clear();

            for (const auto& c : rootNode->getChildren())
            {
                if (c->isGroup())
                {
                    removeNodeFromTree(c);
                }
            }

            if (StateRenamer::RenameState(d.getInstanceRenameInfos(), node, d.getNewStateName(), group))
            {
                for (const StatechartGroupPtr& g : groups)
                {
                    ARMARX_VERBOSE_S << "reloading " << g->getName();
                    auto loadedGroup = loadAndAddGroup(g->getDefinitionFilePath());

                    if (loadedGroup->getDefinitionFilePath() == group->getDefinitionFilePath())
                    {
                        stateTreeModel->generateBaseClasses(loadedGroup);
                    }
                }
            }
            else
            {
                ARMARX_WARNING_S << "Renaming state '" << state->getStateName() << "' to '" << d.getNewStateName() << "' failed";
            }
        }
    }
}

void StateTreeController::onOpenCMakeProject()
{
    StateTreeNodePtr node = getSelectedNode();

    if (!node)
    {
        return;
    }

    ARMARX_INFO_S << node->getBoostPathRelativeToGroup().string() << " group path: " << node->getGroup()->getGroupPath().toStdString();
    EditorFileOpener opener;
    opener.openFile("qtcreatorProject", node->getGroup()->getPackagePath().toStdString() + "/CMakeLists.txt");
}

void StateTreeController::onShowFullPath()
{
    StateTreeNodePtr node = getSelectedNode();

    if (!node)
    {
        return;
    }

    QMessageBox::information(treeView, "Path for Group " + node->getGroup()->getName(), node->getGroup()->getGroupPath());
}

void StateTreeController::onOpenStateCPP()
{
    openStateCPP(getSelectedNode());
}

void StateTreeController::onOpenGroup(QString groupFile)
{
    loadAndAddGroup(groupFile);
}



void StateTreeController::stopGroupExecution(StatechartGroupPtr group)
{
    if (processes.find(group->getGroupPath()) != processes.end())
    {
        processes[group->getGroupPath()]->terminate();
        processes[group->getGroupPath()]->waitForFinished(1000);
        processes[group->getGroupPath()]->kill();
        processes.remove(group->getGroupPath());
    }
}

void StateTreeController::executeGroup(StatechartGroupPtr group, QString startState)
{
    stopGroupExecution(group);

    if (processes.find(group->getGroupPath()) == processes.end())
    {
        processes[group->getGroupPath()] = new QProcess(this);
    }


    QProcess* p = processes[group->getGroupPath()];
    connect(p, SIGNAL(finished(int)), this, SLOT(onStatechartFinished(int)));
    CMakePackageFinder core("ArmarXCore");
    QStringList args;
    if (!startState.isEmpty())
    {
        args << "--ArmarX.XMLStateComponent.StatesToEnter=" + startState;
    }
    args << "--ArmarX.XMLStateComponent.XMLStatechartGroupDefinitionFile=" + group->getDefinitionFilePath();
    args << "--ArmarX.XMLStateComponent.XMLStatechartProfile=" + QString::fromStdString(currentProfile->getName());
    args << "--ArmarX.XMLStateComponent.ObjectName=" + group->getName() + "StateComponent";
    args << "--ArmarX.ApplicationName=" + group->getName() + "StateComponentApp";
    QString path = QString::fromStdString(core.getBinaryDir()) + "/XMLRemoteStateOffererRun";
    ARMARX_INFO_S << path << " " << args.join(" ").toStdString();

    std::string username;
    if (getenv("USER"))
    {
        username = getenv("USER");
    }
    std::string tempLogDir = (std::filesystem::temp_directory_path() / ("armarx-" + username) / group->getName().toStdString()).string();
    if (!std::filesystem::exists(tempLogDir) && !std::filesystem::create_directories(tempLogDir))
    {
        ARMARX_WARNING_S << "Could not create log dirs for executing group - aborting";
        return;
    }
    p->setStandardOutputFile(QString::fromStdString(tempLogDir) + "/out.log");
    p->setStandardErrorFile(QString::fromStdString(tempLogDir) + "/err.log");
    p->setProcessChannelMode(QProcess::MergedChannels);
    p->start(path, args);
    connect(p, SIGNAL(readyReadStandardOutput()), this, SLOT(processOutputReady()));
    connect(p, SIGNAL(readyReadStandardError()), this, SLOT(processErrorsReady()));
}

void StateTreeController::executeGroupWithDependencies(StatechartGroupPtr group, QString startState)
{
    auto deps = GroupCloner(stateTreeModel, packageTool).getGroupDependencies(group, false);
    executeGroup(group, startState);
    for (StatechartGroupPtr dep : deps)
    {
        executeGroup(dep);
    }
}

void StateTreeController::stopGroupExecutionWithDependencies(StatechartGroupPtr group)
{
    auto deps = GroupCloner(stateTreeModel, packageTool).getGroupDependencies(group, false);
    deps.push_back(group);

    for (StatechartGroupPtr dep : deps)
    {
        if (processes.find(dep->getGroupPath()) != processes.end())
        {
            processes[dep->getGroupPath()]->terminate();
        }
    }
    auto startTime = IceUtil::Time::now();
    for (StatechartGroupPtr dep : deps)
    {
        if (processes.find(dep->getGroupPath()) != processes.end())
        {
            auto timeLeftToWait = std::max<int>(0, 1000 - (IceUtil::Time::now() - startTime).toMilliSeconds());
            processes[dep->getGroupPath()]->waitForFinished(timeLeftToWait);
            processes[dep->getGroupPath()]->kill();
            processes.remove(dep->getGroupPath());
        }
    }
}

void StateTreeController::onExecuteGroup()
{
    StateTreeNodePtr node = getSelectedNode();

    if (!node)
    {
        return;
    }

    QStringList states;
    QString noneState("---");
    states << noneState ;
    for (StateTreeNodePtr curnode : node->getGroup()->getAllNodes())
    {
        if (curnode->isPublic() && curnode->isState())
        {
            states << curnode->getState()->getStateName();
        }
    }
    bool ok;
    QString startState = QInputDialog::getItem(0, "State", "Select the state to start or none", states, 0, false, &ok);
    if (!ok)
    {
        return;
    }
    if (startState == noneState)
    {
        startState = "";
    }
    StatechartGroupPtr group = node->getGroup();
    executeGroupWithDependencies(group, startState);


    //    p->start(QString::fromStdString(core.getBinaryDir()) + "/startApplication.sh", args);
    //    sleep(2);
    //    QString p_stdout = p->readAllStandardOutput();
    //    QString p_stderr = p->readAllStandardError();
    //    ARMARX_IMPORTANT_S << "Errors:" << p_stderr;
    //    ARMARX_IMPORTANT_S << "Output: " << p_stdout;

}

void StateTreeController::onStatechartFinished(int exitCode)
{
    auto process = qobject_cast<QProcess*>(sender());
    if (process)
    {
        ARMARX_INFO_S << process->pid() <<  " exited with code " << exitCode;
    }
    else
    {
        ARMARX_INFO_S << "Exited with code " << exitCode;
    }
}
/*
void StateTreeController::onGenerateStateBaseClass()
{
    StateTreeNodePtr node = getSelectedNode();

    XmlWriter writer;
    writer.serialize(node->getState());
    std::string xml = writer.getXmlString(false).toUtf8().data();
    std::vector<std::string> namespaces;
    namespaces.push_back("armarx");
    namespaces.push_back(node->getGroup()->getName().toUtf8().data());

    std::vector<std::string> proxies;
    for(QString p : node->getGroup()->getProxies())
    {
        proxies.push_back(p.toUtf8().data());
    }

    std::string cpp = XmlStateBaseClassGenerator::GenerateCpp(namespaces, RapidXmlReader::FromXmlString(xml), proxies, node->getGroup()->contextGenerationEnabled(), node->getGroup()->getName().toUtf8().data(), variantInfo);
    //QMessageBox::information(NULL, "test", QString::fromUtf8(cpp.c_str()));

    std::string targetPath(node->getBoostCppFilePath().replace_extension().string() + ".generated.h");
    bool identical = false;
    if(std::filesystem::exists(std::filesystem::path(targetPath)))
    {
        std::string currentContents = RapidXmlReader::ReadFileContents(targetPath);
        identical = cpp == currentContents;
    }
    if(!identical)
    {
        QString qtargetPath = QString::fromUtf8(targetPath.data());
        //QMessageBox::information(NULL, "test", qtargetPath);
        ARMARX_LOG_S << "Generated file " << targetPath;
        GroupXmlWriter::WriteFileContents(qtargetPath, QString::fromUtf8(cpp.data()));
    }
    else
    {
        ARMARX_LOG_S << "Skipping identical file " << targetPath;
        QMessageBox::information(NULL, "test", "current file and generated output are identical.");
    }

}

void StateTreeController::onGenerateRsoBaseClass()
{

}*/

void StateTreeController::onGenerateStateCppFiles()
{
    StateTreeNodePtr node = getSelectedNode();

    if (!node || !node->isState())
    {
        QMessageBox::warning(NULL, "No State selected", "Error: No state selected.");
        return;
    }

    if (!packageTool->addCppOnlyXmlStatechart(node->getGroup()->getName().toUtf8().data(),
            node->getBoostPathRelativeToGroup().replace_extension().string(),
            node->getGroup()->getPackagePath().toUtf8().data()))
    {
        ARMARX_WARNING_S << "Creating source files for state " << node->getBasename() << " failed:\n" << packageTool->getLastOutput();
    }

    node->checkCppExists();
}

void StateTreeController::onCloneGroup()
{
    StateTreeNodePtr node = getSelectedNode();

    if (node && node->isGroup())
    {
        StatechartGroupPtr group = node->getGroup();

        CloneGroupDialog d(packageTool, group, groupCloner);

        if (d.exec() == QDialog::Accepted)
        {
            for (const auto& g : d.getGroupsToClone())
            {
                if (!g.first->existsCMakeLists())
                {
                    QMessageBox::warning(0, "Cloning failed",
                                         g.first->getName()  + " at " + g.first->getGroupPath() + " contains no CMakeLists.txt - is it an installed package? Installed packages cannot be cloned.");
                    return;
                }
            }

            if (auto newMapping = groupCloner->cloneGroupsTo(d.getGroupsToClone(), d.getPackagePath(), d.getStatechartGroupMapping()))
            {
                ARMARX_INFO_S << "Cloning successful, creating/overwriting mapping file in package: " << d.getPackagePath();
                newMapping.value().writeToFile(d.getPackagePath());

                const QString packageName = QFileInfo(d.getPackagePath()).fileName();

                for (const QPair<StatechartGroupPtr, QString>& g : d.getGroupsToClone())
                {
                    QDir groupDir(d.getPackagePath());
                    groupDir.cd("source");
                    groupDir.cd(packageName);
                    groupDir.cd("statecharts");
                    groupDir.cd(g.second);
                    QString newDefFile = groupDir.path() + QDir::separator() + g.second + ".scgxml";
                    stateTreeModel->generateBaseClasses(loadAndAddGroup(newDefFile));
                }
            }
        }
    }
}

void StateTreeController::onRenameGroup()
{
    StateTreeNodePtr node = getSelectedNode();

    if (node && node->isGroup())
    {
        StatechartGroupPtr group = node->getGroup();
        RenameGroupDialog d(rootNode, stateTreeModel, group, groupRenamer);

        if (d.exec() == QDialog::Accepted)
        {
            if (d.isSaveAllRequested())
            {
                ARMARX_VERBOSE_S << "Saving all changes first...";
                saveAll();
            }

            if (auto newGroupDirPath = groupRenamer->renameGroup(group, d.getNewName(), d.getDependantGroups()))
            {
                QString newGroupDefinitionPath = *newGroupDirPath + QDir::separator() + d.getNewName() + ".scgxml";

                ARMARX_VERBOSE_S << "closing tabs";
                emit closeAllTabsRequested();

                ARMARX_VERBOSE_S << "removing groups from tree view";
                stateTreeModel->clear();

                for (const auto& c : rootNode->getChildren())
                {
                    if (c->isGroup())
                    {
                        removeNodeFromTree(c);
                    }
                }

                for (const StatechartGroupPtr& g : d.getAllGroups())
                {
                    if (g->getGroupPath() == group->getGroupPath())
                    {
                        continue;
                    }

                    ARMARX_VERBOSE_S << "reloading " << g->getName();
                    onOpenGroup(g->getDefinitionFilePath());
                }

                onOpenGroup(newGroupDefinitionPath);
            }
        }
    }
}

void StateTreeController::onGroupProperties()
{
    StateTreeNodePtr node = getSelectedNode();

    if (node && node->isGroup())
    {
        StatechartGroupPtr group = node->getGroup();
        EditStatechartGroupDialog d(EditStatechartGroupDialog::EditGroup, group->getName(), packageTool, variantInfo,
                                    group->getProxies(), group->contextGenerationEnabled(), profiles, group->getConfigurations(), group->getDescription(), group);

        if (d.exec() == QDialog::Accepted)
        {
            group->setDescription(d.getGroupDescription());
            group->setProxies(d.getProxies());
            group->setContextGeneration(d.contextGenerationEnabled());
            group->setConfigurations(d.getConfigurations());
        }
    }
}

void StateTreeController::collapseAll()
{
    treeView->collapseAll();
}

void StateTreeController::selectNode(StateTreeNodePtr node)
{

    treeView->expand(proxyModel->mapFromSource(getIndex(node->getParent())));
    treeView->setCurrentIndex(proxyModel->mapFromSource(getIndex(node)));
}

void StateTreeController::openStateCPP(StateTreeNodePtr node)
{
    if (!node->getCppExists())
    {
        QMessageBox::warning(NULL, "File missing", QString("File '") + node->getBoostCppFilePath().c_str() + "' not found.");
    }
    else
    {
        opener.openFileWithDefaultEditor(node->getBoostCppFilePath().c_str());
    }
}

void StateTreeController::processOutputReady()
{
    QProcess* p = qobject_cast<QProcess*>(sender());

    if (p)
    {
        std::cout << QString(p->readAllStandardOutput());
    }
    else
    {
        ARMARX_WARNING_S << "cast failed";
    }
}

void StateTreeController::processErrorsReady()
{
    QProcess* p = qobject_cast<QProcess*>(sender());

    if (p)
    {
        std::cerr << QString(p->readAllStandardError());
    }
    else
    {
        ARMARX_WARNING_S << "cast failed";
    }

}

void StateTreeController::stateAddedOrRemoved()
{
    std::cout << "StateTreeController::stateAddedOrRemoved()" << std::endl;
    treeView->repaint();
}

void StateTreeController::expandFilterResults(const QString& filterString)
{
    if (filterString.length() == 0)
    {
        return;
    }

    InfixFilterModel::ExpandFilterResults(treeView);

}

QModelIndex StateTreeController::getIndex(StateTreeNodePtr node)
{
    if (!node || node == rootNode || !node->getParent())
    {
        return QModelIndex();
    }

    return createIndex(node->row(), 0, node.get());
}

QString StateTreeController::buildStateUsageString(QList<StateInstancePtr> instances)
{
    QString message;
    QListIterator<StateInstancePtr> i(instances);

    while (i.hasNext())
    {
        StateInstancePtr stateInstance = i.next();
        message += stateTreeModel->getNodeByState(stateInstance->getParent())->getGroup()->getName() + "/" + stateInstance->getParent()->getStateName() + ": '" + stateInstance->getInstanceName() + "'\n";
    }

    return message.trimmed();
}

void StateTreeController::removeState(StateTreeNodePtr stateNode)
{
    removeNodeFromTree(stateNode);
    stateTreeModel->notifyDeleteStateTreeNode(stateNode);
}

void StateTreeController::tryDeleteFolder()
{
    StateTreeNodePtr node = getSelectedNode();

    if (!nodeIsFolder(node))
    {
        return;
    }

    if (node->getChildren().count() > 0)
    {
        QMessageBox::warning(NULL, "Folder cannot be deleted.", "Folder cannot be deleted. It is not empty.");
        return;
    }

    try
    {
        std::filesystem::remove(node->getAbsoluteBoostPath());
        removeFolder(node);
        GroupXmlWriter::WriteXml(node->getGroup(), QString::fromUtf8((node->getGroup()->getBoostDefinitionFilePath()).c_str()));
    }
    catch (std::exception&)
    {
        QMessageBox::warning(NULL, "Folder cannot be deleted.", "Folder cannot be deleted. It is not empty. ");
    }

}

void StateTreeController::removeFolder(StateTreeNodePtr folderNode)
{
    removeNodeFromTree(folderNode);
}


void StateTreeController::tryDeleteGroup()
{
    StateTreeNodePtr node = getSelectedNode();

    if (!nodeIsGroup(node))
    {
        return;
    }

    auto statechartsCmakeListPath = QString::fromUtf8((node->getAbsoluteBoostPath().remove_filename().parent_path() / "CMakeLists.txt").c_str());
    auto filecontents = GuiStatechartGroupXmlReader::ReadFileContents(statechartsCmakeListPath);
    filecontents = filecontents.remove("add_subdirectory(" + node->getBasename() + ")");
    GroupXmlWriter::WriteFileContents(statechartsCmakeListPath, filecontents);

    std::filesystem::remove_all(node->getAbsoluteBoostPath().remove_filename());

    removeNodeFromTree(node);
}

void StateTreeController::removeNodeFromTree(StateTreeNodePtr node)
{
    int row = node->row();
    //    this->layoutAboutToBeChanged();
    this->beginRemoveRows(getIndex(node->getParent()), row, row);

    node->getParent()->removeChild(node);

    this->endRemoveRows();
    //    this->layoutChanged();
}

void StateTreeController::saveAll()
{
    {
        QProgressDialog d;
        //        d.show();
        d.setMinimumDuration(1000);
        stateTreeModel->saveAll(&d);
    }
    QProgressDialog d;
    //    d.show();
    stateTreeModel->generateAllBaseClasses(&d);
}

/*
StatechartGroupTreeNodePtr StateTreeController::getRoot()
{
    return rootNode;
}
*/


QStringList StateTreeController::mimeTypes() const
{
    QStringList result;
    result << "application/x-State";
    result << "application/pointer";
    return result;
}

QMimeData* StateTreeController::mimeData(const QModelIndexList& indexes) const
{
    if (indexes.size() == 0)
    {
        return new QMimeData();
    }

    StateTreeNodePtr node = getNode(indexes.first());

    if (node->isGroup()) // a group can be dragged into the scenario manager
    {
        QMimeData* mimeData = new QMimeData();
        QByteArray encodedData;

        QDataStream stream(&encodedData, QIODevice::WriteOnly);


        foreach (const QModelIndex& index, indexes)
        {
            if (index.isValid())
            {

                CMakePackageFinder package(node->getGroup()->getPackageName().toStdString());
                ARMARX_CHECK_EXPRESSION(package.packageFound()) << node->getGroup()->getPackageName().toStdString();
                ARMARX_CHECK_GREATER_EQUAL(package.getIncludePathList().size(), 1);
                auto prop = appForDrag->getProperties();
                auto relativeGroupFilePath = ArmarXDataPath::relativeTo(package.getIncludePathList()[0], node->getGroup()->getBoostDefinitionFilePath().string());
                prop->getProperties()->setProperty("ArmarX.XMLStateComponent.XMLStatechartGroupDefinitionFile",
                                                   relativeGroupFilePath);
                prop->getProperties()->setProperty("ArmarX.XMLStateComponent.XMLStatechartProfile",
                                                   currentProfile->getName());
                prop->getProperties()->setProperty("ArmarX.XMLStateComponent.ObjectName",
                                                   node->getGroup()->getName().toStdString() + "XMLStateComponent");
                prop->getProperties()->setProperty("ArmarX.ApplicationName",
                                                   node->getGroup()->getName().toStdString() + "App");
                appForDrag->setProperties(prop);


                stream <<  reinterpret_cast<quint64>(appForDrag.get()) << node->getGroup()->getName() /*instanceName*/;
            }
        }


        mimeData->setData("application/pointer", encodedData);
        return mimeData;
    }

    if (!node || !node->getState())
    {
        return new QMimeData();
    }


    StateMimeData* data = new StateMimeData(node->getState(), stateTreeModel);
    data->setProxyName(node->getGroup()->getName() + STATEOFFERERSUFFIX);
    return data;
}


StateTreeController::StateMimeData::StateMimeData(statechartmodel::StatePtr state, StateTreeModelPtr stateTreeModel) :
    AbstractStateMimeData(state),
    stateTreeModel(stateTreeModel)
{
}

bool StateTreeController::StateMimeData::isInSameGroup(statechartmodel::StatePtr state) const
{
    return stateTreeModel->getNodeByState(this->state)->getGroup() == stateTreeModel->getNodeByState(state)->getGroup();
}

bool StateTreeController::StateMimeData::isPublic() const
{
    return stateTreeModel->getNodeByState(this->state)->isPublic();
}

