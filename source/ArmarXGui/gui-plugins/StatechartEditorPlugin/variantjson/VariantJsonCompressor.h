/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "ParseResult.h"

#include <memory>
#include <ArmarXCore/observers/variant/VariantContainer.h>
#include <ArmarXCore/observers/variant/SingleTypeVariantList.h>
#include <ArmarXCore/observers/variant/StringValueMap.h>
#include <ArmarXGui/libraries/StructuralJson/JPathNavigator.h>
#include <ArmarXGui/libraries/StructuralJson/JsonData.h>

namespace armarx
{
    class VariantJsonCompressor;
    using VariantJsonCompressorPtr = std::shared_ptr<VariantJsonCompressor>;

    class VariantJsonCompressor
    {
    public:
        VariantJsonCompressor();
        static ParseResult CheckUserInput(const std::string& json, const std::string& variantBaseTypeName, const Ice::CommunicatorPtr& communicator);
        static ParseResult FormatUserInput(std::string& json, int indenting = 2);
        static std::string Compress(const std::string& json, const std::string& variantBaseTypeName, int indenting = 2);
        static JsonDataPtr CompressToJson(const std::string& json, const std::string& variantBaseTypeName);
        static std::string Decompress(const std::string& json, const std::string& variantBaseTypeName);
        static std::string DecompressBasicVariant(const std::string& value, const std::string& variantBaseTypeName);

    private:
        static JsonDataPtr Compress(const armarx::JPathNavigator& nav, const ContainerTypePtr& containerInfo);
        static JsonDataPtr Decompress(const JsonDataPtr& json, const ContainerTypePtr& containerInfo);
        static ParseResult CheckUserInput(const JsonDataPtr& json, const ContainerTypePtr& containerInfo, const Ice::CommunicatorPtr& communicator);
    };
}

