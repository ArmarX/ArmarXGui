
SET(LIBS ${LIBS} StatechartEditorGuiPlugin)

armarx_add_test(XmlReaderTest XmlReaderTest.cpp "${LIBS}")
armarx_add_test(XmlWriterTest XmlWriterTest.cpp "${LIBS}")
armarx_add_test(VariantJsonTest VariantJsonTest.cpp "${LIBS}")
