/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "GroupXmlReader.h"

#include <ArmarXCore/core/exceptions/local/FileIOException.h>

#include <QFile>
#include <QFileInfo>
#include <QTextStream>

#include <filesystem>
#include <boost/regex.hpp>

#include <string>
#include <fstream>
#include <streambuf>
#include <stdexcept>


using namespace armarx;
using namespace rapidxml;


GuiStatechartGroupXmlReader::GuiStatechartGroupXmlReader()
{
}

StatechartGroupPtr GuiStatechartGroupXmlReader::ReadXml(QString groupDefinitionFile, StateTreeNodePtr virtualRoot)
{
    RapidXmlReaderPtr wrapper = RapidXmlReader::FromFile(std::string(groupDefinitionFile.toUtf8().data()));
    RapidXmlReaderNode xmlGroupNode = wrapper->getRoot("StatechartGroup");
    QString name = QString::fromUtf8(xmlGroupNode.attribute_value("name").c_str());
    QString description = QString::fromUtf8(xmlGroupNode.first_node_value_or_default("Description", "").c_str());
    QString packageName = QString::fromUtf8(xmlGroupNode.attribute_value("package").c_str());

    QString displayName = name;
    boost::regex projectNameRegex("(\\w+)/statecharts/\\w+/?$");
    boost::match_results<std::string::const_iterator> match;
    std::filesystem::path groupPath(groupDefinitionFile.toUtf8().data());
    const std::string groupPathStr(groupPath.parent_path().c_str());
    StatechartGroup::WriteAccess writable = QFileInfo(QString::fromStdString(groupPath.string())).permissions() & QFile::WriteUser ? StatechartGroup::eWritable : StatechartGroup::eReadOnly;


    if (boost::regex_search(groupPathStr.begin(), groupPathStr.end(), match, projectNameRegex))
    {
        displayName += QString(" [%1]").arg(QString::fromStdString(std::string(match[1].first, match[1].second)));
    }

    QList<QString> proxies;

    for (RapidXmlReaderNode proxyNode : xmlGroupNode.nodes("Proxies", "Proxy"))
    {
        proxies.append(QString::fromUtf8(proxyNode.attribute_value("value").c_str()));
    }

    QMap<QString, QString> statechartGroupConfigurations;
    for (RapidXmlReaderNode configNode : xmlGroupNode.nodes("Configurations", "Configuration"))
    {
        auto profileName = QString::fromUtf8(configNode.attribute_value("profileName").c_str());
        auto configuration = QString::fromUtf8(configNode.value().c_str());
        statechartGroupConfigurations[profileName] = configuration;
    }

    bool generateContext = xmlGroupNode.attribute_as_optional_bool("generateContext", "true", "false", false);

    StatechartGroupPtr group(new StatechartGroup(groupDefinitionFile, QString::fromUtf8(groupPath.parent_path().c_str()), name, description, packageName,
                             proxies, generateContext, statechartGroupConfigurations, writable));
    StateTreeNodePtr rootNode(new StateTreeNode(displayName, "", StateTreeNode::Group, virtualRoot, group, false));
    ReadChildren(xmlGroupNode, rootNode, group);
    group->setRootNode(rootNode);
    ARMARX_LOG_S << "Reading done: " << groupDefinitionFile.toStdString() << flush;
    return group;
}

QString GuiStatechartGroupXmlReader::ReadFileContents(QString path)
{
    QFile f(path);

    if (!f.open(QFile::ReadOnly | QFile::Text))
    {
        throw armarx::exceptions::local::FileOpenException(path.toUtf8().data());
    }

    QTextStream in(&f);
    return in.readAll();
}



void GuiStatechartGroupXmlReader::ReadChildren(RapidXmlReaderNode xmlNode, StateTreeNodePtr parentNode, StatechartGroupPtr group)
{
    for (RapidXmlReaderNode xmlFolderNode : xmlNode.nodes("Folder"))
    {
        QString basename = QString::fromUtf8(xmlFolderNode.attribute_value("basename").c_str());
        StateTreeNodePtr folderNode(new StateTreeNode(basename, basename, StateTreeNode::Folder, parentNode, group, false));
        parentNode->appendChild(folderNode);
        ReadChildren(xmlFolderNode, folderNode, group);
    }

    for (RapidXmlReaderNode xmlStateNode : xmlNode.nodes("State"))
    {
        QString filename = QString::fromUtf8(xmlStateNode.attribute_value("filename").c_str());
        bool stateIsPublic = xmlStateNode.attribute_as_optional_bool("visibility", "public", "private", false);
        StateTreeNodePtr stateNode(new StateTreeNode(filename, filename, StateTreeNode::State, parentNode, group, stateIsPublic));
        parentNode->appendChild(stateNode);
    }
}

