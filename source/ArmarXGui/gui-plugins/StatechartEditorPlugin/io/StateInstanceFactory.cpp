/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Christian Mandery (christian.mandery at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "StateInstanceFactory.h"
#include "XmlReader.h"

#include "../../StatechartViewerPlugin/model/stateinstance/EndState.h"
#include "../../StatechartViewerPlugin/model/stateinstance/LocalState.h"
#include "../../StatechartViewerPlugin/model/stateinstance/RemoteState.h"
#include "../../StatechartViewerPlugin/model/stateinstance/DynamicRemoteState.h"


using namespace armarx::exceptions::local;
using namespace armarx::statechartio;
using namespace armarx::statechartmodel;

// TODO: This function is copied from XmlReader. The code duplication will be resolved once we use the RapidXmlWrapper here.
QString readAttribute(rapidxml::xml_node<>* node, const QString& attributeName, bool required = true)
{
    rapidxml::xml_attribute<>* attribute = node->first_attribute(attributeName.toUtf8());

    if (!attribute)
    {
        if (required)
        {
            throw XmlReaderException("Attribute \"" + attributeName.toStdString() + "\" for \"" + node->name() + "\" node not found!");
        }
        else
        {
            return "";
        }
    }

    return QString::fromUtf8(attribute->value());
}

StateInstanceFactoryBase::SubClassRegistry EndStateInstanceFactory::registry(EndStateInstanceFactory::getName(), &EndStateInstanceFactory::createInstance);
StateInstanceFactoryBase::SubClassRegistry LocalStateInstanceFactory::registry(LocalStateInstanceFactory::getName(), &LocalStateInstanceFactory::createInstance);
StateInstanceFactoryBase::SubClassRegistry RemoteStateInstanceFactory::registry(RemoteStateInstanceFactory::getName(), &RemoteStateInstanceFactory::createInstance);
StateInstanceFactoryBase::SubClassRegistry DynamicRemoteStateInstanceFactory::registry(DynamicRemoteStateInstanceFactory::getName(), &DynamicRemoteStateInstanceFactory::createInstance);

StateInstanceFactoryBase::StateInstanceFactoryBase(rapidxml::xml_node<>* xmlNode, armarx::statechartmodel::StatePtr parentState) :
    xmlNode(xmlNode),
    parentState(parentState) {}

EndStateInstanceFactory::EndStateInstanceFactory(rapidxml::xml_node<>* xmlNode, armarx::statechartmodel::StatePtr parentState) :
    StateInstanceFactoryBase(xmlNode, parentState) {}

StateInstancePtr EndStateInstanceFactory::getStateInstance()
{
    QString instanceName = readAttribute(xmlNode, "name");
    QString eventName = readAttribute(xmlNode, "event");

    EndStatePtr endStateInstance(new EndState(instanceName, eventName, parentState));
    endStateInstance->setPosition(QPointF(readAttribute(xmlNode, "left").toFloat(), readAttribute(xmlNode, "top").toFloat()));
    endStateInstance->setBoundingBox(readAttribute(xmlNode, "boundingSquareSize").toFloat());

    return endStateInstance;
}

StateInstanceFactoryBasePtr EndStateInstanceFactory::createInstance(XmlParentPair xmlParentPair)
{
    StateInstanceFactoryBasePtr factoryPtr(new EndStateInstanceFactory(xmlParentPair.first, xmlParentPair.second));
    return factoryPtr;
}

LocalStateInstanceFactory::LocalStateInstanceFactory(rapidxml::xml_node<>* xmlNode, armarx::statechartmodel::StatePtr parentState) :
    StateInstanceFactoryBase(xmlNode, parentState) {}

StateInstancePtr LocalStateInstanceFactory::getStateInstance()
{
    QString uuid = readAttribute(xmlNode, "refuuid");
    QString instanceName = readAttribute(xmlNode, "name");

    LocalStatePtr localStateInstance(new LocalState(uuid, instanceName, parentState));
    localStateInstance->setPosition(QPointF(readAttribute(xmlNode, "left").toFloat(), readAttribute(xmlNode, "top").toFloat()));
    localStateInstance->setBoundingBox(readAttribute(xmlNode, "boundingSquareSize").toFloat());

    return localStateInstance;
}

StateInstanceFactoryBasePtr LocalStateInstanceFactory::createInstance(XmlParentPair xmlParentPair)
{
    StateInstanceFactoryBasePtr factoryPtr(new LocalStateInstanceFactory(xmlParentPair.first, xmlParentPair.second));
    return factoryPtr;
}

RemoteStateInstanceFactory::RemoteStateInstanceFactory(rapidxml::xml_node<>* xmlNode, armarx::statechartmodel::StatePtr parentState) :
    StateInstanceFactoryBase(xmlNode, parentState) {}

StateInstancePtr RemoteStateInstanceFactory::getStateInstance()
{
    QString uuid = readAttribute(xmlNode, "refuuid");
    QString instanceName = readAttribute(xmlNode, "name");

    statechartmodel::RemoteStatePtr remoteStateInstance(new statechartmodel::RemoteState(uuid, instanceName, parentState));
    remoteStateInstance->proxyName = readAttribute(xmlNode, "proxyName");
    remoteStateInstance->setPosition(QPointF(readAttribute(xmlNode, "left").toFloat(), readAttribute(xmlNode, "top").toFloat()));
    remoteStateInstance->setBoundingBox(readAttribute(xmlNode, "boundingSquareSize").toFloat());

    return remoteStateInstance;
}

StateInstanceFactoryBasePtr RemoteStateInstanceFactory::createInstance(XmlParentPair xmlParentPair)
{
    StateInstanceFactoryBasePtr factoryPtr(new RemoteStateInstanceFactory(xmlParentPair.first, xmlParentPair.second));
    return factoryPtr;
}


DynamicRemoteStateInstanceFactory::DynamicRemoteStateInstanceFactory(rapidxml::xml_node<>* xmlNode, armarx::statechartmodel::StatePtr parentState) :
    StateInstanceFactoryBase(xmlNode, parentState) {}

StateInstancePtr DynamicRemoteStateInstanceFactory::getStateInstance()
{
    QString instanceName = readAttribute(xmlNode, "name");
    QString uuid = readAttribute(xmlNode, "refuuid");
    DynamicRemoteStatePtr dynamicRemoteStateInstance(new statechartmodel::DynamicRemoteState(uuid, instanceName, parentState));
    dynamicRemoteStateInstance->setPosition(QPointF(readAttribute(xmlNode, "left").toFloat(), readAttribute(xmlNode, "top").toFloat()));
    dynamicRemoteStateInstance->setBoundingBox(readAttribute(xmlNode, "boundingSquareSize").toFloat());

    return dynamicRemoteStateInstance;
}

StateInstanceFactoryBasePtr DynamicRemoteStateInstanceFactory::createInstance(XmlParentPair xmlParentPair)
{
    StateInstanceFactoryBasePtr factoryPtr(new DynamicRemoteStateInstanceFactory(xmlParentPair.first, xmlParentPair.second));
    return factoryPtr;
}


