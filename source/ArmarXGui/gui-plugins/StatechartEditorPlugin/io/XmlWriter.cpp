/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Christian Mandery (christian.mandery at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "XmlWriter.h"

#include "../../StatechartViewerPlugin/model/State.h"
#include "../../StatechartViewerPlugin/model/stateinstance/EndState.h"
#include "../../StatechartViewerPlugin/model/stateinstance/LocalState.h"
#include "../../StatechartViewerPlugin/model/stateinstance/RemoteState.h"
#include "../../StatechartViewerPlugin/model/stateinstance/DynamicRemoteState.h"

#include <ArmarXCore/core/rapidxml/rapidxml_print.hpp>
#include <ArmarXCore/util/json/JSONObject.h>
#include <ArmarXCore/statechart/ParameterMapping.h>
#include <ArmarXCore/statechart/xmlstates/profiles/StatechartProfiles.h>

#include <SimoxUtility/algorithm/string/string_tools.h>

using namespace armarx::statechartio;
using namespace armarx::statechartmodel;

XmlWriter::XmlWriter(const armarx::VariantInfoPtr& variantInfo)
    : variantInfo(variantInfo)
{ }

void XmlWriter::serialize(StateCPtr state, const QMap<QString, StateTreeNodePtr>& uuidToNodeMap)
{
    document.clear();

    document.append_node(buildXmlDeclaration());

    rapidxml::xml_node<>* rootNode = document.allocate_node(rapidxml::node_element, "State");

    rootNode->append_attribute(document.allocate_attribute("version", "1.2"));
    rootNode->append_attribute(document.allocate_attribute("name", cloneQString(state->getStateName())));
    rootNode->append_attribute(document.allocate_attribute("uuid", cloneQString(state->getUUID())));
    rootNode->append_attribute(document.allocate_attribute("width", cloneQString(QString::number(state->getSize().width()))));
    rootNode->append_attribute(document.allocate_attribute("height", cloneQString(QString::number(state->getSize().height()))));
    rootNode->append_attribute(document.allocate_attribute("type", cloneQString(statechartmodel::State::StateTypeToString(state->getType()))));

    const QString& description = state->getDescription();

    if (!description.isEmpty())
    {
        rootNode->append_node(buildDescription(description));
    }

    rootNode->append_node(buildParameterList("InputParameters", state->getInputParameters()));
    rootNode->append_node(buildParameterList("OutputParameters", state->getOutputParameters()));
    rootNode->append_node(buildParameterList("LocalParameters", state->getLocalParameters()));
    rootNode->append_node(buildSubstateList(state->getSubstates()));
    rootNode->append_node(buildEventList(state->getOutgoingEvents()));

    TransitionCPtr startStateTransition = state->getStartTransition();

    if (startStateTransition->destinationState)
    {
        rootNode->append_node(buildStartState(startStateTransition));
    }
    rootNode->append_node(buildTransitionList(state->getTransitions(), uuidToNodeMap));

    document.append_node(rootNode);
}

QString XmlWriter::getXmlString(bool indent) const
{
    std::string s;
    rapidxml::print(std::back_inserter(s), document, indent ? 0 : rapidxml::print_no_indenting);

    return QString::fromStdString(s);
}

void XmlWriter::visitLocalState(LocalStateCPtr localState)
{
    substateNode = document.allocate_node(rapidxml::node_element, "LocalState");
    substateNode->append_attribute(document.allocate_attribute("refuuid", cloneQString(localState->getClassUUID())));
    substateNode->append_attribute(document.allocate_attribute("left", cloneQString(QString::number(localState->getTopLeft().x()))));
    substateNode->append_attribute(document.allocate_attribute("top", cloneQString(QString::number(localState->getTopLeft().y()))));
    substateNode->append_attribute(document.allocate_attribute("boundingSquareSize", cloneQString(QString::number(localState->getBoundingSquareSize()))));
}

void XmlWriter::visitRemoteState(RemoteStateCPtr remoteState)
{
    substateNode = document.allocate_node(rapidxml::node_element, "RemoteState");
    substateNode->append_attribute(document.allocate_attribute("refuuid", cloneQString(remoteState->getClassUUID())));
    substateNode->append_attribute(document.allocate_attribute("proxyName", cloneQString(remoteState->proxyName)));
    substateNode->append_attribute(document.allocate_attribute("left", cloneQString(QString::number(remoteState->getTopLeft().x()))));
    substateNode->append_attribute(document.allocate_attribute("top", cloneQString(QString::number(remoteState->getTopLeft().y()))));
    substateNode->append_attribute(document.allocate_attribute("boundingSquareSize", cloneQString(QString::number(remoteState->getBoundingSquareSize()))));
}

void XmlWriter::visitDynamicRemoteState(DynamicRemoteStateCPtr dynamicRemoteState)
{
    substateNode = document.allocate_node(rapidxml::node_element, "DynamicRemoteState");
    substateNode->append_attribute(document.allocate_attribute("refuuid", cloneQString(dynamicRemoteState->getClassUUID())));
    substateNode->append_attribute(document.allocate_attribute("left", cloneQString(QString::number(dynamicRemoteState->getTopLeft().x()))));
    substateNode->append_attribute(document.allocate_attribute("top", cloneQString(QString::number(dynamicRemoteState->getTopLeft().y()))));
    substateNode->append_attribute(document.allocate_attribute("boundingSquareSize", cloneQString(QString::number(dynamicRemoteState->getBoundingSquareSize()))));

}

void XmlWriter::visitEndState(EndStateCPtr endState)
{
    substateNode = document.allocate_node(rapidxml::node_element, "EndState");
    substateNode->append_attribute(document.allocate_attribute("event", cloneQString(endState->getEventName())));
    substateNode->append_attribute(document.allocate_attribute("left", cloneQString(QString::number(endState->getTopLeft().x()))));
    substateNode->append_attribute(document.allocate_attribute("top", cloneQString(QString::number(endState->getTopLeft().y()))));
    substateNode->append_attribute(document.allocate_attribute("boundingSquareSize", cloneQString(QString::number(endState->getBoundingSquareSize()))));
}

rapidxml::xml_node<>* XmlWriter::buildDescription(const QString& description)
{
    return document.allocate_node(rapidxml::node_element, "Description", cloneQString(description));
}

rapidxml::xml_node<>* XmlWriter::buildEventList(const EventList& eventList)
{
    rapidxml::xml_node<>* rootNode = document.allocate_node(rapidxml::node_element, "Events");

    for (EventList::const_iterator i = eventList.begin(); i != eventList.end(); ++i)
    {
        statechartmodel::EventPtr event = *i;

        rapidxml::xml_node<>* eventNode = document.allocate_node(rapidxml::node_element, "Event");
        eventNode->append_attribute(document.allocate_attribute("name", cloneQString(event->name)));

        if (!event->description.isEmpty())
        {
            eventNode->append_node(buildDescription(event->description));
        }

        rootNode->append_node(eventNode);
    }

    return rootNode;
}

rapidxml::xml_node<>* XmlWriter::buildParameterList(const QString& tagName, const armarx::statechartmodel::StateParameterMap& parameterMap)
{
    rapidxml::xml_node<>* rootNode = document.allocate_node(rapidxml::node_element, cloneQString(tagName));

    for (armarx::statechartmodel::StateParameterMap::const_iterator i = parameterMap.begin(); i != parameterMap.end(); ++i)
    {
        QString parameterName = i.key();
        statechartmodel::StateParameterPtr parameter = i.value();

        rapidxml::xml_node<>* parameterNode = document.allocate_node(rapidxml::node_element, "Parameter");

        parameterNode->append_attribute(document.allocate_attribute("name", cloneQString(parameterName)));
        parameterNode->append_attribute(document.allocate_attribute("type", cloneQString(parameter->type)));
        parameterNode->append_attribute(document.allocate_attribute("docType", cloneQString(QString::fromStdString(variantInfo->getNestedHumanNameFromBaseName(parameter->type.toStdString())))));
        parameterNode->append_attribute(document.allocate_attribute("optional", parameter->optional ? "yes" : "no"));

        if (!parameter->description.isEmpty())
        {
            parameterNode->append_node(buildDescription(parameter->description));
        }

        for (auto j = parameter->profileDefaultValues.constBegin(); j != parameter->profileDefaultValues.constEnd(); ++j)
        {
            rapidxml::xml_node<>* profileDefaultValueNode = document.allocate_node(rapidxml::node_element, "DefaultValue");
            QString profileName = j.key();

            if (profileName.toStdString() != StatechartProfiles::GetRootName())
            {
                profileDefaultValueNode->append_attribute(document.allocate_attribute("profile", cloneQString(profileName)));
            }

            if (j.value().first)
            {
                JSONObjectPtr jsonObject = new JSONObject();
                jsonObject->serializeIceObject(j.value().first);
                profileDefaultValueNode->append_attribute(document.allocate_attribute("value", cloneQString(j.value().second)));
                profileDefaultValueNode->append_attribute(document.allocate_attribute("docValue", cloneQString(QString::fromStdString(escapeString(j.value().first->toString())))));
                parameterNode->append_node(profileDefaultValueNode);
            }
            else if (j.value().second.size() != 0)
            {
                profileDefaultValueNode->append_attribute(document.allocate_attribute("value", cloneQString(j.value().second)));
                parameterNode->append_node(profileDefaultValueNode);
            }
        }

        rootNode->append_node(parameterNode);
    }

    return rootNode;
}
std::string XmlWriter::escapeString(std::string str)
{
    str = simox::alg::replace_all(str, "\\", "\\\\");
    str = simox::alg::replace_all(str, "\r", "\\r");
    str = simox::alg::replace_all(str, "\n", "\\n");
    return str;
}

rapidxml::xml_node<>* XmlWriter::buildParameterMappingList(const armarx::statechartmodel::ParameterMappingList& parameterMappingList, const QString& mappingName)
{

    rapidxml::xml_node<>* rootNode = document.allocate_node(rapidxml::node_element,
                                     document.allocate_string(mappingName.toUtf8().data()));

    for (ParameterMappingList::const_iterator j = parameterMappingList.begin(); j != parameterMappingList.end(); ++j)
    {
        statechartmodel::ParameterMappingPtr parameterMapping = *j;

        rapidxml::xml_node<>* parameterMappingNode = document.allocate_node(rapidxml::node_element, "ParameterMapping");

        QString mappingSourceString;

        mappingSourceString = QString::fromStdString(PM::MappingSourceToString(parameterMapping->source));

        parameterMappingNode->append_attribute(document.allocate_attribute("sourceType", cloneQString(mappingSourceString)));
        parameterMappingNode->append_attribute(document.allocate_attribute("from", cloneQString(parameterMapping->sourceKey)));
        parameterMappingNode->append_attribute(document.allocate_attribute("to", cloneQString(parameterMapping->destinationKey)));

        for (auto j = parameterMapping->profileValues.constBegin(); j != parameterMapping->profileValues.constEnd(); ++j)
        {
            rapidxml::xml_node<>* profileDefaultValueNode = document.allocate_node(rapidxml::node_element, "DefaultValue");
            QString profileName = j.key();

            if (profileName.toStdString() != StatechartProfiles::GetRootName())
            {
                profileDefaultValueNode->append_attribute(document.allocate_attribute("profile", cloneQString(profileName)));
            }

            profileDefaultValueNode->append_attribute(document.allocate_attribute("value", cloneQString(j.value())));
            parameterMappingNode->append_node(profileDefaultValueNode);
        }


        rootNode->append_node(parameterMappingNode);
    }

    return rootNode;
}

rapidxml::xml_node<>* XmlWriter::buildStartState(TransitionCPtr startStateTransition)
{
    rapidxml::xml_node<>* rootNode = document.allocate_node(rapidxml::node_element, "StartState");

    rootNode->append_attribute(document.allocate_attribute("substateName", cloneQString(startStateTransition->destinationState->getInstanceName())));

    rapidxml::xml_node<>* parameterMappingsNode = buildParameterMappingList(startStateTransition->mappingToNextStatesInput, "ParameterMappings");
    rootNode->append_node(parameterMappingsNode);

    // Support points
    rapidxml::xml_node<>* supportPointsNode = document.allocate_node(rapidxml::node_element, "SupportPoints");
    auto supportPoints = startStateTransition->supportPoints.toPointList();
    for (QList<QPointF>::const_iterator j = supportPoints.begin(); j != supportPoints.end(); ++j)
    {
        QPointF supportPoint = *j;

        rapidxml::xml_node<>* supportPointNode = document.allocate_node(rapidxml::node_element, "SupportPoint");
        supportPointNode->append_attribute(document.allocate_attribute("posX", cloneQString(QString::number(supportPoint.x()))));
        supportPointNode->append_attribute(document.allocate_attribute("posY", cloneQString(QString::number(supportPoint.y()))));

        supportPointsNode->append_node(supportPointNode);
    }

    rootNode->append_node(supportPointsNode);

    return rootNode;
}

rapidxml::xml_node<>* XmlWriter::buildSubstateList(const StateInstanceMap& substateMap)
{
    rapidxml::xml_node<>* rootNode = document.allocate_node(rapidxml::node_element, "Substates");

    for (StateInstanceMap::const_iterator i = substateMap.begin(); i != substateMap.end(); ++i)
    {
        QString substateName = i.key();
        StateInstancePtr substate = i.value();

        substate->accept(*this);
        substateNode->prepend_attribute(document.allocate_attribute("name", cloneQString(substateName)));
        rootNode->append_node(substateNode);
    }

    return rootNode;
}

rapidxml::xml_node<>* XmlWriter::buildTransitionList(const CTransitionList& transitionList, const QMap<QString, StateTreeNodePtr>& uuidToNodeMap)
{
    rapidxml::xml_node<>* rootNode = document.allocate_node(rapidxml::node_element, "Transitions");

    for (CTransitionList::const_iterator i = transitionList.begin(); i != transitionList.end(); ++i)
    {
        TransitionCPtr transition = *i;
        // Create transition node
        rapidxml::xml_node<>* transitionNode = document.allocate_node(rapidxml::node_element, "Transition");
        transitionNode->append_attribute(document.allocate_attribute("from", cloneQString(transition->sourceState->getInstanceName())));
        if (transition->transitionUserCode && transition->destinationState)
        {
            transitionNode->append_attribute(document.allocate_attribute("transitionCodeEnabled", "1"));
            if (transition->destinationState->getStateClass())
            {
                auto it = uuidToNodeMap.find(transition->destinationState->getStateClass()->getUUID());

                if (it != uuidToNodeMap.end())
                {
                    const StateTreeNodePtr node = it.value();
                    if (node)
                    {
                        transitionNode->append_attribute(document.allocate_attribute("toClass", cloneQString(node->getState()->getStateName())));
                        transitionNode->append_attribute(document.allocate_attribute("toGroup", cloneQString(node->getGroup()->getName())));
                        transitionNode->append_attribute(document.allocate_attribute("toPackage", cloneQString(node->getGroup()->getPackageName())));
                    }

                }
                if (transition->sourceState)
                {
                    if (transition->sourceState->getStateClass())
                    {
                        it = uuidToNodeMap.find(transition->sourceState->getStateClass()->getUUID());

                        if (it != uuidToNodeMap.end())
                        {
                            const StateTreeNodePtr node = it.value();
                            if (node)
                            {
                                transitionNode->append_attribute(document.allocate_attribute("fromClass", cloneQString(node->getState()->getStateName())));
                                transitionNode->append_attribute(document.allocate_attribute("fromGroup", cloneQString(node->getGroup()->getName())));
                                transitionNode->append_attribute(document.allocate_attribute("fromPackage", cloneQString(node->getGroup()->getPackageName())));
                            }
                        }
                    }
                    else
                    {
                        throw LocalException() << ("Could not find source stateclass of instance " + transition->sourceState->getInstanceName() + "! Please open all Remote States in the Statechart Editor before saving.");
                    }
                }
            }
            else
            {
                throw LocalException() << ("Could not find destination stateclass of instance " + transition->destinationState->getInstanceName() + "! Please open all Remote States in the Statechart Editor before saving.");
            }
        }

        if (transition->destinationState)
        {
            transitionNode->append_attribute(document.allocate_attribute("to", cloneQString(transition->destinationState->getInstanceName())));
        }

        transitionNode->append_attribute(document.allocate_attribute("eventName", cloneQString(transition->eventName)));

        // Parameter mappings
        rapidxml::xml_node<>* parameterMappingsNode = buildParameterMappingList(transition->mappingToNextStatesInput, "ParameterMappings");
        transitionNode->append_node(parameterMappingsNode);
        parameterMappingsNode = buildParameterMappingList(transition->mappingToParentStatesLocal, "ParameterMappingsToParentsLocal");
        transitionNode->append_node(parameterMappingsNode);
        parameterMappingsNode = buildParameterMappingList(transition->mappingToParentStatesOutput, "ParameterMappingsToParentsOutput");
        transitionNode->append_node(parameterMappingsNode);


        // Support points
        rapidxml::xml_node<>* supportPointsNode = document.allocate_node(rapidxml::node_element, "SupportPoints");
        auto supportPoints = transition->supportPoints.toPointList();
        for (QList<QPointF>::const_iterator j = supportPoints.begin(); j != supportPoints.end(); ++j)
        {
            QPointF supportPoint = *j;

            rapidxml::xml_node<>* supportPointNode = document.allocate_node(rapidxml::node_element, "SupportPoint");
            supportPointNode->append_attribute(document.allocate_attribute("posX", cloneQString(QString::number(supportPoint.x()))));
            supportPointNode->append_attribute(document.allocate_attribute("posY", cloneQString(QString::number(supportPoint.y()))));

            supportPointsNode->append_node(supportPointNode);
        }

        transitionNode->append_node(supportPointsNode);

        rootNode->append_node(transitionNode);
    }

    return rootNode;
}

rapidxml::xml_node<>* XmlWriter::buildXmlDeclaration()
{
    rapidxml::xml_node<>* node = document.allocate_node(rapidxml::node_declaration);
    node->append_attribute(document.allocate_attribute("version", "1.0"));
    node->append_attribute(document.allocate_attribute("encoding", "utf-8"));

    return node;
}

char* XmlWriter::cloneQString(const QString& string)
{
    // We need to clone all strings because the QByteArrays created be toUtf8() are destroyed immediately after leaving the scope.
    return document.allocate_string(string.toUtf8());
}
