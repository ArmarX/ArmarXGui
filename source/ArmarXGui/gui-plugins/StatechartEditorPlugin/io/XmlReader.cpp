/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Christian Mandery (christian.mandery at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include <ArmarXCore/core/ArmarXManager.h>
#include <ArmarXCore/observers/exceptions/user/UnknownTypeException.h>
#include <ArmarXCore/util/json/JSONObject.h>
#include "../../StatechartViewerPlugin/model/stateinstance/RegularState.h"
#include "../../StatechartViewerPlugin/model/DynamicRemoteStateClass.h"
#include <ArmarXCore/statechart/ParameterMapping.h>
#include <ArmarXCore/statechart/xmlstates/profiles/StatechartProfiles.h>

#include "StateInstanceFactory.h"
#include "XmlReader.h"

using namespace armarx::exceptions::local;
using namespace armarx::statechartio;
using namespace armarx::statechartmodel;

XmlReader::XmlReader(Ice::CommunicatorPtr iceCommunicator, VariantInfoPtr info) :
    iceCommunicator(iceCommunicator),
    info(info) {}

void XmlReader::parseXml(const QString& xmlString)
{
    QByteArray utf8Xml = xmlString.toUtf8();
    rapidxml::xml_document<> document;

    try
    {
        document.parse<rapidxml::parse_validate_closing_tags>(utf8Xml.data());
    }
    catch (const rapidxml::parse_error& e)
    {
        throw XmlReaderException(std::string("Could not parse XML file: ") + e.what());
    }

    rapidxml::xml_node<>* stateNode = XmlNodeIterator(&document, "State", true).getNext();
    QString version = readAttribute(stateNode, "version");

    if (version != "1.0" && version != "1.1" && version != "1.2")
    {
        throw XmlReaderException("Only statechart XML definition version 1.0 - 1.2 are supported right now!");
    }

    if (readAttribute(stateNode, "type", false) == statechartmodel::State::StateTypeToString(eDynamicRemoteState))
    {
        loadedState = statechartmodel::StatePtr(new DynamicRemoteStateClass(readAttribute(stateNode, "uuid")));
        ARMARX_IMPORTANT_S << "Found DynamicRemoteStateClass";
    }
    else
    {
        loadedState = statechartmodel::StatePtr(new statechartmodel::State(readAttribute(stateNode, "uuid")));
    }

    loadedState->setStateName(readAttribute(stateNode, "name"));
    loadedState->setSize(QSizeF(readAttribute(stateNode, "width").toFloat(), readAttribute(stateNode, "height").toFloat()));
    loadedState->setDescription(readDescription(stateNode));

    rapidxml::xml_node<>* curNode;
    armarx::statechartmodel::StateParameterMap parameterList;

    XmlNodeIterator inputParameterNI(stateNode, "InputParameters", false);

    //    parameterList = loadedState->getInputParameters();
    while ((curNode = inputParameterNI.getNext()))
    {
        parameterList.unite(readParameterList(curNode));
    }

    loadedState->setInputParameters(parameterList);

    parameterList.clear();
    //    parameterList = loadedState->getLocalParameters();
    XmlNodeIterator localParameterNI(stateNode, "LocalParameters", false);

    while ((curNode = localParameterNI.getNext()))
    {
        parameterList.unite(readParameterList(curNode));
    }

    loadedState->setLocalParameters(parameterList);

    parameterList.clear();
    //    parameterList = loadedState->getOutputParameters();
    XmlNodeIterator outputParameterNI(stateNode, "OutputParameters", false);

    while ((curNode = outputParameterNI.getNext()))
    {
        parameterList.unite(readParameterList(curNode));
    }

    loadedState->setOutputParameters(parameterList);

    StateInstanceMap substateList;
    XmlNodeIterator substatesNI(stateNode, "Substates", false);

    while ((curNode = substatesNI.getNext()))
    {
        substateList.unite(readSubstateList(curNode));
    }

    loadedState->replaceSubstates(substateList);

    EventList eventList;
    XmlNodeIterator eventNI(stateNode, "Events", false);

    while ((curNode = eventNI.getNext()))
    {
        eventList.append(readEventList(curNode));
    }

    loadedState->setOutgoingEvents(eventList);

    rapidxml::xml_node<>* startStateNode = XmlNodeIterator(stateNode, "StartState", false).getNext();

    if (startStateNode)
    {
        readStartState(startStateNode);
    }

    TransitionList transitionList;
    XmlNodeIterator transitionNI(stateNode, "Transitions", false);

    while ((curNode = transitionNI.getNext()))
    {
        transitionList.append(readTransitionList(curNode));
    }

    loadedState->replaceTransitions(transitionList);
}



QString XmlReader::readDescription(rapidxml::xml_node<>* rootNode) const
{
    rapidxml::xml_node<>* descriptionNode = XmlNodeIterator(rootNode, "Description", false).getNext();
    return descriptionNode ? QString::fromUtf8(descriptionNode->value()) : "";
}

EventList XmlReader::readEventList(rapidxml::xml_node<>* eventListNode) const
{
    EventList eventList;
    rapidxml::xml_node<>* curNode;

    XmlNodeIterator eventNI(eventListNode, "Event", false);

    while ((curNode = eventNI.getNext()))
    {
        statechartmodel::EventPtr event(new statechartmodel::Event());
        event->name = readAttribute(curNode, "name");
        event->description = readDescription(curNode);

        for (EventList::const_iterator i = eventList.begin(); i != eventList.end(); ++i)
        {
            if ((*i)->name == event->name)
            {
                throw XmlReaderException("Duplicate event name: " + event->name.toStdString());
            }
        }

        eventList.append(event);
    }

    return eventList;
}

armarx::statechartmodel::StateParameterMap XmlReader::readParameterList(rapidxml::xml_node<>* parameterListNode) const
{
    armarx::statechartmodel::StateParameterMap stateParameterMap;
    rapidxml::xml_node<>* curNode;

    XmlNodeIterator parameterNI(parameterListNode, "Parameter", false);

    while ((curNode = parameterNI.getNext()))
    {
        statechartmodel::StateParameterPtr stateParameter(new statechartmodel::StateParameter());
        stateParameter->type = readAttribute(curNode, "type");
        //stateParameter->dataType = readAttribute(curNode, "dataType");
        stateParameter->description = readDescription(curNode);

        // Support for legacy "default" attribute
        stateParameter->setDefaultValueJson(readAttribute(curNode, "default", false));

        if (!stateParameter->getDefaultValueJson().isEmpty())
        {
            std::function<bool(std::string, std::string)> loadVar;
            loadVar = [&](std::string curType, std::string prevType)
            {
                if (curType == prevType)
                {
                    return false;
                }
                try
                {

                    JSONObjectPtr jsonObject = new JSONObject(iceCommunicator);
                    jsonObject->fromString(stateParameter->getDefaultValueJson().toUtf8().data());

                    stateParameter->profileDefaultValues[QString::fromUtf8(StatechartProfiles::GetRootName().c_str())] =
                    {VariantContainerBasePtr::dynamicCast(jsonObject->deserializeIceObject()), stateParameter->getDefaultValueJson()};

                }
                catch (Ice::NoValueFactoryException& e)
                {
                    ARMARX_INFO_S << "Variant missing - trying to load variant " << e.type << " now";
                    info->loadLibraryOfVariant(e.type);
                    ArmarXManager::RegisterKnownObjectFactoriesWithIce(iceCommunicator);
                    return loadVar(e.type, curType);
                }
                return true;
            };

            try
            {
                loadVar("", "-");
            }
            catch (exceptions::user::UnknownTypeException& e)
            {
                ARMARX_WARNING_S << "Could not deserialize JSONString for type " << stateParameter->type;
            }
            catch (const armarx::JSONException& e)
            {
                throw XmlReaderException("Could not parse JSON: " + stateParameter->getDefaultValueJson().toStdString());
            }
        }

        // End of code snippet for legacy support

        XmlNodeIterator defaultValueNI(curNode, "DefaultValue", false);
        rapidxml::xml_node<>* defaultValueCurNode;

        while ((defaultValueCurNode = defaultValueNI.getNext()))
        {
            QString profileName = readAttribute(defaultValueCurNode, "profile", false);

            if (profileName.size() == 0)
            {
                profileName = QString::fromUtf8(StatechartProfiles::GetRootName().c_str());
            }

            QString valueJson = readAttribute(defaultValueCurNode, "value", true);
            VariantContainerBasePtr valueVariant;
            std::function<bool(std::string, std::string)> loadVar;
            loadVar = [&](std::string curType, std::string prevType)
            {
                if (curType == prevType)
                {
                    return false;
                }
                try
                {

                    JSONObjectPtr jsonObject = new JSONObject(iceCommunicator);
                    jsonObject->fromString(valueJson.toUtf8().data());

                    valueVariant = VariantContainerBasePtr::dynamicCast(jsonObject->deserializeIceObject());
                }
                catch (Ice::NoValueFactoryException& e)
                {
                    ARMARX_INFO_S << "Variant missing - trying to load variant " << e.type << " now";
                    info->loadLibraryOfVariant(e.type);
                    ArmarXManager::RegisterKnownObjectFactoriesWithIce(iceCommunicator);
                    return loadVar(e.type, curType);
                }
                return true;
            };

            try
            {
                loadVar("", "-");
            }
            catch (exceptions::user::UnknownTypeException& e)
            {
                ARMARX_WARNING_S << "Could not deserialize JSONString for type " << stateParameter->type;
            }
            catch (const armarx::JSONException& e)
            {
                throw XmlReaderException("Could not parse JSON: " + valueJson.toStdString());
            }

            stateParameter->profileDefaultValues[profileName] = QPair<VariantContainerBasePtr, QString>(valueVariant, valueJson);
        }

        QString optionalStr = readAttribute(curNode, "optional");

        if (optionalStr == "yes")
        {
            stateParameter->optional = true;
        }
        else if (optionalStr == "no")
        {
            stateParameter->optional = false;
        }
        else
        {
            throw XmlReaderException("\"optional\" attribute must have value \"yes\" or \"no\"!");
        }

        QString parameterName = readAttribute(curNode, "name");

        if (stateParameterMap.contains(parameterName))
        {
            throw XmlReaderException("Duplicate parameter name: " + parameterName.toStdString());
        }

        stateParameterMap[parameterName] = stateParameter;
    }

    return stateParameterMap;
}

ParameterMappingList XmlReader::readParameterMappingList(rapidxml::xml_node<>* parameterMappingListNode) const
{
    ParameterMappingList parameterMappingList;
    rapidxml::xml_node<>* curNode;

    XmlNodeIterator parameterMappingNI(parameterMappingListNode, "ParameterMapping", false);

    while ((curNode = parameterMappingNI.getNext()))
    {
        statechartmodel::ParameterMappingPtr parameterMapping(new statechartmodel::ParameterMapping());
        parameterMapping->sourceKey = readAttribute(curNode, "from");
        parameterMapping->destinationKey = readAttribute(curNode, "to");

        QString mappingSourceString = readAttribute(curNode, "sourceType");
        parameterMapping->source = PM::StringToMappingSource(mappingSourceString.toStdString());

        if (parameterMapping->source == eMappingSourcesCount)
        {
            throw XmlReaderException("Unknown value for sourceType: \"" + mappingSourceString.toStdString() + "\"");
        }



        XmlNodeIterator defaultValueNI(curNode, "DefaultValue", false);
        rapidxml::xml_node<>* defaultValueCurNode;

        while ((defaultValueCurNode = defaultValueNI.getNext()))
        {
            QString profileName = readAttribute(defaultValueCurNode, "profile", false);

            if (profileName.size() == 0)
            {
                profileName = QString::fromUtf8(StatechartProfiles::GetRootName().c_str());
            }

            QString valueJson = readAttribute(defaultValueCurNode, "value", true);
            parameterMapping->profileValues[profileName] = valueJson;
        }


        parameterMappingList.append(parameterMapping);
    }

    return parameterMappingList;
}

void XmlReader::readStartState(rapidxml::xml_node<>* startStateNode) const
{
    loadedState->setStartState(getSubstateByInstanceName(readAttribute(startStateNode, "substateName")));

    rapidxml::xml_node<>* parameterMappingsNode = XmlNodeIterator(startStateNode, "ParameterMappings", false).getNext();

    if (parameterMappingsNode)
    {
        loadedState->setStartStateInputMapping(readParameterMappingList(parameterMappingsNode));
        StateCPtr state = loadedState;
        TransitionCPtr transition = state->getStartTransition();
        rapidxml::xml_node<>* supportPointsNode = XmlNodeIterator(startStateNode, "SupportPoints", false).getNext();
        SupportPoints points;
        if (supportPointsNode)
        {
            rapidxml::xml_node<>* supportPointNode;
            XmlNodeIterator supportPointNI(supportPointsNode, "SupportPoint", false);
            QPointList supportPoints;
            while ((supportPointNode = supportPointNI.getNext()))
            {
                QPointF supportPoint(readAttribute(supportPointNode, "posX").toFloat(), readAttribute(supportPointNode, "posY").toFloat());

                supportPoints.append(supportPoint);
            }
            points.setControlPoints(supportPoints);
            loadedState->setTransitionSupportPoints(transition, points);
        }
    }
}

StateInstanceMap XmlReader::readSubstateList(rapidxml::xml_node<>* substateListNode) const
{
    StateInstanceMap stateInstanceMap;
    rapidxml::xml_node<>* curNode = substateListNode->first_node();

    while (curNode)
    {
        StateInstanceFactoryBasePtr stateInstanceFactory = StateInstanceFactoryBase::fromName(curNode->name(), XmlParentPair(curNode, loadedState));

        if (!stateInstanceFactory)
        {
            throw XmlReaderException("Invalid substate type: \"" + std::string(curNode->name()) + "\"");
        }

        StateInstancePtr stateInstance = stateInstanceFactory->getStateInstance();

        QString stateInstanceName = stateInstance->getInstanceName();

        if (stateInstanceMap.contains(stateInstanceName))
        {
            throw XmlReaderException("Duplicate substate name: " + stateInstanceName.toStdString());
        }

        stateInstanceMap[stateInstanceName] = stateInstance;

        curNode = curNode->next_sibling();
    }

    return stateInstanceMap;
}

TransitionList XmlReader::readTransitionList(rapidxml::xml_node<>* transitionListNode) const
{
    TransitionList transitionList;
    rapidxml::xml_node<>* curNode;

    XmlNodeIterator transitionNI(transitionListNode, "Transition", false);

    while ((curNode = transitionNI.getNext()))
    {
        TransitionPtr transition(new Transition());

        QString sourceStateName = readAttribute(curNode, "from");
        QString eventName = readAttribute(curNode, "eventName");

        if (!hasSubstateByInstanceName(sourceStateName))
        {
            ARMARX_WARNING_S << "Skipping broken transition in state '" << loadedState->getStateName().toStdString() << "'. SourceState '"
                             << sourceStateName << "' of event '" << eventName << "' could not be found in the list of substates. State UUID=" << loadedState->getUUID()
                             << ".\nStatechart group information or state path are not available at this point. Sorry. Use grep.";
            continue;
        }

        transition->sourceState = getSubstateByInstanceName(sourceStateName);
        transition->destinationState = getSubstateByInstanceName(readAttribute(curNode, "to", false));
        transition->eventName = eventName;
        transition->transitionUserCode = readAttribute(curNode, "transitionCodeEnabled", false) == "1";

        rapidxml::xml_node<>* parameterMappingsNode = XmlNodeIterator(curNode, "ParameterMappings", false).getNext();

        if (parameterMappingsNode)
        {
            transition->mappingToNextStatesInput = readParameterMappingList(parameterMappingsNode);
        }

        parameterMappingsNode = XmlNodeIterator(curNode, "ParameterMappingsToParentsLocal", false).getNext();

        if (parameterMappingsNode)
        {
            transition->mappingToParentStatesLocal = readParameterMappingList(parameterMappingsNode);
        }

        parameterMappingsNode = XmlNodeIterator(curNode, "ParameterMappingsToParentsOutput", false).getNext();

        if (parameterMappingsNode)
        {
            transition->mappingToParentStatesOutput = readParameterMappingList(parameterMappingsNode);
        }

        rapidxml::xml_node<>* supportPointsNode = XmlNodeIterator(curNode, "SupportPoints", false).getNext();

        if (supportPointsNode)
        {
            rapidxml::xml_node<>* supportPointNode;
            XmlNodeIterator supportPointNI(supportPointsNode, "SupportPoint", false);
            QPointList supportPoints;
            while ((supportPointNode = supportPointNI.getNext()))
            {
                QPointF supportPoint(readAttribute(supportPointNode, "posX").toFloat(), readAttribute(supportPointNode, "posY").toFloat());

                supportPoints.append(supportPoint);
            }
            transition->supportPoints.setControlPoints(supportPoints);
        }

        transitionList.append(transition);
    }

    return transitionList;
}

bool XmlReader::hasSubstateByInstanceName(const QString& name) const
{
    if (name.isEmpty())
    {
        return false;
    }

    return loadedState->getSubstates().contains(name);
}

StateInstancePtr XmlReader::getSubstateByInstanceName(const QString& name) const
{
    if (name.isEmpty())
    {
        return StateInstancePtr();
    }

    StateInstanceMap substates = loadedState->getSubstates();

    if (!substates.contains(name))
    {
        std::stringstream ss;
        ss << "Available substates (";
        ss << substates.size();
        ss << "): ";

        for (QString s : substates.keys())
        {
            ss << s.toStdString() << "; ";
        }

        throw XmlReaderException("Referenced substate \"" + name.toStdString() + "\" not found in State \"" + loadedState->getStateName().toStdString() + "\"! " + ss.str());
    }

    return substates[name];
}

QString XmlReader::readAttribute(rapidxml::xml_node<>* node, const QString& attributeName, bool required) const
{
    rapidxml::xml_attribute<>* attribute = node->first_attribute(attributeName.toUtf8());

    if (!attribute)
    {
        if (required)
        {
            throw XmlReaderException("Attribute \"" + attributeName.toStdString() + "\" for \"" + node->name() + "\" node not found!");
        }
        else
        {
            return "";
        }
    }

    return QString::fromUtf8(attribute->value());
}

XmlNodeIterator::XmlNodeIterator(rapidxml::xml_node<>* parentNode, const QString& nodeName, bool required) :
    parentNode(parentNode),
    currentChildNode(0),
    nodeName(nodeName),
    nodeNameUtf8(nodeName.toUtf8()),
    required(required) {}

rapidxml::xml_node<>* XmlNodeIterator::getNext()
{
    if (currentChildNode)
    {
        rapidxml::xml_node<>* nextChildNode = currentChildNode->next_sibling(nodeNameUtf8.data());

        if (!nextChildNode)
        {
            return 0;
        }

        currentChildNode = nextChildNode;
    }
    else
    {
        currentChildNode = parentNode->first_node(nodeNameUtf8.data());

        if (required && !currentChildNode)
        {
            throw XmlReaderException("Required node \"" + nodeName.toStdString() + "\" not found!");
        }
    }

    return currentChildNode;
}
