/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "GroupXmlWriter.h"

#include <ArmarXCore/core/exceptions/local/FileIOException.h>

#include <QFile>
#include <QTextStream>
#include <stdexcept>

#include <ArmarXCore/statechart/xmlstates/StateGroupGenerator.h>


using namespace armarx;

GroupXmlWriter::GroupXmlWriter()
{
}

void GroupXmlWriter::WriteXml(StatechartGroupPtr group, QString path, bool indent)
{
    RapidXmlWriter builder;
    RapidXmlWriterNode root = builder.createRootNode("StatechartGroup");
    root.append_attribute("name", group->getName().toUtf8().data());
    root.append_attribute("package", group->getPackageName().toUtf8().data());
    root.append_optional_bool_attribute("generateContext", "true", "false", group->contextGenerationEnabled(), false);

    if (!group->getDescription().isEmpty())
    {
        root.append_string_node("Description", group->getDescription().toUtf8().data());
    }

    RapidXmlWriterNode proxies = root.append_node("Proxies");

    for (QString proxy : group->getProxies())
    {
        proxies.append_node("Proxy").append_attribute("value", proxy.toUtf8().data());
    }

    auto c = group->getConfigurations();
    if (!c.empty())
    {
        RapidXmlWriterNode configurations = root.append_node("Configurations");
        for (auto it = c.begin(); it != c.end(); it++)
        {
            if (!it->isEmpty())
            {
                configurations.append_node("Configuration").append_attribute("profileName", it.key().toUtf8().data()).append_data_node(it->toUtf8().data());
            }
        }
    }

    StateTreeNodePtr rootNode = group->getRootNode();

    for (int i = 0; i < rootNode->getChildren().count(); i++)
    {
        WriteNode(rootNode->getChildren().at(i), root);
    }


    //builder.saveToFile(path.toUtf8().data(), true);
    std::string contents = builder.print(true);
    if (StatechartGroupGenerator::writeFileContentsIfChanged(path.toUtf8().data(), contents))
    {
        ARMARX_INFO_S << "Writing new group definition to " << path.toUtf8().data();
    }
}

void GroupXmlWriter::WriteFileContents(QString path, QString contents)
{
    QFile f(path);

    if (!f.open(QFile::WriteOnly | QFile::Text))
    {
        throw armarx::exceptions::local::FileOpenException(path.toUtf8().data());
    }

    QTextStream out(&f);
    out << contents;
    out.flush();
}

void GroupXmlWriter::WriteNode(const StateTreeNodePtr& node, RapidXmlWriterNode parentXmlNode)
{
    switch (node->getNodeType())
    {
        case StateTreeNode::Folder:
            WriteFolderNode(node, parentXmlNode);
            return;

        case StateTreeNode::State:
            WriteStateNode(node, parentXmlNode);
            return;

        default:
            throw std::runtime_error("Unknown node type");
    }
}

void GroupXmlWriter::WriteFolderNode(const StateTreeNodePtr& node, RapidXmlWriterNode parentXmlNode)
{
    RapidXmlWriterNode xmlNode = parentXmlNode.append_node("Folder");
    xmlNode.append_attribute("basename", node->getBasename().toUtf8().data());

    for (int i = 0; i < node->getChildren().count(); i++)
    {
        WriteNode(node->getChildren().at(i), xmlNode);
    }
}

void GroupXmlWriter::WriteStateNode(const StateTreeNodePtr& node, RapidXmlWriterNode parentXmlNode)
{
    RapidXmlWriterNode xmlNode = parentXmlNode.append_node("State");
    xmlNode.append_attribute("filename", node->getBasename().toUtf8().data());
    xmlNode.append_optional_bool_attribute("visibility", "public", "private", node->isPublic(), false);
}

