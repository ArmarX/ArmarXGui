/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Christian Mandery (christian.mandery at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include "../../StatechartViewerPlugin/model/State.h"
#include "../../StatechartViewerPlugin/model/StateParameter.h"
#include "../../StatechartViewerPlugin/model/stateinstance/Visitor.h"
#include "../model/StateTreeNode.h"
#include <ArmarXCore/core/rapidxml/rapidxml.hpp>

#include <QList>
#include <QString>

#include <ArmarXCore/observers/variant/VariantInfo.h>

namespace armarx::statechartmodel
{
    class State;
    class Event;
    class ParameterMapping;
    class LocalState;
    class RemoteState;
    class EndState;

    using StateCPtr = std::shared_ptr<const State>;
    using EventPtr = std::shared_ptr<Event>;
    using EventList = QList<EventPtr>;
    using ParameterMappingPtr = std::shared_ptr<ParameterMapping>;
    using ParameterMappingList = QList<ParameterMappingPtr>;
    using LocalStateCPtr = std::shared_ptr<const LocalState>;
    using RemoteStateCPtr = std::shared_ptr<const RemoteState>;
    using EndStateCPtr = std::shared_ptr<const EndState>;
}

namespace armarx::statechartio
{
    /**
     * @class XmlWriter
     * @brief XML writer class used to build XML representations of State objects.
     */
    class XmlWriter : public armarx::statechartmodel::Visitor
    {
    public:
        XmlWriter(const VariantInfoPtr& variantInfo);

        /**
         * Builds XML data structures for serialization of the given state object. Afterwards, the resulting XML document can be generated using the getXmlString() method.
         * @param state State object that should be serialized to XML.
         */
        void serialize(armarx::statechartmodel::StateCPtr state, const QMap<QString, StateTreeNodePtr>& uuidToNodeMap = QMap<QString, StateTreeNodePtr>());

        /**
         * Builds the XML document for the state object that has been handled by serialize() before.
         * @param indent Whether to indent the resulting XML document (for output in a file) or to output unindented XML (for snapshots).
         * @return XML document as a string.
         */
        QString getXmlString(bool indent = true) const;

    protected:
        void visitLocalState(armarx::statechartmodel::LocalStateCPtr localState) override;
        void visitRemoteState(armarx::statechartmodel::RemoteStateCPtr remoteState) override;
        void visitDynamicRemoteState(armarx::statechartmodel::DynamicRemoteStateCPtr dynamicRemoteState) override;
        void visitEndState(armarx::statechartmodel::EndStateCPtr endState) override;

    private:
        rapidxml::xml_node<>* buildDescription(const QString& description);
        rapidxml::xml_node<>* buildEventList(const armarx::statechartmodel::EventList& eventList);
        rapidxml::xml_node<>* buildParameterList(const QString& tagName, const armarx::statechartmodel::StateParameterMap& parameterMap);
        rapidxml::xml_node<>* buildParameterMappingList(const armarx::statechartmodel::ParameterMappingList& parameterMappingList, const QString& mappingName);
        rapidxml::xml_node<>* buildStartState(armarx::statechartmodel::TransitionCPtr startStateTransition);
        rapidxml::xml_node<>* buildSubstateList(const armarx::statechartmodel::StateInstanceMap& substateMap);
        rapidxml::xml_node<>* buildTransitionList(const armarx::statechartmodel::CTransitionList& transitionList, const QMap<QString, StateTreeNodePtr>& uuidToNodeMap);
        rapidxml::xml_node<>* buildXmlDeclaration();

        char* cloneQString(const QString& string);

        rapidxml::xml_document<> document;
        rapidxml::xml_node<>* substateNode;  /* Needed for visit*() methods */
        std::string escapeString(std::string str);

        VariantInfoPtr variantInfo;
    };
}
