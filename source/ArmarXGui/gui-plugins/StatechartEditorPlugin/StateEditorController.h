/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once


#include "view/StatechartEditorMainWindow.h"

#include "../StatechartViewerPlugin/model/State.h"

#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/editorfileopener.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/TipDialog.h>

#include <QObject>

#include <ArmarXCore/observers/variant/VariantInfo.h>

#include <ArmarXCore/statechart/xmlstates/profiles/StatechartProfiles.h>

#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/view/dialogs/StateDialog.h>

namespace armarx
{

    class StateTreeController;
    using StateTreeControllerPtr = std::shared_ptr<StateTreeController>;
    class StateTreeNode;
    using StateTreeNodePtr = std::shared_ptr<StateTreeNode>;

    class StateTabWidget;
    class StateEditorController : public QObject
    {
        Q_OBJECT

    public:
        explicit StateEditorController(StatechartEditorMainWindow* mainWindow, StateTreeControllerPtr treeController, Ice::CommunicatorPtr communicator, VariantInfoPtr variantInfo, StatechartProfilePtr currentProfile, QPointer<TipDialog> tipDialog, QObject* parent = 0);

        QList<QString> getRelevantProfiles() const;
    signals:

    public slots:
        void showStateContextMenu(statechartmodel::StateInstancePtr stateInstance, QPoint mouseScreenPos, QPointF mouseItemPos);
        void showTransitionContextMenu(statechartmodel::TransitionCPtr transition, statechartmodel::StatePtr state, QPoint mouseScreenPos, QPointF mouseItemPos);
        void setLockRemoteStatesByDefault(bool lock);
        void editSelectedState();
    private slots:
        void connectToStateTab(int index);
    protected:
        void stateDialogAccepted(statechartmodel::StateInstancePtr stateInstance, const armarx::StateDialog& d);

        QMenu* createStateContextMenu();
        QMenu* createTransitionContextMenu();

        StatechartEditorMainWindow* mainWindow;
        StateTreeControllerPtr treeController;
        EditorFileOpener opener;
        Ice::CommunicatorPtr communicator;
        VariantInfoPtr variantInfo;
        StatechartProfilePtr currentProfile;
        QPointer<TipDialog> tipDialog;
        bool lockRemoteStates;
        QMenu menu;
        QAction* editState;
        QAction* deleteState;
        QAction* layoutState;
        QAction* setstartstate;

        QAction* newEndstate;
        QAction* replaceState;
        QAction* openCode;
        QAction* findStateInTree;
    };
    using StateEditorControllerPtr = std::shared_ptr<StateEditorController>;

}

