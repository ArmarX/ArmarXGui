/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "model/StateTreeNode.h"
#include "model/StateTreeModel.h"

#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/editorfileopener.h>
#include "../StatechartViewerPlugin/model/StateMimeData.h"
#include "../StatechartViewerPlugin/model/stateinstance/StateInstance.h"

#include <ArmarXCore/core/system/cmake/ArmarXPackageToolInterface.h>
#include <ArmarXCore/util/ScenarioManagerCommon/data_structure/Application.h>

#include <QAbstractItemModel>
#include <QSortFilterProxyModel>
#include <QTreeView>

#include <ArmarXCore/statechart/xmlstates/profiles/StatechartProfiles.h>
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/cloning/GroupCloner.h>
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/renaming/GroupRenamer.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/InfixFilterModel.h>


class QProcess;

namespace armarx
{

    class StatechartProfiles;
    using StatechartProfilesPtr = std::shared_ptr<StatechartProfiles>;





    class StateTreeController;
    using StateTreeControllerPtr = std::shared_ptr<StateTreeController>;

    class StateTreeController : public QAbstractItemModel
    {
        Q_OBJECT

    public:
        StateTreeController(Ice::CommunicatorPtr ic, VariantInfoPtr variantInfo, QList<QVariant> headerInfo, QTreeView* treeView, QLineEdit* filterLineEdit, const ArmarXPackageToolInterfacePtr& packageTool, const StatechartProfilesPtr& statechartProfiles, StatechartProfilePtr currentProfile, QObject* parent = 0);
        ~StateTreeController() override;

        QVariant data(const QModelIndex& index, int role) const override;
        Qt::ItemFlags flags(const QModelIndex& index) const override;
        QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
        QModelIndex index(int row, int column, const QModelIndex& parentIndex = QModelIndex()) const override;
        QModelIndex parent(const QModelIndex& index) const override;
        int rowCount(const QModelIndex& parent = QModelIndex()) const override;
        int columnCount(const QModelIndex& parent = QModelIndex()) const override;
        StatechartGroupPtr loadAndAddGroup(QString groupDefinitionFile);
        StatechartGroupPtr loadGroup(QString groupDefinitionFile);
        StatechartGroupPtr addNewGroup(const QString& groupName, const QString& groupPath, const QString& description, const QString& packageName, const QList<QString> proxies, bool generateContext, const QMap<QString, QString>& statechartGroupConfigurations);

        void loadAllStates();

        StateTreeNodePtr getNodeByState(statechartmodel::StatePtr state);
        StateTreeNodePtr getSelectedNode();
        StateTreeNodePtr getClosestParentFolderOrGroupNode(StateTreeNodePtr node);
        bool nodeIsState(StateTreeNodePtr node);
        bool nodeIsFolder(StateTreeNodePtr node);
        bool nodeIsGroup(StateTreeNodePtr node);
        bool nodeIsFolderOrGroup(StateTreeNodePtr node);
        bool selectedNodeIsState();
        bool selectedNodeIsFolder();
        bool selectedNodeIsGroup();
        bool selectedNodeIsFolderOrGroup();
        StateTreeNodePtr tryCreateNewState(QString name);
        StateTreeNodePtr createNewState(QString name, StateTreeNodePtr parent, bool createDynamicRemoteState = false);
        StateTreeNodePtr createNewFolder(QString name, StateTreeNodePtr parent);
        //StateTreeNodePtr get
        void tryDeleteState();
        void removeState(StateTreeNodePtr stateNode);
        void tryDeleteFolder();
        void removeFolder(StateTreeNodePtr folderNode);
        void tryDeleteGroup();

        void removeNodeFromTree(StateTreeNodePtr node);

        void saveAll();

        StateTreeNodePtr getNode(QModelIndex index) const;

        void selectNodeByState(statechartmodel::StatePtr state);

    signals:
        void closeAllTabsRequested();

    public slots:
        void onContextMenu(const QPoint& point);
        void onNewStateDefinition();
        void onNewDynamicRemoteStateDefinition();
        void onNewFolder();
        void onDeleteNode();
        void onMakeStatePublic(bool enable);
        void onFindStateUsages();
        void onRenameState();
        void onOpenCMakeProject();
        void onShowFullPath();
        void onOpenStateCPP();
        void onOpenGroup(QString groupFile = "");
        void onExecuteGroup();
        void onStatechartFinished(int exitCode);
        //void onGenerateStateBaseClass();
        //void onGenerateRsoBaseClass();
        void onGenerateStateCppFiles();
        void onRenameGroup();
        void onCloneGroup();
        void onGroupProperties();
        void collapseAll();
        void selectNode(StateTreeNodePtr node);

        void executeGroup(StatechartGroupPtr group, QString startState = "");

        void stopGroupExecution(StatechartGroupPtr group);

        void executeGroupWithDependencies(StatechartGroupPtr group, QString startState);

        void stopGroupExecutionWithDependencies(StatechartGroupPtr group);
    public:
        void openStateCPP(StateTreeNodePtr node);

    private slots:
        void processOutputReady();
        void processErrorsReady();
        void stateAddedOrRemoved();
        void expandFilterResults(const QString& filterString);
    private:

        QModelIndex getIndex(StateTreeNodePtr node);
        QString buildStateUsageString(QList<statechartmodel::StateInstancePtr> instances);

        StateTreeNodePtr rootNode;
        QList<QVariant> headerInfo;
        QTreeView* treeView;
        StateTreeModelPtr stateTreeModel;
        InfixFilterModel* proxyModel;
        GroupClonerPtr groupCloner;
        GroupRenamerPtr groupRenamer;
        StatechartGroupPtr activeGroup;
        QMenu* contextMenuGroup;
        QMenu* contextMenuFolder;
        QMenu* contextMenuState;
        QAction* actionNewStateDefinition;
        QAction* actionNewDynamicRemoteStateDefinition;
        QAction* actionNewFolder;
        QAction* actionDeleteNode;
        QAction* actionMakeStatePublic;
        QAction* actionFindStateUsages;
        QAction* actionRenameState;
        QAction* actionOpenCMakeProject;
        QAction* actionShowFullPath;
        QAction* actionOpenStateCPP;
        QAction* actionExecuteGroup;
        //QAction *actionGenerateStateBaseClass;
        //QAction *actionGenerateRsoBaseClass;
        QAction* actionGenerateStateCPP;
        QAction* actionCloneGroup;
        QAction* actionRenameGroup;
        QAction* actionGroupProperties;
        ScenarioManager::Data_Structure::ApplicationPtr appForDrag;

        ArmarXPackageToolInterfacePtr packageTool;
        EditorFileOpener opener;
        QMap<QString, QProcess*> processes;
        VariantInfoPtr variantInfo;
        StatechartProfilesPtr profiles;
        StatechartProfilePtr currentProfile;
        Ice::CommunicatorPtr communicator;

        struct StateMimeData : AbstractStateMimeData
        {
            StateMimeData(statechartmodel::StatePtr state, StateTreeModelPtr stateTreeModel);
            bool isInSameGroup(statechartmodel::StatePtr state) const override;
            bool isPublic() const override;
        private:
            StateTreeModelPtr stateTreeModel;
        };

        // QAbstractItemModel interface
        bool retrieveNewStateName(QString& newStateName, StateTreeNodePtr& node, bool& createCPPFiles, bool askForCPPFiles);
        /*!
         * \brief Buffers the icons so that they are not loaded each time again
         * \param path Path to icon, can be resource.
         * \return Freshly loaded or buffered icon.
         */
        QIcon getIcon(const QString& path) const;
        mutable QMap<QString, QIcon> icons;
    public:
        QStringList mimeTypes() const override;
        QMimeData* mimeData(const QModelIndexList& indexes) const override;
    };
}

