/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX
* @author     Mirko Waechter( mirko.waechter at kit dot edu)
* @date       2016
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#include "MultiProfileDefaultValueEditWidget.h"

#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/ui_MultiProfileDefaultValueEditWidget.h>
#include <ArmarXCore/util/json/JSONObject.h>
#include <ArmarXCore/core/logging/Logging.h>

namespace armarx
{

    MultiProfileDefaultValueEditWidget::MultiProfileDefaultValueEditWidget(QString typeString, QStringList profileNames, QMap<QString, QString> values, Ice::CommunicatorPtr communicator, QWidget* parent) :
        QWidget(parent),
        ui(new Ui::MultiProfileDefaultValueEditWidget),
        typeString(typeString),
        communicator(communicator)
    {
        ui->setupUi(this);
        ARMARX_DEBUG << "type: " << typeString.toStdString();

        for (auto it = values.begin(); it != values.end(); it++)
        {
            QString jsonString = it.value();
            if (!jsonString.isEmpty())
            {
                defaultEdits[it.key()] = new ProfileDefaultValueEditWidget(typeString, jsonString, communicator, this);
                ui->splitter->addWidget(defaultEdits[it.key()].data());
            }

        }
        ui->comboBoxProfiles->addItems(profileNames);
        ui->comboBoxProfiles->setCurrentIndex(ui->comboBoxProfiles->count() - 1);

        // select first available value
        int i = 0;
        for (auto name : profileNames)
        {
            if (defaultEdits.contains(name) && !defaultEdits[name].isNull() && defaultEdits[name]->getValueAsJson())
            {
                ARMARX_DEBUG << name.toStdString() << " contains data: " << defaultEdits[name]->getValueAsJson()->toStdString();
                ui->comboBoxProfiles->setCurrentIndex(i);
                break;
            }
            i++;
        }
        delete ui->editWidget;// is only a placeholder -> delete
    }

    MultiProfileDefaultValueEditWidget::~MultiProfileDefaultValueEditWidget()
    {
        delete ui;
    }

    QMap<QString, VariantContainerBasePtr> MultiProfileDefaultValueEditWidget::getVariantContainers() const
    {
        QMap<QString, VariantContainerBasePtr> result;
        for (auto it = defaultEdits.begin(); it != defaultEdits.end(); it++)
        {
            if (!it->isNull())
            {
                auto var = it.value().data()->getVariantContainer();
                if (var)
                {
                    result[it.key()] = var;
                }
            }
        }
        return result;
    }

    QMap<QString, QString> MultiProfileDefaultValueEditWidget::getJsonMap() const
    {
        QMap<QString, QString> result;
        for (auto it = defaultEdits.begin(); it != defaultEdits.end(); it++)
        {
            std::optional<QString> json = it.value()->getValueAsJson();
            if (json)
            {
                result[it.key()] = *json;
            }
        }
        return result;
    }

    void MultiProfileDefaultValueEditWidget::on_comboBoxProfiles_currentIndexChanged(const QString& arg1)
    {
        for (const auto& it : defaultEdits)
        {
            if (!it.isNull())
            {
                it->hide();
            }
        }
        ARMARX_INFO_S << "profile: " << arg1.toStdString();
        if (!defaultEdits[arg1].isNull())
        {
            ARMARX_INFO_S << "showing: " << arg1.toStdString();
            defaultEdits[arg1]->show();
        }
        else
        {
            defaultEdits[arg1] = new ProfileDefaultValueEditWidget(typeString, "", communicator, this);
            ui->splitter->addWidget(defaultEdits[arg1].data());
        }
    }

} // namespace armarx

