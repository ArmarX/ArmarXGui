/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Valerij Wittenbeck (valerij.wittenbeck at student dot kit dot edu
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "ProfileDefaultValueEditWidget.h"
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/ui_ProfileDefaultValueEditWidget.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <QFileDialog>

#include <ArmarXCore/observers/variant/SingleTypeVariantList.h>
#include <ArmarXCore/observers/variant/StringValueMap.h>
#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/observers/variant/VariantContainer.h>
#include <ArmarXCore/util/json/JSONObject.h>
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/variantjson/VariantJsonCompressor.h>
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/view/dialogs/EditDefaultValueDialog.h>

#include <QtGui>
#include <QTextEdit>
#include <QLineEdit>

using namespace armarx;



ProfileDefaultValueEditWidget::ProfileDefaultValueEditWidget(QString typeString, QString jsonValue, Ice::CommunicatorPtr communicator, QWidget* parent) :
    QWidget(parent),
    ui(new Ui::ProfileDefaultValueEditWidget),
    typeString(typeString),
    jsonValue(jsonValue),
    communicator(communicator),
    editable(true)
{
    ui->setupUi(this);

    QLineEdit* lineEdit = new QLineEdit;
    ui->value->insertWidget(eLineEdit, lineEdit);

    QPushButton* editButton = new QPushButton("Edit");
    ui->value->insertWidget(eEditButton, editButton);
    connect(editButton, SIGNAL(clicked()), this, SLOT(editDefaultButtonClicked()));

    QPushButton* setButton = new QPushButton("Click to set");
    setButton->setStyleSheet("color: #888;");
    setButton->setFlat(true);
    ui->value->insertWidget(eSetButton, setButton);
    connect(setButton, SIGNAL(clicked()), this, SLOT(setButtonClicked()));

    int typeId = Variant::hashTypeName(typeString.toStdString());

    if (typeId == VariantType::String)
    {
        lineEdit->setValidator(NULL);
    }
    else if (typeId == VariantType::Int || typeId == VariantType::Long)
    {
        // do not use QIntValidator, accepts random stuff.
        QRegExp rx("-?\\d+");
        QValidator* validator = new QRegExpValidator(rx, this);
        lineEdit->setValidator(validator);
    }
    else if (typeId == VariantType::Bool)
    {
        QRegExp rx("(true|false)");
        QValidator* validator = new QRegExpValidator(rx, this);
        lineEdit->setValidator(validator);
    }
    else if (typeId == VariantType::Float || typeId == VariantType::Double)
    {
        // do not use QDoubleValidator. QDoubleValidator is buggy. accepts: 1e16.5
        // use JSON compliant regex. See class StructuralJsonParser.
        QRegExp rx("-?\\d+(\\.\\d+)?([eE][+-]?\\d+)?");
        QValidator* validator = new QRegExpValidator(rx, this);
        lineEdit->setValidator(validator);
    }

    if (jsonValue.isEmpty())
    {
        lineEdit->setText("");
    }
    else
    {
        JPathNavigator nav(VariantJsonCompressor::CompressToJson(jsonValue.toUtf8().data(), typeString.toUtf8().data()));
        std::string compressedValue = nav.isValue() ? nav.asValue()->rawValue() : nav.getData()->toJsonString(0);
        lineEdit->setText(QString::fromUtf8(compressedValue.c_str()));
    }



    connect(ui->pushButtonDelete, SIGNAL(clicked()), this, SLOT(resetValue()));

    if (jsonValue.size() == 0)
    {
        resetValue();
    }
    else
    {
        setButtonClicked();
    }
}

ProfileDefaultValueEditWidget::~ProfileDefaultValueEditWidget()
{

}

QString ProfileDefaultValueEditWidget::getType()
{
    return typeString;
}

void ProfileDefaultValueEditWidget::setEditable(bool editable)
{
    this->editable = editable;
    ui->value->widget(eLineEdit)->setEnabled(editable);
}

std::optional<QString> ProfileDefaultValueEditWidget::getValueAsString()
{
    if (ui->value->currentIndex() == Widget::eLineEdit)
    {
        return qobject_cast<QLineEdit*>(ui->value->currentWidget())->text();
    }
    else if (ui->value->currentIndex() == Widget::eEditButton)
    {
        return jsonValue;
    }

    return std::nullopt;
}

std::optional<QString> ProfileDefaultValueEditWidget::getValueAsJson()
{
    //JSONObjectPtr jsonObject = new JSONObject(communicator);
    int index = ui->value->currentIndex();

    if (index == Widget::eEditButton)
    {
        return jsonValue;
    }
    else if (index == Widget::eLineEdit)
    {
        QLineEdit* valueEdit = qobject_cast<QLineEdit*>(ui->value->currentWidget());
        std::string verboseJson = VariantJsonCompressor::DecompressBasicVariant(valueEdit->text().toUtf8().data(), typeString.toStdString());
        return QString::fromUtf8(verboseJson.c_str());
    }

    return std::nullopt;
}

VariantContainerBasePtr ProfileDefaultValueEditWidget::getVariantContainer()
{
    VariantContainerBasePtr result;

    if (ui->value->currentIndex() == eLineEdit)
    {
        QLineEdit* valueEdit = qobject_cast<QLineEdit*>(ui->value->currentWidget());
        int typeId = Variant::hashTypeName(typeString.toStdString());
        Variant variant;

        if (typeId == VariantType::Int)
        {
            variant.setInt(valueEdit->text().toInt());
        }
        else if (typeId == VariantType::Long)
        {
            variant.setLong(valueEdit->text().toLong());
        }
        else if (typeId == VariantType::Bool)
        {
            variant.setBool(valueEdit->text().compare("true", Qt::CaseInsensitive) == 0 || valueEdit->text() == "1");
        }
        else if (typeId == VariantType::Float)
        {
            variant.setFloat(valueEdit->text().toFloat());
        }
        else if (typeId == VariantType::Double)
        {
            variant.setDouble(valueEdit->text().toDouble());
        }
        else if (typeId == VariantType::String)
        {
            variant.setString(valueEdit->text().trimmed().toStdString());
        }

        result = new SingleVariant(variant);
    }
    else if (ui->value->currentIndex() == eEditButton)
    {
        if (jsonValue.isEmpty())
        {
            return result;
        }

        JSONObjectPtr jsonObject = new JSONObject(communicator);
        //        ARMARX_INFO_S << VAROUT(jsonValue.toUtf8().data());
        jsonObject->fromString(jsonValue.toUtf8().data());

        try
        {
            SerializablePtr obj = jsonObject->deserializeIceObject();
            result = VariantContainerBasePtr::dynamicCast(obj);

            if (!result)
            {
                // must be simple variant
                result = new SingleVariant(Variant(VariantDataPtr::dynamicCast(obj)));
            }
        }
        catch (std::exception& e)
        {
            ARMARX_WARNING_S << "JSON string for type " << typeString.toStdString()  << " could not be deserialized: " << e.what();
            return result;
        }
    }

    return result;
}

void ProfileDefaultValueEditWidget::resetValue()
{
    this->setFocus();
    //    ui->value->widget(eSetButton)->setFocus();
    ui->value->setCurrentIndex(eSetButton);
    ui->pushButtonDelete->setEnabled(false);
    ui->pushButtonDelete->hide();
    //    ui->value->widget(eSetButton)->setFocus();
    jsonValue.clear();
    auto lineEdit = qobject_cast<QLineEdit*>(ui->value->widget(eLineEdit));
    lineEdit->setText("");
}

void ProfileDefaultValueEditWidget::setButtonClicked()
{
    ui->pushButtonDelete->setEnabled(true);
    ui->pushButtonDelete->show();
    int variantId = Variant::hashTypeName(typeString.toStdString());

    if (variantId == VariantType::String || variantId == VariantType::Int || variantId == VariantType::Long || variantId == VariantType::Bool || variantId == VariantType::Float || variantId == VariantType::Double)
    {
        ui->value->setCurrentIndex(eLineEdit);
    }
    else
    {
        initEditButton();
        ui->value->setCurrentIndex(eEditButton);
    }
}

void ProfileDefaultValueEditWidget::initEditButton()
{
    try
    {
        if (jsonValue.isEmpty())
        {

            auto variantContainerType = VariantContainerType::FromString(typeString.toStdString());
            JSONObjectPtr jsonObject = new JSONObject(communicator);

            //            ARMARX_INFO_S << VAROUT(variantContainerType->typeId);

            Ice::ValueFactoryPtr factory = communicator->getValueFactoryManager()->find(variantContainerType->typeId);

            if (!factory)
            {
                ARMARX_WARNING << "Could not get ObjectFactory for Variant " << variantContainerType->typeId;
            }
            else
            {
                ARMARX_INFO_S << variantContainerType->typeId;
                Ice::ValuePtr valPtr = factory->create(variantContainerType->typeId);
                ARMARX_INFO_S << valPtr.get() << " " << SerializablePtr::dynamicCast(valPtr).get();

                VariantContainerBasePtr container = VariantContainerBasePtr::dynamicCast(valPtr);

                if (container)
                {
                    // TODO: make better with dummy element function for all containers/variants?!
                    //                ARMARX_INFO_S << VAROUT(variantContainerType->subType->typeId);
                    Ice::ValueFactoryPtr subfactory = communicator->getValueFactoryManager()->find(variantContainerType->subType->typeId);

                    if (!subfactory)
                    {
                        //                    ARMARX_INFO_S << " no factory";
                        subfactory = IceInternal::factoryTable->getValueFactory(variantContainerType->subType->typeId);
                    }

                    if (subfactory)
                    {
                        Ice::ValuePtr subObj = subfactory->create(variantContainerType->subType->typeId);
                        VariantDataPtr var = VariantDataPtr::dynamicCast(subObj);

                        SingleTypeVariantListPtr list = SingleTypeVariantListPtr::dynamicCast(valPtr);

                        if (var && list)
                        {
                            list->addVariant(Variant(var));
                        }

                        StringValueMapPtr map = StringValueMapPtr::dynamicCast(valPtr);

                        if (var && map)
                        {
                            map->addVariant("mykey", Variant(var));
                        }
                    }
                    else
                    {
                        ARMARX_INFO_S << " no base factory";
                    }
                }
                else
                {
                    container = new SingleVariant(Variant(VariantDataPtr::dynamicCast(valPtr)));
                }

                jsonObject->serializeIceObject(container);
            }

            jsonValue = QString::fromStdString(jsonObject->asString(true));
        }
    }
    catch (...)
    {
        handleExceptions();
        ui->value->widget(eEditButton)->setEnabled(false);
    }
}

void ProfileDefaultValueEditWidget::editDefaultButtonClicked()
{
    EditDefaultValueDialog d(jsonValue, typeString, communicator);
    if (d.exec() == QDialog::Accepted)
    {
        jsonValue = d.getJson();
    }
}
