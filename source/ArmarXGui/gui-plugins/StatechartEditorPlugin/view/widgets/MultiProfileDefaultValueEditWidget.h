/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX
* @author     Mirko Waechter( mirko.waechter at kit dot edu)
* @date       2016
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#pragma once

#include "ProfileDefaultValueEditWidget.h"

#include <QMap>
#include <QPointer>
#include <QWidget>

namespace armarx::Ui
{
    class MultiProfileDefaultValueEditWidget;
}

namespace armarx
{
    class MultiProfileDefaultValueEditWidget : public QWidget
    {
        Q_OBJECT

    public:
        explicit MultiProfileDefaultValueEditWidget(QString typeString, QStringList profileNames, QMap<QString, QString> values, Ice::CommunicatorPtr communicator, QWidget* parent = 0);
        ~MultiProfileDefaultValueEditWidget() override;
        QMap < QString, VariantContainerBasePtr> getVariantContainers() const; // @@@ obsolete
        QMap<QString, QString> getJsonMap() const;
    private slots:
        void on_comboBoxProfiles_currentIndexChanged(const QString& arg1);

    private:
        Ui::MultiProfileDefaultValueEditWidget* ui;

        QString typeString;
        Ice::CommunicatorPtr communicator;

        QMap<QString, QPointer<ProfileDefaultValueEditWidget> > defaultEdits;
    };


} // namespace armarx
