/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Valerij Wittenbeck (valerij.wittenbeck at student dot kit dot edu
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/


#pragma once


#include <ArmarXCore/observers/variant/VariantInfo.h>
#include <ArmarXCore/observers/variant/VariantContainer.h>

#include <Ice/ObjectFactory.h>
#include <QWidget>

#include <optional>

namespace armarx::Ui
{
    class ProfileDefaultValueEditWidget;
}

namespace armarx
{
    class ProfileDefaultValueEditWidget : public QWidget
    {
        Q_OBJECT

    private:
        enum Widget
        {
            eLineEdit = 0,
            eEditButton = 1,
            eSetButton = 2
        };

    public:
        explicit ProfileDefaultValueEditWidget(QString typeString, QString jsonValue, Ice::CommunicatorPtr communicator, QWidget* parent = 0);
        ~ProfileDefaultValueEditWidget() override;
        //        void setType(const QString& typeString);
        QString getType();
        void setEditable(bool editable);
        std::optional<QString> getValueAsString();
        std::optional<QString> getValueAsJson();
        VariantContainerBasePtr getVariantContainer();

    private slots:
        void resetValue();
        void setButtonClicked();
        void editDefaultButtonClicked();
    private:
        void initEditButton();

        Ui::ProfileDefaultValueEditWidget* ui;

        QString typeString;
        QString jsonValue;
        Ice::CommunicatorPtr communicator;

        bool editable;
    };


}
