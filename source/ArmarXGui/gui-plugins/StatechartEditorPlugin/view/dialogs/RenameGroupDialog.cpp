/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Valerij Wittenbeck (valerij.wittenbeck at student dot kit dot edu
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "RenameGroupDialog.h"
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/view/dialogs/ui_RenameGroupDialog.h>

#include <ArmarXCore/core/logging/Logging.h>
#include <QFileDialog>
#include <QTimer>

#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/cloning/GroupCloner.h>

#include <QtGui>

namespace armarx
{

    RenameGroupDialog::RenameGroupDialog(const StateTreeNodePtr& rootNode, const StateTreeModelPtr& treeModel, const StatechartGroupPtr& group, const GroupRenamerPtr& groupRenamer, QWidget* parent) :
        QDialog(parent),
        ui(new Ui::RenameGroupDialog),
        group(group),
        groupRenamer(groupRenamer),
        validGroupName("([a-zA-Z][a-zA-Z0-9]*)"),
        colorGreen(QColor::fromRgb(120, 255, 120)),
        colorRed(QColor::fromRgb(255, 120, 120)),
        colorYellow(QColor::fromRgb(255, 200, 0)),
        saveAll(false)
    {
        ui->setupUi(this);
        setOkButtonsEnabled(false);
        ui->editOldName->setText(group->getName());
        ui->editOldName->setReadOnly(true);
        //        ui->editOldName->setEnabled(false);

        ui->editNewName->setValidator(new QRegExpValidator(validGroupName, this));

        connect(ui->btnCancel, SIGNAL(clicked()), this, SLOT(reject()));
        connect(ui->btnSaveAndProceed, SIGNAL(clicked()), this, SLOT(saveAllProceedButtonClicked()));
        connect(ui->btnDontSaveAndProceed, SIGNAL(clicked()), this, SLOT(accept()));
        connect(ui->editNewName, SIGNAL(textChanged(QString)), this, SLOT(verifyNewName(QString)));

        for (const StateTreeNodePtr& c : rootNode->getChildren())
        {
            if (!c->isGroup())
            {
                continue;
            }

            auto childGroup = c->getGroup();
            allGroups.push_back(childGroup);

            QVector<statechartmodel::StatePtr> allGroupStates = childGroup->getAllStates(false);
            auto dependencies = GroupCloner::GetGroupsFromStates(treeModel, allGroupStates);
            bool isDependent = std::find_if(dependencies.constBegin(), dependencies.constEnd(), [&](const StatechartGroupPtr & g)
            {
                return g->getGroupPath() == group->getGroupPath();
            }) != dependencies.constEnd();

            if (isDependent && childGroup->getGroupPath() != group->getGroupPath())
            {
                dependantGroups.push_back(childGroup);
            }
        }

        ui->groupsWidget->setRowCount(dependantGroups.size() + 1);
        ui->groupsWidget->setColumnCount(1);
        ui->groupsWidget->horizontalHeader()->setResizeMode(QHeaderView::Stretch);
        ui->groupsWidget->setHorizontalHeaderLabels({"Affected Groups"});

        QTableWidgetItem* groupItem = new QTableWidgetItem(group->getName());
        groupItem->setFlags(groupItem->flags() ^ Qt::ItemIsEditable);
        ui->groupsWidget->setItem(0, 0, groupItem);

        for (int i = 0; i < dependantGroups.size(); ++i)
        {
            QTableWidgetItem* item = new QTableWidgetItem(dependantGroups[i]->getName());
            item->setFlags(item->flags() ^ Qt::ItemIsEditable);
            ui->groupsWidget->setItem(i + 1, 0, item);
        }
    }

    void RenameGroupDialog::setOkButtonsEnabled(bool enabled)
    {
        ui->btnDontSaveAndProceed->setEnabled(enabled);
        ui->btnSaveAndProceed->setEnabled(enabled);
    }

    RenameGroupDialog::~RenameGroupDialog()
    {
        delete ui;
    }

    StatechartGroupPtr RenameGroupDialog::getGroup() const
    {
        return group;
    }

    GroupRenamerPtr RenameGroupDialog::getGroupRenamer() const
    {
        return groupRenamer;
    }

    bool RenameGroupDialog::isSaveAllRequested() const
    {
        return saveAll;
    }

    QVector<StatechartGroupPtr> RenameGroupDialog::getDependantGroups() const
    {
        return dependantGroups;
    }

    QVector<StatechartGroupPtr> RenameGroupDialog::getAllGroups() const
    {
        return allGroups;
    }

    QString RenameGroupDialog::getNewName() const
    {
        return ui->editNewName->text();
    }

    void RenameGroupDialog::saveAllProceedButtonClicked()
    {
        saveAll = true;
        accept();
    }

    void RenameGroupDialog::verifyNewName(QString newName)
    {
        bool inUse = false;

        for (const auto& g : allGroups)
        {
            if (newName == g->getName())
            {
                inUse = true;
                break;
            }
        }

        QColor colorToUse;

        if (inUse)
        {
            ui->labelNewNameError->setText("Name already in use");
            colorToUse = colorRed;
            setOkButtonsEnabled(false);
        }
        else
        {
            ui->labelNewNameError->setText("Valid name");
            colorToUse = colorGreen;
            setOkButtonsEnabled(true);
        }

        QPalette p(ui->labelNewNameError->palette());
        p.setColor(ui->labelNewNameError->backgroundRole(), colorToUse);
        ui->labelNewNameError->setPalette(p);
    }

} // namespace armarx
