/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Valerij Wittenbeck (valerij.wittenbeck at student dot kit dot edu
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <QDialog>
#include <QList>

#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/renaming/StateRenamer.h>
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/model/StateTreeModel.h>

#include <ArmarXCore/observers/variant/VariantInfo.h>
#include <QTableWidgetItem>

namespace armarx::Ui
{
    class RenameStateDialog;
}

namespace armarx
{
    class RenameStateDialog : public QDialog
    {
        Q_OBJECT

    public:

        explicit RenameStateDialog(const StateTreeModelPtr& treeModel, const StatechartGroupPtr& sourceGroup, const statechartmodel::StatePtr& sourceState, QWidget* parent = 0);
        ~RenameStateDialog() override;
        StatechartGroupPtr getGroup() const;
        statechartmodel::StatePtr getState() const;
        QVector<StateRenamer::InstanceRenameInfo> getInstanceRenameInfos() const;
        bool isSaveAllRequested() const;
        QString getNewStateName() const;

    public slots:
        void saveAllProceedButtonClicked();
        void verifyNewName(QString newName);
        void verifyInstanceName(QTableWidgetItem* item);

    private:
        void setOkButtonsEnabled(bool enabled);
        Ui::RenameStateDialog* ui;
        StatechartGroupPtr sourceGroup;
        statechartmodel::StatePtr sourceState;
        QRegExp validStateNameRegExp;
        QRegExp validInstanceNameRegExp;

        QVector<statechartmodel::StatePtr> localStates;
        QVector<StateRenamer::InstanceRenameInfo> instanceRenameInfos;
        bool validNewName;
        bool validInstanceNames;

        QColor colorGreen;
        QColor colorRed;
        QColor colorYellow;

        bool saveAll;
        bool alreadyChecking;
    };


}
