/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "StatechartEditorParameterEditor.h"

#include <QAction>
#include <QCompleter>
#include <QDialogButtonBox>
#include <QLineEdit>
#include <QMenu>
#include <QMessageBox>
#include <QPushButton>
#include <QTableWidgetItem>
#include <QToolButton>
#include <QVBoxLayout>
#include <QComboBox>
#include <QDialog>
#include <QTextEdit>
#include <QInputDialog>
#include <QLabel>
#include <QStyledItemDelegate>
#include <QPropertyAnimation>

#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/InfixCompleter.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/MarkdownEditor.h>

//#include <ArmarXCore/statechart/StateParameter.h>
#include <ArmarXGui/gui-plugins/StatechartViewerPlugin/model/StateParameter.cpp>
#include <ArmarXCore/observers/variant/SingleTypeVariantList.h>
#include <ArmarXCore/observers/variant/StringValueMap.h>
#include <ArmarXCore/observers/variant/VariantContainer.h>

#include <ArmarXCore/observers/variant/Variant.h>
//#include <ArmarXCore/statechart/StateParameter.h>
#include <ArmarXCore/util/json/JSONObject.h>

#include <Ice/ObjectFactory.h>
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/view/widgets/ProfileDefaultValueEditWidget.h>

#include <memory>
#include <ArmarXGui/libraries/StructuralJson/JPathNavigator.h>
#include <ArmarXGui/libraries/StructuralJson/StructuralJsonParser.h>

namespace armarx
{
    void StatechartEditorParameterEditor::setStateInstance(statechartmodel::StateInstancePtr state)
    {
        this->state = state;
    }

    StatechartEditorParameterEditor::StatechartEditorParameterEditor(QWidget* parent, statechartmodel::StateInstancePtr state, const statechartmodel::StateParameterMap& params) :
        QTableWidget(parent),
        //    currentProfile(currentProfile),
        state(state),
        defaultValueState(Qt::Unchecked)
    {

        // For jsonobject double/float serilization on german/spanish pcs....
        setlocale(LC_ALL, "C");

        setColumnCount(6);
        qRegisterMetaType<statechartmodel::StateParameterMap>("statechartmodel::StateParameterMap");
        //    qRegisterMetaType<QVector<QString>>("QVector<QString>");
        connect(this, SIGNAL(buildRequested(statechartmodel::StateParameterMap)), this, SLOT(__buildFromMap(statechartmodel::StateParameterMap)), Qt::QueuedConnection);

        setHeaders();

        if (params.size() == 0)
        {
            addParameterRow();
        }
        else
        {
            buildFromMap(params);
        }



        setItemDelegateForColumn(eKey, &delegate);

        //        horizontalHeaderItem(eAddtoParent)->setT;
        connect(this, SIGNAL(rowAdded(int)), this, SLOT(addCustomColumn(int)));
        connect(this, SIGNAL(rowFilled(int, QString)), this, SLOT(fillCustomColumn(int, QString)));

        connect(this, SIGNAL(buildRequested(statechartmodel::StateParameterMap)), this, SLOT(init(statechartmodel::StateParameterMap)));
    }



    void StatechartEditorParameterEditor::addCustomColumn(int row)
    {


        AnimatedToolButton* showHelpButton = new AnimatedToolButton(this);


        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icons/help-about.ico"), QSize(), QIcon::Normal, QIcon::Off);
        showHelpButton->setIcon(icon);
        showHelpButton->setToolTip("Show or edit help to this parameter.");
        connect(showHelpButton, SIGNAL(clicked()), this, SLOT(showHelpDialog()));

        if (state && state->getParent())
        {

            QAction* addInputToParentAction = new QAction(showHelpButton);
            QIcon icon;
            icon.addFile(QString::fromUtf8(":/icons/add.png"), QSize(), QIcon::Normal, QIcon::Off);
            addInputToParentAction->setIcon(icon);
            addInputToParentAction->setIconVisibleInMenu(true);
            addInputToParentAction->setText("Add to parent's input");
            addInputToParentAction->setToolTip("Add this parameter to parent's input. This applies immediatly.");
            addInputToParentAction->setData(row);
            connect(addInputToParentAction, SIGNAL(triggered()), this, SLOT(addInputToParent()));

            QMenu* subMenu = new QMenu(showHelpButton);
            subMenu->addAction(addInputToParentAction);
            showHelpButton->setMenu(subMenu);
        }

        setCellWidget(row, eAdditionalButtons, showHelpButton);
    }

    void StatechartEditorParameterEditor::fillCustomColumn(int row, const QString& key)
    {
        QToolButton* showHelpButton = qobject_cast<QToolButton*>(cellWidget(row, eAdditionalButtons));

        if (showHelpButton)
        {
            auto it = originalParamMap.find(key);

            if (it != originalParamMap.end())
            {
                statechartmodel::StateParameterPtr pm = it.value();

                if (pm)
                {
                    showHelpButton->setProperty("MarkdownDocText", pm->description);
                    QColor color = pm->description.isEmpty() ? QColor {250, 0, 0}:
                                   QColor {0, 0, 250};

                    QPalette pal = showHelpButton->palette();
                    showHelpButton->setAutoFillBackground(true);
                    QPropertyAnimation* animation = new QPropertyAnimation(showHelpButton, "color", this);
                    animation->setDuration(3000);
                    float t = 0;
                    while (t < 1)
                    {
                        animation->setKeyValueAt(t, color);
                        t += 0.1f;
                        animation->setKeyValueAt(t, pal.color(QPalette::Button));
                        t += 0.1f;
                    }
                    animation->setKeyValueAt(1, pal.color(QPalette::Button));
                    animation->setEasingCurve(QEasingCurve::InOutQuad);
                    animation->start();

                }

                originalParamMap.erase(it);
            }

        }
    }

    void StatechartEditorParameterEditor::showHelpDialog()
    {

        QDialog editDefaultDialog;
        editDefaultDialog.setWindowTitle("Statechart Parameter Documentation Editor");
        editDefaultDialog.resize(QSize(600, 400));
        MarkdownEditor* dialogTextEdit = new MarkdownEditor(this);
        dialogTextEdit->setPlainText(sender()->property("MarkdownDocText").toString());

        QVBoxLayout* layout = new QVBoxLayout;
        layout->addWidget(dialogTextEdit);
        QDialogButtonBox* buttonBox = new QDialogButtonBox(dialogTextEdit);
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel | QDialogButtonBox::Ok);
        layout->addWidget(buttonBox);
        editDefaultDialog.setLayout(layout);

        connect(buttonBox, SIGNAL(accepted()), &editDefaultDialog, SLOT(accept()));
        connect(buttonBox, SIGNAL(rejected()), &editDefaultDialog, SLOT(reject()));

        if (editDefaultDialog.exec() == QDialog::Accepted)
        {
            sender()->setProperty("MarkdownDocText", dialogTextEdit->toPlainText());
        }

    }

    void StatechartEditorParameterEditor::addInputToParent()
    {

        int row;
        QAction* action = qobject_cast<QAction*>(sender());
        row = action->data().toInt();
        QString keyName = item(row, eKey)->text();
        ARMARX_CHECK_EXPRESSION(state);
        auto parent = state->getParent();
        ARMARX_CHECK_EXPRESSION(parent);
        ARMARX_CHECK_EXPRESSION(keyName.length() > 0);
        auto inputAndLocal = parent->getInputAndLocalParameters();

        if (inputAndLocal.count(keyName) > 0)
        {
            QMessageBox::warning(this, "State Editing Error", "The key '" + keyName + "' already exists");
            return;
        }

        statechartmodel::StateParameterMap input = parent->getInputParameters();
        input.insert(keyName, getStateParameter(row));
        state->getParent()->setInputParameters(input);
    }

    void StatechartEditorParameterEditor::init(const statechartmodel::StateParameterMap& params)
    {
        auto profileNames = getRelevantProfiles();
        int columnCount = eEditableValue + profileNames.size();

        setColumnCount(columnCount);
        setColumnWidth(eAdditionalButtons, 35);

        if (!horizontalHeaderItem(eAdditionalButtons))
        {
            setHorizontalHeaderItem(eAdditionalButtons, new QTableWidgetItem(""));
        }

        for (int i = 0; i < profileNames.size(); ++i)
        {
            setColumnWidth(eEditableValue + i, 200);
            setHorizontalHeaderItem(eEditableValue + i, new QTableWidgetItem(profileNames[i]));
        }


        if (state && state->getParent())
        {
            //            horizontalHeaderItem(eAddtoParent)->setText();
        }
    }

    void StatechartEditorParameterEditor::setKeyBlackList(const QSet<QString>& keyBlackList)
    {
        this->keyBlackList = keyBlackList;
    }

    QSet<QString> StatechartEditorParameterEditor::getKeys() const
    {
        QSet<QString> result;

        for (int row = 0; row < rowCount(); row++)
        {
            if (item(row, eKey) && !item(row, eKey)->text().isEmpty())
            {
                result.insert(item(row, eKey)->text());
            }
        }

        return result;
    }

    QSet<QString> StatechartEditorParameterEditor::getTypes() const
    {
        QSet<QString> types;
        for (int i = 0; i < rowCount(); ++i)
        {
            types.insert(getType(i));
        }
        return types;
    }

    statechartmodel::StateParameterPtr StatechartEditorParameterEditor::getStateParameter(int row) const
    {
        if (getKey(row).isEmpty())
        {
            return statechartmodel::StateParameterPtr();
        }

        statechartmodel::StateParameterPtr p = std::make_shared<statechartmodel::StateParameter>();
        p->type = getType(getKey(row));
        p->optional = getIsOptional(row);

        if (auto descriptionWidget = cellWidget(row, StatechartEditorParameterEditor::eAdditionalButtons))
        {
            p->description = descriptionWidget->property("MarkdownDocText").toString();
        }

        auto jsonValues = getProfileJsonMap(getKey(row));
        auto variants = getProfileVariantContainerMap(row);

        for (const auto& profileName : getRelevantProfiles())
        {
            p->profileDefaultValues[profileName] = {variants[profileName], jsonValues[profileName]};
        }

        return p;
    }

    statechartmodel::StateParameterMap StatechartEditorParameterEditor::getStateParameters() const
    {
        statechartmodel::StateParameterMap result;

        for (int row = 0; row < rowCount(); ++row)
        {
            if (auto param = getStateParameter(row))
            {
                result[getKey(row)] = param;
            }
        }

        return result;
    }

    //StateParameterMap StatechartEditorParameterEditor::getStateParametersWithoutValue()
    //{
    //    StateParameterMap result;
    //    for (int row = 0; row < rowCount(); ++row) {
    //        StateParameterIceBasePtr param = new StateParameterIceBase();
    //if(getKey(row).length() == 0)
    //    continue;

    //    }
    //}

    QMap<QString, QString> StatechartEditorParameterEditor::getValuesAsString(int row) const
    {
        if (row >= rowCount() || row < 0)
        {
            throw LocalException("row index out of range: ") << row;
        }

        QMap<QString, QString> result;

        auto profileNames = getRelevantProfiles();

        for (int i = 0; i < profileNames.size(); ++i)
        {
            auto widget = qobject_cast<ProfileDefaultValueEditWidget*>(cellWidget(row, eEditableValue + i));

            if (auto value = widget->getValueAsString())
            {
                result[profileNames[i]] = *value;
            }
        }

        return result;
    }

    QMap<QString, QString> StatechartEditorParameterEditor::getProfileJsonMap(QString key) const
    {
        int row = -1;

        for (int i = 0; i < rowCount(); i++)
        {
            if (getKey(i) == key)
            {
                row = i;
                break;
            }
        }

        if (row >= rowCount() || row < 0)
        {
            throw LocalException("row index out of range: ") << row;
        }

        QMap<QString, QString> result;

        auto profileNames = getRelevantProfiles();

        for (int i = 0; i < profileNames.size(); ++i)
        {
            auto widget = qobject_cast<ProfileDefaultValueEditWidget*>(cellWidget(row, eEditableValue + i));

            if (auto value = widget->getValueAsJson())
            {
                result[profileNames[i]] = *value;
            }
        }

        return result;
    }


    QMap<QString, VariantContainerBasePtr> StatechartEditorParameterEditor::getProfileVariantContainerMap(int row) const
    {
        if (row >= rowCount() || row < 0)
        {
            throw LocalException("row index out of range: ") << row;
        }

        QMap<QString, VariantContainerBasePtr> result;

        auto profileNames = getRelevantProfiles();

        for (int i = 0; i < profileNames.size(); ++i)
        {
            auto widget = qobject_cast<ProfileDefaultValueEditWidget*>(cellWidget(row, eEditableValue + i));
            result[profileNames[i]] = widget->getVariantContainer();
        }

        return result;
    }

    QString StatechartEditorParameterEditor::getKey(int row) const
    {
        if (row >= rowCount())
        {
            throw LocalException("row index out of range: ") << row;
        }

        if (!item(row, eKey))
        {
            return "";
        }

        return item(row, eKey)->text();
    }

    QString StatechartEditorParameterEditor::getType(int row) const
    {
        if (row >= rowCount())
        {
            throw LocalException("row index out of range: ") << row;
        }

        QComboBox* CBvalueType = qobject_cast<QComboBox*>(cellWidget(row, 1));

        if (!CBvalueType)
        {
            return "";
        }

        QString type = getBaseNameFromHumanName(CBvalueType->currentText()); //@@@
        return type;

    }

    QString StatechartEditorParameterEditor::getType(QString key) const
    {
        int row = getRow(key);
        return getType(row);
    }

    int StatechartEditorParameterEditor::getRow(const QString& key) const
    {
        int row = -1;

        for (int i = 0; i < rowCount(); i++)
        {
            if (getKey(i) == key)
            {
                row = i;
                break;
            }
        }

        if (row == -1)
        {
            throw LocalException("could not find key ") << key.toStdString();
        }

        return row;
    }

    bool StatechartEditorParameterEditor::getIsOptional(int row) const
    {
        if (row >= rowCount())
        {
            throw LocalException("row index out of range: ") << row;
        }

        QComboBox* cbOptional = qobject_cast<QComboBox*>(cellWidget(row, eOptional));
        return cbOptional->currentText() == "true";
    }


    int StatechartEditorParameterEditor::addParameterRow()
    {
        //    ARMARX_IMPORTANT_S << "Adding row";
        int row = rowCount();
        insertRow(row);
        setItem(row, eKey, new QTableWidgetItem());

        //    setItem(row, eProvideDefaultValue, new QTableWidgetItem());
        //    item(row, eProvideDefaultValue)->setFlags(Qt::ItemIsEnabled | Qt::ItemIsUserCheckable);
        //    item(row, eProvideDefaultValue)->setCheckState(defaultValueState);

        QComboBox* valueTypebox = new QComboBox;
        valueTypebox->setEditable(true);
        //    valueTypebox->setInsertPolicy(QComboBox::InsertAlphabetically);
        //    valueTypebox->setValidator(new QIntValidator(this));
        setCellWidget(row, eType, valueTypebox);
        QStringList types = addVariantTypesToComboBox(valueTypebox);
        InfixCompleter* fullCompleter = new InfixCompleter(types, this);
        fullCompleter->setCompletionMode(QCompleter::PopupCompletion);
        fullCompleter->setCaseSensitivity(Qt::CaseInsensitive);
        valueTypebox->setCompleter(fullCompleter);
        valueTypebox->setEditText("string");
        connect(valueTypebox, SIGNAL(editTextChanged(QString)), this, SLOT(typeCbChanged(QString)));
        connect(valueTypebox->lineEdit(), SIGNAL(textEdited(QString)), fullCompleter, SLOT(setCompletionInfix(QString)));
        //valueTypebox->model()->sort(0);
        QComboBox* cbOptional;
        cbOptional = new QComboBox();
        cbOptional->addItems(QString("true;false").split(";"));
        cbOptional->setCurrentIndex(1);
        setCellWidget(row, eOptional, cbOptional);


        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icons/dialog-close.ico"), QSize(), QIcon::Normal, QIcon::Off);
        QToolButton* deleteButton = new QToolButton(this);
        deleteButton->setIcon(icon);
        deleteButton->setToolTip("Add this parameter to parent's input");
        deleteButton->setToolTip("Delete this row");
        setCellWidget(row, eDeleteButton, deleteButton);
        connect(deleteButton, SIGNAL(clicked()), this, SLOT(deleteRow()));

        auto profileNames = getRelevantProfiles();

        for (int i = 0; i < profileNames.size(); ++i)
        {
            ProfileDefaultValueEditWidget* valueWidget = new ProfileDefaultValueEditWidget(getBaseNameFromHumanName(valueTypebox->currentText()), QString(), communicator);
            setCellWidget(row, eEditableValue + i, valueWidget);
        }

        emit rowAdded(row);
        return row;
    }


    void StatechartEditorParameterEditor::addParameterRow(QString key, QString variantIdStr, QVector<QString> values, bool optional)
    {
        int row = addParameterRow();

        item(row, eKey)->setText(key);

        QComboBox* valueTypeBox = qobject_cast<QComboBox*>(cellWidget(row, eType));
        //    ARMARX_INFO_S << "type id: " << variantIdStr.toStdString() <<  " size: " << valueTypeBox->count();
        valueTypeBox->setEditText(getHumanNameFromBaseName(variantIdStr));

        QComboBox* optionalBox = qobject_cast<QComboBox*>(cellWidget(row, eOptional));
        optionalBox->setCurrentIndex(optional ? 0 : 1);

        for (int i = 0; i < values.size(); ++i)
        {
            ProfileDefaultValueEditWidget* valueWidget = new ProfileDefaultValueEditWidget(variantIdStr, values[i], communicator);
            setCellWidget(row, eEditableValue + i, valueWidget);
        }

        emit rowFilled(row, key);
    }

    void StatechartEditorParameterEditor::typeCbChanged(const QString&)
    {
        QWidget* wid = qobject_cast<QWidget*>(sender());

        /*QModelIndex mIndex =*/ indexAt(wid->pos()); // Some strange bug happens here: If this is used, it will deliver on table creation zero
        // If indexAt is not called, wid->pos() delivers a 0,0 Point and then rowAt() also return wrong index
        //    ARMARX_INFO_S << VAROUT(wid->pos()) << VAROUT(rowAt(wid->pos().y()));

        int row = rowAt(wid->pos().y());

        QComboBox* typeBox = qobject_cast<QComboBox*>(cellWidget(row, eType));
        QString typeStr = getBaseNameFromHumanName(typeBox->currentText()); //@@@

        for (int i = eEditableValue; i < columnCount(); ++i)
        {
            if (ProfileDefaultValueEditWidget* valueWidget = qobject_cast<ProfileDefaultValueEditWidget*>(cellWidget(row, i)))
            {
                if (typeStr != valueWidget->getType())
                {
                    auto newWidget = new ProfileDefaultValueEditWidget(typeStr, QString(), communicator);
                    setCellWidget(row, i, newWidget);
                }

                //            valueWidget->setType(typeStr);
            }
        }
        emit typeChanged(row, typeStr);
    }


    void StatechartEditorParameterEditor::deleteRow()
    {
        int row;
        QWidget* wid = qobject_cast<QWidget*>(sender());
        QModelIndex mIndex = indexAt(wid->pos());
        row = mIndex.row();

        disconnect(this);
        removeRow(row);

        if (rowCount() == 0)
        {
            addParameterRow();
        }

        connectUserEditSlots();
    }

    void StatechartEditorParameterEditor::checkAndUpdateRowCount(int row, int column)
    {
        //    ARMARX_IMPORTANT_S << "checking row: " << row << ", " << column;

        //    if(column == eProvideDefaultValue)
        //    {
        //        if(item(row,eProvideDefaultValue)->checkState() == Qt::Unchecked)
        //        {
        //            QWidget* valueWidget = cellWidget(row, eValue);
        //            QWidget* valueLineEdit = qobject_cast<QWidget*>(valueWidget);
        //            if(valueLineEdit){
        //                valueLineEdit->setEnabled(false);
        //            }
        //        }
        //        else
        //        {
        //            QWidget* valueWidget = cellWidget(row, eValue);
        //            QWidget* valueLineEdit = qobject_cast<QWidget*>(valueWidget);
        //            if(valueLineEdit){
        //                valueLineEdit->setEnabled(true);
        //            }
        //        }
        //    }
        if (column == eKey)
        {
            auto keyitem = item(row, eKey);

            if (keyitem)
            {
                if ((keyBlackList.find(keyitem->text()) != keyBlackList.end() || findItems(keyitem->text(), Qt::MatchExactly).size() > 1))
                {
                    keyitem->setText(item(row, eKey)->text() + "_2");
                    ARMARX_WARNING_S << "Keys must be unique (input and local parameters share the same key pool)";
                }
            }
        }

        if (row >= rowCount() - 1 && item(rowCount() - 1, 0) && !item(rowCount() - 1, 0)->text().isEmpty())
        {
            addParameterRow();
        }
    }

    void StatechartEditorParameterEditor::refreshVariantTypes()
    {
        for (int row = 0; row < rowCount(); row++)
        {
            QComboBox* valueTypeBox = qobject_cast<QComboBox*>(cellWidget(row, eType));
            addVariantTypesToComboBox(valueTypeBox);
            //        valueTypeBox->model()->sort(0);
        }

    }

    void StatechartEditorParameterEditor::connectUserEditSlots()
    {
        connect(this, SIGNAL(cellEntered(int, int)), this, SLOT(checkAndUpdateRowCount(int, int)), Qt::UniqueConnection);
        connect(this, SIGNAL(cellPressed(int, int)), this, SLOT(checkAndUpdateRowCount(int, int)), Qt::UniqueConnection);
        connect(this, SIGNAL(cellChanged(int, int)), this, SLOT(checkAndUpdateRowCount(int, int)), Qt::UniqueConnection);
    }

    QStringList StatechartEditorParameterEditor::addVariantTypesToComboBox(QComboBox* combo)
    {
        if (!combo)
        {
            return QStringList();
        }

        combo->clear();


        auto types = Variant::getTypes();
        QStringList list;
        for (std::pair<VariantTypeId, std::string> pair : types)
        {
            QString typeName = tr(pair.second.c_str());
            //QString humanName = tr(Variant::getHumanName(pair->first).c_str());

            if (typeName.contains("Invalid"))
            {
                continue;
            }

            list.append(getHumanNameFromBaseName(typeName));
            /*if(combo->findText(typeName) == -1)
            {
                combo->addItem(getHumanNameFromBaseName(typeName));
            }*/
        }

        std::vector<VariantContainerType> containers;
        containers.push_back(VariantType::List);
        containers.push_back(VariantType::Map);

        for (VariantContainerType c : containers)
        {
            for (auto it : types)
            {
                QString typeName = tr(it.second.c_str());


                if (typeName.contains("Invalid"))
                {
                    continue;
                }

                std::string typeStr = VariantContainerType::allTypesToString(c(it.first).clone());
                typeName = QString::fromStdString(typeStr);
                list.append(getHumanNameFromBaseName(typeName));
                /*if(combo->findText(typeName) == -1)
                {
                    combo->addItem(getHumanNameFromBaseName(typeName));
                }*/
            }
        }

        qSort(list.begin(), list.end(), compareVariantNames);
        combo->addItems(list);
        return list;
    }

    QString StatechartEditorParameterEditor::getHumanNameFromBaseName(QString variantBaseTypeName) const
    {
        if (!variantInfo)
        {
            return variantBaseTypeName;
        }

        std::string humanName = variantInfo->getNestedHumanNameFromBaseName(variantBaseTypeName.toUtf8().data());

        if (humanName.empty())
        {
            return variantBaseTypeName;
        }

        return QString::fromUtf8(humanName.c_str());
    }

    QString StatechartEditorParameterEditor::getBaseNameFromHumanName(QString humanName) const
    {
        if (!variantInfo)
        {
            return humanName;
        }

        std::string variantBaseTypeName = variantInfo->getNestedBaseNameFromHumanName(humanName.toUtf8().data());

        if (variantBaseTypeName.empty())
        {
            return humanName;
        }

        return QString::fromUtf8(variantBaseTypeName.c_str());
    }

    bool StatechartEditorParameterEditor::compareVariantNames(const QString& a, const QString& b)
    {
        bool pa, pb;

        // list nested types after basic ones
        pa = a.contains("(");
        pb = b.contains("(");

        if (pa != pb)
        {
            return pb;
        }

        // list human types before non resolved ones
        pa = a.contains(":");
        pb = b.contains(":");

        if (pa != pb)
        {
            return pb;
        }

        // nested lower case (basic types) first
        int ia = a.indexOf("(");
        int ib = b.indexOf("(");
        pa = ia > 0 && a.count() > ia + 1 && a[ia + 1].isLower();
        pb = ib > 0 && b.count() > ib + 1 && b[ib + 1].isLower();
        if (pa != pb && a.left(ia) == b.left(ib))
        {
            return !pb;
        }

        // lower case (basic types) first
        pa = a.count() > 0 && a[0].isLower();
        pb = b.count() > 0 && b[0].isLower();

        if (pa != pb)
        {
            return pa;
        }

        return a.compare(b) < 0;
    }
    Qt::CheckState StatechartEditorParameterEditor::getDefaultValueState() const
    {
        return defaultValueState;
    }

    void StatechartEditorParameterEditor::setDefaultValueState(const Qt::CheckState& value)
    {
        defaultValueState = value;
    }

    void StatechartEditorParameterEditor::hideValueColumns()
    {
        const auto& relevantProfiles = getRelevantProfiles();

        for (int i = 0; i < relevantProfiles.size(); ++i)
        {
            hideColumn(eEditableValue + i);
        }
    }

    void StatechartEditorParameterEditor::setHeaders()
    {
        QStringList headerLabels {"Key", "Type", "Optional", "Del"};
        setColumnWidth(eKey, 180);
        setColumnWidth(eType, 225);
        setColumnWidth(eOptional, 70);
        setColumnWidth(eDeleteButton, 25);
        //    setColumnWidth(eAdditionalButtons, 25);

        auto profileNames = getRelevantProfiles();
        int col = eEditableValue;

        for (int i = 0; i < profileNames.size(); ++i)
        {
            setColumnWidth(col + i, 200);
            headerLabels.push_back(profileNames[i]);
        }

        setHorizontalHeaderLabels(headerLabels);
    }

    void StatechartEditorParameterEditor::buildFromMap(const statechartmodel::StateParameterMap& map)
    {
        originalParamMap = map;
        emit buildRequested(map);
    }

    void StatechartEditorParameterEditor::__buildFromMap(const statechartmodel::StateParameterMap& map)
    {
        clearContents();
        QWidget* tempW = new QLabel("mylabel", this);
        tempW->deleteLater();
        setRowCount(0);
        //    disconnect(this);
        disconnect(this, SIGNAL(cellEntered(int, int)));
        disconnect(this, SIGNAL(cellPressed(int, int)));
        disconnect(this, SIGNAL(cellChanged(int, int)));

        auto relevantProfiles = getRelevantProfiles();

        for (const auto& entry : map.toStdMap())
        {
            const QString& name = entry.first;
            const statechartmodel::StateParameterPtr& param = entry.second;

            //JSONObjectPtr json = new JSONObject(communicator);
            QVector<QString> jsonStrs;
            QString typeStr = param->type;

            for (const QString& profile : relevantProfiles)
            {
                QString jsonStr = param->profileDefaultValues[profile].second;
                jsonStrs.push_back(jsonStr);
            }

            addParameterRow(name, typeStr, jsonStrs, param->optional);

        }

        addParameterRow();
        connectUserEditSlots();
        //    setHeaders();

    }

    QVector<QString> StatechartEditorParameterEditor::getRelevantProfiles() const
    {
        if (!currentProfile)
        {
            return QVector<QString>();
        }

        QVector<QString> result {QString::fromUtf8(currentProfile->getName().c_str())};
        auto profile = currentProfile;

        while ((profile = profile->getParent()))
        {
            result.push_back(QString::fromUtf8(profile->getName().c_str()));
        }

        return result;
    }

    QWidget* StatechartEditorParameterEditor::LineEditDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const
    {
        QLineEdit* lineEdit = new QLineEdit(parent);
        QString regExpStr("(|([a-z_]{1})([a-z_0-9]*))");
        QRegExp reg(regExpStr, Qt::CaseInsensitive);
        lineEdit->setValidator(new QRegExpValidator(reg, lineEdit));
        return lineEdit;
    }




    void AnimatedToolButton::setColor(QColor color)
    {
        QString val = QString("background-color: rgb(%1, %2, %3);").arg(color.red()).arg(color.green()).arg(color.blue());
        //    ARMARX_INFO << "Setting value to " << val.toStdString();
        setStyleSheet(val);
    }
}
