/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Valerij Wittenbeck (valerij.wittenbeck at student dot kit dot edu
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <QDialog>
#include <QList>

#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/renaming/GroupRenamer.h>

#include <ArmarXCore/observers/variant/VariantInfo.h>
#include <QTableWidgetItem>

namespace armarx::Ui
{
    class RenameGroupDialog;
}

namespace armarx
{
    class RenameGroupDialog : public QDialog
    {
        Q_OBJECT

    public:
        explicit RenameGroupDialog(const StateTreeNodePtr& rootNode, const StateTreeModelPtr& treeModel, const StatechartGroupPtr& group, const GroupRenamerPtr& groupRenamer, QWidget* parent = 0);
        ~RenameGroupDialog() override;
        ArmarXPackageToolInterfacePtr getPackageTool() const;
        QString getPackageName() const;
        QString getPackagePath() const;
        StatechartGroupPtr getGroup() const;
        GroupRenamerPtr getGroupRenamer() const;
        bool isSaveAllRequested() const;
        QVector<StatechartGroupPtr> getDependantGroups() const;
        QVector<StatechartGroupPtr> getAllGroups() const;
        QString getNewName() const;

    public slots:
        void saveAllProceedButtonClicked();
        void verifyNewName(QString newName);

    private:
        void setOkButtonsEnabled(bool enabled);
        Ui::RenameGroupDialog* ui;
        StatechartGroupPtr group;
        GroupRenamerPtr groupRenamer;
        QRegExp validGroupName;

        QVector<StatechartGroupPtr> allGroups;
        QVector<StatechartGroupPtr> dependantGroups;

        QColor colorGreen;
        QColor colorRed;
        QColor colorYellow;

        bool saveAll;
    };


}
