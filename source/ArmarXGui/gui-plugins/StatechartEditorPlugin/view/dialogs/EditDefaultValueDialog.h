/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Valerij Wittenbeck (valerij.wittenbeck at student dot kit dot edu
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <QDialog>
#include <QList>


#include <QTableWidgetItem>

#include <Ice/Communicator.h>


namespace armarx::Ui
{
    class EditDefaultValueDialog;
}

namespace armarx
{
    class EditDefaultValueDialog : public QDialog
    {
        Q_OBJECT

    public:
        explicit EditDefaultValueDialog(QString json, QString typeString, const Ice::CommunicatorPtr& communicator, QWidget* parent = 0);
        ~EditDefaultValueDialog() override;

        QString getJson();

    private slots:
        void onJsonTextChanged();
        void onFormat();
        void onShowInternalJson();
        void onImportInternalJson();

    private:
        Ui::EditDefaultValueDialog* ui;
        QString typeString;
        Ice::CommunicatorPtr communicator;
    };


}
