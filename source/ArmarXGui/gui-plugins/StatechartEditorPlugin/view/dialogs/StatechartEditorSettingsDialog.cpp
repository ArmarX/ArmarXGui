/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "StatechartEditorSettingsDialog.h"
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/view/dialogs/ui_StatechartEditorSettingsDialog.h>

#include <QFileDialog>
#include <QtGui>

#include <ArmarXCore/core/system/ArmarXDataPath.h>

namespace armarx
{

    StatechartEditorSettingsDialog::StatechartEditorSettingsDialog(QWidget* parent) :
        QDialog(parent),
        ui(new Ui::StatechartEditorSettingsDialog)
    {
        ui->setupUi(this);
        connect(ui->btnAddPath, SIGNAL(clicked()), this, SLOT(selectNewPath()));
    }

    StatechartEditorSettingsDialog::~StatechartEditorSettingsDialog()
    {
        delete ui;
    }

    QStringList StatechartEditorSettingsDialog::getPaths() const
    {
        return ui->textEditSearchPaths->toPlainText().split(";");
    }

    void StatechartEditorSettingsDialog::setRemoteStatesLocked(bool locked)
    {
        ui->cbLockRemoteStates->setChecked(locked);
    }

    bool StatechartEditorSettingsDialog::getRemoteStatesLocked() const
    {
        return ui->cbLockRemoteStates->isChecked();
    }

    void StatechartEditorSettingsDialog::setPaths(QStringList paths)
    {
        ui->textEditSearchPaths->setText(paths.join(";"));
    }

    void StatechartEditorSettingsDialog::selectNewPath()
    {
        QFileDialog selectFolder(this, "Select new search folder");
        //        selectFolder.setOption(QFileDialog::ShowDirsOnly, true);
        selectFolder.setOption(QFileDialog::ReadOnly, true);
        selectFolder.setOption(QFileDialog::HideNameFilterDetails, false);
        selectFolder.setFileMode(QFileDialog::Directory);
        QList<QUrl> urls;
        urls << QUrl::fromLocalFile(QDesktopServices::storageLocation(QDesktopServices::HomeLocation))
             << QUrl::fromLocalFile(QDesktopServices::storageLocation(QDesktopServices::DesktopLocation));

        if (!ArmarXDataPath::getHomePath().empty())
        {
            urls << QUrl::fromLocalFile(QString::fromStdString(ArmarXDataPath::getHomePath()));
        }

        selectFolder.setSidebarUrls(urls);

        if (selectFolder.exec() == QDialog::Accepted)
        {
            if (ui->textEditSearchPaths->toPlainText().isEmpty())
            {
                ui->textEditSearchPaths->setText(*selectFolder.selectedFiles().begin());
            }
            else
            {
                ui->textEditSearchPaths->setText(ui->textEditSearchPaths->toPlainText() + ";\n" + *selectFolder.selectedFiles().begin());
            }


        }
    }

}
