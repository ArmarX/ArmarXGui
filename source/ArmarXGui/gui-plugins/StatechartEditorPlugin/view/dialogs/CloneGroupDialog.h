/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Valerij Wittenbeck (valerij.wittenbeck at student dot kit dot edu
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <QDialog>
#include <QList>

#include <ArmarXCore/core/system/cmake/ArmarXPackageToolInterface.h>
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/cloning/GroupCloner.h>

#include <ArmarXCore/observers/variant/VariantInfo.h>
#include <QTableWidgetItem>

namespace armarx::Ui
{
    class CloneGroupDialog;
}

namespace armarx
{
    class CloneGroupDialog : public QDialog
    {
        Q_OBJECT

    public:
        explicit CloneGroupDialog(ArmarXPackageToolInterfacePtr packageTool, StatechartGroupPtr group, GroupClonerPtr groupCloner, QWidget* parent = 0);
        ~CloneGroupDialog() override;
        ArmarXPackageToolInterfacePtr getPackageTool() const;
        QString getPackageName() const;
        QString getPackagePath() const;
        StatechartGroupPtr getGroup() const;
        GroupClonerPtr getGroupCloner() const;
        StatechartGroupMapping getStatechartGroupMapping() const;
        QVector<QPair<StatechartGroupPtr, QString>> getGroupsToClone() const;
        QString getGroupPrefix() const;

    public slots:
        void verifyName(QTableWidgetItem* item);
        void requestCheckPackagePath(QString path);
        void requestPrefixUpdate(QString prefix);
        void selectPackagePath();
        void checkPackagePath();
        void cloneRobotSkillTemplatesToggled(bool state);

    private:
        void buildGroupList();

        Ui::CloneGroupDialog* ui;
        ArmarXPackageToolInterfacePtr packageTool;
        StatechartGroupPtr group;
        GroupClonerPtr groupCloner;
        StatechartGroupMapping mapping;
        QVector<StatechartGroupPtr> deps;
        QVector<StatechartGroupPtr> groupsToClone;
        QString groupPrefix;
        QTimer* timer;
        QRegExp validGroupNameRegExp;

        QColor colorGreen;
        QColor colorRed;
        QColor colorYellow;

        bool alreadyChecking;
        void updateGroupDependencies();
    };


}
