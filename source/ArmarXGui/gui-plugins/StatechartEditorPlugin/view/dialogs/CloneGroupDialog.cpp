/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Valerij Wittenbeck (valerij.wittenbeck at student dot kit dot edu
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "CloneGroupDialog.h"
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/view/dialogs/ui_CloneGroupDialog.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <QFileDialog>
#include <QTimer>

#include <filesystem>

#include <ArmarXCore/core/system/ArmarXDataPath.h>

#include <QtGui>
#include <QPushButton>

namespace armarx
{

    CloneGroupDialog::CloneGroupDialog(ArmarXPackageToolInterfacePtr packageTool, StatechartGroupPtr group, GroupClonerPtr groupCloner, QWidget* parent) :
        QDialog(parent),
        ui(new Ui::CloneGroupDialog),
        packageTool(packageTool),
        group(group),
        groupCloner(groupCloner),
        validGroupNameRegExp("([a-zA-Z][a-zA-Z0-9]*)"),
        colorGreen(QColor::fromRgb(120, 255, 120)),
        colorRed(QColor::fromRgb(255, 120, 120)),
        colorYellow(QColor::fromRgb(255, 200, 0)),
        alreadyChecking(false)
    {
        ui->setupUi(this);

        connect(ui->btnSelectPackageFolder, SIGNAL(clicked()), this, SLOT(selectPackagePath()));
        connect(ui->editPackagePath, SIGNAL(textChanged(QString)), this, SLOT(requestCheckPackagePath(QString)));
        connect(ui->editGroupPrefix, SIGNAL(textChanged(QString)), this, SLOT(requestPrefixUpdate(QString)));
        connect(ui->groupsWidget, SIGNAL(itemChanged(QTableWidgetItem*)), this, SLOT(verifyName(QTableWidgetItem*)));
        connect(ui->checkBoxCloneRobotSkillTemplates, SIGNAL(toggled(bool)), this, SLOT(cloneRobotSkillTemplatesToggled(bool)));

        ui->editGroupPrefix->setEnabled(false);
        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);

        ui->editGroupPrefix->setValidator(new QRegExpValidator(validGroupNameRegExp, this));

        timer = new QTimer(this);
        connect(timer, SIGNAL(timeout()), this, SLOT(checkPackagePath()));
        timer->setSingleShot(true);
    }

    CloneGroupDialog::~CloneGroupDialog()
    {
        delete ui;
    }

    ArmarXPackageToolInterfacePtr CloneGroupDialog::getPackageTool() const
    {
        return packageTool;
    }

    QString CloneGroupDialog::getPackageName() const
    {
        std::filesystem::path path = getPackagePath().toUtf8().data();
        return QString::fromUtf8(path.filename().c_str());
    }

    QString CloneGroupDialog::getPackagePath() const
    {
        std::filesystem::path path = ui->editPackagePath->text().toUtf8().data();
        std::filesystem::path cleanPath = path;

        try
        {
            cleanPath = std::filesystem::canonical(path);
        }
        catch (...)
        {
            cleanPath = ArmarXDataPath::cleanPath(path.string());

            if (*cleanPath.string().rbegin() == '/' || *cleanPath.string().rbegin() == '\\')
            {
                cleanPath = cleanPath.remove_filename();
            }
        }

        return QString::fromUtf8(cleanPath.c_str());
    }

    StatechartGroupPtr CloneGroupDialog::getGroup() const
    {
        return group;
    }

    GroupClonerPtr CloneGroupDialog::getGroupCloner() const
    {
        return groupCloner;
    }

    StatechartGroupMapping CloneGroupDialog::getStatechartGroupMapping() const
    {
        return mapping;
    }

    QVector<QPair<StatechartGroupPtr, QString>> CloneGroupDialog::getGroupsToClone() const
    {
        QVector<QPair<StatechartGroupPtr, QString>> result;

        if (ui->groupsWidget->rowCount() == 0 || ui->groupsWidget->columnCount() < 2)
        {
            return result;
        }

        for (const auto& g : groupsToClone)
        {
            for (int i = 0; i < ui->groupsWidget->rowCount(); ++i)
            {
                const auto oldName = ui->groupsWidget->item(i, 0)->data(Qt::UserRole).toString();

                if (oldName == g->getName())
                {
                    const auto newName = ui->groupsWidget->item(i, 1)->text();
                    ARMARX_CHECK_EXPRESSION(oldName != newName) << "oldName == newName of group " << g->getName();
                    result.push_back({g, newName});
                }
            }
        }

        ARMARX_CHECK_EXPRESSION(groupsToClone.size() == result.size());
        return result;
    }

    QString CloneGroupDialog::getGroupPrefix() const
    {
        return groupPrefix;
    }

    void CloneGroupDialog::verifyName(QTableWidgetItem* item)
    {

        const int c = item->column();

        if (c == 0 || ui->groupsWidget->columnCount() < 2)
        {
            return;
        }

        //setting the background color triggers the "itemchanged" signal, which is what called this method in the first place
        //this elegant solution ensures that no recursive calling is taking place
        if (alreadyChecking)
        {
            return;
        }

        alreadyChecking = true;

        bool hasNameCollisions = false;

        for (int i = 0; i < ui->groupsWidget->rowCount(); ++i)
        {
            const auto& itemTest = ui->groupsWidget->item(i, 1);

            if (!itemTest || (!itemTest->flags().testFlag(Qt::ItemIsEditable)))
            {
                continue;
            }

            bool collision = false;

            for (int j = 0; j < ui->groupsWidget->rowCount() && !collision; ++j)
            {
                const auto& itemL = ui->groupsWidget->item(j, 0);
                const auto& itemR = ui->groupsWidget->item(j, 1);

                collision |= itemL && itemTest->text() == itemL->text();
                collision |= itemR && (i != j) && itemTest->text() == itemR->text();
            }

            itemTest->setBackgroundColor(collision ? colorRed : colorGreen);
            hasNameCollisions |= collision;
        }

        bool validName = validGroupNameRegExp.exactMatch(item->text());
        item->setBackgroundColor(!validName ? colorRed : item->backgroundColor());

        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(validName && !hasNameCollisions);
        alreadyChecking = false;
    }

    void CloneGroupDialog::requestCheckPackagePath(QString path)
    {
        timer->start(250);
    }

    void CloneGroupDialog::requestPrefixUpdate(QString prefix)
    {
        groupPrefix = prefix;
        buildGroupList();
    }

    void CloneGroupDialog::cloneRobotSkillTemplatesToggled(bool state)
    {
        buildGroupList();
    }

    void CloneGroupDialog::buildGroupList()
    {
        if (groupPrefix.size() == 0)
        {
            ui->groupsWidget->clear();
            ui->groupsWidget->setColumnCount(0);
            ui->groupsWidget->setRowCount(0);
        }

        updateGroupDependencies();

        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(!deps.empty() && !groupsToClone.empty() && groupPrefix.size() != 0);

        if (deps.empty() || groupPrefix.size() == 0)
        {
            return;
        }

        ui->groupsWidget->clear();
        ui->groupsWidget->setRowCount(deps.size());
        ui->groupsWidget->setColumnCount(2);
        ui->groupsWidget->horizontalHeader()->setResizeMode(QHeaderView::Stretch);
        ui->groupsWidget->setHorizontalHeaderLabels({"Original StatechartGroup", "Cloned StatechartGroup"});

        for (int i = 0; i < deps.size(); ++i)
        {
            const auto& g = deps[i];

            QTableWidgetItem* item = new QTableWidgetItem(g->getName() + " [" + g->getPackageName() + "]");
            item->setData(Qt::UserRole, g->getName());
            item->setFlags(item->flags() ^ Qt::ItemIsEditable);
            QTableWidgetItem* renamed = new QTableWidgetItem(groupPrefix + g->getName());

            bool alreadyCloned = std::find_if(groupsToClone.constBegin(), groupsToClone.constEnd(), [&](const StatechartGroupPtr & dep)
            {
                return g->getGroupPath() == dep->getGroupPath();
            }) == groupsToClone.constEnd();

            if (!alreadyCloned)
            {
                item->setBackgroundColor(colorGreen);
                renamed->setBackgroundColor(colorGreen);
            }
            else
            {
                item->setBackgroundColor(colorYellow);
                renamed->setBackgroundColor(colorYellow);
                renamed->setFlags(renamed->flags() ^ Qt::ItemIsEditable);

                if (auto newGroupName = mapping.queryMappedGroupName(g->getName()))
                {
                    renamed->setText(*newGroupName);
                }
                else
                {
                    renamed->setText("<Error: Something went terribly wrong>");
                    renamed->setBackgroundColor(colorRed);
                }
            }

            ui->groupsWidget->setItem(i, 0, item);
            ui->groupsWidget->setItem(i, 1, renamed);
        }

        if (groupsToClone.empty())
        {
            ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
        }
    }

    void CloneGroupDialog::selectPackagePath()
    {
        QFileDialog selectFolder(this, "Select Target Package Root Folder");
        QList<QUrl> urls;
        urls << QUrl::fromLocalFile(QDesktopServices::storageLocation(QDesktopServices::HomeLocation))
             << QUrl::fromLocalFile(QDesktopServices::storageLocation(QDesktopServices::DesktopLocation));

        if (!ArmarXDataPath::getHomePath().empty())
        {
            urls << QUrl::fromLocalFile(QString::fromStdString(ArmarXDataPath::getHomePath()));
        }

        selectFolder.setSidebarUrls(urls);
        //        selectFolder.setOption(QFileDialog::ShowDirsOnly, true);
        selectFolder.setOption(QFileDialog::ReadOnly, true);
        selectFolder.setOption(QFileDialog::HideNameFilterDetails, false);
        selectFolder.setFileMode(QFileDialog::Directory);

        if (selectFolder.exec() == QDialog::Accepted)
        {
            ui->editPackagePath->setText(*selectFolder.selectedFiles().begin());
        }
    }

    void CloneGroupDialog::updateGroupDependencies()
    {
        deps = groupCloner->getGroupDependencies(group, true);
        if (!ui->checkBoxCloneRobotSkillTemplates->isChecked())
        {
            auto it = std::remove_if(deps.begin(), deps.end(), [&](const StatechartGroupPtr & g)
            {
                return g->getPackageName() == "RobotSkillTemplates";
            });
            deps.erase(it, deps.end());
        }

        QVector<StatechartGroupPtr> existingDeps = groupCloner->getMappedGroups(mapping);

        groupsToClone.clear();

        for (const auto& g : deps)
        {
            bool alreadyCloned = std::find_if(existingDeps.constBegin(), existingDeps.constEnd(), [&](const StatechartGroupPtr & dep)
            {
                return g->getGroupPath() == dep->getGroupPath();
            }) != existingDeps.constEnd();

            if (!alreadyCloned)
            {
                groupsToClone.push_back(g);
            }
        }
    }

    void CloneGroupDialog::checkPackagePath()
    {
        if (packageTool->checkPackagePath(ui->editPackagePath->text().toStdString()))
        {
            ui->labelPackageError->setText("Package path is valid.");
            QPalette p(ui->labelPackageError->palette());
            p.setColor(ui->labelPackageError->backgroundRole(), colorGreen);
            //            p.setBrush(ui->labelPackageError->backgroundRole(), p.light());
            ui->labelPackageError->setPalette(p);
            //            ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(true);

            mapping = StatechartGroupMapping();

            //            if (auto mappingOpt = StatechartGroupMapping::ReadStatechartGroupMappingFile(ui->editPackagePath->text()))
            //            {
            //                mapping = mappingOpt.get();
            //            }

            updateGroupDependencies();

            ui->editGroupPrefix->setEnabled(true);

            QString prefixSuggestion;

            if (!mapping.groupMappings.empty())
            {
                const auto gm = *mapping.groupMappings.begin();
                prefixSuggestion = QString(gm.newGroupName).replace(gm.groupName, "");
            }

            ui->editGroupPrefix->setText(prefixSuggestion);
        }
        else
        {
            ui->labelPackageError->setText("Package path is not valid!");
            QPalette p(ui->labelPackageError->palette());
            p.setColor(ui->labelPackageError->backgroundRole(), colorRed);
            ui->labelPackageError->setPalette(p);
            ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
        }
    }

} // namespace armarx
