/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once
#include <ArmarXGui/gui-plugins/StatechartViewerPlugin/model/State.h>
#include <ArmarXCore/statechart/xmlstates/profiles/StatechartProfiles.h>
#include <QComboBox>
#include <QStyledItemDelegate>
#include <QTableWidget>
#include <QToolButton>
#include <ArmarXCore/observers/variant/VariantInfo.h>


namespace armarx
{
    class AnimatedToolButton : public QToolButton
    {

        Q_OBJECT
        Q_PROPERTY(QColor color READ color WRITE setColor)

    public:
        AnimatedToolButton(QWidget* parent = 0) : QToolButton(parent)
        {
        }
        void setColor(QColor color);
        QColor color()
        {
            return Qt::black; // getter is not really needed for now
        }
    };

    class StatechartEditorParameterEditor :
        public QTableWidget
    {
        Q_OBJECT
    public:

        void setStateInstance(statechartmodel::StateInstancePtr state);
        StatechartEditorParameterEditor(QWidget* parent = 0, statechartmodel::StateInstancePtr state = statechartmodel::StateInstancePtr(), const statechartmodel::StateParameterMap& params = statechartmodel::StateParameterMap());
        //        void buildFromMap(const statechartmodel::StateParameterMap& map);
    protected slots:
        void addCustomColumn(int row);
        void fillCustomColumn(int row, const QString& key);
        void showHelpDialog();
        void addInputToParent();
        void init(const statechartmodel::StateParameterMap& params);
    protected:
        statechartmodel::StateInstancePtr state;
        statechartmodel::StateParameterMap  originalParamMap;


        // ###### StateParameterEditor: ######
    public:
        enum ColumnIds
        {
            eKey,
            eType,
            eOptional,
            eDeleteButton,
            eAdditionalButtons,
            eEditableValue
        };
        //explicit StateParameterEditor(QWidget *parent = 0, const StateParameterMap& params = StateParameterMap());

        void setCommunicator(Ice::CommunicatorPtr communicator)
        {
            this->communicator = communicator;
        }
        void setVariantInfo(VariantInfoPtr variantInfo)
        {
            this->variantInfo = variantInfo;
        }
        void setCurrentProfile(StatechartProfilePtr currentProfile)
        {
            this->currentProfile = currentProfile;
            setColumnCount(eEditableValue + getRelevantProfiles().size());
        }
        void setKeyBlackList(const QSet<QString>& keyBlackList);
        QSet<QString> getKeys() const;
        QSet<QString> getTypes() const;
        statechartmodel::StateParameterMap getStateParameters() const;
        statechartmodel::StateParameterMap getStateParametersWithoutValue() const;

        QMap<QString, QString> getValuesAsString(int row) const;
        QMap<QString, VariantContainerBasePtr> getProfileVariantContainerMap(int row) const;
        QMap<QString, QString> getProfileJsonMap(QString key) const;
        QString getKey(int row) const;
        QString getType(int row) const;
        QString getType(QString key) const;
        int getRow(const QString& key) const;
        bool getIsOptional(int row) const;

        void addParameterRow(QString key, QString variantIdStr, QVector<QString> values, bool optional = false);
        void setHeaders();
        Qt::CheckState getDefaultValueState() const;
        void setDefaultValueState(const Qt::CheckState& value);
        void hideValueColumns();

        statechartmodel::StateParameterPtr getStateParameter(int row) const;
    signals:
        void buildRequested(const statechartmodel::StateParameterMap& map);
        void rowAdded(int rowId);
        void rowFilled(int rowId, const QString& key);
        void typeChanged(int row, const QString& newType);
    public slots:
        void buildFromMap(const statechartmodel::StateParameterMap& map);
        int addParameterRow();
        void typeCbChanged(const QString&);
        void deleteRow();
        void checkAndUpdateRowCount(int row, int column);
        void refreshVariantTypes();
    private slots:
        void __buildFromMap(const statechartmodel::StateParameterMap& map);

    private:
        QVector<QString> getRelevantProfiles() const;
        void connectUserEditSlots();
        QStringList addVariantTypesToComboBox(QComboBox* combo);
        QString getHumanNameFromBaseName(QString variantBaseTypeName) const;
        QString getBaseNameFromHumanName(QString humanName) const;
        static bool compareVariantNames(const QString& a, const QString& b);

        class LineEditDelegate : public QStyledItemDelegate
        {
            QWidget* createEditor(QWidget* parent,
                                  const QStyleOptionViewItem& option,
                                  const QModelIndex& index) const override;
        };
        LineEditDelegate delegate;
        Ice::CommunicatorPtr communicator;
        VariantInfoPtr variantInfo;
        Qt::CheckState defaultValueState;
        QSet<QString> keyBlackList;
        StatechartProfilePtr currentProfile;
    };

} // namespace armarx

