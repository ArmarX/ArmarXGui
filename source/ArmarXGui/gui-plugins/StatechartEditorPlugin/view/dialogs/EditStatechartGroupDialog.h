/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <QDialog>
#include <QList>
#include <QMap>
#include <QModelIndex>

#include <ArmarXCore/core/system/cmake/ArmarXPackageToolInterface.h>
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/model/StatechartGroupDefs.h>
#include <ArmarXCore/observers/variant/VariantInfo.h>
#include <ArmarXCore/statechart/xmlstates/profiles/StatechartProfiles.h>

class QListWidgetItem;

namespace armarx::Ui
{
    class EditStatechartGroupDialog;
}

namespace armarx
{
    class EditStatechartGroupDialog : public QDialog
    {
        Q_OBJECT

    public:
        enum EditMode { NewGroup, EditGroup };
        explicit EditStatechartGroupDialog(EditMode editMode, QString groupName, ArmarXPackageToolInterfacePtr packageTool, VariantInfoPtr variantInfo,
                                           QList<QString> selectedProxies, bool generateContext,
                                           const StatechartProfilesPtr& statechartProfiles, const QMap<QString, QString>& statechartGroupConfigurations = QMap<QString, QString>(),
                                           const QString& description = "", StatechartGroupPtr group = StatechartGroupPtr(), QWidget* parent = 0);
        ~EditStatechartGroupDialog() override;
        ArmarXPackageToolInterfacePtr getPackageTool() const;
        QString getGroupName() const;
        QString getGroupPath() const;
        QString getGroupDescription() const;
        QString getPackageName() const;
        QString getPackagePath() const;
        QList<QString> getProxies() const;
        bool contextGenerationEnabled() const;
        QMap<QString, QString> getConfigurations() const;

    public slots:
        void requestCheckPackagePath(QString path);
        void selectPackagePath();
        void checkPackagePath();
        void updateProxyListEnabled(int state);
        void updateConfigurationTextField(QString profileText);
        void storeConfigurationText();
        void updateConfigurationContent(int index);
        void updateDependencies(QModelIndex index1 = QModelIndex(), QModelIndex index2 = QModelIndex());

    private slots:
        void on_pushButton_clicked();

        void on_btnShowPackageError_clicked();

    private:
        Ui::EditStatechartGroupDialog* ui;
        ArmarXPackageToolInterfacePtr packageTool;
        QTimer* timer;
        EditMode editMode;
        StatechartProfilesPtr statechartProfiles;
        QMap<QString, QString> configurations;
        VariantInfoPtr variantInfo;
        StatechartGroupPtr group;
    };


}
