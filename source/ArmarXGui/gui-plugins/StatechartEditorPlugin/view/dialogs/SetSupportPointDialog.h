/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Valerij Wittenbeck (valerij.wittenbeck at student dot kit dot edu
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <QDialog>
#include <QList>


#include <QTableWidgetItem>


namespace armarx::Ui
{
    class SetSupportPointDialog;
}

namespace armarx
{
    class SetSupportPointDialog : public QDialog
    {
        Q_OBJECT

    public:
        explicit SetSupportPointDialog(QWidget* parent = 0);
        ~SetSupportPointDialog() override;

        int getU();
        int getV();

    private:
        Ui::SetSupportPointDialog* ui;
    };


}
