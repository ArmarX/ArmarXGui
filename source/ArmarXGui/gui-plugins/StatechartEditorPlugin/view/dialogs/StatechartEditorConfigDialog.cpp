/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Nikolaus Vahrenkamp ( vahrenkamp at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "StatechartEditorConfigDialog.h"
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/view/dialogs/ui_StatechartEditorConfigDialog.h>

#include <QPushButton>

armarx::StatechartEditorConfigDialog::StatechartEditorConfigDialog(StatechartProfilesPtr profiles, QWidget* parent) :
    QDialog(parent),
    ui(new Ui_StatechartEditorConfigDialog())
{
    ui->setupUi(this);
    QPushButton* okButton = ui->buttonBox->button(QDialogButtonBox::Ok);
    okButton->setEnabled(false);

    for (StatechartProfilePtr p : profiles->getAllLeaves())
    {
        QListWidgetItem* item = new QListWidgetItem(QString::fromUtf8(p->getName().data()));
        profileMap.insert(std::make_pair(item, p));

        ui->listWidgetProfiles->addItem(item);
    }
    //    connect(ui->listWidgetProfiles, SIGNAL(doubleClicked(QModelIndex)), this,
    connect(ui->listWidgetProfiles, SIGNAL(itemSelectionChanged()), SLOT(itemSelectionChanged()));
}

armarx::StatechartEditorConfigDialog::~StatechartEditorConfigDialog()
{
    delete ui;
}

armarx::StatechartProfilePtr armarx::StatechartEditorConfigDialog::getSelectedProfile()
{
    QList<QListWidgetItem*> selected = ui->listWidgetProfiles->selectedItems();

    if (selected.size() == 0)
    {
        return armarx::StatechartProfilePtr();
    }
    else
    {
        return profileMap.at(selected.at(0));
    }
}

bool armarx::StatechartEditorConfigDialog::openAllStatesIsSelected()
{
    return ui->checkBoxOpenAllStates->isChecked();
}

void armarx::StatechartEditorConfigDialog::itemSelectionChanged()
{
    QPushButton* okButton = ui->buttonBox->button(QDialogButtonBox::Ok);
    okButton->setEnabled(ui->listWidgetProfiles->selectedItems().size() > 0);
}


void armarx::StatechartEditorConfigDialog::on_listWidgetProfiles_doubleClicked(const QModelIndex& index)
{
    accept();
}
