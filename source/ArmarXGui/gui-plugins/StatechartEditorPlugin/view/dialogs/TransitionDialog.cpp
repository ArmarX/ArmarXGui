/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "TransitionDialog.h"
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/view/dialogs/ui_TransitionDialog.h>

#include "../../../ObserverPropertiesPlugin/ObserverItemModel.h"

#include <QComboBox>
#include <QFileDialog>
#include <QLineEdit>
#include <QMessageBox>


namespace armarx
{

    TransitionDialog::TransitionDialog(statechartmodel::TransitionCPtr transition, statechartmodel::StatePtr state, QStringList profileNames, Ice::CommunicatorPtr ic, VariantInfoPtr variantInfo, QWidget* parent) :
        QDialog(parent),
        ui(new Ui::TransitionDialog),
        transition(transition),
        parentState(state),
        ic(ic),
        variantInfo(variantInfo),
        profileNames(profileNames)
    {
        ui->setupUi(this);
        setup();
    }

    TransitionDialog::~TransitionDialog()
    {
        delete ui;
    }

    statechartmodel::ParameterMappingList TransitionDialog::getMappingToNextStateInput() const
    {
        return ui->inputTableMapping->getMapping();
    }

    statechartmodel::ParameterMappingList TransitionDialog::getMappingToParentStateLocal() const
    {
        return ui->parentLocalMapping->getMapping();
    }

    statechartmodel::ParameterMappingList TransitionDialog::getMappingToParentStateOutput() const
    {
        return ui->parentOutputMapping->getMapping();
    }

    size_t TransitionDialog::LevenshteinEditDistance(const std::string& s1, const std::string& s2)
    {
        // from https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance#C++
        const std::size_t len1 = s1.size(), len2 = s2.size();
        std::vector<std::vector<unsigned int>> d(len1 + 1, std::vector<unsigned int>(len2 + 1));

        d[0][0] = 0;
        for (unsigned int i = 1; i <= len1; ++i)
        {
            d[i][0] = i;
        }
        for (unsigned int i = 1; i <= len2; ++i)
        {
            d[0][i] = i;
        }

        for (unsigned int i = 1; i <= len1; ++i)
            for (unsigned int j = 1; j <= len2; ++j)
                // note that std::min({arg1, arg2, arg3}) works only in C++11,
                // for C++98 use std::min(std::min(arg1, arg2), arg3)
                d[i][j] = std::min({ d[i - 1][j] + 1, d[i][j - 1] + 1, d[i - 1][j - 1] + (s1[i - 1] == s2[j - 1] ? 0 : 1) });
        return d[len1][len2];
    }


    void TransitionDialog::setup()
    {
        //    ui->editEventName->setText(transition->eventName);
        //    ui->editEventName->setFocus();
        ui->labelEventName->setText(transition->eventName);

        if (!transition->destinationState)
        {
            ui->labelDestinationState->setText("Transition is detached");
            return;
        }


        if (transition->sourceState)
        {
            ui->labelSourceState->setText(transition->sourceState->getInstanceName());
            setWindowTitle("Edit Transition " + transition->eventName + " [" + transition->sourceState->getInstanceName() + " -> " + transition->destinationState->getInstanceName() + "]");
        }
        else
        {
            ui->labelSourceState->setText("This is the initial transition to the start state");
            setWindowTitle("Edit Initial Transition  -> " + transition->destinationState->getInstanceName());
        }

        ui->labelDestinationState->setText(transition->destinationState->getInstanceName());


        statechartmodel::StateParameterMap destDict;

        if (transition->destinationState->getType() != eFinalState)
        {
            if (transition->destinationState->getStateClass())
            {
                destDict = transition->destinationState->getStateClass()->getInputParameters();
            }
            else
            {
                QMessageBox::warning(this, "Transition Editing Error", "Transition editing not possible because destination state is not loaded - load statechart group of destination state.");
                return;
            }
        }
        else
        {
            destDict = transition->destinationState->getParent()->getOutputParameters();
        }

        ui->inputTableMapping->setup(destDict, transition->mappingToNextStatesInput, transition, parentState, profileNames, ic, variantInfo);

        if (transition->sourceState)
        {
            ui->tabWidget->setTabEnabled(1, true);
            ui->tabWidget->setTabEnabled(2, true);
            ui->parentLocalMapping->hideColumn(TransitionMappingTable::eMappingRequired);
            ui->parentOutputMapping->hideColumn(TransitionMappingTable::eMappingRequired);
            ui->parentLocalMapping->setup(parentState->getLocalParameters(),  transition->mappingToParentStatesLocal, transition, parentState, profileNames,  ic, variantInfo);
            ui->parentOutputMapping->setup(parentState->getOutputParameters(),  transition->mappingToParentStatesOutput, transition, parentState, profileNames, ic, variantInfo);
        }
        else
        {
            ui->tabWidget->setTabEnabled(1, false);
            ui->tabWidget->setTabEnabled(2, false);
        }

    }





}

void armarx::TransitionDialog::on_btnAutoMap_clicked()
{
    TransitionMappingTable* mapping;
    if (ui->tabWidget->currentIndex() == 0)
    {
        mapping = ui->inputTableMapping;
    }
    else if (ui->tabWidget->currentIndex() == 1)
    {
        mapping = ui->parentLocalMapping;
    }
    else
    {
        mapping = ui->parentOutputMapping;
    }


    auto caseSensitive = [](QString s1, QString s2)
    {
        return std::make_pair(s1 == s2, 0.0f);
    };
    auto caseInsensitive = [](QString s1, QString s2)
    {
        return std::make_pair(s1.compare(s1, s2, Qt::CaseInsensitive) == 0, 0.0f);
    };
    auto editDistance = [](QString s1, QString s2)
    {
        auto distance = LevenshteinEditDistance(s1.toLower().toStdString(), s2.toLower().toStdString());
        return std::make_pair(distance <= 3, (float)distance);
    };

    ARMARX_INFO << "Casesensitive";
    auto mappedRows = mapping->mapByCriteria(caseSensitive);
    //    ARMARX_INFO << mappedRows.size() << " Caseinsensitive";
    mappedRows.append(mapping->mapByCriteria(caseInsensitive));
    //    ARMARX_INFO << mappedRows.size() << " editdistance";
    mappedRows.append(mapping->mapByCriteria(editDistance));
    //    ARMARX_INFO << mappedRows.size() << " after editdistance";
    QString foundMappingsString;
    for (auto& elem : mappedRows)
    {
        foundMappingsString += elem.first + " from " + elem.second + "\n";
    }
    if (!foundMappingsString.isEmpty())
    {
        foundMappingsString = ":\n" + foundMappingsString;
    }
    QMessageBox::information(this, "Auto Mapping Results", "Mapped " + QString::number(mappedRows.size()) + (mappedRows.size() == 1 ? " entry" : " entries") + foundMappingsString);
}
