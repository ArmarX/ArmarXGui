/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Valerij Wittenbeck (valerij.wittenbeck at student dot kit dot edu
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "SetSupportPointDialog.h"
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/view/dialogs/ui_SetSupportPointDialog.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <QFileDialog>

#include <QtGui>

namespace armarx
{
    SetSupportPointDialog::SetSupportPointDialog(QWidget* parent) :
        QDialog(parent),
        ui(new Ui::SetSupportPointDialog)
    {
        ui->setupUi(this);
        ui->spinBoxV->setFocus();
        ui->spinBoxV->selectAll();
    }

    SetSupportPointDialog::~SetSupportPointDialog()
    {

    }

    int SetSupportPointDialog::getU()
    {
        return ui->spinBoxU->value();
    }

    int SetSupportPointDialog::getV()
    {
        return ui->spinBoxV->value();
    }
}
