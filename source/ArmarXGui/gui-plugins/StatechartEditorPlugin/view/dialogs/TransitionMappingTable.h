/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once


#include <QTableWidget>

#include <ArmarXCore/observers/variant/VariantInfo.h>

#include "../../../StatechartViewerPlugin/model/State.h"


class QComboBox;

namespace armarx
{

    class TransitionMappingTable : public QTableWidget
    {
        Q_OBJECT
    public:
        enum
        {
            eDestKey,
            eDestType,
            eMappingRequired,
            eSource,
            eSourceKey
        };

        explicit TransitionMappingTable(QWidget* parent = 0);
        void setup(const statechartmodel::StateParameterMap& targetDict, const statechartmodel::ParameterMappingList& sourceMapping, statechartmodel::TransitionCPtr transition, statechartmodel::StatePtr state, QStringList profileNames, Ice::CommunicatorPtr ic = NULL, VariantInfoPtr variantInfo = VariantInfoPtr());
        statechartmodel::ParameterMappingList getMapping() const;
        QList<std::pair<QString, QString> > mapByCriteria(std::function<std::pair<bool, float>(QString, QString)> compare);
    signals:

    public slots:
    protected slots:
        void setSourceSpecificMappingParameters(int index, int tablerow = -1, QString initialValue = "");

    private:
        QComboBox* getFilteredOutputItems(QString key, statechartmodel::TransitionCPtr transition);
        QList<QPair<QString, QString>> getFilteredParams(const statechartmodel::StateParameterMap& source, const statechartmodel::StateParameter& argumentToMatch) const;
        QString getInputTypeString(int row);
        statechartmodel::TransitionCPtr transition;
        statechartmodel::StatePtr parentState;
        statechartmodel::StateParameterMap destDict;
        statechartmodel::ParameterMappingList oldSourceMapping;
        QStringList profileNames;
        Ice::CommunicatorPtr ic;

        VariantInfoPtr variantInfo;
    };

}

