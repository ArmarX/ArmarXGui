/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "EditStatechartGroupDialog.h"
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/view/dialogs/ui_EditStatechartGroupDialog.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <QFileDialog>
#include <QTimer>
#include <QtGui>
#include <QToolTip>

#include <filesystem>

#include <ArmarXCore/core/system/ArmarXDataPath.h>

#include <ArmarXCore/observers/variant/VariantContainer.h>

#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/model/StatechartGroup.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/InfixFilterModel.h>



namespace armarx
{

    EditStatechartGroupDialog::EditStatechartGroupDialog(EditMode editMode, QString groupName, ArmarXPackageToolInterfacePtr packageTool,
            VariantInfoPtr variantInfo, QList<QString> selectedProxies, bool generateContext,
            const StatechartProfilesPtr& statechartProfiles, const QMap<QString, QString>& statechartGroupConfigurations,
            const QString& description, StatechartGroupPtr group, QWidget* parent) :
        QDialog(parent),
        ui(new Ui::EditStatechartGroupDialog),
        packageTool(packageTool),
        editMode(editMode),
        statechartProfiles(statechartProfiles),
        configurations(statechartGroupConfigurations),
        variantInfo(variantInfo),
        group(group)
    {
        ui->setupUi(this);
        ui->btnShowPackageError->setVisible(false);

        ui->checkBoxGenerateContext->setChecked(generateContext);
        updateProxyListEnabled(ui->checkBoxGenerateContext->checkState());
        connect(ui->checkBoxGenerateContext, SIGNAL(stateChanged(int)), this, SLOT(updateProxyListEnabled(int)));

        if (editMode == NewGroup)
        {
            setWindowTitle("Create new Statechart Group");
            QRegExp rx("([a-zA-Z][a-zA-Z0-9]*)");
            ui->editStatechartGroup->setValidator(new QRegExpValidator(rx, this));

            connect(ui->btnSelectPackageFolder, SIGNAL(clicked()), this, SLOT(selectPackagePath()));
            connect(ui->editPackagePath, SIGNAL(textChanged(QString)), this, SLOT(requestCheckPackagePath(QString)));
            connect(ui->editStatechartGroup, SIGNAL(textChanged(QString)), this, SLOT(requestCheckPackagePath(QString)));

            timer = new QTimer(this);
            connect(timer, SIGNAL(timeout()), this, SLOT(checkPackagePath()));
            timer->setSingleShot(true);

            // disable ok button until everything is valid
            ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
            ui->tabWidget->setCurrentIndex(0);
        }
        else if (editMode == EditGroup)
        {
            setWindowTitle(groupName + " - Properties");
            ui->editStatechartGroup->setText(groupName);
            ui->textEditGroupDescription->setPlainText(description);
            ui->editPackagePath->setEnabled(false);
            ui->editStatechartGroup->setEnabled(false);
            ui->btnSelectPackageFolder->setEnabled(false);

            ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(true);
        }
        else
        {
            throw LocalException("Not supported enum value.");
        }

        QStringList properties;
        QStandardItemModel* model = new QStandardItemModel(ui->listProxies);
        // Populate proxy/topic list
        for (VariantInfo::LibEntryPtr lib : variantInfo->getLibs())
        {
            QString libName = QString::fromUtf8(lib->getName().c_str());

            for (VariantInfo::ProxyEntryPtr proxy : lib->getProxies())
            {
                //QString proxyType = QString::fromUtf8(proxy->getProxyTypeAsString().c_str());
                QString proxyHumanName = QString::fromUtf8(proxy->getHumanName().c_str());
                QString proxyMemberName = QString::fromUtf8(proxy->getMemberName().c_str());
                QString display = QString("[%1] %3").arg(libName, proxyHumanName);
                QStandardItem* listItem = new QStandardItem(display);
                QString proxyId = QString("%1.%2").arg(libName, proxyMemberName);
                listItem->setData(proxyId, Qt::UserRole);
                listItem->setCheckable(true);
                listItem->setEditable(false);
                listItem->setCheckState(selectedProxies.contains(proxyId) ? Qt::Checked : Qt::Unchecked);
                model->appendRow(listItem);
            }
        }
        InfixFilterModel* filterModel = new InfixFilterModel(ui->listProxies);
        filterModel->setFilterCaseSensitivity(Qt::CaseInsensitive);
        filterModel->setSourceModel(model);
        ui->listProxies->setModel(filterModel);
        connect(ui->lineEditProxyFilter, SIGNAL(textChanged(QString)), filterModel, SLOT(setFilterFixedString(QString)));


        std::list<StatechartProfilePtr> profileQueue;
        profileQueue.push_back(statechartProfiles->getRootProfile());
        connect(ui->comboBoxStatechartProfiles, SIGNAL(currentIndexChanged(QString)), this, SLOT(updateConfigurationTextField(QString)));
        connect(ui->tabWidget, SIGNAL(currentChanged(int)), this, SLOT(updateConfigurationContent(int)));
        while (!profileQueue.empty())
        {
            StatechartProfilePtr currentProfile = profileQueue.front();
            profileQueue.pop_front();
            ui->comboBoxStatechartProfiles->addItem(QString(currentProfile->getNesting(), '-') + " " + QString::fromStdString(currentProfile->getName()),
                                                    QString::fromStdString(currentProfile->getName()));
            for (auto it = currentProfile->getChildren().rbegin(); it != currentProfile->getChildren().rend(); it++)
            {
                profileQueue.push_front(*it);
            }
            if (profileQueue.size() == 0)
            {
                break;
            }
            if (configurations[QString::fromStdString(currentProfile->getName())].isEmpty())
            {
                configurations[QString::fromStdString(currentProfile->getName())] = properties.join("\n");
            }
        }
        connect(ui->textEditParameters, SIGNAL(textChanged()), this, SLOT(storeConfigurationText()));
        connect(ui->listProxies->model(), SIGNAL(dataChanged(QModelIndex, QModelIndex)), this, SLOT(updateDependencies(QModelIndex, QModelIndex)));

        updateDependencies();

    }

    EditStatechartGroupDialog::~EditStatechartGroupDialog()
    {
        delete ui;
    }

    ArmarXPackageToolInterfacePtr EditStatechartGroupDialog::getPackageTool() const
    {
        return packageTool;
    }

    QString EditStatechartGroupDialog::getGroupName() const
    {
        return ui->editStatechartGroup->text();
    }

    QString EditStatechartGroupDialog::getGroupPath() const
    {
        // new behavior
        {
            CMakePackageFinder finder(getPackageName().toStdString());

            std::filesystem::path path = finder.getStatechartsDir();

            if(not path.empty())
            {
                path /= getGroupName().toUtf8().data();
                return QString::fromStdString(path.string());
            }
        }

        // legacy behavior

        ARMARX_WARNING << "The package `" << getPackageName() << "` does not provide the STATECHARTS_DIR cmake variable!"
                       << "Using legacy path for now.";

        std::filesystem::path path = ui->editPackagePath->text().toUtf8().data();
        path /= "source";
        path /= getPackageName().toUtf8().data();
        path /= "statecharts";
        path /= getGroupName().toUtf8().data();

        return QString::fromUtf8(path.c_str());
    }

    QString EditStatechartGroupDialog::getGroupDescription() const
    {
        return ui->textEditGroupDescription->toPlainText();
    }

    QString EditStatechartGroupDialog::getPackageName() const
    {
        const std::filesystem::path packagePath = getPackagePath().toStdString();

        std::vector<std::string> packages = CMakePackageFinder::FindAllArmarXSourcePackages();
        ARMARX_IMPORTANT << VAROUT(packages);
        for (const std::string& package : packages)
        {
            CMakePackageFinder finder(package);
            if (finder.getPackageDir() == packagePath)
            {
                return QString::fromStdString(package);
            }
        }
        std::stringstream ss;
        ss << "No CMake package found for path " << packagePath << ".";
        throw LocalException(ss.str());

        // std::filesystem::path path = getPackagePath().toUtf8().data();
        // return QString::fromUtf8(path.filename().c_str());
    }

    QString EditStatechartGroupDialog::getPackagePath() const
    {
        std::filesystem::path path = ui->editPackagePath->text().toUtf8().data();
        std::filesystem::path cleanPath = path;

        try
        {
            cleanPath = std::filesystem::canonical(path);
        }
        catch (...)
        {
            cleanPath = ArmarXDataPath::cleanPath(path.string());

            if (*cleanPath.string().rbegin() == '/' || *cleanPath.string().rbegin() == '\\')
            {
                cleanPath = cleanPath.remove_filename();
            }
        }

        return QString::fromUtf8(cleanPath.c_str());
    }

    QList<QString> EditStatechartGroupDialog::getProxies() const
    {
        QList<QString> proxies;
        QSortFilterProxyModel* proxy = qobject_cast<QSortFilterProxyModel*>(ui->listProxies->model());
        QStandardItemModel* model = qobject_cast<QStandardItemModel*>(proxy->sourceModel());
        for (int row = 0; row < model->rowCount(); row++)
        {
            auto item = model->item(row);
            if (item && item->checkState() == Qt::Checked)
            {
                proxies.append(item->data(Qt::UserRole).toString());
            }
        }

        return proxies;
    }

    bool EditStatechartGroupDialog::contextGenerationEnabled() const
    {
        return ui->checkBoxGenerateContext->isChecked();
    }

    QMap<QString, QString> EditStatechartGroupDialog::getConfigurations() const
    {
        return configurations;
    }

    void EditStatechartGroupDialog::requestCheckPackagePath(QString path)
    {
        timer->start(250);

    }

    void EditStatechartGroupDialog::selectPackagePath()
    {
        QFileDialog selectFolder(this, "Select ArmarX Package Root Folder");
        QList<QUrl> urls;
        urls << QUrl::fromLocalFile(QDesktopServices::storageLocation(QDesktopServices::HomeLocation))
             << QUrl::fromLocalFile(QDesktopServices::storageLocation(QDesktopServices::DesktopLocation));

        if (!ArmarXDataPath::getHomePath().empty())
        {
            urls << QUrl::fromLocalFile(QString::fromStdString(ArmarXDataPath::getHomePath()));
        }

        selectFolder.setSidebarUrls(urls);
        //        selectFolder.setOption(QFileDialog::ShowDirsOnly, true);
        selectFolder.setOption(QFileDialog::ReadOnly, true);
        selectFolder.setOption(QFileDialog::HideNameFilterDetails, false);
        selectFolder.setFileMode(QFileDialog::Directory);

        if (selectFolder.exec() == QDialog::Accepted)
        {
            ui->editPackagePath->setText(*selectFolder.selectedFiles().begin());

        }
    }


    void EditStatechartGroupDialog::checkPackagePath()
    {
        std::string cmdOutput;
        if (packageTool->checkPackagePath(ui->editPackagePath->text().toStdString(), cmdOutput))
        {
            ui->labelPackageError->setText("Package path is valid.");
            QPalette p(ui->labelPackageError->palette());
            p.setColor(ui->labelPackageError->backgroundRole(), QColor::fromRgb(120, 255, 120));
            //            p.setBrush(ui->labelPackageError->backgroundRole(), p.light());
            ui->labelPackageError->setPalette(p);

            if (!ui->editStatechartGroup->text().isEmpty())
            {
                ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(true);
            }
            else
            {
                ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
            }
            ui->labelPackageError->setToolTip("");
            ui->btnShowPackageError->setVisible(false);
        }
        else
        {
            ui->labelPackageError->setText("Package path is not valid!");
            ui->labelPackageError->setToolTip(QString::fromStdString(cmdOutput));
            QPalette p(ui->labelPackageError->palette());
            p.setColor(ui->labelPackageError->backgroundRole(), QColor::fromRgb(255, 120, 120));
            ui->labelPackageError->setPalette(p);
            ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
            ui->btnShowPackageError->setVisible(true);

        }
    }

    void EditStatechartGroupDialog::updateProxyListEnabled(int state)
    {
        ui->listProxies->setEnabled(ui->checkBoxGenerateContext->isChecked());
    }

    void EditStatechartGroupDialog::updateConfigurationTextField(QString profileText)
    {
        int i = ui->comboBoxStatechartProfiles->findText(profileText);
        if (i < 0)
        {
            return;
        }
        auto profile = ui->comboBoxStatechartProfiles->itemData(i).toString();
        ui->textEditParameters->setPlainText(configurations[profile]);
    }

    void EditStatechartGroupDialog::storeConfigurationText()
    {
        int i = ui->comboBoxStatechartProfiles->currentIndex();
        auto profile = ui->comboBoxStatechartProfiles->itemData(i).toString();
        configurations[profile] = ui->textEditParameters->toPlainText();

    }

    void EditStatechartGroupDialog::updateConfigurationContent(int index)
    {


    }

    void EditStatechartGroupDialog::updateDependencies(QModelIndex index1, QModelIndex index2)
    {
        auto selectedProxies  = getProxies();
        QStringList libs;
        for (VariantInfo::LibEntryPtr lib : variantInfo->getLibs())
        {
            QString libName = QString::fromUtf8(lib->getName().c_str());
            for (VariantInfo::ProxyEntryPtr proxy : lib->getProxies())
            {
                QString proxyMemberName = QString::fromUtf8(proxy->getMemberName().c_str());
                QString proxyId = QString("%1.%2").arg(libName, proxyMemberName);
                if (selectedProxies.contains(proxyId))
                {
                    if (!libs.contains(libName))
                    {
                        libs << (libName);
                    }

                }
            }
        }
        if (group)
        {
            QSet<QString> vars;
            Ice::StringSeq types;
            for (statechartmodel::StatePtr state : group->getAllStates(true))
            {
                for (auto& param : state->getInputParameters())
                {
                    vars.insert(param->type);
                }
                for (auto& param : state->getLocalParameters())
                {
                    vars.insert(param->type);
                }
                for (auto& param : state->getOutputParameters())
                {
                    vars.insert(param->type);
                }
            }
            for (auto& var : vars)
            {
                auto type = VariantContainerType::GetInnerType(var.toStdString());
                if (!type.empty())
                {
                    types.push_back(type);
                }
            }
            for (std::string& lib : variantInfo->findLibNames(types))
            {
                auto libName = QString::fromStdString(lib);
                if (!libs.contains(libName))
                {
                    libs << libName;
                }
            }

        }
        ui->editDependencies->setText(libs.join(" "));

    }
}

void armarx::EditStatechartGroupDialog::on_pushButton_clicked()
{
    auto selectedProxies  = getProxies();
    auto groupName = getGroupName();
    int i = ui->comboBoxStatechartProfiles->currentIndex();
    if (i < 0)
    {
        return;
    }
    auto profileName = ui->comboBoxStatechartProfiles->itemData(i).toString();
    for (VariantInfo::LibEntryPtr lib : variantInfo->getLibs())
    {
        QString libName = QString::fromUtf8(lib->getName().c_str());

        for (VariantInfo::ProxyEntryPtr proxy : lib->getProxies())
        {
            QString proxyMemberName = QString::fromUtf8(proxy->getMemberName().c_str());
            QString proxyId = QString("%1.%2").arg(libName, proxyMemberName);
            if (selectedProxies.contains(proxyId))
            {
                QString propName = QString("ArmarX.") +  groupName + "RemoteStateOfferer." + QString::fromUtf8(proxy->getPropertyName().c_str());

                if (!configurations[profileName].contains(propName))
                {
                    QString newProp;
                    if (!proxy->getPropertyIsOptional())
                    {
                        newProp += "# Required Property\n";
                        newProp += "#" + propName + " = <set value and uncomment!>\n";
                    }
                    else
                    {
                        newProp += "#" + propName + " = " + QString::fromUtf8(proxy->getPropertyDefaultValue().c_str()) + "\n";
                    }

                    configurations[profileName] += "\n" + newProp;
                }
            }
        }
    }
    updateConfigurationTextField(ui->comboBoxStatechartProfiles->currentText());
}

void armarx::EditStatechartGroupDialog::on_btnShowPackageError_clicked()
{
    QToolTip::showText(ui->btnShowPackageError->mapToGlobal(QPoint(10, 10)), ui->labelPackageError->toolTip());
}
