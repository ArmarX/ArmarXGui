/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <QDialog>

namespace armarx::Ui
{
    class StatechartEditorSettingsDialog;
}

namespace armarx
{
    class StatechartEditorSettingsDialog : public QDialog
    {
        Q_OBJECT

    public:
        explicit StatechartEditorSettingsDialog(QWidget* parent = 0);
        ~StatechartEditorSettingsDialog() override;
        void setPaths(QStringList paths);
        QStringList getPaths() const;
        void setRemoteStatesLocked(bool locked);
        bool getRemoteStatesLocked() const;
    public slots:
        void selectNewPath();
    private:
        Ui::StatechartEditorSettingsDialog* ui;
    };


} // namespace armarx
