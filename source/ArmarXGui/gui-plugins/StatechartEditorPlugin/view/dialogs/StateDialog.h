/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once


#include "../../../StatechartViewerPlugin/model/State.h"

#include <ArmarXCore/core/system/DynamicLibrary.h>

#include <QDialog>
#include <QErrorMessage>

#include <ArmarXCore/observers/variant/VariantInfo.h>

#include <ArmarXCore/statechart/xmlstates/profiles/StatechartProfiles.h>

class QFileDialog;

namespace armarx
{
    class StatechartEditorParameterEditor;
    class StateParameterEditor;
}

namespace armarx::Ui
{
    class StateDialog;
}

namespace armarx
{
    class StateDialog : public QDialog
    {
        Q_OBJECT

    public:
        explicit StateDialog(statechartmodel::StateInstancePtr stateInstance, Ice::CommunicatorPtr communicator, VariantInfoPtr variantInfo, StatechartProfilePtr currentProfile, bool locked, bool readOnly, QPointer<TipDialog> tipDialog, QWidget* parent = 0);
        ~StateDialog() override;

        statechartmodel::StateParameterMap getInputParams() const;
        statechartmodel::StateParameterMap getLocalParams() const;
        statechartmodel::StateParameterMap getOutputParams() const;
        QString getStateInstanceName() const;
        QString getStateName() const;
        QString getProxyName() const;
        QString getDescription() const;
        statechartmodel::EventList getOutgoingEvents() const;

    private slots:
        void eventNameComboboxChanged(const QString& eventName);
        void eventButtonAdd();
        void eventButtonDelete();
        void eventDescriptionChanged();
        void loadLibrary();
        void setBlackLists();
        void setLockStatus(bool unlock);
        void updateLinkHint(int, QString);


    private:
        statechartmodel::StateParameterMap getUpdatedParameters(const statechartmodel::StateParameterMap& source, const armarx::StatechartEditorParameterEditor& parameterEditor) const;
        //    statechartmodel::StateParameterMap getParameters(const armarx::StatechartEditorParameterEditor &parameterEditor) const;

        Ui::StateDialog* ui;
        statechartmodel::StateInstancePtr stateInstance;
        QMap<QString, QString> eventDescriptionMap;
        QFileDialog* fileDialog;
        static QMap<QString, DynamicLibraryPtr> Libraries;
        Ice::CommunicatorPtr communicator;
        VariantInfoPtr variantInfo;
        StatechartProfilePtr currentProfile;
        QPointer<TipDialog> tipDialog;
        bool readOnly;

        // QWidget interface
    protected:
        void showEvent(QShowEvent*) override;
    };


}
