/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Valerij Wittenbeck (valerij.wittenbeck at student dot kit dot edu
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "EditDefaultValueDialog.h"
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/view/dialogs/ui_EditDefaultValueDialog.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <QFileDialog>

#include <QtGui>
#include <QMessageBox>

#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/variantjson/VariantJsonCompressor.h>
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/variantjson/VariantJsonException.h>

namespace armarx
{
    EditDefaultValueDialog::EditDefaultValueDialog(QString json, QString typeString, const Ice::CommunicatorPtr& communicator, QWidget* parent) :
        QDialog(parent),
        ui(new Ui::EditDefaultValueDialog),
        typeString(typeString),
        communicator(communicator)
    {
        ui->setupUi(this);

        std::string verboseJson = json.toUtf8().data();
        std::string compressedJson;
        try
        {
            compressedJson = VariantJsonCompressor::Compress(verboseJson, typeString.toUtf8().data());
        }
        catch (VariantJsonException& ex)
        {
            compressedJson = verboseJson;
            ui->labelParseResult->setStyleSheet("QLabel { color : red; }");
            ui->labelParseResult->setText(QString::fromUtf8(ex.what()));
        }

        connect(ui->textEditJson, SIGNAL(textChanged()), SLOT(onJsonTextChanged()));
        connect(ui->pushButtonFormat, SIGNAL(clicked()), SLOT(onFormat()));
        connect(ui->pushButtonShowInternalFormat, SIGNAL(clicked()), SLOT(onShowInternalJson()));
        connect(ui->pushButtonImport, SIGNAL(clicked(bool)), SLOT(onImportInternalJson()));
        ui->textEditJson->setText(QString::fromUtf8(compressedJson.c_str()));
    }

    EditDefaultValueDialog::~EditDefaultValueDialog()
    {

    }

    QString EditDefaultValueDialog::getJson()
    {
        std::string compressedJson = ui->textEditJson->toPlainText().toUtf8().data();
        std::string verboseJson = VariantJsonCompressor::Decompress(compressedJson, typeString.toUtf8().data());

        return QString::fromUtf8(verboseJson.c_str());
    }

    void EditDefaultValueDialog::onJsonTextChanged()
    {
        std::string compressedJson = ui->textEditJson->toPlainText().toUtf8().data();
        ParseResult r = VariantJsonCompressor::CheckUserInput(compressedJson, typeString.toUtf8().data(), communicator);
        bool iserr = r.iserr();
        if (iserr)
        {
            ui->labelParseResult->setStyleSheet("QLabel { color : red; }");
            ui->labelParseResult->setText(QString::fromUtf8(r.geterr().c_str()));
        }
        else
        {
            ui->labelParseResult->setStyleSheet("QLabel { color : green }");
            ui->labelParseResult->setText("Input is valid.");
        }
        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(!iserr);
        ui->pushButtonShowInternalFormat->setEnabled(!iserr);
    }

    void EditDefaultValueDialog::onFormat()
    {
        std::string compressedJson = ui->textEditJson->toPlainText().toUtf8().data();
        ParseResult r = VariantJsonCompressor::FormatUserInput(compressedJson);
        if (r.iserr())
        {
            QMessageBox msgBox;
            msgBox.setText(QString::fromStdString(r.geterr()));
            msgBox.exec();
        }
        else
        {
            ui->textEditJson->setText(QString::fromUtf8(compressedJson.c_str()));
        }
    }

    void EditDefaultValueDialog::onShowInternalJson()
    {
        QMessageBox msgBox;
        QString text;
        try
        {
            text = getJson();
        }
        catch (VariantJsonException& ex)
        {
            text = QString("Error: ") + QString::fromStdString(ex.what());
        }

        msgBox.setText(text);
        msgBox.exec();
    }

    void EditDefaultValueDialog::onImportInternalJson()
    {
        QDialog editDefaultDialog;
        editDefaultDialog.setWindowTitle("JSON Import");
        editDefaultDialog.resize(QSize(600, 400));
        QTextEdit* dialogTextEdit = new QTextEdit();
        dialogTextEdit->setAcceptRichText(false);
        dialogTextEdit->setPlainText("");

        QVBoxLayout* layout = new QVBoxLayout;
        layout->addWidget(dialogTextEdit);
        QDialogButtonBox* buttonBox = new QDialogButtonBox(dialogTextEdit);
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel | QDialogButtonBox::Ok);
        layout->addWidget(buttonBox);
        editDefaultDialog.setLayout(layout);

        connect(buttonBox, SIGNAL(accepted()), &editDefaultDialog, SLOT(accept()));
        connect(buttonBox, SIGNAL(rejected()), &editDefaultDialog, SLOT(reject()));

        if (editDefaultDialog.exec() == QDialog::Accepted)
        {
            std::string json(dialogTextEdit->toPlainText().toUtf8().data());
            std::string compressedJson;
            try
            {
                compressedJson = VariantJsonCompressor::Compress(json, typeString.toUtf8().data());
                ui->textEditJson->setText(QString::fromUtf8(compressedJson.c_str()));
            }
            catch (VariantJsonException& ex)
            {
                QMessageBox msgBox;
                msgBox.setText(QString("Error: ") + QString::fromStdString(ex.getReason()));
                msgBox.exec();
            }
            catch (LocalException& ex)
            {
                QMessageBox msgBox;

                msgBox.setText(QString("Error: ") + QString::fromStdString(ex.getReason()));
                msgBox.exec();
            }
        }
    }
}
