/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/observers/variant/VariantInfo.h>
#include <QMainWindow>
#include <QTreeView>
#include <memory>
#include <ArmarXCore/statechart/xmlstates/profiles/StatechartProfiles.h>
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/view/ui_StatechartEditorMainWindow.h>


namespace armarx
{
    class StateTabWidget;
    class CppHighlighter;
}

namespace armarx::Ui
{
    class StatechartEditorMainWindow;
}

namespace armarx
{
    class StatechartEditorMainWindow : public QMainWindow
    {
        Q_OBJECT
        friend class StateEditorController;
        friend class StatechartEditorController;
    public:
        explicit StatechartEditorMainWindow(QWidget* parent = 0);
        ~StatechartEditorMainWindow() override;
        StateTabWidget* getStateTabWidget() const;

        void setCommunicator(Ice::CommunicatorPtr communicator)
        {
            this->communicator = communicator;
        }
        void setVariantInfo(VariantInfoPtr variantInfo)
        {
            this->variantInfo = variantInfo;
        }
        void setCurrentProfile(StatechartProfilePtr currentProfile)
        {
            this->currentProfile = currentProfile;
        }
        void setTipDialog(QPointer<TipDialog> tipDialog);
        Ui::StatechartEditorMainWindow* getUI() const
        {
            return ui;
        }
    public slots:
        void originalZoomCurrentState();
        void deleteSelectedStates();
        void zoomToViewAll();
        void connectOverviewToTab(int tabIndex);
        void layoutState(statechartmodel::StateInstancePtr instance = statechartmodel::StateInstancePtr());
        void addEndState();
    private slots:
        void on_actionSave_as_Image_file_triggered();

    private:
        void setupStatechartGroupsToolbar();

        QToolBar* stGroupToolBar;
        Ui::StatechartEditorMainWindow* ui;
        CppHighlighter* highlighter;
        Ice::CommunicatorPtr communicator;
        VariantInfoPtr variantInfo;
        StatechartProfilePtr currentProfile;
        QPointer<TipDialog> tipDialog;
    };


}
