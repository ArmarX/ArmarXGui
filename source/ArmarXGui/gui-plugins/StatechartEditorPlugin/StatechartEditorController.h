/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "StateEditorController.h"
#include "StateTreeController.h"

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <ArmarXCore/core/system/cmake/ArmarXPackageToolInterface.h>

#include <QFileSystemWatcher>

#include <ArmarXCore/statechart/xmlstates/profiles/StatechartProfiles.h>

#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/view/dialogs/StatechartEditorConfigDialog.h>

#include <ArmarXCore/core/services/tasks/PeriodicTask.h>

#include <ArmarXGui/gui-plugins/StatechartViewerPlugin/StateWatcher.h>

namespace armarx
{
    class StatechartEditorMainWindow;

    /**
      \page ArmarXGui-GuiPlugins-StatechartEditorController StatechartEditor
      \tableofcontents

      The Statechart Editor is a powerful and convenient tool to design complex behavior.
      It is deeply integrated into ArmarX and tailored onto it.
      It allows the user to design hierarchical, distributed statecharts inspired by Harel, 84 [1] in
      a graphical way and generate and connect it directly with C++ code.
      For a step-by-step tutorial refer to: \ref ArmarXCore-Tutorials-sce

      A detailed explanation of ArmarX Statecharts can be found in the publication:<br/>
      M. Wächter, S. Ottenhaus, M. Kröhnert, N. Vahrenkamp and T. Asfour (2016), *The ArmarX Statechart Concept: Graphical Programming of Robot Behaviour*, Front. Robot. AI 3:33.
      <a href="http://journal.frontiersin.org/article/10.3389/frobt.2016.00033/full">[DOI]</a>

      [1]: David Harel: *Statecharts: A Visual Approach to Complex Systems*, CS84-05, Department of Applied Mathematics, The Weizmann Institute of Science, 1984

      \image html StatechartEditor.png

      \see \ref Statechart
      \see \ref ArmarXGui-GuiPlugins-StatechartViewer
      \see \ref RobotSkillTemplates
      \see \ref StatechartEditorController
      \see \ref ArmarXGui-GuiPlugins-EventSender
      \see \ref ArmarXCore-Tutorials-sce

      \section SCE-Group-Execution Statechart Execution from the Statechart Editor
      Statecharts are always executed as part of their statechart group.
      There are two different ways to execute a statechart group:
      \li <b>From the Statechart Editor directly:</b> In the context menu of the statechart group it is possible
      to execute a statechart group directly. This option is meant for quick testing of statecharts. All statechart group dependencies
      are automatically also executed (they need to be loaded in the current session of the Statechart Editor for this).
      Before execution a public state that should immediately be entered can be selected. Note
      that this state must not need any input paramters.
      \li <b>Starting a statechart group with a scenario:</b> The usual way to start a statechart group is as part of
      a scenario. For this option, it is possible to drag'n'drop a statechart group from the Statechart Editor to the \ref ArmarXGui-GuiPlugins-ScenarioManagerGuiPlugin "ScenarioManager".
      The statechart group needs to be dropped on an open scenario. It is then fully parameterized and ready to be started.
      Note that for this option all statechart group dependencies need to be added to the scenario manually as well.
      In the properties of the statechart group it is possible to define a set of state that should be entered automatically. These states will run in parallel.

      \section SCE-Intro Statechart Editor Intro: Making Armar wave in 4 minutes
      In this video, the Statechart Editor used to program a simple
      statechart that makes Armar3 wave. This demonstrates the basic features of the Statechart Editor.
      There was nothing prepared except starting the simulation and getting the needed joint values for the arm joints.
      \htmlonly
    <video width="700" controls>
    <source src="images/SCE-intro-2016-03-25_17.38.00.mp4" type="video/mp4">
    Your browser does not support HTML5 video.
    </video>
      \endhtmlonly

      \section SCE-Configurations Editing the runtime configuration of a statechart group
      Statecharts groups need like component often parameters to work with one specific robot.
      For statechart groups this can be done with the Statechart Editor. In this video, it is shown
      how to setup the configuration for one group. This configuration is then always applied when starting
      the statechart group.
      \htmlonly
    <video width="700" controls>
    <source src="images/SCE-configurations-2016-03-24_03.13.42.mp4" type="video/mp4">
    Your browser does not support HTML5 video.
    </video>
      \endhtmlonly

      \section SCE-GroupCloning Cloning a statechart group to another package
      \htmlonly
    <video width="700" controls>
    <source src="images/SCE-cloning-2016-03-24_03.50.53.mp4" type="video/mp4">
    Your browser does not support HTML5 video.
    </video>
      \endhtmlonly


      \section SCE-DirectMapping Direct mapping of parameters during transitions
      \htmlonly
    <video width="700" controls>
    <source src="images/SCE-directvaluemapping-2016-03-30_23.44.26.mp4" type="video/mp4">
    Your browser does not support HTML5 video.
    </video>
      \endhtmlonly


     */


    /**
      \class StatechartEditorController
      \brief The StatechartEditorController class is the controller of the main widget of the Statechart Editor.
     */
    class StatechartEditorController :
        public ArmarXComponentWidgetControllerTemplate<StatechartEditorController>
    {
        Q_OBJECT

    public:
        StatechartEditorController();
        ~StatechartEditorController() override;

        QStringList findAllStatechartGroupDefinitions(const QString& basePath);

    public slots:
        void treeviewGroupsDoubleClicked(QModelIndex index);
        void requestSave();
        void onStateTabChanged(int index);
        void showNewStatechartGroupDialog();
        void showStatechartEditorSettingsDialog();
        void searchAndOpenPaths(QStringList paths);

        void connectToView(int tabIndex);
        void showStateCode(statechartmodel::StateInstancePtr stateInstance);
        void showCodeFileContent(const QString& path);
        void showOnEnterFunction();
        void showRunFunction();
        void showOnBreakFunction();
        void showOnExitFunction();
        void openSelectedState();
        void closeAllTabs();
        void executeOpenedState(bool);
    public:
        // inherited from Component
        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;

        // inherited of ArmarXWidget
        static QString GetWidgetName()
        {
            return "Statecharts.StatechartEditor";
        }
        static QIcon GetWidgetIcon()
        {
            return QIcon(":/statechart-editor/states.svg");
        }
        static QIcon GetWidgetCategoryIcon()
        {
            return QIcon(":/statechart-editor/states.svg");
        }
        void loadSettings(QSettings* settings) override;
        void saveSettings(QSettings* settings) override;

        // ArmarXWidgetController interface
        QPointer<QWidget> getWidget() override;
        void openStatechartGroups(QStringList groups);
    private slots:
        void initWidget();
    private:
        void updateExecutionButtonStatus();
        void watchState(const std::string& objName);
        QPointer<StatechartEditorMainWindow> editor;
        StateEditorControllerPtr stateEditorController;
        StateTreeControllerPtr treeController;
        ArmarXPackageToolInterfacePtr packageTool;
        QFileSystemWatcher* watcher;
        StateWatcherPtr stateWatcher;
        bool alreadyWatchingState = false;
        VariantInfoPtr variantInfo;
        StatechartProfilesPtr profiles;
        QPointer<StatechartEditorConfigDialog> dialog;
        StatechartGroupPtr executedOpenedGroup;
        PeriodicTask<StatechartEditorController>::pointer_type executionStatusTask;
        // ArmarXWidgetController interface
    public:
        QPointer<QDialog> getConfigDialog(QWidget* parent) override;
        struct Config
        {
            QStringList searchPaths;
            QStringList groupsToLoad;
            bool lockRemoteStates;
            bool openAllStatesWasSelected;
            StatechartProfilePtr selectedProfile;
        } config;



        // ArmarXWidgetController interface
        bool onClose() override;
        void configured() override;
        void storeAutoSaveSettings();

    };

}

