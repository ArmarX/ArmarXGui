/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "StatechartGroupMapping.h"

#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlWriter.h>

#include <QString>
#include <QDir>

#include <set>

namespace armarx
{

    std::optional<StatechartGroupMapping> StatechartGroupMapping::ReadStatechartGroupMappingFile(const QString& packageDir)
    {
        StatechartGroupMapping result;
        const QString packageName = QFileInfo(packageDir).fileName();
        const QString mappingFilePath =
            packageDir + QDir::separator() +
            "data" + QDir::separator() +
            packageName + QDir::separator() +
            "StatechartGroupMapping-" + packageName + ".xml";

        if (!QFileInfo(mappingFilePath).exists())
        {
            return std::nullopt;
        }

        auto readerPtr = RapidXmlReader::FromFile(mappingFilePath.toUtf8().data());
        RapidXmlReaderNode rootNode = readerPtr->getRoot();

        for (RapidXmlReaderNode groupNode : rootNode.nodes())
        {
            if (groupNode.name() != "Group")
            {
                continue;
            }

            const auto groupName = QString::fromUtf8(groupNode.attribute_value("name").c_str());
            const auto newGroupName = QString::fromUtf8(groupNode.attribute_value("new-name").c_str());
            const auto package = QString::fromUtf8(groupNode.attribute_value("source-package").c_str());

            StatechartGroupMapping::GroupMapping groupMapping {groupName, newGroupName, package, {}};

            for (RapidXmlReaderNode stateNode : groupNode.nodes())
            {
                if (stateNode.name() != "Mapping")
                {
                    continue;
                }

                const auto stateName = QString::fromUtf8(stateNode.attribute_value("state").c_str());
                const auto fromUuid = QString::fromUtf8(stateNode.attribute_value("from").c_str());
                const auto toUuid = QString::fromUtf8(stateNode.attribute_value("to").c_str());
                groupMapping.stateMappings.insert({stateName, fromUuid, toUuid});
            }

            result.groupMappings.insert(groupMapping);
        }

        return result;
    }

    std::optional<QString> StatechartGroupMapping::queryMappedGroupName(const QString& sourceGroupName) const
    {
        for (const auto& groupMapping : groupMappings)
        {
            if (groupMapping.groupName == sourceGroupName)
            {
                return groupMapping.newGroupName;
            }
        }

        return std::nullopt;
    }

    std::optional<QString> StatechartGroupMapping::queryMappedUuid(const QString& sourceUuid) const
    {
        for (const auto& groupMapping : groupMappings)
        {
            for (const auto& stateMapping : groupMapping.stateMappings)
            {
                if (stateMapping.fromUuid == sourceUuid)
                {
                    return stateMapping.toUuid;
                }
            }
        }

        return std::nullopt;
    }

    void StatechartGroupMapping::writeToFile(const QString& packageDir) const
    {
        const QString packageName = QFileInfo(packageDir).fileName();
        const QString packageDataDir = packageDir + QDir::separator() + "data" + QDir::separator() + packageName;
        QDir(packageDataDir).mkpath(".");
        const QString mappingFilePath = packageDataDir + QDir::separator() + "StatechartGroupMapping-" + packageName + ".xml";

        RapidXmlWriter writer;
        auto rootNode = writer.createRootNode("StatechartGroupMapping");

        for (const auto& g : groupMappings)
        {
            auto groupNode = rootNode.append_node("Group");
            groupNode.append_attribute("name", g.groupName.toUtf8().data());
            groupNode.append_attribute("new-name", g.newGroupName.toUtf8().data());
            groupNode.append_attribute("source-package", g.groupPackage.toUtf8().data());

            for (const auto& s : g.stateMappings)
            {
                auto stateNode = groupNode.append_node("Mapping");
                stateNode.append_attribute("state", s.stateName.toUtf8().data());
                stateNode.append_attribute("from", s.fromUuid.toUtf8().data());
                stateNode.append_attribute("to", s.toUuid.toUtf8().data());
            }
        }

        writer.saveToFile(mappingFilePath.toUtf8().data(), true);
    }

}
