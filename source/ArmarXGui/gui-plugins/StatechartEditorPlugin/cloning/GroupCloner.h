/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <memory>

#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlWriter.h>

#include <QString>
#include <QMap>

#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/model/StatechartGroup.h>
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/model/StateTreeNode.h>
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/model/StateTreeModel.h>
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/io/GroupXmlReader.h>
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/io/GroupXmlWriter.h>
#include <ArmarXCore/core/system/cmake/ArmarXPackageToolInterface.h>

#include "StatechartGroupMapping.h"

namespace armarx
{
    class GroupCloner;
    using GroupClonerPtr = std::shared_ptr<GroupCloner>;

    class GroupCloner
    {

    public:

        GroupCloner(const std::weak_ptr<StateTreeModel>& treeModel, const ArmarXPackageToolInterfacePtr& packageTool);
        std::optional<StatechartGroupMapping> cloneGroupsTo(
            const QVector<QPair<StatechartGroupPtr, QString>>& groupsNames,
            const QString& packageDir,
            const StatechartGroupMapping& mapping = StatechartGroupMapping(),
            bool createNewUUIDs = true);

        QVector<StatechartGroupPtr> getGroupDependencies(const StatechartGroupPtr& group, bool includeSelf = false);
        QVector<StatechartGroupPtr> getMappedGroups(const StatechartGroupMapping& mapping);

        template<typename Function>
        static void ProcessXMLFile(RapidXmlReaderNode source, RapidXmlWriterNode target, const Function& processorFunction)
        {
            for (const auto& pair : source.get_all_attributes())
            {
                std::string valueToStore = processorFunction(source.name(), pair.first, pair.second);
                target.append_attribute(pair.first, valueToStore);
            }

            for (RapidXmlReaderNode node : source.nodes())
            {
                if (node.type() == rapidxml::node_data)
                {
                    target.append_data_node(node.value());
                }
                else if (node.type() == rapidxml::node_element)
                {
                    ProcessXMLFile(node, target.append_node(node.name()), processorFunction);
                }
                else
                {
                    throw LocalException("Unsupported node type.");
                }
            }
        }

        static QVector<StatechartGroupPtr> GetGroupsFromStates(const StateTreeModelPtr& treeModel, const QVector<statechartmodel::StatePtr>& states);
        static bool RemoveDir(const QString& dirName);
        static void RegexReplaceFile(const QString& srcFilePath, const QString& tgtFilePath, const QVector<QPair<QRegExp, QString>>& replaceList);

    private:
        static bool CopyRecursively(const QString& srcPath, const QString& tgtParentDirPath);

        std::weak_ptr<StateTreeModel> treeModel;
        ArmarXPackageToolInterfacePtr packageTool;

        void registerGroups(const QVector<StatechartGroupPtr>& groups, const StatechartGroupMapping& mapping, const QString& packageDir);
        bool cloneGroupTo(const StatechartGroupPtr& group, const QString& newGroupFolder, const StatechartGroupMapping& mapping);
    };
}

