/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "GroupCloner.h"

#include <filesystem>
#include <IceUtil/UUID.h>
#include <QDir>
#include <QDirIterator>
#include <QFileInfo>
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/io/XmlReader.h>
#include <filesystem>

using namespace armarx;
using namespace armarx::statechartmodel;

GroupCloner::GroupCloner(const std::weak_ptr<StateTreeModel>& treeModel, const ArmarXPackageToolInterfacePtr& packageTool) :
    treeModel(treeModel),
    packageTool(packageTool)
{
}


QVector<StatechartGroupPtr> GroupCloner::getGroupDependencies(const StatechartGroupPtr& group, bool includeSelf)
{
    QVector<statechartmodel::StatePtr> states = group->getAllStates(false);

    auto result = GetGroupsFromStates(treeModel.lock(), states);

    if (!includeSelf)
    {
        auto it = std::remove_if(result.begin(), result.end(), [&](const StatechartGroupPtr & g)
        {
            return g->getGroupPath() == group->getGroupPath();
        });
        result.erase(it, result.end());
    }

    return result;
}

QVector<StatechartGroupPtr> GroupCloner::getMappedGroups(const StatechartGroupMapping& mapping)
{
    QVector<StatechartGroupPtr> result;

    for (const auto& groupMapping : mapping.groupMappings)
    {
        auto group = treeModel.lock()->getGroupByName(groupMapping.groupName);

        if (!group)
        {
            ARMARX_WARNING_S << "Group " << groupMapping.groupName << " was not loaded";
        }
        else
        {
            result.push_back(group);
        }
    }

    return result;
}

std::optional<StatechartGroupMapping> GroupCloner::cloneGroupsTo(const QVector<QPair<StatechartGroupPtr, QString>>& groupsNames, const QString& packageDir, const StatechartGroupMapping& mapping, bool createNewUUIDs)
{
    StatechartGroupMapping newMapping = mapping;
    ARMARX_INFO_S << "Groups that will be cloned: ";

    for (const auto& gEntry : groupsNames)
    {
        const auto& g = gEntry.first;
        ARMARX_INFO_S << " " << g->getName() << " -> " << gEntry.second;

        bool groupMapped = std::find_if(mapping.groupMappings.cbegin(), mapping.groupMappings.cend(), [&](const StatechartGroupMapping::GroupMapping & gm)
        {
            return gm.groupName == g->getName();
        }) != mapping.groupMappings.cend();

        if (groupMapped)
        {
            ARMARX_WARNING_S << "Mapping file seems to be inconsistent with folder structure";
            return std::optional<StatechartGroupMapping>();
        }

        QVector<statechartmodel::StatePtr> states = g->getAllStates();

        StatechartGroupMapping::GroupMapping groupMapping {g->getName(), gEntry.second, g->getPackageName(), {}};

        for (const statechartmodel::StatePtr& state : states)
        {
            bool stateMapped = std::find_if(groupMapping.stateMappings.cbegin(), groupMapping.stateMappings.cend(), [&](const StatechartGroupMapping::StateMapping & sm)
            {
                return sm.stateName == state->getStateName();
            }) != groupMapping.stateMappings.cend();

            if (!stateMapped)
            {
                QString targetUUID = state->getUUID();

                if (createNewUUIDs)
                {
                    targetUUID = QString::fromUtf8(IceUtil::generateUUID().c_str());
                }

                groupMapping.stateMappings.insert({state->getStateName(), state->getUUID(), targetUUID});
            }
        }

        newMapping.groupMappings.insert(groupMapping);
    }

    QVector<StatechartGroupPtr> groups;
    std::transform(groupsNames.begin(), groupsNames.end(), std::back_inserter(groups),
                   [](const QPair<StatechartGroupPtr, QString>& p)
    {
        return p.first;
    });

    for (const StatechartGroupPtr& g : groups)
    {
        if (!g->existsCMakeLists())
        {
            ARMARX_WARNING_S << g->getName()  << " at " << g->getGroupPath() << " contains no CMakeLists.txt - is it an installed package? Installed packages cannot be cloned since they do not contain the CMakelists file and CPP files";
            return std::optional<StatechartGroupMapping>();
        }
    }

    registerGroups(groups, newMapping, packageDir);

    for (const auto& g : groups)
    {
        if (!cloneGroupTo(g, packageDir, newMapping))
        {
            ARMARX_WARNING_S << "Cloning group '" << g->getName() << "' failed, aborting...";
            return std::optional<StatechartGroupMapping>();
        }
    }

    ARMARX_INFO_S << "Cloning done";

    return std::optional<StatechartGroupMapping>(newMapping);
}

void GroupCloner::registerGroups(const QVector<StatechartGroupPtr>& groups, const StatechartGroupMapping& mapping, const QString& packageDir)
{
    const QString packageName = QFileInfo(packageDir).fileName();

    for (const StatechartGroupPtr& g : groups)
    {
        if (auto groupName = mapping.queryMappedGroupName(g->getName()))
        {
            QString tempStateName = *groupName + "TempState";
            packageTool->addStatechart(groupName->toUtf8().data(), tempStateName.toUtf8().data(), packageDir.toUtf8().data());
            RemoveDir(
                packageDir + QDir::separator() +
                "source" + QDir::separator() +
                packageName + QDir::separator() +
                "statecharts" + QDir::separator() +
                *groupName);
        }
    }
}

bool GroupCloner::cloneGroupTo(const StatechartGroupPtr& group, const QString& packageFolder, const StatechartGroupMapping& mapping)
{
    QFileInfo packageInfo(packageFolder);
    ARMARX_CHECK_EXPRESSION(packageInfo.isDir() && packageInfo.exists());

    auto newGroupNameOpt = mapping.queryMappedGroupName(group->getName());
    ARMARX_CHECK_EXPRESSION(newGroupNameOpt);
    QString newGroupName = *newGroupNameOpt;

    const QString oldPackageName = group->getPackageName();
    const QString newPackageName = QFileInfo(packageFolder).fileName();
    const QString packageStatechartsDir =
        packageFolder + QDir::separator() + "source" + QDir::separator() + newPackageName + QDir::separator() + "statecharts";
    QString newGroupPath = packageStatechartsDir + QDir::separator() + newGroupName;

    ARMARX_INFO_S << "Cloning '" << group->getGroupPath() << "' to '" << newGroupPath << "'";

    if (QDir(newGroupPath).exists())
    {
        ARMARX_WARNING_S << newGroupPath << " already exists, but should not exist. Skipping...";
        return false;
    }
    else if (!QDir(newGroupPath).mkpath("."))
    {
        ARMARX_ERROR_S << "Could not create directory: " << newGroupPath;
        return false;
    }

    QMap<QString, QString> renameCandidateMap;
    renameCandidateMap.insert(group->getName() + ".scgxml", newGroupName + ".scgxml");
    renameCandidateMap.insert(group->getName() + "RemoteStateOfferer.h", newGroupName + "RemoteStateOfferer.h");
    renameCandidateMap.insert(group->getName() + "RemoteStateOfferer.cpp", newGroupName + "RemoteStateOfferer.cpp");
    renameCandidateMap.insert(group->getName() + "StatechartContext.h", newGroupName + "StatechartContext.h");
    renameCandidateMap.insert(group->getName() + "StatechartContext.cpp", newGroupName + "StatechartContext.cpp");

    QMap<QString, bool> expectedFileSeenMap;
    expectedFileSeenMap.insert("CMakeLists.txt", false);
    expectedFileSeenMap.insert(group->getName() + ".scgxml", false);
    expectedFileSeenMap.insert(group->getName() + "RemoteStateOfferer.h", false);
    expectedFileSeenMap.insert(group->getName() + "RemoteStateOfferer.cpp", false);

    // the context files don't have to exist; the contents are normally generated as part of the generated files
    QVector<QString> optionalExpectedFiles;
    optionalExpectedFiles.push_back(group->getName() + "StatechartContext.generated.h");
    optionalExpectedFiles.push_back(group->getName() + "StatechartContext.h");
    optionalExpectedFiles.push_back(group->getName() + "StatechartContext.cpp");

    QVector<QString> doNotCopy;
    doNotCopy.push_back(group->getName() + "StatechartContext.generated.h");

    QVector<StateTreeNodePtr> allNodes = group->getAllNodes();

    for (const StateTreeNodePtr& n : allNodes)
    {
        if (!n->isState())
        {
            continue;
        }

        const QString xml = QString::fromUtf8(n->getBoostXmlFilePath(StateTreeNode::Path::RelativeToGroup).c_str());
        const QString cpp = QString::fromUtf8(n->getBoostCppFilePath(StateTreeNode::Path::RelativeToGroup).c_str());
        const QString h = QString::fromUtf8(n->getBoostHFilePath(StateTreeNode::Path::RelativeToGroup).c_str());
        const QString gen = QString::fromUtf8(n->getBoostGeneratedHFilePath(StateTreeNode::Path::RelativeToGroup).c_str());

        expectedFileSeenMap.insert(xml, false);

        if (n->checkCppExists())
        {
            expectedFileSeenMap.insert(cpp, false);
            expectedFileSeenMap.insert(h, false);
            optionalExpectedFiles.push_back(gen);
        }

        doNotCopy.push_back(gen);
    }

    QVector<QPair<QRegExp, QString>> codeFileReplaceList;
    codeFileReplaceList.push_back({QRegExp(oldPackageName + "_" + group->getName()), newPackageName + "_" + newGroupName});
    codeFileReplaceList.push_back({QRegExp(oldPackageName + "::" + group->getName()), newPackageName + "::" + newGroupName});
    codeFileReplaceList.push_back({QRegExp("namespace[\\n\\s]*" + group->getName()), "namespace " + newGroupName});
    codeFileReplaceList.push_back({QRegExp("namespace[\\n\\s]*armarx::" + group->getName()), "namespace armarx::" + newGroupName});
    codeFileReplaceList.push_back({QRegExp(group->getName() + "StatechartContext"), newGroupName + "StatechartContext"});
    codeFileReplaceList.push_back({QRegExp(group->getName() + "RemoteStateOfferer"), newGroupName + "RemoteStateOfferer"});
    codeFileReplaceList.push_back({QRegExp(oldPackageName + "/statecharts/" + group->getName()), newPackageName + "/statecharts/" + newGroupName});

    QVector<QPair<QRegExp, QString>> cmakeListsReplaceList;
    cmakeListsReplaceList.push_back({QRegExp("armarx_component_set_name\\(\"" + group->getName() + "\"\\)"),
                                     "armarx_component_set_name(\"" + newGroupName + "\")"
                                    });
    cmakeListsReplaceList.push_back({QRegExp(group->getName() + "\\.scgxml"), newGroupName + ".scgxml"});
    cmakeListsReplaceList.push_back({QRegExp(group->getName() + "StatechartContext"), newGroupName + "StatechartContext"});
    cmakeListsReplaceList.push_back({QRegExp(group->getName() + "RemoteStateOfferer"), newGroupName + "RemoteStateOfferer"});

    auto xmlProcessor = [&](const std::string&, const std::string & attribName, const std::string & attribValue) -> std::string
    {
        QString value = QString::fromUtf8(attribValue.c_str());

        if (attribName == "refuuid" || attribName == "uuid")
        {
            QString value = QString::fromUtf8(attribValue.c_str());

            if (auto qresult = mapping.queryMappedUuid(value))
            {
                return qresult->toUtf8().data();
            }
            else
            {
                ARMARX_WARNING_S << "No mapping was generated for uuid " << value;
            }
        }
        else if (attribName == "proxyName")
        {
            value = value.replace("RemoteStateOfferer", "");

            if (auto qresult = mapping.queryMappedGroupName(value))
            {
                return std::string(qresult->toUtf8().data()) + "RemoteStateOfferer";
            }
            else
            {
                ARMARX_WARNING_S << "No mapping was generated for group " << value;
            }
        }

        return attribValue;
    };

    auto scgxmlProcessor = [&](const std::string&, const std::string & attribName, const std::string & attribValue) -> std::string
    {
        QString value = QString::fromUtf8(attribValue.c_str());

        if (attribName == "package" && oldPackageName == value)
        {
            return newPackageName.toUtf8().data();
        }
        else if (attribName == "name")
        {
            if (auto newGroupName = mapping.queryMappedGroupName(value))
            {
                return newGroupName->toUtf8().data();
            }
        }

        return attribValue;
    };

    QDirIterator it(group->getGroupPath(), QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot, QDirIterator::Subdirectories);

    while (it.hasNext())
    {
        const auto f = QFileInfo(it.next());
        QString relativePath = QDir(group->getGroupPath()).relativeFilePath(f.absoluteFilePath());
        QString srcFilePath = group->getGroupPath() + QDir::separator() + relativePath;
        QString tgtFilePath = newGroupPath + QDir::separator() + (renameCandidateMap.contains(relativePath) ? renameCandidateMap[relativePath] : relativePath);

        if (f.isDir())
        {
            if (!QDir(tgtFilePath).mkpath("."))
            {
                ARMARX_WARNING_S << "Could not create directory at path: " << tgtFilePath;
                return false;
            }

            ARMARX_VERBOSE_S << "Created directory '" << tgtFilePath << "'";
            continue;
        }

        if (doNotCopy.contains(relativePath))
        {
            ARMARX_VERBOSE_S << "Skipping file '" << relativePath << "'";
            continue;
        }

        ARMARX_DEBUG_S << "Cloning file '" << srcFilePath << "' -> '" << tgtFilePath << "'";

        if (expectedFileSeenMap.contains(relativePath))
        {
            expectedFileSeenMap[relativePath] = true;
        }
        else if (!optionalExpectedFiles.contains(relativePath))
        {
            ARMARX_WARNING_S << "Unexpected file '" << relativePath << "', processing anyway... - expected files" << expectedFileSeenMap.toStdMap();
        }

        if (f.suffix() == "xml" || f.suffix() == "scgxml")
        {
            ARMARX_VERBOSE_S << "Processing xml file: " << relativePath;
            auto readerPtr = RapidXmlReader::FromFile(srcFilePath.toUtf8().data());
            RapidXmlReaderNode rootNode = readerPtr->getRoot();
            RapidXmlWriter writer;
            auto writerNode = writer.createRootNode(rootNode.name());

            if (f.suffix() == "xml")
            {
                ProcessXMLFile(rootNode, writerNode, xmlProcessor);
            }
            else
            {
                ProcessXMLFile(rootNode, writerNode, scgxmlProcessor);
            }

            writer.saveToFile(tgtFilePath.toUtf8().data(), true);
        }
        else if (f.suffix() == "cpp" || f.suffix() == "h")
        {
            ARMARX_VERBOSE_S << "Processing code file: " << relativePath;
            RegexReplaceFile(srcFilePath, tgtFilePath, codeFileReplaceList);
        }
        else if (f.fileName() == "CMakeLists.txt")
        {
            ARMARX_VERBOSE_S << "Processing CMakeLists file: " << relativePath;
            RegexReplaceFile(srcFilePath, tgtFilePath, cmakeListsReplaceList);
        }
        else
        {
            ARMARX_VERBOSE_S << "Copying file: " << relativePath;
            QFile::copy(srcFilePath, tgtFilePath);
        }
    }

    for (const auto& f : expectedFileSeenMap.toStdMap())
    {
        if (!f.second)
        {
            ARMARX_WARNING_S << "Expected file '" << f.first << "' was not found while cloning";
        }
    }

    ARMARX_INFO_S << "Cloned group " << group->getName();
    return true;
}

QVector<StatechartGroupPtr> GroupCloner::GetGroupsFromStates(const StateTreeModelPtr& treeModel, const QVector<statechartmodel::StatePtr>& states)
{
    QVector<StatechartGroupPtr> result;

    for (const auto& state : states)
    {
        auto stateGroup = treeModel->getNodeByState(state)->getGroup();
        ARMARX_CHECK_EXPRESSION(stateGroup);
        bool alreadyExists = std::find_if(result.constBegin(), result.constEnd(), [&](const StatechartGroupPtr & depGroup)
        {
            return stateGroup->getDefinitionFilePath() == depGroup->getDefinitionFilePath();
        }) != result.constEnd();

        if (!alreadyExists)
        {
            result.push_back(stateGroup);
        }
    }

    return result;
}

bool GroupCloner::CopyRecursively(const QString& srcPath, const QString& tgtParentDirPath)
{
    QFileInfo srcInfo(srcPath);
    QFileInfo tgtInfo(tgtParentDirPath);

    if (srcInfo.fileName() == tgtInfo.fileName())
    {
        auto newDir = QDir(tgtParentDirPath);
        newDir.cdUp();
        return CopyRecursively(srcPath, newDir.path());
    }

    if (!tgtInfo.isDir() && tgtInfo.exists())
    {
        ARMARX_WARNING_S << "Destination '" << tgtParentDirPath << "' has to be a folder" << flush;
        return false;
    }

    if (srcInfo.isDir())
    {
        const QString newTgtDirPath = tgtParentDirPath + QDir::separator() + srcInfo.fileName();
        const auto newDir = QDir(newTgtDirPath);

        if (newDir.exists())
        {
            ARMARX_IMPORTANT_S << "Target dir already exists: " << newTgtDirPath;
            return false;
        }

        if (!newDir.mkpath("."))
        {
            ARMARX_IMPORTANT_S << "Failed creating dir: " << newTgtDirPath;
            return false;
        }

        QDir sourceDir(srcPath);
        QStringList fileNames = sourceDir.entryList(QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot);

        for (const QString& fileName : fileNames)
        {
            const QString newSrcFilePath = srcPath + QDir::separator() + fileName;

            if (!CopyRecursively(newSrcFilePath, newTgtDirPath))
            {
                return false;
            }
        }
    }
    else
    {
        const QString newTgtFilePath = tgtParentDirPath + QDir::separator() + srcInfo.fileName();

        if (!QFile::copy(srcPath, newTgtFilePath))
        {
            ARMARX_IMPORTANT_S << "Could not copy file: " << srcPath << " -> " << newTgtFilePath;
            return false;
        }
    }

    return true;
}

void GroupCloner::RegexReplaceFile(const QString& srcFilePath, const QString& tgtFilePath, const QVector<QPair<QRegExp, QString>>& replaceList)
{
    QString fileContent = GuiStatechartGroupXmlReader::ReadFileContents(srcFilePath);

    for (const auto& replace : replaceList)
    {
        fileContent = fileContent.replace(replace.first, replace.second);
    }

    GroupXmlWriter::WriteFileContents(tgtFilePath, fileContent);
}

bool GroupCloner::RemoveDir(const QString& dirName)
{
    bool result = true;
    QDir dir(dirName);

    if (dir.exists(dirName))
    {
        for (QFileInfo info : dir.entryInfoList(QDir::NoDotAndDotDot | QDir::AllDirs | QDir::Files, QDir::DirsFirst))
        {
            if (info.isDir())
            {
                result = RemoveDir(info.absoluteFilePath());
            }
            else
            {
                result = QFile::remove(info.absoluteFilePath());
            }

            if (!result)
            {
                return result;
            }
        }

        result = dir.rmdir(dirName);
    }

    return result;
}
