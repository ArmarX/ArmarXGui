/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <QString>

#include <optional>
#include <set>

namespace armarx
{

    struct StatechartGroupMapping
    {
        struct StateMapping
        {
            QString stateName;
            QString fromUuid;
            QString toUuid;

            bool operator<(const StateMapping& rhs) const
            {
                return stateName.compare(rhs.stateName) < 0;
            }
        };

        struct GroupMapping
        {
            QString groupName;
            QString newGroupName;
            QString groupPackage;
            std::set<StateMapping> stateMappings;

            bool operator<(const GroupMapping& rhs) const
            {
                return groupName.compare(rhs.groupName) < 0;
            }
        };
        std::set<GroupMapping> groupMappings;

        static std::optional<StatechartGroupMapping> ReadStatechartGroupMappingFile(const QString& packageDir);

        std::optional<QString> queryMappedGroupName(const QString& sourceGroupName) const;
        std::optional<QString> queryMappedUuid(const QString& sourceUuid) const;
        void writeToFile(const QString& packageDir) const;
    };

}

