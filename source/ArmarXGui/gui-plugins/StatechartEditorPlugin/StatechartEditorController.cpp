/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "StatechartEditorController.h"

#include "view/StatechartEditorMainWindow.h"
#include <ArmarXGui/gui-plugins/StatechartViewerPlugin/view/StateTabWidget.h>

#include "model/StateTreeModel.h"
#include "io/GroupXmlReader.h"
#include "io/GroupXmlWriter.h"
#include "view/dialogs/EditStatechartGroupDialog.h"
#include "view/dialogs/StatechartEditorSettingsDialog.h"
#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/TipDialog.h>

#include "../StatechartViewerPlugin/view/StatechartView.h"
#include "../StatechartViewerPlugin/view/StateItem.h"
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/interface/statechart/RemoteStateOffererIce.h>

#include <IceUtil/UUID.h>

#include <QCoreApplication>
#include <QInputDialog>
#include <QLabel>
#include <QMessageBox>
#include <QTimer>

#include <SimoxUtility/algorithm/string/string_tools.h>

#include <filesystem>

namespace armarx
{

    StatechartEditorController::StatechartEditorController() :
        //        editor(NULL)
        watcher(NULL)
    {
        // For jsonobject double/float serilization on german/spanish pcs....
        setlocale(LC_ALL, "C");

        QSettings s("KIT", "ArmarXStatechartEditor");
        config.lockRemoteStates = s.value("lockRemoteStates", true).toBool();
        config.searchPaths = s.value("searchPaths").toStringList();


    }


    StatechartEditorController::~StatechartEditorController()
    {
        storeAutoSaveSettings();
    }

    void StatechartEditorController::storeAutoSaveSettings()
    {
        ARMARX_INFO << "Updating auto-storage config";
        QSettings s("KIT", "ArmarXStatechartEditor");
        s.setValue("lockRemoteStates", config.lockRemoteStates);
        s.setValue("searchPaths", config.searchPaths);
    }

    QStringList StatechartEditorController::findAllStatechartGroupDefinitions(const QString& basePath)
    {
        QStringList result;
        if (basePath.isEmpty())
        {
            return result;
        }
        try
        {
            int i = 0;

            for (std::filesystem::recursive_directory_iterator end, dir(basePath.toUtf8().data());
                 dir != end && getState() < eManagedIceObjectExiting; ++dir, i++)
            {
                std::string path(dir->path().c_str());
                // search for all statechart group xml files
                if (dir->path().extension() == ".scgxml")
                {
                    // skip groups in deprecated folders
                    if (path.find("deprecated") != std::string::npos)
                    {
                        ARMARX_INFO << "Skipping deprecated Statechart Group " << path;
                    }
                    else
                    {
                        result << dir->path().c_str();
                    }
                }

                if (i % 100 == 0)
                {
                    QCoreApplication::processEvents();
                }
            }
        }
        catch (std::exception& e)
        {
            ARMARX_WARNING << "Invalid filepath: " << e.what();
        }

        return result;
    }

    void StatechartEditorController::onInitComponent()
    {
        packageTool.reset(new ArmarXPackageToolInterface());

    }

    void StatechartEditorController::onConnectComponent()
    {
        QTimer::singleShot(0, this, SLOT(initWidget()));
        stateWatcher = new StateWatcher();
        getArmarXManager()->addObject(stateWatcher, "StateWatcher" + IceUtil::generateUUID(), false);
    }

    void StatechartEditorController::onDisconnectComponent()
    {
    }

    void StatechartEditorController::onExitComponent()
    {
        if (executionStatusTask)
        {
            executionStatusTask->stop();
        }
    }

    void StatechartEditorController::loadSettings(QSettings* settings)
    {
        //        searchPaths = settings->value("paths").toStringList();
        //        int size = settings->beginReadArray("groups");
        //        for(int row = 0; row < size; row++)
        //        {
        //            settings->setArrayIndex(row);
        //            config.groupsToLoad.push_back(settings->value("path").toString());
        //        }
        //        settings->endArray();
        if (!profiles)
        {
            profiles = StatechartProfiles::ReadProfileFiles(Application::getInstance()->getDefaultPackageNames());
        }

        if (settings->contains("selectedProfile"))
        {
            config.selectedProfile = profiles->getProfileByName(settings->value("selectedProfile").toString().toStdString());
        }
        else if (getConfigDialog(getWidget())->exec() == QDialog::Accepted)
        {
            configured();
        }
        else
        {
            config.selectedProfile = profiles->getProfileByName(profiles->GetRootName());
        }

        config.openAllStatesWasSelected = settings->value("openAllStatesSelected", false).toBool();
    }

    void StatechartEditorController::saveSettings(QSettings* settings)
    {
        //        settings->setValue("paths", searchPaths);
        //        if(treeController)
        //        {
        //            settings->beginWriteArray("groups");
        //            for(int row = 0; row < treeController->rowCount(); row++)
        //            {
        //                QModelIndex index = treeController->index(row, 0);
        //                auto node = treeController->getNode(index);
        //
        //                settings->setArrayIndex(row);
        //                settings->setValue("path", node->getGroup()->getDefinitionFilePath());
        //            }
        //            settings->endArray();
        //        }

        if (config.selectedProfile)
        {
            settings->setValue("selectedProfile", QString::fromStdString(config.selectedProfile->getName()));
        }

        settings->setValue("openAllStatesSelected", config.openAllStatesWasSelected);

    }


    void StatechartEditorController::treeviewGroupsDoubleClicked(QModelIndex index)
    {
        StateTreeNodePtr node = treeController->getNode(index);

        if (node && node->getState())
        {
            int index = editor->getStateTabWidget()->getStateTab(node->getState());
            getTipDialog()->showMessage("You can move states by holding the SHIFT + left click button. You can move the scene by holding ALT + move mouse.", "State Interaction");

            if (index < 0)
            {
                editor->getStateTabWidget()->addStateTab(node->getState());
            }
            else
            {
                editor->getStateTabWidget()->setCurrentIndex(index);
            }
        }

    }

    void StatechartEditorController::requestSave()
    {
        treeController->saveAll();
    }

    void StatechartEditorController::onStateTabChanged(int index)
    {
        StatechartView* view = editor->getUI()->stateTabWidget->stateview(index);

        if (view && view->getStateInstance() && view->getStateInstance()->getStateClass())
        {
            treeController->selectNodeByState(view->getStateInstance()->getStateClass());
            showStateCode(view->getStateInstance());
            view->showSubSubstates(editor->ui->actionShow_Subsubstates->isChecked());
        }
    }

    void StatechartEditorController::showNewStatechartGroupDialog()
    {
        QList<QString> selectedProxies;
        // enable statechart context generation for new groups by default
        EditStatechartGroupDialog d(EditStatechartGroupDialog::NewGroup, "", packageTool, variantInfo, selectedProxies, true, profiles);

        if (d.exec() == QDialog::Accepted)
        {
            StatechartGroupPtr g = treeController->addNewGroup(d.getGroupName(), d.getGroupPath(), d.getGroupDescription(), d.getPackageName(), d.getProxies(), d.contextGenerationEnabled(), d.getConfigurations());
        }
    }

    void StatechartEditorController::showStatechartEditorSettingsDialog()
    {
        StatechartEditorSettingsDialog d;
        d.setPaths(config.searchPaths);
        d.setRemoteStatesLocked(config.lockRemoteStates);
        if (d.exec() == QDialog::Accepted)
        {
            config.searchPaths = d.getPaths();
            config.searchPaths.removeDuplicates();
            searchAndOpenPaths(config.searchPaths);
            //        d.setPaths(config.searchPaths);
            //        d.setRemoteStatesLocked(config.lockRemoteStates);
            //        if(d.exec() == QDialog::Accepted)
            //        {
            //            config.searchPaths = d.getPaths();
            //            searchAndAddPaths(config.searchPaths);
            config.lockRemoteStates = d.getRemoteStatesLocked();
            stateEditorController->setLockRemoteStatesByDefault(config.lockRemoteStates);
            storeAutoSaveSettings();
        }
    }

    void StatechartEditorController::openStatechartGroups(QStringList groups)
    {
        editor->getUI()->treeViewProgressBar->show();
        editor->getUI()->treeViewProgressBar->setMaximum(groups.size());
        int i = 1;
        foreach (QString groupPath, groups)
        {
            treeController->onOpenGroup(groupPath);
            editor->getUI()->treeViewProgressBar->setValue(i);
            i++;
            qApp->processEvents(QEventLoop::ExcludeUserInputEvents);
        }
        editor->getUI()->treeViewProgressBar->hide();
    }

    void StatechartEditorController::searchAndOpenPaths(QStringList paths)
    {
        paths.removeDuplicates();
        QStringList groups;
        foreach (QString path, paths)
        {
            groups.append(findAllStatechartGroupDefinitions(path.trimmed()));
        }
        openStatechartGroups(groups);
    }


    void StatechartEditorController::connectToView(int tabIndex)
    {
        StatechartView* view = editor->getUI()->stateTabWidget->currentStateview();

        if (view)
        {
            connect(view, SIGNAL(selectedStateChanged(statechartmodel::StateInstancePtr)), this, SLOT(showStateCode(statechartmodel::StateInstancePtr)));
        }
    }

    void StatechartEditorController::showStateCode(statechartmodel::StateInstancePtr stateInstance)
    {
        if (stateInstance && stateInstance->getStateClass())
        {
            StateTreeNodePtr node = treeController->getNodeByState(stateInstance->getStateClass());

            if (node)
            {
                std::string filePath = node->getBoostCppFilePath().c_str();
                showCodeFileContent(QString::fromStdString(filePath));
            }
        }

    }

    void StatechartEditorController::showCodeFileContent(const QString& path)
    {
        watcher->removePath(path);
        std::string fileContent;

        if (std::filesystem::exists(path.toUtf8().data()))
        {
            watcher->addPath(path);
            fileContent = RapidXmlReader::ReadFileContents(path.toUtf8().data());
        }
        else
        {
            fileContent = "<cpp missing>";
        }

        int line = editor->getUI()->textEditCppCode->textCursor().blockNumber();
        QTextCursor cursor = editor->getUI()->textEditCppCode->textCursor();
        cursor.movePosition(QTextCursor::Down, QTextCursor::MoveAnchor, line);
        editor->getUI()->textEditCppCode->setText(QString::fromUtf8(fileContent.c_str()));
        editor->getUI()->textEditCppCode->setTextCursor(cursor);

    }


    void StatechartEditorController::showOnEnterFunction()
    {
        editor->getUI()->textEditCppCode->find("::CreateInstance(");
        editor->getUI()->textEditCppCode->find("::onEnter()", QTextDocument::FindBackward);
    }

    void StatechartEditorController::showRunFunction()
    {
        editor->getUI()->textEditCppCode->find("::CreateInstance(");
        editor->getUI()->textEditCppCode->find("::run()", QTextDocument::FindBackward);

    }

    void StatechartEditorController::showOnBreakFunction()
    {
        editor->getUI()->textEditCppCode->find("::CreateInstance(");
        editor->getUI()->textEditCppCode->find("::onBreak()", QTextDocument::FindBackward);

    }

    void StatechartEditorController::showOnExitFunction()
    {
        editor->getUI()->textEditCppCode->find("::CreateInstance(");
        editor->getUI()->textEditCppCode->find("::onExit()", QTextDocument::FindBackward);

    }

    void StatechartEditorController::openSelectedState()
    {
        statechartmodel::StatePtr state;

        if (editor->getUI()->stateTabWidget->currentStateview())
        {

            auto selection = editor->getUI()->stateTabWidget->currentStateview()->getScene()->selectedItems();

            for (QGraphicsItem* item : selection)
            {
                StateItem* stateItem = dynamic_cast<StateItem*>(item);

                if (stateItem)
                {
                    if (stateItem->getStateInstance())
                    {
                        state = stateItem->getStateInstance()->getStateClass();

                        if (state)
                        {
                            break;
                        }
                    }

                }
            }

        }

        if (state)
        {
            auto node = treeController->getNodeByState(state);

            if (node && node->isState())
            {
                treeController->openStateCPP(node);
            }
        }
        else
        {
            showMessageBox("No state with a StateClass is selected");
        }

    }

    void StatechartEditorController::closeAllTabs()
    {
        editor->getStateTabWidget()->clear();
    }

    void StatechartEditorController::executeOpenedState(bool)
    {
        StatechartView* view = editor->getUI()->stateTabWidget->currentStateview();
        if (!executedOpenedGroup)
        {
            if (view)
            {
                auto node = treeController->getNodeByState(view->getStateInstance()->getStateClass());
                if (node)
                {
                    if (node->isPublic())
                    {
                        treeController->executeGroupWithDependencies(node->getGroup(), view->getStateInstance()->getStateClass()->getStateName());
                        executedOpenedGroup = node->getGroup();
                        editor->getUI()->toolButtonRunState->setIcon(QIcon(":/icons/delete.ico"));
                        editor->getUI()->toolButtonRunState->setToolTip("Stop the Statechart Group");
                        editor->getUI()->toolButtonWatchStateExecution->setEnabled(false);
                        editor->getUI()->labelExecutionState->setVisible(true);
                        alreadyWatchingState = false;
                        executionStatusTask->start();
                    }
                    else
                    {
                        QMessageBox::warning(editor, "Execution not possible", "You can only execute public state. Right-click on the state in the tree view on the left and select 'Public State'.");
                    }
                }
            }
            else
            {
                QMessageBox::warning(editor, "Execution not possible", "You need to open a state before executing it with this button.");
            }
        }
        else
        {
            if (executedOpenedGroup)
            {
                treeController->stopGroupExecutionWithDependencies(executedOpenedGroup);
                executedOpenedGroup.reset();
                executionStatusTask->stop();
                editor->getUI()->labelExecutionState->setVisible(false);
            }
            editor->getUI()->toolButtonRunState->setToolTip("Start the Statechart Group");
            editor->getUI()->toolButtonRunState->setIcon(QIcon(":/icons/run.svg"));
            editor->getUI()->toolButtonWatchStateExecution->setEnabled(true);
            if (view)
            {
                view->getScene()->clearActiveSubstates();
            }
            std::function<void(StateItem* state)> unsubscriptionLamba;
            unsubscriptionLamba = [&](StateItem * state)
            {
                stateWatcher->unsubscribeState(state);
                for (auto stateInstance : state->getSubstateItems())
                {
                    if (stateInstance)
                    {
                        unsubscriptionLamba(stateInstance);
                    }
                }
            };
            unsubscriptionLamba(view->getScene()->getTopLevelStateItem());
        }

    }

    void StatechartEditorController::updateExecutionButtonStatus()
    {
        bool changeToWaiting = false;
        QString labelText = "";
        if (executedOpenedGroup)
        {
            std::string proxyName = executedOpenedGroup->getName().toStdString() + "StateComponentAppManager";
            std::string objName = config.selectedProfile->getName() + executedOpenedGroup->getName().toStdString() + "RemoteStateOfferer";
            ArmarXManagerInterfacePrx stateComponentProxy = getProxy<ArmarXManagerInterfacePrx>(proxyName, false, "", false);
            try
            {
                if (getArmarXManager()->getIceManager()->isObjectReachable(objName))
                {
                    // Dont need to do anything, the RemoteStateOfferer is already running.
                }
                else if (stateComponentProxy)
                {
                    auto state = stateComponentProxy->getObjectState(objName);
                    if (state != eManagedIceObjectStarted)
                    {
                        Ice::StringSeq deps;
                        for (auto elem : stateComponentProxy->getObjectConnectivity(objName).dependencies)
                        {
                            ManagedIceObjectDependencyBasePtr dep = elem.second;
                            if (!dep->getResolved())
                            {
                                if (deps.size() >= 2)
                                {
                                    deps.push_back("...");
                                    break;
                                }
                                else
                                {
                                    deps.push_back(dep->getName());
                                }
                            }
                        }
                        labelText = "Waiting for dependencies: " + QString::fromStdString(simox::alg::join(deps, ", "));
                        changeToWaiting = true;
                    }
                }
                else
                {
                    labelText = "Waiting for statechart group to start";
                    changeToWaiting = true;
                }
            }
            catch (const Ice::Exception& e)
            {
                labelText = "Waiting for statechart group to start (ice-exception catched)";
                changeToWaiting = true;
            }

            catch (...)
            {
                labelText = "Waiting for statechart group to start (exception catched)";
                changeToWaiting = true;
            }

            if (!changeToWaiting)
            {
                labelText = "Statechart group is running";
                if (editor->getUI()->toolButtonWatchStateExecution->isChecked() && !alreadyWatchingState)
                {
                    watchState(objName);
                }
            }
        }
        QMetaObject::invokeMethod(editor->getUI()->labelExecutionState, "setText", Qt::QueuedConnection, Q_ARG(QString, labelText));
        //        editor->getUI()->labelExecutionState->setText(labelText);

    }

    void StatechartEditorController::watchState(const std::string& objName)
    {
        RemoteStateOffererInterfacePrx statechartHandler = getProxy<RemoteStateOffererInterfacePrx>(objName, false, "", false);
        //                ARMARX_INFO << deactivateSpam(4) << "getting proxy for " << objName;
        StatechartView* view = editor->getUI()->stateTabWidget->currentStateview();
        if (view)
        {
            std::string globalStateName = "TopLevel->" + view->getStateInstance()->getStateClass()->getStateName().toStdString();
            QMap<QString, StateInstanceData> instanceData = view->getScene()->getStateInstanceData();
            auto toplevelPathString = view->getScene()->getTopLevelStateItem()->getFullStatePath();
            auto asyncResult = statechartHandler->begin_getStatechartInstanceByGlobalIdStr(globalStateName);
            while (!asyncResult->isCompleted())
            {
                if (getState() >= eManagedIceObjectExiting)
                {
                    return;
                }
                usleep(10000);
                //                    qApp->processEvents();
            }
            armarx::StateIceBasePtr stateptr = statechartHandler->end_getStatechartInstanceByGlobalIdStr(asyncResult);
            if (!stateptr)
            {
                //                        ARMARX_WARNING_S << deactivateSpam(4) << "Could not find state with name " << globalStateName;
            }
            else
            {


                std::function<void(StateIceBasePtr iceState, StateItem* state)> subscriptionLamba;
                subscriptionLamba = [&](StateIceBasePtr iceState, StateItem * state)
                {
                    stateWatcher->subscribeToState(iceState, state);
                    size_t i = 0;
                    for (auto stateInstance : state->getSubstateItems())
                    {
                        if (stateInstance->getStateInstance()->getStateClass() && iceState->subStateList.size() > i)
                        {
                            subscriptionLamba(StateIceBasePtr::dynamicCast(iceState->subStateList.at(i)), stateInstance);
                        }
                        i++;
                    }
                };
                subscriptionLamba(stateptr, view->getScene()->getTopLevelStateItem());

                alreadyWatchingState = true;
            }
        }
    }

    QPointer<QWidget> StatechartEditorController::getWidget()
    {
        if (!editor)
        {
            //            QWidget* w = qobject_cast<QWidget*>(new StatechartEditorMainWindow());
            editor = new StatechartEditorMainWindow();
        }

        return qobject_cast<QWidget*>(editor);
    }

    void StatechartEditorController::initWidget()
    {
        getWidget()->setFocusPolicy(Qt::WheelFocus);


        ARMARX_INFO << "selectedProfile: " << config.selectedProfile->getFullName();
        ARMARX_INFO << "profile packages: " << config.selectedProfile->getAllPackages();
        variantInfo = VariantInfo::ReadInfoFiles({"ArmarXCore"}, true, false); // read core variantinfo file in case the root profile is selected
        for (auto p : config.selectedProfile->getAllPackages())
        {
            variantInfo = VariantInfo::ReadInfoFilesRecursive(p, "", true, variantInfo);
        }
        editor->setCommunicator(getIceManager()->getCommunicator());
        editor->setVariantInfo(variantInfo);
        editor->setCurrentProfile(config.selectedProfile);
        editor->getUI()->toolBarViewControl->addWidget(new QLabel(QString::fromStdString("Selected Profile: " + config.selectedProfile->getFullName())));
        editor->getUI()->toolBarViewControl->insertWidget(editor->getUI()->actionEdit_State_Properties, new QLabel("Active State:"));

        QList<QVariant> header;
        header.push_back(QString("TEST"));
        treeController.reset(new StateTreeController(getIceManager()->getCommunicator(), variantInfo, header, editor->getUI()->treeViewGroups, editor->getUI()->lineEditStateSearch, packageTool, profiles, config.selectedProfile, this));


        QStringList searchPaths;
        if (config.openAllStatesWasSelected)
        {
            for (std::string package : config.selectedProfile->getAllPackages())
            {
                for (const auto& includePath : CMakePackageFinder(package).getIncludePathList())
                {
                    std::filesystem::path packageStatechartPath(includePath.c_str());
                    packageStatechartPath /= package;
                    packageStatechartPath /= "statecharts";

                    if (std::filesystem::exists(packageStatechartPath)
                        && !std::filesystem::exists(packageStatechartPath / "cmake_install.cmake")) // do not add the build dir
                    {
                        ARMARX_VERBOSE << "Adding statechart search path: " << packageStatechartPath.string();
                        searchPaths.push_back(packageStatechartPath.c_str());
                    }
                }
            }

        }
        config.searchPaths.removeDuplicates();




        connect(editor->getUI()->actionNew_State_Definition, SIGNAL(triggered()), treeController.get(), SLOT(onNewStateDefinition()));
        connect(editor->getUI()->actionDelete_State_Definition, SIGNAL(triggered()), treeController.get(), SLOT(onDeleteNode()));
        connect(editor->getUI()->treeViewGroups, SIGNAL(doubleClicked(QModelIndex)), SLOT(treeviewGroupsDoubleClicked(QModelIndex)));
        connect(editor->getUI()->actionSave_State, SIGNAL(triggered()), SLOT(requestSave()));
        connect(editor->getUI()->stateTabWidget, SIGNAL(currentChanged(int)), SLOT(onStateTabChanged(int)));
        connect(editor->getUI()->actionNew_Statechart_Group, SIGNAL(triggered()), this, SLOT(showNewStatechartGroupDialog()));
        connect(editor->getUI()->actionOpenStatechartGroup, SIGNAL(triggered()), treeController.get(), SLOT(onOpenGroup()));
        connect(editor->getUI()->actionSettings, SIGNAL(triggered()), this, SLOT(showStatechartEditorSettingsDialog()));
        connect(editor->getUI()->toolButtonRunState, SIGNAL(clicked(bool)), this, SLOT(executeOpenedState(bool)));

        connect(treeController.get(), SIGNAL(closeAllTabsRequested()), this, SLOT(closeAllTabs()));

        // setup plugin specific shortcuts
        editor->getUI()->actionOpenStatechartGroup->setShortcutContext(Qt::WidgetWithChildrenShortcut);
        editor->getUI()->actionOpenStatechartGroup->setShortcut(tr("Ctrl+O"));
        getWidget()->addAction(editor->getUI()->actionOpenStatechartGroup);


        stateEditorController.reset(new StateEditorController(editor, treeController, getIceManager()->getCommunicator(), variantInfo, config.selectedProfile, getTipDialog()));
        stateEditorController->setLockRemoteStatesByDefault(config.lockRemoteStates);
        connect(editor->getUI()->stateTabWidget, SIGNAL(currentChanged(int)), this, SLOT(connectToView(int)), Qt::UniqueConnection);
        connect(editor->getUI()->radioOnEnter, SIGNAL(clicked()), this, SLOT(showOnEnterFunction()), Qt::UniqueConnection);
        connect(editor->getUI()->radioOnBreak, SIGNAL(clicked()), this, SLOT(showOnBreakFunction()), Qt::UniqueConnection);
        connect(editor->getUI()->radioRun, SIGNAL(clicked()), this, SLOT(showRunFunction()), Qt::UniqueConnection);
        connect(editor->getUI()->radioOnExit, SIGNAL(clicked()), this, SLOT(showOnExitFunction()), Qt::UniqueConnection);


        connect(editor->getUI()->btnOpenCppCode, SIGNAL(clicked()), this, SLOT(openSelectedState()));

        watcher = new QFileSystemWatcher(editor);
        connect(watcher, SIGNAL(fileChanged(QString)), this, SLOT(showCodeFileContent(QString)));

        searchAndOpenPaths(config.searchPaths + searchPaths);
        //        OpenStatechartGroups(config.groupsToLoad);
        //        config.groupsToLoad.clear();
        treeController->collapseAll();
        executionStatusTask = new PeriodicTask<StatechartEditorController>(this, &StatechartEditorController::updateExecutionButtonStatus, 300);
    }



    bool StatechartEditorController::onClose()
    {
        //        QSettings s("KIT", "ArmarXStatechartEditor");
        //        saveSettings(&s);
        return ArmarXComponentWidgetController::onClose();
    }

    void StatechartEditorController::configured()
    {
        StatechartEditorConfigDialog* dialog = qobject_cast<StatechartEditorConfigDialog*>(getConfigDialog(getWidget()));
        config.selectedProfile = dialog->getSelectedProfile();
        config.openAllStatesWasSelected = dialog->openAllStatesIsSelected();
    }

}
QPointer<QDialog> armarx::StatechartEditorController::getConfigDialog(QWidget* parent)
{
    if (!dialog)
    {
        profiles = StatechartProfiles::ReadProfileFiles(Application::getInstance()->getDefaultPackageNames());
        dialog = new StatechartEditorConfigDialog(profiles, parent);
    }

    return qobject_cast<StatechartEditorConfigDialog*>(dialog);
}
