/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "StateRenamer.h"

#include <filesystem>
#include <IceUtil/UUID.h>
#include <QDir>
#include <QDirIterator>
#include <QFileInfo>
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/io/GroupXmlReader.h>
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/io/GroupXmlWriter.h>
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/io/XmlReader.h>
#include <filesystem>

#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/cloning/GroupCloner.h>

using namespace armarx;
using namespace armarx::statechartmodel;

bool StateRenamer::RenameState(const QVector<InstanceRenameInfo>& instanceRenameInfos,
                               const StateTreeNodePtr& state,
                               const QString& newStateName,
                               const StatechartGroupPtr& group)
{
    RenameStateInstances(instanceRenameInfos);

    if (!RenameStateClass(state, newStateName))
    {
        return false;
    }

    if (!AdjustStatechartGroupFile(group, state, newStateName))
    {
        return false;
    }

    auto cmakeListPath = QString::fromUtf8((group->getBoostDefinitionFilePath().remove_filename() / "CMakeLists.txt").c_str());
    auto fileContents = GuiStatechartGroupXmlReader::ReadFileContents(cmakeListPath);
    fileContents = fileContents.replace(state->getState()->getStateName() + ".xml", newStateName + ".xml");
    fileContents = fileContents.replace(state->getState()->getStateName() + ".cpp", newStateName + ".cpp");
    fileContents = fileContents.replace(state->getState()->getStateName() + ".h", newStateName + ".h");
    fileContents = fileContents.replace(state->getState()->getStateName() + ".generated.h", newStateName + ".generated.h");
    GroupXmlWriter::WriteFileContents(cmakeListPath, fileContents);

    return true;
}

void StateRenamer::RenameStateInstances(const QVector<InstanceRenameInfo>& instanceRename)
{
    for (const auto& entry : instanceRename)
    {
        if (entry.fromName == entry.toName)
        {
            continue;
        }

        auto xmlProcessor = [&](const std::string & nodeName, const std::string & attribName, const std::string & attribValue) -> std::string
        {
            QString value = QString::fromUtf8(attribValue.c_str());
            const bool nameRef = attribName == "name" && value == entry.fromName;
            const bool transitionRef = nodeName == "Transition" && (attribName == "from" || attribName == "to") && value == entry.fromName;
            const bool startRef = nodeName == "StartState" && attribName == "substateName" && value == entry.fromName;

            if (nameRef || transitionRef || startRef)
            {
                return entry.toName.toUtf8().data();
            }

            return attribValue;
        };

        auto statePath = entry.parentState->getBoostXmlFilePath(StateTreeNode::Path::Absolute).string();

        ARMARX_VERBOSE_S << "processing xml file: " << statePath;
        auto readerPtr = RapidXmlReader::FromFile(statePath);
        RapidXmlReaderNode rootNode = readerPtr->getRoot();
        RapidXmlWriter writer;
        auto writerNode = writer.createRootNode(rootNode.name());

        GroupCloner::ProcessXMLFile(rootNode, writerNode, xmlProcessor);

        writer.saveToFile(statePath, true);
    }
}

bool StateRenamer::RenameStateClass(const StateTreeNodePtr& state, const QString& newStateName)
{
    const std::string newStateNameStd = newStateName.toUtf8().data();
    auto oldStateName = state->getState()->getStateName();
    auto oldStatePath = state->getBoostXmlFilePath(StateTreeNode::Path::Absolute);
    auto newStatePath = state->getBoostXmlFilePath(StateTreeNode::Path::Absolute).remove_filename() / (newStateNameStd + ".xml");
    auto readerPtr = RapidXmlReader::FromFile(oldStatePath.string());
    RapidXmlReaderNode rootNode = readerPtr->getRoot();
    RapidXmlWriter writer;
    auto writerNode = writer.createRootNode(rootNode.name());

    auto xmlProcessor = [&](const std::string & nodeName, const std::string & attribName, const std::string & attribValue) -> std::string
    {
        QString value = QString::fromUtf8(attribValue.c_str());

        if (nodeName == "State" && attribName == "name" && value == state->getState()->getStateName())
        {
            ARMARX_CHECK_EXPRESSION(state->getState()->getStateName() == value)
                    << "expected state file of '"
                    << state->getState()->getStateName()
                    << "', but got '" << value << "'";
            return newStateName.toUtf8().data();
        }

        return attribValue;
    };

    GroupCloner::ProcessXMLFile(rootNode, writerNode, xmlProcessor);
    writer.saveToFile(newStatePath.string(), true);

    QVector<QPair<QRegExp, QString>> codeFileReplaceList;
    codeFileReplaceList.push_back({QRegExp{"\\b(" + oldStateName + ")\\b"}, newStateName});
    codeFileReplaceList.push_back({QRegExp{"_" + oldStateName + "_"}, "_" + newStateName + "_"});
    codeFileReplaceList.push_back({QRegExp{oldStateName + "GeneratedBase"}, newStateName + "GeneratedBase"});

    bool success = true;

    if (state->checkCppExists())
    {
        auto oldCppPath = state->getBoostCppFilePath();
        auto newCppPath = state->getBoostCppFilePath().parent_path() / (newStateNameStd + ".cpp");
        auto oldHPath = state->getBoostHFilePath();
        auto newHPath = state->getBoostHFilePath().parent_path() / (newStateNameStd + ".h");
        auto genPath = state->getBoostGeneratedHFilePath();

        GroupCloner::RegexReplaceFile(QString::fromUtf8(oldCppPath.c_str()), QString::fromUtf8(newCppPath.c_str()), codeFileReplaceList);
        GroupCloner::RegexReplaceFile(QString::fromUtf8(oldHPath.c_str()), QString::fromUtf8(newHPath.c_str()), codeFileReplaceList);
        success = !std::filesystem::exists(oldCppPath) || std::filesystem::remove(oldCppPath);
        success &= !std::filesystem::exists(oldHPath) || std::filesystem::remove(oldHPath);
        success &= !std::filesystem::exists(genPath) || std::filesystem::remove(genPath);
    }


    return success;
}

bool StateRenamer::AdjustStatechartGroupFile(const StatechartGroupPtr& group, const StateTreeNodePtr& state, const QString& newName)
{
    std::string path = group->getDefinitionFilePath().toUtf8().data();
    auto readerPtr = RapidXmlReader::FromFile(path);
    RapidXmlReaderNode rootNode = readerPtr->getRoot();
    RapidXmlWriter writer;
    auto writerNode = writer.createRootNode(rootNode.name());

    auto scgxmlProcessor = [&](const std::string & nodeName, const std::string & attribName, const std::string & attribValue) -> std::string
    {
        QString value = QString::fromUtf8(attribValue.c_str());

        if (nodeName == "State" && attribName == "filename" && value == state->getBasename())
        {
            const QString newStateFile = newName + ".xml";
            return newStateFile.toUtf8().data();
        }

        return attribValue;
    };

    GroupCloner::ProcessXMLFile(rootNode, writerNode, scgxmlProcessor);
    writer.saveToFile(path, true);
    return true;
}

bool StateRenamer::AttemptMoveTo(const QString& source, const QString& target)
{
    if (!QFile(source).rename(target))
    {
        ARMARX_ERROR_S << "Could not move '" << source << "' to '" << target << "'";
        return false;
    }

    return true;
}
