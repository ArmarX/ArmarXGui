/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "GroupRenamer.h"

#include <filesystem>
#include <IceUtil/UUID.h>
#include <QDir>
#include <QDirIterator>
#include <QFileInfo>
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/io/XmlReader.h>
#include <filesystem>

using namespace armarx;
using namespace armarx::statechartmodel;

GroupRenamer::GroupRenamer(const GroupClonerPtr& groupCloner) : groupCloner(groupCloner)
{
}

std::optional<QString> GroupRenamer::renameGroup(const StatechartGroupPtr& group, const QString& newName, const QVector<StatechartGroupPtr>& affectedGroups)
{
    if (!groupCloner->cloneGroupsTo({{group, newName}}, group->getPackagePath(), StatechartGroupMapping(), false))
    {
        ARMARX_ERROR_S << "Renaming group '" << group->getName() << "' to '" << newName << "' failed";
        return std::nullopt;
    }
    GroupCloner::RemoveDir(group->getGroupPath());
    const QString oldRSOName = group->getName() + "RemoteStateOfferer";
    const QString newRSOName = newName + "RemoteStateOfferer";

    auto xmlProcessor = [&](const std::string&, const std::string & attribName, const std::string & attribValue) -> std::string
    {
        QString value = QString::fromUtf8(attribValue.c_str());

        if (attribName == "proxyName" && value == oldRSOName)
        {
            return newRSOName.toUtf8().data();
        }

        return attribValue;
    };

    for (const StatechartGroupPtr& g : affectedGroups)
    {
        QDirIterator it(g->getGroupPath(), QDir::Files, QDirIterator::Subdirectories);

        while (it.hasNext())
        {
            const auto f = QFileInfo(it.next());

            if (f.isDir())
            {
                continue;
            }

            if (f.suffix() == "xml")
            {
                ARMARX_VERBOSE_S << "processing xml file: " << it.fileName();
                auto readerPtr = RapidXmlReader::FromFile(it.filePath().toUtf8().data());
                RapidXmlReaderNode rootNode = readerPtr->getRoot();
                RapidXmlWriter writer;
                auto writerNode = writer.createRootNode(rootNode.name());

                GroupCloner::ProcessXMLFile(rootNode, writerNode, xmlProcessor);

                writer.saveToFile(f.absoluteFilePath().toUtf8().data(), true);
            }
        }
    }

    ARMARX_VERBOSE_S << "Adjusting statecharts/CMakeLists.txt";
    QDir oldDir(group->getGroupPath());
    oldDir.cdUp();
    QString newGroupPath = oldDir.path() + QDir::separator() + newName;
    QString cmakeListsPath = oldDir.path() + QDir::separator() + "CMakeLists.txt";

    QString cmakeLists = GuiStatechartGroupXmlReader::ReadFileContents(cmakeListsPath);
    cmakeLists = cmakeLists.replace(QRegExp("add_subdirectory\\(" + group->getName() + "\\)\\n"), "");
    GroupXmlWriter::WriteFileContents(cmakeListsPath, cmakeLists);

    if (auto mappingOpt = StatechartGroupMapping::ReadStatechartGroupMappingFile(group->getPackagePath()))
    {
        ARMARX_INFO_S << "Package '" << group->getPackagePath() << "' contains a mapping file; updating...";
        StatechartGroupMapping mapping = *mappingOpt;
        auto gmIt = std::find_if(mapping.groupMappings.begin(), mapping.groupMappings.end(), [&](const StatechartGroupMapping::GroupMapping & groupMapping)
        {
            return groupMapping.newGroupName == group->getName();
        });

        if (gmIt != mapping.groupMappings.end())
        {
            StatechartGroupMapping::GroupMapping newGroupMapping {gmIt->groupName, newName, gmIt->groupPackage, gmIt->stateMappings};
            mapping.groupMappings.erase(gmIt);
            mapping.groupMappings.insert(newGroupMapping);
            mapping.writeToFile(group->getPackagePath());
        }
    }

    return newGroupPath;
}
