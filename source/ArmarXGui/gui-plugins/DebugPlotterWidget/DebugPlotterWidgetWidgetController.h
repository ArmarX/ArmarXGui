/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGui::gui-plugins::DebugPlotterWidgetWidgetController
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/libraries/ArmarXCoreComponentPlugins/DebugObserverComponentPlugin.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <ArmarXGui/gui-plugins/DebugPlotterWidget/ui_DebugPlotterWidgetWidget.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXGui/libraries/SimpleConfigDialog/SimpleConfigDialog.h>


namespace armarx
{
    /**
    \page ArmarXGui-GuiPlugins-DebugPlotterWidget DebugPlotterWidget
    \brief The DebugPlotterWidget allows visualizing ...

    \image html DebugPlotterWidget.png
    The user can

    API Documentation \ref DebugPlotterWidgetWidgetController

    \see DebugPlotterWidgetGuiPlugin
    */


    /**
     * \class DebugPlotterWidgetGuiPlugin
     * \ingroup ArmarXGuiPlugins
     * \brief DebugPlotterWidgetGuiPlugin brief description
     *
     * Detailed description
     */

    /**
     * \class DebugPlotterWidgetWidgetController
     * \brief DebugPlotterWidgetWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        DebugPlotterWidgetWidgetController:
        public armarx::ArmarXComponentWidgetControllerTemplate < DebugPlotterWidgetWidgetController >,
        public virtual DebugObserverComponentPluginUser
    {
        Q_OBJECT

    public:
        /// Controller Constructor
        explicit DebugPlotterWidgetWidgetController();

        void loadSettings(QSettings* settings) override;
        void saveSettings(QSettings* settings) override;
        QPointer<QDialog> getConfigDialog(QWidget* parent) override;
        void configured() override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName()
        {
            return "Debugging.DebugPlotterWidget";
        }

        void onInitComponent()       override {}
        void onConnectComponent()    override {}
        void onDisconnectComponent() override {}
        void onExitComponent()       override {}

    protected:
        void timerEvent(QTimerEvent* event) override;

    private slots:
        void send();

    private:
        using clock_t = std::chrono::steady_clock;
        Ui::DebugPlotterWidgetWidget _widget;
        QPointer<SimpleConfigDialog> _dialog;
        typename clock_t::time_point _last_send;
    };
}


