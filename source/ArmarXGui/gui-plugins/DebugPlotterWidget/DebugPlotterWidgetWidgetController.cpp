/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    ArmarXGui::gui-plugins::DebugPlotterWidgetWidgetController
 * \author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * \date       2021
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DebugPlotterWidgetWidgetController.h"

#include <string>
#include <cstdint>

//boilerplate config
namespace armarx
{
    void DebugPlotterWidgetWidgetController::loadSettings(QSettings* settings)
    {
        ARMARX_TRACE;
        getDebugObserverComponentPlugin().setDebugObserverTopic(settings->value("dbgo", "DebugObserver").toString().toStdString());
    }

    void DebugPlotterWidgetWidgetController::saveSettings(QSettings* settings)
    {
        ARMARX_TRACE;
        settings->setValue("dbgo", QString::fromStdString(getDebugObserverComponentPlugin().getDebugObserverTopic()));
    }

    QPointer<QDialog> DebugPlotterWidgetWidgetController::getConfigDialog(QWidget* parent)
    {
        ARMARX_TRACE;
        if (!_dialog)
        {
            _dialog = new SimpleConfigDialog(parent);
            _dialog->addLineEdit("dbgo", "DebugObserver Topic", "DebugObserver");
        }
        return qobject_cast<SimpleConfigDialog*>(_dialog);
    }

    void DebugPlotterWidgetWidgetController::configured()
    {
        ARMARX_TRACE;
        getDebugObserverComponentPlugin().setDebugObserverTopic(_dialog->get("dbgo"));
    }
}

//boilerplate ctor/cycle

namespace armarx
{
    DebugPlotterWidgetWidgetController::DebugPlotterWidgetWidgetController()
    {
        _widget.setupUi(getWidget());
        connect(_widget.pushButtonSend, &QPushButton::clicked,
                this, &DebugPlotterWidgetWidgetController::send);
        startTimer(3);
    }
}

namespace armarx
{
    void DebugPlotterWidgetWidgetController::send()
    {
        _last_send = clock_t::now();

        DebugObserverHelper oh{this, getDebugObserver()};
        oh.setDebugObserverBatchModeEnabled(true);
        getDebugObserverComponentPlugin().setDebugObserverBatchModeEnabled(true);

        auto obs = getDebugObserver()->ice_batchOneway();
        obs->setDebugDatafield("DebugPlotter_obs", "field", new Variant(1));
        obs->setDebugChannel("DebugPlotter_obs_c2",
        {
            {"field_1", new Variant(1)},
            {"field_2", new Variant(1)}
        });
        obs->setDebugDatafield("DebugPlotter_obs", "field_2", new Variant(1));
        obs->removeDebugDatafield("DebugPlotter_obs", "field_2");

        const double val =
            _widget.checkBoxSinValue->isChecked() ?
            std::sin(std::chrono::steady_clock::now().time_since_epoch().count() / 1e9) :
            _widget.doubleSpinBoxValue->value();

        const auto num_instances = _widget.spinBoxNumInstances->value();

        const auto s = [&](const std::string & name, auto value)
        {
            for (int i = 0; i < num_instances; ++i)
            {
                const auto n = name + "_" + std::to_string(i);
                setDebugObserverDatafield("DebugPlotter", n,  value);
                oh.setDebugObserverDatafield("DebugPlotter_via_helper", n,  value);
            }
        };

        const bool b = static_cast<std::uint64_t>(val) % 2;
        s("float",  static_cast<float>(val));
        s("double", static_cast<double>(val));

        s("uint8",  static_cast<unsigned char>(val));
        s("uint16", static_cast<std::uint16_t>(val));
        s("uint32", static_cast<std::uint32_t>(val));
        s("uint64", static_cast<std::uint64_t>(val));

        s("int8",   static_cast<char        >(val));
        s("int16",  static_cast<std::int16_t>(val));
        s("int32",  static_cast<std::int32_t>(val));
        s("int64",  static_cast<std::int64_t>(val));

        s("string", std::to_string(val));

        s("bool", b);

        getDebugObserverComponentPlugin().sendDebugObserverBatch();
        oh.sendDebugObserverBatch();
        obs->ice_flushBatchRequests();
    }

    void DebugPlotterWidgetWidgetController::timerEvent(QTimerEvent* event)
    {
        _widget.doubleSpinBoxValue->setSingleStep(_widget.doubleSpinBoxValueStep->value());
        if (!_widget.checkBoxAutoSend->isChecked())
        {
            return;
        }
        const auto dtMs = std::chrono::duration_cast<std::chrono::milliseconds>(
                              clock_t::now() - _last_send
                          ).count();
        if (dtMs >= _widget.spinBoxAutoSendDelay->value())
        {
            send();
        }
    }
}
