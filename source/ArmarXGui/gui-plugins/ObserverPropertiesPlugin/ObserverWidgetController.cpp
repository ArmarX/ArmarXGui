/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Gui::ObserverWidgetController
* @author     ( at kit dot edu)
* @date
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "ObserverWidgetController.h"
#include <ArmarXCore/observers/variant/DataFieldIdentifier.h>
#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <QToolBar>
#include <ArmarXGui/applications/ArmarXGui/ArmarXMainWindow.h>

namespace armarx
{
    ObserverWidgetController::ObserverWidgetController()
    {

    }

    void ObserverWidgetController::onInitComponent()
    {
        getWidget()->show();
        ArmarXMainWindow* guiMainWindow = qobject_cast<ArmarXMainWindow*>(getMainWindow());
        variantInfo = VariantInfo::ReadInfoFiles(guiMainWindow->getDefaultPackageNames(), true, false);
    }

    void ObserverWidgetController::onConnectComponent()
    {



        connect(this, SIGNAL(connected()), __widget.data(), SLOT(createNewModel()));
        connect(__widget.data(), SIGNAL(createFilterClicked(std::string, DatafieldFilterBasePtr)), this, SLOT(installFilter(std::string, DatafieldFilterBasePtr)));
        // init widget
        emit connected();

        ARMARX_INFO << "Starting ObserverGuiPlugin";
    }

    void ObserverWidgetController::onExitComponent()
    {
        if (__widget)
        {
            __widget->clearDetailedView();
        }
    }

    QPointer<QWidget> ObserverWidgetController::getWidget()
    {
        if (!__widget)
        {
            __widget = new ObserverWidget(this);
        }

        return qobject_cast<QWidget*>(__widget);
    }

    void ObserverWidgetController::installFilter(const std::string& datafieldStr, DatafieldFilterBasePtr filter)
    {
        DataFieldIdentifierPtr  id = new DataFieldIdentifier(datafieldStr);
        ObserverInterfacePrx observer;
        observer = getProxy<ObserverInterfacePrx>(id->observerName);
        ARMARX_CHECK_EXPRESSION(observer);
        DatafieldRefPtr ref = new DatafieldRef(observer, id->getChannelName(), id->getDataFieldName());
        ARMARX_CHECK_EXPRESSION(ref);
        observer->createFilteredDatafield(filter, ref);
        __widget->updateObservers();
    }
    QPointer<QWidget> ObserverWidgetController::getCustomTitlebarWidget(QWidget* parent)
    {
        if (customToolbar)
        {
            if (parent != customToolbar->parent())
            {
                customToolbar->setParent(parent);
            }

            return qobject_cast<QToolBar*>(customToolbar);
        }

        customToolbar = new QToolBar(parent);
        customToolbar->setIconSize(QSize(16, 16));
        QAction* refreshAction = customToolbar->addAction(QIcon(":/icons/view-refresh-7.png"), "Refresh observer data");
        connect(refreshAction, SIGNAL(triggered()), __widget.data(), SLOT(updateObservers()));

        return qobject_cast<QToolBar*>(customToolbar);
    }

    VariantInfoPtr ObserverWidgetController::getVariantInfo() const
    {
        return variantInfo;
    }

}
