/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Gui::ObserverWidgetController
* @author     ( at kit dot edu)
* @date
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include "widgets/ObserverWidget.h"

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/interface/observers/ConditionHandlerInterface.h>
#include <ArmarXCore/interface/observers/Event.h>
#include <ArmarXCore/observers/variant/VariantInfo.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

namespace armarx
{
    /**
     * \page ArmarXGui-GuiPlugins-ObserverWidget ObserverGui
     * \brief This widget allows to inspect all observers in the system and their content.
     * \image html ObserverView.png "Available checks with their supported types"
     * \image html ObserverView-Datafield.png "Live plotting of any datafield"
     *
     * For each \ref Observers "observer" it displays the available checks and channels.
     * For each \ref ConditionChecks "check type" the supported parameters are shown.
     * Channels show their offered datafields with their type, current value and an live plot.
     * It is also possible to install filters for each datafield by right-clicking on the datafield and selecting
     * the desired filter.
     * \see \ref Observers
     * \see \ref ObserverWidgetController
     */

    /**
     * \class ObserverWidgetController
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT ObserverWidgetController :
        public ArmarXComponentWidgetControllerTemplate<ObserverWidgetController>
    {
        Q_OBJECT
    public:
        ObserverWidgetController();

        // inherited from Component
        void onInitComponent() override;
        void onConnectComponent() override;
        void onExitComponent() override;

        // inherited from ArmarXWidget
        QPointer<QWidget> getWidget() override;
        static QString GetWidgetName()
        {
            return "Observers.ObserverView";
        }
        static QIcon GetWidgetIcon()
        {
            return QIcon("://icons/binoculars.svg");
        }
        static QIcon GetWidgetCategoryIcon()
        {
            return QIcon("://icons/binoculars.svg");
        }

        void loadSettings(QSettings* settings) override {}
        void saveSettings(QSettings* settings) override {}


        //        // helper methods for gui
        //        ObserverList getObservers();

        //        ChannelRegistry getChannelRegistry(std::string observerName);
        //        StringConditionCheckMap getAvailableChecks(std::string observerName);
        ////        ConditionCheckMap getActiveConditionChecks(std::string observerName);

        //        ConditionHandlerInterfacePrx handler;
        ObserverInterfacePrx observer;
        VariantInfoPtr getVariantInfo() const;

    public slots:
        void installFilter(const std::string& datafieldStr, DatafieldFilterBasePtr filter);
    signals:
        void connected();
    protected:
        QPointer<QWidget> getCustomTitlebarWidget(QWidget* parent) override;
    private:
        QPointer<ObserverWidget> __widget;
        QPointer<QToolBar> customToolbar;
        VariantInfoPtr variantInfo;
    };
}

