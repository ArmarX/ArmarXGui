/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Gui
* @author     Kai Welke (welke@kit.edu)
* @copyright  2012 Humanoids Group, IAIM, IFA
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include "widgets/properties/PropertiesWidget.h"


#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/interface/observers/ConditionCheckBase.h>

#include <ArmarXCore/core/IceManager.h>

#include <QStandardItemModel>
#include <string>

#include <ArmarXCore/observers/variant/VariantInfo.h>


#define OBSERVER_ITEM_TYPE Qt::UserRole+1
#define OBSERVER_ITEM_ID Qt::UserRole+2
#define OBSERVER_ITEM_DELETE Qt::UserRole+3

namespace armarx
{
    using ObserverList = std::vector<std::string>;

    //    class IceManager;
    //    using IceManagerPtr = IceUtil::Handle<IceManager>;

    enum EItemType
    {
        eObserverItem = 0,
        eChannelsItem = 1,
        eChannelItem = 2,
        eDataFieldItem = 3,
        eChecksItem = 4,
        eCheckItem = 5,
        eConditionsItem = 6,
        eConditionItem = 7,
        eElementaryConditionItem = 8
    };


    class ObserverItemModel : public QStandardItemModel
    {
    public:
        ObserverItemModel(IceManagerPtr iceManager, VariantInfoPtr info = NULL);
        ~ObserverItemModel() override {}

        //      void updateModel(const std::string& observerName, const ChannelRegistry& channels, const StringElementaryConditionCheckMap& checks, const ConditionCheckMap& conditions);
        void updateModel(const std::string& observerName, const ChannelRegistry& channels, const StringConditionCheckMap& checks);
        QWidget* getPropertiesWidget(const QModelIndex& index, QWidget* parent);
        ObserverList getObservers();
    public slots:

        void updateObservers();
    private:
        QStandardItem* updateObserver(std::string observerName);
        void updateChannels(QStandardItem* channelsItem, const ChannelRegistry& channel);
        void updateChecks(QStandardItem* checksItem, const StringConditionCheckMap& checks);
        //      void updateConditions(QStandardItem* conditionsItem, const ConditionCheckMap& conditions);

        void markAllForDelete(QStandardItem* observerItem);
        void deleteUnusedItems(QStandardItem* observerItem);

        QStandardItem* updateOrInsertItem(QStandardItem* parent, QStandardItem* insert);

        QStandardItem* getChildByName(QStandardItem* item, const QString& name);

        std::map<std::string, ChannelRegistry> channelRegistry;
        std::map<std::string, StringConditionCheckMap> availableChecks;
        //      std::map<std::string,ConditionCheckMap> activeConditions;
        IceManagerPtr iceManager;
        VariantInfoPtr info;
    };
}

