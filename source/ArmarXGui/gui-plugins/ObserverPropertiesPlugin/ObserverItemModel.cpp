/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Gui
* @author     Kai Welke (welke@kit.edu)
* @copyright  2012 Humanoids Group, IAIM, IFA
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/



#include "ObserverItemModel.h"

#include "widgets/properties/DataFieldPropertiesWidget.h"
#include "widgets/properties/ChannelPropertiesWidget.h"
#include "widgets/properties/ElementaryCheckPropertiesWidget.h"

#include  <ArmarXCore/interface/observers/ConditionHandlerInterface.h>
#include <ArmarXCore/core/ArmarXManager.h>
#include <ArmarXCore/core/IceGridAdmin.h>
#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/util/algorithm.h>

namespace armarx
{
    ObserverItemModel::ObserverItemModel(IceManagerPtr iceManager, VariantInfoPtr info)
        : QStandardItemModel(0, 1),
          iceManager(iceManager),
          info(info)

    {

        updateObservers();
    }

    void ObserverItemModel::updateModel(const std::string& observerName, const ChannelRegistry& channels, const StringConditionCheckMap& checks)
    {
        // update data
        channelRegistry[observerName] = channels;
        availableChecks[observerName] = checks;
        //  activeConditions[observerName] = conditions;

        // mark all elements for deletion
        markAllForDelete(invisibleRootItem());

        // insert observer
        QStandardItem* observerItem = updateObserver(observerName);

        // update channels
        updateChannels(getChildByName(observerItem, "channels"), channels);

        // update checks
        updateChecks(getChildByName(observerItem, "checks"), checks);

        // update conditions
        //  updateConditions(observerItem->child(2), conditions);

        // delete not updated items
        deleteUnusedItems(observerItem);
    }

    QWidget* ObserverItemModel::getPropertiesWidget(const QModelIndex& index, QWidget* parent)
    {
        QStandardItem* item = itemFromIndex(index);
        if (!item)
        {
            ARMARX_WARNING << "Could not find widget at column " << index.column() << " and row " << index.row();
            return NULL;
        }
        QVariant id = item->data(OBSERVER_ITEM_ID);

        QWidget* widget;

        switch (item->data(OBSERVER_ITEM_TYPE).toInt())
        {
            // properties for channel
            case eChannelItem:
            {
                // retrieve registry entry
                QString channelName = id.toString();
                QString observerName = item->parent()->parent()->data(OBSERVER_ITEM_ID).toString();
                ChannelRegistryEntry& entry = channelRegistry[observerName.toLatin1().data()][channelName.toLatin1().data()];

                // create widget
                widget = new ChannelPropertiesWidget(channelName.toLatin1().data(), observerName.toLatin1().data(), entry.description, parent);
                break;
            }

            // properties for datafields
            case eDataFieldItem:
            {
                // retrieve registry entry
                const DataFieldIdentifier dataFieldIdentifier(id.toString().toLatin1().data());

                ARMARX_CHECK_EXPRESSION(
                    channelRegistry.count(dataFieldIdentifier.getObserverName())) <<
                            "No channel registry for name '" << dataFieldIdentifier.getObserverName() << "' "
                            << "Available names:\n" << getMapKeys(channelRegistry);
                ChannelRegistry& chanReg = channelRegistry.at(dataFieldIdentifier.getObserverName());

                ARMARX_CHECK_EXPRESSION(
                    chanReg.count(dataFieldIdentifier.getChannelName())) <<
                            "Channel registry '" << dataFieldIdentifier.getObserverName()
                            << "' has no channel '" << dataFieldIdentifier.getChannelName() << "' "
                            << "Available channels:\n" << getMapKeys(chanReg);
                ChannelRegistryEntry& chanRegEntry = chanReg.at(dataFieldIdentifier.getChannelName());

                ARMARX_CHECK_EXPRESSION(chanRegEntry.dataFields.count(dataFieldIdentifier.getDataFieldName()))
                        << "Channel '" << dataFieldIdentifier.getObserverName() << "." << dataFieldIdentifier.getChannelName()
                        << "' has no datafield '" << dataFieldIdentifier.getDataFieldName()
                        << "'. Available datafields:\n" << getMapKeys(chanRegEntry.dataFields);
                DataFieldRegistryEntry& entry = chanRegEntry.dataFields.at(dataFieldIdentifier.getDataFieldName());

                // create widget
                DatafieldRefPtr ref = DatafieldRefPtr::dynamicCast(entry.identifier);
                if (info && (!entry.value->data || entry.value->data->ice_id() == "::armarx::VariantData"))
                {
                    ARMARX_INFO << "Variant Factory for " << entry.typeName << " is missing - trying to load lib now";
                    auto lib = info->loadLibraryOfVariant(entry.typeName);
                    if (!lib)
                    {
                        widget = new QWidget(parent);
                        break;
                    }
                    ArmarXManager::RegisterKnownObjectFactoriesWithIce(iceManager->getCommunicator());
                    entry.value = ref->getDataField();
                }
                widget = new DataFieldPropertiesWidget(entry.description, ref, VariantPtr::dynamicCast(entry.value), iceManager, parent);
                break;
            }

            case eCheckItem:
            {
                // retrieve check
                QString checkName = id.toString();
                QString observerName = item->parent()->parent()->data(OBSERVER_ITEM_ID).toString();
                ConditionCheckBasePtr check = availableChecks[observerName.toLatin1().data()][checkName.toLatin1().data()];

                // create widget
                widget = new ElementaryCheckPropertiesWidget(check, checkName.toLatin1().data(), true, false, false, parent);

                break;
            }

            /*      case eConditionItem:
                    {
                        // retrieve check
                        int conditionIndex = id.toInt();
                        QString observerName = item->parent()->parent()->data(OBSERVER_ITEM_ID).toString();
                        ConditionCheckBasePtr condition = activeConditions[observerName.toLatin1().data()][conditionIndex];

                        // create widget
                        widget = new ConditionPropertiesWidget(condition, parent);

                        break;
                    }

                    case eElementaryConditionItem:
                    {
                        // retrieve check
                        int elementId = id.toInt();
                        int conditionIndex = item->parent()->data(OBSERVER_ITEM_ID).toInt();
                        QString observerName = item->parent()->parent()->parent()->data(OBSERVER_ITEM_ID).toString();
                        ConditionCheckBasePtr check = activeConditions[observerName.toLatin1().data()][conditionIndex];
                        ElementaryConditionCheckBasePtr element = check->elementaryChecks[elementId];

                        // create widget
                        widget = new ElementaryCheckPropertiesWidget(element, element->condition->checkName, false, true, true, parent);

                        break;
                    }*/

            default:
                widget = new QWidget(parent);
                break;
        }

        return widget;
    }

    ObserverList ObserverItemModel::getObservers()
    {
        ObserverList list;

        if (!iceManager)
        {
            return list;
        }

        try
        {
            ConditionHandlerInterfacePrx handler = iceManager->getProxy<ConditionHandlerInterfacePrx>("ConditionHandler");

            if (handler)
            {
                ObserverList registeredObs = handler->getObserverNames();
                list = registeredObs;
            }
        }
        catch (...)
        {

        }


        ObserverList globalList = iceManager->getIceGridSession()->getRegisteredObjectNames<ObserverInterfacePrx>("*Observer");
        list.insert(list.end(), globalList.begin(), globalList.end());
        std::sort(list.begin(), list.end());
        ObserverList::iterator it = std::unique(list.begin(), list.end());
        list.resize(std::distance(list.begin(), it));
        return list;
    }

    void ObserverItemModel::updateObservers()
    {
        if (!iceManager)
        {
            ARMARX_WARNING_S << "No IceManager set";
            return;
        }

        ObserverList observerList = getObservers();
        ObserverList::iterator iter = observerList.begin();

        while (iter != observerList.end())
        {
            ObserverInterfacePrx proxy;
            try
            {
                proxy = iceManager->getProxy<ObserverInterfacePrx>(*iter);
                updateModel(*iter, proxy->getAvailableChannels(true), proxy->getAvailableChecks()); //, component->getActiveConditionChecks(*iter));
            }
            catch (...)
            {
                if (proxy)
                {
                    iceManager->removeProxyFromCache(proxy);
                }
            }

            iter++;
        }
    }

    QStandardItem* ObserverItemModel::updateObserver(std::string observerName)
    {
        // create observer item
        QStandardItem* observerItem = new QStandardItem(QString(observerName.c_str()));
        observerItem->setData(QVariant(eObserverItem), OBSERVER_ITEM_TYPE);
        observerItem->setData(QVariant(observerName.c_str()), OBSERVER_ITEM_ID);
        observerItem = updateOrInsertItem(invisibleRootItem(), observerItem);

        // add channels
        QStandardItem* channelsItem = new QStandardItem(QString("channels"));
        channelsItem->setData(QVariant(eChannelsItem), OBSERVER_ITEM_TYPE);
        channelsItem->setData(QVariant("channels"), OBSERVER_ITEM_ID);
        updateOrInsertItem(observerItem, channelsItem);

        // add checks
        QStandardItem* checksItem = new QStandardItem(QString("checks"));
        checksItem->setData(QVariant(eChecksItem), OBSERVER_ITEM_TYPE);
        checksItem->setData(QVariant("checks"), OBSERVER_ITEM_ID);
        updateOrInsertItem(observerItem, checksItem);

        // add conditions
        /*  QStandardItem *conditionsItem = new QStandardItem( QString("conditions") );
            conditionsItem->setData(QVariant(eConditionsItem), OBSERVER_ITEM_TYPE);
            conditionsItem->setData(QVariant("conditions"), OBSERVER_ITEM_ID);
            updateOrInsertItem(observerItem, conditionsItem);*/

        return observerItem;
    }

    void ObserverItemModel::updateChannels(QStandardItem* channelsItem, const ChannelRegistry& channels)
    {
        if (!channelsItem)
        {
            return;
        }

        // loop through channelsobser
        ChannelRegistry::const_iterator iterChannels = channels.begin();

        while (iterChannels != channels.end())
        {
            // create item for channels
            QStandardItem* channelItem = new QStandardItem(QString(iterChannels->second.name.c_str()));

            channelItem->setData(QVariant(eChannelItem), OBSERVER_ITEM_TYPE);
            channelItem->setData(QVariant(iterChannels->second.name.c_str()), OBSERVER_ITEM_ID);

            // insert channel's item
            channelItem = updateOrInsertItem(channelsItem, channelItem);

            // loop through datafields
            DataFieldRegistry::const_iterator iterDataFields = iterChannels->second.dataFields.begin();

            while (iterDataFields != iterChannels->second.dataFields.end())
            {
                // create new  datafield item
                std::string datafieldName = iterDataFields->second.identifier->channelRef->observerName + "." + iterDataFields->second.identifier->channelRef->channelName + "." + iterDataFields->second.identifier->datafieldName;
                QStandardItem* dataFieldItem = new QStandardItem(QString(iterDataFields->second.identifier->datafieldName.c_str()));
                dataFieldItem->setData(QVariant(eDataFieldItem), OBSERVER_ITEM_TYPE);
                dataFieldItem->setData(QVariant(datafieldName.c_str()), OBSERVER_ITEM_ID);

                // insert item
                updateOrInsertItem(channelItem, dataFieldItem);
                iterDataFields++;
            }

            iterChannels++;
        }
    }

    void ObserverItemModel::updateChecks(QStandardItem* checksItem, const StringConditionCheckMap& checks)
    {
        if (!checksItem)
        {
            return;
        }

        StringConditionCheckMap::const_iterator iterChecks = checks.begin();

        while (iterChecks != checks.end())
        {
            QStandardItem* checkItem = new QStandardItem(QString(iterChecks->first.c_str()));
            checkItem->setData(QVariant(eCheckItem), OBSERVER_ITEM_TYPE);
            checkItem->setData(QVariant(iterChecks->first.c_str()), OBSERVER_ITEM_ID);

            updateOrInsertItem(checksItem, checkItem);

            iterChecks++;
        }
    }
    /*
    void ObserverItemModel::updateConditions(QStandardItem* conditionsItem, const ConditionCheckMap& conditions)
    {
        ConditionCheckMap::const_iterator iterConditions = conditions.begin();
        while(iterConditions != conditions.end())
        {
            // update condition
            QString name = QString("%1").arg(iterConditions->second->info.identifier.uniqueId);

            QStandardItem* conditionItem = new QStandardItem( name );
            conditionItem->setData(QVariant(eConditionItem), OBSERVER_ITEM_TYPE);
            conditionItem->setData(QVariant(name), OBSERVER_ITEM_ID);

            conditionItem = updateOrInsertItem(conditionsItem, conditionItem);

            // loop through elementary conditions
            IntElementaryConditionCheckMap::iterator iterElements = iterConditions->second->elementaryChecks.begin();

            while (iterElements != iterConditions->second->elementaryChecks.end())
            {
                // update elementary condition
                QStandardItem* elementItem = new QStandardItem(iterElements->second->condition->checkName.c_str());
                elementItem->setData(QVariant(eElementaryConditionItem), OBSERVER_ITEM_TYPE);
                elementItem->setData(QVariant(iterElements->first), OBSERVER_ITEM_ID);

                updateOrInsertItem(conditionItem, elementItem);

                iterElements++;
            }

            iterConditions++;
        }
    }
    */
    QStandardItem* ObserverItemModel::updateOrInsertItem(QStandardItem* parent, QStandardItem* insert)
    {
        // loop through childs and compare data
        for (int r = 0 ; r < parent->rowCount() ; r++)
        {
            QStandardItem* item = parent->child(r);

            if (item->data(OBSERVER_ITEM_ID) == insert->data(OBSERVER_ITEM_ID))
            {
                delete insert;
                item->setData(QVariant(false), OBSERVER_ITEM_DELETE);
                return item;
            }
        }

        parent->appendRow(insert);

        insert->setData(QVariant(false), OBSERVER_ITEM_DELETE);
        return insert;
    }

    QStandardItem* ObserverItemModel::getChildByName(QStandardItem* item, const QString& name)
    {
        for (int r = 0 ; r < item->rowCount() ; r++)
        {
            if (item->child(r)->data(OBSERVER_ITEM_ID).toString() == name)
            {
                return item->child(r);
            }
        }

        return 0;

    }

    // mark all elements for deletion
    void ObserverItemModel::markAllForDelete(QStandardItem* observerItem)
    {
        int nCount = 0;
        std::list<QStandardItem*> markList;
        markList.push_back(observerItem);

        while (markList.size() > 0)
        {
            QStandardItem* current = markList.front();
            markList.pop_front();
            current->setData(QVariant(true), OBSERVER_ITEM_DELETE);
            nCount++;

            if (current->hasChildren())
            {
                for (int r = 0 ; r < current->rowCount() ; r++)
                {
                    markList.push_back(current->child(r));
                }
            }
        }
    }

    void ObserverItemModel::deleteUnusedItems(QStandardItem* observerItem)
    {
        std::list<QStandardItem*> searchList;
        std::vector<int> removeRows;

        searchList.push_back(observerItem);

        while (searchList.size() > 0)
        {
            QStandardItem* current = searchList.back();
            searchList.pop_back();

            if (!current)
            {
                continue;
            }

            //        ARMARX_INFO_S << "CurrentItem: " << current->data(OBSERVER_ITEM_ID).toString().toStdString() << " num: " << current->rowCount();
            if (current->hasChildren())
            {
                removeRows.clear();

                for (int r = 0 ; r < current->rowCount() ; r++)
                {
                    if (current->child(r)->data(OBSERVER_ITEM_DELETE).toBool())
                    {
                        //                    ARMARX_INFO_S << "Removing row: " << current->child(r)->data(OBSERVER_ITEM_ID).toString().toStdString();
                        removeRows.push_back(r);
                    }
                    else
                    {
                        //                    ARMARX_INFO_S << "Insert row: " << current->child(r)->data(OBSERVER_ITEM_ID).toString().toStdString();
                        searchList.push_back(current->child(r));
                    }
                }

                // remove unused rows
                std::vector<int>::reverse_iterator iter = removeRows.rbegin();

                while (iter != removeRows.rend())
                {
                    current->removeRow(*iter);
                    iter++;
                }
            }
        }
    }
}
