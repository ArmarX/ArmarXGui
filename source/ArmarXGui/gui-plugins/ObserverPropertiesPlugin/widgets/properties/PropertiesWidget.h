/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Gui
* @author     Kai Welke (welke@kit.edu)
* @copyright  2012 Humanoids Group, IAIM, IFA
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// Qt
#include <QWidget>
#include <QFormLayout>
#include <QLabel>
#include <QBoxLayout>
#include <QGroupBox>
#include <QVariant>
#include <ArmarXCore/core/logging/Logging.h>


namespace armarx
{
    class PropertiesWidget : public QWidget
    {
        Q_OBJECT
    public:
        PropertiesWidget(QWidget* parent = 0, Qt::WindowFlags f = 0) :
            QWidget(parent, f)
        {
            createUi();
            setPropertiesTitle("title not set");
        }

        ~PropertiesWidget() override {}

        virtual void createUi()
        {
            // setup widgets
            content = new QGroupBox(this);
            content->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::MinimumExpanding));
            contentLayout = new QGridLayout();
            content->setLayout(contentLayout);

            // setup main layout
            QBoxLayout* boxLayout = new QBoxLayout(QBoxLayout::TopToBottom, this);
            boxLayout->addWidget(content);

            setLayout(boxLayout);
        }
    public slots:
        void setPropertiesTitle(const QString& title)
        {
            QString fullTitle;
            fullTitle = "Properties for " + title;
            content->setTitle(fullTitle);
        }

        void addProperty(const QString& label, QWidget* propertyWidget)
        {
            QString fullLabel = "<b>" + label + ":   </b>";
            auto rowCount = contentLayout->rowCount();
            contentLayout->addWidget(new QLabel(fullLabel), rowCount, 0);
            contentLayout->addWidget(propertyWidget, rowCount, 1);
            contentLayout->addItem(new QSpacerItem(50, 10, QSizePolicy::Expanding), rowCount, 2);
        }

        void addProperty(const QString& label, const QString& text)
        {
            auto rowCount = contentLayout->rowCount();
            setProperty(rowCount, label, text);
        }

        void setProperty(int row, const QString& label, const QString& text)
        {
            QString fullLabel = "<b>" + label + ":   </b>";
            QLabel* textLabel = new QLabel(this);
            textLabel->setText(text);
            textLabel->setTextInteractionFlags(Qt::TextSelectableByMouse);

            QLabel* labelLabel = new QLabel(fullLabel);
            labelLabel->setProperty(rawLabelPropertyString.c_str(), label);
            contentLayout->addWidget(labelLabel, row, 0);
            contentLayout->addWidget(textLabel, row, 1);
            contentLayout->addItem(new QSpacerItem(50, 10, QSizePolicy::Expanding), row, 2);
        }

        void setProperty(const QString& label, const QString& text)
        {
            int row;
            for (row = 0; row < contentLayout->rowCount(); ++row)
            {
                if (!contentLayout->itemAtPosition(row, 0))
                {
                    continue;
                }
                QLabel* labelWidget = qobject_cast<QLabel*>(contentLayout->itemAtPosition(row, 0)->widget());
                if (labelWidget && labelWidget->property(rawLabelPropertyString.c_str()).toString() == label)
                {
                    QLabel* textWidget = qobject_cast<QLabel*>(contentLayout->itemAtPosition(row, 1)->widget());
                    if (textWidget)
                    {
                        textWidget->setText(text);
                    }
                    return;
                }
            }
            setProperty(row, label, text);
        }


        void addHeading(const QString& heading)
        {
            QString fullHeading = "<b>" + heading + ":   </b>";
            QLabel* textLabel = new QLabel(this);
            textLabel->setText(fullHeading);
            textLabel->setTextInteractionFlags(Qt::TextSelectableByMouse);

            auto rowCount = contentLayout->rowCount();
            contentLayout->addWidget(textLabel, rowCount, 0, 1, 3);
        }

        void addWidget(QWidget* widget)
        {
            auto rowCount = contentLayout->rowCount();
            contentLayout->addWidget(widget, rowCount, 0, 1, 3);
        }
    public:
        const std::string rawLabelPropertyString = "RawLabel";
        QGridLayout* getContentLayout() const
        {
            return contentLayout;
        }

    private:
        QGroupBox* content;
        QGridLayout* contentLayout;
    };



}

