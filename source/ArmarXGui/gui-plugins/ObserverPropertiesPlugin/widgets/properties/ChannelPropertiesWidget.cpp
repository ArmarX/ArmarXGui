/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Gui
* @author     Kai Welke (welke@kit.edu)
* @copyright  2012 Humanoids Group, IAIM, IFA
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "ChannelPropertiesWidget.h"
#include <QLabel>

namespace armarx
{
    ChannelPropertiesWidget::ChannelPropertiesWidget(std::string channelName, std::string observerName, std::string description, QWidget* parent, Qt::WindowFlags f) :
        PropertiesWidget(parent, f)
    {
        this->observerName = observerName;
        this->channelName = channelName;
        this->description = description;

        createUi();
    }

    void ChannelPropertiesWidget::createUi()
    {
        // create property entries
        setPropertiesTitle("Channel");
        std::string identifier = observerName + "." + channelName;
        addProperty("Identifier", identifier.c_str());
        addProperty("Description", description.c_str());
        getContentLayout()->addItem(new QSpacerItem(10, 10, QSizePolicy::Minimum, QSizePolicy::Expanding), getContentLayout()->rowCount(), 0);
    }
}
