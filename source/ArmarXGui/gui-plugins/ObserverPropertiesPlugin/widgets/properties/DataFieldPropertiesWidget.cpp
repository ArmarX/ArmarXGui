/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Gui
* @author     Kai Welke (welke@kit.edu)
* @copyright  2012 Humanoids Group, IAIM, IFA
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "DataFieldPropertiesWidget.h"
#include <QLabel>
#include <QPushButton>
#include <QToolButton>
#include <ArmarXCore/observers/variant/TimedVariant.h>
#include <ArmarXGui/libraries/PlotterController/PlotterController.h>
#include <QAction>
#include <QApplication>
#include <QClipboard>
#include <QToolBar>
namespace armarx
{
    DataFieldPropertiesWidget::DataFieldPropertiesWidget(std::string description, DatafieldRefPtr dataFieldIdentifier, VariantPtr variant, IceManagerPtr iceManager, QWidget* parent, Qt::WindowFlags f)
        : PropertiesWidget(parent, f),
          plotter(new PlotterController {this})
    {
        qRegisterMetaType<VariantPtr>("VariantPtr");
        plotter->setIceManager(iceManager);
        this->variant = variant;
        this->dataFieldIdentifier = dataFieldIdentifier;
        this->description = description;

        createUi();
        updateTask = new PeriodicTask<DataFieldPropertiesWidget>(this, &DataFieldPropertiesWidget::updateValue, 200);
        updateTask->start();
        //    startTimer(200);
        setEnableEdit(false);
    }

    DataFieldPropertiesWidget::~DataFieldPropertiesWidget()
    {
        updateTask->stop();
        ARMARX_DEBUG << "~DataFieldPropertiesWidget()";
    }

    void DataFieldPropertiesWidget::setEnableEdit(bool enableEdit)
    {
        variantWidget->setEnableEdit(enableEdit);
    }


    bool DataFieldPropertiesWidget::getEnableEdit()
    {
        return variantWidget->getEnableEdit();
    }

    DataFieldIdentifierPtr DataFieldPropertiesWidget::getDatafieldIdentifier() const
    {
        return dataFieldIdentifier->getDataFieldIdentifier();
    }

    VariantPtr DataFieldPropertiesWidget::getVariant()
    {
        return variant;
    }

    void DataFieldPropertiesWidget::toggleAutoRefresh(bool toggled)
    {
        if (toggled)
        {
            updateTask->start();
        }
        else
        {
            updateTask->stop();
        }
        plotter->plottingPaused(!toggled);
    }

    void DataFieldPropertiesWidget::copyValueToClipboard()
    {
        QClipboard* clipboard = QApplication::clipboard();
        if (clipboard)
        {
            clipboard->setText(variantWidget->getValueText());
        }
        else
        {
            ARMARX_ERROR << "Could not copy text to clipboard";
        }
    }

    void DataFieldPropertiesWidget::createUi()
    {
        // create property entries
        setPropertiesTitle("Datafield");
        addProperty("Identifier", dataFieldIdentifier->getDataFieldIdentifier()->getIdentifierStr().c_str());
        addProperty("Description", description.c_str());

        variantWidget = new VariantWidget(variant);
        addProperty("Type", variant->getTypeName().c_str());
        TimedVariantPtr tvar = TimedVariantPtr::dynamicCast(variant);
        if (tvar)
        {
            auto simestampStr = tvar->getTime().toDateTime() + " (" + std::to_string((TimeUtil::GetTime() - tvar->getTime()).toMilliSeconds()) + " ms old)";
            addProperty("Timestamp", simestampStr.c_str());
        }
        addProperty("Value", "");

        addWidget(variantWidget);
        plotter->setPollingInterval(40);
        plotter->setUpdateInterval(40);
        plotter->setShownInterval(30);
        plotter->setSelectedDatafields(QStringList(QString::fromStdString(dataFieldIdentifier->getDataFieldIdentifier()->getIdentifierStr())), 100);

        addWidget(plotter->getPlotterWidget());
        QToolBar* bar = new QToolBar();
        bar->setIconSize(QSize(16, 16));
        QAction* refreshButton = new QAction(bar);
        refreshButton->setIcon(QIcon(":/icons/view-refresh-7.png"));
        refreshButton->setCheckable(true);
        refreshButton->setToolTip("Refresh value continuously");
        refreshButton->setChecked(true);
        connect(refreshButton, SIGNAL(toggled(bool)), this, SLOT(toggleAutoRefresh(bool)));
        bar->addAction(refreshButton);

        QAction* copyButton = new QAction("Copy", bar);
        copyButton->setToolTip("Copy value to clipboard");
        copyButton->setIcon(QIcon(":/icons/edit-copy-4.svg"));

        connect(copyButton, SIGNAL(triggered()), this, SLOT(copyValueToClipboard()));
        bar->addAction(copyButton);
        getContentLayout()->addWidget(bar, 5, 1);
    }

    void DataFieldPropertiesWidget::updateValue()
    {
        try
        {
            variant = dataFieldIdentifier->getDataField();
            if (variant)
            {
                QMetaObject::invokeMethod(variantWidget, "setValue", Q_ARG(VariantPtr, variant));    //->setValue(variant);
                TimedVariantPtr tvar = TimedVariantPtr::dynamicCast(variant);
                if (tvar)
                {
                    auto simestampStr = tvar->getTime().toDateTime() + " (" + std::to_string((TimeUtil::GetTime() - tvar->getTime()).toMilliSeconds()) + " ms old)";
                    QString dateString = tvar->getInitialized() ?
                                         QString::fromStdString(simestampStr) :
                                         "not initialized";

                    QMetaObject::invokeMethod(this, "setProperty", Q_ARG(QString, "Timestamp"), Q_ARG(QString,  dateString));    //->setValue(variant);
                }

            }
        }
        catch (...)
        {
            ARMARX_INFO << deactivateSpam(10) << "Failed to get current value for datafield " << dataFieldIdentifier->getDataFieldIdentifier() << " - Is the Observer still running?";
        }

    }
}
