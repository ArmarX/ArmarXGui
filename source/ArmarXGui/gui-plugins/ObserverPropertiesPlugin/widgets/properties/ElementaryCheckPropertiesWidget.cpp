/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Gui
* @author     Kai Welke (welke@kit.edu)
* @copyright  2012 Humanoids Group, IAIM, IFA
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "ElementaryCheckPropertiesWidget.h"
#include <ArmarXCore/interface/observers/VariantBase.h>
#include <ArmarXCore/interface/observers/TermImplBase.h>
#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/observers/variant/DataFieldIdentifier.h>
#include "../VariantWidget.h"
#include <QLabel>
#include <QHeaderView>
#include <QScrollBar>

namespace armarx
{
    ElementaryCheckPropertiesWidget::ElementaryCheckPropertiesWidget(const ConditionCheckBasePtr check, std::string checkName, bool showSupportedTypes, bool showParameters, bool showResult, QWidget* parent, Qt::WindowFlags f)
        : PropertiesWidget(parent, f)
    {
        this->showSupportedTypes = showSupportedTypes;
        this->showParameters = showParameters;
        this->showResult = showResult;
        this->checkName = checkName;

        createUi(check);
    }

    void ElementaryCheckPropertiesWidget::createUi(const ConditionCheckBasePtr check)
    {
        // create property entries
        setPropertiesTitle("Check");
        addProperty("Name", checkName.c_str());

        // supported types
        if (showSupportedTypes)
        {
            addHeading("Supported types");

            QTableWidget* typesTable = new QTableWidget(this);
            typesTable->horizontalHeader()->setStretchLastSection(true);
            typesTable->setColumnCount(check->numberParameters + 1);
            QStringList headers;
            headers.append("Datafield");

            for (int i = 0 ; i < check->numberParameters ; i++)
            {
                headers.append(QString("Param %1").arg(i + 1));
            }

            typesTable->setHorizontalHeaderLabels(headers);
            typesTable->verticalHeader()->setVisible(false);
            typesTable->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
            typesTable->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);

            int r = 0;

            SupportedTypeList::const_iterator iter = check->supportedTypes.begin();

            while (iter != check->supportedTypes.end())
            {
                typesTable->setRowCount(r + 1);
                Variant tmp;
                tmp.setType(iter->dataFieldType);
                std::string typeStr = tmp.getTypeName();

                if (iter->dataFieldType == 0)
                {
                    typeStr = "Any type";
                }

                typesTable->setItem(r, 0, new QTableWidgetItem(typeStr.c_str()));

                ParameterTypeList::const_iterator iterList = iter->parameterTypes.begin();
                int c = 1;

                while (iterList != iter->parameterTypes.end())
                {
                    Variant tmp2;
                    tmp2.setType(*iterList);
                    std::string typeStr = tmp2.getTypeName();

                    if (*iterList == 0)
                    {
                        typeStr = "Any type";
                    }

                    typesTable->setItem(r, c, new QTableWidgetItem(typeStr.c_str()));

                    iterList++;
                    c++;
                }

                iter++;
                r++;
            }

            addWidget(typesTable);
        }

        // check parameters
        if (showParameters)
        {
            DataFieldIdentifierPtr dataField = DataFieldIdentifierPtr::dynamicCast(check->configuration.dataFieldIdentifier);
            addProperty("Datafield identifier", dataField->getIdentifierStr().c_str());

            ParameterList::iterator iter = check->configuration.checkParameters.begin();
            int p = 1;

            while (iter != check->configuration.checkParameters.end())
            {
                VariantPtr parameter = VariantPtr::dynamicCast(*iter);

                addProperty(QString("Param %1 type").arg(p), parameter->getTypeName().c_str());
                addProperty(QString("Param %1 value").arg(p), new VariantWidget(parameter));

                p++;
                iter++;
            }
        }

        // check result
        if (showResult)
        {
            std::string text;

            if (check->fulFilled)
            {
                text = "True";
            }
            else
            {
                text = "False";
            }

            addProperty("Fulfilled", text.c_str());
        }
    }
}
