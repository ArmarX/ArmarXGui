/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Gui
* @author     Kai Welke (welke@kit.edu)
* @copyright  2012 Humanoids Group, IAIM, IFA
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "DataFieldWidget.h"

namespace armarx
{
    DataFieldWidget::DataFieldWidget(DataFieldIdentifierPtr dataFieldIdentifier, VariantPtr variant, QWidget* parent, Qt::WindowFlags f)
        : QWidget(parent, f)
    {
        this->variant = variant;
        this->dataFieldIdentifier = dataFieldIdentifier;

        enableEdit = true;
        variantWidget = NULL;

        label = new QLabel(this);
        label->setGeometry(5, 0, 600, 25);
        label->setText((dataFieldIdentifier->getIdentifierStr() + " = ").c_str());
        label->setWordWrap(false);
        label->adjustSize();
        label->setTextInteractionFlags(Qt::TextSelectableByMouse);
        setupLayout();
    }

    void DataFieldWidget::setEnableEdit(bool enableEdit)
    {
        this->enableEdit = enableEdit;
        setupLayout();
    }


    bool DataFieldWidget::getEnableEdit()
    {
        return enableEdit;
    }


    void DataFieldWidget::setupLayout()
    {
        if (variantWidget)
        {
            delete variantWidget;
        }

        variantWidget = new VariantWidget(variant, this);
        variantWidget->setGeometry(label->width() + 5 + 5, 0, 430, 200);
        variantWidget->setEnableEdit(enableEdit);
        variantWidget->show();
    }
}
