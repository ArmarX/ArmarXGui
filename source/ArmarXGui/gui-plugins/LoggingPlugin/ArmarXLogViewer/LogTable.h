/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// ArmarX
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/interface/core/Log.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/editorfileopener.h>


// Qt
#include <QApplication>
#include <QTableWidget>

// C++
#include <vector>
#include <mutex>

#define ARMARX_LOG_TIMESTR "Time"
#define ARMARX_LOG_COMPONENTSTR "Component"
#define ARMARX_LOG_TAGSTR "Tag"
#define ARMARX_LOG_VERBOSITYSTR "Verbosity"
#define ARMARX_LOG_MESSAGESTR "Message"
#define ARMARX_LOG_FILESTR "File"
#define ARMARX_LOG_FUNCTIONSTR "Function"
#define ARMARX_LOG_LOGGINGGROUPSTR "Group"

namespace armarx
{


    class LogTableModel;



    class LogTable :
        public QTableView,
        public Logging
    {
        Q_OBJECT
    protected:
        void resetNewMessageCount()
        {
            newMessageCount = 0;
            maxNewLogLevelType = eUNDEFINED;
        }
        void showEvent(QShowEvent* event) override;
        void hideEvent(QHideEvent*) override;
    public:
        explicit LogTable(QWidget* parent = 0);
        ~LogTable() override;
        int getNewMessageCount()
        {
            return newMessageCount;
        }
        MessageType getMaxNewLogLevelType()
        {
            return maxNewLogLevelType;
        }
        QString getCurrentLiveFilter() const;
        void liveFilter(const QString& search, int startRow = 0);
        bool liveSearch(const QString& search);
        void resetLiveFilter();
        void resetLiveSearch();
        QString getLiveFilterStr()
        {
            return lastLiveFilter;
        }
        LogTableModel*  getModel();
        void fillSearchResults();
        void liveFilterRow(const QString& filterStr, int row);
    public slots:
        bool checkAutoScroll(const QModelIndex& parent, int start, int end);
        bool checkAutoScroll(int dummy = 0);
        void itemsAdded(QModelIndex leftTop, QModelIndex bottomRight);
        void rowsInserted(const QModelIndex& parent, int start, int end) override;
        void rowsAboutToBeRemoved(const QModelIndex& parent, int start, int end) override;
        bool selectNextSearchResult(bool backwards = true, bool keepSelectionIfPossible = false);
        void doubleClickOnCell(const QModelIndex& index);
    protected:
        bool autoscrollActive;
        int newMessageCount;
        MessageType maxNewLogLevelType;
        std::mutex bufferMutex;
        std::vector<LogMessage> logBuffer;
        QString currentLiveFilter, lastLiveFilter;
        int selectedSearchIndex;
        EditorFileOpener fileOpener;



        friend class LogSearch;
    };
}

