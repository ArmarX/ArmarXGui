/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <QAbstractTableModel>

// ArmarX
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <ArmarXCore/interface/core/Log.h>

// C++
#include <vector>
#include <mutex>

namespace armarx
{
    class LogTableModel : public QAbstractTableModel
    {
        Q_OBJECT
    public:
        enum class UserRoles : int
        {
            FullMsgRole = Qt::UserRole + 1
        };

        explicit LogTableModel(QObject* parent = 0);
        int rowCount(const QModelIndex& parent = QModelIndex()) const override;
        int columnCount(const QModelIndex& parent =  QModelIndex()) const override;
        QVariant data(const QModelIndex& index, int role) const override;
        QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
        bool setData(const QModelIndex& index, const QVariant& value, int role) override;
        Qt::ItemFlags flags(const QModelIndex& index) const override;
        void updateView();
        bool insertRows(int row, int count, const QModelIndex& parent) override;

        void addFilter(const std::string& columnName, const std::string& filter);
        void reapplyAllFilters();
        void resetFilters();
        bool rowContainsString(int row, const QString& searchStr) const;
        bool msgContainsString(const LogMessage& logMsg, QString searchStr) const;
        bool rowContainsSameContent(int row, const LogMessage& logMsg) const;

        void search(const QString& searchStr);
        QString getCurrentSearchStr()
        {
            return activeSearchStr;
        }

        MessageType getMaxNewLogLevelType()
        {
            return maxNewLogLevelType;
        }

        int getColumn(const std::string& columnName) const;
        int clearData();
        const std::vector<std::pair<std::string, std::string> >& getFilters() const
        {
            return activeFilters;
        }

        // thread-unsafe access!
        const LogMessage& getLogEntry(size_t row) const;


    signals:

    protected slots:

    public slots:
        bool addEntry(const LogMessage& entry, int* entriesAdded = NULL);
        int addEntries(const std::vector<LogMessage>& entryList, const QString& filterStr);

    protected:
        bool applyFilter(std::pair<std::string, std::string> filter, const LogMessage& logMsg);
        //        bool applyFilter(std::pair<std::string, std::string> filter, int row);
        bool applyFilter(std::string filter, int row, int column);
        bool applyFilters(int row);
        bool applyFilters(const LogMessage& logMsg);
    private:
        QString activeSearchStr;
        int newEntryCount;
        MessageType maxNewLogLevelType;
        std::vector<std::pair<std::string, std::string> > activeFilters;

        std::vector < LogMessage> logEntries;
        std::vector <bool> rowMatchesSearch;
        mutable std::mutex logEntriesMutex;

        RunningTask<LogTableModel>::pointer_type searchResultTask;
    };
}
