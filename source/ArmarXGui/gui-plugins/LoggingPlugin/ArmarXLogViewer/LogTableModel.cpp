/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "LogTableModel.h"
#include "LogTable.h"

#include <ArmarXCore/core/logging/LogSender.h>

namespace armarx
{
    LogTableModel::LogTableModel(QObject* parent) :
        QAbstractTableModel(parent)
    {
        newEntryCount = 0;
        maxNewLogLevelType = eUNDEFINED;
    }

    int LogTableModel::rowCount(const QModelIndex& parent) const
    {
        return logEntries.size();
    }


    int LogTableModel::columnCount(const QModelIndex& parent) const
    {
        return 8;
    }

    QVariant LogTableModel::data(const QModelIndex& index, int role) const
    {
        const int& row = index.row();
        const int& column = index.column();

        switch (role)
        {
            case (int)UserRoles::FullMsgRole:
            {
                std::unique_lock lock(logEntriesMutex);
                if (row >= (signed int)logEntries.size() || row < 0)
                {
                    return QString("n/A");
                }

                const LogMessage& entry = logEntries[row];
                switch (column)
                {

                    case 4:
                    {
                        if (role == Qt::ToolTipRole)
                        {
                            return "";
                        }

                        QString whatStr;

                        if (!entry.backtrace.empty())
                        {
                            whatStr = QString::fromStdString(entry.what + "\nBacktrace:\n" + entry.backtrace);
                        }
                        else
                        {
                            whatStr = QString::fromStdString(entry.what);
                        }

                        return whatStr;
                    }
                    default:
                        return "";
                }
            }
            case Qt::DisplayRole:
            case Qt::ToolTipRole:
            {
                std::unique_lock lock(logEntriesMutex);

                //std::cout << "row " << row << " column:" << column << std::endl;
                if (row >= (signed int)logEntries.size() || row < 0)
                {
                    return QString("n/A");
                }

                const LogMessage& entry = logEntries[row];

                switch (column)
                {
                    case 0:
                    {
                        IceUtil::Time time = IceUtil::Time::microSeconds(entry.time);
                        std::string timeStr = time.toDateTime();
                        timeStr = timeStr.substr(timeStr.find(' '));
                        return QString::fromStdString(timeStr);
                    }

                    case 1:
                    {
                        return QString::fromStdString(entry.who);
                    }

                    case 2:
                    {
                        return QString::fromStdString(entry.tag);
                    }

                    case 3:
                    {
                        if (entry.type != eUNDEFINED)
                        {
                            return QString::fromStdString(LogSender::levelToString((MessageTypeT)entry.type));
                        }
                        else
                        {
                            return QVariant();
                        }
                    }

                    case 4:
                    {
                        if (role == Qt::ToolTipRole)
                        {
                            return "";
                        }

                        QString whatStr;

                        if (!entry.backtrace.empty())
                        {
                            whatStr = QString::fromStdString(entry.what + "\nBacktrace:\n...");
                        }
                        else
                        {
                            whatStr = QString::fromStdString(entry.what);
                        }
                        int lines = 0;
                        int maxLinesToShow = 50;
                        int pos = 0;
                        for (int i = 0; i < whatStr.length(); i++)
                        {
                            auto c = whatStr.at(i);
                            if (c == '\n')
                            {
                                lines++;
                            }
                            if (lines == maxLinesToShow)
                            {
                                break;
                            }
                            pos++;
                        }
                        if (lines >= maxLinesToShow)
                        {
                            whatStr.truncate(pos);
                            return whatStr + "\n...";
                        }
                        return whatStr;
                    }

                    case 5:
                    {
                        if (role == Qt::ToolTipRole)
                        {
                            return QString::fromStdString("Double click to open file in editor: " + entry.file + ":" + QString::number(entry.line).toStdString());
                        }
                        else if (!entry.file.empty())
                        {
                            return QString::fromStdString(entry.file + ":" + QString::number(entry.line).toStdString());
                        }
                        else
                        {
                            return QVariant();
                        }
                    }

                    case 6:
                    {
                        return QString::fromStdString(entry.function);
                    }

                    case 7:
                    {
                        return QString::fromStdString(entry.group);
                    }

                    default:
                        return "";
                }
            }

            case Qt::DecorationRole:
                switch (column)
                {
                    case 5:
                        return QIcon(":icons/document-open-4.ico");

                    default:
                        return QVariant();
                }

            case Qt::BackgroundColorRole:
            {
                if (column == 3) // Log Level color
                {
                    if (row >= (signed int)logEntries.size() || row < 0)
                    {
                        return QVariant();
                    }

                    const LogMessage& entry = logEntries[row];

                    switch (entry.type)
                    {
                        case eVERBOSE:
                            return QColor(200, 200, 250);

                        case eIMPORTANT:
                            return QColor(50, 255, 50);

                        case eWARN:
                            return QColor(216, 88, 0);

                        case eERROR:
                            return QColor(255, 64, 64);

                        case eFATAL:
                            return QColor(176, 0, 0);

                        default:
                            break;
                    }
                }


                if (activeSearchStr.length() == 0)
                {
                    return QVariant();
                }


                std::unique_lock lock(logEntriesMutex);

                //std::cout << "row " << row << " column:" << column << std::endl;
                if (row >= (signed int)logEntries.size() || row < 0)
                {
                    return QVariant();
                }

                const LogMessage& entry = logEntries[row];

                if (msgContainsString(entry, activeSearchStr))
                {
                    return QColor(255, 244, 127);
                }
                else
                {
                    return QVariant();
                }

            }

                //        case Qt::SizeHintRole:
                //        {
                //            if(column == 4)
                //            {
                ////                const armarx::LogMessage & entry = logEntries[row];
                ////                int lines = std::count(entry.what.begin(), entry.what.end(), '\n') +1 ;
                ////                if(lines > 5)
                ////                    lines = 5;
                ////                std::cout << "SizeHintRole lines: " << lines << "-> " << lines*14 << std::endl;
                //                return QSize(350,logRowSize[row]);
                //            }

                //        }

        }

        return QVariant();
    }

    bool LogTableModel::rowContainsString(int row, const QString& searchStr) const
    {
        if (searchStr.length() == 0)
        {
            return true;
        }

        for (int i = 0; i < columnCount(); i++)
        {
            if (data(createIndex(row, i), Qt::DisplayRole).toString().contains(searchStr, Qt::CaseInsensitive))
            {
                return true;
            }
        }

        return false;
    }

    bool LogTableModel::msgContainsString(const LogMessage& logMsg, QString searchStr) const
    {
        if (QString(logMsg.who.c_str()).contains(searchStr, Qt::CaseInsensitive))
        {
            return true;
        }

        if (QString(logMsg.tag.c_str()).contains(searchStr, Qt::CaseInsensitive))
        {
            return true;
        }

        if (QString(logMsg.what.c_str()).contains(searchStr, Qt::CaseInsensitive))
        {
            return true;
        }

        if (QString(logMsg.backtrace.c_str()).contains(searchStr, Qt::CaseInsensitive))
        {
            return true;
        }

        if (QString(logMsg.file.c_str()).contains(searchStr, Qt::CaseInsensitive))
        {
            return true;
        }

        if (QString(logMsg.function.c_str()).contains(searchStr, Qt::CaseInsensitive))
        {
            return true;
        }

        if (QString::number(logMsg.line).contains(searchStr, Qt::CaseInsensitive))
        {
            return true;
        }

        return false;
    }

    bool LogTableModel::rowContainsSameContent(int row, const LogMessage& logMsg) const
    {
        if (row < 0 || row >= (signed int)logEntries.size())
        {
            return false;
        }

        const LogMessage& entry = logEntries[row];

        if (logMsg.line != entry.line)
        {
            return false;
        }

        if (logMsg.who != entry.who)
        {
            return false;
        }

        if (logMsg.tag != entry.tag)
        {
            return false;
        }

        if (logMsg.what != entry.what)
        {
            return false;
        }

        if (logMsg.file != entry.file)
        {
            return false;
        }

        if (logMsg.function != entry.function)
        {
            return false;
        }

        return true;
    }

    void LogTableModel::search(const QString& searchStr)
    {
        activeSearchStr = searchStr;
        QModelIndex leftTop = index(0, 0);
        QModelIndex rightBottom = index(logEntries.size() - 1, 6);
        //    std::cout << "updating  " << leftTop.row() << " til " << rightBottom.row() << std::endl;
        emit dataChanged(leftTop, rightBottom);

        //    searchResultTask = new RunningTask<LogTableModel>(this, &LogTableModel::fillSearchResults, "logTableSearch");

    }



    QVariant LogTableModel::headerData(int section, Qt::Orientation orientation, int role) const
    {
        if (role == Qt::DisplayRole)
        {
            if (orientation == Qt::Horizontal)
            {
                switch (section)
                {
                    case 0:
                        return QString(ARMARX_LOG_TIMESTR);

                    case 1:
                        return QString(ARMARX_LOG_COMPONENTSTR);

                    case 2:
                        return QString(ARMARX_LOG_TAGSTR);

                    case 3:
                        return QString(ARMARX_LOG_VERBOSITYSTR);

                    case 4:
                        return QString(ARMARX_LOG_MESSAGESTR);

                    case 5:
                        return QString(ARMARX_LOG_FILESTR);

                    case 6:
                        return QString(ARMARX_LOG_FUNCTIONSTR);

                    case 7:
                        return QString(ARMARX_LOG_LOGGINGGROUPSTR);

                    default:
                        return QString("");
                }
            }
        }
        else if (role == Qt::ToolTipRole)
        {
            if (orientation == Qt::Horizontal)
            {
                switch (section)
                {
                    case 0:
                        return QString(ARMARX_LOG_TIMESTR);

                    case 1:
                        return QString(ARMARX_LOG_COMPONENTSTR);

                    case 2:
                        return QString(ARMARX_LOG_TAGSTR);

                    case 3:
                        return QString(ARMARX_LOG_VERBOSITYSTR);

                    case 4:
                        return QString(ARMARX_LOG_MESSAGESTR);

                    case 5:
                        return QString::fromStdString(std::string(ARMARX_LOG_FILESTR) + std::string(": Double click cell to open Qtcreator at that location"));

                    case 6:
                        return QString(ARMARX_LOG_FUNCTIONSTR);

                    default:
                        return QString("");
                }
            }
        }

        //    else if(role == Qt::SizeHintRole)
        //    {
        //        QSize size(100,18);
        //        if (orientation == Qt::Horizontal) {
        //            std::cout << "size of header questioned" << std::endl;
        //            switch (section)
        //            {
        //                case 0:
        //                case 1:
        //                case 2:
        //                    size.setWidth(90);
        //                    return size;
        //                case 3:
        //                    size.setWidth(60);
        //                    return size;
        //                case 4:
        //                    size.setWidth(350);
        //                    return size;
        //                case 5:
        //                    size.setWidth(150);
        //                    return size;
        //                case 6:
        //                    size.setWidth(200);
        //                    return size;
        //                default:
        //                    std::cout << "standard size" << std::endl;
        //                    return size;
        //            }
        //        }
        //    }

        return QVariant();
    }

    bool LogTableModel::setData(const QModelIndex& index, const QVariant& value, int role)
    {
        return false;
    }

    Qt::ItemFlags LogTableModel::flags(const QModelIndex& index) const
    {
        if (index.column() == getColumn(ARMARX_LOG_MESSAGESTR))
        {
            return Qt::ItemIsSelectable |  Qt::ItemIsEditable | Qt::ItemIsEnabled ;
        }
        else
        {
            return Qt::ItemIsSelectable  | Qt::ItemIsEnabled ;
        }
    }

    void LogTableModel::updateView()
    {
        //    QModelIndex topLeft = createIndex(0,0);
        //    emit dataChanged(topLeft, topLeft);
        QModelIndex leftTop = index(logEntries.size() - newEntryCount, 0);
        QModelIndex rightBottom = index(logEntries.size() - 1, 6);
        //    std::cout << "updating  " << leftTop.row() << " til " << rightBottom.row() << std::endl;
        emit dataChanged(leftTop, rightBottom);
        newEntryCount = 0;
        maxNewLogLevelType = eUNDEFINED;
    }

    bool LogTableModel::insertRows(int row, int count, const QModelIndex& parent)
    {
        beginInsertRows(QModelIndex(), row, row + count - 1);
        int newEntries = row + count - logEntries.size();

        for (int i = 0; i < newEntries; i++)
        {
            logEntries.push_back(LogMessage());
        }

        endInsertRows();
        return true;
    }

    void LogTableModel::addFilter(const std::string& columnName, const std::string& filter)
    {
        activeFilters.push_back(std::make_pair(columnName, filter));
    }

    void LogTableModel::reapplyAllFilters()
    {

        {
            std::unique_lock lock(logEntriesMutex);
            std::vector < LogMessage>::iterator it = logEntries.begin();
            int row = 0;

            for (; it != logEntries.end(); ++it)
            {
                const LogMessage& entry = *it;

                if (!applyFilters(entry))
                {
                    beginRemoveRows(QModelIndex(), row, row);
                    it = logEntries.erase(it);
                    it--;
                    endRemoveRows();
                }
                else
                {
                    row++;
                }
            }
        }
        updateView();
    }

    void LogTableModel::resetFilters()
    {
        activeFilters.clear();
    }

    bool LogTableModel::addEntry(const LogMessage& entry, int* entriesAdded)
    {
        {
            std::unique_lock lock(logEntriesMutex);

            if (entriesAdded)
            {
                *entriesAdded = 0;
            }

            if (!applyFilters(entry))
            {
                return false;
            }

            if (rowContainsSameContent(logEntries.size() - 1, entry))
            {
                *logEntries.rbegin() = entry;
                return false;
            }
            else
            {
                newEntryCount++;

                if (entry.type > maxNewLogLevelType)
                {
                    maxNewLogLevelType = entry.type;
                }

                logEntries.push_back(entry);

                if (entriesAdded)
                {
                    (*entriesAdded)++;
                }
            }
        }
        return true;
    }

    int LogTableModel::addEntries(const std::vector<LogMessage>& entryList, const QString& filterStr)
    {
        if (entryList.size() == 0)
        {
            return 0;
        }

        //std::cout << "adding entries " << entryList.size() << std::endl;
        unsigned int size = entryList.size();
        int entriesAdded = 0;

        for (unsigned int i = 0; i < size; i++)
        {
            if (addEntry(entryList[i]))
            {
                entriesAdded++;
            }
        }
        if (entriesAdded > 0)
        {
            beginInsertRows(QModelIndex(), logEntries.size() - entriesAdded, logEntries.size() - 1);
            endInsertRows();
            updateView();
        }
        return entriesAdded;
    }



    int LogTableModel::getColumn(const std::string& columnName) const
    {
        if (columnName.empty())
        {
            return -1;
        }

        QString qcolumnName = QString::fromStdString(columnName);

        for (int i = 0; i < columnCount(QModelIndex()); i++)
        {
            if (headerData(i, Qt::Horizontal, Qt::DisplayRole).toString().compare(qcolumnName) == 0)
            {
                return i;
            }
        }

        return -1;
    }

    int LogTableModel::clearData()
    {
        int size;
        {
            beginRemoveRows(QModelIndex(), 0, logEntries.size() - 1);
            std::unique_lock lock(logEntriesMutex);
            size = (int)logEntries.size();
            removeRows(0, logEntries.size() - 1);
            logEntries.clear();
            endRemoveRows();
        }
        updateView();
        return size;
    }

    const LogMessage& LogTableModel::getLogEntry(size_t row) const
    {
        return logEntries.at(row);
    }



    bool LogTableModel::applyFilter(std::pair<std::string, std::string> filter, const LogMessage& logMsg)
    {
        std::string columnName = filter.first;
        QString filterStr = QString::fromStdString(filter.second);

        //    for(unsigned int i = 0; i < columns.size(); i++)
        //    {
        //        if(columnName == columns[i].columnName)
        //        {
        //            if(logMsg.who.find(filterStr) == std::string::npos)
        //                return false;
        //            else
        //                return true;
        //        }
        //    }
        // TODO: identify columns by integer instead of string for better performance
        if (columnName == ARMARX_LOG_LOGGINGGROUPSTR)
        {
            if (QString(logMsg.group.c_str()).contains(filterStr, Qt::CaseSensitive))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else if (columnName == ARMARX_LOG_COMPONENTSTR)
        {
            if (QString(logMsg.who.c_str()).contains(filterStr, Qt::CaseInsensitive))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else if (columnName == ARMARX_LOG_TAGSTR)
        {
            if (QString(logMsg.tag.c_str()).contains(filterStr, Qt::CaseInsensitive))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else if (columnName == ARMARX_LOG_VERBOSITYSTR)
        {
            if (logMsg.type < QString(filterStr).toInt())
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else if (columnName == ARMARX_LOG_MESSAGESTR)
        {
            if (QString(logMsg.what.c_str()).contains(filterStr, Qt::CaseInsensitive))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else if (columnName == ARMARX_LOG_FILESTR)
        {
            if (!QString(logMsg.file.c_str()).contains(filterStr, Qt::CaseInsensitive) && filterStr.toInt() != logMsg.line)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else if (columnName == ARMARX_LOG_FUNCTIONSTR)
        {
            if (QString(logMsg.function.c_str()).contains(filterStr, Qt::CaseInsensitive))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        return true;
    }

    bool LogTableModel::applyFilter(std::string filter, int row, int column)
    {
        if (row >= rowCount(QModelIndex()) || column >= columnCount(QModelIndex()))
        {
            return true;
        }

        return data(createIndex(row, column), Qt::DisplayRole).toString()
               .contains(QString(filter.c_str()), Qt::CaseInsensitive);
        return false;
    }

    bool LogTableModel::applyFilters(int row)
    {
        for (unsigned int  i = 0; i < activeFilters.size(); i++)
        {
            if (!applyFilter(activeFilters[i].second, row, getColumn(activeFilters[i].first)))
            {
                return false;
            }
        }

        return true;
    }

    bool LogTableModel::applyFilters(const LogMessage& logMsg)
    {
        for (unsigned int  i = 0; i < activeFilters.size(); i++)
        {
            if (!applyFilter(activeFilters[i], logMsg))
            {
                return false;
            }
        }

        return true;
    }


    //bool LogTableModel::applyFilter(std::pair filter, int row)
    //{
    //    int selectedColumn = getColumn(filter.first);

    //    if(selectedColumn == -1)
    //    {
    //        ARMARX_WARNING << "Could not find column " << filter.first << flush;
    //        return true;
    //    }

    //    return true;
    //}
}
