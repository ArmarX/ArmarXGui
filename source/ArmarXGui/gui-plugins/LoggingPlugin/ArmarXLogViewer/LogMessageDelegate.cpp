/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "LogMessageDelegate.h"
#include "LogTableModel.h"

#include <QPalette>
#include <QPainter>
#include <QTextEdit>
#include <QTableView>

#include <iostream>
#include <algorithm>


namespace armarx
{
    LogMessageDelegate::LogMessageDelegate(QObject* parent) :
        QStyledItemDelegate(parent)
    {
        //    _lineEdit = new QLabel();
        //    _lineEdit->setWordWrap(true);
    }

    LogMessageDelegate::~LogMessageDelegate()
    {
        //    delete _lineEdit;
    }



    //void LogMessageDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
    //{

    //    QString logMessageStr = index.data().toString();
    //    _lineEdit->setText(logMessageStr);
    //    int lines = std::count(logMessageStr.begin(), logMessageStr.end(), '\n')+1;
    //    if(lines > 5)
    //        lines = 5;
    //    QRect rect = option.rect;

    //    rect.setHeight(15*lines);
    //    _lineEdit->resize(rect.size());
    //    std::cout << "lines:" << lines << "old height: " << option.rect.height() << " new: " << rect.height() << " lineedit height: " << _lineEdit->height() << std::endl;
    //    // Change background color if the item is selected
    //    QPalette pal;
    //    if ((option.state & QStyle::State_Selected) == QStyle::State_Selected)
    //    {
    //      pal.setBrush(QPalette::Window, QBrush(QColor(Qt::lightGray)));
    //    }
    //    else
    //    {
    //      pal.setBrush(QPalette::Window, QBrush(QColor(Qt::transparent)));
    //    }
    //    _lineEdit->setPalette(pal);

    //    // Paint the widget now.
    //    painter->save();
    //    painter->translate(option.rect.topLeft());
    //    _lineEdit->render(painter);
    //    painter->restore();
    //}

    //QSize LogMessageDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
    //{
    //    const int width = 350;

    //    const int height = 200;
    //    std::cout << "height: " << _lineEdit->height() << std::endl;
    //    return QSize(width, _lineEdit->height());
    //}

    QWidget* LogMessageDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const
    {
        if (index.column() == 4)
        {
            //        QTableView* tableView = qobject_cast<QTableView*>(parent);
            QTextEdit* editor = new QTextEdit(parent);
            //        editor->resize(100,100);
            editor->adjustSize();
            editor->setReadOnly(true);
            //        connect(this, SIGNAL(resizeRow(int)),
            //                tableView, SLOT(resizeRowToContents(int)));
            //        emit resizeRow(index.column());
            //        tableView->resizeRowToContents(index.column());
            return editor;
        }
        else
        {
            return QStyledItemDelegate::createEditor(parent, option, index);
        }
    }

    void LogMessageDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const
    {
        QTextEdit* textEdit = qobject_cast<QTextEdit*>(editor);
        textEdit->setText(index.data((int)LogTableModel::UserRoles::FullMsgRole).toString());

    }

    void LogMessageDelegate::updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& index) const
    {
        QRect r = option.rect;
        r.setX(30);   // just for showing the overlay
        r.setY(10);
        r.setSize(QSize(700, 300));
        editor->setGeometry(r);
    }

    void LogMessageDelegate::commitAndCloseEditor()
    {
        emit closeEditor(qobject_cast<QWidget*>(sender()));
    }
}
