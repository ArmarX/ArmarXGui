/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXGui/gui-plugins/LoggingPlugin/ui_LogViewer.h>
#include "LogTable.h"
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <QPointer>
#include <QToolBar>

#include <mutex>

class QTimer;

namespace armarx
{

    class LogTableModel;

    /*!
      \page ArmarXGui-GuiPlugins-LogViewer LogViewer
      \brief The log viewer allows you to filter and view log messages from
      all running ArmarX applications.
      \image html LogViewer.png
      ArmarXGui Documentation \ref LogViewer
      \see LoggingPlugin
     */
    /*!
      \class LogViewer
      \see LoggingPlugin
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT LogViewer :
        public ArmarXComponentWidgetControllerTemplate<LogViewer>,
        public Log
    {
        Q_OBJECT
    public:
        LogViewer();
        ~LogViewer() override;
        //inherited from ArmarXMdiWidget

        void loadSettings(QSettings* settings) override;
        void saveSettings(QSettings* settings) override;
        static QString GetWidgetName()
        {
            return "Meta.LogViewer";
        }
        static QIcon GetWidgetIcon()
        {
            return QIcon("://icons/papyrus.svg");
        }
        static QIcon GetWidgetCategoryIcon()
        {
            return QIcon("://icons/papyrus.svg");
        }
        QPointer<QWidget> getCustomTitlebarWidget(QWidget* parent) override;

        // inherited from Component
        void onInitComponent() override;
        void onConnectComponent() override;
        void onExitComponent() override;

        // inherited from Log
        void write(const std::string& who, Ice::Long time, const std::string& tag, MessageType severity, const std::string& message, const std::string& file, Ice::Int line, const std::string& function, const Ice::Current& = Ice::emptyCurrent);
        void writeLog(const LogMessage& msg, const Ice::Current& = Ice::emptyCurrent) override;


        void setRow(const LogMessage& msg, int rowIndex);
        LogTable* addEmptyFilter();
        LogTable* addFilter(QString filterId, QString filterName, QString loggingGroup, QString componentFilter, QString tagFilter, MessageType minimumVerbosity, QString messageFilter, QString fileFilter, QString functionFilter);
        //! This function checks, if there are new components in the log and if so, it creates new filters for these components
        bool checkAndAddNewFilter(const QString& loggingGroupName, const QString& componentName);


    public slots:
        void addNewEntry(const LogMessage& msg);
        void performLiveFilter(QString searchStr, int startRow = 0);
        void performLiveSearch(QString searchStr);
        void clearSelectedLog();
        void clearAllLogs();
        void pauseLogging(bool pause = false);
        void addFilter();
        void editFilter(QTreeWidgetItem* item, int column);
        void removeSelectedFilter();
        void removeFilter(QTreeWidgetItem* item);
        void filterSelectionChanged(QTreeWidgetItem* item, QTreeWidgetItem* previous);
        void insertPendingEntries();
        void searchTypeChanged(int index);
        void selectNextSearchResult();
        void selectPreviousSearchResult();
        void updateFilterList();
        void OpenConfigureDialog();
    signals:
        void newEntry(const LogMessage& msg);
        void updateFilterListSignal();
        void componentConnected();
    protected:
        bool onClose() override;

        Ui_LogViewer ui;
        LogTable* logTable;
    private:
        QString loggingGroupNameToFilterName(const QString& loggingGroupName) const;

        bool loggingPaused;
        LogPrx logProxy;
        std::vector<LogMessage> buffer;
        IceUtil::Time lastLiveSearchEditChangeTime;
        std::map<QString, LogTable*> filterMap;

        std::vector <LogMessage> pendingEntries;

        mutable std::mutex pendingEntriesMutex;
        QTimer* pendingEntriesTimer;
        QToolBar* customToolbar;


    };
}


