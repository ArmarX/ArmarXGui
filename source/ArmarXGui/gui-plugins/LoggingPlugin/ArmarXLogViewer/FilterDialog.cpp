/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Gui
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "FilterDialog.h"
#include <ArmarXGui/gui-plugins/LoggingPlugin/ui_FilterDialog.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXWidgetController.h>

#include <ArmarXCore/core/logging/LogSender.h>
#include <ArmarXCore/interface/core/Log.h>

namespace armarx
{
    FilterDialog::FilterDialog(QWidget* parent) :
        QDialog(parent),
        ui(new Ui::FilterDialog)
    {
        ui->setupUi(this);

        for (int i = 0; i < eLogLevelCount; i++)
        {
            ui->cbVerbosity->addItem(LogSender::levelToString((MessageTypeT)i).c_str());
        }
    }

    FilterDialog::~FilterDialog()
    {
        delete ui;
    }

    void FilterDialog::accept()
    {
        if (ui->edtFilterName->text().length())
        {
            QDialog::accept();
        }
        else
        {
            ArmarXWidgetController::showMessageBox("Please enter a name for the filter!");
        }
    }
}
