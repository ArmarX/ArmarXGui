/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#include "LogTable.h"


#include <QHeaderView>
#include <QApplication>
#include <QScrollBar>
#include <QTimer>

#include "LogTableModel.h"
#include "LogMessageDelegate.h"

#include <filesystem>

namespace armarx
{
#define MAX_BUFFER_SIZE 10000
#define BUFFER_SIZE_FACTOR 1.3

    LogTable::LogTable(QWidget* parent) :
        QTableView(parent),
        newMessageCount(0),
        maxNewLogLevelType(eUNDEFINED)
    {
        autoscrollActive = true;
        selectedSearchIndex = -1;


        setObjectName(QString::fromUtf8("tableLog"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy2.setHorizontalStretch(10);
        sizePolicy2.setVerticalStretch(10);
        sizePolicy2.setHeightForWidth(sizePolicy().hasHeightForWidth());
        setSizePolicy(sizePolicy2);
        setSizeIncrement(QSize(1, 0));
        setEditTriggers(QAbstractItemView::DoubleClicked); //QAbstractItemView::NoEditTriggers
        setAlternatingRowColors(true);
        setShowGrid(true);
        setGridStyle(Qt::SolidLine);
        setSortingEnabled(false);
        setWordWrap(true);
        setCornerButtonEnabled(true);

        setSelectionMode(QAbstractItemView::SingleSelection);
        setSelectionBehavior(QAbstractItemView::SelectRows);

        horizontalHeader()->setVisible(true);
        horizontalHeader()->setCascadingSectionResizes(true);
        horizontalHeader()->setDefaultSectionSize(100);
        horizontalHeader()->setMinimumSectionSize(20);
        horizontalHeader()->setStretchLastSection(true);
        verticalHeader()->setVisible(false);
        verticalHeader()->setDefaultSectionSize(20);

        //    verticalHeader()->setResizeMode(QHeaderView::ResizeToContents);

        setModel(new LogTableModel(this));
        //connect(model(), SIGNAL(dataChanged(QModelIndex,QModelIndex)), this, SLOT(dataChanged(QModelIndex,QModelIndex)));
        setColumnWidth(0, 90);
        setColumnWidth(1, 90);
        setColumnWidth(2, 90);
        setColumnWidth(3, 60);
        setColumnWidth(4, 700);
        setColumnWidth(5, 150);
        setColumnWidth(6, 200);
        hideColumn(7);

        QFont font;
        font.setPointSize(8);
        setFont(font);


        setItemDelegateForColumn(getModel()->getColumn(ARMARX_LOG_MESSAGESTR), new LogMessageDelegate());

        QAbstractItemModel* absmodel = qobject_cast<QAbstractItemModel*>(model());
        connect(absmodel, SIGNAL(rowsAboutToBeInserted(QModelIndex, int, int)), this, SLOT(checkAutoScroll(QModelIndex, int, int)));
        connect(this, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(doubleClickOnCell(QModelIndex)));
        connect(absmodel, SIGNAL(dataChanged(QModelIndex, QModelIndex)), this, SLOT(itemsAdded(QModelIndex, QModelIndex)));
        connect(verticalScrollBar(), SIGNAL(valueChanged(int)), this, SLOT(checkAutoScroll()));

        //    connect(this, SIGNAL(scrollToEnd()), this, SLOT(scrollToBottom()));
    }



    LogTable::~LogTable()
    {
    }

    QString LogTable::getCurrentLiveFilter() const
    {
        return currentLiveFilter;
    }

    void LogTable::liveFilterRow(const QString& filterStr, int row)
    {
        LogTableModel* logModel = getModel();
        bool contains = logModel->rowContainsString(row, filterStr);
        if (! isRowHidden(row) && !contains)
        {
            setRowHidden(row, true);
        }
        else if (isRowHidden(row) && contains)
        {
            setRowHidden(row, false);
        }
    }


    void LogTable::liveFilter(const QString& filterStr, int startRow)
    {
        LogTableModel* logModel = getModel();
        currentLiveFilter = filterStr;
        if (filterStr.length() && filterStr.contains(lastLiveFilter))
        {
            // incremental filter - Only remove not fitting entries
            int rowCount = model()->rowCount();

            for (int i = startRow; i < rowCount; i++)
            {
                if (! isRowHidden(i) && !logModel->rowContainsString(i, filterStr))
                {
                    setRowHidden(i, true);
                }
                if (i % 1000 == 0)
                {
                    qApp->processEvents();
                }
                if (filterStr != currentLiveFilter)
                {
                    break;    // filterstring already changed again -> cancel
                }
            }
        }
        else
        {
            // filter from scratch
            //        clear();
            //        setRowCount(0);
            //        QList<QTableWidgetItem*>results = findItems(filterStr,Qt::MatchContains);
            //        foreach(QTableWidgetItem*item, results)
            //        {
            //            if(applyFilters(item))
            //                item->
            //        }
            int rowCount = model()->rowCount();

            for (int i = startRow; i < rowCount; i++)
            {
                if (isRowHidden(i))
                {
                    if (logModel->rowContainsString(i, filterStr))
                    {
                        setRowHidden(i, false);
                    }
                }
                else if (!logModel->rowContainsString(i, filterStr))
                {
                    setRowHidden(i, true);
                }
                if (i % 1000 == 0)
                {
                    qApp->processEvents();
                }
                if (filterStr != currentLiveFilter)
                {
                    break;    // filterstring already changed again -> cancel
                }
            }

            //        std::cout << "logBuffer: " << logBuffer.size() <<   std::endl;
            //        for(unsigned int i=0; i < logBuffer.size(); i++)
            //        {
            //            if(msgContainsString(logBuffer[i], filterStr) && applyFilters(logBuffer[i]) )
            //                addEntry(logBuffer[i], true);
            //        }
        }

        lastLiveFilter = filterStr;
    }

    bool LogTable::liveSearch(const QString& search)
    {
        getModel()->search(search);
        //    scrollToBottom();
        clearSelection();
        return selectNextSearchResult(true, true);

    }

    void LogTable::resetLiveFilter()
    {
        //    ARMARX_WARNING_S << "LiveFilterReseted";
        selectedSearchIndex = -1;
        lastLiveFilter = "";
        liveFilter("");
    }

    void LogTable::resetLiveSearch()
    {
        getModel()->search("");
        //    repaint();
    }

    LogTableModel* LogTable::getModel()
    {
        return dynamic_cast<LogTableModel*>(model());
    }
    bool LogTable::checkAutoScroll(const QModelIndex& parent, int start, int end)
    {
        return checkAutoScroll();
    }

    bool LogTable::checkAutoScroll(int dummy)
    {
        if (verticalScrollBar()->value() == verticalScrollBar()->maximum())
        {
            autoscrollActive = true;
        }
        else
        {
            autoscrollActive = false;
        }


        return autoscrollActive;
    }

    void LogTable::itemsAdded(QModelIndex leftTop, QModelIndex bottomRight)
    {

    }

    void LogTable::showEvent(QShowEvent* event)
    {
        resetNewMessageCount();

        //    verticalHeader()->setResizeMode(QHeaderView::ResizeToContents);
        //    resizeRowsToContents();
        //    verticalHeader()->setResizeMode(QHeaderView::Fixed);
        if (autoscrollActive)
        {
            QTimer::singleShot(50, this, SLOT(scrollToBottom()));    // delayed because something is inserting one line after this function or something
        }

    }

    void LogTable::hideEvent(QHideEvent*)
    {
        if (verticalScrollBar()->value() == verticalScrollBar()->maximum())
        {
            autoscrollActive = true;
        }
        else
        {
            autoscrollActive = false;
        }
    }


    void LogTable::rowsInserted(const QModelIndex& parent, int start, int end)
    {
        auto logModel = getModel();
        auto fontHeight =  QFontMetrics(font()).height();
        //    ARMARX_INFO << "Adjusting height for " << start << " to " << end << " count: " << logModel->rowCount();
        for (int i = start; i <= end && i < logModel->rowCount(); ++i)
        {
            //        auto stringRows = armarx::Split(logModel->getLogEntry(i).what, "\n", true, true);
            auto index = model()->index(i, 4, parent);
            auto stringRows = model()->data(index).toString().split("\n").size();
            if (stringRows > 1)
            {
                setRowHeight(i, fontHeight * stringRows + 1);
            }
        }

        if (!isVisible())
        {
            newMessageCount += end - start + 1;

            if (getModel()->getMaxNewLogLevelType() > maxNewLogLevelType)
            {
                maxNewLogLevelType = getModel()->getMaxNewLogLevelType();
            }

            //        LogTableModel * logModel = getModel();
            //        if(start> end)
            //            std::swap(start, end);
            //        if(lastLiveFilter.length() > 0)
            //        {
            //            for (int i = start; i <= end; ++i) {
            //                if(!logModel->rowContainsString(i, lastLiveFilter))
            //                    setRowHidden(i, true);
            //            }
            //        }
        }
        //    resizeRowsToContents();
        else
        {
            //        verticalHeader()->setResizeMode(QHeaderView::ResizeToContents);

            //        for(int i = start; i < end; i++)
            //            resizeRowToContents(i);
            //        verticalHeader()->setResizeMode(QHeaderView::Fixed);

        }

        if (autoscrollActive)
        {
            scrollToBottom();
        }
    }

    void LogTable::rowsAboutToBeRemoved(const QModelIndex& parent, int start, int end)
    {
        if (end - start >= model()->rowCount() - 1)
        {
            resetNewMessageCount();
        }
    }


    bool LogTable::selectNextSearchResult(bool backwards, bool keepSelectionIfPossible)
    {

        int checkCounter = 0; // just a counter for avoiding inifite loops
        int tempSelectedSearchIndex = indexAt(QPoint(10, 10)).row();

        if (selectedIndexes().size() > 0)
        {
            tempSelectedSearchIndex = selectedIndexes()[0].row();
        }

        int oldSelectedSearchIndex = tempSelectedSearchIndex;

        do // search until we reach old line again
        {

            if ((tempSelectedSearchIndex != oldSelectedSearchIndex || keepSelectionIfPossible)
                &&    getModel()->rowContainsString(tempSelectedSearchIndex, getModel()->getCurrentSearchStr()))
            {
                selectRow(tempSelectedSearchIndex);
                return true;
            }

            if (backwards)
            {
                tempSelectedSearchIndex--;
            }
            else
            {
                tempSelectedSearchIndex++;
            }

            if (tempSelectedSearchIndex >= model()->rowCount())
            {
                tempSelectedSearchIndex = 0;
            }

            if (tempSelectedSearchIndex < 0)
            {
                tempSelectedSearchIndex = model()->rowCount() - 1;
            }


            checkCounter++;

            if (checkCounter > model()->rowCount())
            {
                break;
            }
        }
        while (tempSelectedSearchIndex != oldSelectedSearchIndex);

        return false;

    }

    void LogTable::doubleClickOnCell(const QModelIndex& index)
    {
        if (index.column() != getModel()->getColumn(ARMARX_LOG_FILESTR))
        {
            return;
        }

        QString fileWithLineNumber = model()->data(index).toString();
        std::string file =  fileWithLineNumber.toStdString();
        file.erase(file.rfind(':'));
        std::string line = fileWithLineNumber.toStdString();
        line = line.erase(0, line.rfind(':') + 1);

        if (!std::filesystem::exists(file))
        {
            ARMARX_INFO << "File '"  << file << "' does not exists - cannot open it.";
            return;
        }

        fileOpener.openFileWithDefaultEditor(file, atoi(line.c_str()));
        //    std::string command = "qtcreator -client " + fileWithLineNumber.toStdString() + "&";
        //    if(system(command.c_str())){}

    }
}
