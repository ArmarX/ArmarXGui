/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGui::gui-plugins::MessageDisplayPluginWidgetController
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXGui/gui-plugins/MessageDisplayPlugin/ui_MessageDisplayPluginWidget.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXGui/interface/MessageDisplayInterface.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <QTimer>

namespace armarx
{
    /**
    \page ArmarXGui-GuiPlugins-MessageDisplayPlugin MessageDisplayPlugin
    \brief The MessageDisplayPlugin allows visualizing ...

    \image html MessageDisplayPlugin.png
    The user can

    API Documentation \ref MessageDisplayPluginWidgetController

    \see MessageDisplayPluginGuiPlugin
    */

    /**
     * \class MessageDisplayPluginWidgetController
     * \brief MessageDisplayPluginWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        MessageDisplayPluginWidgetController:
        public armarx::ArmarXComponentWidgetControllerTemplate < MessageDisplayPluginWidgetController >,
        public armarx::MessageDisplayInterface
    {
        Q_OBJECT

    public:
        /**
         * Controller Constructor
         */
        explicit MessageDisplayPluginWidgetController();

        /**
         * Controller destructor
         */
        virtual ~MessageDisplayPluginWidgetController();

        /**
         * @see ArmarXWidgetController::loadSettings()
         */
        void loadSettings(QSettings* settings) override;

        /**
         * @see ArmarXWidgetController::saveSettings()
         */
        void saveSettings(QSettings* settings) override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName()
        {
            return "Util.MessageDisplayPlugin";
        }

        /**
         * \see armarx::Component::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * \see armarx::Component::onConnectComponent()
         */
        void onConnectComponent() override;

        void setMessage(const std::string& caption, const std::string& subCaption, const Ice::Current&) override;
        void setCaption(const std::string& caption, const Ice::Current&) override;
        void setSubCaption(const std::string& subCaption, const Ice::Current&) override;

        void updateLabels();

    public slots:
        void timerEvent();
        void setMessageContentImpl(const QString& caption, const QString& subcaption);
        void setCaptionContentImpl(const QString& caption);
        void setSubCaptionContentImpl(const QString& subcaption);
        void clearText();

    signals:
        void setMessageContent(const QString& caption, const QString& subcaption);
        void setCaptionContent(const QString& caption);
        void setSubCaptionContent(const QString& subcaption);

    private:
        /**
         * Widget Form
         */
        Ui::MessageDisplayPluginWidget widget;

        QTimer fadeTimer;
        int fadeDuration;

        QString currentCaption;
        QString currentSubCaption;

        // ArmarXWidgetController interface
    public:
        QPointer<QWidget> getCustomTitlebarWidget(QWidget* parent) override;


    };
}


