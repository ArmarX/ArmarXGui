/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    ArmarXGui::gui-plugins::MessageDisplayPluginWidgetController
 * \author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * \date       2018
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MessageDisplayPluginWidgetController.h"

#include <string>
#include <sstream>
#include <QPushButton>
#include <QToolBar>

namespace armarx
{
#define FADE_TIMER_MSEC 100
#define FADE_DURATION 2000

    MessageDisplayPluginWidgetController::MessageDisplayPluginWidgetController()
    {
        widget.setupUi(getWidget());
    }


    MessageDisplayPluginWidgetController::~MessageDisplayPluginWidgetController()
    {

    }


    void MessageDisplayPluginWidgetController::loadSettings(QSettings* settings)
    {

    }

    void MessageDisplayPluginWidgetController::saveSettings(QSettings* settings)
    {

    }


    void MessageDisplayPluginWidgetController::onInitComponent()
    {
        usingTopic("MessageDisplayTopic");

        // Don't display anything on startup
        widget.labelCaption->setText("");
        widget.labelSubcaption->setText("");

        connect(&fadeTimer, SIGNAL(timeout()), this, SLOT(timerEvent()));
        connect(this, SIGNAL(setMessageContent(QString, QString)), this, SLOT(setMessageContentImpl(QString, QString)));
        connect(this, SIGNAL(setCaptionContent(QString)), this, SLOT(setCaptionContentImpl(QString)));
        connect(this, SIGNAL(setSubCaptionContent(QString)), this, SLOT(setSubCaptionContentImpl(QString)));
        connect(widget.labelCaption, SIGNAL(doubleClicked()), this, SLOT(clearText()));
    }


    void MessageDisplayPluginWidgetController::onConnectComponent()
    {

    }

    void MessageDisplayPluginWidgetController::setMessage(const std::string& caption, const std::string& subCaption, const Ice::Current&)
    {
        ARMARX_DEBUG << "New message: " << caption << " (" << subCaption << ")";
        emit setMessageContent(QString::fromStdString(caption), QString::fromStdString(subCaption));
    }

    void armarx::MessageDisplayPluginWidgetController::setCaption(const std::string& caption, const Ice::Current&)
    {
        ARMARX_DEBUG << "New caption: " << caption;
        emit setCaptionContent(QString::fromStdString(caption));
    }

    void armarx::MessageDisplayPluginWidgetController::setSubCaption(const std::string& subCaption, const Ice::Current&)
    {
        ARMARX_DEBUG << "New subCaption: " << subCaption;
        emit setSubCaptionContent(QString::fromStdString(subCaption));
    }



    void MessageDisplayPluginWidgetController::timerEvent()
    {
        fadeDuration -= FADE_TIMER_MSEC;
        if (fadeDuration <= 0)
        {
            fadeTimer.stop();
        }

        std::stringstream ss;
        ss << "#" << std::hex << (int)(255 * (((float)fadeDuration) / FADE_DURATION)) << "0000";

        ARMARX_INFO << ("<font color='" + QString::fromStdString(ss.str()) + "'>" + currentCaption + "</font>").toStdString();
        widget.labelCaption->setText("<font color='" + QString::fromStdString(ss.str()) + "'>" + currentCaption + "</font>");
        widget.labelSubcaption->setText("<font color='" + QString::fromStdString(ss.str()) + "'>" + currentSubCaption + "</font>");
    }

    void MessageDisplayPluginWidgetController::setMessageContentImpl(const QString& caption, const QString& subcaption)
    {
        currentCaption = caption;
        currentSubCaption = subcaption;
        updateLabels();
    }

    void MessageDisplayPluginWidgetController::setCaptionContentImpl(const QString& caption)
    {
        currentCaption = caption;
        updateLabels();
    }
    void MessageDisplayPluginWidgetController::setSubCaptionContentImpl(const QString& subcaption)
    {
        currentSubCaption = subcaption;
        updateLabels();
    }

    void MessageDisplayPluginWidgetController::updateLabels()
    {
        fadeTimer.stop();

        widget.labelCaption->setText("<font color='#FF0000'>" + currentCaption + "</font>");
        widget.labelSubcaption->setText("<font color='#FF0000'>" + currentSubCaption + "</font>");

        fadeDuration = FADE_DURATION;
        fadeTimer.start(FADE_TIMER_MSEC);
    }

    void MessageDisplayPluginWidgetController::clearText()
    {
        widget.labelCaption->setText("");
        widget.labelSubcaption->setText("");
    }

    QPointer<QWidget> MessageDisplayPluginWidgetController::getCustomTitlebarWidget(QWidget* parent)
    {
        QToolBar* customToolbar = new QToolBar(parent);
        customToolbar->addAction("clear", this, SLOT(clearText()));
        return customToolbar;
    }
}
