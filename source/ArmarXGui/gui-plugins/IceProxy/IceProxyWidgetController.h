/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGui::gui-plugins::IceProxyWidgetController
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXGui/gui-plugins/IceProxy/ui_IceProxyWidget.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/IceProxyFinder.h>

namespace armarx
{
    /**
    \page ArmarXGui-GuiPlugins-IceProxy IceProxy
    \brief The IceProxy widget allows you to gather basig information about a proxy (i.e. Its identity / ping / type ids / stringified version.).
    The proxy can be passed as string or loaded via a proxy finder (requires the proxy to be registered to the ice grid).
    This widget's main purpose is debugging.

    \image html IceProxy.png

    API Documentation \ref IceProxyWidgetController

    \see IceProxyGuiPlugin
    */

    /**
     * \class IceProxyWidgetController
     * \brief IceProxyWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        IceProxyWidgetController:
        public ArmarXComponentWidgetControllerTemplate<IceProxyWidgetController>
    {
        Q_OBJECT

    public:
        /**
         * Controller Constructor
         */
        explicit IceProxyWidgetController();

        /**
         * Controller destructor
         */
        ~IceProxyWidgetController() override = default;

        /**
         * @see ArmarXWidgetController::loadSettings()
         */
        void loadSettings(QSettings* settings) override {}

        /**
         * @see ArmarXWidgetController::saveSettings()
         */
        void saveSettings(QSettings* settings) override {}

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName()
        {
            return "Meta.IceProxy";
        }

        /**
         * \see armarx::Component::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * \see armarx::Component::onConnectComponent()
         */
        void onConnectComponent() override {}

        void loadProxy(Ice::ObjectPrx prx);

    private slots:
        void on_pushButtonFinder_clicked();
        void on_pushButtonString_clicked();

    private:
        /**
         * Widget Form
         */
        Ui::IceProxyWidget widget;
        IceProxyFinder<Ice::ObjectPrx>* finder;
    };
}

