/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    ArmarXGui::gui-plugins::IceProxyWidgetController
 * \author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * \date       2016
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "IceProxyWidgetController.h"

#include <string>
#include <chrono>

#include <Ice/Communicator.h>
#include <Ice/Endpoint.h>

namespace armarx
{
    IceProxyWidgetController::IceProxyWidgetController()
    {
        widget.setupUi(getWidget());
        finder = new armarx::IceProxyFinder<Ice::ObjectPrx>(getWidget());
        widget.finderLayout->addWidget(finder);

        connect(widget.pushButtonFinder, SIGNAL(clicked()), this, SLOT(on_pushButtonFinder_clicked()));
        connect(widget.pushButtonString, SIGNAL(clicked()), this, SLOT(on_pushButtonString_clicked()));
    }

    void IceProxyWidgetController::onInitComponent()
    {
        finder->setIceManager(getIceManager());
    }

    void IceProxyWidgetController::loadProxy(Ice::ObjectPrx prx)
    {
        //catch errors for each call, since each call can fail and we want to know which call failed for what reason
        {
            std::string str{"-----"};
            try
            {
                str = prx->ice_getAdapterId();
            }
            catch (Ice::Exception& e)
            {
                ARMARX_ERROR << "Error while reading data for the current proxy (ice_getAdapterId):\n what:\n" << e.what();
            }
            widget.labelAdapterId->setText(QString::fromStdString(str));
        }
        {
            std::string str{"-----"};
            try
            {
                str = prx->ice_toString();
            }
            catch (Ice::Exception& e)
            {
                ARMARX_ERROR << "Error while reading data for the current proxy (ice_toString):\n what:\n" << e.what();
            }
            widget.labelAsString->setText(QString::fromStdString(str));
        }
        {
            std::string str{"-----"};
            try
            {
                str = prx->ice_getIdentity().category + " / " + prx->ice_getIdentity().name;
            }
            catch (Ice::Exception& e)
            {
                ARMARX_ERROR << "Error while reading data for the current proxy (ice_getIdentity):\n what:\n" << e.what();
            }
            widget.labelIdentity->setText(QString::fromStdString(str));
        }
        {
            std::string str{"-----"};
            try
            {
                std::stringstream ssend;
                auto ends = prx->ice_getEndpoints();
                for (const auto& end : ends)
                {
                    ssend << end->toString() << "\n";
                }
                if (!ends.empty())
                {
                    str = ssend.str();
                }
            }
            catch (Ice::Exception& e)
            {
                ARMARX_ERROR << "Error while reading data for the current proxy (ice_getEndpoints):\n what:\n" << e.what();
            }
            widget.labelEndpoints->setText(QString::fromStdString(str));
        }
        {
            std::string str{"-----"};
            try
            {
                std::stringstream ssctx;
                auto ctxs = prx->ice_getContext();
                for (const auto& ctx : ctxs)
                {
                    ssctx << ctx.first << "\t" << ctx.second << "\n";
                }
                if (!ctxs.empty())
                {
                    str = ssctx.str();
                }
            }
            catch (Ice::Exception& e)
            {
                ARMARX_ERROR << "Error while reading data for the current proxy (ice_getContext):\n what:\n" << e.what();
            }
            widget.labelContext->setText(QString::fromStdString(str));
        }
        {
            std::string str{"-----"};
            try
            {
                std::stringstream ssids;
                auto ids = prx->ice_ids();
                for (const auto& id : ids)
                {
                    ssids << id << "\n";
                }
                if (!ids.empty())
                {
                    str = ssids.str();
                }
            }
            catch (Ice::Exception& e)
            {
                ARMARX_ERROR << "Error while reading data for the current proxy (ice_ids):\n what:\n" << e.what();
            }
            widget.labelIds->setText(QString::fromStdString(str));
        }
        {
            std::string str{"-----"};
            try
            {
                auto start = std::chrono::high_resolution_clock::now();
                prx->ice_ping();
                auto end = std::chrono::high_resolution_clock::now();
                str = to_string(std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()) + " ms";
            }
            catch (Ice::Exception& e)
            {
                ARMARX_ERROR << "Error while reading data for the current proxy (ice_ping):\n what:\n" << e.what();
            }
            widget.labelPing->setText(QString::fromStdString(str));
        }
    }

    void armarx::IceProxyWidgetController::on_pushButtonFinder_clicked()
    {
        try
        {
            loadProxy(getIceManager()->getProxy<Ice::ObjectPrx>(finder->getSelectedProxyName().toStdString()));
        }
        catch (...)
        {
            ARMARX_ERROR << "No proxy with the name '" << finder->getSelectedProxyName().toStdString() << "' for Ice::ObjectPrx";
        }
    }

    void armarx::IceProxyWidgetController::on_pushButtonString_clicked()
    {

        try
        {
            loadProxy(getIceManager()->getCommunicator()->stringToProxy(widget.lineEdit->text().toStdString()));
        }
        catch (...)
        {
            ARMARX_ERROR << "Can't convert '" << finder->getSelectedProxyName().toStdString() << "' to Ice::ObjectPrx";
        }
    }
}
