/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    ArmarXGui::gui-plugins::WriteToDebugObserverGuiPluginWidgetController
 * \author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * \date       2020
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <string>

#include <ArmarXCore/observers/variant/Variant.h>
#include "WriteToDebugObserverGuiPluginWidgetController.h"


namespace armarx
{
    WriteToDebugObserverGuiPluginWidgetController::WriteToDebugObserverGuiPluginWidgetController()
    {
        _widget.setupUi(getWidget());
    }


    WriteToDebugObserverGuiPluginWidgetController::~WriteToDebugObserverGuiPluginWidgetController()
    {

    }


    void WriteToDebugObserverGuiPluginWidgetController::loadSettings(QSettings* settings)
    {
        _debugObserverName = settings->value("DebugObserver", "DebugObserver").toString().toStdString();
    }

    void WriteToDebugObserverGuiPluginWidgetController::saveSettings(QSettings* settings)
    {
        settings->setValue("DebugObserver", QString::fromStdString(_debugObserverName));
    }


    void WriteToDebugObserverGuiPluginWidgetController::onInitComponent()
    {
        offeringTopic(_debugObserverName);
        connect(_widget.pushButtonSend, SIGNAL(clicked()), this, SLOT(on_pushButtonSend_clicked()));
    }


    void WriteToDebugObserverGuiPluginWidgetController::onConnectComponent()
    {
        getTopic(_debugObserver, _debugObserverName);
    }

    void WriteToDebugObserverGuiPluginWidgetController::on_pushButtonSend_clicked()
    {
        const std::string channel = _widget.lineEditChannelName->text().toStdString();
        const std::string datafield = _widget.lineEditDatafield->text().toStdString();
        const std::string value = _widget.lineEditValue->text().toStdString();

        if (datafield.empty() || channel.empty())
        {
            return;
        }

        _debugObserver->setDebugDatafield(channel, datafield, new Variant{value});
    }

    QPointer<QDialog> WriteToDebugObserverGuiPluginWidgetController::getConfigDialog(QWidget* parent)
    {
        if (!_dialog)
        {
            _dialog = new SimpleConfigDialog(parent);
            _dialog->addProxyFinder<DebugObserverInterfacePrx>({"DebugObserver", "", "DebugObserver*"});
        }
        return qobject_cast<SimpleConfigDialog*>(_dialog);
    }
    void WriteToDebugObserverGuiPluginWidgetController::configured()
    {
        _debugObserverName = _dialog->getProxyName("DebugObserver");
    }
}

