/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGui::gui-plugins::WriteToDebugObserverGuiPluginWidgetController
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <ArmarXGui/libraries/SimpleConfigDialog/SimpleConfigDialog.h>
#include <ArmarXGui/gui-plugins/WriteToDebugObserverGuiPlugin/ui_WriteToDebugObserverGuiPluginWidget.h>

namespace armarx
{
    /**
    \page ArmarXGui-GuiPlugins-WriteToDebugObserverGuiPlugin WriteToDebugObserverGuiPlugin
    \brief The WriteToDebugObserverGuiPlugin allows visualizing ...

    \image html WriteToDebugObserverGuiPlugin.png
    The user can

    API Documentation \ref WriteToDebugObserverGuiPluginWidgetController

    \see WriteToDebugObserverGuiPluginGuiPlugin
    */

    /**
     * \class WriteToDebugObserverGuiPluginWidgetController
     * \brief WriteToDebugObserverGuiPluginWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        WriteToDebugObserverGuiPluginWidgetController:
        public armarx::ArmarXComponentWidgetControllerTemplate < WriteToDebugObserverGuiPluginWidgetController >
    {
        Q_OBJECT

    public:
        /**
         * Controller Constructor
         */
        explicit WriteToDebugObserverGuiPluginWidgetController();

        /**
         * Controller destructor
         */
        virtual ~WriteToDebugObserverGuiPluginWidgetController();

        /**
         * @see ArmarXWidgetController::loadSettings()
         */
        void loadSettings(QSettings* settings) override;

        /**
         * @see ArmarXWidgetController::saveSettings()
         */
        void saveSettings(QSettings* settings) override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName()
        {
            return "WriteToDebugObserverGuiPlugin";
        }

        /**
         * \see armarx::Component::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * \see armarx::Component::onConnectComponent()
         */
        void onConnectComponent() override;

        QPointer<QDialog> getConfigDialog(QWidget* parent = 0) override;
        void configured() override;

    public slots:
        /* QT slot declarations */

    signals:
        /* QT signal declarations */

    private slots:
        void on_pushButtonSend_clicked();

    private:
        /**
         * Widget Form
         */
        Ui::WriteToDebugObserverGuiPluginWidget _widget;
        std::string                             _debugObserverName;
        DebugObserverInterfacePrx               _debugObserver;
        QPointer<SimpleConfigDialog>            _dialog;
    };
}


