armarx_set_target("WriteToDebugObserverGuiPluginGuiPlugin")

armarx_build_if(ArmarXGui_FOUND "ArmarXGui not available")

set(SOURCES WriteToDebugObserverGuiPluginGuiPlugin.cpp WriteToDebugObserverGuiPluginWidgetController.cpp)
set(HEADERS WriteToDebugObserverGuiPluginGuiPlugin.h WriteToDebugObserverGuiPluginWidgetController.h)
set(GUI_MOC_HDRS ${HEADERS})
set(GUI_UIS WriteToDebugObserverGuiPluginWidget.ui)

set(COMPONENT_LIBS ArmarXCoreObservers SimpleConfigDialog)

if(ArmarXGui_FOUND)
    armarx_gui_library(WriteToDebugObserverGuiPluginGuiPlugin "${SOURCES}" "${GUI_MOC_HDRS}" "${GUI_UIS}" "" "${COMPONENT_LIBS}")
endif()
