/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGui::gui-plugins::RemoteGuiWidgetController
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXGui/gui-plugins/RemoteGuiPlugin/ui_RemoteGuiWidget.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXGui/libraries/SimpleConfigDialog/SimpleConfigDialog.h>
#include <ArmarXGui/interface/RemoteGuiInterface.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <string>
#include <mutex>
#include <map>
#include <set>

namespace armarx
{

    /**
    \page ArmarXGui-GuiPlugins-RemoteGui RemoteGui
    \brief The RemoteGui allows visualizing remotely defined widgets for configuration.

    \image html RemoteGui.png
    The user can create customized GUIs using the RemoteGuiProvider.
    This plugin will display the created GUIs.

    API Documentation \ref RemoteGuiWidgetController

    \see RemoteGuiPlugin
    \see RemoteGuiProvider
    \see RemoteGuiExample
    */

    /**
     * \class RemoteGuiWidgetController
     * \brief RemoteGuiWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        RemoteGuiWidgetController
        : public armarx::ArmarXComponentWidgetControllerTemplate < RemoteGuiWidgetController >
        , public armarx::RemoteGuiListenerInterface
    {
        Q_OBJECT

    public:
        /**
         * Controller Constructor
         */
        explicit RemoteGuiWidgetController();

        /**
         * Controller destructor
         */
        virtual ~RemoteGuiWidgetController();

        QPointer<QDialog> getConfigDialog(QWidget* parent = 0) override;
        void configured() override;

        /**
         * @see ArmarXWidgetController::loadSettings()
         */
        void loadSettings(QSettings* settings) override;

        /**
         * @see ArmarXWidgetController::saveSettings()
         */
        void saveSettings(QSettings* settings) override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName()
        {
            return "Util.RemoteGui";
        }

        // ManagedIceObject interface
        void onInitComponent() override;
        void onConnectComponent() override;

        // RemoteGuiListenerInterface interface
        void reportTabChanged(const std::string& tab, const Ice::Current&) override;
        void reportTabsRemoved(const Ice::Current&) override;
        void reportWidgetChanged(const std::string& tab, const RemoteGui::WidgetStateMap& widgetState, const Ice::Current&) override;
        void reportStateChanged(const std::string& tab, const RemoteGui::ValueMap& delta, const Ice::Current&) override;

        void onLockWidget() override;
        void onUnlockWidget() override;

    public slots:
        /* QT slot declarations */
        void onTabChanged(QString id);
        void onTabsChanged();
        void onWidgetChanged(QString id);
        void onStateChanged(QString id);

        void onGuiStateChanged();
        void onSendButtonClicked();

        void onCheckBoxGroupTabsClicked(bool checked);
        void onComboBoxTabGroupsCurrentIndexChanged(const QString& group);

    signals:
        /* QT signal declarations */
        void tabChanged(QString id);
        void tabsChanged();
        void widgetChanged(QString id);
        void stateChanged(QString id);

    private:
        void createTab(std::string const& tabId);
        void removeObsoleteTabs();
        void updateWidgets(std::string const& tabId);
        void updateState(std::string const& tabId);


        void createWidgets(std::string const& id);
        void updateValuesInWidget(std::string const& id);

        void updateValuesInState();

        void doAutoUpdate(std::string const& id);

        QWidget* createWidgetFromDescription(std::string const& tabId, RemoteGui::WidgetPtr const& desc);

    private:
        struct TabData
        {
            std::string fullName() const;
            std::string name(const std::string& pre) const;
            int findTabIndex(QTabWidget* tab) const;
            QWidget* w = nullptr;
            std::set<std::string> groups;
        };
        /**
         * Widget Form
         */
        Ui::RemoteGuiWidget widget;

        QPointer<SimpleConfigDialog> dialog;
        std::string remoteGuiProviderName;
        RemoteGuiInterfacePrx remoteGuiProvider;

        std::mutex stateMutex;
        RemoteGui::WidgetMap tabs;
        std::map<std::string, TabData> qtTabs;
        std::map<std::string, RemoteGui::WidgetMap> guiDescriptions;
        RemoteGui::TabValueMap tabValueMap;
        RemoteGui::TabValueMap tabDirtyMap;

        RemoteGui::TabWidgetStateMap tabWidgetStates;

        std::map<std::string, std::map<std::string, QWidget*>> guiWidgets;

        std::atomic<bool> internalUpdate {false};

        static constexpr auto groupAll = "<ALL>";
        static constexpr auto groupUngrouped = "<UNGROUPED>";
        std::string activeGroup = groupAll;

        std::map<std::string, std::size_t> tabsPerGroup;
    };
}
