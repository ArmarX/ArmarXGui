/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    ArmarXGui::gui-plugins::RemoteGuiWidgetController
 * \author     Fabian Paus ( fabian dot paus at kit dot edu )
 * \date       2018
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RemoteGuiWidgetController.h"

#include <ArmarXGui/libraries/RemoteGui/WidgetRegister.h>
#include <ArmarXGui/libraries/RemoteGui/Storage.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <QSpacerItem>
#include <QLayout>
#include <QGridLayout>
#include <QStyledItemDelegate>
#include <QScrollArea>

namespace armarx
{
    void clearLayout(QLayout* layout)
    {
        QLayoutItem* item;
        while ((item = layout->takeAt(0)))
        {
            if (item->layout())
            {
                clearLayout(item->layout());
                delete item->layout();
            }
            if (item->widget())
            {
                delete item->widget();
            }
            delete item;
        }
    }

    namespace
    {

        const char* const REMOTE_TAB_ID = "remote.tab_id";
        const char* const REMOTE_WIDGET_NAME = "remote.widget_name";

        struct InternalUpdateGuard
        {
            InternalUpdateGuard(std::atomic<bool>* enabled)
                : enabled(enabled)
            {
                *enabled = true;
            }

            ~InternalUpdateGuard()
            {
                *enabled = false;
            }

            std::atomic<bool>* enabled;
        };
    }

    RemoteGuiWidgetController::RemoteGuiWidgetController()
    {
        widget.setupUi(getWidget());
        widget.remoteTabWidget->clear();

        widget.comboBoxTabGroups->setVisible(false);
        widget.comboBoxTabGroups->addItem(groupAll);
        widget.comboBoxTabGroups->addItem(groupUngrouped);

        connect(widget.sendRemoteValuesButton, &QPushButton::clicked,
                this, &RemoteGuiWidgetController::onSendButtonClicked);
        connect(widget.checkBoxGroupTabs, &QCheckBox::clicked,
                this, &RemoteGuiWidgetController::onCheckBoxGroupTabsClicked);
        connect(widget.comboBoxTabGroups, QOverload<const QString&>::of(&QComboBox::currentIndexChanged),
                this, &RemoteGuiWidgetController::onComboBoxTabGroupsCurrentIndexChanged);

        connect(this, &RemoteGuiWidgetController::tabChanged,
                this, &RemoteGuiWidgetController::onTabChanged,
                Qt::QueuedConnection);
        connect(this, &RemoteGuiWidgetController::tabsChanged,
                this, &RemoteGuiWidgetController::onTabsChanged,
                Qt::QueuedConnection);
        connect(this, &RemoteGuiWidgetController::widgetChanged,
                this, &RemoteGuiWidgetController::onWidgetChanged,
                Qt::QueuedConnection);
        connect(this, &RemoteGuiWidgetController::stateChanged,
                this, &RemoteGuiWidgetController::onStateChanged,
                Qt::QueuedConnection);

        widget.comboBoxTabGroups->setCurrentIndex(0);
    }


    RemoteGuiWidgetController::~RemoteGuiWidgetController()
    {

    }

    QPointer<QDialog> RemoteGuiWidgetController::getConfigDialog(QWidget* parent)
    {
        if (!dialog)
        {
            dialog = new SimpleConfigDialog(parent);
            dialog->addProxyFinder<RemoteGuiInterfacePrx>({"RemoteGuiProvider", "", "RemoteGui*"});
        }
        return qobject_cast<SimpleConfigDialog*>(dialog);
    }

    void RemoteGuiWidgetController::configured()
    {
        remoteGuiProviderName = dialog->getProxyName("RemoteGuiProvider");
    }


    void RemoteGuiWidgetController::loadSettings(QSettings* settings)
    {
        remoteGuiProviderName = settings->value("remoteGuiProviderName", "RemoteGuiProvider").toString().toStdString();
    }

    void RemoteGuiWidgetController::saveSettings(QSettings* settings)
    {
        settings->setValue("remoteGuiProviderName", QString::fromStdString(remoteGuiProviderName));
    }


    void RemoteGuiWidgetController::onInitComponent()
    {
        usingProxy(remoteGuiProviderName);
    }


    void RemoteGuiWidgetController::onConnectComponent()
    {
        remoteGuiProvider = getProxy<RemoteGuiInterfacePrx>(remoteGuiProviderName);

        // Setup data structures
        {
            std::unique_lock<std::mutex> lock(stateMutex);
            tabs = remoteGuiProvider->getTabs();
            tabWidgetStates = remoteGuiProvider->getTabStates();
            tabValueMap = remoteGuiProvider->getValuesForAllTabs();
        }

        // Load initial values into GUI
        emit tabsChanged();

        // Subscribe to updates
        usingTopic(remoteGuiProvider->getTopicName());
    }

    void RemoteGuiWidgetController::reportTabChanged(const std::string& tab, const Ice::Current&)
    {
        {
            std::unique_lock<std::mutex> lock(stateMutex);
            tabs = remoteGuiProvider->getTabs();
            tabWidgetStates = remoteGuiProvider->getTabStates();
            tabValueMap = remoteGuiProvider->getValuesForAllTabs();
        }
        QString qTabId = QString::fromStdString(tab);
        emit tabChanged(qTabId);
    }


    void RemoteGuiWidgetController::reportTabsRemoved(const Ice::Current&)
    {
        // Setup data structures
        {
            std::unique_lock<std::mutex> lock(stateMutex);
            tabs = remoteGuiProvider->getTabs();
            tabWidgetStates = remoteGuiProvider->getTabStates();
            tabValueMap = remoteGuiProvider->getValuesForAllTabs();
        }
        emit tabsChanged();
    }

    void RemoteGuiWidgetController::reportWidgetChanged(const std::string& tab, const RemoteGui::WidgetStateMap& widgetState, const Ice::Current&)
    {
        {
            std::unique_lock<std::mutex> lock(stateMutex);
            tabWidgetStates[tab] = widgetState;
        }
        QString qTabId = QString::fromStdString(tab);
        emit widgetChanged(qTabId);
    }

    void RemoteGuiWidgetController::reportStateChanged(const std::string& tab, const RemoteGui::ValueMap& delta, const Ice::Current&)
    {
        {
            std::unique_lock<std::mutex> lock(stateMutex);
            for (const auto& pair : delta)
            {
                tabValueMap[tab][pair.first] = pair.second;
                tabDirtyMap[tab][pair.first] = pair.second;
            }
        }
        QString qTabId = QString::fromStdString(tab);
        emit stateChanged(qTabId);
    }

    void RemoteGuiWidgetController::onTabChanged(QString qid)
    {
        // The root widget of a tab was changed, so we have to create everything from scratch
        std::string id = RemoteGui::toUtf8(qid);

        std::unique_lock<std::mutex> lock(stateMutex);

        createTab(id);
        updateWidgets(id);

        removeObsoleteTabs();
    }

    void RemoteGuiWidgetController::onTabsChanged()
    {
        std::unique_lock<std::mutex> lock(stateMutex);

        // Add or update existing tabs
        for (auto& tabWidget : tabs)
        {
            QString qid = QString::fromStdString(tabWidget.first);
            std::string id = RemoteGui::toUtf8(qid);
            createTab(id);
            updateWidgets(id);
        }

        removeObsoleteTabs();
    }

    void RemoteGuiWidgetController::onWidgetChanged(QString qid)
    {
        // The display options of some widgets changed (e.g. enabled, hidden)
        std::string id = RemoteGui::toUtf8(qid);

        std::unique_lock<std::mutex> lock(stateMutex);

        updateWidgets(id);
    }

    void RemoteGuiWidgetController::onStateChanged(QString qid)
    {
        // The remotely stored values changed => update the displayed values
        std::string id = RemoteGui::toUtf8(qid);
        updateState(id);
    }

    void RemoteGuiWidgetController::onGuiStateChanged()
    {
        if (internalUpdate)
        {
            return;
        }

        QWidget* widget = qobject_cast<QWidget*>(sender());
        if (!widget)
        {
            ARMARX_WARNING << "Expected a widget as sender of onGuiStateChanged()";
            return;
        }

        std::string id = widget->property(REMOTE_TAB_ID).toString().toStdString();
        std::string name = widget->property(REMOTE_WIDGET_NAME).toString().toStdString();

        std::unique_lock<std::mutex> lock(stateMutex);

        auto varinfodump = ARMARX_STREAM_PRINTER
        {
            out << '\n' << VAROUT(id)
                << '\n' << VAROUT(name)
                << '\n' << VAROUT(GetTypeString(*widget));
        };

        ARMARX_CHECK_EXPRESSION(tabValueMap.count(id)) << varinfodump;
        ARMARX_CHECK_EXPRESSION(tabValueMap.at(id).count(name)) << varinfodump;
        ARMARX_CHECK_EXPRESSION(guiDescriptions.count(id)) << varinfodump;
        ARMARX_CHECK_EXPRESSION(guiDescriptions.at(id).count(name)) << varinfodump;

        RemoteGui::ValueVariant& currentValue = tabValueMap.at(id).at(name);

        RemoteGui::WidgetPtr const& desc = guiDescriptions.at(id).at(name);
        RemoteGui::WidgetHandler const& widgetHandler = RemoteGui::getWidgetHandler(desc);
        RemoteGui::ValueVariant newValue = widgetHandler.handleGuiChange(*desc, widget);

        if (newValue != currentValue)
        {
            currentValue = newValue;
            doAutoUpdate(id);
        }
    }

    void RemoteGuiWidgetController::onSendButtonClicked()
    {
        const int index = widget.remoteTabWidget->currentIndex();
        QString currentTabText = widget.remoteTabWidget->tabText(index);
        std::string tabId = RemoteGui::toUtf8(currentTabText);

        remoteGuiProvider->setValues(tabId, tabValueMap[tabId]);
    }

    void RemoteGuiWidgetController::createTab(std::string const& tabId)
    {
        RemoteGui::WidgetPtr widgetP = tabs.at(tabId);
        ARMARX_CHECK_EXPRESSION(widgetP);

        ARMARX_INFO << "Updating GUI for tab '" << tabId << "'";

        // Build UI elements
        QString qTabId = QString::fromStdString(tabId);
        QWidget* tabHolder = nullptr;
        for (const auto& pair : qtTabs)
        {
            TabData const& data = pair.second;
            if (data.fullName() == tabId)
            {
                tabHolder = data.w;
                break;
            }
        }
        QGridLayout* layout = nullptr;
        if (tabHolder)
        {
            // Delete all the children
            QScrollArea* scrollArea = qobject_cast<QScrollArea*>(tabHolder);
            if (scrollArea && scrollArea->widget())
            {
                layout = qobject_cast<QGridLayout*>(scrollArea->widget()->layout());
                if (layout)
                {
                    clearLayout(layout);
                }
            }
        }
        else
        {
            {
                QScrollArea* scrollArea = new QScrollArea(widget.remoteTabWidget);
                scrollArea->setObjectName(qTabId);
                scrollArea->setWidgetResizable(true);
                layout = new QGridLayout(scrollArea);
                layout->setContentsMargins(0, 0, 0, 0);
                QWidget* scrollAreaWidgetContents = new QWidget;
                scrollArea->setWidget(scrollAreaWidgetContents);
                scrollAreaWidgetContents->setLayout(layout);
                tabHolder = scrollArea;
            }

            auto& data = qtTabs[tabId];
            data.w = tabHolder;
            // extract groups
            {
                const auto fname = data.fullName();
                std::size_t idx = fname.find('_');
                while (idx != std::string::npos)
                {
                    data.groups.emplace(fname.substr(0, idx));
                    idx = fname.find('_', idx + 1);
                }
            }
            // maybe add tab
            if (activeGroup == groupAll ||
                (data.groups.empty() && activeGroup == groupUngrouped))
            {
                //ungrouped / all group uses full names
                widget.remoteTabWidget->addTab(tabHolder, qTabId);
            }
            else if (data.groups.count(activeGroup))
            {
                widget.remoteTabWidget->addTab(tabHolder, QString::fromStdString(data.name(activeGroup)));
            }
            // inc group count
            for (const auto& group : data.groups)
            {
                if (tabsPerGroup.count(group))
                {
                    ++tabsPerGroup.at(group);
                }
                else
                {
                    tabsPerGroup[group] = 1;
                    const auto box = widget.comboBoxTabGroups;
                    box->blockSignals(true);
                    box->addItem(QString::fromStdString(group));
                    box->blockSignals(false);
                }
                ARMARX_DEBUG << VAROUT(tabsPerGroup);
                ARMARX_DEBUG << VAROUT(widget.comboBoxTabGroups->maxVisibleItems());
            }
        }

        guiWidgets[tabId].clear();
        QWidget* newTab = createWidgetFromDescription(tabId, widgetP);

        newTab->setParent(tabHolder);
        layout->addWidget(newTab, 0, 0);
        layout->addItem(new QSpacerItem(1, 1, QSizePolicy::Minimum, QSizePolicy::Expanding), 1, 1);
        layout->addItem(new QSpacerItem(1, 1, QSizePolicy::Expanding, QSizePolicy::Minimum), 0, 1);
    }

    void RemoteGuiWidgetController::removeObsoleteTabs()
    {
        for (auto iter = qtTabs.begin(); iter != qtTabs.end();)
        {
            std::string const& tabId = iter->first;
            TabData const& data = iter->second;

            if (tabs.count(tabId) > 0)
            {
                ++iter;
                continue;
            }

            ARMARX_INFO << "Removing tab: " << tabId;
            QString qTabId = QString::fromStdString(tabId);

            QWidget* tabHolder = data.w;
            tabHolder->deleteLater();
            QTabWidget* tab = widget.remoteTabWidget;
            int i = data.findTabIndex(tab);
            if (i > 0)
            {
                tab->removeTab(i);
            }

            for (const std::string& group : data.groups)
            {
                std::size_t& tabCount = tabsPerGroup.at(group);
                tabCount -= 1;
                if (tabCount == 0)
                {
                    tabsPerGroup.erase(group);
                    QComboBox* box = widget.comboBoxTabGroups;
                    int boxIdx = box->findText(QString::fromStdString(group));
                    ARMARX_DEBUG << group << ' ' << VAROUT(boxIdx);
                    if (boxIdx > 1)
                    {
                        box->blockSignals(true);
                        box->setCurrentIndex(0);
                        box->removeItem(boxIdx);
                        box->blockSignals(false);
                        ARMARX_DEBUG << "REMOVING entry " << group << ' ' << VAROUT(boxIdx);
                    }
                    box->setCurrentIndex(0);
                }
            }

            iter = qtTabs.erase(iter);
            guiWidgets.erase(tabId);

            ARMARX_DEBUG << VAROUT(tabsPerGroup);
        }
    }

    void RemoteGuiWidgetController::updateWidgets(std::string const& tabId)
    {
        // External widget state update received
        RemoteGui::WidgetStateMap const& states = tabWidgetStates.at(tabId);
        std::map<std::string, QWidget*> const& widgets = guiWidgets.at(tabId);

        for (auto& nameState : states)
        {
            std::string const& name = nameState.first;
            RemoteGui::WidgetState const& state = nameState.second;
            QWidget* widget = widgets.at(name);

            widget->setHidden(state.hidden);
            widget->setDisabled(state.disabled);
        }
    }

    void RemoteGuiWidgetController::updateState(std::string const& tabId)
    {
        // External state update received
        std::unique_lock<std::mutex> lock(stateMutex);

        // Do not handle GUI updates triggered by this method (internal update only)
        InternalUpdateGuard guard(&internalUpdate);

        RemoteGui::ValueMap& values = tabDirtyMap.at(tabId);
        std::map<std::string, QWidget*> const& widgets = guiWidgets.at(tabId);
        RemoteGui::WidgetMap const& widgetDesc = guiDescriptions.at(tabId);

        for (auto& pair : values)
        {
            std::string const& name = pair.first;
            RemoteGui::ValueVariant const& value = pair.second;
            RemoteGui::WidgetPtr const& desc = widgetDesc.at(name);
            QWidget* widget = widgets.at(name);

            auto& widgetHandler = RemoteGui::getWidgetHandler(desc);
            widgetHandler.updateGui(*desc, widget, value);
        }
        tabDirtyMap.at(tabId).clear();
    }

    static void setNameProp(QWidget* w, QString const& name, QString const& id)
    {
        if (w->property(REMOTE_WIDGET_NAME).toString() == "")
        {
            w->setProperty(REMOTE_WIDGET_NAME, name);
            w->setProperty(REMOTE_TAB_ID, id);
            for (auto child :  w->findChildren<QWidget*>(QString{}, Qt::FindDirectChildrenOnly))
            {
                setNameProp(child, name, id);
            }
        }
    }

    QWidget* RemoteGuiWidgetController::createWidgetFromDescription(const std::string& tabId, const RemoteGui::WidgetPtr& desc)
    {
        QWidget* widget = nullptr;

        RemoteGui::ValueMap& values = tabValueMap[tabId];
        bool useDefaultValue = values.count(desc->name) == 0;
        RemoteGui::ValueVariant const& initialValue = useDefaultValue ? desc->defaultValue : values[desc->name];

        RemoteGui::CreateWidgetCallback createChild = [this, tabId](RemoteGui::WidgetPtr const & desc) -> QWidget *
        {
            return this->createWidgetFromDescription(tabId, desc);
        };

        {
            // Do not trigger external updates when initial values are set
            InternalUpdateGuard guard(&internalUpdate);

            auto& widgetHandler = RemoteGui::getWidgetHandler(desc);
            widget = widgetHandler.createWidget(*desc, initialValue, createChild,
                                                this, SLOT(onGuiStateChanged()));
        }

        ARMARX_CHECK_EXPRESSION(widget != nullptr);

        setNameProp(widget, QString::fromStdString(desc->name), QString::fromStdString(tabId));

        // Widgets without a name cannot be referred to later
        if (!desc->name.empty())
        {
            auto result = guiWidgets[tabId].emplace(desc->name, widget);
            bool inserted = result.second;
            if (inserted)
            {
                guiDescriptions[tabId][desc->name] = desc;
                values[desc->name] = initialValue;
            }
            else
            {
                ARMARX_WARNING << "Tried to add a widget with duplicate name '" << desc->name
                               << "' to remote tab ID '" << tabId << "'";
            }
        }

        return widget;
    }

    void RemoteGuiWidgetController::onLockWidget()
    {
        widget.widgetTopBar->setVisible(false);
        widget.remoteTabWidget->tabBar()->setVisible(false);
    }

    void RemoteGuiWidgetController::onUnlockWidget()
    {
        widget.widgetTopBar->setVisible(true);
        widget.remoteTabWidget->tabBar()->setVisible(true);
    }

    void RemoteGuiWidgetController::doAutoUpdate(std::string const& id)
    {
        if (widget.autoUpdateCheckBox->checkState() == Qt::Checked)
        {
            remoteGuiProvider->setValues(id, tabValueMap[id]);
        }
    }

    void RemoteGuiWidgetController::onCheckBoxGroupTabsClicked(bool checked)
    {
        widget.comboBoxTabGroups->setEnabled(checked);
        widget.comboBoxTabGroups->setVisible(checked);
        widget.comboBoxTabGroups->setCurrentIndex(0);
    }

    void RemoteGuiWidgetController::onComboBoxTabGroupsCurrentIndexChanged(const QString& group)
    {
        activeGroup = group.toStdString();
        auto tab = widget.remoteTabWidget;
        // Remove all without running their destructor
        while (tab->count())
        {
            tab->removeTab(0);
        }
        // Readd tabs
        for (const auto& pair : qtTabs)
        {
            TabData const& data = pair.second;

            if (activeGroup == groupAll ||
                (data.groups.empty() && activeGroup == groupUngrouped))
            {
                // Ungrouped / all group uses full names
                tab->addTab(data.w, QString::fromStdString(data.fullName()));
            }
            else if (!data.groups.empty() && data.groups.count(activeGroup))
            {
                tab->addTab(data.w, QString::fromStdString(data.name(activeGroup)));
            }
        }
    }

    std::string RemoteGuiWidgetController::TabData::fullName() const
    {
        return w->objectName().toStdString();
    }

    std::string RemoteGuiWidgetController::TabData::name(const std::string& pre) const
    {
        return fullName().substr(pre.size() + 1);
    }

    int RemoteGuiWidgetController::TabData::findTabIndex(QTabWidget* tab) const
    {
        for (auto idx = 0; idx < tab->count(); ++idx)
        {
            if (w == tab->widget(idx))
            {
                return idx;
            }
        }
        return -1;
    }
}
