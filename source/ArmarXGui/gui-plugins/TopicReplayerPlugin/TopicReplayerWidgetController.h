/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGui::gui-plugins::TopicReplayerWidgetController
 * @author     Stefan Reither ( stef dot reither at web dot de )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXGui/gui-plugins/TopicReplayerPlugin/ui_TopicReplayerWidget.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>

#include <ArmarXCore/util/topicrecording/TopicReplayer.h>

#include <QTimer>

namespace armarx
{
    /**
    \page ArmarXGui-GuiPlugins-TopicReplayer TopicReplayer
    \brief The TopicReplayer allows replaying files that contain recorded topics.

    \image html TopicReplayer.png

    \note The TopicReplayer can only replay files that have been recorded by using the storage mode "database". \n
    Files in the storage mode "file" have to be replayed by running the TopicReplayerAppRun in the command line.

    The user can...
    \li select which of the recorded topics should be replayed. <b>(1)</b>
    \li adjust the replay speed to a value between 0,01x and 10,00x. <b>(2)</b>
    \li and jump to any point of time in the replay <b>(3)</b>


    API Documentation \ref TopicReplayerWidgetController

    */

    /**
     * \class TopicReplayerWidgetController
     * \brief TopicReplayerWidgetController is the controller of the widget of the TopicReplayerGui
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        TopicReplayerWidgetController:
        public ArmarXComponentWidgetControllerTemplate<TopicReplayerWidgetController>
    {
        Q_OBJECT

    public:
        /**
         * Controller Constructor
         */
        explicit TopicReplayerWidgetController();

        /**
         * Controller destructor
         */
        ~TopicReplayerWidgetController() override;

        /**
         * @see ArmarXWidgetController::loadSettings()
         */
        void loadSettings(QSettings* settings) override;

        /**
         * @see ArmarXWidgetController::saveSettings()
         */
        void saveSettings(QSettings* settings) override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName()
        {
            return "Meta.TopicReplayer";
        }

        /**
         * \see armarx::Component::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * \see armarx::Component::onConnectComponent()
         */
        void onConnectComponent() override;

    public slots:
        /* QT slot declarations */

    signals:
        /* QT signal declarations */

    private slots:
        void loadFile();
        void loopReplay();
        void pauseReplay();
        void resumeReplay();

        void setReplaySpeed();

        void replaySpeedSliderChanged();
        void replaySpeedSpinBoxChanged();
        void replayPositionSliderChanged();
        void updateReplayPositionSlider();

        void stopSliderUpdater();
        void startSliderUpdater();

        void jumpToPosition();

    private:
        /**
         * Widget Form
         */
        Ui::TopicReplayerWidget widget;

        IceInternal::Handle<TopicReplayer> topicReplayer;
        IceUtil::Time replayLength;

        int replayPositionSliderUpdaterTimer;

        void fillListOfRecordedTopics();
        void setPausedGuiMode();
        void setPlayingGuiMode();

        void setUpReplayPositionSlider();

        void onDisconnectComponent() override;
        void timerEvent(QTimerEvent* event) override;

    };
}

