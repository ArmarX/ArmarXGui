/*

 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    ArmarXGui::gui-plugins::TopicReplayerWidgetController
 * \author     Stefan Reither ( stef dot reither at web dot de )
 * \date       2016
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "TopicReplayerWidgetController.h"


#include <QProgressDialog>
#include <qnamespace.h>
#include <string>

#include <ArmarXCore/core/ArmarXManager.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/core/ArmarXObjectScheduler.h>

#include <QFileDialog>
#include <QDateTime>
#include <QMessageBox>
#include <IceUtil/UUID.h>

#define REQUEST_TIMEKEEPER_TIME_FREQUENCY 30

namespace armarx
{
    TopicReplayerWidgetController::TopicReplayerWidgetController()
    {
        widget.setupUi(getWidget());
    }


    TopicReplayerWidgetController::~TopicReplayerWidgetController()
    {
    }


    void TopicReplayerWidgetController::loadSettings(QSettings* settings)
    {

    }

    void TopicReplayerWidgetController::saveSettings(QSettings* settings)
    {

    }


    void TopicReplayerWidgetController::onInitComponent()
    {
        this->setPausedGuiMode();
        widget.pushButton_play->setEnabled(false);
        widget.pushButton_load->setFocus();
    }


    void TopicReplayerWidgetController::onConnectComponent()
    {
        connect(widget.pushButton_load, SIGNAL(clicked()), this, SLOT(loadFile()));
        connect(widget.pushButton_play, SIGNAL(clicked()), this, SLOT(resumeReplay()));
        connect(widget.pushButton_pause, SIGNAL(clicked()), this, SLOT(pauseReplay()));
        connect(widget.checkBox_loop, SIGNAL(toggled()), this, SLOT(loopReplay()));
        connect(widget.horizontalSlider_replaySpeed, SIGNAL(valueChanged(int)), this, SLOT(replaySpeedSliderChanged()));
        connect(widget.doubleSpinBox_replaySpeed, SIGNAL(editingFinished()), this, SLOT(replaySpeedSpinBoxChanged()));

        connect(widget.horizontalSlider_replayPosition, SIGNAL(valueChanged(int)), this, SLOT(replayPositionSliderChanged()));

        connect(widget.horizontalSlider_replayPosition, SIGNAL(sliderPressed()), this, SLOT(stopSliderUpdater()));
        connect(widget.horizontalSlider_replayPosition, SIGNAL(sliderReleased()), this, SLOT(startSliderUpdater()));

        connect(widget.horizontalSlider_replayPosition, SIGNAL(sliderReleased()), this, SLOT(jumpToPosition()));
    }

    void TopicReplayerWidgetController::onDisconnectComponent()
    {
        this->killTimer(replayPositionSliderUpdaterTimer);
        if (topicReplayer)
        {
            getArmarXManager()->removeObjectNonBlocking(topicReplayer->getName());
        }
    }

    void armarx::TopicReplayerWidgetController::loadFile()
    {
        widget.pushButton_load->setEnabled(false);

        QFileDialog fileDialog(getWidget(), "Open Topic Recording File");
        fileDialog.setFileMode(QFileDialog::ExistingFile);
        QStringList filters;
        filters << tr("TopicRecorder files (*.bag)");
        filters << tr("All files (*.*)");
        fileDialog.setNameFilters(filters);


        fileDialog.setViewMode(QFileDialog::Detail);
        fileDialog.setVisible(true);

        QString fileName;
        if (fileDialog.exec() == QDialog::Accepted)
        {
            fileName = fileDialog.selectedFiles().first();
            if (topicReplayer)
            {
                getArmarXManager()->removeObjectBlocking(topicReplayer->getName());
            }

            if (!fileName.isEmpty())
            {
                Ice::PropertiesPtr properties = Ice::createProperties();
                properties->setProperty("ArmarX.TopicReplayer.RecordFile", fileName.toStdString());
                topicReplayer = armarx::Component::create<TopicReplayer>(properties);
                topicReplayer->setAutoplay(false);
                widget.doubleSpinBox_replaySpeed->setValue(1.0);
                QProgressDialog dProgress;
                dProgress.setMaximum(0);
                dProgress.setMinimum(0);
                getArmarXManager()->addObject(topicReplayer, false, topicReplayer->getDefaultName() + IceUtil::generateUUID());
                dProgress.show();
                // waiting for TopicReplayer to set up
                while (topicReplayer->getObjectScheduler()->getObject()->getState() < eManagedIceObjectStarted)
                {
                    usleep(100000);
                }

                // Checks whether opened file has database-format, if not, abort
                if (topicReplayer->getReplayLength().toMilliSeconds() == 0)
                {
                    this->setPausedGuiMode();
                    widget.pushButton_play->setEnabled(false);
                    widget.lineEdit_fileName->setText("");
                    widget.horizontalSlider_replayPosition->setValue(0);
                    widget.horizontalSlider_replayPosition->setEnabled(false);
                    widget.horizontalSlider_replaySpeed->setEnabled(false);
                    widget.listWidget_topics->clear();

                    QMessageBox msg;
                    msg.setWindowTitle("TopicReplayerGui");
                    msg.setText("The format of the selected file is not supported.");
                    msg.setIcon(QMessageBox::Warning);
                    msg.exec();
                }
                else
                {
                    widget.lineEdit_fileName->setText(fileName);
                    this->fillListOfRecordedTopics();
                    this->setUpReplayPositionSlider();
                    widget.pushButton_play->setEnabled(true);
                    widget.horizontalSlider_replayPosition->setEnabled(true);
                    widget.horizontalSlider_replaySpeed->setEnabled(true);

                    replayPositionSliderUpdaterTimer = this->startTimer(REQUEST_TIMEKEEPER_TIME_FREQUENCY);
                }
            }
        }

        widget.pushButton_load->setEnabled(true);
    }

    void armarx::TopicReplayerWidgetController::pauseReplay()
    {
        this->setPausedGuiMode();

        if (topicReplayer)
        {
            topicReplayer->pauseReplay();
        }
    }

    void armarx::TopicReplayerWidgetController::loopReplay()
    {
        // if (topicReplayer)
        // {
        //     topicReplayer->setLoop(widget.checkBox_loop->isChecked());
        // }
    }

    void armarx::TopicReplayerWidgetController::resumeReplay()
    {
        if (topicReplayer && !(this->replayLength.toMilliSeconds() == widget.horizontalSlider_replayPosition->value()))
        {
            std::vector<std::string> replayingTopics;
            for (int i = 0; i < widget.listWidget_topics->count(); i++)
            {
                auto item = widget.listWidget_topics->item(i);
                if (item->checkState() == Qt::Checked)
                {
                    replayingTopics.push_back(item->text().toStdString());
                }
            }
            topicReplayer->setReplayingTopics(replayingTopics);

            setReplaySpeed();
            topicReplayer->resumeReplay();

            this->setPlayingGuiMode();
        }
    }

    void armarx::TopicReplayerWidgetController::setReplaySpeed()
    {
        if (topicReplayer)
        {
            topicReplayer->setReplaySpeed(widget.doubleSpinBox_replaySpeed->value());
        }
    }

    void armarx::TopicReplayerWidgetController::replaySpeedSliderChanged()
    {
        updateReplayPositionSlider();

        double spinBoxValue = 0.0;
        int sliderValue = widget.horizontalSlider_replaySpeed->value();
        int m = widget.horizontalSlider_replaySpeed->maximum() / 2;

        if (sliderValue < m)
        {
            spinBoxValue = (double) sliderValue / (double) m;
        }
        else
        {
            spinBoxValue = 1 + 9.0 * ((double) sliderValue / (double) m - 1);
        }

        widget.doubleSpinBox_replaySpeed->setValue(spinBoxValue);
        setReplaySpeed();
    }

    void TopicReplayerWidgetController::replaySpeedSpinBoxChanged()
    {
        double spinBoxValue = widget.doubleSpinBox_replaySpeed->value();
        int sliderValue = 0;
        int m = widget.horizontalSlider_replaySpeed->maximum() / 2;

        if (spinBoxValue <= 1.0)
        {
            sliderValue = (int)(spinBoxValue * m);
        }
        else
        {
            sliderValue = (int)(m * (1 + (spinBoxValue - 1) / 9));
        }

        widget.horizontalSlider_replaySpeed->setValue(sliderValue);
        setReplaySpeed();
    }


    void TopicReplayerWidgetController::fillListOfRecordedTopics()
    {
        QListWidget* topicsListWidget = widget.listWidget_topics;
        topicsListWidget->clear();

        const Qt::CheckState checkState = widget.checkBoxLoadTopicsEnabled->checkState();

        if (this->topicReplayer)
        {
            std::vector<std::string> recordedTopics = topicReplayer->getRecordedTopics();
            std::sort(recordedTopics.begin(), recordedTopics.end());

            for (std::string topic : recordedTopics)
            {
                QListWidgetItem* item = new QListWidgetItem;
                item->setData(Qt::DisplayRole, QString::fromStdString(topic));
                item->setData(Qt::CheckStateRole, checkState);
                topicsListWidget->addItem(item);
            }
        }
    }

    void TopicReplayerWidgetController::setPausedGuiMode()
    {
        widget.pushButton_pause->setEnabled(false);

        widget.pushButton_load->setEnabled(true);
        widget.pushButton_play->setEnabled(true);
        widget.pushButton_play->setFocus();
        widget.listWidget_topics->setEnabled(true);

        this->stopSliderUpdater();
    }

    void TopicReplayerWidgetController::setPlayingGuiMode()
    {
        widget.pushButton_load->setEnabled(false);
        widget.listWidget_topics->setEnabled(false);
        widget.pushButton_play->setEnabled(false);

        widget.pushButton_pause->setEnabled(true);
        widget.pushButton_pause->setFocus();

        widget.horizontalSlider_replayPosition->setEnabled(true);
        widget.horizontalSlider_replaySpeed->setEnabled(true);

        this->startSliderUpdater();
    }

    void TopicReplayerWidgetController::setUpReplayPositionSlider()
    {
        if (topicReplayer)
        {
            replayLength = topicReplayer->getReplayLength();
            QTime t(0, 0, 0, 0);
            t = t.addMSecs((int)replayLength.toMilliSeconds());
            widget.label_replayLength->setText(t.toString("h:mm:ss.zzz"));
            widget.horizontalSlider_replayPosition->setMaximum((int) replayLength.toMilliSeconds());
            widget.horizontalSlider_replayPosition->setTickInterval((int) replayLength.toMilliSeconds() / 100);
            widget.horizontalSlider_replayPosition->setSingleStep((int) replayLength.toMilliSeconds() / 100);
            widget.horizontalSlider_replayPosition->setValue(0);
        }
    }

    void TopicReplayerWidgetController::updateReplayPositionSlider()
    {
        if (topicReplayer)
        {
            int currentTime = (int) topicReplayer->getCurrentTimePosition().toMilliSeconds();
            widget.horizontalSlider_replayPosition->setValue(currentTime);
            if (currentTime >= replayLength.toMilliSeconds())
            {
                this->pauseReplay();

                if (widget.checkBox_loop->isChecked())
                {
                    widget.horizontalSlider_replayPosition->setSliderPosition(0);
                    jumpToPosition();
                    resumeReplay();
                }
            }
        }
    }

    void TopicReplayerWidgetController::replayPositionSliderChanged()
    {
        QTime t(0, 0, 0, 0);
        t = t.addMSecs(widget.horizontalSlider_replayPosition->value());
        widget.label_replayPosition->setText(t.toString("h:mm:ss.zzz"));
    }


    void TopicReplayerWidgetController::timerEvent(QTimerEvent* event)
    {
        if (event->timerId() == this->replayPositionSliderUpdaterTimer)
        {
            this->updateReplayPositionSlider();
        }
    }

    void TopicReplayerWidgetController::stopSliderUpdater()
    {
        this->killTimer(replayPositionSliderUpdaterTimer);
    }

    void TopicReplayerWidgetController::startSliderUpdater()
    {
        replayPositionSliderUpdaterTimer = this->startTimer(REQUEST_TIMEKEEPER_TIME_FREQUENCY);
    }

    void TopicReplayerWidgetController::jumpToPosition()
    {
        if (topicReplayer)
        {
            topicReplayer->jumpToPosition(IceUtil::Time().milliSeconds(widget.horizontalSlider_replayPosition->value()));
        }
    }
}
