/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include "JsonData.h"

#include <memory>

namespace armarx
{
    class JsonValue;
    using JsonValuePtr = std::shared_ptr<JsonValue>;

    class StructuralJsonParser;

    class JsonValue : public JsonData
    {
        friend class StructuralJsonParser;
    public:
        enum Type { eString, eNumber, eBool, eNull };

        JsonValue(const std::string& value);
        JsonValue(int value);
        JsonValue(long value);
        JsonValue(float value);
        JsonValue(double value);

        static JsonValuePtr Null();
        static JsonValuePtr True();
        static JsonValuePtr False();
        static JsonValuePtr Create(const std::string& value);
        static JsonValuePtr Create(int value);
        static JsonValuePtr Create(long value);
        static JsonValuePtr Create(float value);
        static JsonValuePtr Create(double value);

        static JsonValuePtr CreateRaw(Type type, const std::string& value);
        static bool CheckValue(Type type, const std::string& value);

        static bool CheckNumber(const std::string& value);
        static bool CheckInt(const std::string& value);
        static bool CheckBool(const std::string& value);
        static bool CheckNull(const std::string& value);


        void writeJson(const JsonWriterPtr& writer) override;

        JsonValuePtr toSharedPtr() const;

        static std::string ToString(int value);
        static std::string ToString(long value);
        static std::string ToString(float value);
        static std::string ToString(double value);

        std::string asString() const;
        float asFloat() const;
        int asInt() const;
        bool asBool() const;

        std::string rawValue();

        JsonDataPtr clone() override;

        Type getType();

    private:
        JsonValue(Type type, const std::string& value);
        Type type;
        std::string value;

    };
}

