/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "JsonWriter.h"

#include <ArmarXCore/core/exceptions/Exception.h>

#include <iomanip>
#include <boost/regex.hpp>

namespace armarx
{
    JsonWriter::JsonWriter(int indenting, const std::string& indentChars, bool jsStyleKeys)
        : indenting(indenting), nesting(0), indentChars(indentChars), jsStyleKeys(jsStyleKeys)
    {
    }

    void JsonWriter::startObject()
    {
        beginValue();

        stack.push(eEmptyObject);
        ss << "{";
        nesting++;
    }

    void JsonWriter::endObject()
    {
        if (stack.size() == 0)
        {
            throw LocalException("Stack is empty");
        }

        if (stack.top() != eEmptyObject && stack.top() != eObject)
        {
            throw LocalException("Wrong type");
        }

        nesting--;
        endArrayOrObject();
        stack.pop();
        ss << "}";
        endValue();
    }

    void JsonWriter::writeKey(const std::string& key)
    {
        beginKey();
        if (jsStyleKeys && isId(key))
        {
            ss << key << (indenting == 2 ? ": " : ":");
        }
        else
        {
            ss << EscapeQuote(key) << (indenting == 2 ? ": " : ":");
        }
        endKey();
    }

    void JsonWriter::startArray()
    {
        beginValue();
        stack.push(eEmptyArray);
        ss << "[";
        nesting++;
    }

    void JsonWriter::endArray()
    {
        if (stack.size() == 0)
        {
            throw LocalException("Stack is empty");
        }

        if (stack.top() != eEmptyArray && stack.top() != eArray)
        {
            throw LocalException("Wrong type");
        }

        nesting--;
        endArrayOrObject();
        stack.pop();
        ss << "]";
        endValue();
    }

    void JsonWriter::writeRawValue(const std::string& value)
    {
        beginValue();
        ss << value;
        endValue();
    }

    std::string JsonWriter::toString()
    {
        return ss.str();
    }

    bool JsonWriter::isId(const std::string& str)
    {
        boost::cmatch result;
        return boost::regex_search(str.c_str(), result, boost::regex("^[_A-Za-z]\\w*$"), boost::regex_constants::match_default);
    }

    std::string JsonWriter::EscapeQuote(const std::string& str)
    {
        return "\"" + Escape(str) + "\"";
    }

    std::string JsonWriter::Escape(const std::string& str)
    {
        std::ostringstream ss;

        for (auto iter = str.cbegin(); iter != str.cend(); iter++)
        {
            switch (*iter)
            {
                case '\\':
                    ss << "\\\\";
                    break;

                case '"':
                    ss << "\\\"";
                    break;

                case '\b':
                    ss << "\\b";
                    break;

                case '\f':
                    ss << "\\f";
                    break;

                case '\n':
                    ss << "\\n";
                    break;

                case '\r':
                    ss << "\\r";
                    break;

                case '\t':
                    ss << "\\t";
                    break;

                default:
                    if (IsControlChar(*iter))
                    {
                        ss << "\\u" << std::hex << std::uppercase << std::setfill('0')  << std::setw(4) << static_cast<int>(*iter);
                    }
                    else
                    {
                        ss << *iter;
                    }

                    break;
            }
        }

        return ss.str();
    }

    void JsonWriter::writeNewLine()
    {
        if (indenting == 2)
        {
            ss << "\n";

            for (int i = 0; i < nesting; i++)
            {
                ss << indentChars;
            }
        }
    }

    void JsonWriter::beginValue()
    {
        if (stack.size() == 0)
        {
            return;
        }

        if (stack.top() == eArray)
        {
            ss << (indenting == 1 ? ", " : ",");
        }

        if (stack.top() != eObjectKey)
        {
            writeNewLine();
        }

        if (stack.top() != eEmptyArray && stack.top() != eArray && stack.top() != eObjectKey)
        {
            throw LocalException("Wrong type");
        }
    }

    void JsonWriter::beginKey()
    {
        if (stack.size() == 0)
        {
            throw LocalException("Stack is empty");
        }

        if (stack.top() != eEmptyObject && stack.top() != eObject)
        {
            throw LocalException("Wrong type");
        }

        if (stack.top() == eObject)
        {
            ss << (indenting == 1 ? ", " : ",");
        }

        writeNewLine();
    }

    void JsonWriter::endKey()
    {
        stack.pop();
        stack.push(eObjectKey);
    }

    void JsonWriter::endValue()
    {
        if (stack.size() == 0)
        {
            stack.push(eFinal);
        }
        else if (stack.top() == eEmptyArray)
        {
            stack.top() = eArray;
        }
        else if (stack.top() == eObjectKey)
        {
            stack.top() = eObject;
        }
    }

    void JsonWriter::endArrayOrObject()
    {
        if (stack.top() == eArray || stack.top() == eObject)
        {
            writeNewLine();
        }
    }
}
