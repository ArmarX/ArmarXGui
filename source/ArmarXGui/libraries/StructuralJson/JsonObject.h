/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include "JsonArray.h"
#include "JsonData.h"
#include "JsonValue.h"

#include <memory>
#include <vector>
#include <utility>

namespace armarx
{
    class JsonObject;
    using JsonObjectPtr = std::shared_ptr<JsonObject>;

    class JsonObject : public JsonData
    {
        friend class JPathNavigator;

    public:
        JsonObject();
        void writeJson(const JsonWriterPtr& writer) override;
        void add(const std::string& key, const JsonDataPtr& value);
        void add(const std::string& key, const JsonValue& value);
        void add(const std::string& key, const JsonArray& value);
        void set(const std::string& key, const JsonDataPtr& value);
        void set(const std::string& key, const JsonValue& value);
        bool remove(const std::string& key);
        JsonDataPtr get(const std::string& key);
        uint size();
        void clear();
        JsonObjectPtr toSharedPtr() const;
        JsonDataPtr clone() override;
        std::vector<std::string> getKeys() const;

    private:
        std::vector<std::pair<std::string, JsonDataPtr>> elements;
        int getKeyIndex(const std::string& key);
    };
}

