/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "SimpleLexer.h"

namespace armarx
{
    SimpleLexer::SimpleLexer(std::string src, int tokenFin, int tokenError)
    {
        this->pos = 0;
        this->lastPos = 0;
        this->src = src;
        this->tokenFin = tokenFin;
        this->tokenError = tokenError;
    }

    void SimpleLexer::addRule(int id, const std::string& name, const std::string& regex)
    {
        rules.push_back(Rule(id, name, boost::regex("\\A(" + regex + ")")));
    }

    int SimpleLexer::nextToken()
    {
        if (pos >= (int)src.size())
        {
            currentTokenValue = "";
            currentTokenId = tokenFin;
            lastPos = pos;
            return currentTokenId;
        }

        for (const Rule& rule : rules)
        {
            boost::cmatch result;

            if (boost::regex_search(src.c_str() + pos, result, rule.regex, boost::regex_constants::match_default))
            {
                currentTokenValue = std::string(result[0].first, result[0].second);
                currentTokenId = rule.id;
                lastPos = pos;
                pos += currentTokenValue.size();
                return currentTokenId;
            }
        }

        currentTokenValue = "";
        currentTokenId = tokenError;
        lastPos = pos;
        return currentTokenId;
    }

    void SimpleLexer::reset()
    {
        this->pos = 0;
        this->lastPos = 0;
        currentTokenValue = "";
        currentTokenId = tokenError;
    }

    std::string SimpleLexer::tokenIdToName(int id)
    {
        if (id == tokenError)
        {
            return "Error";
        }

        if (id == tokenFin)
        {
            return "EndOfString";
        }

        for (Rule rule : rules)
        {
            if (rule.id == id)
            {
                return rule.name;
            }
        }

        return "";
    }

    void SimpleLexer::positionToLineAndColumn(int pos, int& line, int& column)
    {
        if (pos < 0 || pos > (int)src.length())
        {
            line = 0;
            column = 0;
            return;
        }

        line = 1; // 1 based counting
        column = 0;
        int lineStart = 0;

        for (int i = 0; i < (int)src.length(); i++)
        {
            if (i == pos)
            {
                column = i - lineStart + 1; // 1 based counting
                return;
            }

            if (src[i] == '\n')
            {
                line++;
                lineStart = i + 1; // line starts after \n
            }
        }

        column = pos - lineStart; // pos == src.length(), no +1 needed here

    }

    LexerInfo SimpleLexer::getPositionInfo(int pos)
    {
        int line, column;
        positionToLineAndColumn(pos, line, column);
        return LexerInfo(pos, line, column);
    }
}
