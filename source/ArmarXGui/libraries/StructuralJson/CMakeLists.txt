armarx_set_target("Gui Library: StructuralJson")

set(LIB_NAME StructuralJson)


set(LIBS ArmarXCore)

set(LIB_FILES JPathNavigator.cpp
              JsonData.cpp
              JsonValue.cpp
              JsonArray.cpp
              JsonObject.cpp
              JsonWriter.cpp
              LexerInfo.cpp
              SimpleLexer.cpp
              StructuralJsonParser.cpp
              )

set(LIB_HEADERS JPathNavigator.h
                JsonData.h
                JsonValue.h
                JsonArray.h
                JsonObject.h
                JsonWriter.h
                LexerInfo.h
                SimpleLexer.h
                StructuralJsonParser.h
                )

armarx_add_library("${LIB_NAME}"  "${LIB_FILES}" "${LIB_HEADERS}" "${LIBS}")

add_subdirectory(test)
