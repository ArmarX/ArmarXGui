/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/


#include "StructuralJsonParser.h"

#include "SimpleLexer.h"

namespace armarx
{
    StructuralJsonParser::StructuralJsonParser(const std::string& src, bool acceptSingleValue, bool accceptJsStyleKeys)
        : lexer(new SimpleLexer(src)), acceptSingleValue(acceptSingleValue), accceptJsStyleKeys(accceptJsStyleKeys), previousTokenEndOffset(-1, -1, -1)
    {
        // regex taken from http://stackoverflow.com/questions/2583472/regex-to-validate-json

        lexer->addRule(eWhitespace, "Whitespace", "[\\r\\n\\s]+");
        lexer->addRule(eOpeningCurlyBracket, "{", "\\{");
        lexer->addRule(eClosingCurlyBracket, "}", "\\}");
        lexer->addRule(eOpeningSquareBracket, "[", "\\[");
        lexer->addRule(eClosingSquareBracket, "]", "\\]");
        lexer->addRule(eComma, ",", ",");
        lexer->addRule(eColon, ":", "\\:");
        lexer->addRule(eNumber, "Number", "-?\\d+(\\.\\d+)?([eE][+-]?\\d+)?"); // also accept leading 0
        lexer->addRule(eString, "String", "\"([^\\\\\\\"]|\\\\.)*\""); // use very simpe string parsing
        lexer->addRule(eBoolean, "Boolean", "true|false");
        lexer->addRule(eNull, "Null", "null");
        lexer->addRule(eId, "ID", "[_A-Za-z]\\w*"); // add ID last to avoid detecting true|false|null as ID

    }

    StructuralJsonParser::~StructuralJsonParser()
    {

    }

    void StructuralJsonParser::parse()
    {
        lexer->reset();
        next();
        parsedJson = p_json();

        if (!iserr())
        {
            while (lexer->currentTokenId == eWhitespace)
            {
                lexer->nextToken();
            }

            if (lexer->currentTokenId != lexer->tokenFin)
            {
                error("end of string");
            }
        }
    }

    void StructuralJsonParser::next()
    {
        lexer->nextToken();
        previousTokenEndOffset = lexer->getPositionInfo(lexer->pos);

        while (lexer->currentTokenId == eWhitespace)
        {
            lexer->nextToken();
        }
    }

    JsonDataPtr StructuralJsonParser::p_json()
    {
        if (acceptSingleValue)
        {
            return p_item();
        }
        else if (lexer->currentTokenId == eOpeningCurlyBracket)
        {
            return p_object();
        }
        else if (lexer->currentTokenId == eOpeningSquareBracket)
        {
            return p_array();
        }
        else
        {
            return error("{ or [");
        }
    }

    JsonDataPtr StructuralJsonParser::p_object()
    {
        JsonObjectPtr o(new JsonObject());
        o->setLexerStartOffset(lexer->getPositionInfo(lexer->pos));
        next(); // consume {

        if (lexer->currentTokenId == eClosingCurlyBracket)
        {
            next(); // consume }
            o->setLexerEndOffset(previousTokenEndOffset);
            return o;
        }

        if (!accceptJsStyleKeys && lexer->currentTokenId != eString)
        {
            return error("String or '}'", o);
        }
        else if (accceptJsStyleKeys && lexer->currentTokenId != eString && lexer->currentTokenId != eId)
        {
            return error("String, ID or '}'", o);
        }

        p_pair(o);

        if (iserr())
        {
            return o;
        }

        while (lexer->currentTokenId == eComma)
        {
            next(); // consume ,
            p_pair(o);

            if (iserr())
            {
                return o;
            }
        }

        if (lexer->currentTokenId == eClosingCurlyBracket)
        {
            next(); // consume }
            o->setLexerEndOffset(previousTokenEndOffset);
            return o;
        }
        else
        {
            return error("',' or '}'", o);
        }
    }

    void StructuralJsonParser::p_pair(JsonObjectPtr o)
    {
        if (!accceptJsStyleKeys && lexer->currentTokenId != eString)
        {
            error("String");
            return;
        }
        else if (accceptJsStyleKeys && lexer->currentTokenId != eString && lexer->currentTokenId != eId)
        {
            error("String or ID");
            return;
        }

        std::string key;

        if (accceptJsStyleKeys && lexer->currentTokenId == eId)
        {
            key = lexer->currentTokenValue;
        }
        else if (!DequoteString(lexer->currentTokenValue, key))
        {
            stringerror(key);
            return;
        }

        next(); // consume key

        if (lexer->currentTokenId != eColon)
        {
            o->add(key, JsonDataPtr());
            error("':'");
            return;
        }

        next(); // consue :
        JsonDataPtr value = p_item();
        o->add(key, value);

        if (iserr())
        {
            return;
        }

    }

    JsonDataPtr StructuralJsonParser::p_array()
    {
        JsonArrayPtr a(new JsonArray());
        a->setLexerStartOffset(lexer->getPositionInfo(lexer->pos));
        next(); // consume [

        if (lexer->currentTokenId == eClosingSquareBracket)
        {
            next(); // consume ]
            a->setLexerEndOffset(previousTokenEndOffset);
            return a;
        }

        int posBeforeItem = lexer->pos;
        JsonDataPtr item1 = p_item();
        a->add(item1);

        if (iserr())
        {
            if (posBeforeItem == lexer->pos)
            {
                // No Token was consumed by p_item() => add ']' to expected tokens in error message.
                return error("']', " + expected, a);
            }
            else
            {
                return a;
            }
        }

        while (lexer->currentTokenId == eComma)
        {
            next(); // consume ,
            JsonDataPtr item = p_item();
            a->add(item);

            if (iserr())
            {
                return a;
            }
        }

        if (lexer->currentTokenId == eClosingSquareBracket)
        {
            next(); // consume ]
            a->setLexerEndOffset(previousTokenEndOffset);
            return a;
        }
        else
        {
            return error(", or ]", a);
        }
    }

    // Item = String | Number | true | false | null | Object | Array
    JsonDataPtr StructuralJsonParser::p_item()
    {
        switch (lexer->currentTokenId)
        {
            case eString:
            case eNumber:
            case eBoolean:
            case eNull:
                return p_leaf();

            case eOpeningCurlyBracket:
                return p_object();

            case eOpeningSquareBracket:
                return p_array();
                break;

            default:
                return error("'{', '[', String, Number, 'true', 'false' or 'null'");
        }

    }

    JsonDataPtr StructuralJsonParser::p_leaf()
    {
        JsonValuePtr value;
        std::string val;

        switch (lexer->currentTokenId)
        {
            case eString:

                if (!DequoteString(lexer->currentTokenValue, val))
                {
                    return stringerror(val);
                }

                value.reset(new JsonValue(JsonValue::eString, val));
                break;

            case eNumber:
                value.reset(new JsonValue(JsonValue::eNumber, lexer->currentTokenValue));
                break;

            case eBoolean:
                value.reset(new JsonValue(JsonValue::eBool, lexer->currentTokenValue));
                break;

            case eNull:
                value.reset(new JsonValue(JsonValue::eNull, lexer->currentTokenValue));
                break;

            default:
                return error("String, Number, 'true', 'false' or 'null'");
        }

        value->setLexerStartOffset(lexer->getPositionInfo(lexer->pos));
        next(); // consume eString | eNumber | eBoolean | eNull
        value->setLexerEndOffset(previousTokenEndOffset);
        return value;
    }

    JsonDataPtr StructuralJsonParser::error(const std::string& expected, const JsonDataPtr& toReturn)
    {
        this->err = "Unexpected Token " + lexer->tokenIdToName(lexer->currentTokenId) + " at " + getlongerrposstr() + ". Expecting " + expected + ".";
        this->expected = expected;
        return toReturn;
    }

    JsonDataPtr StructuralJsonParser::stringerror(const std::string& error, const JsonDataPtr& toReturn)
    {
        this->err = "invalid string at " + getlongerrposstr() + ":" + error + ".";
        this->expected = expected;
        return toReturn;
    }

    bool StructuralJsonParser::iserr()
    {
        return err.length() > 0;
    }

    std::string StructuralJsonParser::geterr()
    {
        return err;
    }

    void StructuralJsonParser::geterrpos(int& line, int& col)
    {
        lexer->positionToLineAndColumn(lexer->lastPos, line, col);
    }

    std::string StructuralJsonParser::geterrposstr()
    {
        int line, col;
        lexer->positionToLineAndColumn(lexer->lastPos, line, col);
        return "L" + std::to_string(line) + " C" + std::to_string(col);
    }

    std::string StructuralJsonParser::getlongerrposstr()
    {
        int line, col;
        lexer->positionToLineAndColumn(lexer->lastPos, line, col);
        return "line " + std::to_string(line) + " column " + std::to_string(col);
    }

    bool StructuralJsonParser::DequoteString(std::string s, std::string& res)
    {
    // *INDENT-OFF*
    res = "";
    std::string::const_iterator it = s.begin();
    std::string::const_iterator end = s.end() - 1;

    if (s.length() < 1 || *it != '"')
    {
        res = "Missing starting \"";
        return false;
    }

    it++; // consume starting "

    if (s.length() < 2 || *end != '"')
    {
        res = "Missing ending \"";
        return false;
    }

    while (it < end)
    {
        char c = *it++;

        if (c == '\\' && it != s.end())
        {
            switch (*it++)
            {
                case '\\':
                    res += '\\';
                    break;

                case '/':
                    res += '/';
                    break;

                case '"':
                    res += '"';
                    break;

                case 'b':
                    res += '\b';
                    break;

                case 'f':
                    res += '\f';
                    break;

                case 'n':
                    res += '\n';
                    break;

                case 'r':
                    res += '\r';
                    break;

                case 't':
                    res += '\t';
                    break;

                case 'u':
                    res = "\\u is not supported";
                    return false;

                default:
                    res = "invalid escape sequence";
                    return false;
            }
        }
        else
        {
            res += c;
        }

    }

    return true;
    // *INDENT-ON*
    }
}
