/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include "JsonData.h"
#include "JsonValue.h"

#include <memory>
#include <vector>

namespace armarx
{
    class JsonArray;
    using JsonArrayPtr = std::shared_ptr<JsonArray>;

    class JsonArray : public JsonData
    {
        friend class JPathNavigator;

    public:
        JsonArray();
        JsonArray(const std::vector<JsonValue>& values);
        void writeJson(const JsonWriterPtr& writer) override;
        void add(const JsonDataPtr& value);
        void add(const JsonValue& value);
        void add(const JsonArray& value);
        void set(uint index, const JsonDataPtr& value);
        void set(uint index, const JsonValue& value);
        void remove(uint index);
        uint size();
        void clear();
        JsonArrayPtr toSharedPtr() const;
        JsonDataPtr clone() override;

    private:
        std::vector<JsonDataPtr> elements;
    };
}

