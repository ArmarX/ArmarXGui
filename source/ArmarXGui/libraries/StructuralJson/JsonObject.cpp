/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "JsonObject.h"

#include <ArmarXCore/core/exceptions/Exception.h>

namespace armarx
{
    JsonObject::JsonObject()
    {
    }

    void JsonObject::writeJson(const JsonWriterPtr& writer)
    {
        writer->startObject();

        for (std::pair<std::string, JsonDataPtr> pair : elements)
        {
            writer->writeKey(pair.first);
            pair.second->writeJson(writer);
        }

        writer->endObject();
    }


    void armarx::JsonObject::add(const std::string& key, const armarx::JsonDataPtr& value)
    {
        if (getKeyIndex(key) >= 0)
        {
            throw LocalException("Key \"") << key << "\" exists already in this json object.";
        }

        elements.push_back(std::make_pair(key, value));
    }

    void JsonObject::add(const std::string& key, const JsonValue& value)
    {
        add(key, value.toSharedPtr());
    }

    void JsonObject::add(const std::string& key, const JsonArray& value)
    {
        add(key, value.toSharedPtr());
    }

    void JsonObject::set(const std::string& key, const JsonDataPtr& value)
    {
        int index = getKeyIndex(key);

        if (index < 0)
        {
            elements.push_back(std::make_pair(key, value));
        }
        else
        {
            elements.at(index).second = value;
        }
    }

    void JsonObject::set(const std::string& key, const JsonValue& value)
    {
        JsonDataPtr valuePtr(new JsonValue(value));
        set(key, valuePtr);
    }

    bool JsonObject::remove(const std::string& key)
    {
        int index = getKeyIndex(key);

        if (index >= 0)
        {
            elements.erase(elements.begin() + 1);
        }

        return index >= 0;
    }

    JsonDataPtr JsonObject::get(const std::string& key)
    {
        int index = getKeyIndex(key);

        if (index < 0)
        {
            return JsonDataPtr();
        }
        else
        {
            return elements.at(index).second;
        }
    }

    uint JsonObject::size()
    {
        return elements.size();
    }

    void JsonObject::clear()
    {
        elements.clear();
    }

    JsonObjectPtr JsonObject::toSharedPtr() const
    {
        JsonObjectPtr ptr(new JsonObject(*this));
        return ptr;
    }

    JsonDataPtr JsonObject::clone()
    {
        JsonObjectPtr o(new JsonObject);
        for (const std::pair<std::string, JsonDataPtr>& pair : elements)
        {
            o->add(pair.first, pair.second->clone());
        }
        return o;
    }

    std::vector<std::string> JsonObject::getKeys() const
    {
        std::vector<std::string> v;
        for (uint i = 0; i < elements.size(); i++)
        {
            v.push_back(elements.at(i).first);
        }
        return v;
    }

    int JsonObject::getKeyIndex(const std::string& key)
    {
        for (uint i = 0; i < elements.size(); i++)
        {
            if (elements.at(i).first == key)
            {
                return i;
            }
        }

        return -1;
    }
}
