/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "JsonData.h"

namespace armarx
{
    JsonData::JsonData()
        : lexerStartOffset(-1, -1, -1), lexerEndOffset(-1, -1, -1)
    {
    }

    std::string JsonData::toJsonString(int indenting, const std::string indentChars, bool jsStyleKeys)
    {
        JsonWriterPtr writer(new JsonWriter(indenting, indentChars, jsStyleKeys));
        writeJson(writer);
        return writer->toString();
    }

    void JsonData::setLexerStartOffset(LexerInfo lexerStartOffset)
    {
        this->lexerStartOffset = lexerStartOffset;
    }

    void JsonData::setLexerEndOffset(LexerInfo lexerEndOffset)
    {
        this->lexerEndOffset = lexerEndOffset;
    }

    LexerInfo JsonData::getLexerStartOffset()
    {
        return lexerStartOffset;
    }

    LexerInfo JsonData::getLexerEndOffset()
    {
        return lexerEndOffset;
    }
}
