/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include "LexerInfo.h"

#include <string>
#include <boost/regex.hpp>
namespace armarx
{
    class SimpleLexer
    {
    public:
        struct Rule
        {
            Rule(int id, const std::string& name, const boost::regex& regex)
                : id(id), name(name), regex(regex)
            {}
            int id;
            std::string name;
            boost::regex regex;
        };

        SimpleLexer(std::string src, int tokenFin = -1, int tokenError = -2);

        void addRule(int id, const std::string& name, const std::string& regex);

        int nextToken();

        void reset();

        std::string tokenIdToName(int id);

        void positionToLineAndColumn(int pos, int& line, int& column);
        LexerInfo getPositionInfo(int pos);

        int pos;
        int lastPos;
        std::string src;

        int currentTokenId;
        std::string currentTokenValue;

        int tokenFin, tokenError;
        std::vector<Rule> rules;
    };
}


