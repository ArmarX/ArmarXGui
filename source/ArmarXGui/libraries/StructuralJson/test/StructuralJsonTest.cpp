/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE ArmarX::Core::StructuralJsonParser
#define ARMARX_BOOST_TEST
#include <ArmarXGui/Test.h>
#include <ArmarXGui/libraries/StructuralJson//JsonValue.h>
#include <ArmarXGui/libraries/StructuralJson/JsonArray.h>
#include <ArmarXGui/libraries/StructuralJson/JsonObject.h>
#include <ArmarXGui/libraries/StructuralJson/StructuralJsonParser.h>
#include <ArmarXGui/libraries/StructuralJson/JPathNavigator.h>

namespace armarx
{
    void RoundTripTest(std::string src, int indenting = 0)
    {
        StructuralJsonParser parser(src);
        parser.parse();
        BOOST_CHECK_EQUAL(parser.iserr(), false);
        BOOST_CHECK_EQUAL(parser.geterr(), "");
        std::string roundtrip = parser.parsedJson->toJsonString(indenting);
        BOOST_CHECK_EQUAL(src, roundtrip);
    }

    void ErrorTest(std::string src, std::string errpos)
    {
        StructuralJsonParser parser(src);
        parser.parse();
        BOOST_CHECK_EQUAL(parser.iserr(), true);
        BOOST_CHECK_EQUAL(parser.geterrposstr(), errpos);
    }

    BOOST_AUTO_TEST_CASE(testParseStructuralJson)
    {
        RoundTripTest(R"(1)");
        RoundTripTest(R"(1.2)");
        RoundTripTest(R"([])");
        RoundTripTest(R"([1])");
        RoundTripTest(R"([1.2])");
        RoundTripTest(R"([1,true,false,null,"test"])");
        RoundTripTest(R"("abc\r\n\täöü")");

        RoundTripTest(R"({"test":1})");
        RoundTripTest(R"({"keyA":1,"keyB":2})");
        RoundTripTest(R"({"keyA":1,"keyB":2,"keyC":3})");

        RoundTripTest(R"({"keyA":[]})");
        RoundTripTest(R"({"keyA":[],"keyB":[1],"keyC":[2]})");

        RoundTripTest(R"([[]])");
        RoundTripTest(R"([[[]]])");
        RoundTripTest(R"([1,[2,[3]]])");
        RoundTripTest(R"([[[1],2],3])");

        RoundTripTest(R"({"a":{"b":1}})");
        RoundTripTest(R"({"a":{"b":{"c":1}}})");
        RoundTripTest(R"({"x":5,"a":{"b":{"c":1}}})");
        RoundTripTest(R"({"x":5,"a":{"y":7,"b":{"c":1}}})");

        ErrorTest(R"(1 1)", "L1 C3");
        ErrorTest(R"([)", "L1 C1");
        ErrorTest(R"([{)", "L1 C2");
        ErrorTest(R"([{})", "L1 C3");

    }

    BOOST_AUTO_TEST_CASE(testStructuralJsonFormatting)
    {
        RoundTripTest(R"([1, true, false, null, "test"])", 1);
        RoundTripTest(R"({"a":1, "b":true, "c":false, "d":null, "e":"test"})", 1);

        RoundTripTest(R"([])", 2);
        RoundTripTest(R"({})", 2);
        RoundTripTest("[\n  1\n]", 2);
        RoundTripTest("[\n  1,\n  2\n]", 2);
        RoundTripTest("[\n  []\n]", 2);
        RoundTripTest("[\n  [],\n  2\n]", 2);
        RoundTripTest("[\n  {}\n]", 2);
        RoundTripTest("[\n  {},\n  2\n]", 2);
        RoundTripTest("[\n  [],\n  {},\n  3\n]", 2);
        RoundTripTest("[\n  [\n    []\n  ]\n]", 2);
        RoundTripTest("{\n  \"a\": 1\n}", 2);
        RoundTripTest("{\n  \"a\": []\n}", 2);
        RoundTripTest("{\n  \"a\": [\n    1\n  ]\n}", 2);
    }

    BOOST_AUTO_TEST_CASE(testWriteStructuralJson)
    {
        JsonObjectPtr o(new JsonObject());
        JsonArrayPtr a(new JsonArray());
        JsonArrayPtr a2(new JsonArray());

        a->clear();
        //a->add(JsonArray({JsonArray({JsonValue(1)})}));
        a2->add(JsonArray({JsonValue(1)}));
        a->add(a2);
        BOOST_CHECK_EQUAL("[[[1]]]", a->toJsonString());

        o->clear();
        BOOST_CHECK_EQUAL("{}", o->toJsonString());
        o->add("test", 5);
        BOOST_CHECK_EQUAL(R"({"test":5})", o->toJsonString());
        o->set("test", JsonValue::False());
        BOOST_CHECK_EQUAL(R"({"test":false})", o->toJsonString());
        BOOST_CHECK_EQUAL(false, o->remove("NotAKey"));
        BOOST_CHECK_EQUAL(true, o->remove("test"));
        BOOST_CHECK_EQUAL(false, o->remove("NotAKey"));
        o->add("b", JsonValue("\r\n\t"));
        o->add("a", JsonValue::Null());
        BOOST_CHECK_EQUAL("{\"b\":\"\\r\\n\\t\",\"a\":null}", o->toJsonString());

        a->clear();
        BOOST_CHECK_EQUAL("[]", a->toJsonString());
        a->add(3);
        BOOST_CHECK_EQUAL("[3]", a->toJsonString());

        o->clear();
        o->add("arr", JsonArray({JsonValue(5), JsonValue("x")}));
        o->add("b", 7);
        BOOST_CHECK_EQUAL("{\"arr\":[5,\"x\"],\"b\":7}", o->toJsonString());

        o->clear();
        o->add("arr1", JsonArray({JsonValue(1.5f)}));
        o->add("arr2", JsonArray({JsonValue(2.5f)}));
        o->add("arr3", JsonArray({JsonValue(3.5)}));
        BOOST_CHECK_EQUAL("{\"arr1\":[1.5],\"arr2\":[2.5],\"arr3\":[3.5]}", o->toJsonString());
    }

    BOOST_AUTO_TEST_CASE(testJPathNavigatorAsInt)
    {
        StructuralJsonParser parser(R"({int:5,float:5.5,boolTrue:true,boolFalse:false}})");
        parser.parse();
        JPathNavigator nav(parser.parsedJson);
        BOOST_CHECK_EQUAL(5, nav.selectSingleNode("int").asInt());
        BOOST_CHECK_EQUAL(5.5f, nav.selectSingleNode("float").asFloat());
        BOOST_CHECK_EQUAL(true, nav.selectSingleNode("boolTrue").asBool());
        BOOST_CHECK_EQUAL(false, nav.selectSingleNode("boolFalse").asBool());
    }
}
