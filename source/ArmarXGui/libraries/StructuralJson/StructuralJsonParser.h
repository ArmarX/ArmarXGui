/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <memory>

#include "JsonData.h"
#include "JsonObject.h"

namespace armarx
{
    class SimpleLexer;

    class StructuralJsonParser;
    using StructuralJsonParserPtr = std::shared_ptr<StructuralJsonParser>;

    class StructuralJsonParser
    {
    public:
        enum TokenType { eWhitespace, eOpeningCurlyBracket, eClosingCurlyBracket, eOpeningSquareBracket, eClosingSquareBracket,
                         eColon, eComma, eId, eString, eNumber, eBoolean, eNull
                       };
        StructuralJsonParser(const std::string& src, bool acceptSingleValue = true, bool accceptJsStyleKeys = true);

        ~StructuralJsonParser();

        void parse();

        JsonDataPtr parsedJson;
        bool iserr();
        std::string geterr();
        void geterrpos(int& line, int& col);
        std::string geterrposstr();
        std::string getlongerrposstr();

        static bool DequoteString(std::string str, std::string& res);

    private:
        std::unique_ptr<SimpleLexer> lexer;
        bool acceptSingleValue;
        bool accceptJsStyleKeys;
        LexerInfo previousTokenEndOffset;

        void next();
        std::string err;
        std::string expected;

        JsonDataPtr p_json();
        JsonDataPtr p_object();
        void p_pair(JsonObjectPtr o);
        JsonDataPtr p_array();
        JsonDataPtr p_item();
        JsonDataPtr p_leaf();
        JsonDataPtr error(const std::string& expected, const JsonDataPtr& toReturn = JsonDataPtr());
        JsonDataPtr stringerror(const std::string& error, const JsonDataPtr& toReturn = JsonDataPtr());


    };
}

