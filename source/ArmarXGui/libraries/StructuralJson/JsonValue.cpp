/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "JsonValue.h"

#include <ArmarXCore/core/exceptions/Exception.h>
#include <ArmarXCore/core/util/StringHelpers.h>
#include <boost/regex.hpp>

namespace armarx
{
    JsonValue::JsonValue(const std::string& value)
        : type(eString), value(value)
    { }

    JsonValue::JsonValue(int value)
        : type(eNumber), value(ToString(value))
    { }

    JsonValue::JsonValue(long value)
        : type(eNumber), value(ToString(value))
    { }

    JsonValue::JsonValue(float value)
        : type(eNumber), value(ToString(value))
    { }

    JsonValue::JsonValue(double value)
        : type(eNumber), value(ToString(value))
    { }

    JsonValuePtr JsonValue::Null()
    {
        JsonValuePtr value(new JsonValue(eNull, "null"));
        return value;
    }
    JsonValuePtr JsonValue::True()
    {
        JsonValuePtr value(new JsonValue(eBool, "true"));
        return value;
    }
    JsonValuePtr JsonValue::False()
    {
        JsonValuePtr value(new JsonValue(eBool, "false"));
        return value;
    }

    JsonValuePtr JsonValue::Create(const std::string& value)
    {
        JsonValuePtr jsonValue(new JsonValue(value));
        return jsonValue;
    }

    JsonValuePtr JsonValue::Create(int value)
    {
        JsonValuePtr jsonValue(new JsonValue(value));
        return jsonValue;
    }

    JsonValuePtr JsonValue::Create(long value)
    {
        JsonValuePtr jsonValue(new JsonValue(value));
        return jsonValue;
    }

    JsonValuePtr JsonValue::Create(float value)
    {
        JsonValuePtr jsonValue(new JsonValue(value));
        return jsonValue;
    }

    JsonValuePtr JsonValue::Create(double value)
    {
        JsonValuePtr jsonValue(new JsonValue(value));
        return jsonValue;
    }

    JsonValuePtr JsonValue::CreateRaw(JsonValue::Type type, const std::string& value)
    {
        if (!CheckValue(type, value))
        {
            throw LocalException("Invalid value: ") << value;
        }
        JsonValuePtr result(new JsonValue(type, value));
        return result;
    }

    bool JsonValue::CheckValue(JsonValue::Type type, const std::string& value)
    {
        switch (type)
        {
            case eNull:
                return CheckNull(value);

            case eBool:
                return CheckBool(value);

            case eNumber:
                return CheckNumber(value);

            case eString:
                return true;
        }
        throw LocalException("Invalid type.");
    }

    bool JsonValue::CheckNumber(const std::string& value)
    {
        boost::regex re("^-?\\d+(\\.\\d+)?([eE][+-]?\\d+)?$");
        boost::match_results<std::string::const_iterator> results;
        return boost::regex_match(value, results, re, boost::regex_constants::match_default) != 0;
    }

    bool JsonValue::CheckInt(const std::string& value)
    {
        boost::regex re("^-?\\d+$");
        boost::match_results<std::string::const_iterator> results;
        return boost::regex_match(value, results, re, boost::regex_constants::match_default) != 0;
    }

    bool JsonValue::CheckBool(const std::string& value)
    {
        return value == "true" || value == "false";
    }

    bool JsonValue::CheckNull(const std::string& value)
    {
        return value == "null";
    }

    void armarx::JsonValue::writeJson(const armarx::JsonWriterPtr& writer)
    {
        switch (type)
        {
            case eNull:
            case eBool:
            case eNumber:
                writer->writeRawValue(value);
                break;

            case eString:
                writer->writeRawValue(JsonWriter::EscapeQuote(value));
                break;
        }
    }

    JsonValuePtr JsonValue::toSharedPtr() const
    {
        JsonValuePtr ptr(new JsonValue(*this));
        return ptr;
    }

    std::string JsonValue::ToString(int value)
    {
        std::ostringstream oss;
        oss.imbue(std::locale::classic());
        oss << value;
        return oss.str();
    }

    std::string JsonValue::ToString(long value)
    {
        std::ostringstream oss;
        oss.imbue(std::locale::classic());
        oss << value;
        return oss.str();
    }

    std::string JsonValue::ToString(float value)
    {
        std::ostringstream oss;
        oss.imbue(std::locale::classic());
        oss << value;
        return oss.str();
    }

    std::string JsonValue::ToString(double value)
    {
        std::ostringstream oss;
        oss.imbue(std::locale::classic());
        oss << value;
        return oss.str();
    }

    std::string JsonValue::asString() const
    {
        if (type != eString)
        {
            throw LocalException("Invalid type");
        }
        return value;
    }

    float JsonValue::asFloat() const
    {
        if (type != eNumber)
        {
            throw LocalException("Invalid type");
        }
        return armarx::toFloat(value);
    }

    int JsonValue::asInt() const
    {
        if (type != eNumber || !CheckInt(value))
        {
            throw LocalException("Invalid type");
        }
        return armarx::toFloat(value);
    }

    bool JsonValue::asBool() const
    {
        if (type != eBool)
        {
            throw LocalException("Invalid type");
        }
        return value == "true";
    }

    std::string JsonValue::rawValue()
    {
        return value;
    }

    JsonDataPtr JsonValue::clone()
    {
        JsonValuePtr ptr(new JsonValue(*this));
        return ptr;
    }

    JsonValue::Type JsonValue::getType()
    {
        return type;
    }

    JsonValue::JsonValue(JsonValue::Type type, const std::string& value)
        : type(type), value(value)
    { }
}
