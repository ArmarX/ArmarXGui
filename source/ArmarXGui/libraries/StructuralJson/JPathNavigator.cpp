/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "JPathNavigator.h"

#include <ArmarXCore/core/exceptions/Exception.h>
#include <ArmarXCore/core/util/StringHelpers.h>

#include <SimoxUtility/algorithm/string/string_tools.h>

namespace armarx
{
    JPathNavigator::JPathNavigator(const JsonDataPtr& data, int index, const std::string& key)
        : data(data), index(index), key(key)
    {
    }

    JPathNavigator::JPathNavigator(const JsonDataPtr& data)
        : JPathNavigator(data, -1, "")
    {
    }

    bool JPathNavigator::isArray() const
    {
        return std::dynamic_pointer_cast<JsonArray>(data) != 0;
    }

    bool JPathNavigator::isObject() const
    {
        return std::dynamic_pointer_cast<JsonObject>(data) != 0;
    }

    bool JPathNavigator::isValue() const
    {
        return std::dynamic_pointer_cast<JsonValue>(data) != 0;
    }

    bool JPathNavigator::isString() const
    {
        JsonValuePtr val = std::dynamic_pointer_cast<JsonValue>(data);
        return val != 0 && val->getType() == JsonValue::eString;
    }

    bool JPathNavigator::isBool() const
    {
        JsonValuePtr val = std::dynamic_pointer_cast<JsonValue>(data);
        return val != 0 && val->getType() == JsonValue::eBool;
    }

    bool JPathNavigator::isNull() const
    {
        JsonValuePtr val = std::dynamic_pointer_cast<JsonValue>(data);
        return val != 0 && val->getType() == JsonValue::eNull;
    }

    bool JPathNavigator::isNumber() const
    {
        JsonValuePtr val = std::dynamic_pointer_cast<JsonValue>(data);
        return val != 0 && val->getType() == JsonValue::eNumber;
    }

    bool JPathNavigator::isInt() const
    {
        JsonValuePtr val = std::dynamic_pointer_cast<JsonValue>(data);
        return val != 0 && val->getType() == JsonValue::eNumber && JsonValue::CheckInt(val->rawValue());
    }

    bool JPathNavigator::isValid() const
    {
        return data != 0;
    }

    void JPathNavigator::checkValid() const
    {
        if (!isValid())
        {
            throw LocalException("JPathNavigator is not valid.");
        }
    }

    std::vector<JPathNavigator> JPathNavigator::select(const std::string& expr, bool limitToOne) const
    {
        checkValid();
        std::vector<std::string> parts = simox::alg::split(expr, "/");
        std::vector<JPathNavigator> result;
        select(parts, 0, result, limitToOne);
        return result;
    }

    JPathNavigator JPathNavigator::selectSingleNode(const std::string& expr) const
    {
        std::vector<JPathNavigator> result = select(expr, true);
        return result.size() > 0 ? result.at(0) : JPathNavigator(0);
    }

    void JPathNavigator::select(const std::vector<std::string>& parts, size_t partIndex, std::vector<JPathNavigator>& result, bool limitToOne) const
    {
        if (partIndex >= parts.size())
        {
            result.push_back(JPathNavigator(data, index, key));
            return;
        }
        std::string part = parts.at(partIndex);
        if (part == "*" && isArray())
        {
            JsonArrayPtr a = asArray();
            for (size_t i = 0; i < a->elements.size(); i++)
            {
                JPathNavigator(a->elements.at(i), i, "").select(parts, partIndex + 1, result);
                if (limitToOne && result.size() > 0)
                {
                    return;
                }
            }
        }
        else if (part == "*" && isObject())
        {
            JsonObjectPtr o = asObject();
            for (const std::pair<std::string, JsonDataPtr>& pair : o->elements)
            {
                JPathNavigator(pair.second, -1, pair.first).select(parts, partIndex + 1, result);
                if (limitToOne && result.size() > 0)
                {
                    return;
                }
            }
        }
        else if (isObject())
        {
            JsonObjectPtr o = asObject();
            JsonDataPtr child = o->get(part);
            if (child)
            {
                JPathNavigator(child, -1, part).select(parts, partIndex + 1, result);
            }
        }
    }


    bool JPathNavigator::remove(const std::string& key)
    {
        return asObject()->remove(key);
    }

    void JPathNavigator::remove(uint index)
    {
        asArray()->remove(index);
    }

    JsonObjectPtr JPathNavigator::asObject() const
    {
        checkValid();
        JsonObjectPtr o = std::dynamic_pointer_cast<JsonObject>(data);
        if (o == 0)
        {
            throw LocalException("Invalid cast");
        }
        return o;
    }

    JsonArrayPtr JPathNavigator::asArray() const
    {
        checkValid();
        JsonArrayPtr a = std::dynamic_pointer_cast<JsonArray>(data);
        if (a == 0)
        {
            throw LocalException("Invalid cast");
        }
        return a;
    }

    JsonValuePtr JPathNavigator::asValue() const
    {
        checkValid();
        JsonValuePtr v = std::dynamic_pointer_cast<JsonValue>(data);
        if (v == 0)
        {
            throw LocalException("Invalid cast");
        }
        return v;
    }

    std::string JPathNavigator::asString() const
    {
        return asValue()->asString();
    }


    float JPathNavigator::asFloat() const
    {
        return asValue()->asFloat();
    }

    int JPathNavigator::asInt() const
    {
        return asValue()->asInt();
    }

    bool JPathNavigator::asBool() const
    {
        return asValue()->asBool();
    }

    JsonDataPtr JPathNavigator::getData() const
    {
        checkValid();
        return data;
    }

    int JPathNavigator::getIndex() const
    {
        checkValid();
        return index;
    }

    std::string JPathNavigator::getKey() const
    {
        checkValid();
        return key;
    }
}
