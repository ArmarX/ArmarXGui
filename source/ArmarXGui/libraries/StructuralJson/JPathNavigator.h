/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "JsonData.h"
#include "JsonArray.h"
#include "JsonObject.h"
#include "JsonValue.h"

#include <memory>

namespace armarx
{
    class JPathNavigator
    {
    private:
        JPathNavigator(const JsonDataPtr& data, int index, const std::string& key);

    public:
        JPathNavigator(const JsonDataPtr& data);

        bool isArray() const;
        bool isObject() const;
        bool isValue() const;
        bool isString() const;
        bool isBool() const;
        bool isNull() const;
        bool isNumber() const;
        bool isInt() const;
        bool isValid() const;

        void checkValid() const;

        std::vector<JPathNavigator> select(const std::string& expr, bool limitToOne = false) const;
        JPathNavigator selectSingleNode(const std::string& expr) const;
        bool remove(const std::string& key);
        void remove(uint index);

        JsonObjectPtr asObject() const;
        JsonArrayPtr asArray() const;
        JsonValuePtr asValue() const;

        std::string asString() const;
        float asFloat() const;
        int asInt() const;
        bool asBool() const;

        JsonDataPtr getData() const;
        int getIndex() const;
        std::string getKey() const;

    private:

        JsonDataPtr data;
        int index;
        std::string key;
        void select(const std::vector<std::string>& parts, size_t partIndex, std::vector<JPathNavigator>& result, bool limitToOne = false) const;
    };
}

