/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGui::ArmarXObjects::WidgetDescription
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "WidgetDescription.h"

#include <ArmarXGui/interface/WidgetDescription.h>
#include <ArmarXGui/libraries/VariantWidget/VariantWidget.h>

#include <ArmarXCore/core/util/algorithm.h>
#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QSpacerItem>
#include <QFormLayout>
#include <QDoubleSpinBox>
#include <QSpinBox>
#include <QLineEdit>
#include <QComboBox>
#include <QCheckBox>
#include <QDoubleValidator>
#include <QSlider>
#include <QGroupBox>
#include <QStandardItemModel>
#include <QStyledItemDelegate>
#include <QPointer>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#include <qwt_slider.h>

#include <ArmarXCore/observers/variant/SingleTypeVariantList.h>
#pragma GCC diagnostic pop

namespace armarx::WidgetDescription
{
    class DescribedWidget : public DescribedWidgetBase
    {
    public:
        DescribedWidget(const WidgetPtr& ptr, ValueChangedListenerInterface*): DescribedWidgetBase(ptr) {}
        std::map<std::string, VariantBasePtr> getVariants() override
        {
            return {};
        }
    };
    DescribedWidgetFactoryRegistration<DescribedWidget> registerDescribedWidget {Widget::ice_staticId()};
    //Layouts
    class DescribedVBoxLayout : public DescribedWidgetLayoutBase
    {
    public:
        DescribedVBoxLayout(const WidgetPtr& p, ValueChangedListenerInterface* listener)
            : DescribedWidgetLayoutBase(p, listener)
        {
            VBoxLayoutPtr ptr = VBoxLayoutPtr::dynamicCast(p);
            ARMARX_CHECK_NOT_NULL(ptr);
            setLayout(new QVBoxLayout);
            layout()->setContentsMargins(0, 0, 0, 0);
            for (const WidgetPtr& childPtr : ptr->children)
            {
                layout()->addWidget(addChild(childPtr));
            }
        }

    };
    DescribedWidgetFactoryRegistration<DescribedVBoxLayout> registerDescribedVBoxLayout {VBoxLayout::ice_staticId()};

    class DescribedHBoxLayout : public DescribedWidgetLayoutBase
    {
    public:
        DescribedHBoxLayout(const WidgetPtr& p, ValueChangedListenerInterface* listener)
            : DescribedWidgetLayoutBase(p, listener)
        {
            HBoxLayoutPtr ptr = HBoxLayoutPtr::dynamicCast(p);
            ARMARX_CHECK_NOT_NULL(ptr);
            setLayout(new QHBoxLayout);
            layout()->setContentsMargins(0, 0, 0, 0);
            for (const WidgetPtr& childPtr : ptr->children)
            {
                layout()->addWidget(addChild(childPtr));
            }
        }
    };
    DescribedWidgetFactoryRegistration<DescribedHBoxLayout> registerDescribedHBoxLayout {HBoxLayout::ice_staticId()};

    class DescribedFormLayout : public DescribedWidgetLayoutBase
    {
    public:
        DescribedFormLayout(const WidgetPtr& p, ValueChangedListenerInterface* listener)
            : DescribedWidgetLayoutBase(p, listener)
        {
            FormLayoutPtr ptr = FormLayoutPtr::dynamicCast(p);
            ARMARX_CHECK_NOT_NULL(ptr);
            auto l = new QFormLayout;
            setLayout(l);
            layout()->setContentsMargins(0, 0, 0, 0);
            for (const FormLayoutElementPtr& child : ptr->children)
            {
                if (child->childIsSpanning)
                {
                    l->addRow(addChild(child->child));
                    continue;
                }
                if (child->labelWidget)
                {
                    l->addRow(addChild(child->labelWidget), addChild(child->child));
                    continue;
                }
                l->addRow(QString::fromStdString(child->label), addChild(child->child));
            }
        }
    };
    DescribedWidgetFactoryRegistration<DescribedFormLayout> registerDescribedFormLayout {FormLayout::ice_staticId()};

    class DescribedFormLayoutElement : public DescribedWidgetLayoutBase
    {
    public:
        DescribedFormLayoutElement(const WidgetPtr& p, ValueChangedListenerInterface* listener)
            : DescribedWidgetLayoutBase(p, listener)
        {
            FormLayoutElementPtr ptr = FormLayoutElementPtr::dynamicCast(p);
            ARMARX_CHECK_NOT_NULL(ptr);
            auto l = new QFormLayout;
            setLayout(l);
            layout()->setContentsMargins(0, 0, 0, 0);
            if (ptr->childIsSpanning)
            {
                l->addRow(addChild(ptr->child));
            }
            if (ptr->labelWidget)
            {
                l->addRow(addChild(ptr->labelWidget), addChild(ptr->child));
            }
            l->addRow(QString::fromStdString(ptr->label), addChild(ptr->child));
        }
    };
    DescribedWidgetFactoryRegistration<DescribedFormLayoutElement> registerDescribedFormLayoutElement {FormLayoutElement::ice_staticId()};

    class DescribedGroupBox : public DescribedWidgetLayoutBase
    {
    public:
        DescribedGroupBox(const WidgetPtr& p, ValueChangedListenerInterface* listener)
            : DescribedWidgetLayoutBase(p, listener)
        {
            GroupBoxPtr ptr = GroupBoxPtr::dynamicCast(p);
            ARMARX_CHECK_NOT_NULL(ptr);
            auto ltop = new QHBoxLayout;
            setLayout(ltop);
            layout()->setContentsMargins(0, 0, 0, 0);
            QGroupBox* grp = new QGroupBox;
            ltop->addWidget(grp);
            grp->setTitle(QString::fromStdString(ptr->label));
            auto l = new QHBoxLayout;
            grp->setLayout(l);
            l->setContentsMargins(0, 9, 0, 0);
            l->addWidget(addChild(ptr->child));
        }
    };
    DescribedWidgetFactoryRegistration<DescribedGroupBox> DescribedGroupBox {GroupBox::ice_staticId()};

    //static elements
    class DescribedHSpacer : public DescribedWidgetBase
    {
    public:
        DescribedHSpacer(const WidgetPtr& p, ValueChangedListenerInterface*): DescribedWidgetBase(p)
        {
            setLayout(new QVBoxLayout);
            layout()->setContentsMargins(0, 0, 0, 0);
            layout()->addItem(new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding));
        }
        std::map<std::string, VariantBasePtr> getVariants() override
        {
            return {};
        }
    };
    DescribedWidgetFactoryRegistration<DescribedHSpacer> registerDescribedHSpacer {HSpacer::ice_staticId()};

    class DescribedVSpacer : public DescribedWidgetBase
    {
    public:
        DescribedVSpacer(const WidgetPtr& p, ValueChangedListenerInterface*): DescribedWidgetBase(p)
        {
            setLayout(new QVBoxLayout);
            layout()->setContentsMargins(0, 0, 0, 0);
            layout()->addItem(new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding));
        }
        std::map<std::string, VariantBasePtr> getVariants() override
        {
            return {};
        }
    };
    DescribedWidgetFactoryRegistration<DescribedVSpacer> registerDescribedVSpacer {VSpacer::ice_staticId()};

    class DescribedHLine : public DescribedWidgetBase
    {
    public:
        DescribedHLine(const WidgetPtr& p, ValueChangedListenerInterface*): DescribedWidgetBase(p)
        {
            setFrameShape(QFrame::HLine);
            setFrameShadow(QFrame::Sunken);
        }
        std::map<std::string, VariantBasePtr> getVariants() override
        {
            return {};
        }
    };
    DescribedWidgetFactoryRegistration<DescribedHLine> registerDescribedHLine {HLine::ice_staticId()};

    class DescribedVLine : public DescribedWidgetBase
    {
    public:
        DescribedVLine(const WidgetPtr& p, ValueChangedListenerInterface*): DescribedWidgetBase(p)
        {
            setFrameShape(QFrame::VLine);
            setFrameShadow(QFrame::Sunken);
        }
        std::map<std::string, VariantBasePtr> getVariants() override
        {
            return {};
        }
    };
    DescribedWidgetFactoryRegistration<DescribedVLine> registerDescribedVLine {VLine::ice_staticId()};

    class DescribedLabel : public DescribedWidgetBase
    {
    public:
        DescribedLabel(const WidgetPtr& p, ValueChangedListenerInterface*): DescribedWidgetBase(p)
        {
            auto ptr = LabelPtr::dynamicCast(p);
            ARMARX_CHECK_NOT_NULL(ptr);
            setLayout(new QVBoxLayout);
            layout()->setContentsMargins(0, 0, 0, 0);
            layout()->addWidget(new QLabel(QString::fromStdString(ptr->text)));
        }
        std::map<std::string, VariantBasePtr> getVariants() override
        {
            return {};
        }
    };
    DescribedWidgetFactoryRegistration<DescribedLabel> registerDescribedLabel {Label::ice_staticId()};

    //dynamic content elements
    class DescribedVariantWidget : public DescribedWidgetBase
    {
    public:
        DescribedVariantWidget(const WidgetPtr& p, ValueChangedListenerInterface*): DescribedWidgetBase(p)
        {
            auto ptr = VariantWidgetPtr::dynamicCast(p);
            ARMARX_CHECK_NOT_NULL(ptr);
            setName(ptr->name);
            setLayout(new QVBoxLayout);
            child = new armarx::VariantWidget;
            layout()->setContentsMargins(0, 0, 0, 0);
            layout()->addWidget(child);
        }
        void variantWidgetUpdatedSlot(std::string name, VariantWidgetContent::VariantWidgetContentBasePtr data) override
        {
            if (name != getName())
            {
                return;
            }
            if (!data)
            {
                child->update(nullptr);
            }
            else if (data->ice_id() == VariantWidgetContent::SingleVariant::ice_staticId())
            {
                child->update(VariantWidgetContent::SingleVariantPtr::dynamicCast(data)->content);
            }
            else if (data->ice_id() == VariantWidgetContent::VariantMap::ice_staticId())
            {
                child->update(VariantWidgetContent::VariantMapPtr::dynamicCast(data)->content);
            }
            else if (data->ice_id() == VariantWidgetContent::VariantSeq::ice_staticId())
            {
                child->update(VariantWidgetContent::VariantSeqPtr::dynamicCast(data)->content);
            }
        }
    private:
        armarx::VariantWidget* child;
    };
    DescribedWidgetFactoryRegistration<DescribedVariantWidget> registerDescribedVariantWidget {VariantWidget::ice_staticId()};

    //config elements
    class DescribedCheckBox : public DescribedWidgetBase
    {
    public:
        DescribedCheckBox(const WidgetPtr& p, ValueChangedListenerInterface*): DescribedWidgetBase(p)
        {
            CheckBoxPtr ptr = CheckBoxPtr::dynamicCast(p);
            ARMARX_CHECK_NOT_NULL(ptr);
            setName(ptr->name);
            setLayout(new QVBoxLayout);
            layout()->setContentsMargins(0, 0, 0, 0);
            child = new QCheckBox {QString::fromStdString(ptr->label)};
            child->setChecked(ptr->defaultValue);
            layout()->addWidget(child);
            connect(child, SIGNAL(clicked(bool)), this, SLOT(contentUpdated()));
        }
        std::map<std::string, VariantBasePtr> getVariants() override
        {
            return {{getName(), new Variant{child->isChecked()}}};
        }
    private:
        QCheckBox* child;
    protected:
        void contentUpdated() override
        {
            emit valueChangedSignal(getName(), new Variant(child->isChecked()));
        }
    };
    DescribedWidgetFactoryRegistration<DescribedCheckBox> registerDescribedCheckBox {CheckBox::ice_staticId()};

    class DescribedIntSpinBox : public DescribedWidgetBase
    {
    public:
        DescribedIntSpinBox(const WidgetPtr& p, ValueChangedListenerInterface*): DescribedWidgetBase(p)
        {
            IntSpinBoxPtr ptr = IntSpinBoxPtr::dynamicCast(p);
            ARMARX_CHECK_NOT_NULL(ptr);
            setName(ptr->name);
            setLayout(new QVBoxLayout);
            layout()->setContentsMargins(0, 0, 0, 0);
            child = new QSpinBox {};
            child->setMaximum(ptr->max);
            child->setMinimum(ptr->min);
            child->setValue(ptr->defaultValue);
            layout()->addWidget(child);
            connect(child, SIGNAL(valueChanged(int)), this, SLOT(contentUpdated()));
        }
        std::map<std::string, VariantBasePtr> getVariants() override
        {
            return {{getName(), new Variant(child->value())}};
        }
    private:
        QSpinBox* child;
    protected:
        void contentUpdated() override
        {
            emit valueChangedSignal(getName(), new Variant(child->value()));
        }
    };
    DescribedWidgetFactoryRegistration<DescribedIntSpinBox> registerDescribedIntSpinBox {IntSpinBox::ice_staticId()};

    class DescribedDoubleSpinBox : public DescribedWidgetBase
    {
    public:
        DescribedDoubleSpinBox(const WidgetPtr& p, ValueChangedListenerInterface*): DescribedWidgetBase(p)
        {
            DoubleSpinBoxPtr ptr = DoubleSpinBoxPtr::dynamicCast(p);
            ARMARX_CHECK_NOT_NULL(ptr);
            setName(ptr->name);
            setLayout(new QVBoxLayout);
            layout()->setContentsMargins(0, 0, 0, 0);
            child = new QDoubleSpinBox {};
            child->setMaximum(ptr->max);
            child->setMinimum(ptr->min);
            child->setDecimals(ptr->decimals);
            child->setValue(ptr->defaultValue);
            child->setSingleStep((ptr->max - ptr->min) / ptr->steps);
            layout()->addWidget(child);
            connect(child, SIGNAL(valueChanged(double)), this, SLOT(contentUpdated()));
        }
        std::map<std::string, VariantBasePtr> getVariants() override
        {
            return {{getName(), new Variant(child->value())}};
        }
    private:
        QDoubleSpinBox* child;
    protected:
        void contentUpdated() override
        {
            emit valueChangedSignal(getName(), new Variant(child->value()));
        }
    };
    DescribedWidgetFactoryRegistration<DescribedDoubleSpinBox> registerDescribedDoubleSpinBox {DoubleSpinBox::ice_staticId()};

    class DescribedFloatSpinBox : public DescribedWidgetBase
    {
    public:
        DescribedFloatSpinBox(const WidgetPtr& p, ValueChangedListenerInterface*): DescribedWidgetBase(p)
        {
            FloatSpinBoxPtr ptr = FloatSpinBoxPtr::dynamicCast(p);
            ARMARX_CHECK_NOT_NULL(ptr);
            setName(ptr->name);
            setLayout(new QVBoxLayout);
            layout()->setContentsMargins(0, 0, 0, 0);
            child = new QDoubleSpinBox {};
            child->setMaximum(ptr->max);
            child->setMinimum(ptr->min);
            child->setDecimals(ptr->decimals);
            child->setValue(ptr->defaultValue);
            child->setSingleStep((ptr->max - ptr->min) / ptr->steps);
            layout()->addWidget(child);
            connect(child, SIGNAL(valueChanged(double)), this, SLOT(contentUpdated()));
        }
        std::map<std::string, VariantBasePtr> getVariants() override
        {
            return {{getName(), new Variant(static_cast<float>(child->value()))}};
        }
    private:
        QDoubleSpinBox* child;
    protected:
        void contentUpdated() override
        {
            emit valueChangedSignal(getName(), new Variant(static_cast<float>(child->value())));
        }
    };
    DescribedWidgetFactoryRegistration<DescribedFloatSpinBox> registerDescribedFloatSpinBox {FloatSpinBox::ice_staticId()};

    class SubclassOfQStyledItemDelegate : public QStyledItemDelegate
    {
    public:
        SubclassOfQStyledItemDelegate(QObject* parent = 0) : QStyledItemDelegate(parent) {}
        void paint(QPainter* painter_, const QStyleOptionViewItem& option_, const QModelIndex& index_) const override
        {
            QStyleOptionViewItem& refToNonConstOption = const_cast<QStyleOptionViewItem&>(option_);
            refToNonConstOption.showDecorationSelected = false;
            //refToNonConstOption.state &= ~QStyle::State_HasFocus & ~QStyle::State_MouseOver;

            QStyledItemDelegate::paint(painter_, refToNonConstOption, index_);
        }
    };

    class DescribedStringComboBox : public DescribedWidgetBase
    {
    public:
        DescribedStringComboBox(const WidgetPtr& p, ValueChangedListenerInterface*): DescribedWidgetBase(p)
        {
            StringComboBoxPtr ptr = StringComboBoxPtr::dynamicCast(p);
            ARMARX_CHECK_NOT_NULL(ptr);
            setName(ptr->name);
            setLayout(new QVBoxLayout);
            layout()->setContentsMargins(0, 0, 0, 0);
            child = new QComboBox {};
            multiSelect = ptr->multiSelect;
            if (multiSelect)
            {
                model = new QStandardItemModel(ptr->options.size() + 1, 1);
                QStandardItem* item = new QStandardItem("Please select items");
                item->setFlags(Qt::NoItemFlags);
                model->setItem(0, 0, item);

                int row = 1;
                for (const auto& opt : ptr->options)
                {
                    QStandardItem* item = new QStandardItem(QString::fromStdString(opt));

                    item->setFlags(Qt::ItemIsUserCheckable | Qt::ItemIsEnabled);
                    item->setData(Qt::Unchecked, Qt::CheckStateRole);

                    model->setItem(row, 0, item);
                    row++;

                }
                model->setParent(child);
                child->setModel(model);
                SubclassOfQStyledItemDelegate* delegate = new SubclassOfQStyledItemDelegate(child);
                child->setItemDelegate(delegate);
                child->setCurrentIndex(-1);
            }
            else
            {
                for (const auto& opt : ptr->options)
                {
                    child->addItem(QString::fromStdString(opt));
                }
                ARMARX_CHECK_GREATER(ptr->options.size(), static_cast<std::size_t>(ptr->defaultIndex));
                child->setCurrentIndex(child->findText(QString::fromStdString(ptr->options.at(ptr->defaultIndex))));
            }
            layout()->addWidget(child);

            connect(child, SIGNAL(currentIndexChanged(int)), this, SLOT(contentUpdated()));
        }
        std::map<std::string, VariantBasePtr> getVariants() override
        {
            if (multiSelect)
            {
                SingleTypeVariantListPtr list(new SingleTypeVariantList());
                for (int i = 0; i < child->model()->rowCount(); ++i)
                {
                    if (model->item(i)->checkState() == Qt::Checked)
                    {

                        list->addVariant(child->itemText(i).toStdString());
                    }
                }
                ARMARX_DEBUG << "selected items: " << list->output();
                return {{getName(), new Variant{list}}};
            }
            else
            {
                return {{getName(), new Variant{child->currentText().toStdString()}}};
            }
        }
    private:
        QPointer<QStandardItemModel> model = nullptr;
        QComboBox* child;
        bool multiSelect = false;
    protected:
        void contentUpdated() override
        {
            emit valueChangedSignal(getName(), new Variant {child->currentText().toStdString()});
        }
    };
    DescribedWidgetFactoryRegistration<DescribedStringComboBox> registerDescribedStringComboBox {StringComboBox::ice_staticId()};

    class DescribedLineEdit : public DescribedWidgetBase
    {
    public:
        DescribedLineEdit(const WidgetPtr& p, ValueChangedListenerInterface*): DescribedWidgetBase(p)
        {
            LineEditPtr ptr = LineEditPtr::dynamicCast(p);
            ARMARX_CHECK_NOT_NULL(ptr);
            setName(ptr->name);
            setLayout(new QVBoxLayout);
            layout()->setContentsMargins(0, 0, 0, 0);
            child = new QLineEdit {};
            child->setText(QString::fromStdString(ptr->defaultValue));
            layout()->addWidget(child);
            connect(child, SIGNAL(editingFinished()), this, SLOT(contentUpdated()));
        }
        std::map<std::string, VariantBasePtr> getVariants() override
        {
            return {{getName(), new Variant{child->text().toStdString()}}};
        }
    private:
        QLineEdit* child;
    protected:
        void contentUpdated() override
        {
            emit valueChangedSignal(getName(), new Variant {child->text().toStdString()});
        }
    };
    DescribedWidgetFactoryRegistration<DescribedLineEdit> registerDescribedLineEdit {LineEdit::ice_staticId()};

    class DescribedDoubleLineEdit : public DescribedWidgetBase
    {
    public:
        DescribedDoubleLineEdit(const WidgetPtr& p, ValueChangedListenerInterface*): DescribedWidgetBase(p)
        {
            DoubleLineEditPtr ptr = DoubleLineEditPtr::dynamicCast(p);
            ARMARX_CHECK_NOT_NULL(ptr);
            setName(ptr->name);
            setLayout(new QVBoxLayout);
            layout()->setContentsMargins(0, 0, 0, 0);
            child = new QLineEdit {};
            child->setText(QString::number(ptr->defaultValue));
            child->setValidator(new QDoubleValidator {this});
            layout()->addWidget(child);
            connect(child, SIGNAL(editingFinished()), this, SLOT(contentUpdated()));
        }
        std::map<std::string, VariantBasePtr> getVariants() override
        {
            return {{getName(), new Variant{child->text().toDouble()}}};
        }
    private:
        QLineEdit* child;
    protected:
        void contentUpdated() override
        {
            emit valueChangedSignal(getName(), new Variant {child->text().toDouble()});
        }
    };
    DescribedWidgetFactoryRegistration<DescribedDoubleLineEdit> registerDescribedDoubleLineEdit {DoubleLineEdit::ice_staticId()};

    class DescribedFloatLineEdit : public DescribedWidgetBase
    {
    public:
        DescribedFloatLineEdit(const WidgetPtr& p, ValueChangedListenerInterface*): DescribedWidgetBase(p)
        {
            FloatLineEditPtr ptr = FloatLineEditPtr::dynamicCast(p);
            ARMARX_CHECK_NOT_NULL(ptr);
            setName(ptr->name);
            setLayout(new QVBoxLayout);
            layout()->setContentsMargins(0, 0, 0, 0);
            child = new QLineEdit {};
            child->setText(QString::number(ptr->defaultValue));
            child->setValidator(new QDoubleValidator {this});
            layout()->addWidget(child);
            connect(child, SIGNAL(editingFinished()), this, SLOT(contentUpdated()));
        }
        std::map<std::string, VariantBasePtr> getVariants() override
        {
            return {{getName(), new Variant{static_cast<float>(child->text().toDouble())}}};
        }
    private:
        QLineEdit* child;
    protected:
        void contentUpdated() override
        {
            emit valueChangedSignal(getName(), new Variant {static_cast<float>(child->text().toDouble())});
        }
    };
    DescribedWidgetFactoryRegistration<DescribedFloatLineEdit> registerDescribedFloatLineEdit {FloatLineEdit::ice_staticId()};


    class DescribedIntSlider : public DescribedWidgetBase
    {
    public:
        DescribedIntSlider(const WidgetPtr& p, ValueChangedListenerInterface*): DescribedWidgetBase(p)
        {
            IntSliderPtr ptr = IntSliderPtr::dynamicCast(p);
            ARMARX_CHECK_NOT_NULL(ptr);
            setName(ptr->name);
            setLayout(new QVBoxLayout);
            layout()->setContentsMargins(0, 0, 0, 0);
            child = new QSlider {};
            child->setMaximum(ptr->max);
            child->setMinimum(ptr->min);
            child->setValue(ptr->defaultValue);
            layout()->addWidget(child);
            connect(child, SIGNAL(valueChanged(int)), this, SLOT(contentUpdated()));
        }
        std::map<std::string, VariantBasePtr> getVariants() override
        {
            return {{getName(), new Variant(child->value())}};
        }
    private:
        QSlider* child;
    protected:
        void contentUpdated() override
        {
            emit valueChangedSignal(getName(), new Variant(child->value()));
        }
    };
    DescribedWidgetFactoryRegistration<DescribedIntSlider> registerDescribedIntSlider {IntSlider::ice_staticId()};

    class DescribedDoubleSlider : public DescribedWidgetBase
    {
    public:
        DescribedDoubleSlider(const WidgetPtr& p, ValueChangedListenerInterface*): DescribedWidgetBase(p)
        {
            DoubleSliderPtr ptr = DoubleSliderPtr::dynamicCast(p);
            ARMARX_CHECK_NOT_NULL(ptr);
            setName(ptr->name);
            setLayout(new QVBoxLayout);
            layout()->setContentsMargins(0, 0, 0, 0);
            child = new QwtSlider {this};
            child->setScale(ptr->min, ptr->max);
            child->setOrientation(Qt::Horizontal);
            child->setValue(ptr->defaultValue);
            doubleLabel = new QLabel(QString::number(ptr->defaultValue, 'g', 4), this);
            layout()->addWidget(child);
            connect(child, SIGNAL(valueChanged(double)), this, SLOT(contentUpdated()));
        }
        std::map<std::string, VariantBasePtr> getVariants() override
        {
            return {{getName(), new Variant(child->value())}};
        }
    private:
        QwtSlider* child;
        QLabel* doubleLabel;
    protected:
        void contentUpdated() override
        {
            doubleLabel->setText(QString::number(child->value(), 'g', 4));
            emit valueChangedSignal(getName(), new Variant(child->value()));
        }
    };
    DescribedWidgetFactoryRegistration<DescribedDoubleSlider> registerDescribedDoubleSlider {DoubleSlider::ice_staticId()};

    class DescribedFloatSlider : public DescribedWidgetBase
    {
    public:
        DescribedFloatSlider(const WidgetPtr& p, ValueChangedListenerInterface*): DescribedWidgetBase(p)
        {
            FloatSliderPtr ptr = FloatSliderPtr::dynamicCast(p);
            ARMARX_CHECK_NOT_NULL(ptr);
            setName(ptr->name);
            setLayout(new QHBoxLayout);
            layout()->setContentsMargins(0, 0, 0, 0);
            child = new QwtSlider {this};
            child->setScale(ptr->min, ptr->max);
            child->setOrientation(Qt::Horizontal);
            child->setValue(ptr->defaultValue);
            floatLabel = new QLabel(QString::number(ptr->defaultValue, 'g', 4), this);
            layout()->addWidget(child);
            layout()->addWidget(floatLabel);
            connect(child, SIGNAL(valueChanged(double)), this, SLOT(contentUpdated()));
        }
        std::map<std::string, VariantBasePtr> getVariants() override
        {
            return {{getName(), new Variant(static_cast<float>(child->value()))}};
        }
    private:
        QwtSlider* child;
        QLabel* floatLabel;
    protected:
        void contentUpdated() override
        {
            floatLabel->setText(QString::number(child->value(), 'g', 4));
            emit valueChangedSignal(getName(), new Variant(static_cast<float>(child->value())));
        }
    };
    DescribedWidgetFactoryRegistration<DescribedFloatSlider> registerDescribedFloatSlider {FloatSlider::ice_staticId()};

    //functions from base classes
    DescribedWidgetLayoutBase::DescribedWidgetLayoutBase(WidgetPtr ptr, ValueChangedListenerInterface* listener): DescribedWidgetBase(ptr), listener {listener ? listener : this} {}

    std::map<std::string, VariantBasePtr> DescribedWidgetLayoutBase::getVariants()
    {
        std::map<std::string, VariantBasePtr> result;
        for (DescribedWidgetBase* child : children)
        {
            mergeMaps(result, child->getVariants());
        }
        return result;
    }

    DescribedWidgetBase* DescribedWidgetLayoutBase::addChild(const WidgetPtr& childPtr)
    {
        ARMARX_CHECK_EXPRESSION(listener);
        DescribedWidgetBase* c = makeDescribedWidget(childPtr, listener);
        children.emplace_back(c);
        connect(
            this, SIGNAL(variantWidgetUpdatedSignal(std::string, VariantWidgetContent::VariantWidgetContentBasePtr)),
            c, SLOT(variantWidgetUpdatedSlot(std::string, VariantWidgetContent::VariantWidgetContentBasePtr))
        );
        return c;
    }

    DescribedWidgetBase* makeDescribedWidget(const WidgetPtr& p, ValueChangedListenerInterface* listener)
    {
        class ErrorMessageWidget : public DescribedWidgetBase
        {
        public:
            ErrorMessageWidget(const std::string& errorMessage): DescribedWidgetBase(nullptr)
            {
                setLayout(new QVBoxLayout);
                setName(".ErrorMessageWidget");
                layout()->setContentsMargins(0, 0, 0, 0);
                QLabel* label = new QLabel {QString::fromStdString(errorMessage)};
                layout()->addWidget(label);
                label->setStyleSheet("QLabel { background-color : yellow; color : red; }");
            }
        };

        if (!p)
        {
            return new ErrorMessageWidget {"Null WidgetPtr"};
        }
        auto type = p->ice_id();
        if (DescribedWidgetFactory::has(type))
        {
            try
            {
                auto w = DescribedWidgetFactory::get(type)(p, listener);
                if (listener)
                {
                    QObject::connect(w, SIGNAL(valueChangedSignal(std::string, VariantBasePtr)), listener, SLOT(valueChangedSlot(std::string, VariantBasePtr)));
                }
                return w;
            }
            catch (std::exception& e)
            {
                return new ErrorMessageWidget {"DescribedWidgetFactory for " + type + " threw exception\n" + e.what()};
            }
            catch (...)
            {
                return new ErrorMessageWidget {"DescribedWidgetFactory for " + type + " threw exception"};
            }
        }
        else
        {
            return new ErrorMessageWidget {"No DescribedWidgetFactory for " + type};
        }
        return new ErrorMessageWidget {"makeDescribedWidget: Unknown error"};
    }

    DescribedWidgetBase::DescribedWidgetBase(WidgetPtr ptr)
    {
        if (ptr && ptr->framed)
        {
            setFrameShape(QFrame::StyledPanel);
        }
    }

    ValueChangedListenerInterface::ValueChangedListenerInterface()
    {
        setFrameShape(QFrame::NoFrame);
        setFrameShadow(QFrame::Raised);
    }

}
