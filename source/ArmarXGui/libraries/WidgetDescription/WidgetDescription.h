/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGui::ArmarXObjects::WidgetDescription
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <functional>

#include <QFrame>

#include <ArmarXCore/core/util/Registrar.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/interface/observers/VariantBase.h>
#include <ArmarXGui/interface/WidgetDescription.h>

namespace armarx::WidgetDescription
{
    class ValueChangedListenerInterface: public QFrame
    {
        Q_OBJECT
    public:
        ValueChangedListenerInterface();
    public slots:
        virtual void valueChangedSlot(std::string name, VariantBasePtr value) {}
    };

    class DescribedWidgetBase: public ValueChangedListenerInterface
    {
        Q_OBJECT
    public:
        DescribedWidgetBase(WidgetPtr ptr);

        virtual std::map<std::string, VariantBasePtr> getVariants()
        {
            return {};
        }
        virtual std::string getName() const
        {
            return objectName().toStdString();
        }
        virtual void setName(const std::string& name)
        {
            setObjectName(QString::fromStdString(name));
        }
    signals:
        void valueChangedSignal(std::string name, VariantBasePtr value);
        void variantWidgetUpdatedSignal(std::string name, VariantWidgetContent::VariantWidgetContentBasePtr data);
    public slots:
        virtual void variantWidgetUpdatedSlot(std::string name, VariantWidgetContent::VariantWidgetContentBasePtr data) {}
    protected slots:
        virtual void contentUpdated() {}
    };

    //has to be here since it has the Q_OBJECT macro
    class DescribedWidgetLayoutBase : public DescribedWidgetBase
    {
        Q_OBJECT
    public:
        DescribedWidgetLayoutBase(WidgetPtr ptr, ValueChangedListenerInterface* listener);
        std::map<std::string, VariantBasePtr> getVariants() override;
        void valueChangedSlot(std::string name, VariantBasePtr value) override
        {
            emit valueChangedSignal(name, value);
        }
        void variantWidgetUpdatedSlot(std::string name, VariantWidgetContent::VariantWidgetContentBasePtr data) override
        {
            emit variantWidgetUpdatedSignal(name, data);
        }
        virtual DescribedWidgetBase* addChild(const WidgetPtr& childPtr);

    protected:
        std::vector<DescribedWidgetBase*> children;
        ValueChangedListenerInterface* listener;
    };

    using DescribedWidgetFactory = Registrar<std::function<DescribedWidgetBase*(const WidgetPtr&, ValueChangedListenerInterface*)>>;

    template<class T>
    struct DescribedWidgetFactoryRegistration
    {
        DescribedWidgetFactoryRegistration(const std::string& type)
        {
            DescribedWidgetFactory::registerElement(
                type,
                [type](const WidgetPtr & p, ValueChangedListenerInterface * listener)
            {
                ARMARX_CHECK_EXPRESSION(p) << "Passed WidgetPtr is null";
                ARMARX_CHECK_EXPRESSION(p->ice_id() == type) << p->ice_id() << " == " << type;
                // ARMARX_DEBUG << "Making DescribedWidget for type '" << type << "'";
                return new T(p, listener);
            }
            );
        }
    };
    DescribedWidgetBase* makeDescribedWidget(const WidgetPtr& p, ValueChangedListenerInterface* listener = nullptr);
}
