#include "Storage.h"

namespace armarx::RemoteGui
{

    bool buttonClicked(RemoteGui::ValueMap const& newValues, RemoteGui::ValueMap const& oldValues, std::string const& name)
    {
        auto oldIter = oldValues.find(name);
        if (oldIter == oldValues.end())
        {
            return false;
        }

        auto newIter = newValues.find(name);
        if (newIter == newValues.end())
        {
            throw LocalException("Could find value with name: ") << name;
        }

        bool changed = false;
        if (oldIter->second.type == VALUE_VARIANT_INT)
        {
            int oldValue = getSingleValue<int>(oldIter->second, oldIter->first);
            int newValue = getSingleValue<int>(newIter->second, newIter->first);
            changed = newValue > oldValue;
        } else if (oldIter->second.type == VALUE_VARIANT_BOOL)
        {
            bool oldValue = getSingleValue<bool>(oldIter->second, oldIter->first);
            bool newValue = getSingleValue<bool>(newIter->second, newIter->first);
            changed = newValue != oldValue;
        }

        return changed;
    }

    bool operator ==(const ValueVariant& left, const ValueVariant& right)
    {
        if (left.type != right.type)
        {
            return false;
        }

        switch (left.type)
        {
            case VALUE_VARIANT_EMPTY:
                return true;

            case VALUE_VARIANT_BOOL:
                return (left.i != 0) == (right.i != 0);

            case VALUE_VARIANT_INT:
                return left.i == right.i;

            case VALUE_VARIANT_FLOAT:
                return left.f == right.f;

            case VALUE_VARIANT_STRING:
                return left.s == right.s;

            case VALUE_VARIANT_VECTOR3:
            case VALUE_VARIANT_MATRIX4:
                return left.v == right.v;
        }

        throw std::logic_error("Remote value type not handled");
    }

    bool operator !=(const ValueVariant& left, const ValueVariant& right)
    {
        return !(left == right);
    }

    Vector3f toIceF(Eigen::Vector3f v)
    {
        Vector3f result;
        result.x = v.x();
        result.y = v.y();
        result.z = v.z();
        return result;
    }

    Vector3i toIceI(Eigen::Vector3i v)
    {
        Vector3i result;
        result.x = v.x();
        result.y = v.y();
        result.z = v.z();
        return result;
    }

    Eigen::Vector3f fromIce(Vector3f v)
    {
        Eigen::Vector3f result;
        result.x() = v.x;
        result.y() = v.y;
        result.z() = v.z;
        return result;
    }

    Eigen::Vector3i fromIce(Vector3i v)
    {
        Eigen::Vector3i result;
        result.x() = v.x;
        result.y() = v.y;
        result.z() = v.z;
        return result;
    }

    const char* getVariantTypeName(ValueVariantType type)
    {
        switch (type)
        {
            case VALUE_VARIANT_EMPTY:
                return "";
            case VALUE_VARIANT_BOOL:
                return "bool";
            case VALUE_VARIANT_INT:
                return "int";
            case VALUE_VARIANT_FLOAT:
                return "float";
            case VALUE_VARIANT_STRING:
                return "string";
            case VALUE_VARIANT_VECTOR3:
                return "vector3";
            case VALUE_VARIANT_MATRIX4:
                return "matrix4";
            default:
                throw LocalException("Unknown ValueVariantType: ")
                        << (int)type;
        }
    }

    ValueVariant makeValue(bool value)
    {
        ValueVariant result;
        result.type = VALUE_VARIANT_BOOL;
        result.i = value ? 1 : 0;
        return result;
    }

    ValueVariant makeValue(int value)
    {
        ValueVariant result;
        result.type = VALUE_VARIANT_INT;
        result.i = value;
        return result;
    }

    ValueVariant makeValue(float value)
    {
        ValueVariant result;
        result.type = VALUE_VARIANT_FLOAT;
        result.f = value;
        return result;
    }

    ValueVariant makeValue(std::string value)
    {
        ValueVariant result;
        result.type = VALUE_VARIANT_STRING;
        result.s = value;
        return result;
    }

    ValueVariant makeValue(const Eigen::Vector3f& value)
    {
        ValueVariant result;
        result.type = VALUE_VARIANT_VECTOR3;
        result.v.resize(3);
        result.v[0] = value(0);
        result.v[1] = value(1);
        result.v[2] = value(2);
        return result;
    }

    ValueVariant makeValue(const Eigen::Matrix4f& value)
    {
        ValueVariant result;
        result.type = VALUE_VARIANT_MATRIX4;
        result.v.resize(16);
        result.v[0] = value.row(0)[0];
        result.v[1] = value.row(0)[1];
        result.v[2] = value.row(0)[2];
        result.v[3] = value.row(0)[3];
        result.v[4] = value.row(1)[0];
        result.v[5] = value.row(1)[1];
        result.v[6] = value.row(1)[2];
        result.v[7] = value.row(1)[3];
        result.v[8] = value.row(2)[0];
        result.v[9] = value.row(2)[1];
        result.v[10] = value.row(2)[2];
        result.v[11] = value.row(2)[3];
        result.v[12] = value.row(3)[0];
        result.v[13] = value.row(3)[1];
        result.v[14] = value.row(3)[2];
        result.v[15] = value.row(3)[3];
        return result;
    }

    void getValueFromMap(bool& val, const ValueMap& values, const std::string& name)
    {
        val = getAndReturnValue<bool>(values, name);
    }

    void getValueFromMap(int& val, const ValueMap& values, const std::string& name)
    {
        val = getAndReturnValue<int>(values, name);
    }

    void getValueFromMap(float& val, const ValueMap& values, const std::string& name)
    {
        val = getAndReturnValue<float>(values, name);
    }

    void getValueFromMap(std::string& val, const ValueMap& values, const std::string& name)
    {
        val = getAndReturnValue<std::string>(values, name);
    }

    void getValueFromMap(Eigen::Vector3f& val, const ValueMap& values, const std::string& name)
    {
        val = getAndReturnValue<Eigen::Vector3f>(values, name);
    }

    void getValueFromMap(Eigen::Matrix4f& val, const ValueMap& values, const std::string& name)
    {
        val = getAndReturnValue<Eigen::Matrix4f>(values, name);
    }







}
