#pragma once

#include <ArmarXCore/util/CPPUtility/cnull.h>

#include <ArmarXGui/interface/RemoteGuiInterface.h>

#include <functional>

class QWidget;
class QObject;
class QString;

namespace armarx::RemoteGui
{

    std::string toUtf8(QString const& qstring);
    QString toQString(std::string const& string);

    using QWidgetPtr = QWidget* ;
    using CreateWidgetCallback = std::function<QWidgetPtr(WidgetPtr const&)>;

    struct WidgetHandler
    {
        virtual ~WidgetHandler() = default;

        virtual bool isValid(Widget const& desc, std::ostream& out = cnull) const = 0;

        virtual QWidget* createWidget(Widget const& desc,
                                      ValueVariant const& initialValue,
                                      CreateWidgetCallback const& createChild,
                                      const QObject* stateChangeReceiver,
                                      const char* stateChangeSlot) const = 0;

        virtual void updateGui(Widget const& desc,
                               QWidget* widget,
                               ValueVariant const& value) const = 0;

        virtual RemoteGui::ValueVariant handleGuiChange(Widget const& desc,
                QWidget* widget) const = 0;

        virtual std::string getHandlerT() const = 0;
    };

}
