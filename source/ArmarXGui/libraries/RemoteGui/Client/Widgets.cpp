#include "Widgets.h"

#include "Impl.h"

namespace armarx::RemoteGui::Client
{

    Widget::Widget(void* iceWidget)
        : impl(new WidgetImpl)
    {
        impl->desc = static_cast<RemoteGui::Widget*>(iceWidget);

        // Set a default name, so the client code does not need to bother
        static std::atomic<std::uint64_t> counter{0};
        std::uint64_t uniqueNumber = counter++;
        setName(std::to_string(uniqueNumber));
    }

    void Widget::setName(const std::string& name)
    {
        impl->desc->name = name;
    }

    const std::string& Widget::getName() const
    {
        return impl->desc->name;
    }

    Label::Label()
        : Widget(new RemoteGui::Label)
    {
    }

    Label::Label(const std::string& text)
        : Label()
    {
        setText(text);
    }

    void Label::setText(const std::string& text)
    {
        impl->setValue(text);
    }

    IntSpinBox::IntSpinBox()
        : Widget(new RemoteGui::IntSpinBox)
    {
        setValue(0);
    }

    void IntSpinBox::setRange(int min, int max)
    {
        auto* concrete = static_cast<RemoteGui::IntSpinBox*>(impl->desc.get());
        concrete->min = min;
        concrete->max = max;
    }

    void IntSpinBox::setValue(int newValue)
    {
        impl->setValue(newValue);
    }

    bool IntSpinBox::hasValueChanged() const
    {
        return impl->hasValueChanged();
    }

    int IntSpinBox::getValue() const
    {
        return impl->getValue<int>();
    }

    HBoxLayout::HBoxLayout()
        : ContainerWidget(new RemoteGui::HBoxLayout)
    {
    }

    HBoxLayout::HBoxLayout(std::initializer_list<Widget> children)
        : HBoxLayout()
    {
        addChildren(children);
    }

    VBoxLayout::VBoxLayout()
        : ContainerWidget(new RemoteGui::VBoxLayout)
    {
    }

    VBoxLayout::VBoxLayout(std::initializer_list<Widget> children)
        : VBoxLayout()
    {
        addChildren(children);
    }

    void ContainerWidget::addChild(const Widget& child)
    {
        impl->addChild(child.impl);
    }

    void ContainerWidget::addChildren(std::initializer_list<Widget> children)
    {
        for (auto& child : children)
        {
            addChild(child);
        }
    }

    VSpacer::VSpacer()
        : Widget(new RemoteGui::VSpacer)
    {
    }

    HSpacer::HSpacer()
        : Widget(new RemoteGui::HSpacer)
    {
    }

    Button::Button()
        : Widget(new RemoteGui::Button)
    {
        impl->desc->defaultValue = makeValue(0);
    }

    void Button::setLabel(const std::string& label)
    {
        auto* concrete = static_cast<RemoteGui::Button*>(impl->desc.get());
        concrete->label = label;
    }

    bool Button::wasClicked() const
    {
        return impl->tab->getButton(getName()).clicked();
    }

    LineEdit::LineEdit()
        : Widget(new RemoteGui::LineEdit)
    {
        setValue("");
    }

    std::string LineEdit::getValue()
    {
        return impl->getValue<std::string>();
    }

    void LineEdit::setValue(const std::string& text)
    {
        auto* concrete = static_cast<RemoteGui::LineEdit*>(impl->desc.get());
        concrete->defaultValue = makeValue(text);
    }

    bool LineEdit::hasValueChanged() const
    {
        return impl->hasValueChanged();
    }

    ComboBox::ComboBox()
        : Widget(new RemoteGui::ComboBox)
    {
    }

    void ComboBox::addOption(const std::string& option)
    {
        auto* concrete = static_cast<RemoteGui::ComboBox*>(impl->desc.get());
        concrete->options.push_back(option);

        // Set the default value to the first option that is set
        if (concrete->defaultValue.type == VALUE_VARIANT_EMPTY)
        {
            concrete->defaultValue = makeValue(option);
        }
    }

    void ComboBox::addOptions(std::initializer_list<std::string> options)
    {
        for (auto& option : options)
        {
            addOption(option);
        }
    }

    void ComboBox::setOptions(const std::vector<std::string>& options)
    {
        auto* concrete = static_cast<RemoteGui::ComboBox*>(impl->desc.get());
        concrete->options = options;
        if (options.size() > 0)
        {
            concrete->defaultValue = makeValue(options.front());
        }
    }

    void ComboBox::setValue(const std::string& newValue)
    {
        impl->setValue(newValue);
    }

    std::string ComboBox::getValue() const
    {
        return impl->getValue<std::string>();
    }

    void ComboBox::setIndex(int index)
    {
        auto* concrete = static_cast<RemoteGui::ComboBox*>(impl->desc.get());
        if (index < 0 || index >= (int)concrete->options.size())
        {
            throw std::runtime_error("ComboBox::setIndex(i) Index i is invalid");
        }
        setValue(concrete->options[index]);
    }

    int ComboBox::getIndex() const
    {
        auto* concrete = static_cast<RemoteGui::ComboBox*>(impl->desc.get());
        std::string value = getValue();
        for (int i = 0; i < (int)concrete->options.size(); ++i)
        {
            if (concrete->options[i] == value)
            {
                return i;
            }
        }
        return -1;
    }

    bool ComboBox::hasValueChanged() const
    {
        return impl->hasValueChanged();
    }

    CheckBox::CheckBox()
        : Widget(new RemoteGui::CheckBox)
    {
        setValue(false);
    }

    bool CheckBox::getValue() const
    {
        return impl->getValue<bool>();
    }

    void CheckBox::setValue(bool newValue)
    {
        impl->setValue(newValue);
    }

    bool CheckBox::hasValueChanged() const
    {
        return impl->hasValueChanged();
    }

    ToggleButton::ToggleButton()
        : Widget(new RemoteGui::ToggleButton)
    {
        setValue(false);
    }

    void ToggleButton::setLabel(const std::string& label)
    {
        auto* concrete = static_cast<RemoteGui::ToggleButton*>(impl->desc.get());
        concrete->label = label;
    }

    bool ToggleButton::getValue() const
    {
        return impl->getValue<bool>();
    }

    void ToggleButton::setValue(bool newValue)
    {
        impl->setValue(newValue);
    }

    bool ToggleButton::hasValueChanged() const
    {
        return impl->hasValueChanged();
    }

    IntSlider::IntSlider()
        : Widget(new RemoteGui::IntSlider)
    {
        setValue(0);
    }

    void IntSlider::setRange(int min, int max)
    {
        auto* concrete = static_cast<RemoteGui::IntSlider*>(impl->desc.get());
        concrete->min = min;
        concrete->max = max;
    }

    int IntSlider::getValue() const
    {
        return impl->getValue<int>();
    }

    void IntSlider::setValue(int newValue)
    {
        impl->setValue(newValue);
    }

    bool IntSlider::hasValueChanged() const
    {
        return impl->hasValueChanged();
    }

    FloatSpinBox::FloatSpinBox()
        : Widget(new RemoteGui::FloatSpinBox)
    {
        setValue(0.0f);
    }

    void FloatSpinBox::setRange(float min, float max)
    {
        auto* concrete = static_cast<RemoteGui::FloatSpinBox*>(impl->desc.get());
        concrete->min = min;
        concrete->max = max;
    }

    void FloatSpinBox::setSteps(int steps)
    {
        auto* concrete = static_cast<RemoteGui::FloatSpinBox*>(impl->desc.get());
        concrete->steps = steps;
    }

    void FloatSpinBox::setDecimals(int decimals)
    {
        auto* concrete = static_cast<RemoteGui::FloatSpinBox*>(impl->desc.get());
        concrete->decimals = decimals;
    }

    float FloatSpinBox::getValue() const
    {
        return impl->getValue<float>();
    }

    void FloatSpinBox::setValue(float newValue)
    {
        impl->setValue(newValue);
    }

    bool FloatSpinBox::hasValueChanged() const
    {
        return impl->hasValueChanged();
    }

    FloatSlider::FloatSlider()
        : Widget(new RemoteGui::FloatSlider)
    {
        setValue(0.0f);
    }

    void FloatSlider::setRange(float min, float max)
    {
        auto* concrete = static_cast<RemoteGui::FloatSlider*>(impl->desc.get());
        concrete->min = min;
        concrete->max = max;
    }

    void FloatSlider::setSteps(int steps)
    {
        auto* concrete = static_cast<RemoteGui::FloatSlider*>(impl->desc.get());
        concrete->steps = steps;
    }

    float FloatSlider::getValue() const
    {
        return impl->getValue<float>();
    }

    void FloatSlider::setValue(float newValue)
    {
        impl->setValue(newValue);
    }

    bool FloatSlider::hasValueChanged() const
    {
        return impl->hasValueChanged();
    }

    GroupBox::GroupBox()
        : ContainerWidget(new RemoteGui::GroupBox)
    {
    }

    GroupBox::GroupBox(std::initializer_list<Widget> children)
        : GroupBox()
    {
        for (auto& child : children)
        {
            addChild(child);
        }
    }

    void GroupBox::setLabel(const std::string& text)
    {
        auto* concrete = static_cast<RemoteGui::GroupBox*>(impl->desc.get());
        concrete->label = text;
    }

    void GroupBox::setCollapsed(bool collapsed)
    {
        auto* concrete = static_cast<RemoteGui::GroupBox*>(impl->desc.get());
        concrete->collapsed = collapsed;
    }

    GridLayout::GridLayout()
        : Widget(new RemoteGui::GridLayout)
    {
    }

    GridLayout& GridLayout::add(const Widget& child, Pos pos, Span span)
    {
        impl->addChild(child.impl);

        auto* concrete = static_cast<RemoteGui::GridLayout*>(impl->desc.get());
        concrete->childrenLayoutInfo.push_back({pos.row, pos.column, span.rows, span.columns});

        return *this;
    }

}
