#pragma once

#include <memory>
#include <string>
#include <vector>

namespace armarx::RemoteGui::Client
{

    /*
     * This is a RemoteGui client library which avoids including unnecessary headers.
     * The RemoteGui Ice interface now includes a bunch of C++ header files which
     * slow down compilation dramatically. To avoid this, this client library uses
     * private implementation to prevent the inclusion of the header file.
     * Please do not include any boost, Eigen, Simox or other headers in this file
     * since this would destroy the whole purpose why this library exists.
     */

    struct WidgetImpl;

    struct Widget
    {
        Widget(void* iceWidget);

        void setName(std::string const& name);
        std::string const& getName() const;

        std::shared_ptr<WidgetImpl> impl;
    };

    struct Label : Widget
    {
        Label();

        Label(std::string const& text);

        void setText(std::string const& text);
    };

    struct LineEdit : Widget
    {
        LineEdit();

        std::string getValue();
        void setValue(std::string const& text);

        bool hasValueChanged() const;
    };

    struct ComboBox : Widget
    {
        ComboBox();

        void addOption(std::string const& option);

        void addOptions(std::initializer_list<std::string> options);

        void setOptions(std::vector<std::string> const& options);

        void setValue(std::string const& newValue);
        std::string getValue() const;

        bool hasValueChanged() const;

        void setIndex(int index);
        int getIndex() const;
    };

    struct IntSpinBox : Widget
    {
        IntSpinBox();

        void setRange(int min, int max);

        int getValue() const;
        void setValue(int newValue);

        bool hasValueChanged() const;
    };

    struct IntSlider : Widget
    {
        IntSlider();

        void setRange(int min, int max);

        int getValue() const;
        void setValue(int newValue);

        bool hasValueChanged() const;
    };

    struct FloatSpinBox : Widget
    {
        FloatSpinBox();

        void setRange(float min, float max);
        void setSteps(int steps);
        void setDecimals(int decimals);

        float getValue() const;
        void setValue(float newValue);

        bool hasValueChanged() const;
    };

    struct FloatSlider : Widget
    {
        FloatSlider();

        void setRange(float min, float max);
        void setSteps(int steps);

        float getValue() const;
        void setValue(float newValue);

        bool hasValueChanged() const;
    };

    struct Button : Widget
    {
        Button();

        void setLabel(std::string const& label);

        bool wasClicked() const;
    };

    struct CheckBox : Widget
    {
        CheckBox();

        bool getValue() const;
        void setValue(bool newValue);

        bool hasValueChanged() const;
    };

    struct ToggleButton : Widget
    {
        ToggleButton();

        void setLabel(std::string const& label);

        bool getValue() const;
        void setValue(bool newValue);

        bool hasValueChanged() const;
    };

    struct ContainerWidget : Widget
    {
        using Widget::Widget;

        void addChild(Widget const& child);

        void addChildren(std::initializer_list<Widget> children);
    };

    struct HBoxLayout : ContainerWidget
    {
        HBoxLayout();

        HBoxLayout(std::initializer_list<Widget> children);
    };

    struct VBoxLayout : ContainerWidget
    {
        VBoxLayout();

        VBoxLayout(std::initializer_list<Widget> children);
    };

    struct Pos
    {
        int row;
        int column;
    };

    struct Span
    {
        int rows;
        int columns;
    };

    struct GridLayout : Widget
    {
        GridLayout();

        GridLayout& add(Widget const& child, Pos pos, Span span = Span{1, 1});
    };

    struct GroupBox : ContainerWidget
    {
        GroupBox();

        GroupBox(std::initializer_list<Widget> children);

        void setLabel(std::string const& text);

        void setCollapsed(bool collapsed);
    };

    struct VSpacer : Widget
    {
        VSpacer();
    };

    struct HSpacer : Widget
    {
        HSpacer();
    };


}
