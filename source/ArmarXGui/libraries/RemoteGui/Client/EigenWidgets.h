#pragma once

#include <ArmarXGui/libraries/RemoteGui/Client/Widgets.h>

// Warning: These includes are heavy. Therefore, these widgets are only available if you need them
#include <Eigen/Core>

namespace armarx::RemoteGui::Client
{

    struct Vector3Widget : HBoxLayout
    {
        FloatSpinBox x;
        FloatSpinBox y;
        FloatSpinBox z;

        Vector3Widget();

        Eigen::Vector3f getValue() const;

        void setValue(Eigen::Vector3f value);

        void setRange(float allMin, float allMax);

        void setSteps(int steps);

        void setDecimals(int decimals);

        bool hasValueChanged() const;
    };

    struct PoseWidget : HBoxLayout
    {
        FloatSpinBox x;
        FloatSpinBox y;
        FloatSpinBox z;

        FloatSpinBox roll;
        FloatSpinBox pitch;
        FloatSpinBox yaw;

        PoseWidget();

        Eigen::Matrix4f getPose() const;

        Eigen::Vector3f getPosition() const;
        void setPosition(Eigen::Vector3f value);

        Eigen::Matrix3f getOrientation() const;
        void setOrientation(Eigen::Matrix3f const& m);

        void setRangePosition(float allMin, float allMax);
        void setStepsPosition(int steps);

        void setRangeRPY(float allMin, float allMax);

        void setStepsRPY(int steps);

        void setDecimalsPosition(int decimals);
        void setDecimalsRPY(int decimals);
    };

}
