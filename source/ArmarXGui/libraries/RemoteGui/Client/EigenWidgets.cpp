#include "EigenWidgets.h"

// Warning: This header includes Eigen/Dense for no particular reasons besides some metaprogramming shennanigans
//          Do not use the SimoxUtility/math headers in any header files, since they increase compile time dramatically
#include <SimoxUtility/math/convert/rpy_to_mat3f.h>
#include <SimoxUtility/math/convert/mat3f_to_rpy.h>

namespace armarx::RemoteGui::Client
{

    Vector3Widget::Vector3Widget()
    {
        addChildren(
        {
            Label("x:"), x,
            Label("y:"), y,
            Label("z:"), z,
        });

        setRange(-5000.0f, 5000.0f);
        setSteps(100);
    }

    Eigen::Vector3f Vector3Widget::getValue() const
    {
        Eigen::Vector3f result;
        result.x() = x.getValue();
        result.y() = y.getValue();
        result.z() = z.getValue();
        return result;
    }

    void Vector3Widget::setValue(Eigen::Vector3f value)
    {
        x.setValue(value.x());
        y.setValue(value.y());
        z.setValue(value.z());
    }

    void Vector3Widget::setRange(float allMin, float allMax)
    {
        x.setRange(allMin, allMax);
        y.setRange(allMin, allMax);
        z.setRange(allMin, allMax);
    }

    void Vector3Widget::setSteps(int steps)
    {
        x.setSteps(steps);
        y.setSteps(steps);
        z.setSteps(steps);
    }

    void Vector3Widget::setDecimals(int decimals)
    {
        x.setDecimals(decimals);
        y.setDecimals(decimals);
        z.setDecimals(decimals);
    }

    bool Vector3Widget::hasValueChanged() const
    {
        return x.hasValueChanged() || y.hasValueChanged() || z.hasValueChanged();
    }

    PoseWidget::PoseWidget()
    {
        addChildren(
        {
            Label("pos:"), x, y, z,
            Label("rpy:"), roll, pitch, yaw,
        });

        setRangeRPY(-M_PI, M_PI);
        setStepsRPY(30);
        setDecimalsRPY(2);

        setRangePosition(-5000.0f, 5000.0f);
        setStepsPosition(100);
        setDecimalsPosition(0);
    }

    Eigen::Matrix4f PoseWidget::getPose() const
    {
        Eigen::Matrix4f result;
        result.block<3, 1>(0, 3) = getPosition();
        result.block<3, 3>(0, 0) = getOrientation();
        result.block<1, 4>(3, 0) = Eigen::Vector4f::UnitW();
        return result;
    }

    Eigen::Vector3f PoseWidget::getPosition() const
    {
        Eigen::Vector3f result;
        result.x() = x.getValue();
        result.y() = y.getValue();
        result.z() = z.getValue();
        return result;
    }

    void PoseWidget::setPosition(Eigen::Vector3f value)
    {
        x.setValue(value.x());
        y.setValue(value.y());
        z.setValue(value.z());
    }

    Eigen::Matrix3f PoseWidget::getOrientation() const
    {
        float r = roll.getValue();
        float p = pitch.getValue();
        float y = yaw.getValue();

        return simox::math::rpy_to_mat3f(r, p, y);
    }

    void PoseWidget::setOrientation(Eigen::Matrix3f const& m)
    {
        Eigen::Vector3f rpy = simox::math::mat3f_to_rpy(m);

        roll.setValue(rpy(0));
        pitch.setValue(rpy(1));
        yaw.setValue(rpy(2));
    }

    void PoseWidget::setRangePosition(float allMin, float allMax)
    {
        x.setRange(allMin, allMax);
        y.setRange(allMin, allMax);
        z.setRange(allMin, allMax);
    }

    void PoseWidget::setStepsPosition(int steps)
    {
        x.setSteps(steps);
        y.setSteps(steps);
        z.setSteps(steps);
    }

    void PoseWidget::setRangeRPY(float allMin, float allMax)
    {
        roll.setRange(allMin, allMax);
        pitch.setRange(allMin, allMax);
        yaw.setRange(allMin, allMax);
    }

    void PoseWidget::setStepsRPY(int steps)
    {
        roll.setSteps(steps);
        pitch.setSteps(steps);
        yaw.setSteps(steps);
    }

    void PoseWidget::setDecimalsPosition(int decimals)
    {
        x.setDecimals(decimals);
        y.setDecimals(decimals);
        z.setDecimals(decimals);
    }

    void PoseWidget::setDecimalsRPY(int decimals)
    {
        roll.setDecimals(decimals);
        pitch.setDecimals(decimals);
        yaw.setDecimals(decimals);
    }


}
