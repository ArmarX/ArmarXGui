#pragma once

/*
 * Do not include this header file in your source file!
 * Use the other headers. Otherwise, your compile times will suffer.
 */

// This is the convoluted header, we want to avoid
#include <ArmarXGui/interface/RemoteGuiInterface.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetProxy.h>

#include <memory>
#include <vector>

namespace armarx::RemoteGui::Client
{
    struct WidgetImpl
    {
        RemoteGui::WidgetPtr desc;
        RemoteGui::TabProxy* tab = nullptr;
        std::vector<std::shared_ptr<WidgetImpl>> children;

        template <typename ValueT>
        void setValue(ValueT const& newValue)
        {
            desc->defaultValue = makeValue(newValue);

            if (tab)
            {
                tab->getValue<ValueT>(desc->name).set(newValue);
            }
        }

        template <typename ValueT>
        auto getValue() const
        {
            if (tab)
            {
                return tab->getValue<ValueT>(desc->name).get();
            }
            else
            {
                throw std::runtime_error("Tab is not initialized. Call Tab::create before getting stuff!");
            }
        }

        bool hasValueChanged() const
        {
            return tab->hasValueChanged(desc->name);
        }

        void addChild(std::shared_ptr<WidgetImpl> const& child)
        {
            desc->children.push_back(child->desc);
            children.push_back(child);
        }
    };

    struct TabImpl
    {
        RemoteGui::TabProxy proxy;
    };

}
