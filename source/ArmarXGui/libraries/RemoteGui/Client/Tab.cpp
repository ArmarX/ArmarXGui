#include "Tab.h"

#include "Widgets.h"
#include "Impl.h"

namespace armarx::RemoteGui::Client
{
    static void initializeTab(WidgetImpl& widget, RemoteGui::TabProxy* tab)
    {
        widget.tab = tab;
        for (auto& child : widget.children)
        {
            initializeTab(*child, tab);
        }
    }

    Tab::Tab()
        : impl(new TabImpl)
    {
    }

    Tab::Tab(Tab&& other)
        : impl(std::move(other.impl))
    {
    }

    Tab::~Tab()
    {
    }

    void Tab::create(const std::string& name, const Ice::ObjectPrx& remoteGuiObject, const Widget& rootWidget)
    {
        RemoteGuiInterfacePrx remoteGui = RemoteGuiInterfacePrx::checkedCast(remoteGuiObject);
        remoteGui->createTab(name, rootWidget.impl->desc);

        impl->proxy = RemoteGui::TabProxy(remoteGui, name);
        initializeTab(*rootWidget.impl, &impl->proxy);
    }

    void Tab::remove()
    {
        impl->proxy.remove();
    }

    void Tab::receiveUpdates()
    {
        impl->proxy.receiveUpdates();
    }

    void Tab::sendUpdates()
    {
        impl->proxy.sendUpdates();
    }

}

