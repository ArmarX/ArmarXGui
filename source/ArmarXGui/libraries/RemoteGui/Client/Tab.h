#pragma once

#include <Ice/Object.h>

#include <memory>
#include <string>

namespace armarx::RemoteGui::Client
{

    struct Widget;

    struct TabImpl;

    struct Tab
    {
        Tab();

        Tab(Tab&& other);

        ~Tab();

        void create(std::string const& name,
                    Ice::ObjectPrx const& remoteGuiObject,
                    Widget const& rootWidget);

        void remove();

        void receiveUpdates();

        void sendUpdates();

        std::unique_ptr<TabImpl> impl;
    };

    // We take the ObjectPrx because we can no longer include our own Ice interface header
    // since it includes a lot of C++ headers which cause massive compile times.
    // So we do this in a type-unsafe manner :(
    void createTab(Ice::ObjectPrx const& remoteGuiObject,
                   std::string const& tabName,
                   Widget const& rootWidget);

}
