#pragma once

#include "WidgetHandlers/Basic.h"
#include "WidgetHandlers/BoolWidgets.h"
#include "WidgetHandlers/FloatWidgets.h"
#include "WidgetHandlers/IntegerWidgets.h"
#include "WidgetHandlers/LayoutWidgets.h"
#include "WidgetHandlers/Matrix4fWidgets.h"
#include "WidgetHandlers/StaticWidgets.h"
#include "WidgetHandlers/StringWidgets.h"
#include "WidgetHandlers/Vector3fWidgets.h"
