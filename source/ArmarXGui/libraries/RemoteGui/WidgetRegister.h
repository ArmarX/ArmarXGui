#pragma once

#include "WidgetHandler.h"

#include <ArmarXGui/interface/RemoteGuiInterface.h>

namespace armarx::RemoteGui
{
    WidgetHandler const& getWidgetHandler(WidgetPtr const& desc);
}

