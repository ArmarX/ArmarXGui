#include "WidgetHandler.h"

#include <QString>

namespace armarx::RemoteGui
{
    std::string toUtf8(QString const& qstring)
    {
        return std::string(qstring.toUtf8().constData());
    }

    QString toQString(std::string const& string)
    {
        return QString::fromUtf8(string.c_str());
    }
}
