#include "RemoteGuiBase.h"

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/time/CycleUtil.h>


namespace armarx::RemoteGui
{

    RemoteGuiBase::RemoteGuiBase(const RemoteGuiInterfacePrx& remoteGui)
        : remoteGui(remoteGui)
    {
    }

    void RemoteGuiBase::run()
    {
        constexpr int kCycleDurationMs = 100;

        CycleUtil c(kCycleDurationMs);
        while (!runningTask->isStopped())
        {
            tab.receiveUpdates();

            handleEvents(tab);

            tab.sendUpdates();

            c.waitForCycleDuration();
        }
    }

    void RemoteGuiBase::createTab(const WidgetPtr& widget)
    {
        ARMARX_INFO << "RemoteGui: creating tab " << tabName();
        remoteGui->createTab(tabName(), widget);
        tab = TabProxy(remoteGui, tabName());

        runningTask = new RunningTask<RemoteGuiBase>(this, &RemoteGuiBase::run),
        runningTask->start();
    }


    void RemoteGuiBase::shutdown()
    {
        if (!runningTask->isStopped())
        {
            runningTask->stop();
        }

        ARMARX_DEBUG << "Removing tab: " << tabName();
        remoteGui->removeTab(tabName());
    }

    armarx::RemoteGui::TabProxy& RemoteGuiBase::getTab()
    {
        return tab;
    }

    const armarx::RemoteGui::TabProxy& RemoteGuiBase::getTab() const
    {
        return tab;
    }

    RemoteGuiInterfacePrx& RemoteGuiBase::getRemoteGui()
    {
        return remoteGui;
    }

    const RemoteGuiInterfacePrx& RemoteGuiBase::getRemoteGui() const
    {
        return remoteGui;
    }

} // namespace armarx::RemoteGui
