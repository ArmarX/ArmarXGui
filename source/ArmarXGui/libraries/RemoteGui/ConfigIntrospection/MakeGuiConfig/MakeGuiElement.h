#pragma once

#include <SimoxUtility/meta/type_traits/is_any_of.h>
#include <SimoxUtility/meta/type_traits/is_std_array.h>

#include "../../WidgetBuilder.h"
#include "../Common.h"

namespace armarx::meta::cfg
{
    template<class CL, class MT, MT CL::* ptr>
    struct gui_definition_create_element_details : element_details<CL, MT, ptr>
    {
        using element = element_details<CL, MT, ptr>;
        static void SetMin(auto& builder, [[maybe_unused]] MT defaultValue)
        {
            if constexpr(is_not_undefined_t(element::min))
            {
                builder.min(element::min);
            }
            else
            {
                builder.min(std::min(MT{0}, defaultValue));
            }
        }
        static void SetMax(auto& builder, [[maybe_unused]] MT defaultValue)
        {
            if constexpr(is_not_undefined_t(element::max))
            {
                builder.max(element::max);
            }
            else
            {
                builder.max(std::max(MT{100}, defaultValue));
            }
        }
        static void SetDescription(auto& builder)
        {
            if constexpr(is_not_undefined_t(element::description))
            {
                builder.toolTip(element::description);
            }
        }
        static void SetSteps(auto& builder)
        {
            if constexpr(is_not_undefined_t(element::steps))
            {
                builder.steps(element::steps);
            }
        }
        static void SetDecimals(auto& builder)
        {
            if constexpr(is_not_undefined_t(element::decimals))
            {
                builder.decimals(element::decimals);
            }
        }

        static std::string GetLabel(const std::string& name)
        {
            if constexpr(is_not_undefined_t(element::label))
            {
                return element::label;
            }
            return name;
        }
        static auto MakeLabel(const std::string& name)
        {
            auto b = RemoteGui::makeTextLabel(GetLabel(name));
            SetDescription(b);
            return b;
        }
    };
}
//element def (variant types except int)
namespace armarx::meta::cfg
{
    template<class CL, class MT, MT CL::* ptr, class = void>
    struct gui_definition_create_element : std::false_type {};

#define create_specalization_for_type(type)                                     \
    template<class CL, type CL::* ptr>                                          \
    struct gui_definition_create_element<CL, type, ptr, void> : std::true_type  \
    {                                                                           \
        using element = gui_definition_create_element_details<CL, type, ptr>;   \
        static std::vector<RemoteGui::WidgetPtr>                                \
        create(type val, const std::string& prefix, const std::string& name);   \
    };                                                                          \
    template<class CL, type CL::* ptr>                                          \
    std::vector<RemoteGui::WidgetPtr>                                           \
    gui_definition_create_element<CL, type, ptr>::create(                       \
            type val, const std::string& prefix, const std::string& name)       \

    create_specalization_for_type(float)
    {
        ARMARX_TRACE;
        using widget_t = typename element::widget_t;
        static constexpr bool is_spinbox = simox::meta::is_any_of_v<widget_t, RemoteGui::FloatSpinBox, undefined_t>;
        static constexpr bool is_slider = std::is_same_v<RemoteGui::FloatSlider, widget_t>;

        static_assert(is_spinbox || is_slider, "the given type for element_widget can't be used");

        auto w = [&]
        {
            if constexpr(is_slider)
            {
                return RemoteGui::makeFloatSlider(ConcatID(prefix, name)).value(val);
            }
            else
            {
                return RemoteGui::makeFloatSpinBox(ConcatID(prefix, name)).value(val);
            }
        }();
        element::SetMin(w, 0);
        element::SetMax(w, 100);
        element::SetDescription(w);
        element::SetSteps(w);
        if constexpr(is_spinbox)
        {
            element::SetDecimals(w);
        }
        else
        {
            static_assert(!is_not_undefined_t(element::decimals), "element_decimals can't be set for a slider");
        }

        return {element::MakeLabel(name), w};
    }

    create_specalization_for_type(bool)
    {
        ARMARX_TRACE;
        using widget_t = typename element::widget_t;
        static constexpr bool is_checkbox = simox::meta::is_any_of_v<widget_t, RemoteGui::CheckBox, undefined_t>;
        static constexpr bool is_toggleButton = std::is_same_v<RemoteGui::ToggleButton, widget_t>;

        static_assert(is_checkbox || is_toggleButton, "the given type for element_widget can't be used");

        auto w = [&]
        {
            if constexpr(is_checkbox)
            {
                return RemoteGui::makeCheckBox(ConcatID(prefix, name)).value(val);
            }
            else
            {
                return RemoteGui::makeToggleButton(ConcatID(prefix, name)).value(val);
            }
        }();
        w.label(element::GetLabel(name));

        static_assert(!is_not_undefined_t(element::min), "element_min can't be set for bool widgets");
        static_assert(!is_not_undefined_t(element::max), "element_max can't be set for bool widgets");

        element::SetDescription(w);

        static_assert(!is_not_undefined_t(element::decimals), "element_decimals can't be set for bool widgets");
        static_assert(!is_not_undefined_t(element::steps), "element_steps can't be set for bool widgets");
        return {w};
    }

    create_specalization_for_type(std::string)
    {
        ARMARX_TRACE;
        return
        {
            RemoteGui::makeTextLabel(name),
            RemoteGui::makeLineEdit(ConcatID(prefix, name)).value(val) ///TODO
        };
    }

    create_specalization_for_type(Eigen::Matrix4f)
    {
        ARMARX_TRACE;
        return
        {
            RemoteGui::makeTextLabel(name),
            RemoteGui::makePosRPYSpinBoxes(ConcatID(prefix, name)).value(val) ///TODO
        };
    }

    create_specalization_for_type(Eigen::Vector3f)
    {
        ARMARX_TRACE;
        using widget_t = typename element::widget_t;
        static constexpr bool is_spinbox = simox::meta::is_any_of_v<widget_t, RemoteGui::FloatSpinBox, undefined_t>;
        auto w = RemoteGui::makeVector3fSpinBoxes(ConcatID(prefix, name)).value(val);
        if constexpr(is_not_undefined_t(element::min))
        {
            element::SetMin(w, Eigen::Vector3f(-100, -100, -100));
        }
        else {}
        if constexpr(is_not_undefined_t(element::max))
        {
            element::SetMax(w, Eigen::Vector3f(100, 100, 100));
        }
        else {}
        element::SetSteps(w);
        if constexpr(is_spinbox)
        {
            element::SetDecimals(w);
        }
        else
        {
            static_assert(!is_not_undefined_t(element::decimals), "element_decimals can't be set for a slider");
        }
        return
        {
            RemoteGui::makeTextLabel(name), w ///TODO

        };
    }
#undef create_specalization_for_type
}


//element def (integral types except bool)
namespace armarx::meta::cfg
{
    template<class CL, class MT, MT CL::* ptr>
    struct gui_definition_create_element <
        CL, MT, ptr,
        std::enable_if_t < std::is_integral_v<MT>&& !std::is_same_v<MT, bool >>
                >
                : std::true_type
    {
        using element = gui_definition_create_element_details<CL, MT, ptr>;

        static std::vector<RemoteGui::WidgetPtr>
        create(MT val, const std::string& prefix, const std::string& name)
        {
            ARMARX_TRACE;
            using widget_t = typename element::widget_t;
            static constexpr bool is_spinbox = simox::meta::is_any_of_v<widget_t, RemoteGui::IntSpinBox, undefined_t>;
            static constexpr bool is_slider = std::is_same_v<RemoteGui::IntSlider, widget_t>;

            static_assert(is_spinbox || is_slider, "the given type for element_widget can't be used");

            auto w = [&]
            {
                if constexpr(is_slider)
                {
                    return RemoteGui::makeIntSlider(ConcatID(prefix, name)).value(val);
                }
                else
                {
                    return RemoteGui::makeIntSpinBox(ConcatID(prefix, name)).value(val);
                }
            }();

            element::SetMin(w, 0);
            element::SetMax(w, 100);

            element::SetDescription(w);

            static_assert(!is_not_undefined_t(element::decimals), "element_decimals can't be set for int spin boxes or sliders");
            static_assert(!is_not_undefined_t(element::steps), "element_steps can't be set for int spin boxes or sliders");

            return {element::MakeLabel(name), w};
        }
    };
}

#include <SimoxUtility/meta/enum/adapt_enum.h>
//element def (enum types)
namespace armarx::meta::cfg
{
    template<class CL, class MT, MT CL::* ptr>
    struct gui_definition_create_element <
        CL, MT, ptr,
        std::enable_if_t<simox::meta::is_enum_adapted_v<MT>>
                >
                : std::true_type
    {
        using element = gui_definition_create_element_details<CL, MT, ptr>;

        static std::vector<RemoteGui::WidgetPtr>
        create(MT val, const std::string& prefix, const std::string& name)
        {
            ARMARX_TRACE;
            static_assert(
                simox::meta::is_any_of_v<typename element::widget_t, RemoteGui::ComboBox, undefined_t>,
                "the given type for element_widget can't be used"
            );
            static_assert(is_undefined_t(element::min));
            static_assert(is_undefined_t(element::max));
            static_assert(is_undefined_t(element::decimals));
            static_assert(is_undefined_t(element::steps));
            static_assert(is_undefined_t(element::label));

            auto builder = RemoteGui::makeComboBox(ConcatID(prefix, name));

            const auto& names = simox::meta::enum_names<MT>;

            builder.addOptions(names.template names<std::vector>());
            builder.value(names.to_name(val));
            element::SetDescription(builder);

            return {element::MakeLabel(name), builder};
        }
    };
}
