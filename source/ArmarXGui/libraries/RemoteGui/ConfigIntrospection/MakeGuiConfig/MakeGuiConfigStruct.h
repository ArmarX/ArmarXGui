#pragma once

#include "MakeGuiElement.h"

namespace armarx::meta::cfg
{
    template<class T, class = void>
    struct gui_definition_create : std::false_type {};
}

namespace armarx::meta::cfg::detail
{
    template<class VarName, class CL, class MT, MT CL::* ptr>
    std::vector<RemoteGui::WidgetPtr>
    DoCreateElement(
        const boost::hana::pair<VarName, boost::hana::struct_detail::member_ptr<MT CL::*, ptr>>&,
        const std::string& prefix,
        const CL& val
    )
    {
        ARMARX_TRACE;
        const auto varname = hana::to<char const*>(VarName{});
        const auto& var = val.*ptr;
        using def_create = gui_definition_create<MT>;
        using elem_create = gui_definition_create_element<CL, MT, ptr>;
        if constexpr(def_create::value)
        {
            return {def_create::create(var, prefix, varname)};
        }
        else if constexpr(elem_create::value)
        {
            return elem_create::create(var, prefix, varname);
        }
        else
        {
            static_assert(!std::is_same_v<MT, MT>, "this member type is not handeled!");
            return {};
        }
    }
}

namespace armarx::meta::cfg
{
    template<class T>
    struct gui_definition_create <
        T,
        std::enable_if_t<meta::cfg::gui_definition_enabled_v<T>>
                > : std::true_type
    {
        template<class OverrideLayoutT = undefined_t>
        static auto create(const T& val, const std::string& prefix, const std::string& name)
        {
            using layout_t = first_not_undefined_t <
                             OverrideLayoutT,
                             config_layout<T>,
                             RemoteGui::GroupBox // default value
                             >;

            ARMARX_TRACE;
            namespace hana = boost::hana;
            static constexpr auto accessors = hana::accessors<T>();

            auto [widget, addChildren] = [&]
            {
                static constexpr bool grpbox = std::is_same_v<layout_t, RemoteGui::GroupBox>;
                static constexpr bool sgrid  = std::is_same_v<layout_t, RemoteGui::SimpleGridLayout>;
                static constexpr bool vbox   = std::is_same_v<layout_t, RemoteGui::VBoxLayout>;
                static constexpr bool hbox   = std::is_same_v<layout_t, RemoteGui::HBoxLayout>;

                if constexpr(grpbox || sgrid)
                {
                    RemoteGui::SimpleGridLayoutPtr l = new RemoteGui::SimpleGridLayout;
                    l->columns = 2;
                    auto add = [l](const auto & cs)
                    {
                        if (cs.size() == 1)
                        {
                            l->children.emplace_back(new RemoteGui::Widget);
                            l->children.emplace_back(cs.front());
                        }
                        else
                        {
                            l->children.insert(l->children.end(), cs.begin(), cs.end());
                        }
                    };
                    if constexpr(grpbox)
                    {
                        return std::make_pair(RemoteGui::makeGroupBox(ConcatID(prefix, name)).label(name).addChild(l), add);
                    }
                    else
                    {
                        return std::make_pair(l, add);
                    }
                }
                else if constexpr(vbox || hbox)
                {
                    RemoteGui::WidgetPtr l = new layout_t;
                    auto add = [l](const auto & cs)
                    {
                        l->children.insert(l->children.end(), cs.begin(), cs.end());
                    };
                    return std::make_pair(l, add);
                }
                else
                {
                    static_assert(!std::is_same_v<T, T>, "the given type for config_layout can't be used");
                }
            }();

            hana::for_each(accessors, [&, addChildren=std::ref(addChildren)](auto & e)
            {
                ARMARX_TRACE_LITE;
                using elem_det = decltype(armarx::meta::cfg::to_element_detail(e));
                if constexpr(!elem_det::no_remote_gui)
                {
                    addChildren(detail::DoCreateElement(e, ConcatID(prefix, name), val));
                }
                else
                {
                    // do nothing
                }
            });

            return widget;
        }
        static auto create(const T& val, const std::string& prefix, const std::string& name)
        {
            return create<>(val, prefix, name);
        }
    };
}
