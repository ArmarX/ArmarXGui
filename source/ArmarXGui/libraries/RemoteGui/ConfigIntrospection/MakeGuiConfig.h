#pragma once

#include "MakeGuiConfig/MakeGuiConfigStruct.h"

namespace armarx::RemoteGui
{
    template<class T>
    RemoteGui::WidgetPtr MakeGuiConfig(const std::string& name, const T& val)
    {
        ARMARX_TRACE;
        using maker = armarx::meta::cfg::gui_definition_create<T>;
        static_assert(maker::value, "This type can't be used");
        if (name.empty())
        {
            return maker::template create<RemoteGui::SimpleGridLayout>(val, "", name);
        }
        return maker::create(val, "", name);
    }
}

