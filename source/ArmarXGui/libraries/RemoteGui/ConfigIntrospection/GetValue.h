#pragma once

#include <SimoxUtility/meta/enum/adapt_enum.h>

#include "../Storage.h"

#include "Common.h"

namespace armarx::RemoteGui
{
    template<class T>
    std::enable_if_t<meta::cfg::gui_definition_enabled_v<T>, void>
    getValueFromMap(T& cfg,
                    RemoteGui::ValueMap const& values,
                    std::string const& name)
    {
        ARMARX_TRACE;
        namespace hana = boost::hana;
        static constexpr auto accessors = hana::accessors<T>();
        hana::for_each(accessors, [&](auto & e)
        {
            ARMARX_TRACE_LITE;
            using elem_det = decltype(armarx::meta::cfg::to_element_detail(e));
            if constexpr(!elem_det::no_remote_gui)
            {
                const auto varname = hana::to<char const*>(hana::first(e));
                const auto& id = armarx::meta::cfg::ConcatID(name, varname);
                auto& elem = hana::second(e)(cfg);
                ARMARX_TRACE_LITE;
                getValueFromMap(elem, values, id);
            }
            else
            {
                // do nothing
            }
        });
    }

    template<class T>
    simox::meta::enable_if_enum_adapted_t<T>
    getValueFromMap(T& cfg,
                    RemoteGui::ValueMap const& values,
                    std::string const& name)
    {
        ARMARX_TRACE;
        cfg = simox::meta::enum_names<T>.from_name(getAndReturnValue<std::string>(values, name));
    }

    inline void getValueFromMap(long unsigned int& val, ValueMap const& values, std::string const& name)
    {
        int value;
        getValueFromMap(value, values, name);
        val = value;
    }

}
