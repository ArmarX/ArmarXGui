#pragma once

#include <ArmarXCore/util/CPPUtility/ConfigIntrospection.h>
#include <ArmarXCore/util/CPPUtility/trace.h>

namespace armarx::meta::cfg
{
    template<class T>
    struct gui_definition_enabled
        : std::bool_constant<meta::has_hana_accessor_v<T>> {};

    template<class T>
    static constexpr bool gui_definition_enabled_v =
        gui_definition_enabled<T>::value;
}
