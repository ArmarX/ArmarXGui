#include "WidgetProxy.h"


namespace armarx::RemoteGui
{
    ButtonProxy TabProxy::getButton(const std::string& name)
    {
        return ButtonProxy(this, name);
    }

    bool TabProxy::getButtonClicked(const std::string& name)
    {
        return getButton(name).clicked();
    }

    bool TabProxy::hasValueChanged(const std::string& name)
    {
        ARMARX_TRACE;
        return oldValues.at(name) != currentValues.at(name);
    }
}

