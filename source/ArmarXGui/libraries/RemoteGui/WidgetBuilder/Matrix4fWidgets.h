#pragma once

#include "Basic.h"

namespace armarx::RemoteGui::detail
{
    struct PosRPYSpinBoxesBuilder
        : public ValueMixin<PosRPYSpinBoxes, Eigen::Matrix4f, PosRPYSpinBoxesBuilder>
        , public ToolTipMixin<PosRPYSpinBoxesBuilder>
    {
        PosRPYSpinBoxesBuilder(std::string const& name, float limpos = 1000, float limrpy = M_PI) : ValueMixin(name)
        {
            widget().stepsPos = toIceI(Eigen::Vector3i::Ones() * static_cast<int>(2 * limpos + 1));
            widget().stepsRPY = toIceI(Eigen::Vector3i::Ones() * static_cast<int>(200 * limrpy + 1));
            widget().decimalsPos = toIceI(Eigen::Vector3i::Ones() * 2);
            widget().decimalsRPY = toIceI(Eigen::Vector3i::Ones() * 2);
            widget().minPos = toIceF(Eigen::Vector3f::Ones() * -limpos);
            widget().maxPos = toIceF(Eigen::Vector3f::Ones() * +limpos);
            widget().minRPY = toIceF(Eigen::Vector3f::Ones() * -limrpy);
            widget().maxRPY = toIceF(Eigen::Vector3f::Ones() * +limrpy);
            widget().defaultValue = makeValue(Eigen::Matrix4f{Eigen::Matrix4f::Identity()});
        }


        PosRPYSpinBoxesBuilder& minPos(const Eigen::Vector3f min)
        {
            widget().minPos = toIceF(min);
            return *this;
        }
        PosRPYSpinBoxesBuilder& minRPY(const Eigen::Vector3f min)
        {
            widget().minRPY = toIceF(min);
            return *this;
        }

        PosRPYSpinBoxesBuilder& maxPos(const Eigen::Vector3f max)
        {
            widget().maxPos = toIceF(max);
            return *this;
        }
        PosRPYSpinBoxesBuilder& maxRPY(const Eigen::Vector3f max)
        {
            widget().maxRPY = toIceF(max);
            return *this;
        }

        PosRPYSpinBoxesBuilder& stepsPos(const Eigen::Vector3i steps)
        {
            widget().stepsPos = toIceI(steps);
            return *this;
        }
        PosRPYSpinBoxesBuilder& stepsRPY(const Eigen::Vector3i steps)
        {
            widget().stepsRPY = toIceI(steps);
            return *this;
        }

        PosRPYSpinBoxesBuilder& decimalsPos(const Eigen::Vector3i decimals)
        {
            widget().decimalsPos = toIceI(decimals);
            return *this;
        }
        PosRPYSpinBoxesBuilder& decimalsRPY(const Eigen::Vector3i decimals)
        {
            widget().decimalsRPY = toIceI(decimals);
            return *this;
        }
    };
}


namespace armarx::RemoteGui
{
    inline detail::PosRPYSpinBoxesBuilder makePosRPYSpinBoxes(std::string const& name, float limpos = 1000, float limrpy = M_PI)
    {
        return {name, limpos, limrpy};
    }
}

