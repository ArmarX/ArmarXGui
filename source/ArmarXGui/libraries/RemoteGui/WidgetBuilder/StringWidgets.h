#pragma once

#include "Basic.h"

namespace armarx::RemoteGui::detail
{
    struct LineEditBuilder
        : public ValueMixin<LineEdit, std::string, LineEditBuilder>
        , public ToolTipMixin<LineEditBuilder>
    {
        using ValueMixin::ValueMixin;
    };

    struct ComboBoxBuilder
        : public ValueMixin<ComboBox, std::string, ComboBoxBuilder>
        , public ToolTipMixin<ComboBoxBuilder>
    {
        using ValueMixin::ValueMixin;

        ComboBoxBuilder& options(std::vector<std::string> const& options)
        {
            widget().options = options;
            return *this;
        }
        ComboBoxBuilder& addOptions(std::vector<std::string> const& options)
        {
            widget().options.insert(widget().options.end(), options.begin(), options.end());
            return *this;
        }

        operator WidgetPtr() const override
        {
            ComboBox& w = *dynamic_cast<ComboBox*>(widget_.get());
            if (!w.options.empty())
            {
                const auto it = std::find(
                                    w.options.begin(),
                                    w.options.end(),
                                    getSingleValue<std::string>(w.defaultValue)
                                );
                if (it == w.options.end())
                {
                    w.defaultValue = makeValue(w.options.front());
                }
            }
            return widget_;
        }
    };
}

namespace armarx::RemoteGui
{
    inline detail::LineEditBuilder makeLineEdit(std::string const& name)
    {
        return detail::LineEditBuilder(name);
    }

    inline detail::ComboBoxBuilder makeComboBox(std::string const& name)
    {
        return detail::ComboBoxBuilder(name);
    }
}
