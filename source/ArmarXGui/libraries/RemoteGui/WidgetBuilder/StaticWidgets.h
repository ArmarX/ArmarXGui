#pragma once

#include "Basic.h"

namespace armarx::RemoteGui::detail
{
    struct LabelBuilder
        : public ValueMixin<Label, std::string, LabelBuilder>
        , public ToolTipMixin<LabelBuilder>
    {
        using ValueMixin::ValueMixin;
    };
}

namespace armarx::RemoteGui
{
    inline detail::LabelBuilder makeLabel(std::string const& name)
    {
        return detail::LabelBuilder(name);
    }

    inline detail::LabelBuilder makeTextLabel(std::string const& text)
    {
        return detail::LabelBuilder("").value(text);
    }
}
