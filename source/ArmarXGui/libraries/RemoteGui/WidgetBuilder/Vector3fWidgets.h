#pragma once

#include "Basic.h"

namespace armarx::RemoteGui::detail
{
    template <typename Derived>
    struct Vector3fMinMaxMixin : MinMaxMixin<Derived,  Eigen::Vector3f>
    {
        using Base = MinMaxMixin<Derived,  Eigen::Vector3f>;

        Derived& steps(const Eigen::Vector3i steps)
        {
            Derived& this_ = *static_cast<Derived*>(this);
            this_.widget().steps = toIceI(steps);
            return this_;
        }

        using Base::min;
        using Base::max;

        Derived& min(float v)
        {
            return Base::min({v, v, v});
        }
        Derived& min(std::array<float, 1> v)
        {
            return min(v.at(0));
        }

        Derived& max(float v)
        {
            return Base::max({v, v, v});
        }
        Derived& max(std::array<float, 1> v)
        {
            return max(v.at(0));
        }

        Derived& steps(int v)
        {
            return steps(v, v, v);
        }
        Derived& steps(std::array<int, 1> v)
        {
            return steps(v.at(0));
        }

        Derived& min(float x, float y, float z)
        {
            return Base::min(Eigen::Vector3f{x, y, z});
        }
        Derived& min(std::array<float, 3> v)
        {
            return min(v.at(0), v.at(1), v.at(2));
        }

        Derived& max(float x, float y, float z)
        {
            return Base::max(Eigen::Vector3f{x, y, z});
        }
        Derived& max(std::array<float, 3> v)
        {
            return max(v.at(0), v.at(1), v.at(2));
        }

        Derived& steps(int x, int y, int z)
        {
            return steps(Eigen::Vector3i{x, y, z});
        }
        Derived& steps(std::array<int, 3> v)
        {
            return steps(v.at(0), v.at(1), v.at(2));
        }
    };

    struct Vector3fSpinBoxesBuilder
        : public ValueMixin<Vector3fSpinBoxes, Eigen::Vector3f, Vector3fSpinBoxesBuilder>
        , public Vector3fMinMaxMixin<Vector3fSpinBoxesBuilder>
        , public ToolTipMixin<Vector3fSpinBoxesBuilder>
    {
        Vector3fSpinBoxesBuilder(std::string const& name, float limpos = 1000) : ValueMixin(name)
        {
            widget().steps = toIceI(Eigen::Vector3i::Ones() * static_cast<int>(2 * limpos + 1));
            widget().decimals = toIceI(Eigen::Vector3i::Ones());
            widget().min = toIceF(Eigen::Vector3f::Ones() * -limpos);
            widget().max = toIceF(Eigen::Vector3f::Ones() * +limpos);
            widget().defaultValue = makeValue(Eigen::Vector3f{Eigen::Vector3f::Zero()});
        }

        Vector3fSpinBoxesBuilder& decimals(const Eigen::Vector3i decimals)
        {
            widget().decimals = toIceI(decimals);
            return *this;
        }
        Vector3fSpinBoxesBuilder& decimals(int x, int y, int z)
        {
            return decimals(Eigen::Vector3i(x, y, z));
        }
        Vector3fSpinBoxesBuilder& decimals(int d)
        {
            return decimals(Eigen::Vector3i(d, d, d));
        }
        Vector3fSpinBoxesBuilder& decimals(std::array<int, 1> d)
        {
            return decimals(d.at(0));
        }
    };
}

namespace armarx::RemoteGui
{
    inline detail::Vector3fSpinBoxesBuilder makeVector3fSpinBoxes(std::string const& name, float limpos = 1000)
    {
        return {name, limpos};
    }
}

