#pragma once

#include "Basic.h"

namespace armarx::RemoteGui::detail
{
    template <typename Derived>
    struct IntMinMaxMixin : MinMaxMixin<Derived, int>
    {};

    struct IntSpinBoxBuilder
        : public ValueMixin<IntSpinBox, int, IntSpinBoxBuilder>
        , public IntMinMaxMixin<IntSpinBoxBuilder>
        , public ToolTipMixin<IntSpinBoxBuilder>
    {
        using ValueMixin::ValueMixin;
    };

    struct IntSliderBuilder
        : public ValueMixin<IntSlider, int, IntSliderBuilder>
        , public IntMinMaxMixin<IntSliderBuilder>
        , public ToolTipMixin<IntSliderBuilder>
    {
        using ValueMixin::ValueMixin;
    };

    struct ButtonBuilder
        : public ValueMixin<Button, int, ButtonBuilder>
        , public LabelMixin<ButtonBuilder>
        , public ToolTipMixin<ButtonBuilder>
    {
        using ValueMixin::ValueMixin;
    };
}

namespace armarx::RemoteGui
{
    inline detail::IntSpinBoxBuilder makeIntSpinBox(std::string const& name)
    {
        return detail::IntSpinBoxBuilder(name);
    }

    inline detail::IntSliderBuilder makeIntSlider(std::string const& name)
    {
        return detail::IntSliderBuilder(name);
    }

    inline detail::ButtonBuilder makeButton(std::string const& name)
    {
        auto b = detail::ButtonBuilder(name);
        b.label(name);
        return b;
    }
}
