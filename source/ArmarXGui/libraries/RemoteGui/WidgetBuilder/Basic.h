#pragma once

#include <ArmarXGui/libraries/RemoteGui/Storage.h>
#include <ArmarXGui/interface/RemoteGuiInterface.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <SimoxUtility/math/compare/value_in_limits_of.h>

namespace armarx::RemoteGui::detail
{
    class WidgetBuilder
    {
    public:
        virtual ~WidgetBuilder() = default;

        WidgetBuilder& hidden(bool hidden = true)
        {
            widget_->defaultState.hidden = hidden;
            return *this;
        }

        WidgetBuilder& disabled(bool disabled = true)
        {
            widget_->defaultState.disabled = disabled;
            return *this;
        }

        virtual operator WidgetPtr() const
        {
            return widget_;
        }

    protected:
        WidgetPtr widget_;
    };

    template <typename WidgetT, typename Derived>
    class WidgetMixin : public WidgetBuilder
    {
    public:
        WidgetMixin(std::string const& name)
        {
            widget_ = new WidgetT;
            widget_->name = name;
            widget_->defaultValue = Derived::defaultValue();
        }

    public:
        WidgetT& widget()
        {
            return *dynamic_cast<WidgetT*>(widget_.get());
        }
    };

    template <typename WidgetT, typename Derived>
    struct NoValueMixin : WidgetMixin<WidgetT, Derived>
    {
        using WidgetMixin<WidgetT, Derived>::WidgetMixin;

        static ValueVariant defaultValue()
        {
            return ValueVariant();
        }
    };

    template <typename WidgetT, typename ValueT, typename Derived>
    struct ValueMixin : WidgetMixin<WidgetT, Derived>
    {
        using WidgetMixin<WidgetT, Derived>::WidgetMixin;

        static ValueVariant defaultValue()
        {
            return makeValue(ValueT());
        }

        Derived& value(ValueT const& value)
        {
            Derived& this_ = *static_cast<Derived*>(this);
            this_.widget().defaultValue = makeValue(value);
            return this_;
        }
    };

    template <typename Derived, typename Type>
    struct MinMaxMixin
    {
        Derived& min(Type min)
        {
            Derived& this_ = *static_cast<Derived*>(this);
            this_.widget().min = min;
            return this_;
        }
        Derived& min(std::array<Type, 1> v)
        {
            return min(v.at(0));
        }
        template<class T>
        std::enable_if_t <simox::meta::are_arithmetic_v<Type, T>, Derived&>
        min(T v)
        {
            ARMARX_CHECK(simox::math::value_in_limits_of_type<Type>(v))
                    << VAROUT(v) << " : " << GetTypeString<T>();
            return min(static_cast<Type>(v));
        }
        template<class T>
        std::enable_if_t <simox::meta::are_arithmetic_v<Type, T>, Derived&>
        min(std::array<T, 1> v)
        {
            ARMARX_CHECK(simox::math::value_in_limits_of_type<Type>(v.at(0)))
                    << VAROUT(v.at(0)) << " : " << GetTypeString<T>();
            return min(static_cast<Type>(v.at(0)));
        }


        Derived& max(Type max)
        {
            Derived& this_ = *static_cast<Derived*>(this);
            this_.widget().max = max;
            return this_;
        }
        Derived& max(std::array<Type, 1> v)
        {
            return max(v.at(0));
        }
        template<class T>
        std::enable_if_t <simox::meta::are_arithmetic_v<Type, T>, Derived&>
        max(T v)
        {
            ARMARX_CHECK(simox::math::value_in_limits_of_type<Type>(v))
                    << VAROUT(v) << " : " << GetTypeString<T>();
            return max(static_cast<Type>(v));
        }
        template<class T>
        std::enable_if_t <simox::meta::are_arithmetic_v<Type, T>, Derived&>
        max(std::array<T, 1> v)
        {
            ARMARX_CHECK(simox::math::value_in_limits_of_type<Type>(v.at(0)))
                    << VAROUT(v.at(0)) << " : " << GetTypeString<T>();
            return max(static_cast<Type>(v.at(0)));
        }

        template<class T0, class T1>
        Derived& minmax(T0 lo, T1 hi)
        {
            Derived& this_ = *static_cast<Derived*>(this);
            this_.min(lo);
            return this_.max(hi);
        }
    };

    template <typename Derived>
    struct MinMaxMixin<Derived, Eigen::Vector3f>
    {
        using Type = Eigen::Vector3f;

        Derived& min(Type min)
        {
            Derived& this_ = *static_cast<Derived*>(this);
            this_.widget().min = toIceF(min);
            return this_;
        }
        Derived& min(std::array<Type, 1> v)
        {
            return min(v.at(0));
        }

        Derived& max(Type max)
        {
            Derived& this_ = *static_cast<Derived*>(this);
            this_.widget().max = toIceF(max);
            return this_;
        }
        Derived& max(std::array<Type, 1> v)
        {
            return max(v.at(0));
        }

        template<class T0, class T1>
        Derived& minmax(T0 lo, T1 hi)
        {
            Derived& this_ = *static_cast<Derived*>(this);
            this_.min(lo);
            return this_.max(hi);
        }
    };

    template <typename Derived>
    struct LabelMixin
    {
        Derived& label(std::string const& label)
        {
            Derived& this_ = *static_cast<Derived*>(this);
            this_.widget().label = label;
            return this_;
        }
    };

    template <typename Derived>
    struct ToolTipMixin
    {
        Derived& toolTip(std::string const& toolTip)
        {
            Derived& this_ = *static_cast<Derived*>(this);
            this_.widget().toolTip = toolTip;
            return this_;
        }
    };
}
