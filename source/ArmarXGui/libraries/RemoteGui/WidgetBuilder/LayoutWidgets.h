#pragma once

#include "Basic.h"

namespace armarx::RemoteGui::detail
{
    template <typename Derived>
    struct ChildrenMixin
    {
        Derived& appendChild(WidgetPtr const& child)
        {
            Derived& this_ = *static_cast<Derived*>(this);
            this_.widget().children.push_back(child);
            return this_;
        }
        Derived& prependChild(WidgetPtr const& child)
        {
            Derived& this_ = *static_cast<Derived*>(this);
            this_.widget().children.insert(this_.widget().children.begin(), child);
            return this_;
        }
        Derived& addChild(WidgetPtr const& child)
        {
            return appendChild(child);
        }
        Derived& addChildren(std::vector<WidgetPtr> const& children)
        {
            Derived& this_ = *static_cast<Derived*>(this);
            this_.widget().children.insert(
                this_.widget().children.end(),
                children.begin(), children.end()
            );
            return this_;
        }

        Derived& children(std::vector<WidgetPtr> const& children)
        {
            Derived& this_ = *static_cast<Derived*>(this);
            this_.widget().children = children;
            return this_;
        }

        Derived& child(WidgetPtr const& child)
        {
            Derived& this_ = *static_cast<Derived*>(this);
            this_.widget().children = {child};
            return this_;
        }

        const WidgetPtr& child(std::size_t i)
        {
            Derived& this_ = *static_cast<Derived*>(this);
            return this_.widget().children.at(i);
        }

        Derived& addTextLabel(std::string const& text)
        {
            return appendChild(new Label("", makeValue(text), {}, {}, {}));
        }

        Derived& addHSpacer()
        {
            return appendChild(new HSpacer);
        }

        Derived& addVSpacer()
        {
            return appendChild(new VSpacer);
        }

        Derived& addEmptyWidget()
        {
            return appendChild(new Widget);
        }

        Derived& addHLine()
        {
            return appendChild(new HLine);
        }

        Derived& addVLine()
        {
            return appendChild(new VLine);
        }
    };

    struct HBoxLayoutBuilder
        : public NoValueMixin<HBoxLayout, HBoxLayoutBuilder>
        , public ChildrenMixin<HBoxLayoutBuilder>
    {
        using NoValueMixin::NoValueMixin;
    };

    struct VBoxLayoutBuilder
        : public NoValueMixin<VBoxLayout, VBoxLayoutBuilder>
        , public ChildrenMixin<VBoxLayoutBuilder>
    {
        using NoValueMixin::NoValueMixin;
    };

    struct SimpleGridLayoutBuilder
        : public NoValueMixin<SimpleGridLayout, SimpleGridLayoutBuilder>
        , public ChildrenMixin<SimpleGridLayoutBuilder>
    {
        using Base = ChildrenMixin<SimpleGridLayoutBuilder>;
        using NoValueMixin::NoValueMixin;

        SimpleGridLayoutBuilder& cols(int n)
        {
            widget().columns = n;
            return *this;
        }

        SimpleGridLayoutBuilder& columns(int n)
        {
            widget().columns = n;
            return *this;
        }

        using Base::addChild;
        SimpleGridLayoutBuilder& addChild(WidgetPtr const& child, int colspan)
        {
            SimpleGridLayoutSpanningChildPtr c = new SimpleGridLayoutSpanningChild;
            c->children.emplace_back(child);
            c->columns = colspan;
            return appendChild(c);
        }

        using Base::addTextLabel;
        SimpleGridLayoutBuilder& addTextLabel(std::string const& text, int colspan)
        {
            return addChild(new Label("", makeValue(text), {}, {}, {}), colspan);
        }

        using Base::addHSpacer;
        SimpleGridLayoutBuilder& addHSpacer(int colspan)
        {
            return addChild(new HSpacer, colspan);
        }

        using Base::addVSpacer;
        SimpleGridLayoutBuilder& addVSpacer(int colspan)
        {
            return addChild(new VSpacer, colspan);
        }

        using Base::addEmptyWidget;
        SimpleGridLayoutBuilder& addEmptyWidget(int colspan)
        {
            return addChild(new Widget, colspan);
        }

        using Base::addHLine;
        SimpleGridLayoutBuilder& addHLine(int colspan)
        {
            return addChild(new HLine, colspan);
        }

        using Base::addVLine;
        SimpleGridLayoutBuilder& addVLine(int colspan)
        {
            return addChild(new VLine, colspan);
        }
    };

    struct GroupBoxBuilder
        : public NoValueMixin<GroupBox, GroupBoxBuilder>
        , public ChildrenMixin<GroupBoxBuilder>
        , public LabelMixin<GroupBoxBuilder>
    {
        GroupBoxBuilder(const std::string& name) : NoValueMixin(name)
        {
            label(name);
        }

        GroupBoxBuilder& collapsed(bool isCollapsed = true)
        {
            this->widget().collapsed = isCollapsed;
            return *this;
        }
    };

    struct GridLayoutBuilder
        : public NoValueMixin<GridLayout, GridLayoutBuilder>
    //can't use ChildrenMixin since it is missing the index information
    {
        using NoValueMixin::NoValueMixin;

        GridLayoutBuilder& addChild(WidgetPtr const& child, int row, int col, int spanRow = 1, int spanCol = 1)
        {
            this->widget().children.emplace_back(child);
            this->widget().childrenLayoutInfo.push_back({row, col, spanRow, spanCol});
            return *this;
        }

        GridLayoutBuilder& addTextLabel(std::string const& text, int row, int col, int spanRow = 1, int spanCol = 1)
        {
            return addChild(new Label("", makeValue(text), {}, {}, {}), row, col, spanRow, spanCol);
        }

        GridLayoutBuilder& addHSpacer(int row, int col, int spanRow = 1, int spanCol = 1)
        {
            return addChild(new HSpacer, row, col, spanRow, spanCol);
        }

        GridLayoutBuilder& addVSpacer(int row, int col, int spanRow = 1, int spanCol = 1)
        {
            return addChild(new VSpacer, row, col, spanRow, spanCol);
        }

        GridLayoutBuilder& addEmptyWidget(int row, int col, int spanRow = 1, int spanCol = 1)
        {
            return addChild(new Widget, row, col, spanRow, spanCol);
        }

        GridLayoutBuilder& addHLine(int row, int col, int spanRow = 1, int spanCol = 1)
        {
            return addChild(new HLine, row, col, spanRow, spanCol);
        }

        GridLayoutBuilder& addVLine(int row, int col, int spanRow = 1, int spanCol = 1)
        {
            return addChild(new VLine, row, col, spanRow, spanCol);
        }
    };
}

namespace armarx::RemoteGui
{
    inline detail::HBoxLayoutBuilder makeHBoxLayout(std::string const& name = "")
    {
        return detail::HBoxLayoutBuilder(name);
    }

    inline detail::SimpleGridLayoutBuilder makeSimpleGridLayout(std::string const& name = "")
    {
        return detail::SimpleGridLayoutBuilder(name);
    }

    inline detail::GridLayoutBuilder makeGridLayout(std::string const& name = "")
    {
        return detail::GridLayoutBuilder(name);
    }

    inline detail::VBoxLayoutBuilder makeVBoxLayout(std::string const& name = "")
    {
        return detail::VBoxLayoutBuilder(name);
    }

    inline detail::GroupBoxBuilder makeGroupBox(std::string const& name = "")
    {
        return detail::GroupBoxBuilder(name);
    }
}
