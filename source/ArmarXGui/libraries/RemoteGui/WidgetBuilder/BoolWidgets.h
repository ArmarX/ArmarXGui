#pragma once

#include "Basic.h"

namespace armarx::RemoteGui::detail
{
    struct CheckBoxBuilder
        : public ValueMixin<CheckBox, bool, CheckBoxBuilder>
        , public LabelMixin<CheckBoxBuilder>
        , public ToolTipMixin<CheckBoxBuilder>
    {
        using ValueMixin::ValueMixin;
    };

    struct ToggleButtonBuilder
        : public ValueMixin<ToggleButton, bool, ToggleButtonBuilder>
        , public LabelMixin<ToggleButtonBuilder>
        , public ToolTipMixin<ToggleButtonBuilder>
    {
        using ValueMixin::ValueMixin;
    };
}

namespace armarx::RemoteGui
{
    inline detail::CheckBoxBuilder makeCheckBox(std::string const& name)
    {
        detail::CheckBoxBuilder b(name);
        b.label(name);
        return b;
    }

    inline detail::ToggleButtonBuilder makeToggleButton(std::string const& name)
    {
        detail::ToggleButtonBuilder b(name);
        b.label(name);
        return b;
    }
}
