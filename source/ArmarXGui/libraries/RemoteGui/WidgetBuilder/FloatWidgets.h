#pragma once

#include "Basic.h"

namespace armarx::RemoteGui::detail
{
    template <typename Derived>
    struct FloatMinMaxMixin : MinMaxMixin<Derived, float>
    {
        Derived& steps(int steps)
        {
            Derived& this_ = *static_cast<Derived*>(this);
            this_.widget().steps = steps;
            return this_;
        }
        Derived& steps(std::array<int, 1> v)
        {
            return steps(v.at(0));
        }
    };

    struct FloatSpinBoxBuilder
        : public ValueMixin<FloatSpinBox, float, FloatSpinBoxBuilder>
        , public FloatMinMaxMixin<FloatSpinBoxBuilder>
        , public ToolTipMixin<FloatSpinBoxBuilder>
    {
        using ValueMixin::ValueMixin;

        FloatSpinBoxBuilder& decimals(int decimals)
        {
            widget().decimals = decimals;
            return *this;
        }

        FloatSpinBoxBuilder& decimals(std::array<int, 1> d)
        {
            return decimals(d.at(0));
        }
    };

    struct FloatSliderBuilder
        : public ValueMixin<FloatSlider, float, FloatSliderBuilder>
        , public FloatMinMaxMixin<FloatSliderBuilder>
        , public ToolTipMixin<FloatSliderBuilder>
    {
        using ValueMixin::ValueMixin;
    };
}

namespace armarx::RemoteGui
{
    inline detail::FloatSpinBoxBuilder makeFloatSpinBox(std::string const& name)
    {
        return detail::FloatSpinBoxBuilder(name);
    }

    inline detail::FloatSliderBuilder makeFloatSlider(std::string const& name)
    {
        return detail::FloatSliderBuilder(name);
    }
}
