#pragma once

#include <ArmarXGui/interface/RemoteGuiInterface.h>

#include <ArmarXCore/core/exceptions/LocalException.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <Eigen/Core>

namespace armarx::RemoteGui
{
    Vector3f toIceF(Eigen::Vector3f v);

    Vector3i toIceI(Eigen::Vector3i v);

    Eigen::Vector3f fromIce(Vector3f v);

    Eigen::Vector3i fromIce(Vector3i v);

    const char* getVariantTypeName(ValueVariantType type);

    ValueVariant makeValue(bool value);

    ValueVariant makeValue(int value);

    ValueVariant makeValue(float value);

    ValueVariant makeValue(std::string value);

    ValueVariant makeValue(Eigen::Vector3f const& value);

    ValueVariant makeValue(Eigen::Matrix4f const& value);

    template <ValueVariantType Type_>
    struct Storage
    {
        using Type = std::nullptr_t;
    };

    template <>
    struct Storage<VALUE_VARIANT_BOOL>
    {
        using Type = bool;
    };

    template <>
    struct Storage<VALUE_VARIANT_INT>
    {
        using Type = int;
    };

    template <>
    struct Storage<VALUE_VARIANT_FLOAT>
    {
        using Type = float;
    };

    template <>
    struct Storage<VALUE_VARIANT_STRING>
    {
        using Type = std::string;
    };

    template <>
    struct Storage<VALUE_VARIANT_VECTOR3>
    {
        using Type = Eigen::Vector3f;
    };

    template <>
    struct Storage<VALUE_VARIANT_MATRIX4>
    {
        using Type = Eigen::Matrix4f;
    };



    template <typename T>
    T getSingleValue(ValueVariant const& value, const std::string& name = "");

    template <>
    inline std::nullptr_t getSingleValue<std::nullptr_t>(RemoteGui::ValueVariant const& value, const std::string& name)
    {
        return nullptr;
    }

    template <>
    inline bool getSingleValue<bool>(ValueVariant const& value, const std::string& name)
    {
        if (value.type != VALUE_VARIANT_BOOL)
        {
            throw LocalException()
                    << "Expected type 'bool' but got type '"
                    << getVariantTypeName(value.type) << "', name = '" << name << "'";
        }
        return value.i != 0;
    }

    template <>
    inline int getSingleValue<int>(ValueVariant const& value, const std::string& name)
    {
        if (value.type != VALUE_VARIANT_INT)
        {
            throw LocalException()
                    << "Expected type 'int' but got type '"
                    << getVariantTypeName(value.type) << "', name = '" << name << "'";
        }
        return value.i;
    }

    template <>
    inline float getSingleValue<float>(ValueVariant const& value, const std::string& name)
    {
        if (value.type != VALUE_VARIANT_FLOAT)
        {
            throw LocalException()
                    << "Expected type 'float' but got type '"
                    << getVariantTypeName(value.type) << "', name = '" << name << "'";
        }
        return value.f;
    }

    template <>
    inline std::string getSingleValue<std::string>(ValueVariant const& value, const std::string& name)
    {
        if (value.type != VALUE_VARIANT_STRING)
        {
            throw LocalException()
                    << "Expected type 'string' but got type '"
                    << getVariantTypeName(value.type) << "', name = '" << name << "'";
        }
        return value.s;
    }

    template <>
    inline Eigen::Vector3f getSingleValue<Eigen::Vector3f>(ValueVariant const& value, const std::string& name)
    {
        if (value.type != VALUE_VARIANT_VECTOR3)
        {
            throw LocalException()
                    << "Expected type 'vector3' but got type '"
                    << getVariantTypeName(value.type) << "', name = '" << name << "'";
        }
        if (value.v.size() != 3)
        {
            throw LocalException()
                    << "Expected type 'vector3' (size = 3) but got sequence with size '"
                    << value.v.size() << "', name = '" << name << "'";
        }
        return Eigen::Vector3f(value.v[0], value.v[1], value.v[2]);
    }

    template <>
    inline Eigen::Matrix4f getSingleValue<Eigen::Matrix4f>(ValueVariant const& value, const std::string& name)
    {
        if (value.type != VALUE_VARIANT_MATRIX4)
        {
            throw LocalException()
                    << "Expected type 'matrix4' but got type '"
                    << getVariantTypeName(value.type) << "', name = '" << name << "'";
        }
        if (value.v.size() != 16)
        {
            throw LocalException()
                    << "Expected type 'matrix4' (size = 16) but got sequence with size '"
                    << value.v.size() << "', name = '" << name << "'";
        }
        Eigen::Matrix4f result;
        result.row(0) = Eigen::Vector4f(value.v[0], value.v[1], value.v[2], value.v[3]);
        result.row(1) = Eigen::Vector4f(value.v[4], value.v[5], value.v[6], value.v[7]);
        result.row(2) = Eigen::Vector4f(value.v[8], value.v[9], value.v[10], value.v[11]);
        result.row(3) = Eigen::Vector4f(value.v[12], value.v[13], value.v[14], value.v[15]);
        return result;
    }

    template <typename T>
    T getAndReturnValue(ValueMap const& values, std::string const& name)
    {
        try
        {
            ValueVariant const& variantValue = values.at(name);
            return getSingleValue<T>(variantValue, name);
        }
        catch (...)
        {
            ARMARX_ERROR << GetHandledExceptionString();
            ARMARX_ERROR << "Type: " << GetTypeString<T>();
            ARMARX_ERROR << "RemoteGui::getValue: name.size()" << name.size();
            ARMARX_ERROR << "RemoteGui::getValue: name" << name;
            throw;
        }
    }

    // This function can be overloaded to support more types
    // Example: CartesianWaypointControllerConfig
    //   (in /RobotAPI/source/RobotAPI/libraries/ControllerUIUtility/CartesianWaypointControllerConfig/RemoteGui.h)
    void getValueFromMap(bool& val, ValueMap const& values, std::string const& name);
    void getValueFromMap(int& val, ValueMap const& values, std::string const& name);
    void getValueFromMap(float& val, ValueMap const& values, std::string const& name);
    void getValueFromMap(std::string& val, ValueMap const& values, std::string const& name);
    void getValueFromMap(Eigen::Vector3f& val, ValueMap const& values, std::string const& name);
    void getValueFromMap(Eigen::Matrix4f& val, ValueMap const& values, std::string const& name);

    bool buttonClicked(RemoteGui::ValueMap const& newValues, ValueMap const& oldValues, std::string const& name);

    bool operator == (ValueVariant const& left, ValueVariant const& right);

    bool operator != (ValueVariant const& left, ValueVariant const& right);
}
