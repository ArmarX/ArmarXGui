#pragma once

#include <string>

#include "WidgetHandler.h"
#include "Storage.h"

#include <ArmarXGui/interface/RemoteGuiInterface.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <QWidget>

namespace armarx::RemoteGui
{

    template <typename HandlerT>
    struct TypedWidgetHandler : WidgetHandler
    {
        using RemoteWidgetT = typename HandlerT::RemoteWidgetT;
        using QWidgetT = typename HandlerT::QWidgetT;
        static const RemoteGui::ValueVariantType ValueType = HandlerT::ValueType;
        using ValueT = typename Storage<ValueType>::Type;


        std::string getHandlerT() const override
        {
            return GetTypeString<HandlerT>();
        }

        bool isValid(Widget const& desc, std::ostream& out = cnull) const override
        {
            if (desc.defaultValue.type != ValueType)
            {
                return false;
            }

            auto concreteDesc = dynamic_cast<RemoteWidgetT const*>(&desc);
            ARMARX_CHECK_EXPRESSION(concreteDesc != nullptr);

            return HandlerT::isValid(*concreteDesc, out);
        }

        QWidget* createWidget(Widget const& desc,
                              ValueVariant const& initialValue,
                              CreateWidgetCallback const& createChild,
                              const QObject* stateChangeReceiver,
                              const char* stateChangeSlot) const override
        {
            auto concreteDesc = dynamic_cast<RemoteWidgetT const*>(&desc);
            ARMARX_CHECK_EXPRESSION(concreteDesc != nullptr);

            ARMARX_CHECK_EXPRESSION(initialValue.type == ValueType)
                    << getVariantTypeName(initialValue.type) << " != " << GetTypeString<ValueT>();
            ValueT concreteInitialValue = getSingleValue<ValueT>(initialValue);

            auto* widget = HandlerT::createWidget(*concreteDesc, createChild,
                                                  stateChangeReceiver, stateChangeSlot);

            HandlerT::updateGui(*concreteDesc, widget, concreteInitialValue);

            return widget;
        }

        virtual void updateGui(Widget const& desc,
                               QWidget* widget,
                               ValueVariant const& value) const override
        {
            auto concreteDesc = dynamic_cast<RemoteWidgetT const*>(&desc);
            ARMARX_CHECK_EXPRESSION(concreteDesc != nullptr);

            QWidgetT* concreteWidget = qobject_cast<QWidgetT*>(widget);
            ARMARX_CHECK_EXPRESSION(concreteWidget != nullptr);

            ARMARX_CHECK_EXPRESSION(value.type == ValueType)
                    << getVariantTypeName(value.type) << " != " << GetTypeString<ValueT>();
            ValueT concreteValue = getSingleValue<ValueT>(value);

            HandlerT::updateGui(*concreteDesc, concreteWidget, concreteValue);
        }

        virtual ValueVariant handleGuiChange(Widget const& desc,
                                             QWidget* widget) const override
        {
            ARMARX_CHECK_EXPRESSION(widget != nullptr);
            const auto dumpParamInfo = ARMARX_STREAM_PRINTER
            {
                out <<   "desc type   = "
                    << desc.ice_id()
                    << "\ntarget type = "
                    << GetTypeString<RemoteWidgetT>()
                    << "\nwidget type  = "
                    << GetTypeString(*widget)
                    << "\ntarget type = "
                    << GetTypeString<QWidgetT>();
            };
            auto concreteDesc = dynamic_cast<RemoteWidgetT const*>(&desc);
            QWidgetT* concreteWidget = qobject_cast<QWidgetT*>(widget);

            ARMARX_CHECK_EXPRESSION(concreteDesc   != nullptr) << dumpParamInfo;
            ARMARX_CHECK_EXPRESSION(concreteWidget != nullptr) << dumpParamInfo;

            ValueVariant currentState = makeValue(HandlerT::handleGuiChange(*concreteDesc, concreteWidget));
            return currentState;
        }
    };


    template <typename RemoteWidgetT_,
              typename QWidgetT_,
              ValueVariantType ValueType_ = RemoteGui::VALUE_VARIANT_EMPTY>
    struct TypedWidget
    {
        using RemoteWidgetT = RemoteWidgetT_;
        using QWidgetT = QWidgetT_;
        static const ValueVariantType ValueType = ValueType_;
        using ValueT = typename Storage<ValueType>::Type;

        static bool isValid(RemoteWidgetT const&, std::ostream& out)
        {
            return true;
        }
    };

    template <typename RemoteWidgetT_,
              typename QWidgetT_>
    struct TypedWidget<RemoteWidgetT_, QWidgetT_, RemoteGui::VALUE_VARIANT_EMPTY>
    {
        using RemoteWidgetT = RemoteWidgetT_;
        using QWidgetT = QWidgetT_;
        static const ValueVariantType ValueType = VALUE_VARIANT_EMPTY;
        using ValueT = typename Storage<ValueType>::Type;

        static bool isValid(RemoteWidgetT const&, std::ostream&)
        {
            return true;
        }

        static void updateGui(RemoteWidgetT const& desc, QWidgetT* widget, ValueT const& value)
        {
            // Do nothing
        }

        static ValueT handleGuiChange(RemoteWidgetT const& desc, QWidgetT* widget)
        {
            // Do nothing
            return ValueT {};
        }
    };

}
