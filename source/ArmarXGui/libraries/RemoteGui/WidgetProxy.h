#pragma once

#include <ArmarXGui/interface/RemoteGuiInterface.h>
#include <ArmarXGui/libraries/RemoteGui/Storage.h>

#include <sstream>

namespace armarx::RemoteGui
{
    class TabProxy;

    template <typename T>
    class ValueProxy;

    class ButtonProxy;

    class TabProxy
    {
    public:
        TabProxy() = default;
        TabProxy(RemoteGuiInterfacePrx const& remoteGui, std::string const& tabId);

        void receiveUpdates();
        void sendUpdates();
        void remove()
        {
            remoteGui->removeTab(tabId);
        }

        template <typename T>
        ValueProxy<T> getValue(std::string const& name);
        template <typename T>
        void getValue(T& val, std::string const& name);
        template <typename T>
        void getValue(std::atomic<T>& val, std::string const& name);

        template <typename T>
        void setValue(const T& val, std::string const& name);
        template <typename T>
        void setValue(const std::atomic<T>& val, std::string const& name);

        bool hasValueChanged(std::string const& name);

        ButtonProxy getButton(std::string const& name);
        bool getButtonClicked(std::string const& name);

        template <typename T>
        T internalGetValue(std::string const& name) const;
        template <typename T>
        void internalSetValue(std::string const& name, T const& value);
        bool internalButtonClicked(std::string const& name) const;
        void internalSetHidden(std::string const& name, bool hidden);
        void internalSetDisabled(std::string const& name, bool disabled);

        RemoteGui::WidgetState& getWidgetState(std::string const& name);
    private:
        RemoteGuiInterfacePrx remoteGui;
        std::string tabId;
        RemoteGui::ValueMap currentValues;
        RemoteGui::ValueMap oldValues;
        RemoteGui::ValueMap newValues;
        RemoteGui::ValueMap dirtyValues;

        RemoteGui::WidgetStateMap currentWidgetStates;
        RemoteGui::WidgetStateMap oldWidgetStates;
        RemoteGui::WidgetStateMap newWidgetStates;
        RemoteGui::WidgetState& getNewWidgetState(std::string const& name);

        bool valuesChanged = false;
        bool widgetChanged = false;
    };

    class WidgetProxy
    {
    public:
        WidgetProxy(TabProxy* tab, std::string const& name);

        void setHidden(bool hidden = true);
        void setDisabled(bool disabled = true);

        bool isHidden() const;
        bool isDisabled() const;
    protected:
        TabProxy* tab;
        std::string name;
    };

    template <typename T>
    class ValueProxy : public WidgetProxy
    {
    public:
        using WidgetProxy::WidgetProxy;

        T get() const;

        template<class T2>
        T2 cast_static() const;
        template<class T2>
        T2 cast_lexical() const;

        template <typename T1 = T, std::enable_if_t<std::is_trivially_copyable_v<T1>, int> = 0>
        void set(std::atomic<T1> const& value);

        void set(T const& value);

    };

    class ButtonProxy : public ValueProxy<int>
    {
    public:
        using ValueProxy::ValueProxy;
        bool clicked() const;
    };
}
//TabProxy
namespace armarx::RemoteGui
{
    inline TabProxy::TabProxy(RemoteGuiInterfacePrx const& remoteGui, std::string const& tabId)
        : remoteGui(remoteGui)
        , tabId(tabId)
    {
        currentValues = remoteGui->getValues(tabId);
        oldValues = currentValues;
        newValues = currentValues;

        currentWidgetStates = remoteGui->getWidgetStates(tabId);
        oldWidgetStates = currentWidgetStates;
        newWidgetStates = currentWidgetStates;
    }

    inline void TabProxy::receiveUpdates()
    {
        ARMARX_TRACE;
        oldValues = currentValues;
        currentValues = remoteGui->getValues(tabId);
        newValues = currentValues;
        valuesChanged = false;
        dirtyValues.clear();

        currentWidgetStates = remoteGui->getWidgetStates(tabId);
        oldWidgetStates = currentWidgetStates;
        newWidgetStates = currentWidgetStates;
        widgetChanged = false;
    }

    inline void TabProxy::sendUpdates()
    {
        ARMARX_TRACE;
        if (valuesChanged)
        {
            remoteGui->setValues(tabId, dirtyValues);
            dirtyValues.clear();
        }
        if (widgetChanged)
        {
            remoteGui->setWidgetStates(tabId, newWidgetStates);
        }
    }

    template <typename T>
    inline ValueProxy<T> TabProxy::getValue(std::string const& name)
    {
        ARMARX_TRACE;
        return ValueProxy<T>(this, name);
    }
    template <typename T>
    inline void TabProxy::getValue(T& val, std::string const& name)
    {
        ARMARX_TRACE;
        getValueFromMap(val, currentValues, name);
    }
    template <typename T>
    inline void TabProxy::getValue(std::atomic<T>& val, std::string const& name)
    {
        ARMARX_TRACE;
        val.store(getValue<T>(name).get());
    }

    template <typename T>
    inline void TabProxy::setValue(const T& val, std::string const& name)
    {
        getValue<T>(name).set(val);
    }
    template <typename T>
    inline void TabProxy::setValue(const std::atomic<T>& val, std::string const& name)
    {
        setValue(val.load(), name);
    }

    template <typename T>
    inline T TabProxy::internalGetValue(std::string const& name) const
    {
        ARMARX_TRACE;
        return RemoteGui::getAndReturnValue<T>(currentValues, name);
    }

    template <typename T>
    inline void TabProxy::internalSetValue(std::string const& name, T const& value)
    {
        ARMARX_TRACE;
        T currentValue = internalGetValue<T>(name);
        if (currentValue != value)
        {
            valuesChanged = true;
            dirtyValues[name] = makeValue(value);
        }
        newValues[name] = makeValue(value);
    }

    inline bool TabProxy::internalButtonClicked(std::string const& name) const
    {
        return RemoteGui::buttonClicked(newValues, oldValues, name);
    }

    inline void TabProxy::internalSetHidden(std::string const& name, bool hidden)
    {
        RemoteGui::WidgetState& state = getNewWidgetState(name);
        if (state.hidden != hidden)
        {
            widgetChanged = true;
        }
        state.hidden = hidden;
    }

    inline void TabProxy::internalSetDisabled(std::string const& name, bool disabled)
    {
        RemoteGui::WidgetState& state = getNewWidgetState(name);
        if (state.disabled != disabled)
        {
            widgetChanged = true;
        }
        state.disabled = disabled;
    }

    inline RemoteGui::WidgetState& TabProxy::getWidgetState(std::string const& name)
    {
        auto iter = currentWidgetStates.find(name);
        if (iter == currentWidgetStates.end())
        {
            throw LocalException("No widget with name '") << name << "' found";
        }
        return iter->second;
    }

    inline RemoteGui::WidgetState& TabProxy::getNewWidgetState(std::string const& name)
    {
        auto iter = newWidgetStates.find(name);
        if (iter == newWidgetStates.end())
        {
            throw LocalException("No widget with name '") << name << "' in NewWidgetStates found";
        }
        return iter->second;
    }
}
//WidgetProxy
namespace armarx::RemoteGui
{
    inline WidgetProxy::WidgetProxy(TabProxy* tab, std::string const& name)
        : tab(tab)
        , name(name)
    {
    }

    inline void WidgetProxy::setHidden(bool hidden)
    {
        tab->internalSetHidden(name, hidden);
    }

    inline void WidgetProxy::setDisabled(bool disabled)
    {
        tab->internalSetDisabled(name, disabled);
    }

    inline bool WidgetProxy::isHidden() const
    {
        return tab->getWidgetState(name).hidden;
    }

    inline bool WidgetProxy::isDisabled() const
    {
        return tab->getWidgetState(name).disabled;
    }
}
//ValueProxy
namespace armarx::RemoteGui
{
    template <typename T>
    inline T ValueProxy<T>::get() const
    {
        ARMARX_TRACE;
        return tab->internalGetValue<T>(name);
    }

    template <typename T>
    template<class T2>
    inline T2 ValueProxy<T>::cast_static() const
    {
        ARMARX_TRACE;
        return static_cast<T2>(get());
    }

    template <typename T>
    template<class T2>
    inline T2 ValueProxy<T>::cast_lexical() const
    {
        ARMARX_TRACE;
        std::stringstream s;
        s << get();
        T2 result;
        s >> result;
        return result;
    }

    template <typename T>
    inline void ValueProxy<T>::set(T const& value)
    {
        ARMARX_TRACE;
        tab->internalSetValue<T>(name, value);
    }

    template<typename T>
    template<typename T1, typename std::enable_if_t<std::is_trivially_copyable_v<T1>, int>>
    void ValueProxy<T>::set(const std::atomic<T1>& value)
    {
        set(value.load());
    }
}
//ButtonProxy
namespace armarx::RemoteGui
{
    inline bool ButtonProxy::clicked() const
    {
        ARMARX_TRACE;
        return tab->internalButtonClicked(name);
    }
}
