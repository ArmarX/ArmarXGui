#pragma once

#include <QLineEdit>
#include <QComboBox>

#include "Basic.h"

namespace armarx::RemoteGui
{
    struct LineEditHandler : TypedWidget<LineEdit, QLineEdit, VALUE_VARIANT_STRING>
    {
        static QWidgetT* createWidget(RemoteWidgetT const& desc, CreateWidgetCallback const& createChild,
                                      const QObject* stateChangeReceiver, const char* stateChangeSlot)
        {
            QWidgetT* widget = new QWidgetT();
            widget->setToolTip(QString::fromStdString(desc.toolTip));

            QObject::connect(widget, SIGNAL(editingFinished()), stateChangeReceiver, stateChangeSlot);

            return widget;
        }

        static void updateGui(RemoteWidgetT const& desc, QWidgetT* widget, ValueT const& value)
        {
            widget->setText(toQString(value));
        }

        static ValueT handleGuiChange(RemoteWidgetT const& desc, QWidgetT* widget)
        {
            return toUtf8(widget->text());
        }
    };

    struct ComboBoxHandler : TypedWidget<ComboBox, QComboBox, VALUE_VARIANT_STRING>
    {
        static QWidgetT* createWidget(RemoteWidgetT const& desc, CreateWidgetCallback const& createChild,
                                      const QObject* stateChangeReceiver, const char* stateChangeSlot)
        {
            QWidgetT* widget = new QWidgetT();
            widget->setToolTip(QString::fromStdString(desc.toolTip));

            for (std::string const& option : desc.options)
            {
                widget->addItem(toQString(option));
            }

            QObject::connect(widget, SIGNAL(currentIndexChanged(int)), stateChangeReceiver, stateChangeSlot);

            return widget;
        }

        static void updateGui(RemoteWidgetT const& desc, QWidgetT* widget, ValueT const& value)
        {
            int index = widget->findText(toQString(value));
            if (index >= 0)
            {
                widget->setCurrentIndex(index);
            }
            else
            {
                throw LocalException() << "Invalid value set for ComboBox '"
                                       << desc.name << "': " << value;
            }
        }

        static ValueT handleGuiChange(RemoteWidgetT const& desc, QWidgetT* widget)
        {
            return toUtf8(widget->currentText());
        }
    };

}
