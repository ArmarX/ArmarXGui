#pragma once

#include <QDoubleSpinBox>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#include <qwt_slider.h>
#pragma GCC diagnostic pop

#include "Basic.h"

namespace armarx::RemoteGui
{
    struct FloatSpinBoxHandler : TypedWidget<FloatSpinBox, QDoubleSpinBox, VALUE_VARIANT_FLOAT>
    {
        static bool isValid(RemoteWidgetT const& desc, std::ostream& out)
        {
#define cmp_or_log(l,c,r)                              \
    (l c r ? true :                                    \
     (                                                 \
             out << " FAILED: " #l " " #c " " #r " "   \
             << VAROUT(l) << ", " << VAROUT(r) << '\n',\
             false                                     \
     ))
            return cmp_or_log(desc.min, <=, desc.max)
                   && cmp_or_log(desc.steps, >, 0)
                   && cmp_or_log(desc.decimals, >=, 0);
#undef cmp_or_log
        }

        static QWidgetT* createWidget(RemoteWidgetT const& desc, CreateWidgetCallback const& createChild,
                                      const QObject* stateChangeReceiver, const char* stateChangeSlot)
        {
            QWidgetT* widget = new QWidgetT();

            widget->setToolTip(QString::fromStdString(desc.toolTip));
            widget->setMinimum(desc.min);
            widget->setMaximum(desc.max);
            widget->setDecimals(desc.decimals);
            widget->setSingleStep((desc.max - desc.min) / desc.steps);

            QObject::connect(widget, SIGNAL(valueChanged(double)), stateChangeReceiver, stateChangeSlot);

            return widget;
        }

        static void updateGui(RemoteWidgetT const& desc, QWidgetT* widget, ValueT const& value)
        {
            widget->setValue(value);
        }

        static ValueT handleGuiChange(RemoteWidgetT const& desc, QWidgetT* widget)
        {
            return widget->value();
        }
    };

    struct FloatSliderHandler : TypedWidget<FloatSlider, QwtSlider, VALUE_VARIANT_FLOAT>
    {
        static bool isValid(RemoteWidgetT const& desc, std::ostream& out)
        {
#define cmp_or_log(l,c,r)                              \
    (l c r ? true :                                    \
     (                                                 \
             out << " FAILED: " #l " " #c " " #r " "   \
             << VAROUT(l) << ", " << VAROUT(r) << '\n',\
             false                                     \
     ))
            return cmp_or_log(desc.min, <=, desc.max)
                   && cmp_or_log(desc.steps, >, 0);
#undef cmp_or_log
        }

        static QWidgetT* createWidget(RemoteWidgetT const& desc, CreateWidgetCallback const& createChild,
                                      const QObject* stateChangeReceiver, const char* stateChangeSlot)
        {
            QWidgetT* widget = new QWidgetT(nullptr);

            widget->setToolTip(QString::fromStdString(desc.toolTip));
            widget->setLowerBound(desc.min);
            widget->setUpperBound(desc.max);
            widget->setTotalSteps(desc.steps);
            widget->setOrientation(Qt::Horizontal);

            QObject::connect(widget, SIGNAL(valueChanged(double)), stateChangeReceiver, stateChangeSlot);

            return widget;
        }

        static void updateGui(RemoteWidgetT const& desc, QWidgetT* widget, ValueT const& value)
        {
            widget->setValue(value);
        }

        static ValueT handleGuiChange(RemoteWidgetT const& desc, QWidgetT* widget)
        {
            return widget->value();
        }
    };

}
