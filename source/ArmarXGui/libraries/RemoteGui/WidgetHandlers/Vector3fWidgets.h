#pragma once

#include <QWidget>
#include <QDoubleSpinBox>
#include <QHBoxLayout>

#include "Basic.h"

namespace armarx::RemoteGui
{
    struct Vector3fSpinBoxesHandler : TypedWidget<Vector3fSpinBoxes, QWidget, VALUE_VARIANT_VECTOR3>
    {
        static bool isValid(RemoteWidgetT const& desc, std::ostream& out)
        {
#define cmp_or_log(l,c,r)                              \
    (l c r ? true :                                    \
     (                                                 \
             out << " FAILED: " #l " " #c " " #r " "   \
             << VAROUT(l) << ", " << VAROUT(r) << '\n',\
             false                                     \
     ))

            return  cmp_or_log(desc.min.x, <=, desc.max.x)
                    && cmp_or_log(desc.min.y, <=, desc.max.y)
                    && cmp_or_log(desc.min.z, <=, desc.max.z)
                    && cmp_or_log(desc.steps.x, >, 0)
                    && cmp_or_log(desc.steps.y, >, 0)
                    && cmp_or_log(desc.steps.z, >, 0)
                    && cmp_or_log(desc.decimals.x, >=, 0)
                    && cmp_or_log(desc.decimals.y, >=, 0)
                    && cmp_or_log(desc.decimals.z, >=, 0);
#undef cmp_or_log
        }

        static QWidgetT* createWidget(RemoteWidgetT const& desc, CreateWidgetCallback const& createChild,
                                      const QObject* stateChangeReceiver, const char* stateChangeSlot)
        {
            QWidget* widget = new QWidget;
            widget->setToolTip(QString::fromStdString(desc.toolTip));
            QHBoxLayout* l = new QHBoxLayout;
            l->setContentsMargins(0, 0, 0, 0);
            widget->setLayout(l);

            Eigen::Vector3f min = fromIce(desc.min);
            Eigen::Vector3f max = fromIce(desc.max);
            Eigen::Vector3i decimals = fromIce(desc.decimals);
            Eigen::Vector3i steps = fromIce(desc.steps);
            for (int i = 0; i < 3; ++i)
            {
                QDoubleSpinBox* e = new QDoubleSpinBox;
                l->addWidget(e);
                e->setMinimum(min(i));
                e->setMaximum(max(i));
                e->setDecimals(decimals(i));
                e->setSingleStep((max(i) - min(i)) / steps(i));
                QObject::connect(e, SIGNAL(valueChanged(double)), stateChangeReceiver, stateChangeSlot);
            }
            return widget;
        }

        static void updateGui(RemoteWidgetT const&, QWidgetT* widget, ValueT const& value)
        {
            static_cast<QDoubleSpinBox*>(widget->layout()->itemAt(0)->widget())->setValue(value(0));
            static_cast<QDoubleSpinBox*>(widget->layout()->itemAt(1)->widget())->setValue(value(1));
            static_cast<QDoubleSpinBox*>(widget->layout()->itemAt(2)->widget())->setValue(value(2));
        }

        static ValueT handleGuiChange(RemoteWidgetT const&, QWidgetT* widget)
        {
            QWidgetT* parent = static_cast<QWidget*>(widget->parent());
            return
            {
                static_cast<float>(static_cast<QDoubleSpinBox*>(parent->layout()->itemAt(0)->widget())->value()),
                static_cast<float>(static_cast<QDoubleSpinBox*>(parent->layout()->itemAt(1)->widget())->value()),
                static_cast<float>(static_cast<QDoubleSpinBox*>(parent->layout()->itemAt(2)->widget())->value())
            };
        }
    };

}
