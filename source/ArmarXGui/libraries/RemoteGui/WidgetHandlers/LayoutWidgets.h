#pragma once

#include <QGroupBox>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGridLayout>

#include "Basic.h"

namespace armarx::RemoteGui
{
    struct GroupBoxHandler : TypedWidget<GroupBox, QGroupBox>
    {
        static QWidgetT* createWidget(RemoteWidgetT const& desc, CreateWidgetCallback const& createChild,
                                      const QObject* stateChangeReceiver, const char* stateChangeSlot)
        {
            QGroupBox* widget = new QGroupBox;
            widget->setTitle(toQString(desc.label));
            widget->setCheckable(true);
            widget->setChecked(true);

            //use an arrow as indicator for the checkbox
            {
                widget->setStyleSheet(R"(
                        QGroupBox::indicator:checked {
                            image: url(:/icons/TriangleBlackDown.svg);
                        }
                        QGroupBox::indicator:unchecked {
                            image: url(:/icons/TriangleBlackRight.svg);
                        }
                    )");
            }

            QHBoxLayout* layout = new QHBoxLayout;
            widget->setLayout(layout);
            layout->setContentsMargins(0, 0, 0, 0);

            QWidget* holder = new QWidget;
            layout->addWidget(holder);

            QObject::connect(widget, SIGNAL(clicked(bool)), holder, SLOT(setVisible(bool)));

            if (desc.children.size() > 0)
            {

                QVBoxLayout* layout = new QVBoxLayout;
                holder->setLayout(layout);
                layout->setContentsMargins(9, 0, 0, 0);
                for (const auto& child : desc.children)
                {
                    layout->addWidget(createChild(child));
                }
            }

            widget->setChecked(!desc.collapsed);
            holder->setVisible(!desc.collapsed);

            return widget;
        }
    };

    struct VBoxLayoutHandler : TypedWidget<VBoxLayout, QWidget>
    {
        static QWidgetT* createWidget(RemoteWidgetT const& desc, CreateWidgetCallback const& createChild,
                                      const QObject* stateChangeReceiver, const char* stateChangeSlot)
        {
            QWidgetT* widget = new QWidget();

            QVBoxLayout* layout = new QVBoxLayout(widget);
            widget->setLayout(layout);
            layout->setContentsMargins(0, 0, 0, 0);
            for (const RemoteGui::WidgetPtr& child : desc.children)
            {
                QWidget* childWidget = createChild(child);
                layout->addWidget(childWidget);
            }

            return widget;
        }
    };

    struct HBoxLayoutHandler : TypedWidget<HBoxLayout, QWidget>
    {
        static QWidgetT* createWidget(RemoteWidgetT const& desc, CreateWidgetCallback const& createChild,
                                      const QObject* stateChangeReceiver, const char* stateChangeSlot)
        {
            QWidgetT* widget = new QWidget();

            QHBoxLayout* layout = new QHBoxLayout(widget);
            widget->setLayout(layout);
            layout->setContentsMargins(0, 0, 0, 0);
            for (const RemoteGui::WidgetPtr& child : desc.children)
            {
                QWidget* childWidget = createChild(child);
                layout->addWidget(childWidget);
            }

            return widget;
        }
    };
    struct SimpleGridLayoutSpanningChildHandler : TypedWidget<SimpleGridLayoutSpanningChild, QWidget>
    {
        static QWidgetT* createWidget(RemoteWidgetT const& desc, CreateWidgetCallback const& createChild,
                                      const QObject* stateChangeReceiver, const char* stateChangeSlot)
        {
            ARMARX_CHECK_GREATER(desc.columns, 0);
            switch (desc.children.size())
            {
                case 0:
                    return new QWidget;
                case 1:
                    return createChild(desc.children.front());
                default:; //below
            }

            QWidgetT* widget = new QWidget();

            QHBoxLayout* layout = new QHBoxLayout(widget);
            widget->setLayout(layout);
            layout->setContentsMargins(0, 0, 0, 0);
            for (const RemoteGui::WidgetPtr& child : desc.children)
            {
                QWidget* childWidget = createChild(child);
                layout->addWidget(childWidget);
            }

            return widget;

        }
    };
    struct SimpleGridLayoutHandler : TypedWidget<SimpleGridLayout, QWidget>
    {
        static QWidgetT* createWidget(RemoteWidgetT const& desc, CreateWidgetCallback const& createChild,
                                      const QObject* stateChangeReceiver, const char* stateChangeSlot)
        {
            QWidgetT* widget = new QWidget();

            QGridLayout* layout = new QGridLayout(widget);
            widget->setLayout(layout);
            layout->setContentsMargins(0, 0, 0, 0);
            ARMARX_CHECK_GREATER(desc.columns, 0);
            int col = 0;
            int row = 0;
            for (const RemoteGui::WidgetPtr& child : desc.children)
            {
                QWidget* childWidget = createChild(child);
                int colspan = 1;
                if (auto spc = SimpleGridLayoutSpanningChildPtr::dynamicCast(child); spc)
                {
                    colspan = std::max(1, spc->columns);
                }
                layout->addWidget(childWidget, row, col, 1, colspan);

                if (col + colspan >= desc.columns)
                {
                    //next row
                    ++row;
                    col = 0;
                }
                else
                {
                    col += colspan;
                }
            }

            return widget;
        }
    };

    struct GridLayoutHandler : TypedWidget<GridLayout, QWidget>
    {
        static QWidgetT* createWidget(RemoteWidgetT const& desc, CreateWidgetCallback const& createChild,
                                      const QObject* stateChangeReceiver, const char* stateChangeSlot)
        {
            QWidgetT* widget = new QWidget();

            QGridLayout* layout = new QGridLayout(widget);
            widget->setLayout(layout);
            layout->setContentsMargins(0, 0, 0, 0);
            int maxrow = 0;
            for (std::size_t i = 0; i < desc.children.size(); ++i)
            {
                const RemoteGui::WidgetPtr& child = desc.children.at(i);

                QWidget* childWidget = createChild(child);

                GridLayoutData l;
                if (i < desc.childrenLayoutInfo.size())
                {
                    l = desc.childrenLayoutInfo.at(i);
                }
                else
                {
                    l.col = 0;
                    l.row = maxrow;
                    l.spanCol = 1;
                    l.spanRow = 1;
                }
                maxrow = std::max(maxrow, l.row + l.spanRow);
                layout->addWidget(childWidget, l.row, l.col, l.spanRow, l.spanCol);
            }

            return widget;
        }
    };
}
