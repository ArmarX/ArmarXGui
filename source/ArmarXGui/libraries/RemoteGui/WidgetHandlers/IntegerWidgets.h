#pragma once

#include <QSpinBox>
#include <QSlider>
#include <QPushButton>

#include "Basic.h"

namespace armarx::RemoteGui
{
    struct IntSpinBoxHandler : TypedWidget<IntSpinBox, QSpinBox, VALUE_VARIANT_INT>
    {
        static QWidgetT* createWidget(RemoteWidgetT const& desc, CreateWidgetCallback const& createChild,
                                      const QObject* stateChangeReceiver, const char* stateChangeSlot)
        {
            QWidgetT* widget = new QWidgetT();

            widget->setToolTip(QString::fromStdString(desc.toolTip));
            widget->setMinimum(desc.min);
            widget->setMaximum(desc.max);

            QObject::connect(widget, SIGNAL(valueChanged(int)), stateChangeReceiver, stateChangeSlot);

            return widget;
        }

        static void updateGui(RemoteWidgetT const& desc, QWidgetT* widget, ValueT const& value)
        {
            widget->setValue(value);
        }

        static ValueT handleGuiChange(RemoteWidgetT const& desc, QWidgetT* widget)
        {
            return widget->value();
        }
    };

    struct IntSliderHandler : TypedWidget<IntSlider, QSlider, VALUE_VARIANT_INT>
    {
        static QWidgetT* createWidget(RemoteWidgetT const& desc, CreateWidgetCallback const& createChild,
                                      const QObject* stateChangeReceiver, const char* stateChangeSlot)
        {
            QWidgetT* widget = new QWidgetT();

            widget->setToolTip(QString::fromStdString(desc.toolTip));
            widget->setOrientation(Qt::Horizontal);

            widget->setMinimum(desc.min);
            widget->setMaximum(desc.max);

            QObject::connect(widget, SIGNAL(valueChanged(int)), stateChangeReceiver, stateChangeSlot);

            return widget;
        }

        static void updateGui(RemoteWidgetT const& desc, QWidgetT* widget, ValueT const& value)
        {
            widget->setValue(value);
        }

        static ValueT handleGuiChange(RemoteWidgetT const& desc, QWidgetT* widget)
        {
            return widget->value();
        }
    };

    struct ButtonHandler : TypedWidget<Button, QPushButton, VALUE_VARIANT_INT>
    {
        static QWidgetT* createWidget(RemoteWidgetT const& desc, CreateWidgetCallback const& createChild,
                                      const QObject* stateChangeReceiver, const char* stateChangeSlot)
        {
            QWidgetT* widget = new QWidgetT();
            widget->setToolTip(QString::fromStdString(desc.toolTip));
            widget->setText(toQString(desc.label));

            QObject::connect(widget, SIGNAL(clicked()), stateChangeReceiver, stateChangeSlot);

            return widget;
        }

        static void updateGui(RemoteWidgetT const& desc, QWidgetT* widget, ValueT const& value)
        {
            widget->setProperty("ClickCount", QVariant(value));
        }

        static ValueT handleGuiChange(RemoteWidgetT const& desc, QWidgetT* widget)
        {
            int newValue = widget->property("ClickCount").toInt() + 1;
            widget->setProperty("ClickCount", QVariant(newValue));
            return newValue;
        }
    };
}
