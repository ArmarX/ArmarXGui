#pragma once

#include <QLabel>

#include "Basic.h"

namespace armarx::RemoteGui
{
    struct LabelHandler : TypedWidget<Label, QLabel, VALUE_VARIANT_STRING>
    {
        static QWidgetT* createWidget(RemoteWidgetT const& desc, CreateWidgetCallback const& createChild,
                                      const QObject* stateChangeReceiver, const char* stateChangeSlot)
        {
            QWidgetT* widget = new QWidgetT();
            widget->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
            widget->setToolTip(QString::fromStdString(desc.toolTip));
            return widget;
        }

        static void updateGui(RemoteWidgetT const& desc, QWidgetT* widget, ValueT const& value)
        {
            widget->setText(toQString(value));
        }

        static ValueT handleGuiChange(RemoteWidgetT const& desc, QWidgetT* widget)
        {
            return toUtf8(widget->text());
        }
    };

    struct HSpacerHandler : TypedWidget<HSpacer, QWidget>
    {
        static QWidgetT* createWidget(RemoteWidgetT const& desc, CreateWidgetCallback const& createChild,
                                      const QObject* stateChangeReceiver, const char* stateChangeSlot)
        {
            QWidgetT* widget = new QWidget();

            widget->setLayout(new QVBoxLayout);
            widget->layout()->setContentsMargins(0, 0, 0, 0);
            widget->layout()->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum));

            return widget;
        }
    };

    struct VSpacerHandler : TypedWidget<VSpacer, QWidget>
    {
        static QWidgetT* createWidget(RemoteWidgetT const& desc, CreateWidgetCallback const& createChild,
                                      const QObject* stateChangeReceiver, const char* stateChangeSlot)
        {
            QWidgetT* widget = new QWidget();

            widget->setLayout(new QVBoxLayout);
            widget->layout()->setContentsMargins(0, 0, 0, 0);
            widget->layout()->addItem(new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding));

            return widget;
        }
    };

    struct HLineHandler : TypedWidget<HLine, QFrame>
    {
        static QWidgetT* createWidget(RemoteWidgetT const& desc, CreateWidgetCallback const& createChild,
                                      const QObject* stateChangeReceiver, const char* stateChangeSlot)
        {
            QWidgetT* widget = new QFrame();

            widget->setFrameShape(QFrame::HLine);
            widget->setFrameShadow(QFrame::Sunken);

            return widget;
        }
    };
    struct VLineHandler : TypedWidget<VLine, QFrame>
    {
        static QWidgetT* createWidget(RemoteWidgetT const& desc, CreateWidgetCallback const& createChild,
                                      const QObject* stateChangeReceiver, const char* stateChangeSlot)
        {
            QWidgetT* widget = new QFrame();

            widget->setFrameShape(QFrame::VLine);
            widget->setFrameShadow(QFrame::Sunken);

            return widget;
        }
    };

    struct EmptyWidgetHandler : TypedWidget<Widget, QWidget>
    {
        static QWidgetT* createWidget(RemoteWidgetT const& desc, CreateWidgetCallback const& createChild,
                                      const QObject* stateChangeReceiver, const char* stateChangeSlot)
        {
            auto widget = new QWidget();
            widget->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
            return widget;
        }
    };
}
