#pragma once

#include <QCheckBox>
#include <QPushButton>

#include "Basic.h"

namespace armarx::RemoteGui
{
    struct CheckBoxHandler : TypedWidget<CheckBox, QCheckBox, VALUE_VARIANT_BOOL>
    {
        static QWidgetT* createWidget(RemoteWidgetT const& desc, CreateWidgetCallback const& createChild,
                                      const QObject* stateChangeReceiver, const char* stateChangeSlot)
        {
            QWidgetT* widget = new QWidgetT();
            widget->setToolTip(QString::fromStdString(desc.toolTip));
            widget->setText(toQString(desc.label));

            QObject::connect(widget, SIGNAL(stateChanged(int)), stateChangeReceiver, stateChangeSlot);

            return widget;
        }

        static void updateGui(RemoteWidgetT const& desc, QWidgetT* widget, ValueT const& value)
        {
            widget->setCheckState(value ? Qt::Checked : Qt::Unchecked);
        }

        static ValueT handleGuiChange(RemoteWidgetT const& desc, QWidgetT* widget)
        {
            return widget->checkState() == Qt::Checked;
        }
    };

    struct ToggleButtonHandler : TypedWidget<ToggleButton, QPushButton, VALUE_VARIANT_BOOL>
    {
        static QWidgetT* createWidget(RemoteWidgetT const& desc, CreateWidgetCallback const& createChild,
                                      const QObject* stateChangeReceiver, const char* stateChangeSlot)
        {
            QWidgetT* widget = new QWidgetT();
            widget->setToolTip(QString::fromStdString(desc.toolTip));
            widget->setText(toQString(desc.label));
            widget->setCheckable(true);

            QObject::connect(widget, SIGNAL(toggled(bool)), stateChangeReceiver, stateChangeSlot);

            return widget;
        }

        static void updateGui(RemoteWidgetT const& desc, QWidgetT* widget, ValueT const& value)
        {
            widget->setChecked(value);
        }

        static ValueT handleGuiChange(RemoteWidgetT const& desc, QWidgetT* widget)
        {
            return widget->isChecked();
        }
    };
}
