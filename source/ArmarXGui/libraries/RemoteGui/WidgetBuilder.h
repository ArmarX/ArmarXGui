#pragma once

#include "WidgetBuilder/Basic.h"
#include "WidgetBuilder/BoolWidgets.h"
#include "WidgetBuilder/FloatWidgets.h"
#include "WidgetBuilder/IntegerWidgets.h"
#include "WidgetBuilder/LayoutWidgets.h"
#include "WidgetBuilder/Matrix4fWidgets.h"
#include "WidgetBuilder/StaticWidgets.h"
#include "WidgetBuilder/StringWidgets.h"
#include "WidgetBuilder/Vector3fWidgets.h"
