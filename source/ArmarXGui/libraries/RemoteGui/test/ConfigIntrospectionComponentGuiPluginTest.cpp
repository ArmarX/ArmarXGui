/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::CPPUtility::Test
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#define BOOST_TEST_MODULE ArmarX::RemoteGui::ConfigIntrospection

#define ARMARX_BOOST_TEST

#include <SimoxUtility/meta/enum/adapt_enum.h>

#include <ArmarXCore/util/CPPUtility/ConfigIntrospection.h>
#include <ArmarXCore/util/CPPUtility/TripleBuffer.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/libraries/ArmarXCoreComponentPlugins/DebugObserverComponentPlugin.h>
#include <ArmarXCore/core/application/properties/PluginAll.h>

#include <ArmarXGui/Test.h>
#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/RemoteGuiComponentPlugin.h>

namespace armarx
{
    enum class enu
    {
        e0, e1, e2
    };
}
namespace simox::meta
{
    template<>
    const simox::meta::EnumNames<armarx::enu>
    enum_names<armarx::enu>
    {
        { armarx::enu::e0, "e0" },
        { armarx::enu::e1, "e1" },
        { armarx::enu::e2, "e2" }
    };
}
#define mk(...) ARMARX_CONFIG_STRUCT_DEFINE_ADAPT_CONFIGURE(__VA_ARGS__)

mk(armarx, cfg_str, (std::string, _str, AX_DEFAULT("1")));
mk(armarx, cfg_flt, (float,       _flt, AX_DEFAULT(100), AX_MIN(0), AX_MAX(128)));
//mk(armarx, cfg_dbl, (double,      _dbl, AX_DEFAULT(100), AX_MIN(0), AX_MAX(128)));
mk(armarx, cfg_int, (int,         _int, AX_DEFAULT(100), AX_MIN(0), AX_MAX(128)));
//mk(armarx, cfg_chr, (char,        _chr, AX_DEFAULT(100), AX_MIN(0), AX_MAX(128)));
mk(armarx, cfg_bol, (bool,        _bol, AX_DEFAULT(true)));
mk(armarx, cfg_enu, (armarx::enu, _enu, AX_DEFAULT(armarx::enu::e0)));


mk(
    armarx, config,
    (armarx::cfg_enu, _enu),
    (armarx::cfg_str, _str),
    (armarx::cfg_flt, _flt),
    //(armarx::cfg_dbl, _dbl),
    (armarx::cfg_int, _int),
    //(armarx::cfg_chr, _chr),
    (armarx::cfg_bol, _bol)
);
namespace armarx
{
    class Testcomponent_RemoteGui_ConfigIntrospection :
        virtual public RemoteGuiComponentPluginUser,
        virtual public DebugObserverComponentPluginUser,
        virtual public Component
    {
        std::string getDefaultName() const override
        {
            return "";
        }
        void onInitComponent() override
        {
            setDebugObserverDatafield("cfg", cfg.getReadBuffer());
        }
        void onConnectComponent() override
        {
            createOrUpdateRemoteGuiTab(cfg);
        }
        void onDisconnectComponent() override {}
        void onExitComponent() override {}
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            armarx::PropertyDefinitionsPtr ptr = new ComponentPropertyDefinitions(getConfigIdentifier());
            ptr->optional(cfg.getWriteBuffer(), "", "");
            return ptr;
        }
        using config = armarx::config;
        WriteBufferedTripleBuffer<config> cfg;
    };
}

BOOST_AUTO_TEST_CASE(empty)
{
    // test to check everything still compiles
    // using the optional plugins for config introspection
    // (properties, dbg drawer, remote gui)
}

