/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <chrono>

#include "PlotterController.h"
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <qwt_legend_label.h>

namespace armarx
{
    PlotterController::PlotterController(QObject* parent) :
        QObject(parent),
        plotter(new QwtPlot())
    {

        curveColors = {Qt::red,
                       Qt::green,
                       Qt::blue,
                       Qt::cyan,
                       Qt::magenta,
                       Qt::darkRed,
                       Qt::darkGreen,
                       Qt::darkBlue,
                       Qt::darkCyan,
                       Qt::darkMagenta
                      };

        qRegisterMetaType<std::map<std::string, VariantPtr>>("std::map<std::string, VariantPtr>");
        widget = new QWidget();
        widget->setMinimumHeight(150);
        //            widget->setMaximumWidth(600);
        //            widget->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::MinimumExpanding));
        ////////////////
        //  Setup Plotter
        ///////////////
        // panning with the left mouse button
        (void) new QwtPlotPanner(plotter->canvas());

        // zoom in/out with the wheel
        QwtPlotMagnifier* magnifier = new QwtPlotMagnifier(plotter->canvas());
        magnifier->setAxisEnabled(QwtPlot::xBottom, false);

        dynamic_cast<QwtPlotCanvas&>(*plotter->canvas()).setPaintAttribute(QwtPlotCanvas::BackingStore, false); //increases performance for incremental drawing

        QwtLegend* legend = new QwtLegend;
        legend->setDefaultItemMode(QwtLegendData::Mode::Checkable);
        plotter->insertLegend(legend, QwtPlot::BottomLegend);


        plotter->setAxisTitle(QwtPlot::xBottom, "Time (in sec)");
        plotter->enableAxis(QwtPlot::yLeft, false);

        plotter->enableAxis(QwtPlot::yRight, true);
        plotter->setAxisAutoScale(QwtPlot::yRight, true);
        //        plotter->setAutoReplot();


        //            plotter->setCanvasBackground(* new QBrush(Qt::white));

        connect(legend, SIGNAL(checked(const QVariant&, bool, int)),
                SLOT(legendChecked(const QVariant&, bool)));

        connect(&timer, SIGNAL(timeout()), this, SLOT(updateGraph()));
        stackedLayout = new QStackedLayout(widget);
        stackedLayout->addWidget(plotter);

        barlayout = new QHBoxLayout;
        QWidget* barsWidget = new QWidget();
        barsWidget->setLayout(barlayout);
        barsWidget->setAccessibleName("barsWidget");
        stackedLayout->addWidget(barsWidget);
    }

    PlotterController::~PlotterController()
    {
        ARMARX_DEBUG << "~PlotterController";
        if (!widget.isNull())
        {
            widget->deleteLater();
        }
        timer.stop();
        if (pollingTask)
        {
            pollingTask->stop();
        }
    }

    QWidget* PlotterController::getPlotterWidget()
    {
        return widget;
    }

    void PlotterController::pollingExec()
    {
        std::map<std::string, VariantPtr> newData = getData(selectedDatafields);


        emit newDataAvailable(TimeUtil::GetTime().toMicroSeconds(), newData);
    }

    void PlotterController::updateGraph()
    {
        if (this->__plottingPaused)
        {
            return;
        }
        std::lock_guard lock(dataMutex);

        IceUtil::Time curTime = TimeUtil::GetTime();
        // erase old markers
        for (auto& markerChannel : markers)
        {
            for (auto it = markerChannel.second.begin(); it != markerChannel.second.end();)
            {
                if ((curTime - it->first).toSecondsDouble() > shownInterval)
                {
                    for (auto elem : it->second)
                    {
                        elem.second->detach();
                    }
                    it = markerChannel.second.erase(it);
                }
                else
                {
                    it++;
                }
            }
        }

        GraphDataMap::iterator it = dataMap.begin();

        auto addMarker = [this](IceUtil::Time & age, VariantPtr & var, std::string & datafieldId)
        {
            std::map < std::string, QwtPlotMarkerPtr>& markerSubMap = markers[datafieldId][age];

            std::string value = var->getString();
            auto it2 = markerSubMap.find(value);
            if (it2 != markerSubMap.end())
            {
                QwtPlotMarkerPtr marker = it2->second;
                marker->setXValue(0.001 * age.toMilliSecondsDouble());
            }
            else
            {
                QwtPlotMarkerPtr marker(new QwtPlotMarker());
                marker->setValue(0.001 * age.toMilliSecondsDouble(), 0);
                marker->setLabel(QwtText(QString::fromStdString(value)));
                marker->setLineStyle(QwtPlotMarker::VLine);
                marker->setLabelAlignment(Qt::AlignBottom | Qt::AlignLeft);
                int i = 0;
                for (auto& markerSubMap : markers)
                {
                    if (markerSubMap.first == datafieldId)
                    {
                        break;
                    }
                    i++;
                }
                marker->setLinePen(QColor(Qt::GlobalColor((i) % 5 + 13)));
                marker->attach(plotter);
                markerSubMap[value] = marker;
            }

        };

        for (; it != dataMap.end(); ++it)
        {

            QVector<QPointF> pointList;
            pointList.clear();
            auto datafieldId = it->first;
            std::vector<TimeData>& dataVec = it->second;
            //            int newSize = min(size,(int)dataVec.size());
            pointList.reserve(dataVec.size());


            try
            {
                int j = 0;

                for (int i = dataVec.size() - 1; i >= 0 ; --i)
                {
                    TimeData& data = dataVec[i];
                    IceUtil::Time age = (data.time - curTime);

                    if (age.toSecondsDouble() <= shownInterval /*&& age.toMicroSeconds() > 0*/)
                    {
                        VariantPtr var = VariantPtr::dynamicCast(data.data);

                        if (var->getInitialized())
                        {
                            float xValue = 0.001 * age.toMilliSecondsDouble();
                            auto type = var->getType();
                            double yValue = 0;
                            if (type == VariantType::Float && var->getFloat() != std::numeric_limits<float>::infinity())
                            {
                                yValue = (var->getFloat());
                            }
                            else if (type == VariantType::Double && var->getDouble() != std::numeric_limits<double>::infinity())
                            {
                                yValue = (var->getDouble());
                            }
                            else if (type == VariantType::Int && var->getInt() != std::numeric_limits<int>::infinity())
                            {
                                yValue = (var->getInt());
                            }
                            else if (type == VariantType::Long && var->getLong() != std::numeric_limits<long>::infinity())
                            {
                                yValue = static_cast<double>(var->getLong());
                            }
                            else if (type == VariantType::Bool)
                            {
                                yValue = (var->getBool() ? 1 : -1);
                            }
                            else if (type == VariantType::String)
                            {
                                addMarker(age, var, datafieldId);
                            }
                            else
                            {
                                continue;
                            }

                            pointList.push_back(QPointF(xValue, yValue));

                            j++;
                        }
                        else
                        {
                            ARMARX_WARNING << deactivateSpam(3) << "uninitialized field: " << it->first;
                        }

                    }
                    else
                    {
                        break;    // data too old from now
                    }
                }
            }
            catch (...)
            {
                handleExceptions();
            }

            QwtSeriesData<QPointF>* pointSeries = new  QwtPointSeriesData(pointList);

            if (pointList.size() > 0 && curves.find(it->first) != curves.end() && bars.find(it->first) != bars.end())
            {
                QwtPlotCurve* curve = curves[it->first];
                curve->setData(pointSeries);
                curve->setYAxis(QwtPlot::yRight);
                QwtThermo* bar = bars[it->first];
                bar->setValue(pointList.first().y());
                float range = curve->maxYValue() - curve->minYValue();
                bar->setLowerBound(curve->minYValue() - 0.05 * range);
                bar->setUpperBound((curve->maxYValue()) + range * 0.05);
            }
        }

        plotter->setAxisAutoScale(QwtPlot::yRight, autoScalePlot);

        //        plotter->setAxisScale( QwtPlot::yLeft, -1, 1);

        plotter->replot();
    }

    void PlotterController::showCurve(QwtPlotItem* item, bool on)
    {
        item->setVisible(on);
        QwtLegend* lgd = qobject_cast<QwtLegend*>(plotter->legend());

        QList<QWidget*> legendWidgets =
            lgd->legendWidgets(plotter->itemToInfo(item));

        if (legendWidgets.size() == 1)
        {
            QwtLegendLabel* legendLabel =
                qobject_cast<QwtLegendLabel*>(legendWidgets[0]);

            if (legendLabel)
            {
                legendLabel->setChecked(on);
            }
            else
            {
                ARMARX_WARNING << "legendLabel is no QwtLegendLabel";
            }
        }
        else
        {
            ARMARX_WARNING << "found " << legendWidgets.size() << " items for the given curve";
        }
        plotter->replot();
    }

    void PlotterController::autoScale(bool toggled)
    {
        ARMARX_VERBOSE << "clicked autoscale" << flush;

        plotter->setAxisAutoScale(QwtPlot::yRight, toggled);
        plotter->replot();
        autoScalePlot = toggled;
    }

    void PlotterController::plottingPaused(bool toggled)
    {
        __plottingPaused = toggled;

        //            if (pollingTask)
        //            {
        //                pollingTask->stop();
        //            }

        if (__plottingPaused)
        {
            timer.stop();
        }
        else
        {
            //                pollingTask = new PeriodicTask<PlotterController>(this, &PlotterController::pollingExec, pollingInterval, false, "DataPollingTask", false);
            //                pollingTask->start();
            timer.start(updateInterval);
        }
    }

    std::map<std::string, VariantPtr> PlotterController::getData(const QStringList& channels)
    {
        using clock_t = std::chrono::high_resolution_clock;
        const auto now = [] {return clock_t::now();};
        const auto dt_ms = [](auto t0, auto t1) -> float
        {
            return std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t0).count() / 1e6f;
        };
        if (iceManager->isShutdown())
        {
            return {};
        }
        //create Data field ids from channel names
        std::map< std::string, DataFieldIdentifierBaseList > channelsSplittedByObserver;
        for (QString const& channel : channels)
        {
            DataFieldIdentifierPtr identifier = new DataFieldIdentifier(channel.toStdString());
            channelsSplittedByObserver[identifier->getObserverName()].push_back(identifier);
            //            ARMARX_INFO << identifier;
        }
        std::map<std::string, VariantPtr> newData;
        const auto time_clear_start = now();
        {
            std::lock_guard lock(dataMutex);

            // first clear to old entries
            auto now = TimeUtil::GetTime();
            GraphDataMap::iterator itmap = dataMap.begin();

            for (; itmap != dataMap.end(); ++itmap)
            {
                std::vector<TimeData>& dataVec = itmap->second;
                int stepSize = std::max<int>(1, dataVec.size() * 0.01);
                int thresholdIndex = -1;

                for (unsigned int i = 0; i < dataVec.size(); i += stepSize)
                {
                    // only delete if entries are older than 2*showninterval
                    // and delete then all entries that are older than showninterval.
                    // otherwise it would delete items on every call, which would be very slow

                    if ((now - dataVec[i].time).toSecondsDouble() > shownInterval * 2
                        || (thresholdIndex != -1 && (now - dataVec[i].time).toSecondsDouble() > shownInterval)
                       )
                    {
                        thresholdIndex = i;
                    }
                    else
                    {
                        break;
                    }
                }

                if (thresholdIndex != -1)
                {
                    unsigned int offset = std::min((int)dataVec.size(), thresholdIndex);

                    //                ARMARX_IMPORTANT << "Erasing " << offset << " fields";
                    if (offset > dataVec.size())
                    {
                        dataVec.clear();
                    }
                    else
                    {
                        dataVec.erase(dataVec.begin(), dataVec.begin() + offset);
                    }
                }

                //            ARMARX_IMPORTANT << deactivateSpam(5) << "size: " << dataVec.size();
            }
        }
        const auto time_clear_end = now();

        // now get new data
        IceUtil::Time time = TimeUtil::GetTime();
        std::map<std::string, DataFieldIdentifierBaseList >::iterator it = channelsSplittedByObserver.begin();
        const auto time_get_data_start   = now();
        float time_get_data_get_variants = 0;
        float time_get_data_find_obs     = 0;
        float time_get_data_lock         = 0;
        float time_get_data_process      = 0;
        std::map<std::string, float> time_get_data_get_variants_per_obs;
        try
        {
            for (; it != channelsSplittedByObserver.end(); ++it)
            {
                const std::string& observerName = it->first;

                const auto time_get_data_find_obs_start = now();
                if (proxyMap.find(observerName) == proxyMap.end())
                {
                    if (!iceManager)
                    {
                        continue;
                        time_get_data_find_obs += dt_ms(time_get_data_find_obs_start, now());
                    }
                    proxyMap[observerName] = iceManager->getProxy<ObserverInterfacePrx>(observerName);
                }
                time_get_data_find_obs += dt_ms(time_get_data_find_obs_start, now());

                //            QDateTime time(QDateTime::currentDateTime());
                const auto time_get_data_get_variants_start = now();
                TimedVariantBaseList variants = proxyMap[observerName]->getDataFields(it->second);
                const auto time_get_data_get_variants_dt = dt_ms(time_get_data_get_variants_start, now());
                time_get_data_get_variants_per_obs[observerName] = time_get_data_get_variants_dt;
                time_get_data_get_variants += time_get_data_get_variants_dt;

                const auto time_get_data_lock_start = now();
                std::lock_guard lock(dataMutex);
                time_get_data_lock += dt_ms(time_get_data_lock_start, now());
                //            #    ARMARX_IMPORTANT << "data from observer: " << observerName;
                const auto time_get_data_process_start = now();
                for (unsigned int i = 0; i < variants.size(); ++i)
                {
                    //                    ARMARX_IMPORTANT << "Variant: " << VariantPtr::dynamicCast(variants[i]);
                    VariantPtr var = VariantPtr::dynamicCast(variants[i]);
                    std::string id = DataFieldIdentifierPtr::dynamicCast(it->second[i])->getIdentifierStr();
                    if (!var->getInitialized())
                    {
                        //dataMap[id] = {};
                        continue;
                    }
                    auto type = var->getType();
                    if (type == VariantType::String)
                    {
                        if (dataMap[id].size() == 0 || dataMap[id].rbegin()->data->getString() != var->getString())
                        {
                            // only insert if changed
                            dataMap[id].push_back(TimeData(time, var));
                            newData[id] = var;
                        }
                    }
                    else if (type == VariantType::Float)
                    {
                        if (std::isfinite(var->getFloat()))
                        {
                            dataMap[id].push_back(TimeData(time, var));
                        }
                        newData[id] = var;
                    }
                    else if (type == VariantType::Double)
                    {
                        if (std::isfinite(var->getDouble()))
                        {
                            dataMap[id].push_back(TimeData(time, var));
                        }
                        newData[id] = var;
                    }
                    else if (VariantType::IsBasicType(type))
                    {
                        dataMap[id].push_back(TimeData(time, var));
                        newData[id] = var;
                    }
                    else
                    {
                        auto dict = JSONObject::ConvertToBasicVariantMap(json, var);

                        for (const auto& e : dict)
                        {
                            if (e.first == "timestamp") // TimedVariants always contain a timestamp field which is irrelevant
                            {
                                continue;
                            }
                            std::string key = id + "." + e.first;
                            //                            ARMARX_INFO << key << ": " << *VariantPtr::dynamicCast(e.second);
                            VariantPtr var = VariantPtr::dynamicCast(e.second);
                            auto type = var->getType();
                            if (type == VariantType::String)
                            {
                                // complex contain additional strings often, which cannot be selected right now -> disable strings from complex types
                                //                                if (dataMap[id].size() == 0 || dataMap[id].rbegin()->data->getString() != var->getString())
                                //                                {
                                //                                    // only insert if changed
                                //                                    dataMap[id].push_back(TimeData(time, var));
                                //                                    newData[key] = var;
                                //                                }
                            }
                            else
                            {
                                dataMap[key].push_back(TimeData(time, var));
                                newData[key] = var;
                            }
                        }
                    }
                }
                time_get_data_process += dt_ms(time_get_data_process_start, now());
            }
        }
        catch (Ice::NotRegisteredException& e)
        {
            ARMARX_WARNING << deactivateSpam(3) << "Caught Ice::NotRegisteredException: " << e.what();
        }
        catch (exceptions::local::InvalidDataFieldException& e)
        {
            ARMARX_WARNING << deactivateSpam(5) << "Caught InvalidDataFieldException: " << e.what();
        }
        catch (exceptions::local::InvalidChannelException& e)
        {
            ARMARX_WARNING << deactivateSpam(5) << "Caught InvalidChannelException: " << e.what();
        }
        catch (armarx::UserException& e)
        {
            ARMARX_WARNING << deactivateSpam(5) << "Caught UserException: " << e.what() << "\nReason: " << e.reason;
        }
        catch (...)
        {
            ARMARX_WARNING << deactivateSpam(5) << GetHandledExceptionString();
        }
        const auto time_get_data_end = now();
        ARMARX_DEBUG << ARMARX_STREAM_PRINTER
        {
            const auto dt_get_data = dt_ms(time_get_data_start, time_get_data_end);
            const auto dt_clear = dt_ms(time_clear_start, time_clear_end);
            out << "timings PlotterController::getData"
                << "\ndt_clear    " << dt_clear << " ms"
                << "\ndt_get_data " << dt_get_data << " ms"
                << "\n    time_get_data_find_obs     " << time_get_data_find_obs << " ms"
                << "\n    time_get_data_get_variants " << time_get_data_get_variants << " ms";
            for (const auto& [k, v] : time_get_data_get_variants_per_obs)
            {
                out << "\n        " << k << "\t" << v;
            }
            out << "\n    time_get_data_lock         " << time_get_data_lock << " ms"
                << "\n    time_get_data_process      " << time_get_data_process << " ms";
        };
        return newData;
    }

    void PlotterController::setupCurves(int samplingIntervalMs)
    {
        {
            if (samplingIntervalMs < 0)
            {
                samplingIntervalMs = pollingInterval;
            }
            std::lock_guard lock(dataMutex);
            clear();
            auto now = TimeUtil::GetTime();
            std::map<std::string, TimedVariantBaseList> histories;
            markers.clear();
            //                for(auto marker : markers)
            //                    marker->detach();
            plotter->detachItems();
            for (int i = 0; i < selectedDatafields.size(); i++)
            {
                try
                {
                    ARMARX_VERBOSE << "Channel: " << selectedDatafields.at(i).toStdString() << flush;
                    DataFieldIdentifierPtr identifier = new DataFieldIdentifier(selectedDatafields.at(i).toStdString());
                    auto prx = iceManager->getProxy<ObserverInterfacePrx>(identifier->observerName);


                    // get past data of that datafield
                    auto id = identifier->getIdentifierStr();
                    auto historiesIt = histories.find(id);

                    if (historiesIt == histories.end())
                    {
                        auto start = IceUtil::Time::now();
                        histories[id] = prx->getPartialDatafieldHistory(identifier->channelName, identifier->datafieldName,
                                        (now - IceUtil::Time::seconds(shownInterval)).toMicroSeconds(), now.toMicroSeconds(), samplingIntervalMs);
                        ARMARX_DEBUG << "history data polling took : " << (IceUtil::Time::now() - start).toMilliSecondsDouble() << " got " << histories[identifier->channelName].size() << " entries";
                        historiesIt = histories.find(id);
                    }

                    long lastTimestamp = 0;

                    VariantPtr var = VariantPtr::dynamicCast(prx->getDataField(identifier));
                    auto type = var->getType();
                    if (type == VariantType::String)
                    {
                        // do nothing for now
                    }
                    else if (VariantType::IsBasicType(type))
                    {
                        QwtPlotCurve* curve = createCurve(selectedDatafields.at(i));
                        curves[selectedDatafields.at(i).toStdString()] = curve;

                        QwtThermo* bar = createBar(selectedDatafields.at(i));
                        bars[selectedDatafields.at(i).toStdString()] = bar;

                        for (TimedVariantBasePtr& entry : historiesIt->second)
                        {
                            if (lastTimestamp + pollingInterval < entry->getTimestamp())
                            {
                                dataMap[id].push_back(TimeData(IceUtil::Time::microSeconds(entry->getTimestamp()), VariantPtr::dynamicCast(entry)));
                                lastTimestamp = entry->getTimestamp();
                            }
                        }
                    }
                    else
                    {
                        auto id = identifier->getIdentifierStr();
                        ARMARX_VERBOSE << id;
                        auto dict = JSONObject::ConvertToBasicVariantMap(json, var);

                        for (auto e : dict)
                        {
                            if (e.first == "timestamp") // TimedVariants always contain a timestamp field which is irrelevant
                            {
                                continue;
                            }
                            VariantTypeId type = e.second->getType();

                            if (type == VariantType::Double
                                || type == VariantType::Float
                                || type == VariantType::Int
                                || type == VariantType::Long)
                            {
                                std::string key = id + "." + e.first;
                                ARMARX_VERBOSE << key << ": " << *VariantPtr::dynamicCast(e.second);
                                QwtPlotCurve* curve = createCurve(QString::fromStdString(key));
                                curves[key] = curve;

                                QwtThermo* bar = createBar(QString::fromStdString(key));
                                bars[key] = bar;
                            }
                        }
                        for (auto& entry : historiesIt->second)
                        {
                            if (lastTimestamp + pollingInterval < entry->getTimestamp())
                            {
                                auto dict = JSONObject::ConvertToBasicVariantMap(json, entry);
                                for (const auto& e : dict)
                                {
                                    if (e.first == "timestamp") // TimedVariants always contain a timestamp field which is irrelevant
                                    {
                                        continue;
                                    }
                                    std::string key = id + "." + e.first;
                                    //                            ARMARX_INFO << key << ": " << *VariantPtr::dynamicCast(e.second);
                                    VariantPtr var = VariantPtr::dynamicCast(e.second);
                                    auto type = var->getType();
                                    if (type == VariantType::String)
                                    {
                                        // complex contain additional strings often, which cannot be selected right now -> disable strings from complex types
                                        //                                if (dataMap[id].size() == 0 || dataMap[id].rbegin()->data->getString() != var->getString())
                                        //                                {
                                        //                                    // only insert if changed
                                        //                                    dataMap[id].push_back(TimeData(time, var));
                                        //                                    newData[key] = var;
                                        //                                }
                                    }
                                    else
                                    {
                                        dataMap[key].push_back(TimeData(IceUtil::Time::microSeconds(entry->getTimestamp()), var));
                                    }
                                }
                            }
                        }


                    }

                }
                catch (...)
                {
                    handleExceptions();
                }
            }

            plotter->replot();

        }
        plotter->setAxisScale(QwtPlot::xBottom, shownInterval * -1, 0.f);

        timer.start(updateInterval);

        if (pollingTask)
        {
            pollingTask->stop();
        }

        pollingTask = new PeriodicTask<PlotterController>(this, & PlotterController::pollingExec, pollingInterval, false, "DataPollingTask", false);
        pollingTask->start();
    }

    void PlotterController::legendChecked(const QVariant& itemInfo, bool on)
    {
        QwtPlotItem* plotItem = plotter->infoToItem(itemInfo);
        if (plotItem)
        {
            showCurve(plotItem, on);
        }
        else
        {
            ARMARX_WARNING << "nonexistent legend item toggled to " << on;
        }
    }

    QwtPlotCurve* PlotterController::createCurve(const QString& label)
    {
        QwtPlotCurve* curve = new QwtPlotCurve(label);
        curve->setRenderHint(QwtPlotItem::RenderAntialiased);
        curve->setPen(QColor(curveColors.at(curves.size() % curveColors.size())));
        curve->setStyle(QwtPlotCurve::Lines);

        curve->setPaintAttribute(QwtPlotCurve::ClipPolygons, true);

        curve->attach(plotter);
        showCurve(curve, true);

        return curve;
    }

    QwtThermo* PlotterController::createBar(const QString& label)
    {
        //creates a widget containing a bar chart and a label
        //with the fill color of chart same as its curve

        QwtThermo* bar = new QwtThermo(this->widget);
        bar->setAccessibleName(label);
        bar->setFillBrush(QBrush(QColor(Qt::GlobalColor((bars.size() + 7) % 15))));
        bar->setStyleSheet("* { background-color: rgb(240, 240, 240); }"); //neccessary because white is a picked fill color

        QTextEdit* lab = new QTextEdit(label, this->widget);
        lab->setStyleSheet("* { background-color: rgb(240, 240, 240); }");
        lab->setLineWrapMode(QTextEdit::WidgetWidth);
        lab->setTextInteractionFlags(nullptr);
        lab->setFrameStyle(0);
        int linecount = lab->document()->blockCount();
        lab->setMaximumHeight(70 * linecount);
        lab->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        QVBoxLayout* layout = new QVBoxLayout();
        layout->addWidget(bar);
        layout->addWidget(lab);

        QWidget* widget = new QWidget(layout->widget());
        widget->setLayout(layout);
        barlayout->addWidget(widget);

        return bar;
    }

    QStringList PlotterController::getSelectedDatafields() const
    {
        std::lock_guard lock(dataMutex);
        return selectedDatafields;
    }



    std::vector<std::string> PlotterController::getSelectedDatafieldsKeys() const
    {

        std::vector<std::string> keys;

        std::lock_guard lock(dataMutex);

        for (int i = 0; i < selectedDatafields.size(); i++)
        {
            try
            {
                DataFieldIdentifierPtr identifier = new DataFieldIdentifier(selectedDatafields.at(i).toStdString());
                ObserverInterfacePrx observer = iceManager->getProxy<ObserverInterfacePrx>(identifier->observerName);

                VariantPtr var = VariantPtr::dynamicCast(observer->getDataField(identifier));
                VariantTypeId type = var->getType();
                if (type == VariantType::String || VariantType::IsBasicType(type))
                {
                    keys.push_back(selectedDatafields.at(i).toStdString());
                }
                else
                {
                    // TODO auto load libraries see ObserverItemModel.cpp:108

                    auto dict = JSONObject::ConvertToBasicVariantMap(json, var);
                    for (auto e : dict)
                    {
                        ARMARX_LOG_S << e.first;
                        if (e.first == "timestamp")
                        {
                            continue;
                        }
                        VariantTypeId type = e.second->getType();

                        if (type == VariantType::Double
                            || type == VariantType::Float
                            || type == VariantType::Int
                            || type == VariantType::Long)
                        {
                            keys.push_back(identifier->getIdentifierStr() + "." + e.first);
                        }

                    }
                }
            }
            catch (...)
            {
                handleExceptions();
            }

        }
        return keys;
    }


    void PlotterController::setSelectedDatafields(const QStringList& value, int samplingIntervalMs)
    {
        {
            std::lock_guard lock(dataMutex);
            if (selectedDatafields == value)
            {
                return;
            }
            selectedDatafields = value;
        }
        setupCurves(samplingIntervalMs);
    }

    int PlotterController::getPollingInterval() const
    {
        return pollingInterval;
    }

    void PlotterController::setPollingInterval(int value)
    {
        pollingInterval = value;
        if (pollingTask)
        {
            pollingTask->changeInterval(value);
        }
    }

    int PlotterController::getUpdateInterval() const
    {
        return updateInterval;
    }

    void PlotterController::setUpdateInterval(int value)
    {
        updateInterval = value;
        timer.start(value);
    }

    void PlotterController::setIceManager(const IceManagerPtr& value)
    {
        iceManager = value;
        json = new JSONObject(iceManager->getCommunicator());

    }

    void PlotterController::clearBarList()
    {
        //maybe comparing new and old selected list better than doing a new one every time?
        QLayoutItem* child;
        int containedItems = barlayout->count() - 1;

        for (int i = containedItems; i >= 0 ; i--) //must be done in this order due to internal implementation of layout item numbering
        {
            child = barlayout->itemAt(i);
            barlayout->removeItem(child);
            child->widget()->deleteLater();
        }

        bars.clear();
    }

    void PlotterController::clear()
    {
        std::lock_guard g(dataMutex);
        plotter->detachItems();
        markers.clear();
        curves.clear();
        dataMap.clear();
        clearBarList();

    }

    void PlotterController::clearHistory()
    {
        std::lock_guard g(dataMutex);
        dataMap.clear();
    }

    int PlotterController::getShownInterval() const
    {
        return shownInterval;
    }

    void PlotterController::setShownInterval(int value)
    {
        ARMARX_VERBOSE << "Setting shown interval to " << shownInterval;
        shownInterval = value;
        plotter->setAxisScale(QwtPlot::xBottom, shownInterval * -1, 0.f);

    }

    bool PlotterController::getAutoScale() const
    {
        return autoScalePlot;
    }

    void PlotterController::setAutoScale(bool value)
    {
        autoScalePlot = value;
    }

    std::string PlotterController::getGraphStyle() const
    {
        return graphStyle;
    }

    void PlotterController::setGraphStyle(const std::string& style)
    {
        if (graphStyle != style)
        {
            graphStyle = style;
            if (style == "Bar chart")
            {
                stackedLayout->setCurrentIndex(1);
            }
            else
            {
                stackedLayout->setCurrentIndex(0);
                QwtPlotCurve::CurveStyle st = QwtPlotCurve::Lines;
                if (style == "Curve (line, steps)")
                {
                    st = QwtPlotCurve::Steps;
                }
                else if (style == "Curve (dots)")
                {
                    st = QwtPlotCurve::Dots;
                }
                for (const auto& [_, curve] : curves)
                {
                    curve->setStyle(st);
                }
                plotter->replot();
            }
        }
    }

    std::vector<Qt::GlobalColor> PlotterController::getCurveColors() const
    {
        return curveColors;
    }

    void PlotterController::setCurveColors(const std::vector<Qt::GlobalColor>& value)
    {
        ARMARX_CHECK_GREATER(value.size(), 0);
        curveColors = value;
    }
}
