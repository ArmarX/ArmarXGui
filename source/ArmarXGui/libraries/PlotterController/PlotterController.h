/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <QBoxLayout>
#include <QObject>
#include <QPointer>
#include <QStackedLayout>
#include <QTextEdit>
#include <QTimer>
#include <qwt/qwt_plot.h>
#include <qwt/qwt_plot_magnifier.h>
#include <qwt/qwt_plot_panner.h>
//ignore errors about extra ; from qwt
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#include <qwt/qwt_plot.h>
#include <qwt/qwt_plot_marker.h>
#include <qwt/qwt_plot_curve.h>
#include <qwt/qwt_plot_panner.h>
#include <qwt/qwt_plot_magnifier.h>
#include <qwt/qwt_plot_canvas.h>
#include <qwt/qwt_legend.h>
#include <qwt/qwt_series_data.h>
#include <qwt/qwt_thermo.h>
#include <qwt/qwt_scale_draw.h>
#pragma GCC diagnostic pop

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/util/json/JSONObject.h>
#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <ArmarXCore/observers/variant/DataFieldIdentifier.h>
#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/observers/exceptions/local/InvalidChannelException.h>
#include <ArmarXCore/observers/exceptions/local/InvalidDataFieldException.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>

#include <mutex>

namespace armarx
{
    using QwtPlotMarkerPtr = QwtPlotMarker*;
    struct TimeData
    {
        TimeData(const IceUtil::Time& time, const VariantPtr& data): time(time), data(data) {}

        IceUtil::Time time;
        VariantPtr data;
    };
    using ProxyMap = std::map< std::string, ObserverInterfacePrx>;
    using GraphDataMap = std::map< std::string, std::vector<TimeData> >;


    class PlotterController :
        public QObject
    {
        Q_OBJECT
    public:
        PlotterController(QObject* parent);
        ~PlotterController() override;
        QWidget* getPlotterWidget();

        void pollingExec();


    public slots:
        void updateGraph();
        void showCurve(QwtPlotItem* item, bool on);

        void autoScale(bool toggled);

        void plottingPaused(bool toggled);
        void setupCurves(int samplingIntervalMs = -1);
        void clearHistory();

        /**
         * Required for Qt5.
         * This can't be deactivated via #if, because the moc ignores all preprocessor code.
         * Hence there is a dummy implementation for Qt4
         */
        void legendChecked(const QVariant& itemInfo, bool on);
    public:
        std::map<std::string, VariantPtr> getData(const QStringList& channels);

    protected:
        QwtPlotCurve* createCurve(const QString& label);

        QwtThermo* createBar(const QString& label);
    public:
        QStringList getSelectedDatafields() const;
        std::vector<std::string> getSelectedDatafieldsKeys() const;
        /**
         * @brief Changes the datafields that are plotted
         * @param value
         * @param samplingIntervalMs
         */
        void setSelectedDatafields(const QStringList& value, int samplingIntervalMs = -1);

        int getPollingInterval() const;
        void setPollingInterval(int value);

        int getUpdateInterval() const;
        void setUpdateInterval(int value);

        void setIceManager(const IceManagerPtr& value);

        void clearBarList();
        void clear();

        int getShownInterval() const;
        void setShownInterval(int value);

        bool getAutoScale() const;
        void setAutoScale(bool value);

        std::string getGraphStyle() const;

        std::vector<Qt::GlobalColor> getCurveColors() const;
        void setCurveColors(const std::vector<Qt::GlobalColor>& value);

    signals:
        void newDataAvailable(long timestamp, const std::map<std::string, VariantPtr>& newData);
    public slots:
        void setGraphStyle(const std::string& style);
    protected:
        // Configuration Parameters
        int pollingInterval = 33;
        int updateInterval = 33;
        int shownInterval = 60;
        std::string graphStyle = "Curve (line, interpolate)";
        bool autoScalePlot = true;
        bool __plottingPaused = false;
        QPointer<QWidget> widget;
        QPointer<QwtPlot> plotter;
        QStringList selectedDatafields;
        JSONObjectPtr json;
        IceManagerPtr iceManager;
        PeriodicTask<PlotterController>::pointer_type pollingTask;
        QTimer timer;
        mutable std::recursive_mutex dataMutex;
        GraphDataMap dataMap;
        ProxyMap proxyMap;
        std::map<std::string, QwtPlotCurve*> curves;
        std::vector<Qt::GlobalColor> curveColors;
        std::map<std::string, QwtThermo*> bars;
        std::map<std::string, std::map<IceUtil::Time, std::map<std::string, QwtPlotMarkerPtr>>> markers;

        QStackedLayout* stackedLayout;
        QHBoxLayout* barlayout;


    };



}


