/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGui::ArmarXObjects::VariantWidget
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "VariantWidget.h"

#include <QFormLayout>
#include <QLabel>
#include <QTableWidget>
#include <QVBoxLayout>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/util/StringHelpers.h>
#include <ArmarXCore/interface/observers/Complex.h>
#include <ArmarXCore/interface/observers/Matrix.h>
#include <ArmarXCore/interface/observers/Timestamp.h>
#include <ArmarXCore/interface/observers/VariantContainers.h>

namespace armarx::VariantDataWidgets
{
    class BoolVariantDataWidget : public VariantDataWidgetBase
    {
    public:
        enum class DisplayOption
        {
            boolalpha,
            Boolalpha,
            BOOLALPHA,
            number,
            XO,
            X
        };

        BoolVariantDataWidget(const VariantDataPtr& v) : disp{DisplayOption::Boolalpha}
        {
            auto l = new QVBoxLayout;
            l->setContentsMargins(0, 0, 0, 0);
            setLayout(l);
            label = new QLabel;
            l->addWidget(label);
            update(v);
        }
        void
        update(const VariantDataPtr& p) override
        {
            BoolVariantDataPtr v = BoolVariantDataPtr::dynamicCast(p);
            ARMARX_CHECK_EXPRESSION(v);
            switch (disp)
            {
                case DisplayOption::boolalpha:
                    label->setText(v->b ? "true" : "false");
                    break;
                case DisplayOption::Boolalpha:
                    label->setText(v->b ? "True" : "False");
                    break;
                case DisplayOption::BOOLALPHA:
                    label->setText(v->b ? "TRUE" : "FALSE");
                    break;
                case DisplayOption::number:
                    label->setText(v->b ? "1" : "0");
                    break;
                case DisplayOption::XO:
                    label->setText(v->b ? "X" : "O");
                    break;
                case DisplayOption::X:
                    label->setText(v->b ? "X" : " ");
                    break;
            }
        }

    private:
        QLabel* label;
        DisplayOption disp;
    };
    VariantDataWidgetFactoryRegistration<BoolVariantDataWidget> registerBoolVariantDataWidget{
        BoolVariantData::ice_staticId()};

    class DoubleVariantDataWidget : public VariantDataWidgetBase
    {
    public:
        DoubleVariantDataWidget(const VariantDataPtr& v)
        {
            auto l = new QVBoxLayout;
            l->setContentsMargins(0, 0, 0, 0);
            setLayout(l);
            label = new QLabel;
            l->addWidget(label);
            update(v);
        }
        void
        update(const VariantDataPtr& p) override
        {
            DoubleVariantDataPtr v = DoubleVariantDataPtr::dynamicCast(p);
            ARMARX_CHECK_EXPRESSION(v);
            label->setText(QString::number(v->d));
        }

    private:
        QLabel* label;
    };
    VariantDataWidgetFactoryRegistration<DoubleVariantDataWidget> registerDoubleVariantDataWidget{
        DoubleVariantData::ice_staticId()};

    class FloatVariantDataWidget : public VariantDataWidgetBase
    {
    public:
        FloatVariantDataWidget(const VariantDataPtr& v)
        {
            auto l = new QVBoxLayout;
            l->setContentsMargins(0, 0, 0, 0);
            setLayout(l);
            label = new QLabel;
            l->addWidget(label);
            update(v);
        }
        void
        update(const VariantDataPtr& p) override
        {
            FloatVariantDataPtr v = FloatVariantDataPtr::dynamicCast(p);
            ARMARX_CHECK_EXPRESSION(v);
            label->setText(QString::number(v->f));
        }

    private:
        QLabel* label;
    };
    VariantDataWidgetFactoryRegistration<FloatVariantDataWidget> registerFloatVariantDataWidget{
        FloatVariantData::ice_staticId()};

    class IntVariantDataWidget : public VariantDataWidgetBase
    {
    public:
        IntVariantDataWidget(const VariantDataPtr& v)
        {
            auto l = new QVBoxLayout;
            l->setContentsMargins(0, 0, 0, 0);
            setLayout(l);
            label = new QLabel;
            l->addWidget(label);
            update(v);
        }
        void
        update(const VariantDataPtr& p) override
        {
            IntVariantDataPtr v = IntVariantDataPtr::dynamicCast(p);
            ARMARX_CHECK_EXPRESSION(v);
            label->setText(QString::number(v->n));
        }

    private:
        QLabel* label;
    };
    VariantDataWidgetFactoryRegistration<IntVariantDataWidget> registerIntVariantDataWidget{
        IntVariantData::ice_staticId()};

    class LongVariantDataWidget : public VariantDataWidgetBase
    {
    public:
        LongVariantDataWidget(const VariantDataPtr& v)
        {
            auto l = new QVBoxLayout;
            l->setContentsMargins(0, 0, 0, 0);
            setLayout(l);
            label = new QLabel;
            l->addWidget(label);
            update(v);
        }
        void
        update(const VariantDataPtr& p) override
        {
            LongVariantDataPtr v = LongVariantDataPtr::dynamicCast(p);
            ARMARX_CHECK_EXPRESSION(v);
            label->setText(QString::number(v->n));
        }

    private:
        QLabel* label;
    };
    VariantDataWidgetFactoryRegistration<LongVariantDataWidget> registerLongVariantDataWidget{
        LongVariantData::ice_staticId()};


    class StringVariantDataWidget : public VariantDataWidgetBase
    {
    public:
        StringVariantDataWidget(const VariantDataPtr& v)
        {
            auto l = new QVBoxLayout;
            l->setContentsMargins(0, 0, 0, 0);
            setLayout(l);
            label = new QLabel;
            l->addWidget(label);
            update(v);
        }
        void
        update(const VariantDataPtr& p) override
        {
            StringVariantDataPtr v = StringVariantDataPtr::dynamicCast(p);
            ARMARX_CHECK_EXPRESSION(v);
            label->setText(QString::fromStdString(v->s));
        }

    private:
        QLabel* label;
    };
    VariantDataWidgetFactoryRegistration<StringVariantDataWidget> registerStringVariantDataWidget{
        StringVariantData::ice_staticId()};

    class TimestampBaseWidget : public VariantDataWidgetBase
    {
    public:
        TimestampBaseWidget(const VariantDataPtr& v) : suffix{"us"}
        {
            auto l = new QVBoxLayout;
            l->setContentsMargins(0, 0, 0, 0);
            setLayout(l);
            label = new QLabel;
            l->addWidget(label);
            update(v);
        }
        void
        update(const VariantDataPtr& p) override
        {
            TimestampBasePtr v = TimestampBasePtr::dynamicCast(p);
            ARMARX_CHECK_EXPRESSION(v);
            label->setText(QString::number(v->timestamp) + suffix);
        }

    private:
        QLabel* label;
        QString suffix;
    };
    VariantDataWidgetFactoryRegistration<TimestampBaseWidget> registerTimestampBaseWidget{
        TimestampBase::ice_staticId()};

    class ComplexFloatBaseWidget : public VariantDataWidgetBase
    {
    public:
        ComplexFloatBaseWidget(const VariantDataPtr& v)
        {
            auto l = new QFormLayout;
            l->setContentsMargins(0, 0, 0, 0);
            setLayout(l);
            labelR = new QLabel;
            labelI = new QLabel;
            l->addRow("Real", labelR);
            l->addRow("Imag", labelI);
            update(v);
        }
        void
        update(const VariantDataPtr& p) override
        {
            ComplexFloatBasePtr v = ComplexFloatBasePtr::dynamicCast(p);
            ARMARX_CHECK_EXPRESSION(v);
            labelR->setText(QString::number(v->real));
            labelI->setText(QString::number(v->imag));
        }

    private:
        QLabel* labelR;
        QLabel* labelI;
    };
    VariantDataWidgetFactoryRegistration<ComplexFloatBaseWidget> registerComplexFloatBaseWidget{
        ComplexFloatBase::ice_staticId()};

    class ComplexDoubleBaseWidget : public VariantDataWidgetBase
    {
    public:
        ComplexDoubleBaseWidget(const VariantDataPtr& v)
        {
            auto l = new QFormLayout;
            l->setContentsMargins(0, 0, 0, 0);
            setLayout(l);
            labelR = new QLabel;
            labelI = new QLabel;
            l->addRow("Real", labelR);
            l->addRow("Imag", labelI);
            update(v);
        }
        void
        update(const VariantDataPtr& p) override
        {
            ComplexDoubleBasePtr v = ComplexDoubleBasePtr::dynamicCast(p);
            ARMARX_CHECK_EXPRESSION(v);
            labelR->setText(QString::number(v->real));
            labelI->setText(QString::number(v->imag));
        }

    private:
        QLabel* labelR;
        QLabel* labelI;
    };
    VariantDataWidgetFactoryRegistration<ComplexDoubleBaseWidget> registerComplexDoubleBaseWidget{
        ComplexDoubleBase::ice_staticId()};

    class MatrixDoubleBaseWidget : public VariantDataWidgetBase
    {
    public:
        MatrixDoubleBaseWidget(const VariantDataPtr& v)
        {
            auto l = new QVBoxLayout;
            l->setContentsMargins(0, 0, 0, 0);
            setLayout(l);
            table = new QTableWidget;
            l->addWidget(table);
            update(v);
        }
        void
        update(const VariantDataPtr& p) override
        {
            MatrixDoubleBasePtr v = MatrixDoubleBasePtr::dynamicCast(p);
            ARMARX_CHECK_EXPRESSION(v);
            table->setRowCount(v->rows);
            table->setColumnCount(v->cols);
            for (int row = 0; row < v->rows; ++row)
            {
                for (int col = 0; col < v->cols; ++col)
                {
                    int i = row + col * v->rows;
                    table->setItem(row, col, new QTableWidgetItem{QString::number(v->data.at(i))});
                }
            }
        }

    private:
        QTableWidget* table;
    };
    VariantDataWidgetFactoryRegistration<MatrixDoubleBaseWidget> registerMatrixDoubleBaseWidget{
        MatrixDoubleBase::ice_staticId()};

    class MatrixFloatBaseWidget : public VariantDataWidgetBase
    {
    public:
        MatrixFloatBaseWidget(const VariantDataPtr& v)
        {
            auto l = new QVBoxLayout;
            l->setContentsMargins(0, 0, 0, 0);
            setLayout(l);
            table = new QTableWidget;
            l->addWidget(table);
            update(v);
        }
        void
        update(const VariantDataPtr& p) override
        {
            MatrixFloatBasePtr v = MatrixFloatBasePtr::dynamicCast(p);
            ARMARX_CHECK_EXPRESSION(v);
            table->setRowCount(v->rows);
            table->setColumnCount(v->cols);
            for (int row = 0; row < v->rows; ++row)
            {
                for (int col = 0; col < v->cols; ++col)
                {
                    int i = row + col * v->rows;
                    table->setItem(row, col, new QTableWidgetItem{QString::number(v->data.at(i))});
                }
            }
        }

    private:
        QTableWidget* table;
    };
    VariantDataWidgetFactoryRegistration<MatrixFloatBaseWidget> registerMatrixFloatBaseWidget{
        MatrixFloatBase::ice_staticId()};
} // namespace armarx::VariantDataWidgets

namespace armarx
{
    VariantWidget::VariantWidget(QWidget* parent) : QWidget{parent}
    {
        l = new QFormLayout;
        setLayout(l);
        reset();
    }

    void
    VariantWidget::reset(Mode newMode)
    {
        if (mode != Mode::Empty)
        {
            //clear layout
            while (QLayoutItem* item = l->takeAt(0))
            {
                QWidget* widget = item->widget();
                if (widget)
                {
                    widget->deleteLater();
                }
                delete item;
            }
        }
        entries.clear();
        mode = newMode;
        if (mode == Mode::Map)
        {
            l->setHorizontalSpacing(6);
        }
        else
        {
            l->setHorizontalSpacing(0);
        }
    }

    void
    VariantWidget::update(const VariantBasePtr& v)
    {
        if (mode != Mode::SingleEntry)
        {
            reset(Mode::SingleEntry);
        }
        updateEntry("SingleEntry", v);
    }

    void
    VariantWidget::update(const std::vector<VariantBasePtr>& vec)
    {
        if (mode != Mode::Vector)
        {
            reset(Mode::Vector);
        }
        for (std::size_t i = 0; i < vec.size(); ++i)
        {
            updateEntry(to_string(i), vec.at(i));
        }
    }

    void
    VariantWidget::update(const std::map<std::string, VariantBasePtr>& map)
    {
        if (mode != Mode::Map)
        {
            reset(Mode::Map);
        }
        for (const auto& entry : map)
        {
            updateEntry(entry.first, entry.second);
        }
    }

    void
    VariantWidget::updateEntry(const std::string& name, const VariantBasePtr& v)
    {
        ARMARX_CHECK_EXPRESSION(layout());
        if (entries.count(name))
        {
            //there is an old entry
            VariantDataWidgetBase* entryOld = entries.at(name);
            if (!v || !v->data)
            {
                //there is no new entry -> delete old entry delete
                if (mode == Mode::Map)
                {
                    //remove the label
                    auto label = l->labelForField(entryOld);
                    l->removeWidget(label);
                    label->deleteLater();
                }
                l->removeWidget(entryOld);
                entryOld->deleteLater();
                entries.erase(name);
            }
            else
            {
                //update the entry
                std::string type = v->data->ice_id();
                if (entryOld->getTypeName() != type)
                {
                    //other type -> exchange data widget
                    entries[name] = VariantDataWidgets::makeVariantDataWidget(v);
                    //new data widget
                    int row = getEntryRow(entryOld);
                    ARMARX_CHECK_EXPRESSION(row < l->rowCount());
                    ARMARX_CHECK_EXPRESSION(row >= 0);
                    l->removeWidget(entryOld);
                    entryOld->deleteLater();
                    l->setWidget(row, QFormLayout::FieldRole, entries[name]);
                }
                else
                {
                    //same type -> update data widget
                    entryOld->update(v->data);
                }
            }
        }
        else
        {
            entries[name] = VariantDataWidgets::makeVariantDataWidget(v);
            //add to layout
            if (mode == Mode::Map)
            {
                l->addRow(QString::fromStdString(name), entries[name]);
            }
            else
            {
                l->addRow("", entries[name]);
            }
        }
    }

    int
    VariantWidget::getEntryRow(VariantWidget::VariantDataWidgetBase* entry) const
    {
        //i need to find the correct row.
        //there is no other way than iterating over the rows and check them
        for (int row = 0; row < l->rowCount(); ++row)
        {
            QLayoutItem* it = l->itemAt(row, QFormLayout::FieldRole);
            if (it && it->widget() == entry)
            {
                return row;
            }
        }
        return -1;
    }

    VariantDataWidgets::VariantDataWidgetBase*
    VariantDataWidgets::makeVariantDataWidget(const VariantBasePtr& p)
    {
        class ErrorMessageWidget : public VariantDataWidgetBase
        {
        public:
            ErrorMessageWidget(const std::string& errorMessage)
            {
                setLayout(new QVBoxLayout);
                setTypeName(".ErrorMessageWidget");
                layout()->setContentsMargins(0, 0, 0, 0);
                QLabel* label = new QLabel{QString::fromStdString(errorMessage)};
                layout()->addWidget(label);
                label->setStyleSheet("QLabel { background-color : yellow; color : red; }");
            }
        };

        if (!p)
        {
            return new ErrorMessageWidget{"Null VariantBasePtr"};
        }
        if (!p->data)
        {
            return new ErrorMessageWidget{"Null VariantBasePtr::data"};
        }
        auto type = p->data->ice_id();
        if (VariantDataWidgetFactory::has(type))
        {
            try
            {
                VariantDataWidgetBase* w = VariantDataWidgetFactory::get(type)(p->data);
                w->setTypeName(type);
                return w;
            }
            catch (std::exception& e)
            {
                return new ErrorMessageWidget{"VariantDataWidgetFactory for " + type +
                                              " threw exception\n" + e.what()};
            }
            catch (...)
            {
                return new ErrorMessageWidget{"VariantDataWidgetFactory for " + type +
                                              " threw exception"};
            }
        }
        else
        {
            return new ErrorMessageWidget{"No VariantDataWidgetFactory for " + type};
        }
        return new ErrorMessageWidget{"makeVariantDataWidget: Unknown error"};
    }
} // namespace armarx
