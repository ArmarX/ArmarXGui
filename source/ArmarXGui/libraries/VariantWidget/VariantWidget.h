/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGui::ArmarXObjects::VariantWidget
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/interface/observers/VariantBase.h>
#include <ArmarXCore/core/util/Registrar.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <QWidget>
#include <QFormLayout>

namespace armarx::VariantDataWidgets
{
    class VariantDataWidgetBase;
}

namespace armarx
{
    class VariantWidget : public QWidget
    {
        using VariantDataWidgetBase = VariantDataWidgets::VariantDataWidgetBase;
        Q_OBJECT
    public:
        VariantWidget(QWidget* parent = nullptr);
        using QWidget::QWidget;
    public slots:
        void update(const VariantBasePtr& v);
        void update(const std::vector<VariantBasePtr>& vec);
        void update(const std::map<std::string, VariantBasePtr>& map);
    private:
        void updateEntry(const std::string& name, const VariantBasePtr& v);

        enum class Mode
        {
            Empty,
            SingleEntry,
            Vector,
            Map
        };
        void reset(Mode newMode = Mode::Empty);
        Mode mode {Mode::Empty};

        std::map<std::string, VariantDataWidgetBase*> entries;
        QFormLayout* l;
        int getEntryRow(VariantDataWidgetBase* entry) const;
    };
}

namespace armarx::VariantDataWidgets
{
    class VariantDataWidgetBase: public QWidget
    {
        Q_OBJECT
    public:
        virtual void update(const VariantDataPtr&) {}
        virtual std::string getTypeName() const
        {
            return typeName;
        }
        virtual void setTypeName(const std::string& name)
        {
            typeName = name;
        }
    private:
        std::string typeName;
    };
    using VariantDataWidgetFactory = Registrar<std::function<VariantDataWidgetBase*(const VariantDataPtr&)>>;

    template<class T>
    struct VariantDataWidgetFactoryRegistration
    {
        VariantDataWidgetFactoryRegistration(const std::string& type)
        {
            VariantDataWidgetFactory::registerElement(
                type,
                [type](const VariantDataPtr & p)
            {
                ARMARX_CHECK_EXPRESSION(p) << "Passed VariantDataPtr is null";
                ARMARX_CHECK_EXPRESSION(p->ice_id() == type) << p->ice_id() << " == " << type;
                return new T(p);
            }

            );
        }
    };
    VariantDataWidgetBase* makeVariantDataWidget(const VariantBasePtr& p);
}
