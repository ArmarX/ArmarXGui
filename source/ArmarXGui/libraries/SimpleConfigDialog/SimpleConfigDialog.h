/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGui::ArmarXObjects::SimpleConfigDialog
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/IceProxyFinder.h>

#include <ArmarXCore/core/ManagedIceObject.h>

#include <QDialog>

#include <functional>
#include <map>

class QLineEdit;
class QVBoxLayout;

namespace Ui
{
    class SimpleConfigDialog;
}

namespace armarx
{
    class IceProxyFinderBase;

    /**
    * @class SimpleConfigDialog
    * @ingroup ArmarXGui
    * @brief A config-dialog containing one (or multiple) proxy finders.
    *
    * To use it in your gui:
    * Include
    * \code{.cpp}
    * #include <ArmarXGui/libraries/SimpleConfigDialog/SimpleConfigDialog.h>
    * \endcode
    * and link against SimpleConfigDialog from ArmarXGui
    *
    * Add this member to your WidgetController
    * \code{.cpp}
    * QPointer<SimpleConfigDialog> dialog;
    * \endcode
    *
    * Override the function
    * \code{.cpp}
    * virtual QPointer<QDialog> getConfigDialog(QWidget *parent) override
    * {
    *    if (!dialog)
    *    {
    *        dialog = new SimpleConfigDialog(parent);
    *        dialog->addProxyFinder<ProxyType1>({"Accessname1", "Description text 1", "SearchMask1"});
    *        dialog->addProxyFinder<ProxyType2>({"Accessname2", "Description text 2", "SearchMask2"});
    *        dialog->getLayout()->addWidget(new QLabel{"i am a label"};
    *        //or add multiple at once
    *        dialog->addProxyFinder<ProxyType3,ProxyType4>({{"Accessname3", "Description text 3", "SearchMask3"}, {"Accessname4", "Description text 4", "SearchMask4"}});
    *    }
    *    return qobject_cast<SimpleConfigDialog*>(dialog);
    * }
    * \endcode
    *
    * Where you want to access the proxy names:
    * \code{.cpp}
    * dialog->getProxyName("Accessname1");
    * \endcode
    */
    class SimpleConfigDialog :
        public QDialog,
        virtual public armarx::ManagedIceObject
    {
        Q_OBJECT
    public:
        struct EntryData
        {
            std::string name;
            std::string description;
            std::string mask;
        };

        /**
         * @brief ctor
         * @param parent the dialog's parent
         */
        explicit SimpleConfigDialog(QWidget* parent = nullptr);

        template<class...ProxyTypes>
        void addProxyFinder(const std::vector<EntryData>& entryData)
        {
            SimpleConfigDialogAdder<ProxyTypes...>().addEntries(*this, 0, entryData);
        }

        template<class ProxyType>
        void addProxyFinder(const EntryData& entryData)
        {
            SimpleConfigDialogAdder<ProxyType>().addEntries(*this, 0, {entryData});
        }

        template<class ProxyType>
        void addProxyFinder(const std::string& name,
                            const std::string& description,
                            const std::string& mask)
        {
            addProxyFinder<ProxyType>({name, description, mask});
        }

        /**
         * @brief dtor
         */
        ~SimpleConfigDialog() override;

        std::string getProxyName(const std::string& entryName) const;

        std::string getProxyName(const std::string& entryName, const std::string& def) const;

        bool hasProxyName(const std::string& entryName) const;

        QVBoxLayout* getLayout();

        void addLineEdit(const std::string& name,
                         const std::string& label,
                         const std::string& defaultValue = "");

        std::string getLineEditText(const std::string& entryName)const;
        bool hasLineEdit(const std::string& entryName) const;

        std::string get(const std::string& entryName) const;

        std::string get(const std::string& entryName, const std::string& def) const;
    private:

        struct SimpleConfigDialogAdderBase
        {
            virtual ~SimpleConfigDialogAdderBase() = default;
            virtual armarx::IceProxyFinderBase* createIceProxyFinder(SimpleConfigDialog* d)
            {
                return nullptr;
            }

            void implAddEntries(SimpleConfigDialog& d, std::size_t index, const std::vector<EntryData>& entryData);
        };

        template<class...ProxyTs>
        struct SimpleConfigDialogAdder
        {
            void addEntries(SimpleConfigDialog&, std::size_t, const std::vector<EntryData>&)
            {
            }
        };

        template<class ProxyT, class...ProxyTs>
        struct SimpleConfigDialogAdder<ProxyT, ProxyTs...> : SimpleConfigDialogAdderBase
        {
            IceProxyFinderBase* createIceProxyFinder(SimpleConfigDialog* d) override
            {
                return new armarx::IceProxyFinder<ProxyT>(d);
            }
            void addEntries(SimpleConfigDialog& d, std::size_t index, const std::vector<EntryData>& entryData)
            {
                this->implAddEntries(d, index, entryData);
                SimpleConfigDialogAdder<ProxyTs...>().addEntries(d, index + 1, entryData);
            }
        };

        template<class...ProxyTs> friend struct SimpleConfigDialogAdder;

        struct EntryCallbacks
        {
            std::function<std::string()> proxyName;
            std::function<void(const IceManagerPtr&)> setIceManager;
        };

        std::map<std::string, EntryCallbacks> entries;
        std::map<std::string, QLineEdit*> lineEdits;
        /**
         * @brief The internal ui.
         */
        Ui::SimpleConfigDialog* ui;
        /**
         * @brief The used uuid.
         */
        std::string uuid;

    protected:
        /**
         * @brief Initializes the proxy finder
         */
        void onInitComponent() override;
        /**
         * @brief noop
         */
        void onConnectComponent() override {}
        /**
         * @brief Returns the dialog's default name.
         * @return The dialog's default name.
         */
        std::string getDefaultName() const override;
    };
}
