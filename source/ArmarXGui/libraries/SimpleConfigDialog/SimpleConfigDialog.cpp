/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGui::ArmarXObjects::SimpleConfigDialog
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "SimpleConfigDialog.h"

#include <ArmarXGui/libraries/SimpleConfigDialog/ui_SimpleConfigDialog.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <IceUtil/UUID.h>

#include <QDialog>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QLabel>


armarx::SimpleConfigDialog::SimpleConfigDialog(QWidget* parent):
    QDialog(parent),
    ui(new Ui::SimpleConfigDialog),
    uuid {IceUtil::generateUUID()}
{
    ui->setupUi(this);
}

armarx::SimpleConfigDialog::~SimpleConfigDialog()
{
    delete ui;
}

std::string armarx::SimpleConfigDialog::getProxyName(const std::string& entryName) const
{
    return entries.at(entryName).proxyName();
}

std::string armarx::SimpleConfigDialog::getProxyName(const std::string& entryName, const std::string& def) const
{
    return hasProxyName(entryName) ? getProxyName(entryName) : def;
}

bool armarx::SimpleConfigDialog::hasProxyName(const std::string& entryName) const
{
    return entries.count(entryName);
}

void armarx::SimpleConfigDialog::onInitComponent()
{
    auto manager = getIceManager();
    for (auto& entry : entries)
    {
        entry.second.setIceManager(manager);
    }
}

std::string armarx::SimpleConfigDialog::getDefaultName() const
{
    return "SimpleConfigDialog" + uuid;
}


QVBoxLayout* armarx::SimpleConfigDialog::getLayout()
{
    return ui->verticalLayout;
}

void armarx::SimpleConfigDialog::addLineEdit(const std::string& name, const std::string& label, const std::string& defaultValue)
{
    ARMARX_CHECK_EXPRESSION(!lineEdits.count(name));
    QHBoxLayout* horizontalLayout = new QHBoxLayout;
    horizontalLayout->addWidget(new QLabel {QString::fromStdString(label)});
    lineEdits[name] = new QLineEdit;
    lineEdits.at(name)->setText(QString::fromStdString(defaultValue));
    horizontalLayout->addWidget(lineEdits.at(name));
    getLayout()->addLayout(horizontalLayout);
}

std::string armarx::SimpleConfigDialog::getLineEditText(const std::string& entryName) const
{
    return lineEdits.at(entryName)->text().toStdString();
}

bool armarx::SimpleConfigDialog::hasLineEdit(const std::string& entryName) const
{
    return lineEdits.count(entryName);
}

std::string armarx::SimpleConfigDialog::get(const std::string& entryName) const
{
    const auto hasprx = hasProxyName(entryName);
    const auto hasedt = hasLineEdit(entryName);
    ARMARX_CHECK_EXPRESSION(hasprx != hasedt)
            << VAROUT(entryName) << ' ' << VAROUT(hasprx) << ' ' << VAROUT(hasedt);
    return hasprx ? getProxyName(entryName) : getLineEditText(entryName);
}

std::string armarx::SimpleConfigDialog::get(const std::string& entryName, const std::string& def) const
{
    const auto hasprx = hasProxyName(entryName);
    const auto hasedt = hasLineEdit(entryName);
    ARMARX_CHECK_EXPRESSION(!(hasprx && hasedt))
            << VAROUT(entryName) << ' ' << VAROUT(hasprx) << ' ' << VAROUT(hasedt);
    return hasprx ? getProxyName(entryName) : (hasedt ? getLineEditText(entryName) : def);
}

void armarx::SimpleConfigDialog::SimpleConfigDialogAdderBase::implAddEntries(armarx::SimpleConfigDialog& d, std::size_t index, const std::vector<armarx::SimpleConfigDialog::EntryData>& entryData)
{
    auto& entry = entryData.at(index);
    QHBoxLayout* horizontalLayout = new QHBoxLayout;

    horizontalLayout->addWidget(new QLabel {QString::fromStdString(entry.description)});

    armarx::IceProxyFinderBase* finder = createIceProxyFinder(&d);
    finder->setSearchMask(QString::fromStdString(entry.mask));
    horizontalLayout->addWidget(finder);

    d.entries[entry.name]  =
    {
        [finder]{return finder->getSelectedProxyName().trimmed().toStdString();},
        [finder](const IceManagerPtr & m)
        {
            finder->setIceManager(m);
        }
    };

    d.getLayout()->addLayout(horizontalLayout);
}
