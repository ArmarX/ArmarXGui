/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGui::ArmarXObjects::ArmarXGuiComponentPlugins
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MessageDisplayComponentPlugin.h"

#include <ArmarXCore/core/Component.h>


namespace armarx::plugins
{
    void MessageDisplayComponentPlugin::setDisplayMessage(const std::string& caption, const std::string& subCaption) const
    {
        ARMARX_CHECK_NOT_NULL(_display);
        _display->setMessage(caption, subCaption);
    }
    void MessageDisplayComponentPlugin::setDisplayMessageCaption(const std::string& text) const
    {
        ARMARX_CHECK_NOT_NULL(_display);
        _display->setCaption(text);
    }
    void MessageDisplayComponentPlugin::setDisplayMessageSubCaption(const std::string& text) const
    {
        ARMARX_CHECK_NOT_NULL(_display);
        _display->setSubCaption(text);
    }

    const MessageDisplayInterfacePrx& MessageDisplayComponentPlugin::getMessageDisplay() const
    {
        return _display;
    }

    void MessageDisplayComponentPlugin::preOnInitComponent()
    {
        if (!_display)
        {
            parent<Component>().offeringTopicFromProperty(_propertyName);
        }
    }

    void MessageDisplayComponentPlugin::preOnConnectComponent()
    {
        if (!_display)
        {
            parent<Component>().getTopicFromProperty(_display, _propertyName);
        }
    }

    void MessageDisplayComponentPlugin::postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties)
    {
        if (!properties->hasDefinition(_propertyName))
        {
            properties->defineOptionalProperty<std::string>(
                _propertyName,
                "MessageDisplayTopic",
                "Name of the message display component that should be used");
        }
    }
}

namespace armarx
{
    MessageDisplayComponentPluginUser::MessageDisplayComponentPluginUser()
    {
        addPlugin(_messageDisplayComponentPlugin);
    }

    void MessageDisplayComponentPluginUser::setDisplayMessage(const std::string& caption, const std::string& subCaption) const
    {
        _messageDisplayComponentPlugin->setDisplayMessage(caption, subCaption);
    }
    void MessageDisplayComponentPluginUser::setDisplayMessageCaption(const std::string& text) const
    {
        _messageDisplayComponentPlugin->setDisplayMessageCaption(text);
    }
    void MessageDisplayComponentPluginUser::setDisplayMessageSubCaption(const std::string& text) const
    {
        _messageDisplayComponentPlugin->setDisplayMessageSubCaption(text);
    }

    const MessageDisplayInterfacePrx& MessageDisplayComponentPluginUser::getMessageDisplay() const
    {
        return _messageDisplayComponentPlugin->getMessageDisplay();
    }
}
