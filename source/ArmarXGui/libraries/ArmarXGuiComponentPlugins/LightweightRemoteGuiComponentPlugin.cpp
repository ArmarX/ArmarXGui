/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGui::ArmarXObjects::ArmarXGuiComponentPlugins
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "LightweightRemoteGuiComponentPlugin.h"

#include <ArmarXCore/core/services/tasks/RunningTask.h>

// These are the headers which we try to avoid to include.
// It is needed here to get the actual proxy.
// But this is only in a source file, so only this file will compile slowly.
// Any user of this plugin will not need to include it.
#include <ArmarXCore/core/time/CycleUtil.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXGui/interface/RemoteGuiInterface.h>
#include <ArmarXGui/libraries/RemoteGui/Client/Impl.h>


namespace armarx
{
    using RunningTaskT = RunningTask<LightweightRemoteGuiComponentPluginUser>;
    using RunningTaskPtr = RunningTaskT::pointer_type;

    namespace plugins
    {
        static const std::string PROPERTY_NAME = "RemoteGuiName";

        void LightweightRemoteGuiComponentPlugin::preOnInitComponent()
        {
            if (!remoteGuiObject)
            {
                parent<Component>().usingProxyFromProperty(PROPERTY_NAME);
            }
        }

        void LightweightRemoteGuiComponentPlugin::preOnConnectComponent()
        {
            if (!remoteGuiObject)
            {
                remoteGuiObject = parent<Component>().getProxyFromProperty<RemoteGuiInterfacePrx>(PROPERTY_NAME);
            }
        }
        void LightweightRemoteGuiComponentPlugin::postOnConnectComponent()
        {
        }

        void LightweightRemoteGuiComponentPlugin::preOnDisconnectComponent()
        {
            if (runningTask)
            {
                RunningTaskPtr::dynamicCast(runningTask)->stop();
                runningTask = nullptr;
            }
        }

        void LightweightRemoteGuiComponentPlugin::postOnDisconnectComponent()
        {
        }

        void LightweightRemoteGuiComponentPlugin::postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties)
        {
            if (!properties->hasDefinition(PROPERTY_NAME))
            {
                properties->defineOptionalProperty<std::string>(
                    "RemoteGuiName",
                    "RemoteGuiProvider",
                    "Name of the remote gui provider");
            }
        }
    }

    LightweightRemoteGuiComponentPluginUser::LightweightRemoteGuiComponentPluginUser()
    {
        addPlugin(plugin);
    }

    void LightweightRemoteGuiComponentPluginUser::RemoteGui_createTab(
        std::string const& name,
        RemoteGui::Client::Widget const& rootWidget,
        RemoteGui::Client::Tab* tab)
    {
        if (!plugin->remoteGuiObject)
        {
            throw armarx::LocalException()
                    << "You tried to create a RemoteGui tab before the component was initialized.\n"
                    << "You can start creating tabs once you have entered your onConnectComponent().";
        }
        tab->create(name, plugin->remoteGuiObject, rootWidget);
        plugin->tabs[name] = &tab->impl->proxy;
    }

    void LightweightRemoteGuiComponentPluginUser::RemoteGui_startRunningTask()
    {
        if (plugin->runningTask)
        {
            throw armarx::LocalException()
                    << "There is already a RunningTask which has not been stopped.";
        }
        RunningTaskPtr runningTask = new RunningTaskT(
            this,
            &LightweightRemoteGuiComponentPluginUser::RemoteGui_run);
        runningTask->start();
        plugin->runningTask = runningTask;
    }

    bool LightweightRemoteGuiComponentPluginUser::RemoteGui_isRunning()
    {
        if (plugin->runningTask)
        {
            return !RunningTaskPtr::dynamicCast(plugin->runningTask)->isStopped();
        }
        else
        {
            return false;
        }
    }

    void LightweightRemoteGuiComponentPluginUser::RemoteGui_run()
    {
        CycleUtil c(RemoteGui_updatePeriodInMS);
        while (RemoteGui_isRunning())
        {
            for (auto& tab : plugin->tabs)
            {
                tab.second->receiveUpdates();
            }
            RemoteGui_update();
            for (auto& tab : plugin->tabs)
            {
                tab.second->sendUpdates();
            }
            c.waitForCycleDuration();
        }
    }

}
