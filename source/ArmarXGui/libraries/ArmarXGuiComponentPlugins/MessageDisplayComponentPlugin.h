/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGui::ArmarXObjects::ArmarXGuiComponentPlugins
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>
#include <map>
#include <functional>

#include <ArmarXCore/core/ComponentPlugin.h>

#include <ArmarXGui/interface/MessageDisplayInterface.h>


namespace armarx::plugins
{
    /**
     * @class MessageDisplayComponentPlugin
     * @ingroup Library-ArmarXGuiComponentPlugins
     * @brief Brief description of class MessageDisplayComponentPlugin.
     *
     * Detailed description of class MessageDisplayComponentPlugin.
     */
    class MessageDisplayComponentPlugin : public ComponentPlugin
    {
    public:
        using ComponentPlugin::ComponentPlugin;

        void setDisplayMessage(const std::string& caption, const std::string& subCaption) const;
        void setDisplayMessageCaption(const std::string& text) const;
        void setDisplayMessageSubCaption(const std::string& text) const;

        const MessageDisplayInterfacePrx& getMessageDisplay() const;
    protected:
        void preOnInitComponent() override;
        void preOnConnectComponent() override;

        void postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties) override;

        //data
    private:
        static constexpr auto _propertyName = "MessageDisplayTopic";
        MessageDisplayInterfacePrx _display;
    };
}


#include <ArmarXCore/core/ManagedIceObject.h>

namespace armarx
{
    class MessageDisplayComponentPluginUser : virtual public ManagedIceObject
    {
    public:
        using MessageDisplayComponentPlugin = armarx::plugins::MessageDisplayComponentPlugin;
    private:
        MessageDisplayComponentPlugin* _messageDisplayComponentPlugin{nullptr};
    public:
        MessageDisplayComponentPluginUser();

        void setDisplayMessage(const std::string& caption, const std::string& subCaption) const;
        void setDisplayMessageCaption(const std::string& text) const;
        void setDisplayMessageSubCaption(const std::string& text) const;

        const MessageDisplayInterfacePrx& getMessageDisplay() const;
    };
}
