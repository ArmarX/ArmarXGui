/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGui::ArmarXObjects::ArmarXGuiComponentPlugins
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXGui/libraries/RemoteGui/Client/Tab.h>
#include <ArmarXGui/libraries/RemoteGui/Client/Widgets.h>

#include <ArmarXCore/core/ComponentPlugin.h>

namespace armarx::RemoteGui
{
    class TabProxy;
}

namespace armarx::plugins
{
    /**
    * @defgroup Library-ArmarXGuiComponentPlugins ArmarXGuiComponentPlugins
    * @ingroup ArmarXGui
    * A description of the library ArmarXGuiComponentPlugins.
    */

    /**
     * @class ArmarXGuiComponentPlugins
     * @ingroup Library-ArmarXGuiComponentPlugins
     * @brief Brief description of class ArmarXGuiComponentPlugins.
     *
     * Detailed description of class ArmarXGuiComponentPlugins.
     */
    struct LightweightRemoteGuiComponentPlugin : ComponentPlugin
    {
        using ComponentPlugin::ComponentPlugin;

        void preOnInitComponent() override;
        void preOnConnectComponent() override;
        void postOnConnectComponent() override;
        void preOnDisconnectComponent() override;
        void postOnDisconnectComponent() override;

        void postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties) override;

        Ice::ObjectPrx remoteGuiObject;
        IceUtil::Handle<IceUtil::Shared> runningTask;
        std::map<std::string, RemoteGui::TabProxy*> tabs;
    };
}


#include <ArmarXCore/core/ManagedIceObject.h>

namespace armarx
{
    struct LightweightRemoteGuiComponentPluginUser : virtual ManagedIceObject
    {
        LightweightRemoteGuiComponentPluginUser();

        void RemoteGui_createTab(std::string const& name,
                                 RemoteGui::Client::Widget const& rootWidget,
                                 RemoteGui::Client::Tab* tab);

        void RemoteGui_startRunningTask();
        virtual void RemoteGui_update() = 0;

        bool RemoteGui_isRunning();

        void RemoteGui_run();

        int RemoteGui_updatePeriodInMS = 20;
        plugins::LightweightRemoteGuiComponentPlugin* plugin = nullptr;
    };
}
