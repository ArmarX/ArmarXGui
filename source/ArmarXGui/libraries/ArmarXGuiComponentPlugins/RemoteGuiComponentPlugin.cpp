/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGui::ArmarXObjects::ArmarXGuiComponentPlugins
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RemoteGuiComponentPlugin.h"

#include <ArmarXCore/core/Component.h>


namespace armarx::plugins
{
    void RemoteGuiComponentPlugin::createOrUpdateTab(
        const RemoteGui::WidgetPtr& widget,
        RemoteGuiComponentPlugin::TabTask guiTaskFnc,
        unsigned long guiTaskPeriod)
    {
        createOrUpdateTab("", widget, guiTaskFnc, guiTaskPeriod);
    }

    void RemoteGuiComponentPlugin::createOrUpdateTab(
        const std::string& name,
        const RemoteGui::WidgetPtr& widget,
        RemoteGuiComponentPlugin::TabTask guiTaskFnc,
        unsigned long guiTaskPeriod)
    {
        std::lock_guard g{_tabsMutex};
        if (hasTab(name))
        {
            ARMARX_INFO << "Replacing the tab " << tabName(name);
        }
        auto& data = _tabs[name];
        if (data.task)
        {
            data.task->stop();
            data.task = nullptr;
        }
        data.fnc = std::move(guiTaskFnc);
        data.taskPeriod = guiTaskPeriod;
        data.widget = widget;
        create(name, data);
    }

    void RemoteGuiComponentPlugin::createOrUpdateTab(const std::string& name, const RemoteGui::WidgetPtr& widget,
                                                     unsigned long guiTaskPeriod)
    {
        std::lock_guard g{_tabsMutex};
        if (!hasTab(name))
        {
            throw armarx::LocalException("Can only update Tab with a name for an already existing Tab");
        }
        auto& data = _tabs[name];
        if (data.task)
        {
            data.task->stop();
            data.task = nullptr;
        }
        data.taskPeriod = guiTaskPeriod;
        data.widget = widget;
        create(name, data);
    }

    RemoteGui::TabProxy& RemoteGuiComponentPlugin::getTab(const std::string& name)
    {
        ARMARX_CHECK_EXPRESSION(hasTab(name));
        std::lock_guard g{_tabsMutex};
        return _tabs.at(name).tab;
    }

    bool RemoteGuiComponentPlugin::hasTab(const std::string& name)
    {
        std::lock_guard g{_tabsMutex};
        return _tabs.count(name);
    }

    void RemoteGuiComponentPlugin::removeTab(const std::string& name)
    {
        std::lock_guard g{_tabsMutex};
        if (_tabs.count(name))
        {
            auto& data = _tabs.at(name);
            remove(name, data);
        }
        _tabs.erase(name);
    }

    const RemoteGuiInterfacePrx& RemoteGuiComponentPlugin::getRemoteGui() const
    {
        return _remoteGui;
    }

    void RemoteGuiComponentPlugin::preOnInitComponent()
    {
        if (!_remoteGui)
        {
            parent<Component>().usingProxyFromProperty(makePropertyName(_propertyName));
        }
    }

    void RemoteGuiComponentPlugin::preOnConnectComponent()
    {
        if (!_remoteGui)
        {
            parent<Component>().getProxyFromProperty(_remoteGui, makePropertyName(_propertyName));
        }
    }
    void RemoteGuiComponentPlugin::postOnConnectComponent()
    {
        std::lock_guard g{_tabsMutex};
        for (auto& [name, data] : _tabs)
        {
            if (data.task)
            {
                continue;
            }
            ARMARX_CHECK_IS_NULL(data.task);
            create(name, data);
        }
    }

    void RemoteGuiComponentPlugin::postOnDisconnectComponent()
    {
        std::lock_guard g{_tabsMutex};
        for (auto& [name, data] : _tabs)
        {
            if (data.task)
            {
                data.task->stop();
                data.task = nullptr;
            }
            remove(name, data);
        }
        _remoteGui = nullptr;
    }

    void RemoteGuiComponentPlugin::postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties)
    {
        if (!properties->hasDefinition(makePropertyName(_propertyName)))
        {
            properties->defineOptionalProperty<std::string>(
                makePropertyName(_propertyName),
                "RemoteGuiProvider",
                "Name of the remote gui provider");
        }
    }

    std::string RemoteGuiComponentPlugin::tabName(const std::string& name)
    {
        return parent<ManagedIceObject>().getName() + (name.empty() ? "" : "_" + name);
    }

    void RemoteGuiComponentPlugin::create(const std::string& name, RemoteGuiComponentPlugin::TabData& td)
    {
        {
            const auto state = parent<ManagedIceObject>().getState();
            if (
                state != ManagedIceObjectState::eManagedIceObjectStarting &&
                state != ManagedIceObjectState::eManagedIceObjectStarted
            )
            {
                return;
            }
        }
        ARMARX_CHECK_NOT_NULL(_remoteGui);
        if (td.task)
        {
            td.task->stop();
            td.task = nullptr;
        }
        const auto tabname = tabName(name);
        td.task = new SimplePeriodicTask<>([&td] {td.fnc(td.tab);}, td.taskPeriod);
        _remoteGui->createTab(tabname, td.widget);
        td.tab = RemoteGui::TabProxy(_remoteGui, tabname);
        td.task->start();
    }
    void RemoteGuiComponentPlugin::remove(const std::string& name, RemoteGuiComponentPlugin::TabData& td)
    {
        if (td.task)
        {
            td.task->stop();
            td.task = nullptr;
        }
        try
        {
            const auto tabname = tabName(name);
            ARMARX_INFO << "Removing tab: " << tabname;
            _remoteGui->removeTab(tabname);
        }
        catch (...) {}
    }
}

namespace armarx
{
    RemoteGuiComponentPluginUser::RemoteGuiComponentPluginUser()
    {
        addPlugin(_remoteGuiComponentPlugin);
    }

    RemoteGui::TabProxy& RemoteGuiComponentPluginUser::getRemoteGuiTab(const std::string& name)
    {
        return getRemoteGuiPlugin().getTab(name);
    }

    bool RemoteGuiComponentPluginUser::hasRemoteGuiTab(const std::string& name)
    {
        return getRemoteGuiPlugin().hasTab(name);
    }

    void RemoteGuiComponentPluginUser::removeRemoteGuiTab(const std::string& name)
    {
        getRemoteGuiPlugin().removeTab(name);
    }

    const RemoteGuiInterfacePrx& RemoteGuiComponentPluginUser::getRemoteGui() const
    {
        return getRemoteGuiPlugin().getRemoteGui();
    }
    const armarx::plugins::RemoteGuiComponentPlugin& RemoteGuiComponentPluginUser::getRemoteGuiPlugin() const
    {
        return *_remoteGuiComponentPlugin;
    }
    armarx::plugins::RemoteGuiComponentPlugin& RemoteGuiComponentPluginUser::getRemoteGuiPlugin()
    {
        return *_remoteGuiComponentPlugin;
    }
}
