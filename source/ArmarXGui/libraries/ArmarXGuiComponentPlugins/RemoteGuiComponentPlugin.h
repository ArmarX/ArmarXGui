/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGui::ArmarXObjects::ArmarXGuiComponentPlugins
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>
#include <map>
#include <functional>

#include <ArmarXCore/core/ComponentPlugin.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>
#include <ArmarXCore/util/CPPUtility/TripleBuffer.h>

#include <ArmarXGui/libraries/RemoteGui/ConfigIntrospection.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetProxy.h>


namespace armarx::plugins
{
    /**
    * @defgroup Library-ArmarXGuiComponentPlugins ArmarXGuiComponentPlugins
    * @ingroup ArmarXGui
    * A description of the library ArmarXGuiComponentPlugins.
    */

    /**
     * @class ArmarXGuiComponentPlugins
     * @ingroup Library-ArmarXGuiComponentPlugins
     * @brief Brief description of class ArmarXGuiComponentPlugins.
     *
     * Detailed description of class ArmarXGuiComponentPlugins.
     */
    class RemoteGuiComponentPlugin : public ComponentPlugin
    {
    public:

        template<class T>
        using enable_if_auto_gui_cfg = std::enable_if_t<meta::cfg::gui_definition_create<T>::value>;

        using TabTask = std::function<void(RemoteGui::TabProxy&)>;

        using ComponentPlugin::ComponentPlugin;

        void createOrUpdateTab(const RemoteGui::WidgetPtr& widget,
                               TabTask guiTaskFnc,
                               unsigned long guiTaskPeriod = 10);
        void createOrUpdateTab(const std::string& name,
                               const RemoteGui::WidgetPtr& widget,
                               TabTask guiTaskFnc,
                               unsigned long guiTaskPeriod = 10);

        void createOrUpdateTab(const std::string& name,
                               const RemoteGui::WidgetPtr& widget,
                               unsigned long guiTaskPeriod = 10);

        template<class T>
        enable_if_auto_gui_cfg<T>
        createOrUpdateTab(
            const std::string& name,
            T& data,
            unsigned long guiTaskPeriod = 10)
        {
            ARMARX_INFO << "create remote tab '" << name
                        << "'. writing data to 0x" << &data << " (type: "
                        << simox::meta::get_type_name<T>() << ')';
            createOrUpdateTab(
                name,
                RemoteGui::MakeGuiConfig(name, data),
                [nam = name, ptr = &data](RemoteGui::TabProxy & prx)
            {
                prx.receiveUpdates();
                ARMARX_DEBUG << deactivateSpam(1)
                             << "updating remote tab '" << nam
                             << "'. writing data to 0x" << ptr << " (type: "
                             << simox::meta::get_type_name<T>() << ')';
                prx.getValue(*ptr, nam);
                prx.sendUpdates();
            },
            guiTaskPeriod
            );
        }
        template<class T>
        enable_if_auto_gui_cfg<T>
        createOrUpdateTab(T& data,
                          unsigned long guiTaskPeriod = 10)
        {
            createOrUpdateTab("", data, guiTaskPeriod);
        }

        template<class T>
        enable_if_auto_gui_cfg<T>
        createOrUpdateTab(
            const std::string& name,
            WriteBufferedTripleBuffer<T>& data,
            unsigned long guiTaskPeriod = 10)
        {
            ARMARX_INFO << "create remote tab '" << name
                        << "'. writing data to 0x" << &data << " (type: "
                        << simox::meta::get_type_name<T>() << ')';
            createOrUpdateTab(
                name,
                RemoteGui::MakeGuiConfig(name, data.getWriteBuffer()),
                [nam = name, ptr = &data](RemoteGui::TabProxy & prx)
            {
                ARMARX_TRACE;
                ARMARX_DEBUG << deactivateSpam(1)
                             << "updating remote tab '" << nam
                             << "'. writing data to 0x" << ptr << " (type: "
                             << simox::meta::get_type_name<T>() << ')';
                prx.receiveUpdates();
                prx.getValue(ptr->getWriteBuffer(), nam);
                ptr->commitWrite();
                prx.sendUpdates();
            },
            guiTaskPeriod
            );
        }
        template<class T>
        enable_if_auto_gui_cfg<T>
        createOrUpdateTab(
            WriteBufferedTripleBuffer<T>& data,
            unsigned long guiTaskPeriod = 10)
        {
            createOrUpdateTab("", data, guiTaskPeriod);
        }

        template<class T, class LockableT>
        enable_if_auto_gui_cfg<T>
        createOrUpdateTab(
            const std::string& name,
            WriteBufferedTripleBuffer<T>& data,
            LockableT& mutex,
            unsigned long guiTaskPeriod = 10)
        {
            ARMARX_INFO << "create remote tab '" << name
                        << "'. writing data to 0x" << &data << " (type: "
                        << simox::meta::get_type_name<T>() << ')';
            std::lock_guard lock{mutex};
            createOrUpdateTab(
                name,
                RemoteGui::MakeGuiConfig(name, data.getWriteBuffer()),
                [nam = name, mtx = &mutex, ptr = &data](RemoteGui::TabProxy & prx)
            {
                ARMARX_TRACE;
                ARMARX_DEBUG << deactivateSpam(1)
                             << "updating remote tab '" << nam
                             << "'. writing data to 0x" << ptr << " (type: "
                             << simox::meta::get_type_name<T>() << ')';
                prx.receiveUpdates();
                {
                    std::lock_guard<LockableT> lock{*mtx};
                    prx.getValue(ptr->getWriteBuffer(), nam);
                    ptr->commitWrite();
                }
                prx.sendUpdates();
            },
            guiTaskPeriod
            );
        }
        template<class T, class LockableT>
        enable_if_auto_gui_cfg<T>
        createOrUpdateTab(
            WriteBufferedTripleBuffer<T>& data,
            LockableT& mutex,
            unsigned long guiTaskPeriod = 10)
        {
            createOrUpdateTab("", data, mutex, guiTaskPeriod);
        }



        RemoteGui::TabProxy& getTab(const std::string& name = "");

        bool hasTab(const std::string& name = "");

        void removeTab(const std::string& name = "");

        const RemoteGuiInterfacePrx& getRemoteGui() const;
    protected:
        void preOnInitComponent() override;
        void preOnConnectComponent() override;
        void postOnConnectComponent() override;
        void postOnDisconnectComponent() override;

        void postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties) override;

        //structs
    private:
        struct TabData
        {
            //settings
            TabTask                            fnc;
            RemoteGui::WidgetPtr               widget;
            unsigned long                      taskPeriod{10};

            //run data
            SimplePeriodicTask<>::pointer_type task;
            RemoteGui::TabProxy                tab;
        };

        //fncs
    private:
        std::string tabName(const std::string& name);
        void create(const std::string& name, TabData& td);
        void remove(const std::string& name, TabData& td);

        //data
    private:
        static constexpr auto _propertyName = "RemoteGuiName";
        RemoteGuiInterfacePrx           _remoteGui;
        mutable std::recursive_mutex    _tabsMutex;
        std::map<std::string, TabData>  _tabs;
    };
}


#include <ArmarXCore/core/ManagedIceObject.h>

namespace armarx
{
    class RemoteGuiComponentPluginUser : virtual public ManagedIceObject
    {
    public:
        using RemoteGuiComponentPlugin = armarx::plugins::RemoteGuiComponentPlugin;
        using RemoteGuiTabTask = RemoteGuiComponentPlugin::TabTask;
    private:
        RemoteGuiComponentPlugin* _remoteGuiComponentPlugin{nullptr};
    public:
        RemoteGuiComponentPluginUser();

        const RemoteGuiInterfacePrx& getRemoteGui() const;
        const armarx::plugins::RemoteGuiComponentPlugin& getRemoteGuiPlugin() const;
        armarx::plugins::RemoteGuiComponentPlugin& getRemoteGuiPlugin();

        template<class...Ts>
        void createOrUpdateRemoteGuiTab(Ts&& ...ts)
        {
            getRemoteGuiPlugin().createOrUpdateTab(std::forward<Ts>(ts)...);
        }

        RemoteGui::TabProxy& getRemoteGuiTab(const std::string& name = "");

        bool hasRemoteGuiTab(const std::string& name = "");

        void removeRemoteGuiTab(const std::string& name = "");
    };
}
