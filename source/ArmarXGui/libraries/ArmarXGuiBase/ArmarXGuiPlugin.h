/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "ArmarXGuiInterface.h"
#include "ArmarXComponentWidgetController.h"

#include <ArmarXCore/util/CPPUtility/TemplateMetaProgramming.h>
#include <ArmarXCore/core/exceptions/LocalException.h>

#include <type_traits>

namespace armarx
{
    /**
      \defgroup ArmarXGuiBase ArmarXGui

      The ArmarXGuiBase library is used to build custom ArmarXGui plugins.
      See \ref ArmarXGui-Tutorials-CreateGuiPlugin andf \ref ArmarXGui-HowTos-Build-Gui-Plugin for detailed instructions on custom gui plugin creation.

      \class ArmarXGuiPlugin

      \ingroup ArmarXGuiBase
     */
    class ArmarXGuiPlugin :
        public QObject,
        public ArmarXGuiInterface
    {
        Q_OBJECT
        Q_INTERFACES(ArmarXGuiInterface)

    public:

        ARMARX_META_MAKE_HAS_MEMBER_FNC_CHECK(HasGetWidgetName,  GetWidgetName, QString(*)());

        template < typename ArmarXWidgetType>
        typename std::enable_if < !HasGetWidgetName<ArmarXWidgetType>::value >::type addWidget()
        {
            static_assert(std::is_base_of_v<ArmarXWidgetController, ArmarXWidgetType>,
                          "The template parameter of addWidget, must be a class that derives from ArmarXWidget");

            try
            {
                ArmarXWidgetControllerPtr creatorInstance(new ArmarXWidgetType());

                if (__availableWidgets.find(creatorInstance->getWidgetName()) != __availableWidgets.end())
                {
                    throw LocalException(QString("A widget with the name '"
                                                 + creatorInstance->getWidgetName()
                                                 + "' already exists in a loaded plugin!").toStdString());
                }

                if (ArmarXComponentWidgetControllerPtr::dynamicCast(creatorInstance))
                {
                    ArmarXWidgetInfoPtr widgetInfo(new ArmarXWidgetInfo(ArmarXComponentWidgetController::createInstance<ArmarXWidgetType>,
                                                   creatorInstance->getWidgetIcon(),
                                                   creatorInstance->getWidgetCategoryIcon()));
                    __availableWidgets[creatorInstance->getWidgetName()] = widgetInfo;
                }
                else
                {
                    ArmarXWidgetInfoPtr widgetInfo(new ArmarXWidgetInfo(ArmarXWidgetController::createInstance<ArmarXWidgetType>,
                                                   creatorInstance->getWidgetIcon(),
                                                   creatorInstance->getWidgetCategoryIcon()));
                    __availableWidgets[creatorInstance->getWidgetName()] = widgetInfo;
                }
            }
            catch (...)
            {
                ARMARX_ERROR_S << "Could not add widget!\n" << GetHandledExceptionString();
            }

            //            ARMARX_VERBOSE_S << "Added new widget named " + creatorInstance->getWidgetName().toStdString() << std::endl;
        }

        template <typename ArmarXWidgetType>
        typename std::enable_if<HasGetWidgetName<ArmarXWidgetType>::value>::type addWidget()
        {
            static_assert(std::is_base_of_v<ArmarXWidgetController, ArmarXWidgetType>,
                          "The template parameter of addWidget, must be a class that derives from ArmarXWidget");

            try
            {
                if (__availableWidgets.find(ArmarXWidgetType::GetWidgetName()) != __availableWidgets.end())
                {
                    throw LocalException(QString("A widget with the name '"
                                                 + ArmarXWidgetType::GetWidgetName()
                                                 + "' already exists in a loaded plugin!").toStdString());
                }

                if (std::is_base_of_v<ArmarXComponentWidgetController, ArmarXWidgetType>)
                {
                    ArmarXWidgetInfoPtr widgetInfo(new ArmarXWidgetInfo(ArmarXComponentWidgetController::createInstance<ArmarXWidgetType>,
                                                   ArmarXWidgetType::GetWidgetIcon(),
                                                   ArmarXWidgetType::GetWidgetCategoryIcon()));
                    __availableWidgets[ArmarXWidgetType::GetWidgetName()] = widgetInfo;
                }
                else
                {
                    ArmarXWidgetInfoPtr widgetInfo(new ArmarXWidgetInfo(ArmarXWidgetController::createInstance<ArmarXWidgetType>,
                                                   ArmarXWidgetType::GetWidgetIcon(),
                                                   ArmarXWidgetType::GetWidgetCategoryIcon()));
                    __availableWidgets[ArmarXWidgetType::GetWidgetName()] = widgetInfo;
                }
            }
            catch (...)
            {
                ARMARX_ERROR_S << "Could not add widget!\n" << GetHandledExceptionString();
            }
        }

        WidgetCreatorMap getProvidedWidgets() override
        {
            return __availableWidgets;
        }

    private:
        WidgetCreatorMap __availableWidgets;
    };
}

