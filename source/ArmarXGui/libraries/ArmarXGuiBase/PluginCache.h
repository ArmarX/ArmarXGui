#pragma once

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXWidgetController.h>

#include <ArmarXCore/core/ArmarXFwd.h>

#include <QDateTime>
#include <QPluginLoader>
#include <QSet>

#include <memory>
#include <future>
#include <mutex>


class QProgressDialog;

class ArmarXWidgetInfo;
using ArmarXWidgetInfoPtr = std::shared_ptr<ArmarXWidgetInfo>;

typedef armarx::ArmarXWidgetControllerPtr(*WidgetCreatorFunction)();
using WidgetCreatorMap = std::map<QString, ArmarXWidgetInfoPtr >;

namespace armarx
{

    struct PluginData
    {
        QSharedPointer<QPluginLoader> pluginLoader;
        QString pluginPath;
        QByteArray hash;
        QDateTime lastModified;
        WidgetCreatorMap widgets;
    };

    class PluginCache
    {
    public:
        PluginCache(ArmarXManagerPtr manager);
        ~PluginCache();
        bool cachePlugin(const QString& pluginPath);
        bool cacheWidget(QString widgetName, ArmarXWidgetInfoPtr widgetCreator);
        ArmarXWidgetInfoPtr getWidgetCreator(const QString& widgetName);
        QStringList getAvailableWidgetNames() const;
        WidgetCreatorMap getAvailableWidgets() const;
        const QString settingsOrganization = "KIT";
        const QString settingsApplicationName = "PluginCache";
        void copyResourcesToCache();
        static QString GetIconCachePath();
        static QString GetIconPath(const QString& widgetName);
        static QString GetCategoryIconPath(const QString& widgetName);
        void preloadAsync(QStringList widgetNames, int delayMS = 1000);
        void clearCacheFile();
        QByteArray getHash(const QString& pluginPath);

        void removeWidgetFromCache(QString pluginPath, QString widgetName);
        void removePluginFromCache(QString pluginPath);
        static bool ContainsAny(const QString& str, const QStringList& items);
    protected:
        void updateLastModifiedTimestamp(const QString& pluginPath);
        void writeToCache(const QString& pluginPath);
        PluginData loadFromCache(const QString& pluginPath);
        WidgetCreatorMap loadPlugin(QSharedPointer<QPluginLoader> loader);
        const QString cachePath;
        QMap<QString, PluginData> pluginData;
        ArmarXManagerPtr manager;
        QSettings s;
        mutable std::recursive_mutex cacheMutex;
        std::future<void> preloadFuture;
        bool shutdown = false;

    };
    using PluginCachePtr = std::shared_ptr<PluginCache>;

} // namespace armarx

