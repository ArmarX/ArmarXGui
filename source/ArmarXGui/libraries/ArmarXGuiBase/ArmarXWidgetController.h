/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Gui
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/logging/Logging.h>

#include <IceUtil/Shared.h>

#include <QSettings>
#include <QPointer>
#include <QWidget>

#include <memory>
#include <mutex>


class SoNode;
class QDialog;
class QMainWindow;

namespace armarx
{
    class ArmarXWidgetController;
    class TipDialog;
    using ArmarXWidgetControllerPtr = IceUtil::Handle<ArmarXWidgetController>;
    /**
      \class ArmarXWidgetController

      \ingroup ArmarXGuiBase
     */
    class ArmarXWidgetController :
        public QObject,
        virtual public Logging,
        virtual public IceUtil::Shared
    {
        Q_OBJECT
    public:
        explicit ArmarXWidgetController();
        ~ArmarXWidgetController() override;

        /**
         * @brief getWidget returns a pointer to the a widget of this
         * controller.
         *
         * This widget should be the main widget of this WidgetController.
         * The standard implementation of this function creates a QWidget on the
         * first call and assigns the pointer to the widget-member and returns
         * the member.
         * @return pointer to the main widget.
         *
         * @note This function must <b>always, at any time</b> return a valid
         * pointer. So if you reimplement it, you should create the widget in this function
         * on the first call.
         */
        virtual QPointer<QWidget> getWidget();

        /**
         * @brief getConfigDialog returns a pointer to the a configuration
         * widget of this controller. It is optional.
         *
         * If you need your widget configured, before it is shown, you should
         * implement this function and return a QDialog derived class.
         * @param parent Pointer to the object, that should own this dialog.
         * @return pointer to a QDialog derived class, that should be used to
         * configure your widget.
         *
         * @note When the dialog gets accepted (emit SIGNAL accepted()), the function
         * configured() is called, which must be implemented as well.
         *
         * @see configured(), getWidget()
         */
        virtual QPointer<QDialog> getConfigDialog(QWidget* parent = 0);

        /**
         * @brief This function must be implemented by the user, if he supplies
         * a config dialog. This function is then automatically called, when
         * the dialog was accepted.
         *
         * @see getConfigDialog()
         */
        virtual void configured() {}

        /**
         * @brief getTitleToolbar returns a pointer to the a toolbar
         * widget of this controller. It is optional. It must return the same instance
         * on all calls.
         *
         * The returned toolbar is displayed in the titlebar of the parent dockwidget.
         * Use this to show button that should always be visible in the widget, but should
         * not take up space in the widget. Keep the size of the toolbar to a minimum, so that
         * it is not wider than the widget itself.
         */
        virtual QPointer<QWidget> getCustomTitlebarWidget(QWidget* parent = 0);

        /**
         * @brief Implement this function to specify the default name of your Widget.
         *
         * Implement the function as:
         * \code{.cpp}
         * static QString GetWidgetName()
         * {
         *      return "my_widget_category.my_widget_name"
         * }
         * virtual QString getWidgetName() const override
         * {
         *      return GetWidgetName();
         * }
         * \endcode
         * If you implement the static GetWidgetName() function, the armarx gui does not need to instantiate
         * this widget only to get the name.
         *
         * Dot-Notation can be used to insert the widget in categories and subcategories
         * (e.g.: "Statecharts.StatechartEditor"). Categories must not collide with Widget names.
         * @return Default name of the implemented Widget.
         */
        virtual QString getWidgetName() const = 0;

        /**
         * @brief Implement this function to supply an icon for the menu.
         * @return
         */
        virtual QIcon getWidgetIcon() const;

        /**
         * @brief Implement this function to supply an icon for the menu (if you implemented static QString GetWidgetName()).
         *
         * The implementation should look like this:
         * \code{.cpp}
         * static QIcon GetWidgetIcon()
         * {
         *      return QIcon{"my_resource_path"};
         * }
         * virtual QIcon getWidgetIcon() const override
         * {
         *      return GetWidgetIcon();
         * }
         * \endcode
         */
        static QIcon GetWidgetIcon();

        /**
         * @brief Implement this function to supply an icon for the category.
         * @return
         */
        virtual QIcon getWidgetCategoryIcon() const;
        /**
         * @brief Implement this function to supply an icon for the menu (if you implemented static QString GetWidgetName()).
         *
         * The implementation should look like this:
         * \code{.cpp}
         * static QIcon GetWidgetCategoryIcon()
         * {
         *      return QIcon{"my_resource_path"};
         * }
         * virtual QIcon getWidgetCategoryIcon() const override
         * {
         *      return GetWidgetCategoryIcon();
         * }
         * \endcode
         */
        static QIcon GetWidgetCategoryIcon();

        /**
         * @brief Implement to load the settings that are part of the GUI configuration.
         * These settings are \b NOT loaded automatically on widget startup, but
         * when a GUI config is loaded (instead of the optional config dialog).
         */
        virtual void loadSettings(QSettings* settings) = 0;

        /**
         * @brief Implement to save the settings as part of the GUI configuration.
         * These settings are \b NOT saved automatically when closing the widget,
         * but when a GUI config is saved.
         */
        virtual void saveSettings(QSettings* settings) = 0;

        /**
         * @brief Reimplementing this function and returning a SoNode* will show this SoNode in
         * the 3DViewerWidget, so that you do not need to implement a viewer yourself.
         * @return
         */
        virtual SoNode* getScene()
        {
            return 0;
        }
        /**
         * Reimplementing this function and returning a QDialog* will enable a configuration button which opens the returned dialog.
         *
         * @param parent The parent widget which should be used as parent of the dialog
         * @return The dialog to open wehen the configuration button is clicked
         */
        virtual QPointer<QDialog> getSceneConfigDialog(QWidget* parent = nullptr);


        /**
         * @brief onClose is called before the DockWidget is closed.
         * @return returns close, if the widget should really be closed
         */
        virtual bool onClose()
        {
            return true;
        }

        /**
         * @brief postDocking is called after the widget has been docked into the main window.
         * This can be used to initialize OpenGL etc. to avoid strange effects like window plopping out.
         */
        virtual void postDocking()
        {

        }

        /**
         * @brief sets the name of this instance. Can only be set once.
         * Only the first call has an effect. Further calls are ignored.
         *
         * @return True on first call, else false.
         */
        bool setInstanceName(QString instanceName);
        QString getInstanceName();
        virtual void setMainWindow(QMainWindow* mainWindow);
        /**
         * @brief Returns the ArmarX MainWindow.
         */
        virtual QMainWindow* getMainWindow();

        /**
         * @brief Returns the default instance for the TipDialog used by all widgets (if not set otherwise).
         * This dialog has always the checkbox to not show a tip again, which is stored persistently.
         */
        QPointer<TipDialog> getTipDialog() const;
        void setTipDialog(QPointer<TipDialog> tipDialog);

        // This is all total madness. Why do we put mutexes and locks into shared pointers?
        using RecursiveMutex = std::recursive_mutex;
        using RecursiveMutexPtr = std::shared_ptr<RecursiveMutex>;
        using RecursiveMutexLock = std::unique_lock<RecursiveMutex>;
        using RecursiveMutexLockPtr = std::shared_ptr<RecursiveMutexLock>;
        /*!
         * \brief This mutex is used to protect 3d scene updates. Usually called by the ArmarXGui main window on creation of this controller.
         * \param mutex3D
         */
        virtual void setMutex3D(RecursiveMutexPtr const& mutex3D);

        template <typename Class>
        static ArmarXWidgetControllerPtr createInstance()
        {
            ArmarXWidgetControllerPtr ptr = new Class();
            return ptr;
        }

        static int showMessageBox(const QString& msg);

        /**
         * @brief This function enables/disables the main widget
         * asynchronously (if called from a non qt thread).
         * Call this function from onConnectComponent, onDisconnectComponent
         * if your widget should be disabled if the connection was lost
         * @param enable true to enable widget, false to disable widget
         * @see onConnectComponent(), onDisconnectComponent(), getWidget()
         */
        void enableMainWidgetAsync(bool enable);


        virtual void onLockWidget();
        virtual void onUnlockWidget();
    signals:
        void configAccepted(ArmarXWidgetControllerPtr widget);
        void configRejected(ArmarXWidgetControllerPtr widget);
    public slots:
        void configAccepted();
        void configRejected();
        void enableMainWidget(bool enable);

    private:
        QString __instanceName;
        QPointer<QWidget> __widget;
        QMainWindow* __appMainWindow;
        QPointer<TipDialog> tipDialog;
    protected:
        std::shared_ptr<std::recursive_mutex> mutex3D;
    };
}

namespace std
{

    ARMARXCORE_IMPORT_EXPORT ostream& operator<< (ostream& stream, const QString& string);

    ARMARXCORE_IMPORT_EXPORT ostream& operator<<(ostream& stream, const QPointF& point);

    ARMARXCORE_IMPORT_EXPORT ostream& operator<<(ostream& stream, const QRectF& rect);

    ARMARXCORE_IMPORT_EXPORT ostream& operator<<(ostream& stream, const QSizeF& rect);

}

