/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "ArmarXComponentWidgetController.h"
#include <ArmarXCore/core/ArmarXManager.h>

#include <IceUtil/UUID.h>

namespace armarx
{
    ArmarXComponentWidgetController::ArmarXComponentWidgetController() :
        ArmarXWidgetController(),
        uuid(IceUtil::generateUUID())
    {
        __setNoDelete(false);
    }

    std::string ArmarXComponentWidgetController::getDefaultName() const
    {
        return getWidgetName().toStdString() + "-" + uuid;
    }

    bool ArmarXComponentWidgetController::onClose()
    {
        QObject::disconnect();
        blockSignals(true);

        //    ARMARX_WARNING << "onClose of " << getWidgetName();
        if (getArmarXManager())
        {
            getArmarXManager()->removeObjectBlocking(this); //will be deleted after this from another thread
        }

        return true;
    }
}
