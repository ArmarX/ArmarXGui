/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGui::ArmarXGuiBase
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <string>
#include <sstream>
#include <vector>

#include <QString>

//#include <ArmarXCore/core/util/TemplateMetaProgramming.h>

namespace armarx
{
    inline QString ToQString(const char* str)
    {
        return {str};
    }

    inline QString ToQString(const std::string& s)
    {
        return QString::fromStdString(s);
    }

    inline QString ToQString(float v)
    {
        return QString::number(v);
    }

    inline QString ToQString(double v)
    {
        return QString::number(v);
    }

    inline QString ToQString(char v)
    {
        return QString::number(v);
    }

    inline QString ToQString(std::uint8_t v)
    {
        return QString::number(v);
    }

    inline QString ToQString(std::uint16_t v)
    {
        return QString::number(v);
    }

    inline QString ToQString(std::uint32_t v)
    {
        return QString::number(v);
    }

    inline QString ToQString(std::uint64_t v)
    {
        return QString::number(v);
    }

    inline QString ToQString(std::int8_t v)
    {
        return QString::number(v);
    }

    inline QString ToQString(std::int16_t v)
    {
        return QString::number(v);
    }

    inline QString ToQString(std::int32_t v)
    {
        return QString::number(v);
    }

    inline QString ToQString(std::int64_t v)
    {
        return QString::number(v);
    }

    inline QString ToQString(const std::stringstream& str)
    {
        return ToQString(str.str());
    }

    inline QString ToQString(bool b)
    {
        return b ? "true" : "false";
    }

    template<class T>
    inline QString ToQString(const std::vector<T>& v)
    {
        if (v.empty())
        {
            return "";
        }
        std::stringstream str;
        str << v.front();
        for (std::size_t i = 1; i < v.size(); ++i)
        {
            str << " " << v.at(i);
        }
        return ToQString(str);
    }

    template<class T>
    inline QString ToQString(const std::vector<std::vector<T>>& v)
    {
        if (v.empty())
        {
            return "";
        }
        std::stringstream str;
        str << ToQString(v.front()).toStdString();
        for (std::size_t i = 1; i < v.size(); ++i)
        {
            str << "\n" << ToQString(v.at(i)).toStdString();
        }
        return ToQString(str);
    }

    //    template<class T>
    //    inline QString ToQString(const T& t)
    //    //    inline typename std::enable_if<meta::HasToString<T>::value, QString>::type ToQString(const T& t)
    //    {
    //        return ToQString(to_string(t));
    //    }

    //    template<class T>
    //    inline QString ToQString(const T& t)
    //    {
    //        std::stringstream str;
    //        str << t;
    //        return ToQString(str);
    //    }
}
