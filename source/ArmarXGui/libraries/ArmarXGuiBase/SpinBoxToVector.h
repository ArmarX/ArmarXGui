/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::NJointControllerGuiPluginUtility
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <array>
#include <vector>

#include <Eigen/Dense>

#include <QDoubleSpinBox>

#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/Nodes/RobotNode.h>

#include <ArmarXCore/util/CPPUtility/trace.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

namespace armarx
{
    template<class Qwid = QDoubleSpinBox, std::size_t Size = 0>
    class SpinBoxToVector
    {
    public:
        template<class Scalar, int Rows, int Cols>
        void get(Eigen::Matrix<Scalar, Rows, Cols>& m) const
        {
            ARMARX_TRACE;
            if constexpr(Size)
            {
                ARMARX_CHECK_EQUAL(Size, _widgets.size());
            }
            if constexpr(Rows == -1 && Cols == -1)
            {
                m.resize(_widgets.size(), 1);
            }
            else if constexpr(Rows == 1 && Cols == -1)
            {
                m.resize(_widgets.size());
            }
            else if constexpr(Rows == -1 && Cols == 1)
            {
                m.resize(_widgets.size());
            }
            else if constexpr(Cols == -1)
            {
                m.resize(Rows, _widgets.size() / Rows);
            }
            else if constexpr(Rows == -1)
            {
                m.resize(_widgets.size() / Cols, Cols);
            }

            ARMARX_TRACE;
            ARMARX_CHECK_EQUAL(static_cast<std::size_t>(m.size()), _widgets.size());
            for (std::size_t i = 0; i < _widgets.size(); ++i)
            {
                m.data()[i] = _widgets.at(i)->value();
            }
        }
        template<class Scalar>
        void get(std::vector<Scalar>& m) const
        {
            ARMARX_TRACE;
            if constexpr(Size)
            {
                ARMARX_CHECK_EQUAL(Size, _widgets.size());
            }
            m.resize(_widgets.size());
            ARMARX_CHECK_EQUAL(m.size(), _widgets.size());
            for (std::size_t i = 0; i < _widgets.size(); ++i)
            {
                m.at(i) = _widgets.at(i)->value();
            }
        }
        template<class T>
        T get() const
        {
            ARMARX_TRACE;
            if constexpr(Size)
            {
                ARMARX_CHECK_EQUAL(Size, _widgets.size());
            }
            T m;
            get(m);
            return m;
        }
        template<class Scalar, int Rows, int Cols>
        void set(const Eigen::Matrix<Scalar, Rows, Cols>& m)
        {
            ARMARX_TRACE;
            if constexpr(Size)
            {
                ARMARX_CHECK_EQUAL(Size, _widgets.size());
            }
            ARMARX_CHECK_EQUAL(m.size(), _widgets.size());
            for (int i = 0; i < _widgets.size(); ++i)
            {
                _widgets.at(i)->setValue(m.data()[i]);
            }
        }
        void set(const VirtualRobot::RobotNodeSetPtr& set)
        {
            ARMARX_TRACE;
            ARMARX_CHECK_NOT_NULL(set);
            ARMARX_CHECK_EQUAL(set->getSize(), _widgets.size());
            for (std::size_t i = 0; i < _widgets.size(); ++i)
            {
                ARMARX_CHECK_NOT_NULL(set->getNode(i));
                _widgets.at(i)->setValue(set->getNode(i)->getJointValue());
            }
        }
        void set(double d)
        {
            for (auto w : _widgets)
            {
                w->setValue(d);
            }
        }
    public:
        void addWidget(Qwid* s)
        {
            ARMARX_TRACE;
            ARMARX_CHECK_NOT_NULL(s);
            if constexpr(Size)
            {
                ARMARX_CHECK_LESS(_sz, Size);
                _widgets.at(_sz) = s;
            }
            else
            {
                _widgets.emplace_back(s);
            }
            ++_sz;
        }
        void clear()
        {
            ARMARX_TRACE;
            _sz = 0;
            if constexpr(!Size)
            {
                _widgets.clear();
            }
        }
        void visitWidgets(const auto& f)
        {
            for (auto w : _widgets)
            {
                f(w);
            }
        }
        void setMin(auto min)
        {
            for (auto w : _widgets)
            {
                w->setMinimum(min);
            }
        }
        void setMax(auto max)
        {
            for (auto w : _widgets)
            {
                w->setMaximum(max);
            }
        }
        void setMinMax(auto min, auto max)
        {
            setMin(min);
            setMax(max);
        }
        auto& widgets()
        {
            return _widgets;
        }
    private:
        unsigned _sz = 0;
        std::conditional_t <
        Size,
        std::array<Qwid*, Size>,
        std::vector<Qwid*>
        > _widgets;
    };
}
