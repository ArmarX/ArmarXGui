/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
/**
 * Macro to check whether the current function is executed in the thread of the given Qt object.
 * This object should be the object that is manipulated/read by the called function.
 */
#define CHECK_QT_THREAD(qtObject) ARMARX_CHECK_EXPRESSION(qtObject!=nullptr); ARMARX_CHECK_EQUAL(qtObject->thread(), QThread::currentThread()) << \
        "The current function is not called in the correct Qt Thread of the object (most of the time the main thread)!"

#define CHECK_NOT_QT_THREAD(qtObject) ARMARX_CHECK_EXPRESSION(qtObject!=nullptr); ARMARX_CHECK_NOT_EQUAL(qtObject->thread(), QThread::currentThread()) << \
        "The current function is called in the Qt Thread of the object (most of the time the main thread)! This function was declared to be not in the GUI thread."
