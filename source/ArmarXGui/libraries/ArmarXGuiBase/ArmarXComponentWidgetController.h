/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include "ArmarXWidgetController.h"

#include <ArmarXCore/core/Component.h>

#include <QIcon>


namespace armarx
{
    /**
      \class ArmarXComponentWidgetController

      \ingroup ArmarXGuiBase
     */
    class ArmarXComponentWidgetController :
        public ArmarXWidgetController,
        virtual public Component
    {
        friend class GuiWindow;

    public:
        ArmarXComponentWidgetController();
        std::string getDefaultName() const override;

        template <typename Class>
        static ArmarXWidgetControllerPtr createInstance()
        {
            ArmarXWidgetControllerPtr ptr = ArmarXWidgetControllerPtr::dynamicCast(Component::create<Class>());
            return ptr;
        }

    protected:
        /*!
         * \brief If you overwrite this method, make sure to call this
         * implementation at the end of your implementation with ArmarXComponentWidgetController::onClose().
         */
        bool onClose() override;

    private:
        std::string uuid;
    };
    using ArmarXComponentWidgetControllerPtr = IceUtil::Handle<ArmarXComponentWidgetController>;

    template<class Derived>
    class ArmarXComponentWidgetControllerTemplate:
        public ArmarXComponentWidgetController
    {
    public:
        QString getWidgetName() const final override
        {
            return Derived::GetWidgetName();
        }
        QIcon getWidgetIcon() const final override
        {
            return Derived::GetWidgetIcon();
        }
        QIcon getWidgetCategoryIcon() const final override
        {
            return Derived::GetWidgetCategoryIcon();
        }
    };
}

