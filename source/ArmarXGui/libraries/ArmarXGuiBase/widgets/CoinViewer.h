/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Gui
* @author     Nikolaus Vahrenkamp
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once


#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>

#include <memory>
#include <mutex>

// project
namespace armarx
{

    /*!
     * Basically this is a SoQtExaminerViewer, with the extension that you can specify a mutex that protects the drawing.
     */
    class CoinViewer :
        public SoQtExaminerViewer
    {

    public:
        CoinViewer(QWidget* parent,
                   const char* name = NULL,
                   SbBool embed = TRUE,
                   SoQtFullViewer::BuildFlag flag = BUILD_ALL,
                   SoQtViewer::Type type = BROWSER);
        ~CoinViewer() override;

        // This is all total madness. Why do we put mutexes and locks into shared pointers?
        using RecursiveMutex = std::recursive_mutex;
        using RecursiveMutexPtr = std::shared_ptr<RecursiveMutex>;
        using RecursiveMutexLock = std::unique_lock<RecursiveMutex>;
        using RecursiveMutexLockPtr = std::shared_ptr<RecursiveMutexLock>;

        /*!
         * If set, the drawing is protected by this mutex. This overwrites the default mutex.
         */
        void setMutex(RecursiveMutexPtr const& m);


        /*!
         * \return This lock allows to safely access the viewer's scene graph.
         */
        RecursiveMutexLockPtr getScopedLock();

    protected:

        /*!
        * \brief actualRedraw Reimplement the redraw method in order to lock engine mutex
        */
        void actualRedraw(void) override;

        RecursiveMutexPtr mutex;
    };

    using CoinViewerPtr = std::shared_ptr<CoinViewer>;
}

