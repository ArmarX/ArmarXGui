/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "InfixFilterModel.h"
#include <ArmarXCore/core/logging/Logging.h>
#include <QTreeView>

namespace armarx
{


    InfixFilterModel::InfixFilterModel(QObject* parent) : QSortFilterProxyModel(parent)
    {
        addCustomFilter([this](QAbstractItemModel * model, int source_row, const QModelIndex & source_parent)
        {
            QModelIndex index0 = model->index(source_row, 0, source_parent);

            QStringList searchParts = this->filterRegExp().pattern().split(" ", QString::SkipEmptyParts);

            auto result = containsAll(index0.data().toString(), searchParts);

            return result;
        });
    }

    bool InfixFilterModel::containsAll(const QString& text, const QStringList& searchParts)
    {
        for (const QString& s : searchParts)
        {
            if (!text.contains(s, Qt::CaseInsensitive))
            {
                return false;
            }
        }
        return true;
    }

    void InfixFilterModel::addCustomFilter(InfixFilterModel::FilterFunc function)
    {
        customFilters.push_back(function);
    }

    void InfixFilterModel::ExpandFilterResults(QTreeView* treeView)
    {
        if (!treeView)
        {
            return;
        }
        treeView->collapseAll();
        InfixFilterModel* proxyModel = qobject_cast<InfixFilterModel*>(treeView->model());
        if (!proxyModel)
        {
            ARMARX_WARNING_S << "Could not cast treeview model to proxymodel!";
            return;
        }
        QList<QModelIndex> indexList;
        for (int i = 0; i <  proxyModel->rowCount(); ++i)
        {
            indexList << proxyModel->index(i, 0);
        }
        //        QStringList searchParts = proxyModel->filterRegExp().pattern().split(" ", QString::SkipEmptyParts);

        while (indexList.size() > 0)
        {
            QModelIndex& index = indexList.front();
            //            ARMARX_INFO << "Checking of " << index.data().toString().toStdString() << VAROUT(index.row()) << " parent: " << index.parent().data().toString().toStdString();
            auto sourceIndex = proxyModel->mapToSource(index);
            if (proxyModel->filterAcceptsRow(sourceIndex.row(), sourceIndex.parent(), false))
            {
                //                ARMARX_INFO << "Expanding parents of " << index.data().toString().toStdString();
                QModelIndex current = index.parent();
                while (current.isValid())
                {
                    treeView->expand(current);
                    current = current.parent();
                }
            }
            int i = 0;

            while (index.child(i, 0).isValid())
            {
                indexList << index.child(i, 0);
                i++;
            }
            indexList.pop_front();
        }
    }

    bool InfixFilterModel::filterAcceptsRow(int source_row, const QModelIndex& source_parent) const
    {
        return filterAcceptsRow(source_row, source_parent, true);
    }

    bool InfixFilterModel::filterAcceptsRow(int source_row, const QModelIndex& source_parent, bool considerParents) const
    {
        QModelIndex index0 = sourceModel()->index(source_row, 0, source_parent);
        QList<QModelIndex> indexList;
        indexList << index0;
        /*auto checkIndex = [this](QModelIndex index)
        {
            ARMARX_IMPORTANT << filterRegExp().pattern().toStdString();
            QString text = sourceModel()->data(index).toString();
            for (const QString& s : filterRegExp().pattern().split(" ", QString::SkipEmptyParts))
            {
                ARMARX_IMPORTANT << s.toStdString();
                if (!text.contains(s, Qt::CaseInsensitive))
                {
                    return false;
                }
            }
            return true;
            //return sourceModel()->data(index).toString().contains(filterRegExp());
        };*/

        auto checkRow = [this](int source_row, const QModelIndex & source_parent)
        {
            //            QModelIndex index0 = sourceModel()->index(source_row, 0, source_parent);
            bool accepted = true;
            for (auto& f : this->customFilters)
            {
                accepted &= f(this->sourceModel(), source_row, source_parent);
                if (!accepted)
                {
                    break;
                }
            }
            return accepted;
        };

        //        QStringList searchParts = filterRegExp().pattern().split(" ", QString::SkipEmptyParts);
        while (indexList.size() > 0)
        {
            QModelIndex& index = indexList.front();
            //            ARMARX_INFO << "Current: " << index.data().toString().toStdString();
            bool accepted = checkRow(index.row(), index.parent());
            //            accepted = containsAll(sourceModel()->data(index).toString(), searchParts);


            if (accepted)
            {
                //                ARMARX_INFO << "Accepting " << index0.data().toString().toStdString();
                return true;
            }
            int i = 0;

            while (index.child(i, 0).isValid())
            {
                indexList << index.child(i, 0);
                i++;
            }
            indexList.pop_front();
        }
        if (considerParents)
        {
            QModelIndex currentParent = source_parent;
            //            ARMARX_INFO << "Checking parent " << currentParent.data().toString().toStdString();
            while (currentParent.isValid())
            {

                //            if (sourceModel()->data(parent).toString().contains(filterRegExp()))
                if (checkRow(currentParent.row(), currentParent.parent()))
                {
                    return true;
                }
                currentParent = currentParent.parent();

            }
        }
        return false;
    }


} // namespace armarx
