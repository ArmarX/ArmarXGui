/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGui::EnhancedGraphicsView
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include <QGraphicsItem>

#include "EnhancedGraphicsView.h"

namespace armarx
{

    Qt::KeyboardModifier EnhancedGraphicsView::getRotationKeyboardModifier() const
    {
        return rotationModifier;
    }

    qreal EnhancedGraphicsView::getRotationFactor() const
    {
        return rotationFactor;
    }

    Qt::KeyboardModifier EnhancedGraphicsView::getZoomKeyboardModifier() const
    {
        return zoomModifier;
    }

    qreal EnhancedGraphicsView::getZoomFactor() const
    {
        return zoomFacor;
    }

    Qt::KeyboardModifier EnhancedGraphicsView::getDraggingKeyboardModifier() const
    {
        return draggingModifier;
    }

    Qt::MouseButton EnhancedGraphicsView::getDraggingMouseButton() const
    {
        return draggingButton;
    }

    void EnhancedGraphicsView::setRotationEnabled(bool enabled)
    {
        rotationEnabled = enabled;
    }

    void EnhancedGraphicsView::setRotationDisabled(bool disabled)
    {
        rotationEnabled = !disabled;
    }

    void EnhancedGraphicsView::setRotationKeyboardModifier(Qt::KeyboardModifier mod)
    {
        rotationModifier = mod;
    }

    void EnhancedGraphicsView::setRotationFactor(qreal factor)
    {
        rotationFactor = factor;
    }

    void EnhancedGraphicsView::setZoomEnabled(bool enabled)
    {
        zoomEnabled = enabled;
    }

    void EnhancedGraphicsView::setZoomDisabled(bool disabled)
    {
        zoomEnabled = !disabled;
    }

    void EnhancedGraphicsView::setZoomKeyboardModifier(Qt::KeyboardModifier mod)
    {
        zoomModifier = mod;
    }

    void EnhancedGraphicsView::setZoomFactor(qreal factor)
    {
        zoomFacor = factor;
    }

    void EnhancedGraphicsView::setDraggingEnabled(bool enabled)
    {
        draggingEnabled = enabled;
    }

    void EnhancedGraphicsView::setDraggingDisabled(bool disabled)
    {
        draggingEnabled = !disabled;
    }

    void EnhancedGraphicsView::setDraggingKeyboardModifier(Qt::KeyboardModifier mod)
    {
        draggingModifier = mod;
    }

    void EnhancedGraphicsView::setDraggingMouseButton(Qt::MouseButton button)
    {
        draggingButton = button;
    }

    void EnhancedGraphicsView::setAllVisible(bool visible)
    {
        for (QGraphicsItem* i : items())
        {
            i->setVisible(visible);
        }
    }

    void EnhancedGraphicsView::mousePressEvent(QMouseEvent* e)
    {
        draggingStartPosition = mapToScene(e->pos());
        QGraphicsView::mousePressEvent(e);
    }

    void EnhancedGraphicsView::mouseMoveEvent(QMouseEvent* e)
    {
        if (
            (e->modifiers() & draggingModifier).testFlag(draggingModifier) &&
            (e->buttons() & draggingButton).testFlag(draggingButton)
        )
        {
            const auto oldAnchor = transformationAnchor();
            setTransformationAnchor(QGraphicsView::NoAnchor);

            const QPointF vector = mapToScene(e->pos()) - draggingStartPosition;
            translate(vector.x(), vector.y());
            draggingStartPosition = mapToScene(e->pos());
            e->accept();

            setTransformationAnchor(oldAnchor);
            return;
        }
        QGraphicsView::mouseMoveEvent(e);
    }

    void EnhancedGraphicsView::wheelEvent(QWheelEvent* e)
    {
        bool used = false;
        if ((e->modifiers() & zoomModifier).testFlag(zoomModifier))
        {
            const auto oldAnchor = transformationAnchor();
            setTransformationAnchor(QGraphicsView::NoAnchor);

            float factor = std::pow(zoomFacor, e->delta() < 0 ? -1 : +1);
            scale(factor, factor);
            used = true;

            setTransformationAnchor(oldAnchor);
        }
        if ((e->modifiers() & rotationModifier).testFlag(rotationModifier))
        {
            const auto oldAnchor = transformationAnchor();
            setTransformationAnchor(QGraphicsView::NoAnchor);

            rotate(e->delta() * rotationFactor);
            used = true;

            setTransformationAnchor(oldAnchor);
        }
        if (used)
        {
            e->accept();
        }
        else
        {
            QGraphicsView::wheelEvent(e);
        }
    }

}
