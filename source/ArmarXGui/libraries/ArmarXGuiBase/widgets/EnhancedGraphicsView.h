/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGui::EnhancedGraphicsView
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <QGraphicsView>
#include <QPointF>
#include <QMouseEvent>
#include <QWheelEvent>
#include <cmath>
namespace armarx
{
    /**
     * @brief The EnhancedGraphicsView is a QGraphicsView with some additional
     * functions.
     *
     * Additional functions are:
     * - Zooming (default ctrl+scroll)
     * - Roatating (default shift+scroll)
     * - Dragging the scene (default ctrl+left click+dragging)
     */
    class EnhancedGraphicsView: public QGraphicsView
    {
        Q_OBJECT
    public:
        using QGraphicsView::QGraphicsView;

        Qt::KeyboardModifier getRotationKeyboardModifier() const;
        qreal getRotationFactor() const;

        Qt::KeyboardModifier getZoomKeyboardModifier() const;
        qreal setZoomFactor();
        qreal getZoomFactor() const;

        Qt::KeyboardModifier getDraggingKeyboardModifier() const;
        Qt::MouseButton getDraggingMouseButton() const;
    public slots:
        void setRotationEnabled(bool enabled = true);
        void setRotationDisabled(bool disabled = true);
        void setRotationKeyboardModifier(Qt::KeyboardModifier mod);
        void setRotationFactor(qreal factor);

        void setZoomEnabled(bool enabled = true);
        void setZoomDisabled(bool disabled = true);
        void setZoomKeyboardModifier(Qt::KeyboardModifier mod);
        void setZoomFactor(qreal factor);

        void setDraggingEnabled(bool enabled = true);
        void setDraggingDisabled(bool disabled = true);
        void setDraggingKeyboardModifier(Qt::KeyboardModifier mod);
        void setDraggingMouseButton(Qt::MouseButton button);

        void setAllVisible(bool visible);

    protected:
        void mousePressEvent(QMouseEvent* e) override;
        void mouseMoveEvent(QMouseEvent* e) override;
        void wheelEvent(QWheelEvent* e) override;

        bool rotationEnabled {true};
        Qt::KeyboardModifier rotationModifier {Qt::ShiftModifier};
        qreal rotationFactor {0.01};

        bool zoomEnabled {true};
        Qt::KeyboardModifier zoomModifier {Qt::ControlModifier};
        qreal zoomFacor {1.05};

        bool draggingEnabled {true};
        Qt::KeyboardModifier draggingModifier {Qt::ControlModifier};
        Qt::MouseButton draggingButton {Qt::LeftButton};
        QPointF draggingStartPosition;
    };
}
