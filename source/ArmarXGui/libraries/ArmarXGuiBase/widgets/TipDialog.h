/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <QDialog>

namespace armarx::Ui
{
    class TipDialog;
}

namespace armarx
{
    /**
     * @brief The TipDialog is a dialog to show tips/hints to the user, which are optionally only shown once.
     * This dialog contains a textfield in which the developer can show a tip to the user.
     * It also contains a checkbox "Do not show again", which upon selection puts the shown string into a blacklist
     * of messages, which are not shown again.
     *
     * To show the dialog use the function showMessage().
     * The dialog should only be created once and passed around or the blacklist must be passed on each creation to the dialog via
     * setBlackListedStrings().
     *
     *
     * @see \ref armarx::ArmarXWidgetController::getTipDialog, armarx::ArmarXMainWindow
     */
    class TipDialog : public QDialog
    {
        Q_OBJECT

    public:
        explicit TipDialog(QWidget* parent = 0);
        ~TipDialog() override;

        /**
         * @brief Shows this dialog and shows the message, if it was not blacklisted.
         * If the message was blacklisted, nothing happens.
         * @param tipText Text that should be shown in the dialog. If stringIdentifier is empty, this string is used for blacklisting the
         * message.
         * @param windowTitle WindowTitle of the Dialog
         * @param stringIdentifier If not empty, this string is used for the blacklisting instead of the tiptext.
         */
        void showMessage(const QString& tipText, const QString& windowTitle, const QString& stringIdentifier = "");

        /**
         * @brief Returns all currently blacklisted strings.
         *
         */
        QStringList getBlackListedStrings() const;

        /**
         * @brief Sets list of blacklisted strings. Pass empty list to clear.
         * All previously set strings are forgotten.
         */
        void setBlackListedStrings(const QStringList& strings);

        /**
         * @brief Checks whether a specific string is blacklisted.
         * @param stringId String to check
         * @return True if blacklisted.
         */
        bool isBlackListed(const QString& stringId) const;
        // QWidget interface
    protected:
        void hideEvent(QHideEvent*) override;
    private:
        Ui::TipDialog* ui;
        QStringList blackList;
        QString lastId;

    };


}
