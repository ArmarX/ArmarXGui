/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * @package    ArmarX::RobotAPI
 * @author     Raphael Grimm <raphael dot grimm at kit dot edu>
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <memory>

//qt
#include <QWidget>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QMouseEvent>
#include <QResizeEvent>

#include <QPointF>
#include <QPointer>
#include <QGraphicsEllipseItem>

namespace armarx
{
    /**
     * @brief Provides the coordinates of mouse events through signals.
     * Used in JoystickControlWidget.
     */
    class JoystickControlWidgetQGraphicsView: public QGraphicsView
    {
        Q_OBJECT
    public:
        JoystickControlWidgetQGraphicsView(QGraphicsScene* scene, QWidget* parent = nullptr);
        ~JoystickControlWidgetQGraphicsView() override;

    public:
        /**
         * @brief Passes the mapped mouse coordinates of the event through the signal positionChanged.
         * @param event Contains the coordinates
         */
        void mousePressEvent(QMouseEvent* event) override;

        /**
         * @brief Passes the mapped mouse coordinates of the event through the signal positionChanged.
         * @param event Contains the coordinates
         */
        void mouseMoveEvent(QMouseEvent* event) override;

        /**
         * @brief Passes (0;0) through the signal positionChanged.
         */
        void mouseReleaseEvent(QMouseEvent*) override;
    signals:
        /**
         * @brief Sends the mouse position on press or move in the scene coordinates.
         * On mouse release (0;0) is send.
         */
        void positionChanged(QPointF);

        /**
         * @brief Emitted when the mouse was released.
         */
        void released();

        /**
         * @brief Emitted when the mouse was pressed.
         */
        void pressed();
    };//JoystickControlWidgetQGraphicsView

    /**
     * @brief Provides a simple joystick control.
     *
     * The widget emits signals when the control is moved.
     *
     * The signal
     * @code{.cpp}
     * positionChanged(QPointF)
     * @endcode
     * passes the nibble's current position.
     * The position is in the unit circle.
     * The x-axis is horizontal and increases to the right.
     * The y-axis is vertical and increases upwards.
     *
     * The signal
     * @code{.cpp}
     * rotationChanged(double)
     * @endcode
     * passes the position vectors rotation in polar coordinates (-pi,pi].
     * The up position is 0.
     * The down position is pi.
     * The Quadrants 1 and 4 have positive value.
     *
     * If the constructor is called with useQuadrant3and4==true the control is the whole unit circle.
     * If the constructor is called with useQuadrant3and4==false the control is the unit circle's upper semicircle. (y>=0)
     *
     * Possible positions can be influenced with
     * \code{.cpp}
     * setSteps(int steps);
     * \endcode
     *      - steps>0 : only position vectors with a length from {n/steps | n in {0,1,...,steps} are valid. The nibble snaps to a valid position.
     *      - steps<=0: all positions in the unit circle are valid.
     *
     * @image html JoystickControlWidget_widget.png "Left: A widget with 2 steps using the whole unit circle. Right: A widget using the unit circle's upper semi circle." width=300px
     */
    class JoystickControlWidget : public QWidget
    {
        Q_OBJECT
    public:
        explicit JoystickControlWidget(bool useQuadrant3and4 = true, QWidget* parent = 0);

        /**f the control in polar coordinates (-pi,pi].
             * The top position is 0. The bottom pos
             * @brief Returns the angle oition is pi. The Quadrants 1 and 4 have positive value.
             * @return The angle of the control in polar coordinates (-pi,pi].
             */
        double getRotation() const;

        /**
         * @brief Returns the position of the nibble.
         * The position is in a circle with the radius 1 around (0;0)
         * @return The position of the nibble.
         */
        QPointF getPosition() const;

    public slots:
        /**
         * @brief Sets the steps of the control. (0=> unlimited steps)
         * @param stepCount The new step count. (values <0 will be used as 0)
         */
        void setSteps(int stepCount);

        /**
         * @brief Returns the steps of the control. (0=> unlimited steps)
         * @return The steps of the control. (0=> unlimited steps)
         */
        int getSteps();

        /**
         * @brief Sets the nibble to pos. (pos will be transformed to be a valid position)
         * @param pos The new position.
         */
        void setNibble(QPointF pos);


    signals:
        /**
         * @brief Passes the position of the control.
         * The position is in a circle with the radius 1 around (0;0)
         */
        void positionChanged(QPointF);

        /**
         * @brief Passes the angle of the control in polar coordinates (-pi,pi].
         * The top position is 0. The bottom position is pi. The Quadrants 1 and 4 have positive value.
         */
        void rotationChanged(double);

        /**
         * @brief Passes the position and angle of the control in polar coordinates (-pi,pi].
         */
        void changed(QPointF, double);

        /**
         * @brief Emitted when the nibble was released.
         */
        void released();

        /**
         * @brief Emitted when the nibble was pressed.
         */
        void pressed();

    protected:
        void resizeEvent(QResizeEvent* event) override;

    private slots:

        /**
         * @brief Called when the nibble is released.
         * Emits released() and sets the Position to 0.
         */
        void mouseReleased();

    private:
        /**
         * @brief The view containing the scene.
         */
        QPointer<JoystickControlWidgetQGraphicsView> view;
        /**
         * @brief Whether only quadrant 1 and 2 are in use
         */
        bool onlyQuadrant2and1;

        //QGraphicsEllipseItem is no QObject so it cant be stored in a QPointer.
        /**
         * @brief The control item of this widget. (the red dot)
         */
        QGraphicsEllipseItem* nibble;

        /**
         * @brief The steps of the control. (0=> unlimited steps)
         */
        int steps;
    };// class JoystickControlWidget

    using JoystickControlWidgetPtr = std::shared_ptr<JoystickControlWidget>;

}// namespace armarx
