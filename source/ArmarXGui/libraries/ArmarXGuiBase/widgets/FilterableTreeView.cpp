/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "FilterableTreeView.h"
#include "InfixFilterModel.h"

#include <QLineEdit>
#include <QMouseEvent>
#include <QStandardItem>

#include <ArmarXCore/core/exceptions/Exception.h>

#define DEFAULT_FILTER_CONTENT  "Click&type to filter"
const QString FilterableTreeView::DefaultFilterStr = DEFAULT_FILTER_CONTENT;
#define FILTER_HEIGHT 22
FilterableTreeView::FilterableTreeView(QWidget* parent, bool hideChildren) :
    QTreeView(parent),
    hideChildren(hideChildren)
{
    filterLineEdit = new QLineEdit(this);
    filterLineEdit->setPlaceholderText(DefaultFilterStr);
    filterLineEdit->resize(this->width(), FILTER_HEIGHT);
    filterExpansionTimer.setSingleShot(true);
    connect(&filterExpansionTimer, SIGNAL(timeout()), this, SLOT(delayedFilterExpansion()));
    setStyleSheet("QTreeView{margin-top: " + QString::number(filterLineEdit->height()) + "px}");

}

void FilterableTreeView::resizeEvent(QResizeEvent* event)
{
    filterLineEdit->resize(event->size().width(), FILTER_HEIGHT);
    QTreeView::resizeEvent(event);
}

armarx::InfixFilterModel* FilterableTreeView::getProxyModel() const
{
    return proxyModel;
}

void FilterableTreeView::setModel(QAbstractItemModel* model)
{
    proxyModel = new armarx::InfixFilterModel(this);
    proxyModel->setSourceModel(model);
    QTreeView::setModel(proxyModel);
    //    connect(filterLineEdit, SIGNAL(textChanged(QString)), this, SLOT(applyFilter(QString)));
    connect(filterLineEdit, SIGNAL(textChanged(QString)), proxyModel, SLOT(setFilterFixedString(QString)));
    connect(filterLineEdit, SIGNAL(textChanged(QString)), this, SLOT(expandFilterSelection(QString)), Qt::QueuedConnection);


}

void FilterableTreeView::expandFilterSelection(QString filterStr)
{
    //ARMARX_INFO_S << VAROUT(filterStr);
    if (filterStr.length() == 0)
    {
        collapseAll();
        //        ui.monitoredManagersTree->expandToDepth(1);
    }
    else
    {
        filterExpansionTimer.start(500);
    }
}

void FilterableTreeView::delayedFilterExpansion()
{
    armarx::InfixFilterModel::ExpandFilterResults(this);
}
