/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * @package    ArmarX::RobotAPI
 * @author     Raphael Grimm <raphael dot grimm at kit dot edu>
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <QTreeWidget>

namespace armarx
{
    /**
     * @brief The EnhancedTreeWidget is a QTreeWidget with some extra functionalities.
     *
     * This TreeWidget:
     * - has working pixel wise scrolling
     * - can expand its size to the size of its content (currently visible widgets)
     * - can be set to auto expadn its size to its content
     */
    class EnhancedTreeWidget : public QTreeWidget
    {
        Q_OBJECT
    public:
        using QTreeWidget::QTreeWidget;
        int getHeight();
    protected:
        void wheelEvent(QWheelEvent* event) override;
    public slots:
        void expand();
        void setAutoExpand(bool active);
        void setWheelTicksPerScrollTick(int wheelTicksPerScrollTick);
    private:
        int calcHeight(QTreeWidgetItem* it);

        int vdelta = 0;
        int hdelta = 0;
        int wheelTicksPerScrollTick = 8;
    };
}
