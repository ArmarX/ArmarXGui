/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <IceUtil/Handle.h>
#include <Ice/Proxy.h>

#include <QWidget>

#include <memory>


namespace Ui
{
    class IceProxyFinder;
}

namespace armarx
{
    class IceManager;
    using IceManagerPtr = IceUtil::Handle<IceManager>;

    /**
     * \class IceProxyFinderBase
     * \brief The IceProxyFinderBase class provides a convenient way to query online proxies in the
     * ice network, i.e. registered with IceGrid.
     *
     * This is just the non-template base class. For the real implementation refer to
     * \ref armarx::IceProxyFinder and \ref armarx::IceTopicFinder.
     *
     * The selected proxy name can be queried with getSelectedProxyName().
     */
    class IceProxyFinderBase :
        public QWidget
    {
        Q_OBJECT

    public:
        explicit IceProxyFinderBase(QWidget* parent = 0);
        ~IceProxyFinderBase() override;

        virtual QStringList getProxyNameList(QString searchMask) const = 0;

        void setIceManager(IceManagerPtr icemanager, bool fetchProxies = true);
        QString getSelectedProxyName() const;
        void setDefaultSelectedProxy(const QString& proxyName);
        void setDefaultSelectedProxyAsItem(const QString& proxyName);
        void setSearchMask(const QString& searchMask);
        void showSearchMaskField(bool show);
        void showLabels(bool show);
        Ui::IceProxyFinder* getUi();

        QWidget* getProxyNameComboBox();

        virtual std::string getTypedProxyName(Ice::ObjectPrx const& current) const
        {
            return "";
        }

        QStringList getProxyNameListBase(QString searchMask) const;

    signals:
        void updateProxyListComboBoxSignal(const QStringList& proxyList);
        void triggerProxyListComboBoxUpdateSignal();
        void validProxySelected(const QString& proxyName);

    public slots:
        void triggerProxyListUpdate();
        void comboBoxTextChanged(QString currentText);
        void updateProxyListComboBox(const QStringList& proxyList);
        void evaluateValidProxySelectedSignal(const QString& proxyName);

    protected:
        void updateProxyList();
        void setupUi();

        Ui::IceProxyFinder* ui;
        struct Impl;
        std::unique_ptr<Impl> impl;
    };

    /**
     * @class IceProxyFinder
     * Widget to conveniently retrieve a proxy instance name of a specific interface type (the template parameter).
     * The search mask supports wild cards (only one, \*Unit\* is not possible) and seperators (e.g. Unit|Result).
     */
    template <typename ProxyType>
    class IceProxyFinder :
        public IceProxyFinderBase
    {
    public:
        IceProxyFinder(QWidget* parent = 0) :
            IceProxyFinderBase(parent)
        {}

        std::string getTypedProxyName(Ice::ObjectPrx const& current) const override
        {
            ProxyType object = ProxyType::checkedCast(current->ice_timeout(60));
            if (object)
            {
                return current->ice_getIdentity().name;
            }
            return "";
        }

        QStringList getProxyNameList(QString searchMask) const override
        {
            return getProxyNameListBase(searchMask);
        }
    };

    /**
     * @class IceTopicFinder
     * @brief The IceTopicFinder class queries and show all available topic registered with IceStorm.
     */
    class IceTopicFinder :
        public IceProxyFinderBase
    {
    public:
        IceTopicFinder(QWidget* parent = 0);

        QStringList getProxyNameList(QString searchMask) const override;
    };

}

