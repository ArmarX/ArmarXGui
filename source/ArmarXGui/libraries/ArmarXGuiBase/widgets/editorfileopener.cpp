/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "editorfileopener.h"


#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>

#include <SimoxUtility/algorithm/string/string_tools.h>

#include <boost/property_tree/xml_parser.hpp>

#define EDITORFILEOPENER_CONFIGFILE "ArmarXGui/EditorFileOpenerConfig.xml"
#define FILE_VARIABLE "$file"
#define LINE_VARIABLE "$line"
#define DEFAULT_EDITOR "qtcreator"

namespace armarx
{
    EditorFileOpener::EditorFileOpener(std::string configFile)
    {
        setTag("EditorFileOpener");
        using namespace boost::property_tree;


        static CMakePackageFinder cmake("ArmarXGui");

        if (configFile.empty())
        {
            configFile = cmake.getDataDir()  + "/" + std::string(EDITORFILEOPENER_CONFIGFILE);
        }

        if (!ArmarXDataPath::getAbsolutePath(configFile, configFile))
        {
            ARMARX_WARNING << "Cannot find file '" << configFile << "'";
            return;
        }

        try
        {
            xml_parser::read_xml(configFile, pt);
        }
        catch (xml_parser::xml_parser_error& e)
        {

            ARMARX_WARNING << "Could not load state-configfile '"
                           << configFile << "'. Required parameters may be unset.\nReason at "
                           << e.filename() << ":" << e.line() << ":\n" << e.message() << flush;
            return;
        }

        for (ptree::value_type const& v : pt.get_child("editors"))
        {
            if (v.first == "editor")
            {
                std::string editorName = v.second.get<std::string>("name");
                std::string openCommandLine = v.second.get<std::string>("opencommandline");
                editors[editorName] = openCommandLine;
            }
        }
    }

    void EditorFileOpener::openFile(const std::string& editorName, const std::string& filepath, int lineNumber)
    {
        std::string openCommandLine  = editors[editorName];

        if (openCommandLine.empty())
        {
            ARMARX_WARNING << "Cannot find editor named '" << editorName << "'";
            return;
        }

        std::string::size_type lenFile = std::string(FILE_VARIABLE).size();
        std::string::size_type posFile = openCommandLine.find(FILE_VARIABLE);

        if (posFile != std::string::npos)
        {
            openCommandLine.replace(posFile, lenFile, filepath);
        }

        std::string::size_type lenLine = std::string(LINE_VARIABLE).size();
        std::string::size_type posLine = openCommandLine.find(LINE_VARIABLE);
        std::stringstream lineStr;
        lineStr << lineNumber;

        if (posLine != std::string::npos)
        {
            openCommandLine.replace(posLine, lenLine, lineStr.str());
        }

        simox::alg::trim(openCommandLine);

        if (*openCommandLine.rbegin() != '&')
        {
            openCommandLine += " &";
        }

        ARMARX_VERBOSE << "OpenCommand: " << openCommandLine;

        if (std::system(openCommandLine.c_str())) {}
    }

    void EditorFileOpener::openFileWithDefaultEditor(const std::string& filepath, int lineNumber)
    {
        const char* envEditor = std::getenv("ARMARX_EDITOR");

        const std::string editorName = [&]() -> std::string
        {
            if (envEditor == nullptr)
            {
                return DEFAULT_EDITOR;
            }

            const std::string editor = std::string(envEditor);
            if (editors.count(editor) > 0)
            {
                return editor;
            }

            ARMARX_WARNING << "The editor '" << editor << "' is not registered. "
                           << "Check the config '" << EDITORFILEOPENER_CONFIGFILE << "'";

            return DEFAULT_EDITOR;
        }();

        openFile(editorName, filepath, lineNumber);
    }

}  // namespace armarx
