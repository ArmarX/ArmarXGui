/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <QObject>
#include <QSortFilterProxyModel>
#include <functional>

class QTreeView;

namespace armarx
{

    /**
     * @brief This proxy model reimplements the filterAcceptsRow function with a new behavior:
     * All elements that fit the filter string are shown alongside with all their parents as well as all their children.
     * This means the full sub-tree which contains matching elements is shown.
     *
     */
    class InfixFilterModel :
        public QSortFilterProxyModel
    {
        Q_OBJECT
    public:
        using FilterFunc = std::function<bool(QAbstractItemModel*, int, const QModelIndex&)>;
        InfixFilterModel(QObject* parent = 0);
        // QSortFilterProxyModel interface
        /**
         * @brief Expands the treeview that all items that match the filterstring are expanded and directly visible.
         * Anything else is collapsed.
         * @param treeView
         */
        static void ExpandFilterResults(QTreeView* treeView);
        static bool containsAll(const QString& text, const QStringList& searchParts);
        void addCustomFilter(FilterFunc function);
    protected:
        bool filterAcceptsRow(int source_row, const QModelIndex& source_parent) const override;
        bool filterAcceptsRow(int source_row, const QModelIndex& source_parent, bool considerParents) const;
        std::vector<FilterFunc> customFilters;
    };

}

