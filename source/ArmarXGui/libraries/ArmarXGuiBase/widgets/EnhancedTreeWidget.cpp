/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * @package    ArmarX::RobotAPI
 * @author     Raphael Grimm <raphael dot grimm at kit dot edu>
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "EnhancedTreeWidget.h"

#include <QHeaderView>
#include <QWheelEvent>
#include <QScrollBar>

namespace armarx
{

    int EnhancedTreeWidget::getHeight()
    {
        int sz = 0;
        for (int i = 0; i < topLevelItemCount(); ++i)
        {
            sz += calcHeight(topLevelItem(i));
        }
        if (!isHeaderHidden())
        {
            sz += header()->height();
        }
        return sz + 2;
    }

    void EnhancedTreeWidget::setWheelTicksPerScrollTick(int wheelTicksPerScrollTick)
    {
        this->wheelTicksPerScrollTick = wheelTicksPerScrollTick;
    }

    void EnhancedTreeWidget::wheelEvent(QWheelEvent* event)
    {
        if (event->orientation() == Qt::Horizontal)
        {
            if (horizontalScrollMode() == QAbstractItemView::ScrollPerPixel)
            {
                hdelta += event->delta();
                horizontalScrollBar()->setValue(horizontalScrollBar()->value() - (hdelta / wheelTicksPerScrollTick));
                hdelta = hdelta % wheelTicksPerScrollTick;
            }
            else
            {
                QTreeWidget::wheelEvent(event);
            }
        }
        else
        {
            if (verticalScrollMode() == QAbstractItemView::ScrollPerPixel)
            {
                vdelta += event->delta();
                verticalScrollBar()->setValue(verticalScrollBar()->value() - (vdelta / wheelTicksPerScrollTick));
                vdelta = hdelta % wheelTicksPerScrollTick;
            }
            else
            {
                QTreeWidget::wheelEvent(event);
            }
        }
        event->accept();
    }

    void EnhancedTreeWidget::expand()
    {
        setSizePolicy(sizePolicy().horizontalPolicy(), QSizePolicy::Fixed);
        setFixedHeight(getHeight());
    }

    void EnhancedTreeWidget::setAutoExpand(bool active)
    {
        if (active)
        {
            expand();
            connect(this, SIGNAL(collapsed(QModelIndex)), this, SLOT(expand()));
            connect(this, SIGNAL(expanded(QModelIndex)), this, SLOT(expand()));
        }
        else
        {
            disconnect(this, SIGNAL(collapsed(QModelIndex)), this, SLOT(expand()));
            disconnect(this, SIGNAL(expanded(QModelIndex)), this, SLOT(expand()));
        }
    }

    int EnhancedTreeWidget::calcHeight(QTreeWidgetItem* it)
    {
        int sz = 0;
        if (it->isHidden())
        {
            return sz;
        }
        for (int i = 0; i < it->columnCount(); ++i)
        {
            sz = std::max(sz, rowHeight(indexFromItem(it, i)));
        }
        if (it->isExpanded())
        {
            for (int i = 0; i < it->childCount(); ++i)
            {
                sz += calcHeight(it->child(i));
            }
        }
        return sz;
    }

}
