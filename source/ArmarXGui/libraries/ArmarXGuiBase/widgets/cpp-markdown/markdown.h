
/*
    Copyright (c) 2009 by Chad Nelson
    Released under the MIT License.
    See the provided LICENSE.TXT file for details.
*/

#pragma once

#include <iostream>
#include <string>
#include <list>
#include <memory>

namespace markdown
{
    // Forward references.
    class Token;
    class LinkIds;

    using TokenPtr = std::shared_ptr<Token>;
    using TokenGroup = std::list<TokenPtr>;

    class Document
    {
    public:
        Document(size_t spacesPerTab = cDefaultSpacesPerTab);
        Document(std::istream& in, size_t spacesPerTab = cDefaultSpacesPerTab);
        ~Document();

        // You can call read() functions multiple times before writing if
        // desirable. Once the document has been processed for writing, it can't
        // accept any more input.
        bool read(const std::string&);
        bool read(std::istream&);
        void write(std::ostream&);
        void writeTokens(std::ostream&); // For debugging

        // The class is marked noncopyable because it uses reference-counted
        // links to things that get changed during processing. If you want to
        // copy it, use the `copy` function to explicitly say that.
        Document copy() const; // TODO: Copy function not yet written.

    private:
        bool _getline(std::istream& in, std::string& line);
        void _process();
        void _mergeMultilineHtmlTags();
        void _processInlineHtmlAndReferences();
        void _processBlocksItems(TokenPtr inTokenContainer);
        void _processParagraphLines(TokenPtr inTokenContainer);

        static const size_t cSpacesPerInitialTab, cDefaultSpacesPerTab;

        const size_t cSpacesPerTab;
        TokenPtr mTokenContainer;
        LinkIds* mIdTable;
        bool mProcessed;
    };

} // namespace markdown

