/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "IceProxyFinder.h"

#include <ArmarXGui/libraries/ArmarXGuiBase/ui_IceProxyFinder.h>

#include <ArmarXCore/core/exceptions/LocalException.h>
#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <ArmarXCore/core/IceGridAdmin.h>
#include <ArmarXCore/core/IceManager.h>

#include <IceStorm/IceStorm.h>
#include <Ice/Initialize.h>

#include <QLineEdit>
#include <QtGui>

namespace armarx
{
    struct IceProxyFinderBase::Impl
    {
        IceManagerPtr icemanager;
        RunningTask<IceProxyFinderBase>::pointer_type refreshProxyListTask;
        QString defaultItem;
    };

    IceProxyFinderBase::IceProxyFinderBase(QWidget* parent) :
        QWidget(parent),
        ui(new ::Ui::IceProxyFinder),
        impl(new Impl)
    {
        setupUi();
    }

    IceProxyFinderBase::~IceProxyFinderBase()
    {
        if (impl->refreshProxyListTask)
        {
            impl->refreshProxyListTask->stop(true);
        }
        delete ui;
    }

    void IceProxyFinderBase::setIceManager(IceManagerPtr icemanager, bool fetchProxies)
    {
        this->impl->icemanager = icemanager;
        if (fetchProxies)
        {
            emit triggerProxyListComboBoxUpdateSignal();
        }
    }

    QString IceProxyFinderBase::getSelectedProxyName() const
    {
        return ui->cbProxyName->currentText();
    }

    void IceProxyFinderBase::setDefaultSelectedProxy(const QString& proxyName)
    {
        ui->cbProxyName->setEditText(proxyName);
    }

    void IceProxyFinderBase::setDefaultSelectedProxyAsItem(const QString& proxyName)
    {
        impl->defaultItem = proxyName;
    }

    void IceProxyFinderBase::setSearchMask(const QString& searchMask)
    {
        ui->edtSearchMask->setText(searchMask);
    }

    void IceProxyFinderBase::showSearchMaskField(bool show)
    {
        ui->edtSearchMask->setVisible(show);
        ui->labelMask->setVisible(show);
    }

    void IceProxyFinderBase::showLabels(bool show)
    {
        ui->labelMask->setVisible(show);
        ui->labelProxy->setVisible(show);
    }

    ::Ui::IceProxyFinder* IceProxyFinderBase::getUi()
    {
        return ui;
    }

    QWidget* IceProxyFinderBase::getProxyNameComboBox()
    {
        return ui->cbProxyName;
    }

    QStringList IceProxyFinderBase::getProxyNameListBase(QString searchMask) const
    {
        if (searchMask.length() == 0)
        {
            searchMask = "*";
        }

        Ice::StringSeq proxyList;
        QStringList qProxyList;
        if (!impl->icemanager)
        {
            throw LocalException("IceManager must not be NULL");
        }
        auto searchMasks = Split(searchMask.toStdString(), "|");
        for (auto& subSearchMasks : searchMasks)
        {
            if (impl->icemanager && impl->icemanager->getIceGridSession())
            {

                IceGridAdminPtr iceGridSession = impl->icemanager->getIceGridSession();
                IceGrid::AdminPrx admin = iceGridSession->getAdmin();
                Ice::AsyncResultPtr localBegin_getAllObjectInfos = admin->begin_getAllObjectInfos(subSearchMasks);
                while (!localBegin_getAllObjectInfos->isCompleted())
                {
                    if (impl->refreshProxyListTask->isStopped())
                    {
                        return qProxyList;
                    }
                    usleep(10000);
                }
                IceGrid::ObjectInfoSeq objects = admin->end_getAllObjectInfos(localBegin_getAllObjectInfos);
                Ice::StringSeq tempProxyList;

                for (auto& object : objects)
                {
                    // if objects are hangig we might get connection refused
                    try
                    {
                        if (impl->refreshProxyListTask->isStopped())
                        {
                            return qProxyList;
                        }
                        std::string typedName = getTypedProxyName(object.proxy);
                        if (!typedName.empty())
                        {
                            tempProxyList.push_back(typedName);
                        }
                    }
                    catch (...)
                    {
                    }
                }


                proxyList.insert(proxyList.end(), tempProxyList.begin(), tempProxyList.end());
            }
        }



        for (unsigned int i = 0; i < proxyList.size(); ++i)
        {
            qProxyList << QString::fromStdString(proxyList.at(i));
        }
        qProxyList.removeDuplicates();
        return qProxyList;
    }

    void IceProxyFinderBase::triggerProxyListUpdate()
    {
        if (impl->refreshProxyListTask && impl->refreshProxyListTask->isRunning())
        {
            return;
        }

        ui->btnRefresh->setEnabled(false);
        ui->btnRefresh->setToolTip("Refreshing proxy list with mask '" + ui->edtSearchMask->text() + "'. Please wait...");
        impl->refreshProxyListTask = new RunningTask<IceProxyFinderBase>(this, &IceProxyFinderBase::updateProxyList);
        impl->refreshProxyListTask->start();
    }

    void IceProxyFinderBase::comboBoxTextChanged(QString currentText)
    {
        triggerProxyListUpdate();
    }

    void IceProxyFinderBase::updateProxyListComboBox(const QStringList& proxyList)
    {
        ui->cbProxyName->clear();
        if (not impl->defaultItem.isEmpty())
        {
            ui->cbProxyName->addItem(impl->defaultItem);
            ui->cbProxyName->setCurrentIndex(0);
        }
        int i = 0;

        for (; i < proxyList.size(); ++i)
        {
            if (proxyList.at(i) != impl->defaultItem)
            {
                ui->cbProxyName->addItem(proxyList.at(i));
            }
        }

        ui->btnRefresh->setEnabled(true);
        ui->btnRefresh->setToolTip("Press to refetch all available Kinematic Units with the given mask");
    }

    void IceProxyFinderBase::evaluateValidProxySelectedSignal(const QString& proxyName)
    {
        try
        {
            // Try to get the object proxy "objPrx" represented by "proxyName"
            IceGrid::AdminPrx admin = this->impl->icemanager->getIceGridSession()->getAdmin();
            Ice::Identity objectIceId = Ice::stringToIdentity(proxyName.toStdString());
            Ice::ObjectPrx objPrx = admin->getObjectInfo(objectIceId).proxy;

            // Try to ping the proxy (it could still be a stale entry in IceGrid)
            objPrx->ice_ping();

            // If this point is reached, the object represented by "proxyName" is valid and reachable via Ice - that is, the signal can be emitted
            emit validProxySelected(proxyName);
        }
        catch (...)
        {
            // pass: Any exception triggered in the try-block indicates that "proxyName" is not a valid indicator for an object known to Ice
        }
    }

    void IceProxyFinderBase::updateProxyList()
    {
        auto result = getProxyNameList(ui->edtSearchMask->text());
        if (!impl->refreshProxyListTask->isStopped())
        {
            emit updateProxyListComboBoxSignal(result);
        }
    }

    void IceProxyFinderBase::setupUi()
    {
        this->ui->setupUi(this);

        this->connect(this, SIGNAL(updateProxyListComboBoxSignal(QStringList)), this, SLOT(updateProxyListComboBox(QStringList)));

        // These signals should trigger an update of the proxy list
        this->connect(this->ui->btnRefresh, SIGNAL(clicked()), this, SLOT(triggerProxyListUpdate()));
        this->connect(this->ui->edtSearchMask, SIGNAL(textEdited(QString)), this, SLOT(triggerProxyListUpdate()));
        this->connect(this, SIGNAL(triggerProxyListComboBoxUpdateSignal()), this, SLOT(triggerProxyListUpdate()));

        // If the combobox changes in any way (text manually updated or new item selected) it should be evaluated whether
        // the "validProxySelected" signal should be emitted
        this->connect(this->ui->cbProxyName, SIGNAL(editTextChanged(QString)), this, SLOT(evaluateValidProxySelectedSignal(QString)));
        this->connect(this->ui->cbProxyName, SIGNAL(currentIndexChanged(QString)), this, SLOT(evaluateValidProxySelectedSignal(QString)));
    }

    IceTopicFinder::IceTopicFinder(QWidget* parent)
        : IceProxyFinderBase(parent)
    {
        ui->labelProxy->setText("Topic");
    }

    QStringList IceTopicFinder::getProxyNameList(QString searchMask) const
    {
        if (searchMask.length() == 0)
        {
            searchMask = "*";
        }

        IceStorm::TopicDict topicList;

        if (!impl->icemanager)
        {
            throw LocalException("IceManager must not be NULL");
        }

        if (impl->icemanager && impl->icemanager->getIceGridSession())
        {
            topicList = impl->icemanager->getTopicManager()->retrieveAll();
        }

        QStringList qProxyList;

        for (auto& e : topicList)
        {
            auto name =  QString::fromStdString(e.first);
            QRegExp rex(searchMask, Qt::CaseInsensitive, QRegExp::Wildcard);

            if (name.contains(rex))
            {
                qProxyList << name;
            }
        }

        return qProxyList;
    }

}
