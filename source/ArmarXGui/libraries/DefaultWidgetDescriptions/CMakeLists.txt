set(LIB_NAME       DefaultWidgetDescriptions)

armarx_component_set_name("${LIB_NAME}")
armarx_set_target("Library: ${LIB_NAME}")

set(LIBS ArmarXCore ArmarXGuiInterfaces)

set(LIB_FILES   DefaultWidgetDescriptions.cpp)
set(LIB_HEADERS DefaultWidgetDescriptions.h  )


armarx_add_library("${LIB_NAME}" "${LIB_FILES}" "${LIB_HEADERS}" "${LIBS}")

# add unit tests
add_subdirectory(test)
