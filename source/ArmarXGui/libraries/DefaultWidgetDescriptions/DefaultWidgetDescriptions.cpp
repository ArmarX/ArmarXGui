/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGui::ArmarXObjects::DefaultWidgetDescriptions
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include "DefaultWidgetDescriptions.h"

namespace armarx::WidgetDescription
{
    //base widgets
    HBoxLayoutPtr makeHBoxLayout(std::vector<WidgetPtr> elements)
    {
        HBoxLayoutPtr w = new HBoxLayout;
        w->children = std::move(elements);
        return w;
    }

    VBoxLayoutPtr makeVBoxLayout(std::vector<WidgetPtr> elements)
    {
        VBoxLayoutPtr w = new VBoxLayout;
        w->children = std::move(elements);
        return w;
    }

    FormLayoutElementPtr makeFormLayoutElement(WidgetPtr labelWidget, WidgetPtr child)
    {
        ARMARX_CHECK_NOT_NULL(labelWidget);
        ARMARX_CHECK_NOT_NULL(child);
        FormLayoutElementPtr e = new FormLayoutElement;
        e->labelWidget = std::move(labelWidget);
        e->child = std::move(child);
        e->childIsSpanning = false;
        return e;
    }

    FormLayoutElementPtr makeFormLayoutElement(std::string name, WidgetPtr child, bool spanning)
    {
        ARMARX_CHECK_NOT_NULL(child);
        FormLayoutElementPtr e = new FormLayoutElement;
        e->label = std::move(name);
        e->child = std::move(child);
        e->childIsSpanning = spanning;
        return e;
    }

    FormLayoutElementPtr makeFormLayoutElement(WidgetPtr child, bool spanning)
    {
        ARMARX_CHECK_NOT_NULL(child);
        FormLayoutElementPtr e = new FormLayoutElement;
        if (ConfigWidgetPtr::dynamicCast(child))
        {
            e->label = ConfigWidgetPtr::dynamicCast(child)->name;
        }
        e->child = std::move(child);
        e->childIsSpanning = spanning;
        return e;
    }

    FormLayoutElementPtr makeSpanningFormLayoutElement(WidgetPtr child)
    {
        return makeFormLayoutElement(std::move(child), true);
    }

    FormLayoutPtr makeFormLayout(std::vector<std::pair<std::string, WidgetPtr> > elements)
    {
        FormLayoutPtr l = new FormLayout;
        l->children.reserve(elements.size());
        for (auto& elem : elements)
        {
            l->children.emplace_back(makeFormLayoutElement(std::move(elem.first), std::move(elem.second)));
        }
        return l;
    }

    FormLayoutPtr makeFormLayout(std::vector<WidgetPtr> elements)
    {
        FormLayoutPtr l = new FormLayout;
        l->children.reserve(elements.size());
        for (auto& elem : elements)
        {
            l->children.emplace_back(makeFormLayoutElement(std::move(elem)));
        }
        return l;
    }

    GroupBoxPtr makeGroupBox(std::string label, WidgetPtr child, bool framed)
    {
        GroupBoxPtr w = new GroupBox;
        w->child = std::move(child);
        w->label = std::move(label);
        w->framed = framed;
        return w;
    }

    WidgetPtr makeFrame(WidgetPtr child)
    {
        child->framed = true;
        return child;
    }

    HSpacerPtr makeHSpacer()
    {
        return new HSpacer;
    }

    VSpacerPtr makeVSpacer()
    {
        return new VSpacer;
    }

    HLinePtr makeHLine()
    {
        return new HLine;
    }

    VLinePtr makeVLine()
    {
        return new VLine;
    }

    LabelPtr makeLabel(std::string text)
    {
        return new Label {false, std::move(text)};
    }

    CheckBoxPtr makeCheckBox(std::string name, bool defaultValue, std::string label)
    {
        CheckBoxPtr w = new CheckBox;
        w->name = std::move(name);
        w->label = std::move(label);
        w->defaultValue = defaultValue;
        return w;
    }
    CheckBoxPtr makeCheckBox(std::string name, bool defaultValue)
    {
        CheckBoxPtr w = new CheckBox;
        w->name = std::move(name);
        w->label = w->name;
        w->defaultValue = defaultValue;
        return w;
    }

    IntSpinBoxPtr makeIntSpinBox(std::string name, int min, int max, int defaultValue)
    {
        IntSpinBoxPtr w = new IntSpinBox;
        w->name = std::move(name);
        w->min = min;
        w->max = max;
        w->defaultValue = defaultValue;
        return w;
    }

    FloatSpinBoxPtr makeFloatSpinBox(std::string name, float min, float max, float defaultValue, int steps, int decimals)
    {
        FloatSpinBoxPtr w = new FloatSpinBox;
        w->name = std::move(name);
        w->min = min;
        w->max = max;
        w->defaultValue = defaultValue;
        w->steps = steps;
        w->decimals = decimals;
        return w;
    }

    DoubleSpinBoxPtr makeDoubleSpinBox(std::string name, double min, double max, double defaultValue, int steps, int decimals)
    {
        DoubleSpinBoxPtr w = new DoubleSpinBox;
        w->name = std::move(name);
        w->min = min;
        w->max = max;
        w->defaultValue = defaultValue;
        w->steps = steps;
        w->decimals = decimals;
        return w;
    }

    IntSliderPtr makeIntSlider(std::string name, int min, int max, int defaultValue)
    {
        IntSliderPtr w = new IntSlider;
        w->name = std::move(name);
        w->min = min;
        w->max = max;
        w->defaultValue = defaultValue;
        return w;
    }

    FloatSliderPtr makeFloatSlider(std::string name, float min, float max, float defaultValue)
    {
        FloatSliderPtr w = new FloatSlider;
        w->name = std::move(name);
        w->min = min;
        w->max = max;
        w->defaultValue = defaultValue;
        return w;
    }

    DoubleSliderPtr makeDoubleSlider(std::string name, double min, double max, double defaultValue)
    {
        DoubleSliderPtr w = new DoubleSlider;
        w->name = std::move(name);
        w->min = min;
        w->max = max;
        w->defaultValue = defaultValue;
        return w;
    }

    StringComboBoxPtr makeStringComboBox(std::string name, std::vector<std::string> options, long defaultIndex)
    {
        StringComboBoxPtr w = new StringComboBox;
        w->name = std::move(name);
        w->options = std::move(options);
        w->defaultIndex = defaultIndex;
        return w;
    }

    LineEditPtr makeLineEdit(std::string name, std::string defaultValue)
    {
        LineEditPtr w = new LineEdit;
        w->name = std::move(name);
        w->defaultValue = std::move(defaultValue);
        return w;
    }

    FloatLineEditPtr makeFloatLineEdit(std::string name, float defaultValue)
    {
        FloatLineEditPtr w = new FloatLineEdit;
        w->name = std::move(name);
        w->defaultValue = defaultValue;
        return w;
    }

    DoubleLineEditPtr makeDoubleLineEdit(std::string name, double defaultValue)
    {
        DoubleLineEditPtr w = new DoubleLineEdit;
        w->name = std::move(name);
        w->defaultValue = defaultValue;
        return w;
    }

    //more complex widgets
    HBoxLayoutPtr makeXYZRollPitchYawWidget(
        const std::string& namePrefix,
        float initX,
        float initY,
        float initZ,
        float initRoll,
        float initPitch,
        float initYaw,
        FloatRange rangeX,
        FloatRange rangeY,
        FloatRange rangeZ,
        FloatRange rangeRoll,
        FloatRange rangePitch,
        FloatRange rangeYaw)
    {
        return makeHBoxLayout(
        {
            makeLabel("x / y / z"),
            makeFloatSpinBox(namePrefix + "X", rangeX.min, rangeX.max, initX),
            makeFloatSpinBox(namePrefix + "Y", rangeY.min, rangeY.max, initY),
            makeFloatSpinBox(namePrefix + "Z", rangeZ.min, rangeZ.max, initZ),
            makeLabel("   rx / ry / rz"),
            makeFloatSpinBox(namePrefix + "Roll", rangeRoll.min, rangeRoll.max, initRoll, 2000),
            makeFloatSpinBox(namePrefix + "Pitch", rangePitch.min, rangePitch.max, initPitch, 2000),
            makeFloatSpinBox(namePrefix + "Yaw", rangeYaw.min, rangeYaw.max, initYaw, 2000)
        });
    }
}
