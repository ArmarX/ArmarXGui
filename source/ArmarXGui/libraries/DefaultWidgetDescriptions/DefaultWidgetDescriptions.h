/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGui::ArmarXObjects::DefaultWidgetDescriptions
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXGui/interface/WidgetDescription.h>

#include <cmath>

namespace armarx::WidgetDescription
{
    //base widgets
    HBoxLayoutPtr makeHBoxLayout(std::vector<WidgetPtr> elements);
    VBoxLayoutPtr makeVBoxLayout(std::vector<WidgetPtr> elements);

    FormLayoutElementPtr makeFormLayoutElement(WidgetPtr child, bool spanning = false);
    FormLayoutElementPtr makeSpanningFormLayoutElement(WidgetPtr child);
    FormLayoutElementPtr makeFormLayoutElement(std::string name, WidgetPtr child, bool spanning = false);
    FormLayoutElementPtr makeFormLayoutElement(WidgetPtr labelWidget, WidgetPtr child);

    FormLayoutPtr makeFormLayout(std::vector<std::pair<std::string, WidgetPtr>> elements);
    FormLayoutPtr makeFormLayout(std::vector<WidgetPtr> elements);

    GroupBoxPtr makeGroupBox(std::string label, WidgetPtr child, bool framed = false);

    WidgetPtr makeFrame(WidgetPtr child);

    HSpacerPtr makeHSpacer();
    VSpacerPtr makeVSpacer();
    HLinePtr makeHLine();
    VLinePtr makeVLine();
    LabelPtr makeLabel(std::string text);

    CheckBoxPtr makeCheckBox(std::string name, bool defaultValue, std::string label);
    CheckBoxPtr makeCheckBox(std::string name, bool defaultValue = false);

    IntSpinBoxPtr makeIntSpinBox(std::string name, int min = 0, int max = 100, int defaultValue = 0);
    FloatSpinBoxPtr makeFloatSpinBox(std::string name, float min = 0, float max = 100, float defaultValue = 0, int steps = 100, int decimals = 3);
    DoubleSpinBoxPtr makeDoubleSpinBox(std::string name, double min = 0, double max = 100, double defaultValue = 0, int steps = 100, int decimals = 3);

    IntSliderPtr makeIntSlider(std::string name, int min = 0, int max = 100, int defaultValue = 0);
    FloatSliderPtr makeFloatSlider(std::string name, float min = 0, float max = 100, float defaultValue = 0);
    DoubleSliderPtr makeDoubleSlider(std::string name, double min = 0, double max = 100, double defaultValue = 0);

    StringComboBoxPtr makeStringComboBox(std::string name, std::vector<std::string> options, long defaultIndex = 0);

    LineEditPtr makeLineEdit(std::string name, std::string defaultValue = "");
    FloatLineEditPtr makeFloatLineEdit(std::string name, float defaultValue = 0);
    DoubleLineEditPtr makeDoubleLineEdit(std::string name, double defaultValue = 0);

    //more complex widgets
    /**
         * @brief Values will have the names namePrefix{X,Y,Z,Roll,Pitch,Yaw}
         */
    HBoxLayoutPtr makeXYZRollPitchYawWidget(
        const std::string& namePrefix = "",
        float initX           = 0,
        float initY           = 0,
        float initZ           = 0,
        float initRoll        = 0,
        float initPitch       = 0,
        float initYaw         = 0,
        FloatRange rangeX     = { -25000, 25000},
        FloatRange rangeY     = { -25000, 25000},
        FloatRange rangeZ     = { -0, 25000},
        FloatRange rangeRoll  = { -M_PI, M_PI},
        FloatRange rangePitch = { -M_PI, M_PI},
        FloatRange rangeYaw   = { -M_PI, M_PI});

    inline HBoxLayoutPtr makeXYZRollPitchYawWidget(
        const std::string& namePrefix,
        FloatRange rangeLin,
        FloatRange rangeAng)
    {
        return makeXYZRollPitchYawWidget(namePrefix, 0, 0, 0, 0, 0, 0, rangeLin, rangeLin, rangeLin, rangeAng, rangeAng, rangeAng);
    }
}
