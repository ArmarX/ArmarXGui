/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGui::ArmarXObjects::RemoteGuiProvider
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <functional>

#include "RemoteGuiProvider.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/util/algorithm.h>
#include <ArmarXCore/util/CPPUtility/GetTypeString.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetHandler.h>


namespace armarx
{
    void RemoteGuiProvider::onInitComponent()
    {
        topicName = getProperty<std::string>("TopicName").getValue();

        offeringTopic(topicName);
    }


    void RemoteGuiProvider::onConnectComponent()
    {
        topic = getTopic<RemoteGuiListenerInterfacePrx>(topicName);
    }


    void RemoteGuiProvider::onDisconnectComponent()
    {

    }


    void RemoteGuiProvider::onExitComponent()
    {

    }

    armarx::PropertyDefinitionsPtr RemoteGuiProvider::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new RemoteGuiProviderPropertyDefinitions(
                getConfigIdentifier()));
    }

    std::string RemoteGuiProvider::getTopicName(const Ice::Current&)
    {
        return topicName;
    }

    static void fillRecursively(RemoteGui::WidgetStateMap& widgetStates,
                                RemoteGui::ValueMap& values,
                                RemoteGui::WidgetPtr const& widget,
                                RemoteGui::WidgetPtr const& rootWidget)
    {
        ARMARX_TRACE;
        const auto dumpInfo = ARMARX_STREAM_PRINTER
        {
            out << "\nall state names: " << getMapKeys(widgetStates)
                << "\nall value names: " << getMapKeys(values)
                << "\nwidget tree\n";

            std::function<void(RemoteGui::WidgetPtr const&, std::size_t)> dump =
            [&](RemoteGui::WidgetPtr const & node, std::size_t lvl)
            {
                out << std::string(lvl * 4, '-') << " " << node->name
                    << " [" << GetTypeString(*node) << "]\n";
                for (const auto& c : node->children)
                {
                    dump(c, lvl + 1);
                }
            };
            dump(rootWidget, 1);
        };
        RemoteGui::WidgetHandler const& handler = RemoteGui::getWidgetHandler(widget);
        {
            std::stringstream s;
            if (!handler.isValid(*widget, s))
            {
                throw LocalException() << "Widget is not valid: " << widget->name
                                       << ", WidgetT: " << widget->ice_id()
                                       << ", HandlerT: " << handler.getHandlerT()
                                       << "\nMessage:\n" << s.str()
                                       << dumpInfo;
            }
        }

        std::string const& name = widget->name;
        if (!name.empty())
        {
            ARMARX_TRACE;
            bool nameIsNew = widgetStates.emplace(widget->name, widget->defaultState).second;
            if (!nameIsNew)
            {
                ARMARX_TRACE;
                throw LocalException() << "Widget with name '" << widget->name << "' already exists"
                                       << dumpInfo;
            }
            nameIsNew = values.emplace(widget->name, widget->defaultValue).second;
            ARMARX_CHECK_EXPRESSION(nameIsNew);
        }

        for (auto& child : widget->children)
        {
            ARMARX_TRACE;
            fillRecursively(widgetStates, values, child, rootWidget);
        }
    }

    void RemoteGuiProvider::createTab(const std::string& tabId, const RemoteGui::WidgetPtr& rootWidget, const Ice::Current&)
    {
        ARMARX_TRACE;
        ARMARX_INFO << "Creating remote tab: " << tabId;
        {
            std::unique_lock<std::mutex> lock(tabMutex);
            tabs[tabId] = rootWidget;

            // Clear old state
            auto& widgetStates = tabWidgetStates[tabId];
            widgetStates.clear();
            auto& values = tabStates[tabId];
            values.clear();

            // Fill default values
            fillRecursively(tabWidgetStates[tabId], tabStates[tabId], rootWidget, rootWidget);
        }

        topic->reportTabChanged(tabId);
    }

    void RemoteGuiProvider::removeTab(const std::string& tabId, const Ice::Current&)
    {
        ARMARX_TRACE;
        ARMARX_INFO << "Removing remote tab: " << tabId;
        {
            std::unique_lock<std::mutex> lock(tabMutex);
            tabs.erase(tabId);
            tabWidgetStates.erase(tabId);
            tabStates.erase(tabId);
        }
        topic->reportTabsRemoved();
    }

    RemoteGui::WidgetMap RemoteGuiProvider::getTabs(const Ice::Current&)
    {
        std::unique_lock<std::mutex> lock(tabMutex);
        return tabs;
    }

    RemoteGui::TabWidgetStateMap RemoteGuiProvider::getTabStates(const Ice::Current&)
    {
        std::unique_lock<std::mutex> lock(tabMutex);
        return tabWidgetStates;
    }

    RemoteGui::TabValueMap RemoteGuiProvider::getValuesForAllTabs(const Ice::Current&)
    {
        std::unique_lock<std::mutex> lock(tabMutex);
        return tabStates;
    }

    RemoteGui::ValueMap RemoteGuiProvider::getValues(const std::string& tabId, const Ice::Current&)
    {
        std::unique_lock<std::mutex> lock(tabMutex);
        return tabStates.at(tabId);
    }

    void RemoteGuiProvider::setValue(const std::string& tabId, const std::string& widgetName, const RemoteGui::ValueVariant& value, const Ice::Current&)
    {
        ARMARX_TRACE;
        std::unique_lock<std::mutex> lock(tabMutex);
        RemoteGui::ValueMap& tabState = tabStates.at(tabId);
        tabState.at(widgetName) = value;
        RemoteGui::ValueMap delta;
        delta[widgetName] = value;
        topic->reportStateChanged(tabId, delta);
    }

    void RemoteGuiProvider::setValues(const std::string& tabId, const RemoteGui::ValueMap& values, const Ice::Current&)
    {
        ARMARX_TRACE;
        std::unique_lock<std::mutex> lock(tabMutex);
        RemoteGui::ValueMap& merged = tabStates.at(tabId);
        for (const auto& pair : values)
        {
            merged[pair.first] = pair.second;
        }
        topic->reportStateChanged(tabId, values);
    }

    RemoteGui::WidgetStateMap RemoteGuiProvider::getWidgetStates(const std::string& tabId, const Ice::Current&)
    {
        std::unique_lock<std::mutex> lock(tabMutex);
        return tabWidgetStates.at(tabId);
    }

    void RemoteGuiProvider::setWidgetStates(const std::string& tabId, const RemoteGui::WidgetStateMap& state, const Ice::Current&)
    {
        ARMARX_TRACE;
        std::unique_lock<std::mutex> lock(tabMutex);
        RemoteGui::WidgetStateMap& tabWidgetState = tabWidgetStates.at(tabId);
        tabWidgetState = state;
        topic->reportWidgetChanged(tabId, tabWidgetState);
    }
}
