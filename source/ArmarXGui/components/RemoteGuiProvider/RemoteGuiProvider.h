/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGui::ArmarXObjects::RemoteGuiProvider
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>

#include <ArmarXGui/interface/RemoteGuiInterface.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetRegister.h>

#include <mutex>

namespace armarx
{
    /**
     * @class RemoteGuiProviderPropertyDefinitions
     * @brief
     */
    class RemoteGuiProviderPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        RemoteGuiProviderPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("TopicName", "RemoteGuiTopic", "Name of the topic on which updates to the remote state are reported.");
        }
    };

    /**
     * @defgroup Component-RemoteGuiProvider RemoteGuiProvider
     * @ingroup ArmarXGui-Components
     * A description of the component RemoteGuiProvider.
     *
     * @class RemoteGuiProvider
     * @ingroup Component-RemoteGuiProvider
     * @brief Brief description of class RemoteGuiProvider.
     *
     * Detailed description of class RemoteGuiProvider.
     */
    class RemoteGuiProvider
        : virtual public armarx::Component
        , virtual public armarx::RemoteGuiInterface
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "RemoteGuiProvider";
        }

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        // RemoteGuiInterface interface
    public:
        std::string getTopicName(const Ice::Current&) override;

        void createTab(const std::string& tabId, const RemoteGui::WidgetPtr& rootWidget, const Ice::Current&) override;
        void removeTab(const std::string& tabId, const Ice::Current&) override;

        RemoteGui::WidgetMap getTabs(const Ice::Current&) override;
        RemoteGui::TabWidgetStateMap getTabStates(const Ice::Current&) override;
        RemoteGui::TabValueMap getValuesForAllTabs(const Ice::Current&) override;
        RemoteGui::ValueMap getValues(const std::string& tabId, const Ice::Current&) override;

        void setValue(const std::string& tabId, const std::string& widgetName, const RemoteGui::ValueVariant& value, const Ice::Current&) override;
        void setValues(const std::string& tabId, const RemoteGui::ValueMap& values, const Ice::Current&) override;

        RemoteGui::WidgetStateMap getWidgetStates(const std::string& tabId, const Ice::Current&) override;
        void setWidgetStates(const std::string& tabId, const RemoteGui::WidgetStateMap& state, const Ice::Current&) override;

    private:
        std::string topicName;

        RemoteGuiListenerInterfacePrx topic;

        std::mutex tabMutex;
        RemoteGui::WidgetMap tabs;
        RemoteGui::TabWidgetStateMap tabWidgetStates;
        RemoteGui::TabValueMap tabStates;
    };
}
