/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXGui
 * @author     Mirko Waechter (waechter at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/interface/core/BasicVectorTypes.ice>
#include <ArmarXCore/interface/observers/VariantBase.ice>

module armarx
{
    dictionary <string, Vector2fSeq> StringVector2fSeqDict;
    dictionary <string, Ice::FloatSeq> StringFloatSeqDict;

    interface StaticPlotterInterface
    {
        void addPlot(string plotName, StringVector2fSeqDict plotsData);
        void addPlotWithTimestampVector(string plotName, Ice::FloatSeq timestamps, StringFloatSeqDict plotsData);
    };

};
