/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGui::ArmarXObjects::RemoteGuiExample2
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RemoteGuiExample2.h"

#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>


namespace armarx
{

    RemoteGuiExample2::RemoteGuiExample2()
    {
    }


    std::string RemoteGuiExample2::getDefaultName() const
    {
        return GetDefaultName();
    }


    std::string RemoteGuiExample2::GetDefaultName()
    {
        return "RemoteGuiExample2";
    }


    armarx::PropertyDefinitionsPtr RemoteGuiExample2::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs(new armarx::ComponentPropertyDefinitions(getConfigIdentifier()));

        return defs;
    }


    void RemoteGuiExample2::onInitComponent()
    {
    }


    void RemoteGuiExample2::onConnectComponent()
    {
        createTab_Widgets();
        createTab_Events();

        RemoteGui_startRunningTask();
    }


    void RemoteGuiExample2::onDisconnectComponent()
    {
        ARMARX_INFO << "Removing tabs";
        widgetsTab.remove();
        eventsTab.remove();
    }


    void RemoteGuiExample2::onExitComponent()
    {
        onDisconnectComponent();
    }


    void RemoteGuiExample2::RemoteGui_update()
    {
        updateTab_Widgets();
        updateTab_Events();
    }


    void RemoteGuiExample2::createTab_Widgets()
    {
        // Just add a lot of widgets...

        VBoxLayout vLayout;
        {
            widgetsTab.lineEdit.setValue("Hello");

            Label label("Line: ");
            HBoxLayout line{label, widgetsTab.lineEdit};
            vLayout.addChild(line);
        }

        {
            widgetsTab.combo.addOptions({"First", "Second", "Third", "Fourth"});
            widgetsTab.combo.setValue("Second");

            HBoxLayout line{Label("Combo: "), widgetsTab.combo};
            vLayout.addChild(line);
        }

        {
            widgetsTab.checkBox.setValue(true);

            HBoxLayout line{Label("Check: "), widgetsTab.checkBox};
            vLayout.addChild(line);
        }

        {
            widgetsTab.toggle.setValue(true);
            widgetsTab.toggle.setLabel("Toggle");

            HBoxLayout line{Label("Toggle: "), widgetsTab.toggle};
            vLayout.addChild(line);
        }

        {
            widgetsTab.intSlider.setRange(0, 10);
            widgetsTab.intSlider.setValue(5);

            widgetsTab.intSpin.setRange(0, 10);
            widgetsTab.intSpin.setValue(5);

            HBoxLayout line{Label("Int: "), widgetsTab.intSlider, widgetsTab.intSpin};
            vLayout.addChild(line);
        }

        {
            widgetsTab.floatSlider.setRange(0.0f, 2.0f);
            widgetsTab.floatSlider.setSteps(100);
            widgetsTab.floatSlider.setValue(0.0f);

            widgetsTab.floatSpin.setRange(0.0f, 2.0f);
            widgetsTab.floatSpin.setSteps(20);
            widgetsTab.floatSpin.setDecimals(2);
            widgetsTab.floatSpin.setValue(0.4f);

            HBoxLayout line{Label("Float: "), widgetsTab.floatSlider, widgetsTab.floatSpin};
            vLayout.addChild(line);
        }

        {
            widgetsTab.button.setLabel("Button");

            HBoxLayout line{Label("Button: "), widgetsTab.button};
            vLayout.addChild(line);
        }

        vLayout.addChild(VSpacer());

        GroupBox groupBox;
        groupBox.setLabel("Group VBoxLayout");
        groupBox.addChild(vLayout);

        auto makeButton = [](std::string const & text)
        {
            Button result;
            result.setLabel(text);
            return result;
        };

        GroupBox gridGroupBox;
        gridGroupBox.setLabel("Group GridLayout");
        gridGroupBox.addChild(
            GridLayout()
            .add(makeButton("1"), Pos{0, 0}, Span{1, 2})
            .add(makeButton("2"), Pos{0, 2}, Span{2, 1})
            .add(makeButton("3"), Pos{1, 1}, Span{1, 1})
            .add(makeButton("4"), Pos{1, 0}, Span{2, 1})
            .add(Label("foooooooooooooooooo"), Pos{2, 1}, Span{1, 2})
            .add(HSpacer(), Pos{0, 3}, Span{1, 1})
        );

        GroupBox collapsedGroup;
        collapsedGroup.setLabel("Group Collapsed");
        collapsedGroup.setCollapsed(true);
        collapsedGroup.addChild(Label("This can only be seen if expanded"));

        VBoxLayout root = {groupBox, gridGroupBox, collapsedGroup};

        RemoteGui_createTab("Example2_Widgets", root, &widgetsTab);
    }


    void RemoteGuiExample2::updateTab_Widgets()
    {
        widgetsTab.receiveUpdates();

        // Sync the slider and the spin box manually (slider is the master)
        int intSliderValue = widgetsTab.intSlider.getValue();
        widgetsTab.intSpin.setValue(intSliderValue);

        // And the other way around for the float slider/spin box
        float floatBoxValue = widgetsTab.floatSpin.getValue();
        widgetsTab.floatSlider.setValue(floatBoxValue);

        widgetsTab.sendUpdates();
    }


    void RemoteGuiExample2::createTab_Events()
    {
        VBoxLayout vLayout;

        {
            Label label;
            label.setText("Counter: ");

            eventsTab.spinInt.setRange(-10, 10);
            eventsTab.spinInt.setValue(0);


            HBoxLayout line;
            line.addChild(label);
            line.addChild(eventsTab.spinInt);

            vLayout.addChild(line);
        }

        {
            eventsTab.buttonUp.setLabel("Up");
            eventsTab.buttonDown.setLabel("Down");

            HBoxLayout line;
            line.addChild(eventsTab.buttonUp);
            line.addChild(eventsTab.buttonDown);

            vLayout.addChild(line);
        }

        vLayout.addChild(VSpacer());

        RemoteGui_createTab("Example2_Events", vLayout, &eventsTab);
    }


    void RemoteGuiExample2::updateTab_Events()
    {
        int currentValue = eventsTab.spinInt.getValue();
        if (eventsTab.buttonUp.wasClicked())
        {
            ++currentValue;
        }
        if (eventsTab.buttonDown.wasClicked())
        {
            --currentValue;
        }

        eventsTab.spinInt.setValue(currentValue);
    }


    ARMARX_REGISTER_COMPONENT_EXECUTABLE(RemoteGuiExample2, RemoteGuiExample2::GetDefaultName());
}
