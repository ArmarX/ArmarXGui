/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGui::ArmarXObjects::RemoteGuiExample2
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/Component.h>

#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>

#include <ArmarXGui/libraries/RemoteGui/Client/Tab.h>
#include <ArmarXGui/libraries/RemoteGui/Client/Widgets.h>


namespace armarx
{
    using namespace armarx::RemoteGui::Client;

    struct WidgetsTab : Tab
    {
        LineEdit lineEdit;
        ComboBox combo;
        CheckBox checkBox;
        ToggleButton toggle;

        IntSpinBox intSpin;
        IntSlider intSlider;

        FloatSpinBox floatSpin;
        FloatSlider floatSlider;

        Button button;
    };

    struct EventsTab : Tab
    {
        IntSpinBox spinInt;
        Button buttonUp;
        Button buttonDown;
    };



    /**
     * @defgroup Component-RemoteGuiExample2 RemoteGuiExample2
     * @ingroup ArmarXGui-Components
     *
     * An example for how to use the remote gui framework for building simple
     * user interfaces over ice.
     *
     * Often, you want to allow simple user interactions with an ArmarX
     * component, e.g. to tweak a parameter, enable a feature or visualization
     * or trigger an action. Building a dedicated, full-blown gui plugin can
     * be very tedious for simple cases.
     * For this use case, ArmarX offers a remote gui framework for building
     * user interfaces over ice.
     *
     * This component demonstrates how to do this based on two examples:
     * \li A *widgets* tab focusing on how to add different widget types,
     *     such as sliders, spinners, check boxes and buttons.
     * \li An *events* tab showing how you can change the remote gui as a
     *     reaction to events triggered by the user via the remote gui.
     *
     * To run the example:
     * \li Start the component `RemoteGuiProvider`
     * \li Open the gui plugin `RemoteGui`
     * \li Start the component `RemoteGuiExample2`
     * \li Observe and interact with the created tabs in the `RemoteGui` widget.
     *
     * The scenario `RemoteGuiTest` starts the necessary components,
     * including the example component.
     *
     * A component which wants to offer a remote gui needs to:
     * \li `#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>`
     * \li Inherit from the `armarx::LightweightRemoteGuiComponentPluginUser`
     * \li Build and create a `armarx::RemoteGui::Client::Tab` in its
     *     `onConnectComponent()` using `RemoteGui_createTab()`
     * \li Start the update processing thread in `onConnectComponent()`
     *     using `RemoteGui_startRunningTask()`
     * \li Implement the virtual function `RemoteGui_update()` to process gui
     *     events (e.g. changed values, pressed buttons)
     *
     * `RemoteGui_createTab()` can be called once by the component to create
     * a tab in the `RemoteGui` widget. In can be called again later to
     * rebuild the tab, e.g. if the gui's structure should change.
     *
     * `RemoteGui_update()` is called by a processing thread at a regular
     * interval. There, you can check the widgets for whether their value has
     * changed (`.hasValueChanged()`) or whether a button was pressed
     * (`.wasClicked()`).
     *
     * **Important note:** `RemoteGui_update()` is called from a separate thread,
     * so you have to use synchronization mechanisms such as `std::mutex` and
     * `std::scoped_lock` to synchronize access to variables which are
     * accessed by different threads.
     *
     * \see RemoteGuiExample2
     *
     *
     * @class RemoteGuiExample2
     * @ingroup Component-RemoteGuiExample2
     *
     * @brief Brief description of class RemoteGuiExample2.
     *
     * @see @ref Component-RemoteGuiExample2
     */
    class RemoteGuiExample2
        : virtual public armarx::Component
        , virtual public armarx::LightweightRemoteGuiComponentPluginUser
    {
    public:

        RemoteGuiExample2();

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;
        static std::string GetDefaultName();


    protected:

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;


    private:

        void RemoteGui_update() override;

        void createTab_Widgets();
        void updateTab_Widgets();

        void createTab_Events();
        void updateTab_Events();


    private:

        EventsTab eventsTab;
        WidgetsTab widgetsTab;

    };
}
