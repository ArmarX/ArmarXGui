/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "ArmarXMainWindow.h"
#include <ArmarXGui/applications/ArmarXGui/ui_ArmarXMainWindow.h>

#include "Widgets/ViewerWidget.h"
#include "Widgets/TitlebarWidget.h"
#include "Widgets/WidgetNameDialog.h"

// ArmarX
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/logging/LogSender.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/ArmarXObjectScheduler.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>

// Qt
#include <QtCore/QDirIterator>
#include <QFileDialog>
#include <QLabel>
#include <QListView>
#include <QInputDialog>
#include <QPushButton>
#include <QFile>
#include <QLabel>
#include <QToolButton>
#include <QTimer>
#include <QList>
#include <QStringList>
#include <QPluginLoader>
#include <QDialog>
#include <QComboBox>
#include <QGridLayout>
#include <QStackedWidget>
#include <QSplitter>
#include <QPlainTextEdit>
#include <QMdiArea>
#include <QMutex>
#include <QSignalMapper>
#include <QSpinBox>
#include <QMenu>
#include <QActionGroup>
#include <QModelIndex>
#include <QListWidget>
#include <QHBoxLayout>
#include <QDialogButtonBox>
#include <QProxyStyle>
#include <QPainter>
#include <QDesktopServices>
#include <QHostInfo>

#include <Inventor/nodes/SoPerspectiveCamera.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/InfixCompleter.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/InfixFilterModel.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/TipDialog.h>
#include <ArmarXGui/applications/ArmarXGui/Widgets/GuiUseCaseSelector.h>

#include <filesystem>

#include <QCompleter>
#include <QMessageBox>
#include <QScrollArea>
#include <QStringListModel>
#include <QUrl>
#include <QWidgetAction>

#include <SimoxUtility/algorithm/string/string_tools.h>

#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>
#include <QtSvg/QSvgRenderer>

#include <unistd.h>
#include <optional>
#include <sys/wait.h>
#include <sys/types.h>
#include <sstream>
//#include <omp.h>

// To start ArViz Godot
#include <fcntl.h>


#define ARMARX_ORGANIZATION "KIT"
#define ARMARX_GUI_APPLICATION_NAME "ArmarXGUI"

#define CONFIG_LOAD_LAST_CONFIG "LoadLastConfig"
#define CONFIG_BLACKLISTED_TIPS "BlacklistedTips"

namespace armarx
{
    ArmarXMainWindow::ArmarXMainWindow(const armarx::ManagedIceObjectRegistryInterfacePtr& registry, const std::vector<std::string>& packages, const QString& configToLoad, bool disablePreloading) :
        QMainWindow(NULL),
        pluginCache(ArmarXManagerPtr::dynamicCast(registry)),
        defaultPackageNames(packages),
        widgetsLocked(false),
        mainSettings(QSettings::UserScope, ARMARX_ORGANIZATION, ARMARX_GUI_APPLICATION_NAME)
    {
        setStyleSheet(R"(
            QMainWindow::separator {
                width: 3px; /* when vertical */
                height: 3px; /* when horizontal */
            }
            QMainWindow::separator:hover {
                background: gray;
            }
        )");

        mainWidgets = QStringList() << "Meta.LogViewer" << "Statecharts.StatechartEditor" << "Meta.SystemStateMonitor" << "Meta.ScenarioManager" << "MemoryX.WorkingMemoryGui";

        splashscreenPrefix =  "v" + QString::fromStdString(Application::GetVersion()) + " - ";
        QPixmap pm(QString("://icons/ArmarX-Splashscreen.png"));
        splashSceen = new QSplashScreen(pm);
        //    splashSceen->show();

        ui = new ::Ui::ArmarXMainWindow();
        mutex3d = std::make_shared<std::recursive_mutex>();
        ArmarXWidgetInfoPtr viewerWidgetInfo(new ArmarXWidgetInfo(ArmarXComponentWidgetController::createInstance<Viewer3DWidget>, Viewer3DWidget::GetWidgetIcon(), QIcon()));
        pluginCache.cacheWidget(ARMARX_VIEWER_NAME, viewerWidgetInfo);

        setTag(ARMARX_GUI_APPLICATION_NAME);
        QString username = qgetenv("USER");
        if (username.isEmpty())
        {
            username = qgetenv("USERNAME");
        }

        guiWindowBaseName = QString{ARMARX_GUI_APPLICATION_NAME} + " [" + username + "@" + QHostInfo::localHostName() + "]";
        setWindowTitle(guiWindowBaseName);
        this->registry = registry;
        setAttribute(Qt::WA_QuitOnClose);

        ui->setupUi(this);
        setCentralWidget(NULL);

        tipDialog = new TipDialog(this);
        tipDialog->setBlackListedStrings(mainSettings.value(CONFIG_BLACKLISTED_TIPS).toStringList());
        ui->actionLoadLastConfig->setChecked(mainSettings.value(CONFIG_LOAD_LAST_CONFIG).toBool());

        addWidgetAction = new AddArmarXWidgetAction("", this, this);
        connect(addWidgetAction, SIGNAL(clicked(QString, QString)), this, SLOT(createArmarXWidget(QString, QString)), Qt::UniqueConnection);


        connect(ui->actionLoadPlugin, SIGNAL(triggered()), this, SLOT(pluginDialog()));
        connect(ui->actionSave_Gui, SIGNAL(triggered()), this, SLOT(saveGuiConfig()));
        connect(ui->actionLoad_Gui_Config, SIGNAL(triggered()), this, SLOT(loadGuiConfig()));
        connect(ui->actionQuit, SIGNAL(triggered()), this, SLOT(close()));
        connect(ui->actionSave_Gui_as, SIGNAL(triggered()), this, SLOT(saveGuiConfigAs()));
        connect(ui->actionFullscreen, SIGNAL(triggered()), this, SLOT(enterFullscreenMode()));
        connect(ui->actionLock_Widgets, SIGNAL(triggered()), this, SLOT(toggleWidgetLock()));

        // EmergencyStop
        emergencyStopWidget = EmergencyStopWidgetPtr::dynamicCast(ArmarXComponentWidgetController::createInstance<EmergencyStopWidget>());
        this->registry->addObject(ManagedIceObjectPtr::dynamicCast(emergencyStopWidget));

        // plugins
        QSet<QString> pluginDirs;

        for (const std::string& packageName : packages)
        {
            pluginDirs.insert(getLibraryPathFromPackage(packageName.c_str()));
        }
        loadPlugins(pluginDirs, false);
        if (!disablePreloading)
        {
            pluginCache.preloadAsync(getFavoriteWidgets() + mainWidgets);
        }
        //    instantiatePlugins();

        updateRecentlyOpenedFileList();


        connectionStatusTimer = new QTimer(this);
        connect(connectionStatusTimer, SIGNAL(timeout()), this, SLOT(updateStatusOfOpenWidgets()));
        connectionStatusTimer->start(300);
        //changeLayout(2);
        QStringList recentlyFiles = mainSettings.value("RecentlyFiles").toStringList();
        splashSceen->finish(this);

        guiUseCaseSelector = new GuiUseCaseSelector(this, mainSettings.value("DoNotShowUseCaseDialog").toBool(), this);



        if (!configToLoad.isEmpty())
        {
            loadGuiConfig(configToLoad);
        }
        else if (!mainSettings.value("DoNotShowUseCaseDialog").toBool() && guiUseCaseSelector->exec() == QDialog::Accepted)
        {
            QString path = guiUseCaseSelector->getSelectedConfigFilePath();
            ARMARX_INFO << VAROUT(path);
            if (!path.isEmpty())
            {
                loadGuiConfig(path, false);
            }
        }
        else if (recentlyFiles.size() > 0 && mainSettings.value(CONFIG_LOAD_LAST_CONFIG).toBool())
        {
            //set to false in case a plugin crashes the gui
            mainSettings.setValue(CONFIG_LOAD_LAST_CONFIG, false);
            mainSettings.sync();
            loadGuiConfig(recentlyFiles.first());
            mainSettings.setValue(CONFIG_LOAD_LAST_CONFIG, ui->actionLoadLastConfig->isChecked());
            mainSettings.sync();
        }
        else
        {
            QStringList defaultWidgets {"Meta.LogViewer"};
            for (auto& widgetName : defaultWidgets)
            {
                if (existsWidget(widgetName))
                {
                    createArmarXWidget(widgetName, widgetName);
                }
            }
        }

        mainSettings.setValue("DoNotShowUseCaseDialog", guiUseCaseSelector->getDoNotShowAgain());
    }

    ArmarXMainWindow::~ArmarXMainWindow()
    {
        delete ui;
        connectionStatusTimer->stop();
        mainSettings.setValue(CONFIG_BLACKLISTED_TIPS, tipDialog->getBlackListedStrings());
        ARMARX_INFO << "~GuiWindow() ";
        delete splashSceen;
    }



    void ArmarXMainWindow::removeViewerWidget(QObject* widget)
    {
        ARMARX_WARNING << "Removing 3d viewer" << std::endl;

        if (!widget)
        {
            ARMARX_INFO << "viewerDockWidget ptr is NULL";
        }

        removeArmarXWidget(widget);
    }


    Viewer3DWidgetPtr ArmarXMainWindow::setupViewerWidget()
    {
        if (listOpenWidgets.find(ARMARX_VIEWER_NAME) != listOpenWidgets.end())
        {
            return Viewer3DWidgetPtr::dynamicCast(listOpenWidgets.find(ARMARX_VIEWER_NAME).value().second);
        }

        Viewer3DWidgetPtr viewer = Viewer3DWidgetPtr::dynamicCast(createArmarXWidget(ARMARX_VIEWER_NAME, ARMARX_VIEWER_NAME));

        // ensure that all drawings are protected with a mutex
        viewer->setMutex3D(mutex3d);

        return viewer;
    }



    void ArmarXMainWindow::pluginDialog()
    {
        QFileDialog dialog(this);
        dialog.setFileMode(QFileDialog::ExistingFiles);
        dialog.setNameFilter(tr("Libraries (*.dll *.so *.dylib)"));
        QStringList fileNames;

        if (dialog.exec())
        {
            fileNames = dialog.selectedFiles();
        }

        foreach (QString filePath, fileNames)
        {
            pluginCache.removePluginFromCache(filePath);
            ARMARX_INFO << filePath.toStdString() << armarx::flush;
            loadPlugin(filePath);
        }
    }

    void ArmarXMainWindow::loadPlugins(const QStringList& fileNames)
    {
        QStringList guiFiles;

        for (int i = 0; i < fileNames.size(); i++)
        {
            QString fileName = fileNames.at(i);

            // only try to load files ending with GuiPlugin.so and GuiPlugin.dll
            if (!fileName.endsWith("GuiPlugin.so") && !fileName.endsWith("GuiPlugin.dll") && !fileName.endsWith("GuiPlugin.dylib"))
            {
                continue;
            }

            guiFiles.push_back(fileName);
        }
        auto start = IceUtil::Time::now();
        for (int i = 0; i < guiFiles.size(); i++)
        {
            QString fileName = guiFiles.at(i);
            if ((IceUtil::Time::now() - start).toSeconds() >= 2)
            {
                splashSceen->show();
                splashSceen->showMessage(splashscreenPrefix + fileName,
                                         Qt::AlignJustify | Qt::AlignBottom);
                qApp->processEvents();

            }
            pluginCache.cachePlugin(fileName);
        }
        updateAvailableWidgetList();
    }

    QAction* ArmarXMainWindow::addActionToToolBar(QAction* action, bool allowText)
    {
        QToolButton* button = new QToolButton(ui->toolBar);

        std::optional<QSize> textSize;
        if (allowText)
        {
            button->setToolButtonStyle(Qt::ToolButtonStyle::ToolButtonTextBesideIcon);
            button->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Fixed);

            // Get the width of the button when just showing text.
            button->setText(action->text());
            textSize = button->sizeHint();
        }

        button->setDefaultAction(action);
        if (not action->icon().isNull())
        {
            const QSize maxSize {256, 24};
            const QSize iconSize = action->icon().actualSize(maxSize);
            // Setting the action has already set the icon, so this line is unnecessary.
            // button->setIcon(action->icon());
            button->setIconSize(iconSize);
            button->setFixedSize(textSize.has_value()
                                 ? QSize{textSize->width() + iconSize.width(), iconSize.height()}
                                 : iconSize);
        }
        return ui->toolBar->addWidget(button);
    }

    QString ArmarXMainWindow::getLibraryPathFromPackage(const QString& packageName)
    {
        CMakePackageFinder packageFinder(packageName.toStdString());
        std::string libPaths = packageFinder.getLibraryPaths();
        QString dir = QString::fromStdString(libPaths);
        dir = QDir::cleanPath(dir);

        return dir;
    }

    QStringList ArmarXMainWindow::getFavoriteWidgets()
    {
        auto widgetUsageHistory = mainSettings.value("WidgetUsageHistory").toList();
        std::map<QString, int> widgetUsage;
        for (auto& widgetName : widgetUsageHistory)
        {
            if (widgetUsage.count(widgetName.toString()) == 0)
            {
                widgetUsage[widgetName.toString()] = 1;
            }
            else
            {
                widgetUsage[widgetName.toString()] += 1;
            }
        }
        std::multimap<int, QString> ranking;
        for (auto it = widgetUsage.begin(); it != widgetUsage.end(); it++)
        {
            ranking.insert(std::make_pair(it->second, it->first));
        }
        int i = 0;
        const int favCount =  mainSettings.value("FavoriteWidgetCount", 6).toInt();
        QStringList favoriteWidgetNames;
        for (auto it = ranking.rbegin(); it != ranking.rend() && i < favCount ; it++)
        {
            auto& widgetName = it->second;
            if (!mainWidgets.contains(widgetName))
            {
                favoriteWidgetNames << widgetName;
                i++;
            }
        }
        return favoriteWidgetNames;
    }

    void ArmarXMainWindow::loadPluginsFromPackage(const QString& packageName)
    {
        QSet<QString> pluginDirs;
        splashSceen->showMessage(splashscreenPrefix + packageName,
                                 Qt::AlignJustify | Qt::AlignBottom);
        qApp->processEvents();

        ARMARX_INFO << "Looking for gui plugins in package " << packageName;
        QString dir = getLibraryPathFromPackage(packageName);
        pluginDirs.insert(dir);
        loadedPackages << packageName;
        loadPlugins(pluginDirs, false);
    }


    void ArmarXMainWindow::loadPlugins(const QString& pluginDir, bool searchRecursive)
    {
        QSet<QString> dirs;
        dirs.insert(pluginDir);
        loadPlugins(dirs, searchRecursive);
    }


    void ArmarXMainWindow::loadPlugins(const QSet<QString>& pluginDirs, bool searchRecursive)
    {
        // only search paths ending with lib
        QStringList filters;
        filters << "*.so" << "*.dll" << "*.dylib";
        QList<QDir> directoriesToCheck;
        QStringList allFileNames;

        for (const auto& pluginDir : pluginDirs)
        {
            directoriesToCheck.push_back(QDir(pluginDir));

            while (directoriesToCheck.size() > 0)
            {
                ARMARX_INFO_S << "Checking dir : " + directoriesToCheck.front().absolutePath() << std::endl;
                splashSceen->showMessage(splashscreenPrefix + directoriesToCheck.front().absolutePath(),
                                         Qt::AlignJustify | Qt::AlignBottom);
                qApp->processEvents();
                QDir currentPluginDir = directoriesToCheck.front();
                directoriesToCheck.pop_front();
                QString curPath = currentPluginDir.path();

                if (curPath.length() == 0)
                {
                    continue;
                }

                QStringList fileNames = currentPluginDir.entryList(filters, QDir::Files);

                for (auto& fileName : fileNames)
                {
                    fileName = currentPluginDir.absoluteFilePath(fileName);
                }

                allFileNames.append(fileNames);

                if (searchRecursive)
                {
                    QDirIterator directoryIterator(currentPluginDir.absolutePath(), QStringList(), QDir::Dirs | QDir::NoDotAndDotDot, QDirIterator::Subdirectories);

                    while (directoryIterator.hasNext())
                    {
                        directoryIterator.next();
                        directoriesToCheck.push_back(QDir(directoryIterator.filePath()));

                    }
                }

            }
        }

        loadPlugins(allFileNames);
    }



    bool ArmarXMainWindow::existsWidget(const QString& widgetName)
    {
        auto list = pluginCache.getAvailableWidgetNames();
        return list.contains(widgetName);
    }

    void ArmarXMainWindow::loadPlugin(QString filePath)
    {
        QFileInfo pathInfo(filePath);
        QString fileName(pathInfo.fileName());
        pluginCache.cachePlugin(filePath);
        ARMARX_INFO << "Plugin Path: " << pathInfo.absoluteFilePath().toStdString() << armarx::flush;
        updateAvailableWidgetList();
        //    if (!loadedPluginFilepaths.contains(pathInfo.absoluteFilePath()))
        //    {
        //        QPluginLoader loader(filePath);
        //        //        ARMARX_INFO << "loader created" << armarx::flush;
        //        instantiatePlugin(loader);
        //    }
        //    else
        //    {
        //        ARMARX_INFO << "Plugin already loaded" << armarx::flush;
        //    }
    }




    void ArmarXMainWindow::closeEvent(QCloseEvent* event)
    {
        emit closeRequest();
        event->accept();

    }



    void ArmarXMainWindow::closeAllWidgets()
    {


        OpenWidgetMap listOpenWidgetsTemp = listOpenWidgets;
        OpenWidgetMap::iterator it = listOpenWidgetsTemp.begin();

        for (; it != listOpenWidgetsTemp.end() ; ++it)
        {
            ARMARX_INFO << "Closing widget " << it.key() << " size: "  << listOpenWidgetsTemp.size();
            QDockWidget* w = it.value().first;

            if (w)
            {
                if (!w->close())
                {
                    ARMARX_WARNING << "Could not close widget!" << std::endl;
                }
            }
        }

        //    if (viewerDockWidget)
        //        removeViewerWidget(viewerDockWidget);

        // causes segfault during redraw of 3d scene ?!
        //QApplication::instance()->processEvents();

        ARMARX_VERBOSE << "Closed all widgets";
        listOpenWidgets.clear();

        this->configFilepath = "";
        appendFileToWindowTitle(configFilepath);
    }

    void ArmarXMainWindow::closeAllWidgetsWithDialog()
    {
        QMessageBox dialog(QMessageBox::Question, tr("Closing all widgets"), tr("Do you really want to close all widgets?"));
        dialog.setStandardButtons(QMessageBox::No | QMessageBox::Yes);
        dialog.setDefaultButton(QMessageBox::Yes);

        if (dialog.exec() == QMessageBox::Yes)
        {
            closeAllWidgets();
        }
    }



    void ArmarXMainWindow::addToRecentlyOpenedFileList(QString configFilepath)
    {

        QDir path(configFilepath);
        configFilepath = path.absolutePath();

        QStringList recentlyFiles = mainSettings.value("RecentlyFiles").toStringList();
        recentlyFiles.removeAll(configFilepath);
        recentlyFiles.push_front(configFilepath);

        if (recentlyFiles.size() > 10)
        {
            recentlyFiles.pop_back();
        }

        mainSettings.setValue("RecentlyFiles", recentlyFiles);
    }


    std::vector<std::string> ArmarXMainWindow::getDefaultPackageNames()
    {
        return defaultPackageNames;
    }


    Viewer3DWidgetPtr ArmarXMainWindow::getViewerWidget()
    {
        if (listOpenWidgets.find(ARMARX_VIEWER_NAME) != listOpenWidgets.end())
        {
            return Viewer3DWidgetPtr::dynamicCast(listOpenWidgets.find(ARMARX_VIEWER_NAME).value().second);
        }
        else
        {
            return NULL;
        }
    }

    void ArmarXMainWindow::loadGuiConfig(QString configFilepath, bool showAsOpenGuiConfig)
    {

        if (configFilepath.length() == 0)
        {
            QFileDialog dialog(this);
            dialog.setFileMode(QFileDialog::ExistingFile);
            dialog.setAcceptMode(QFileDialog::AcceptOpen);
            QStringList nameFilters;
            nameFilters << "ArmarX Gui-Files (*.armarxgui)"
                        << "All Files (*.*)";
            dialog.setNameFilters(nameFilters);

            if (dialog.exec())
            {
                if (dialog.selectedFiles().size() == 0)
                {
                    return;
                }

                configFilepath = dialog.selectedFiles()[0];
            }
            else
            {
                ARMARX_INFO << "load config dialog canceled" << std::endl;
                return;
            }
        }


        closeAllWidgets();

        QDir dir(configFilepath);
        dir.makeAbsolute();
        configFilepath = dir.absolutePath();

        if (!QFile::exists(configFilepath))
        {
            ArmarXWidgetController::showMessageBox("GUI configuration file '" + configFilepath + "' does not exist.");
            return;
        }

        ARMARX_VERBOSE << "Loading config file: " << configFilepath;

        if (showAsOpenGuiConfig)
        {
            addToRecentlyOpenedFileList(configFilepath);
            updateRecentlyOpenedFileList();
        }

        QSettings settings(configFilepath, QSettings::IniFormat);


        if (settings.allKeys().size() == 0)
        {
            ArmarXWidgetController::showMessageBox("Could not find any settings in '" + configFilepath + "'!");
            return;
        }

        QStringList packagesToLoad = settings.value("loadedPackages").toStringList();
        foreach (QString package, packagesToLoad)
        {
            loadPluginsFromPackage(package);
        }

        QStringList widgetNames = settings.value("WidgetCustomNames").toStringList();
        foreach (QString widgetCustomName, widgetNames)
        {
            settings.beginGroup(widgetCustomName);
            //        if(widgetCustomName != ARMARX_VIEWER_NAME || !view3D)
            {
                ARMARX_INFO << "Creating widget " << settings.value("WidgetBaseName").toString() << "," << widgetCustomName;
                ArmarXWidgetControllerPtr widget = createArmarXWidget(settings.value("WidgetBaseName").toString(), widgetCustomName, &settings);
                //            if(widgetCustomName == ARMARX_VIEWER_NAME && !view3D)
                //                view3D = Viewer3DWidgetPtr::dynamicCast(widget);
            }
            qApp->processEvents();
            //        widget->getWidget()->resize(settings.value("widgetWidth").toInt(), settings.value("widgetHeight").toInt());<-- not working
            settings.endGroup();
        }
        auto geometryByteArray = settings.value("MainWindowGeometry").toByteArray();
        if (geometryByteArray.size() > 0)
        {
            // for legacy
            restoreGeometry(geometryByteArray);
        }
        else
        {
            QRect geometry = settings.value("MainWindowGeometry").toRect();
            setGeometry(geometry);
        }

        if (!restoreState(settings.value("DockWidgetsState").toByteArray()))
        {
            ARMARX_WARNING << "Failed to restore state";
        }

        foreach (QString widgetCustomName, widgetNames)
        {
            settings.beginGroup(widgetCustomName);
            OpenWidgetMap::iterator it = listOpenWidgets.find(widgetCustomName);

            if (it != listOpenWidgets.end())
            {
                it->first->resize(settings.value("widgetWidth").toInt(), settings.value("widgetHeight").toInt());
            }

            settings.endGroup();
        }
        if (showAsOpenGuiConfig)
        {
            this->configFilepath = configFilepath;
            appendFileToWindowTitle(configFilepath);
        }
        updateOpenWidgetList();
        QFileInfo file(configFilepath);
        statusBar()->showMessage("'" + file.fileName() + "' loaded.", 10000);

    }


    void ArmarXMainWindow::saveGuiConfig()
    {
        if (configFilepath.length() == 0)
        {
            QFileDialog dialog(this);
            dialog.setFileMode(QFileDialog::AnyFile);
            dialog.setAcceptMode(QFileDialog::AcceptSave);
            QStringList nameFilters;
            nameFilters << "ArmarX Gui-Files (*.armarxgui)"
                        << "All Files (*.*)";
            dialog.setNameFilters(nameFilters);

            if (dialog.exec())
            {
                if (dialog.selectedFiles().size() == 0)
                {
                    return;
                }

                QString file = dialog.selectedFiles()[0];
                QFileInfo fileInfo(file);

                if (fileInfo.suffix().isEmpty())
                {
                    file += ".armarxgui";
                }

                configFilepath = file;
            }
            else
            {
                return;
            }

        }


        QSettings settings(configFilepath, QSettings::IniFormat);
        settings.clear();
        // first make all libpaths relative
        //    for (int i = 0; i < loadedPluginFilepaths.size(); ++i) {
        //        loadedPluginFilepaths[i] = QString::fromStdString(loadedPluginFilepaths[i].toStdString());
        //    }
        settings.setValue("loadedPackages", loadedPackages);
        settings.setValue("MainWindowGeometry", geometry());
        settings.setValue("DockWidgetsState", saveState());
        QStringList widgetCustomNames;
        OpenWidgetMap::iterator it = listOpenWidgets.begin();

        for (; it != listOpenWidgets.end(); it++)
        {
            QString prefix = it.value().first->objectName();
            ARMARX_VERBOSE << "Saving widget: " << prefix.toStdString();
            settings.beginGroup(prefix);
            ArmarXWidgetControllerPtr w = ArmarXWidgetControllerPtr::dynamicCast(it.value().second);

            if (w /*&& prefix != ARMARX_VIEWER_NAME*/)
            {
                settings.setValue("WidgetBaseName", w->getWidgetName());
                settings.setValue("widgetWidth", w->getWidget()->width());
                settings.setValue("widgetHeight", w->getWidget()->height());
                w->saveSettings(&settings);
                widgetCustomNames.push_back(prefix);
            }
            else
            {
                ARMARX_WARNING << "Could not cast widget: " << prefix;
            }

            settings.endGroup();
        }

        settings.setValue("WidgetCustomNames", widgetCustomNames);
        ARMARX_INFO << "Saved config to " << configFilepath;
        appendFileToWindowTitle(configFilepath);

        // update recently opened file list
        addToRecentlyOpenedFileList(configFilepath);
        updateRecentlyOpenedFileList();
        QFileInfo file(configFilepath);
        statusBar()->showMessage("'" + file.fileName() + "' saved.", 10000);

    }

    void ArmarXMainWindow::saveGuiConfigAs()
    {
        configFilepath.clear();
        saveGuiConfig();
    }

    void ArmarXMainWindow::appendFileToWindowTitle(const QString& filepath)
    {
        if (filepath.length() > 0)
        {
            QDir path(filepath);
            std::filesystem::path file(filepath.toStdString());
            path.cdUp();

            setWindowTitle(guiWindowBaseName + " - " + QString::fromStdString(file.filename().string()) + " in " + path.absolutePath());
        }
        else
        {
            setWindowTitle(guiWindowBaseName);
        }
    }



    ArmarXWidgetControllerPtr ArmarXMainWindow::createArmarXWidget(QString widgetName, QString customInstanceName, QSettings* settings)
    {

        if (listOpenWidgets.find("Dock" + customInstanceName) != listOpenWidgets.end())
        {
            ARMARX_WARNING << "A Widget with that name already exists";
            return NULL;
        }

        // HACK: Creating an OpenGL context fails after libPointCloudViewerGuiPlugin.so is loaded.
        // Therefore, we need to check before calling getWidgetCreator since this loads
        // the shared object. If we do not do this here, then the following error occurs:
        // "Can't set up a valid opengl canvas something is seriously wrong with the system"
        if (widgetName == "VisionX.PointCloudViewer")
        {
            ARMARX_INFO << "Creating SoQtExaminerViewer for " << widgetName.toStdString();
            SoQtExaminerViewer(nullptr, "", TRUE, SoQtExaminerViewer::BUILD_NONE);
        }

        auto creator = pluginCache.getWidgetCreator(widgetName);

        if (!creator)
        {
            ArmarXWidgetController::showMessageBox("Could not find widget with Name: " + widgetName);
            return NULL;
        }
        auto widgetUsage = mainSettings.value("WidgetUsageHistory").toList();
        widgetUsage.push_back(widgetName);
        if (widgetUsage.size() > widgetUsageHistoryLength)
        {
            widgetUsage.pop_front();
        }
        mainSettings.setValue("WidgetUsageHistory", widgetUsage);


        ArmarXWidgetControllerPtr w = creator->createInstance();

        w->setMainWindow(this);
        w->setInstanceName(customInstanceName);
        w->setTipDialog(tipDialog);

        if (settings)
        {
            w->loadSettings(settings);
        }
        else if (w->getConfigDialog(this))
        {

            ManagedIceObjectPtr comp = ManagedIceObjectPtr::dynamicCast(w->getConfigDialog().data());

            if (comp)
            {
                comp->__setNoDelete(true);
                registry->addObject(comp, false);
            }

            w->getConfigDialog()->setModal(true);
            connect(w->getConfigDialog().data(), SIGNAL(accepted()), w.get(), SLOT(configAccepted()));
            connect(w.get(), SIGNAL(configAccepted(ArmarXWidgetControllerPtr)), this, SLOT(addArmarXWidget(ArmarXWidgetControllerPtr)));
            connect(w->getConfigDialog().data(), SIGNAL(rejected()), w.get(), SLOT(configRejected()));
            connect(w.get(), SIGNAL(configRejected(ArmarXWidgetControllerPtr)), this, SLOT(addArmarXWidgetCanceled(ArmarXWidgetControllerPtr)));

            pendingWidgets.push_back(w);
            w->getConfigDialog()->show();
            w->getConfigDialog()->raise();
            w->getConfigDialog()->activateWindow();
            return w;
        }

        // if we are loading a gui config we do not want to create the viewer widget
        // it will be created later during the loading process
        addArmarXWidget(w, !settings);
        return w;
    }

    ArmarXWidgetControllerPtr ArmarXMainWindow::addArmarXWidget(ArmarXWidgetControllerPtr newWidgetController, bool createViewerWidget)
    {
        for (unsigned int i = 0; i < pendingWidgets.size(); ++i)
        {
            if (pendingWidgets[i].get() == newWidgetController.get())
            {
                pendingWidgets.erase(pendingWidgets.begin() + i);
                break;
            }
        }

        QString widgetName = newWidgetController->getWidgetName();
        QString customInstanceName = newWidgetController->getInstanceName();

        if (listOpenWidgets.find(customInstanceName) != listOpenWidgets.end())
        {
            return NULL;
        }

        ARMARX_VERBOSE << "Adding new widget: " << customInstanceName;
        assert(widgetName == newWidgetController->getWidgetName());
        ArmarXDockWidget* dockWidget = new ArmarXDockWidget(customInstanceName, newWidgetController);
        //w->connectDestroySlot();
        connect(dockWidget, SIGNAL(destroyed(QObject*)), this, SLOT(removeArmarXWidget(QObject*)));

        dockWidget->setArmarXWidget(newWidgetController->getWidget());
        dockWidget->setObjectName(customInstanceName);
        StatusDockWidgetTitleBar* customTitlebar = new StatusDockWidgetTitleBar(dockWidget, this);
        dockWidget->setTitleBarWidget(customTitlebar);
        customTitlebar->addCustomWidget(newWidgetController->getCustomTitlebarWidget(dockWidget));

        if (listOpenWidgets.find(dockWidget->objectName()) != listOpenWidgets.end())
        {
            ArmarXWidgetController::showMessageBox("A Widget with the title '" + customInstanceName + "' already exists.");
            delete dockWidget;
            return ArmarXWidgetControllerPtr();
        }

        QDockWidget* biggestOpenDockWidget = getBiggestOpenWidget();
        listOpenWidgets[customInstanceName] = qMakePair<QDockWidget*, ArmarXWidgetControllerPtr>(dockWidget, newWidgetController);
        addDockWidget(Qt::RightDockWidgetArea, dockWidget);
        newWidgetController->postDocking();

        QSize widgetStartSize = newWidgetController->getWidget()->size();
        if (widgetStartSize.width() * widgetStartSize.height() >= 400 * 400)
            if (biggestOpenDockWidget)
            {
                tabifyDockWidget(biggestOpenDockWidget, dockWidget);
            }

        dockWidget->show();
        dockWidget->raise();

        updateOpenWidgetList();

        // set it even if no 3d scene may be currently available
        newWidgetController->setMutex3D(mutex3d);

        QApplication::instance()->processEvents(); // process events so that the widgets are completetly intialized?
        ManagedIceObjectPtr comp = ManagedIceObjectPtr::dynamicCast(newWidgetController);

        if (comp)
        {
            registry->addObject(comp, false);
            ARMARX_INFO << "Waiting for widget initialization " << widgetName << armarx::flush;
            int timeoutMs = 30000;

            if (!comp->getObjectScheduler()->waitForObjectState(eManagedIceObjectInitialized, timeoutMs))
            {
                ARMARX_WARNING << "Widget " << customInstanceName << " was not connected to Ice after " << timeoutMs / 1000 << " seconds" << std::endl;
            }
        }

        ARMARX_INFO << "Widget initialization done for " << widgetName << armarx::flush;



        SoNode* node = newWidgetController->getScene();

        if (node)
        {
            Viewer3DWidgetPtr viewerInstance;
            ARMARX_INFO << "3D Scene available in widget " << widgetName << armarx::flush;
            OpenWidgetMap::Iterator it = listOpenWidgets.begin();

            for (; it != listOpenWidgets.end(); it++)
            {
                viewerInstance = Viewer3DWidgetPtr::dynamicCast(it.value().second);

                if (viewerInstance)
                {
                    break;
                }
            }

            if (!viewerInstance && createViewerWidget)
                //        if(listOpenWidgets.find(ARMARX_VIEWER_NAME) == listOpenWidgets.end())
            {
                ARMARX_INFO << "Setting up CoinQt Viewer";
                viewerInstance = setupViewerWidget();
            }

            SoSeparator* sep = new SoSeparator;
            sep->ref();

            SoPerspectiveCamera* camera = new SoPerspectiveCamera;

            sep->addChild(camera);
            sep->addChild(node);

            viewer3DMap[customInstanceName] = {sep, newWidgetController->getSceneConfigDialog()};
            emit updateSceneList(viewer3DMap);
        }


        return newWidgetController;
    }

    void ArmarXMainWindow::addArmarXWidgetCanceled(ArmarXWidgetControllerPtr newWidgetController)
    {
        for (unsigned int i = 0; i < pendingWidgets.size(); ++i)
        {
            if (pendingWidgets[i].get() == newWidgetController.get())
            {
                pendingWidgets.erase(pendingWidgets.begin() + i);
                break;
            }
        }
    }

    void ArmarXMainWindow::removeArmarXWidget(QObject* widget)
    {
        ARMARX_DEBUG << "removing widgetname: " << widget->objectName();
        QDockWidget* dockWidget = qobject_cast<QDockWidget*>(widget);


        OpenWidgetMap::iterator it = listOpenWidgets.begin();

        for (; it != listOpenWidgets.end(); it++)
        {
            if (widget == it.value().first)
            {

                QString widgetName;

                if (it.value().second)
                {
                    widgetName = it.value().second->getInstanceName();

                }
                else
                {
                    if (it.value().first)
                    {
                        widgetName = it.value().first->objectName();
                    }
                }


                listOpenWidgets.erase(it);
                updateOpenWidgetList();


                if (viewer3DMap.contains(widgetName) /*&& view3D->cb3DViewers*/)
                {
                    auto& node = viewer3DMap[widgetName].node;

                    if (node)
                    {
                        node->unref();
                    }

                    ARMARX_DEBUG << "Removing from 3D list: " << widgetName;
                    viewer3DMap.remove(widgetName);
                    emit updateSceneList(viewer3DMap);
                }

                break;
            }
        }

        removeDockWidget(dockWidget);
    }

    QPointer<QDockWidget> ArmarXMainWindow::getBiggestOpenWidget()
    {
        OpenWidgetMap::iterator it = listOpenWidgets.begin();
        QPointer<QDockWidget> biggestWidget = NULL;

        for (; it != listOpenWidgets.end(); it++)
        {
            QDockWidget* dockWidget = it.value().first;

            if (!biggestWidget || dockWidget->size().width() * dockWidget->size().height() > biggestWidget->size().width() * biggestWidget->size().height())
            {
                biggestWidget = dockWidget;
            }
        }

        return biggestWidget;
    }

    QMenu* ArmarXMainWindow::getCategoryMenu(const std::string& categoryString, QMenu* menu, QIcon categoryIcon)
    {
        Ice::StringSeq items = simox::alg::split(categoryString, ".");

        if (items.size() <= 1)
        {
            return menu;
        }

        auto actions = menu->actions();

        for (QAction* action : actions)
        {
            if (action->text().toStdString() == *items.begin())
            {
                items.erase(items.begin(), items.begin() + 1);
                std::string rest = simox::alg::join(items, ".");

                if (!action->menu())
                {
                    action->setMenu(new QMenu(QString::fromStdString(*items.begin()), this));
                }

                if (action->icon().isNull() && !categoryIcon.isNull())
                {
                    action->setIcon(categoryIcon);
                    action->setIconVisibleInMenu(true);
                }

                return getCategoryMenu(rest, action->menu(), categoryIcon);
            }
        }

        return menu->addMenu(QString::fromStdString(*items.begin()));
    }

    void ArmarXMainWindow::updateAvailableWidgetList()
    {
        ui->menuAdd_Widget->clear();

        searchField = new QLineEdit(ui->menuAdd_Widget);
        searchField->setToolTip("Search and add a new widget from all loaded ArmarX Gui Plugins");
        searchField->setMaximumWidth(250);
        searchField->setPlaceholderText("Widget Search");

        QStringList widgetNameList;

        ui->toolBar->clear();

        // EmergencyStop
        ui->toolBar->addWidget(emergencyStopWidget->getButtonWidget());

        ui->toolBar->setIconSize(QSize(256, 24));

        actionList.clear();
        for (std::pair<QString, ArmarXWidgetInfoPtr> pair : pluginCache.getAvailableWidgets())
        {

            QString fullWidgetName = pair.first;
            QString widgetName = pair.first;
            widgetName = widgetName.remove(0, widgetName.lastIndexOf(".") + 1);
            widgetNameList << pair.first;
            QIcon categoryIcon;
            if (pair.second)
            {
                categoryIcon = pair.second->getCategoryIcon();
            }
            else
            {
                if (QFile::exists(PluginCache::GetCategoryIconPath(pair.first)))
                {
                    categoryIcon = QIcon(PluginCache::GetCategoryIconPath(pair.first));
                }
            }
            auto menu = getCategoryMenu(pair.first.toStdString(), ui->menuAdd_Widget, categoryIcon);
            AddArmarXWidgetAction* action = new AddArmarXWidgetAction(widgetName, menu, this);
            QIcon widgetIcon;
            if (pair.second)
            {
                widgetIcon = pair.second->getIcon();
            }
            else
            {
                if (QFile::exists(PluginCache::GetIconPath(pair.first)))
                {
                    widgetIcon = QIcon(PluginCache::GetIconPath(pair.first));
                }
            }
            action->setIcon(widgetIcon);
            action->setIconVisibleInMenu(true);

            action->setData(pair.first);
            menu->addAction(action);

            if (mainWidgets.contains(fullWidgetName))
            {
                bool allowText = false;
                addActionToToolBar(action, allowText);
            }
            actionList[fullWidgetName] = action;
            connect(action, SIGNAL(triggered()), action, SLOT(addArmarXWidget()), Qt::UniqueConnection);
            connect(action, SIGNAL(clicked(QString, QString)), this, SLOT(createArmarXWidget(QString, QString)), Qt::UniqueConnection);

        }

        addArVizGodotIcon();

        AddArmarXWidgetAction* completerAction = new AddArmarXWidgetAction("", ui->menuAdd_Widget, this);
        InfixCompleter* completer = new InfixCompleter(widgetNameList, searchField);
        connect(searchField, SIGNAL(textEdited(QString)), completer, SLOT(setCompletionInfix(QString)));

        //    connect(completer, SIGNAL(activated(QString)), completerAction, SLOT(triggered(QString)));
        connect(completerAction, SIGNAL(accepted()), searchField, SLOT(clear()));
        connect(completerAction, SIGNAL(clicked(QString, QString)), this, SLOT(createArmarXWidget(QString, QString)), Qt::UniqueConnection);
        searchField->setCompleter(completer);

        ui->toolBar->addSeparator();
        ui->toolBar->addWidget(searchField);
        QIcon icon;
        icon.addFile(QString::fromUtf8("://icons/edit-add.ico"), QSize(), QIcon::Normal, QIcon::Off);
        openWidgetButton = new QToolButton(this);
        openWidgetButton->setEnabled(false);
        openWidgetButton->setIcon(icon);
        openWidgetButton->setFixedSize({24, 24});
        openWidgetButton->setToolTip("Open selected widget");
        connect(openWidgetButton, SIGNAL(clicked()), this, SLOT(openWidgetButtonClicked()), Qt::UniqueConnection);
        ui->toolBar->addWidget(openWidgetButton);
        connect(searchField, SIGNAL(textChanged(QString)), this, SLOT(updateOpenWidgetButtonStatus(QString)), Qt::UniqueConnection);

        connect(searchField, SIGNAL(returnPressed()), this, SLOT(openWidgetButtonClicked()), Qt::UniqueConnection);

        {
            ui->toolBar->addSeparator();
            favoritesLabel = new QLabel("Favorites:");
            favoritesLabel->setToolTip(
                "The favorites are generated from the usage frequency over the last X widget creations."
                " Rightclick to change the number of displayed favorites"
            );
            ui->toolBar->addWidget(favoritesLabel);
            //add menu
            favoritesLabel->setContextMenuPolicy(Qt::CustomContextMenu);
            connect(favoritesLabel, SIGNAL(customContextMenuRequested(const QPoint&)),
                    this, SLOT(onContextMenuFavoritesRequested(const QPoint&)));
        }
        updatefavoriteActions();
    }

    void ArmarXMainWindow::updateOpenWidgetList()
    {
        ui->menuWindows->clear();
        QAction* closeAllAction = new QAction("Close all widgets", ui->menuWindows);
        connect(closeAllAction, SIGNAL(triggered()), this, SLOT(closeAllWidgetsWithDialog()));
        ui->menuWindows->addAction(closeAllAction);
        ui->menuWindows->addSeparator();

        OpenWidgetMap::iterator it = listOpenWidgets.begin();

        for (; it != listOpenWidgets.end(); it++)
        {
            QDockWidget* dockWidget = it.value().first;
            QAction* action = new QAction(dockWidget ->objectName(), ui->menuWindows);
            action->setCheckable(true);
            action->setChecked(!dockWidget ->isHidden());

            connect(action, SIGNAL(toggled(bool)), dockWidget, SLOT(setVisible(bool)));
            //        connect(object, SIGNAL(visibilityChanged(bool)), action, SLOT(setChecked(bool)));
            ui->menuWindows->addAction(action);

        }
    }

    void ArmarXMainWindow::addArVizGodotIcon()
    {
        const char* path = std::getenv("arviz_godot_DIR");
        if (path == nullptr)
        {
            return;
        }

        std::filesystem::path buildDirectory(path);
        std::filesystem::path mainDirectory = buildDirectory.parent_path();

        std::filesystem::path binaryPath = buildDirectory / "bin" / "arviz-godot";
        std::filesystem::path iconPath = mainDirectory / "source" / "arviz_godot" / "project" / "icon.png";

        if (not std::filesystem::exists(binaryPath) or not std::filesystem::exists(iconPath))
        {
            return;
        }

        QIcon icon(iconPath.c_str());
        QString name("ArViz Godot");

        QAction* action = new QAction(icon, name, this);

        bool allowText = false;
        addActionToToolBar(action, allowText);

        auto slot = [action, this, binaryPath]()
        {
            if (not std::filesystem::exists(binaryPath))
            {
                QMessageBox errorBox;
                errorBox.critical(nullptr, "Error", "The ArViz Godot executable is no longer available.");

                ARMARX_ERROR << "Failed to find ArViz Godot";

                return;
            }

            pid_t pid = fork();

            if (pid == -1)
            {
                ARMARX_ERROR << "Failed to start ArViz Godot";
                return;
            }

            if (pid != 0)
            {
                return;
            }

            int null = open("/dev/null", O_WRONLY);
            dup2(null, STDOUT_FILENO);
            dup2(null, STDERR_FILENO);

            execl(binaryPath.c_str(), "arviz-godot", nullptr);
            exit(-1);
        };

        connect(action, &QAction::triggered, this, slot, Qt::UniqueConnection);
    }

    void ArmarXMainWindow::updateRecentlyOpenedFileList()
    {
        QStringList recentlyFiles = mainSettings.value("RecentlyFiles").toStringList();
        QMenu* menu = ui->menuRecently_Opened_Files;
        auto actions = menu->actions();

        for (QAction* action : actions)
        {
            if (!action->isCheckable() && !action->isSeparator())
            {
                menu->removeAction(action);
            }
        }

        foreach (QString file, recentlyFiles)
        {
            OpenRecentlyOpenedFileAction* action = new OpenRecentlyOpenedFileAction(file, menu, this);

            action->setCheckable(false);
            connect(action, SIGNAL(triggered()), action, SLOT(openFile()));
            menu->addAction(action);
        }
    }

    void ArmarXMainWindow::updateStatusOfOpenWidgets()
    {
        OpenWidgetMap::iterator it = listOpenWidgets.begin();

        for (; it != listOpenWidgets.end(); it++)
        {
            QDockWidget* dockWidget = it.value().first;
            ManagedIceObjectPtr comp = ManagedIceObjectPtr::dynamicCast(it.value().second);
            StatusDockWidgetTitleBar* titlebar = qobject_cast<StatusDockWidgetTitleBar*>(dockWidget->titleBarWidget());

            if (titlebar && comp)
            {

                ManagedIceObjectConnectivity con = comp->getConnectivity();
                QStringList dependencies;

                for (DependencyMap::iterator i = con.dependencies.begin(); i != con.dependencies.end(); i++)
                {
                    ManagedIceObjectDependencyBasePtr& dep = i->second;

                    if (!dep->getResolved() || comp->getState() == eManagedIceObjectStarted)
                    {
                        dependencies << QString::fromStdString(dep->getName());
                    }
                }

                titlebar->changeStatus((ManagedIceObjectState)comp->getState(), dependencies);
            }
        }
    }

    void ArmarXMainWindow::enterFullscreenMode()
    {
        showFullScreen();
        ui->toolBar->hide();
        ui->menubar->hide();
        ui->statusbar->hide();

        // Add actions for leaving fullscreen mode
        leaveFullScreenActionF11 = new QAction(this);
        leaveFullScreenActionF11->setShortcut(Qt::Key_F11);
        connect(leaveFullScreenActionF11, SIGNAL(triggered()), this, SLOT(leaveFullscreenMode()));
        addAction(leaveFullScreenActionF11);

        leaveFullScreenActionEsc = new QAction(this);
        leaveFullScreenActionEsc->setShortcut(Qt::Key_Escape);
        connect(leaveFullScreenActionEsc, SIGNAL(triggered()), this, SLOT(leaveFullscreenMode()));
        addAction(leaveFullScreenActionEsc);
    }

    void ArmarXMainWindow::leaveFullscreenMode()
    {
        // Remove actions for leaving fullscreen mode (menu will be available again)
        removeAction(leaveFullScreenActionF11);
        leaveFullScreenActionF11 = nullptr;

        removeAction(leaveFullScreenActionEsc);
        leaveFullScreenActionEsc = nullptr;

        showNormal();
        ui->toolBar->show();
        ui->menubar->show();
        ui->statusbar->show();
    }

    void ArmarXMainWindow::toggleWidgetLock()
    {
        QList<ArmarXDockWidget*> dockWidgets = findChildren<ArmarXDockWidget*>();
        if (widgetsLocked)
        {
            ARMARX_INFO << "Unlocking widgets";
            for (auto& widget : dockWidgets)
            {
                widget->unlockWidget();
            }
        }
        else
        {
            ARMARX_INFO << "Locking widgets";
            for (auto& widget : dockWidgets)
            {
                widget->lockWidget();
            }
        }

        widgetsLocked = !widgetsLocked;
    }



    /////////////////////////////////////////////////////////////////////////
    //// Additional Helper Classes
    /////////////////////////////////////////////////////////////////////////

    AddArmarXWidgetAction::AddArmarXWidgetAction(QString widgetName, QObject* parent, ArmarXMainWindow* mainGui)
        : QAction(widgetName, parent),
          mainGui(mainGui)
    {
        dialog = new WidgetNameDialog(widgetName, mainGui);
        connect(dialog, SIGNAL(accepted()), this, SLOT(dialogAccepted()));
    }

    AddArmarXWidgetAction::~AddArmarXWidgetAction()
    {
        delete dialog;
    }

    void AddArmarXWidgetAction::triggered(QString widgetName)
    {
        if (widgetName.isEmpty())
        {
            QLineEdit* edit = qobject_cast<QLineEdit*>(sender());
            if (edit)
            {
                widgetName = edit->text();
            }
            else
            {
                return;
            }
        }
        ARMARX_INFO_S << "Setting action name to " << widgetName;
        setText(widgetName);
        setData(widgetName);
        addArmarXWidget();
    }

    void AddArmarXWidgetAction::addArmarXWidget()
    {
        dialog->editWidgetName->setText("");// reset so that TextChanged is called for sure
        dialog->editWidgetName->setText(this->text());

        if (!dialog->checkWidgetName(this->text()))
        {
            dialog->setModal(true);
            dialog->show();
        }
        else
        {
            dialogAccepted();
        }
    }

    void AddArmarXWidgetAction::dialogAccepted()
    {
        emit clicked(this->data().toString(), dialog->editWidgetName->text());
        emit accepted();
    }


    ArmarXDockWidget::ArmarXDockWidget(QString name, ArmarXWidgetControllerPtr controller) :
        QDockWidget(name),
        controller(controller)
    {
        scrollArea = new QScrollArea();
        scrollArea->setWidgetResizable(true);

        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        scrollArea->setSizePolicy(sizePolicy);
        setWidget(scrollArea);

        savedTitleBar = nullptr;
    }

    ArmarXDockWidget::~ArmarXDockWidget()
    {
        //    ARMARX_WARNING_S << "Deleting dockwidget: " << this->windowTitle();
        delete scrollArea;
    }

    void ArmarXDockWidget::setArmarXWidget(QWidget* widget)
    {
        scrollArea->setWidget(widget);
    }


    void ArmarXDockWidget::closeEvent(QCloseEvent* event)
    {
        if (controller && !controller->onClose())
        {
            event->ignore();
            return;
        }
        else
        {
            event->accept();
        }

        emit destroyed(this); // calls removeArmarXWidget slot

        deleteLater();// use this function, so that pending signals are processed correctly

    }

    void ArmarXDockWidget::lockWidget()
    {
        ARMARX_INFO << "Locking widget: " << objectName();
        savedTitleBar = titleBarWidget();
        setTitleBarWidget(new QWidget());
        if (controller)
        {
            controller->onLockWidget();
        }
    }

    void ArmarXDockWidget::unlockWidget()
    {
        ARMARX_INFO << "Unlocking widget: " << objectName();
        if (savedTitleBar != nullptr)
        {
            QWidget* old = titleBarWidget();
            setTitleBarWidget(savedTitleBar);
            savedTitleBar = nullptr;
            if (old)
            {
                old->deleteLater();
            }
        }
        if (controller)
        {
            controller->onUnlockWidget();
        }
    }




    OpenRecentlyOpenedFileAction::OpenRecentlyOpenedFileAction(QString text, QObject* parent, ArmarXMainWindow* mainGui) :
        QAction(text, parent),
        mainGui(mainGui)
    {

    }

    void OpenRecentlyOpenedFileAction::openFile()
    {
        if (mainGui)
        {
            mainGui->loadGuiConfig(text());
        }
    }



    void armarx::ArmarXMainWindow::on_actionClear_tip_blacklist_triggered()
    {
        tipDialog->setBlackListedStrings(QStringList());
        mainSettings.setValue(CONFIG_BLACKLISTED_TIPS, tipDialog->getBlackListedStrings());
        mainSettings.sync();
        ARMARX_INFO << "Blacklist cleared";
    }

    void armarx::ArmarXMainWindow::on_actionAbout_triggered()
    {
        QMessageBox::about(this, "ArmarX Graphical User Interface", QString("ArmarX is being developed at the High Performance Humanoid Technologies (H2T) lab at the Karlsruhe Institute of Technology (KIT)\nCopyright H2T, KIT, ")
                           + QString(&__DATE__[ 7]) +
                           + "\nVersion: " +  QString::fromStdString(Application::GetVersion()));
    }

    void armarx::ArmarXMainWindow::on_actionLoadLastConfig_toggled(bool toggled)
    {
        mainSettings.setValue(CONFIG_LOAD_LAST_CONFIG, toggled);
        mainSettings.sync();
    }




    void armarx::ArmarXMainWindow::on_actionArmarX_Documentation_triggered()
    {
        QDesktopServices::openUrl(QUrl("http://armarx.humanoids.kit.edu/"));
    }

    void armarx::ArmarXMainWindow::on_actionOpen_Use_Case_triggered()
    {
        guiUseCaseSelector->setCancelButtonText("Cancel");
        if (guiUseCaseSelector->exec() == QDialog::Accepted)
        {
            QString path = guiUseCaseSelector->getSelectedConfigFilePath();
            ARMARX_INFO << VAROUT(path);
            if (!path.isEmpty())
            {
                loadGuiConfig(path, false);
            }
        }
        mainSettings.setValue("DoNotShowUseCaseDialog", guiUseCaseSelector->getDoNotShowAgain());

    }

    void armarx::ArmarXMainWindow::on_actionClear_plugin_cache_triggered()
    {
        pluginCache.clearCacheFile();
    }

    void ArmarXMainWindow::updateOpenWidgetButtonStatus(QString widgetName)
    {
        openWidgetButton->setEnabled(pluginCache.getAvailableWidgetNames().contains(widgetName));
    }

    void ArmarXMainWindow::openWidgetButtonClicked()
    {
        addWidgetAction->triggered(searchField->text());
    }

    void ArmarXMainWindow::onContextMenuFavoritesRequested(const QPoint& pos)
    {
        QMenu menu;

        auto numberOfFavIcons = new QSpinBox(&menu);
        numberOfFavIcons->setRange(1, 100);
        numberOfFavIcons->setSingleStep(1);
        numberOfFavIcons->setValue(mainSettings.value("FavoriteWidgetCount", 6).toInt());
        numberOfFavIcons->setToolTip("Max number of favorites");
        connect(numberOfFavIcons, SIGNAL(valueChanged(int)), this, SLOT(onNumberOfMaxFavoritesChanged(int)));

        QWidgetAction* action = new QWidgetAction{&menu};
        action->setDefaultWidget(numberOfFavIcons);
        menu.addAction(action);
        menu.exec(favoritesLabel->mapToGlobal(pos));
    }

    void ArmarXMainWindow::onNumberOfMaxFavoritesChanged(int i)
    {
        mainSettings.setValue("FavoriteWidgetCount", i);
        updatefavoriteActions();
    }

    void ArmarXMainWindow::updatefavoriteActions()
    {
        const auto list = getFavoriteWidgets();
        if (
            favoriteActionWidgetNames == list
            //&&
            //favoriteActions.size() == static_cast<std::size_t>(favoriteActionWidgetNames.size())
        )
        {
            return;
        }
        favoriteActionWidgetNames = list;
        for (auto* action : favoriteActions)
        {
            ui->toolBar->removeAction(action);
            action->deleteLater();
        }
        favoriteActions.clear();
        for (auto widgetName : favoriteActionWidgetNames)
        {
            if (actionList.contains(widgetName))
            {
                bool allowText = true;
                favoriteActions.emplace_back(addActionToToolBar(actionList[widgetName], allowText));
            }
        }
    }
}
