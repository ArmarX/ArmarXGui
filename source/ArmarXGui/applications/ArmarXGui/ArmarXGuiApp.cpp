/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Component::ArmarXGui
* @author     David Ruscheweyh (david.ruscheweyh at student.kit dot edu)
* @author     Kai Welke (welke at kit dot edu)
* @copyright  2011 Humanoids Group, HIS, KITh
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "ArmarXGuiApp.h"

#include <QListWidget>
#include <QMessageBox>
#include <QTime>
#include <QTextCodec>
#include <iostream>

#include <ArmarXCore/util/CPPUtility/trace.h>
#include <ArmarXCore/core/util/StringHelpers.h>

#include <IceUtil/UUID.h>


namespace armarx
{
    int ArmarXGuiApp::globalargc = 0;


    ArmarXGuiApp::ArmarXGuiApp(int& argc, char** argv) :
        qApplication(nullptr)
    {
        this->argc = 0;
        this->argv = 0;
        qApplication = new ArmarXQApplication(argc, argv);

        // Set text encoding to UTF-8 (otherwise, umlauts display wrongly in, e.g., the log viewer)
        QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
    }

    ArmarXGuiApp::~ArmarXGuiApp()
    {
        //    std::cout << "FINISH" << std::endl;
    }

    void ArmarXGuiApp::setup(const ManagedIceObjectRegistryInterfacePtr& registry, Ice::PropertiesPtr properties)
    {
        this->registry = registry;
    }

    int ArmarXGuiApp::run(int argc, char* argv[])
    {
        if (!ArmarXManager::CheckIceConnection(communicator(), false))
        {
            if (QMessageBox::question(nullptr, "ArmarX is not running", "ArmarX is not running - do you want me to start it now?", QMessageBox::No, QMessageBox::Yes) == QMessageBox::Yes)
            {
                int result = startArmarX();
                if (result != 0)
                {
                    return result;
                }
            }
            else
            {
                return 1;
            }
        }
        setName("ArmarXGui-" + std::to_string(IceUtil::Time::now().toMilliSeconds()));
        return armarx::Application::run(argc, argv);
    }


    int ArmarXGuiApp::exec(const ArmarXManagerPtr& armarXManager)
    {
        // init SoDB and Qt, SoQt::init also creates an application object
        SoDB::init();
        SoQt::init(argc, argv, "ArmarXGuiApp");

        // enable Coin3D extension: transparent settings without color
        (void)coin_setenv("COIN_SEPARATE_DIFFUSE_TRANSPARENCY_OVERRIDE", "1", TRUE);

        {
            std::vector<std::string> packageNames = getDefaultPackageNames();
            QString configToLoad;
            if (getProperty<std::string>("GuiConfigFile").isSet())
            {
                configToLoad = QString::fromStdString(getProperty<std::string>("GuiConfigFile").getValue());
            }

            // create top-level widget
            mainWindow = new ArmarXMainWindow(registry, packageNames, configToLoad, getProperty<bool>("DisablePreloading").getValue());
            connect(mainWindow, SIGNAL(closeRequest()), this, SLOT(closeRequest_sent()));
        }
        {
            // parse properties
            std::string plugins = getProperty<std::string>("LoadPlugins").getValue();
            std::vector<std::string> pluginList = Split(plugins, ",");
            for (auto iter = pluginList.begin(); iter != pluginList.end(); ++iter)
            {
                if (!iter->empty())
                {
                    std::cout << "Loading plugin: " << iter->c_str() << std::endl;
                    mainWindow->loadPlugin(iter->c_str());
                }
            }
        }
        std::cout <<  "Started ArmarXGui App" << std::endl;

        armarxManagerTask = new RunningTask<ArmarXGuiApp>(this, &ArmarXGuiApp::runArmarXManager, "ArmarXManagerWaitThread");
        armarxManagerTask->start();

        mainWindow->show();
        mainWindow->appendFileToWindowTitle();

        ARMARX_INFO << "Now showing main window";
        int result = qApplication->exec();

        // exit SoQt and Qt
        interruptCallback(0);  // signal to the application it should terminate the ice connection

        armarXManager->waitForShutdown();

        // Shutdown.
        qApplication->quit();
        delete mainWindow;

        SoQt::done();
        delete qApplication;
        qApplication = 0;

        return result;
    }

    void ArmarXGuiApp::closeRequest_sent()
    {
    }

    void ArmarXGuiApp::runArmarXManager()
    {
        getArmarXManager()->waitForShutdown();

        if (qApplication)
        {
            qApplication->quit();
        }
    }

    int ArmarXGuiApp::startArmarX()
    {
        CMakePackageFinder finder("ArmarXCore");

        int res;
        if (finder.packageFound())
        {
            res = system(std::string(finder.getBinaryDir() + std::string("/armarx start")).c_str());
        }
        else
        {
            res = system("armarx start");
        }
        res = WEXITSTATUS(res);
        if (res == EXIT_SUCCESS)
        {
            ARMARX_INFO_S << "Started ArmarX successfully!";
            if (finder.packageFound())
            {
                res = system(std::string(finder.getBinaryDir() + std::string("/armarx memory start")).c_str());
            }
            else
            {
                res = system("armarx memory assureRunning");
            }
            res = WEXITSTATUS(res);
            if (res == EXIT_FAILURE)
            {
                QMessageBox::warning(nullptr, "ArmarX Error", "Could not start MongoDB! See terminal output for more information. The GUI will now started anyway.");
                return 0;
            }
        }
        else
        {
            QMessageBox::critical(nullptr, "ArmarX Error", "Could not start ArmarX! See terminal output for more information.");
            return 1;
        }
        return 0;
    }



    bool ArmarXQApplication::notify(QObject* obj, QEvent* ev)
    {
        try
        {
            return QApplication::notify(obj, ev);
        }
        catch (std::exception& e)
        {
            ARMARX_ERROR_S << "Exception caught:\n" << armarx::GetHandledExceptionString();
            emit exceptionCaught(QString::fromStdString(armarx::GetHandledExceptionString()));
            detail::Trace::ClearExceptionBacktrace();
        }

        return false;
    }

    void ArmarXQApplication::showException(QString exceptionReason)
    {
        QStringList infos = exceptionReason.split("Backtrace:");
        QListWidget* list = exceptionDialogHandler.listWidgetExceptions;
        auto item = new QListWidgetItem("[" + QTime::currentTime().toString() + "] " + infos.first().trimmed());
        item->setToolTip(exceptionReason);
        list->addItem(item);
        list->scrollToBottom();

        if (!exceptionDialogHandler.checkBoxDoNotShowAgain->isChecked())
        {
            exceptionDialog.show();
        }
        /*
        QStringList infos = exceptionReason.split("Backtrace:");
        QMessageBox box;
        box.setIcon(QMessageBox::Warning);
        box.setWindowTitle("An exception occured!");
        box.setText("An exception was caught!\n" + infos.first());
        if(infos.size() > 1)
            box.setDetailedText(infos.last());
        box.exec();
        */
    }


    ArmarXQApplication::ArmarXQApplication(int& argc, char** argv) :
        QApplication(argc, argv)
    {
        connect(this, SIGNAL(exceptionCaught(QString)), this, SLOT(showException(QString)), Qt::QueuedConnection);

        exceptionDialogHandler.setupUi(&exceptionDialog);
        QListWidget* list = exceptionDialogHandler.listWidgetExceptions;
        connect(exceptionDialogHandler.pushButtonClearHistory, SIGNAL(clicked()), list, SLOT(clear()), Qt::QueuedConnection);
    }

    ArmarXQApplication::~ArmarXQApplication()
    {
    }
}
