/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "TitlebarWidget.h"
#include "WidgetNameDialog.h"


#include <QGridLayout>
#include <QEvent>
#include <QDockWidget>
#include <QToolButton>
#include <QLabel>
#include <QSpacerItem>
#include <QToolBar>

#include <ArmarXGui/applications/ArmarXGui/ArmarXMainWindow.h>

namespace armarx
{
    StatusDockWidgetTitleBar::StatusDockWidgetTitleBar(QWidget* parent, ArmarXMainWindow* mainWindow) :
        QWidget(parent),
        mainWindow(mainWindow),
        imageOffline(QPixmap::fromImage(QImage(QString::fromUtf8(":/icons/network-offline.png")))),
        imageIdle(QPixmap::fromImage(QImage(QString::fromUtf8(":/icons/network-idle.png"))))
    {

        this->setContentsMargins(1, 1, 1, 1);
        dockWidget = qobject_cast<QDockWidget*>(parentWidget());
        layout = new QGridLayout(this);
        layout->setMargin(2);
        layout->setSpacing(2);
        layout->setContentsMargins(-1, 0, -1, 0);

        statusIcon = new QLabel(this);
        statusIcon->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        layout->addWidget(statusIcon, 0, 0);


        title = new QLabel(dockWidget->windowTitle(), this);
        layout->addWidget(title, 0, 1);

        editTitleBar = new QToolBar(this);
        editTitleBar->setIconSize(QSize(16, 16));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/icons/accessories-text-editor-6.ico"), QSize(16, 16));
        QAction* editTitleAction = editTitleBar->addAction(icon3, "");
        editTitleAction->setToolTip("Change the name of this widget instance");
        connect(editTitleAction, SIGNAL(triggered(bool)), this, SLOT(openChangeTitleDialog(bool)));

        layout->addWidget(editTitleBar, 0, 2);

        QSpacerItem* spacer = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Expanding);
        layout->addItem(spacer, 0, 4);


        undockButton = new QToolButton(this);
        undockButton->setObjectName(QString::fromUtf8("undockButton"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/icons/tab-detach.svg"), QSize(10, 10));
        undockButton->setIcon(icon2);
        undockButton->setIconSize(QSize(10, 10));
        //    undockButton->setCheckable(true);
        undockButton->setToolTip("Toggle the floating status of the widget");
        layout->addWidget(undockButton, 0, 5);
        closeButton = new QToolButton(this);
        closeButton->setObjectName(QString::fromUtf8("btnClose"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/icons/dialog-close.ico"), QSize(8, 8));
        closeButton->setIcon(icon1);
        closeButton->setIconSize(QSize(10, 10));
        closeButton->setToolTip("Close the widget");

        layout->addWidget(closeButton, 0, 6);



        connect(closeButton, SIGNAL(clicked()), dockWidget, SLOT(close()));
        connect(undockButton, SIGNAL(clicked()), this, SLOT(changeFloatingStatus()));
    }

    void StatusDockWidgetTitleBar::addCustomWidget(QWidget* widget)
    {
        if (!widget)
        {
            return;
        }

        if (!widget->parent())
        {
            widget->setParent(this);
        }

        layout->addWidget(widget, 0, 3);

    }

    QSize armarx::StatusDockWidgetTitleBar::sizeHint() const
    {
        QDockWidget* dockWidget = qobject_cast<QDockWidget*>(parentWidget());
        QSize size;
        //    size = dockWidget->size();
        size.setHeight(0);
        size.setWidth(0);


        if (dockWidget->features() & QDockWidget::DockWidgetVerticalTitleBar)
        {
            // I need to be vertical
            size.setHeight(title->minimumWidth() + 30);
            size.setWidth(34);
        }
        else
        {
            // I need to be horizontal
            return layout->sizeHint();

        }

        return size;
    }

    QSize armarx::StatusDockWidgetTitleBar::minimumSizeHint() const
    {
        QDockWidget* dockWidget = qobject_cast<QDockWidget*>(parentWidget());
        QSize size;

        size.setHeight(0);

        if (dockWidget->features() & QDockWidget::DockWidgetVerticalTitleBar)
        {
            // I need to be vertical
            size.setHeight(layout->minimumSize().height());

            size.setWidth(30);
        }
        else
        {
            // I need to be horizontal
            size.setHeight(30);

            size.setWidth(layout->minimumSize().width());

        }

        return size;
    }

    bool armarx::StatusDockWidgetTitleBar::event(QEvent* event)
    {
        event->ignore();
        return false;
    }

    void StatusDockWidgetTitleBar::changeFloatingStatus()
    {
        QDockWidget* dockWidget = qobject_cast<QDockWidget*>(parentWidget());
        //    undockButton->setChecked(!dockWidget->isFloating());
        dockWidget->setFloating(!dockWidget->isFloating());
    }

    void StatusDockWidgetTitleBar::changeStatus(ManagedIceObjectState state, QStringList dependencies)
    {
        auto setTextColor = [this](QString color)
        {
            title->setStyleSheet("QLabel { color : " + color + "; }");
        };
        setTextColor("black");
        QDockWidget* dockWidget = qobject_cast<QDockWidget*>(parentWidget());
        title->setText(dockWidget->windowTitle());
        switch (state)
        {
            case eManagedIceObjectCreated:
                statusIcon->setToolTip("Component created");
                break;

            case eManagedIceObjectInitializing:
                statusIcon->setToolTip("Component initializing");
                setTextColor("yellow");
                break;

            case eManagedIceObjectInitialized:
                if (dependencies.length() > 0)
                {
                    QString waitingFor = "Component initialized & waiting for dependencies: " + dependencies.join(", ");
                    statusIcon->setToolTip(waitingFor);
                    title->setToolTip(waitingFor);
                    title->setText(dockWidget->windowTitle() + " waiting for " + dependencies.at(0) + (dependencies.length() > 1 ? " ..." : ""));
                    setTextColor("red");
                }
                else
                {
                    setTextColor("black");
                    statusIcon->setToolTip("Component initialized");
                }

                break;

            case eManagedIceObjectExited:
                statusIcon->setToolTip("Component exited");
                break;

            case eManagedIceObjectExiting:
                statusIcon->setToolTip("Component exiting");
                break;

            case eManagedIceObjectStarting:
                statusIcon->setToolTip("Component starting");
                setTextColor("yellow");

                break;

            case eManagedIceObjectStarted:
                if (dependencies.length() > 0)
                {
                    QString connectedTo = "Component started & connected to " + dependencies.join(", ");
                    statusIcon->setToolTip(connectedTo);
                    title->setToolTip(connectedTo);
                }
                else
                {
                    statusIcon->setToolTip("Component started");
                }

                break;

            default:
                break;
        }

        switch (state)
        {
            case eManagedIceObjectStarted:
            {
                statusIcon->setPixmap(imageIdle);
            }
            break;

            default:
            {
                statusIcon->setPixmap(imageOffline);
            }
            break;
        }
    }

    void StatusDockWidgetTitleBar::openChangeTitleDialog(bool)
    {
        WidgetNameDialog d(dockWidget->objectName(), mainWindow);

        if (d.exec() == QDialog::Accepted)
        {
            dockWidget->setWindowTitle(d.getWidgetName());
            dockWidget->setObjectName(d.getWidgetName());
            title->setText(d.getWidgetName());
        }
    }
}
