/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <QWidget>

namespace armarx::Ui
{
    class UseCaseSelectorItem;
}

namespace armarx
{
    class UseCaseSelectorItem : public QWidget
    {
        Q_OBJECT

    public:
        explicit UseCaseSelectorItem(const QString& title, const QString& description, const QString& configFilePath, const QString& packageName, QString iconPath, QWidget* parent = 0);
        ~UseCaseSelectorItem() override;
        QString getConfigFilePath() const;
        bool isHighlighted() const;
        const int pixmapSize = 64;
    public slots:
        void highlight(bool highlight);
    signals:
        void selected(QString filepath);
        void doubleClicked();
    private:
        Ui::UseCaseSelectorItem* ui;
        QString configFilePath;
        // QWidget interface
    protected:
        void mouseReleaseEvent(QMouseEvent*) override;
        void mouseDoubleClickEvent(QMouseEvent*) override;
        bool highlighted;
    };


} // namespace armarx
