/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::ArmarXObjects::EmergencyStop
 * @author     Stefan Reither ( stef dot reither at web dot de )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "EmergencyStopWidget.h"

#include <QGridLayout>
#include <QShortcut>


#define EMERGENCY_STOP_PROXY "EmergencyStopMaster"
#define EMERGENCY_STOP_TOPIC_NAME "EmergencyStop"

namespace armarx
{
    EmergencyStopWidget::EmergencyStopWidget(QWidget* parent, ArmarXMainWindow* mainWindow) :
        mainWindow(mainWindow),
        iconNormal(QPixmap::fromImage(QImage(QString::fromUtf8(":/icons/emergency-stop.png")))),
        iconDark(QPixmap::fromImage(QImage(QString::fromUtf8(":/icons/emergency-stop-dark.png"))))
    {
        qRegisterMetaType<EmergencyStopState>("EmergencyStopState");

        QIcon icon;
        icon.addPixmap(iconNormal, QIcon::Normal, QIcon::Off);
        icon.addPixmap(iconDark, QIcon::Normal, QIcon::On);

        QGridLayout* layout = new QGridLayout(this->getWidget());
        button = new QToolButton();
        button->setCheckable(true);
        button->setIcon(icon);
        button->setIconSize(QSize(68, 28));
        button->setToolTip(QString::fromStdString("Controls the EmergencyStop. When pressed the EmergencyStop is active. Shortcut: Pause Key"));
        button->setVisible(false);
        layout->addWidget(button, 0, 0);
        layout->setMargin(0);
        layout->setContentsMargins(0, 0, 0, 0);
        this->getWidget()->setLayout(layout);
        emergencyStopShortcut = new QShortcut(this->getWidget());
        emergencyStopShortcut->setContext(Qt::ApplicationShortcut);
        emergencyStopShortcut->setKey(Qt::Key_Pause);
        connect(emergencyStopShortcut, SIGNAL(activated()), this, SLOT(clicked()));

        connect(button, SIGNAL(clicked(bool)), this, SLOT(clicked(bool)));
        std::stringstream str;
        str << "After EmergencyStop was activated, you have to wait "
            << deactivationWaitPeriod.count() << " ms before you can deactivate it.";
        button->setToolTip(QString::fromStdString(str.str()));
    }

    QWidget* EmergencyStopWidget::getButtonWidget()
    {
        return this->getWidget();
    }

    void EmergencyStopWidget::onInitComponent()
    {
        usingProxy(EMERGENCY_STOP_PROXY);
        usingTopic(EMERGENCY_STOP_TOPIC_NAME);
    }

    void EmergencyStopWidget::onConnectComponent()
    {
        emergencyStopMasterPrx = getProxy < EmergencyStopMasterInterfacePrx>(EMERGENCY_STOP_PROXY);
        QMetaObject::invokeMethod(button, "setVisible", Qt::QueuedConnection, Q_ARG(bool, true));
        QMetaObject::invokeMethod(this, "setChecked", Qt::QueuedConnection, Q_ARG(EmergencyStopState, emergencyStopMasterPrx->getEmergencyStopState()));
    }

    void EmergencyStopWidget::onDisconnectComponent()
    {
        QMetaObject::invokeMethod(button, "setVisible", Qt::QueuedConnection, Q_ARG(bool, false));
    }


    void EmergencyStopWidget::loadSettings(QSettings* settings)
    {

    }

    void EmergencyStopWidget::saveSettings(QSettings* settings)
    {

    }

    void EmergencyStopWidget::reportEmergencyStopState(EmergencyStopState state, const Ice::Current&)
    {
        QMetaObject::invokeMethod(this, "setChecked", Qt::QueuedConnection, Q_ARG(EmergencyStopState, state));
    }

    void EmergencyStopWidget::clicked(bool checked)
    {
        EmergencyStopState state = emergencyStopMasterPrx->getEmergencyStopState();
        switch (state)
        {
            case EmergencyStopState::eEmergencyStopActive :
                if (clock_t::now() > timeLastActivated + deactivationWaitPeriod)
                {
                    emergencyStopMasterPrx->setEmergencyStopState(EmergencyStopState::eEmergencyStopInactive);
                }
                else
                {
                    button->setChecked(true);
                }
                break;
            case EmergencyStopState::eEmergencyStopInactive :
                emergencyStopMasterPrx->setEmergencyStopState(EmergencyStopState::eEmergencyStopActive);
                break;
        }
    }

    std::string EmergencyStopWidget::getDefaultName() const
    {
        return "EmergencyStopGuiWidget" + iceNameUUID;
    }

    void EmergencyStopWidget::setChecked(const EmergencyStopState state)
    {
        switch (state)
        {
            case EmergencyStopState::eEmergencyStopActive :
                button->setChecked(true);
                break;
            case EmergencyStopState::eEmergencyStopInactive :
                button->setChecked(false);
                timeLastActivated = clock_t::now();
                break;
            default :
                button->setChecked(false);
        }
    }
}
