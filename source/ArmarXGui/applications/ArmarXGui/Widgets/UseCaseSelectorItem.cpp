/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "UseCaseSelectorItem.h"
#include <ArmarXGui/applications/ArmarXGui/ui_UseCaseSelectorItem.h>
#include <filesystem>
#include <ArmarXCore/core/logging/Logging.h>
#include <QFile>
#include <ArmarXGui/libraries/ArmarXGuiBase/PluginCache.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>

namespace armarx
{

    UseCaseSelectorItem::UseCaseSelectorItem(const QString& title, const QString& description, const QString& configFilePath,  const QString& packageName, QString iconPath, QWidget* parent) :
        QWidget(parent),
        ui(new Ui::UseCaseSelectorItem),
        configFilePath(configFilePath)
    {
        ui->setupUi(this);
        this->setToolTip(configFilePath);
        ui->titleLabel->setText(title);
        ui->descriptionLabel->setText(description);
        std::filesystem::path p(configFilePath.toStdString());
        p.filename().stem();
        ui->filenameLabel->setText(QString(p.filename().string().c_str()) + " in " + packageName + " ");
        QString finalPath = iconPath;
        if (!iconPath.isEmpty())
        {
            if (!QFile::exists(iconPath))
            {
                auto copy = iconPath;
                finalPath = PluginCache::GetIconCachePath() + "resources/" + copy.remove(0, 1);
            }
            if (!QFile::exists(finalPath))
            {
                finalPath = PluginCache::GetIconCachePath() + iconPath;
            }
            if (!QFile::exists(finalPath))
            {
                CMakePackageFinder f(packageName.toStdString());
                if (f.packageFound())
                {
                    std::filesystem::path path = f.getDataDir();
                    finalPath = QString::fromStdString((path / iconPath.toStdString()).string());
                }
            }
            if (QFile::exists(finalPath))
            {
                QPixmap pixmap(finalPath);
                if (!pixmap.isNull())
                {
                    ui->iconLabel->setPixmap(pixmap.scaled(pixmapSize, pixmapSize, Qt::KeepAspectRatio, Qt::SmoothTransformation));
                }
                else
                {
                    ARMARX_INFO_S << "Cannot find image: " << iconPath.toStdString();
                }
            }
        }
    }

    UseCaseSelectorItem::~UseCaseSelectorItem()
    {
        delete ui;
    }

    QString UseCaseSelectorItem::getConfigFilePath() const
    {
        return configFilePath;
    }

    bool UseCaseSelectorItem::isHighlighted() const
    {
        return highlighted;
    }

    void UseCaseSelectorItem::highlight(bool highlight)
    {
        highlighted = highlight;
        QPalette p(palette());
        setAutoFillBackground(true);
        if (highlight)
        {
            p.setColor(QPalette::Window, p.highlight().color());
        }
        else
        {
            p = qobject_cast<QWidget*>(parent())->palette();
        }
        setPalette(p);
    }

    void UseCaseSelectorItem::mouseDoubleClickEvent(QMouseEvent*)
    {
        emit doubleClicked();
    }

    void UseCaseSelectorItem::mouseReleaseEvent(QMouseEvent* event)
    {
        emit selected(configFilePath);
    }

}
