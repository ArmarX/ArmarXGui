/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "GuiUseCaseSelector.h"
#include "UseCaseSelectorItem.h"
#include <QPushButton>
#include <ArmarXGui/applications/ArmarXGui/ArmarXMainWindow.h>
#include <ArmarXGui/applications/ArmarXGui/ui_GuiUseCaseSelector.h>
#include <filesystem>
namespace armarx
{

    GuiUseCaseSelector::GuiUseCaseSelector(ArmarXMainWindow* mainWindow, bool doNotShowAgain, QWidget* parent) :
        QDialog(parent),
        ui(new Ui::GuiUseCaseSelector)
    {
        ui->setupUi(this);
        ui->buttonBox->button(QDialogButtonBox::Ok)->setText("Open selected use case");
        ui->buttonBox->button(QDialogButtonBox::Ok)->setToolTip("Opens the selected use case and loads the associated preconfigured widgets.");
        ui->buttonBox->button(QDialogButtonBox::Cancel)->setText("Open empty GUI");
        ui->buttonBox->button(QDialogButtonBox::Cancel)->setToolTip("Opens an empty gui.");
        for (auto& p : mainWindow->getDefaultPackageNames())
        {
            CMakePackageFinder f(p);
            if (!f.packageFound())
            {
                continue;
            }
            std::filesystem::path path = f.getDataDir();
            path /= p;
            path /= "GuiDefaultConfigs";
            if (!std::filesystem::exists(path))
            {
                continue;
            }
            int i = 1;
            for (std::filesystem::recursive_directory_iterator end, dir(path);
                 dir != end ; ++dir, i++)
            {
                if (dir->path().extension() == ".armarxgui")
                {
                    addDefaultConfig(dir->path().string().c_str(), p.c_str());
                }

                if (i % 100 == 0)
                {
                    ARMARX_INFO_S << "Scanning file " << i << ": " << dir->path().c_str();
                }
            }
        }
        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
        ui->checkBoxDoNotShowAgain->setChecked(doNotShowAgain);
    }

    GuiUseCaseSelector::~GuiUseCaseSelector()
    {
        delete ui;
    }

    QString GuiUseCaseSelector::getSelectedConfigFilePath() const
    {
        for (int i = 0; i < ui->verticalLayout_3->count(); ++i)
        {
            UseCaseSelectorItem* item = qobject_cast<UseCaseSelectorItem*>(ui->verticalLayout_3->itemAt(i)->widget());
            if (item && item->isHighlighted())
            {
                return item->getConfigFilePath();
            }
        }
        return "";
    }

    bool GuiUseCaseSelector::getDoNotShowAgain() const
    {
        return ui->checkBoxDoNotShowAgain->isChecked();
    }

    void GuiUseCaseSelector::setCancelButtonText(QString text)
    {
        ui->buttonBox->button(QDialogButtonBox::Cancel)->setText(text);
    }

    void GuiUseCaseSelector::addDefaultConfig(QString configPath, QString packageName)
    {
        ARMARX_INFO_S << "Adding " << configPath;
        QSettings s(configPath, QSettings::IniFormat);
        UseCaseSelectorItem* item = new UseCaseSelectorItem(s.value("ConfigTitle").toString(),
                s.value("ConfigDescription").toString(),
                configPath,
                packageName,
                s.value("IconPath").toString(),
                this);
        ui->verticalLayout_3->addWidget(item);
        connect(item, SIGNAL(selected(QString)), this, SLOT(setSelection(QString)));
        connect(item, SIGNAL(doubleClicked()), this, SLOT(accept()));
    }

    void GuiUseCaseSelector::setSelection(QString selectedUseCase)
    {
        for (int i = 0; i < ui->verticalLayout_3->count(); ++i)
        {
            UseCaseSelectorItem* item = qobject_cast<UseCaseSelectorItem*>(ui->verticalLayout_3->itemAt(i)->widget());
            if (item)
            {
                if (item->getConfigFilePath() == selectedUseCase)
                {
                    item->highlight(true);
                }
                else
                {
                    item->highlight(false);
                };
            }
        }
        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(true);

    }

} // namespace armarx
