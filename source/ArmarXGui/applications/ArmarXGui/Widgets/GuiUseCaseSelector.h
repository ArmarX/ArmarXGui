/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <QDialog>

namespace armarx
{
    class ArmarXMainWindow;
}

namespace armarx::Ui
{
    class GuiUseCaseSelector;
}

namespace armarx
{
    class GuiUseCaseSelector : public QDialog
    {
        Q_OBJECT

    public:
        explicit GuiUseCaseSelector(ArmarXMainWindow* mainWindow, bool doNotShowAgain = false, QWidget* parent = 0);
        ~GuiUseCaseSelector() override;
        QString getSelectedConfigFilePath() const;
        bool getDoNotShowAgain() const;
        void setCancelButtonText(QString text);
    public slots:
        void addDefaultConfig(QString configPath, QString packageName);
        void setSelection(QString selectedUseCase);
    private:
        Ui::GuiUseCaseSelector* ui;
        ArmarXMainWindow* mainWindow;
    };


} // namespace armarx
