/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "ViewerWidget.h"
#include <ArmarXGui/applications/ArmarXGui/ui_ViewerWidget.h>
#include <ArmarXGui/applications/ArmarXGui/ui_ViewerWidgetConfigDialog.h>

// Coin3D & SoQt
#include <Inventor/nodes/SoNode.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoCamera.h>
#include <Inventor/actions/SoLineHighlightRenderAction.h>
#include <Inventor/actions/SoSearchAction.h>

#include "../ArmarXMainWindow.h"

#include <QGridLayout>
#include <QComboBox>
#include <QLabel>
#include <QToolBar>
#include <QCheckBox>
#include <QColorDialog>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QShortcut>
#include <QDialog>

namespace armarx
{
#define ARMARX_ORGANIZATION "KIT"
#define ARMARX_GUI_APPLICATION_NAME "ArmarXGUI"

#define VIEWER_SETTINGS_AA "AntiAliasing"
#define VIEWER_SETTINGS_BACKGROUND_COLOR "BackgroundColor"
#define VIEWER_SETTINGS_TRANSPARENCY_TYPE "TransparencyType"

    Viewer3DWidget::Viewer3DWidget(QWidget* parent) :
        ui(new Ui_Viewer3DWidget),
        customToolbar(NULL),
        settings(QSettings::UserScope, ARMARX_ORGANIZATION, ARMARX_GUI_APPLICATION_NAME),
        viewingModeAction(NULL)
    {
        configDialog = nullptr;
        configDialogUi = nullptr;
    }

    Viewer3DWidget::~Viewer3DWidget()
    {
        ARMARX_INFO << "~Viewer3DWidget()";
        viewer.reset();

        delete configDialogUi;
        delete ui;
    }

    void Viewer3DWidget::setNode(int index)
    {
        if (!viewer)
        {
            return;
        }

        auto l = viewer->getScopedLock();

        if (index < 0 || index >= cb3DViewers->count())
        {
            viewer->setSceneGraph(emptyNode);
        }
        else
        {
            QString sceneName = cb3DViewers->itemText(index);

            ARMARX_INFO << "Setting 3D scene: " << sceneName;

            SoNode* node = sceneMap[sceneName].node;
            bool initializeView = find(selectedViews.begin(), selectedViews.end(), node) == selectedViews.end();
            viewer->setSceneGraph(node);

            if (initializeView)
            {
                if (initialCameraPoses.find(sceneName) != initialCameraPoses.end())
                {
                    CameraPose p = initialCameraPoses[sceneName];

                    viewer->getCamera()->position.setValue(p.position[0], p.position[1], p.position[2]);
                    viewer->getCamera()->orientation.setValue(p.orientation[0], p.orientation[1], p.orientation[2], p.orientation[3]);
                    viewer->getCamera()->focalDistance.setValue(p.focalDistance);
                }
                else
                {
                    viewer->viewAll();
                }

                selectedViews.push_back(node);
            }
        }
    }

    void Viewer3DWidget::loadSettings(QSettings* settings)
    {
        std::set<QString> sceneNames;
        for (auto& key : settings->allKeys())
        {
            if (key.startsWith("camera."))
            {
                sceneNames.insert(key.split('.').at(1));
            }
        }

        for (auto& scene : sceneNames)
        {
            CameraPose p;

            p.position[0] = settings->value("camera." + scene + ".pos_x").toFloat();
            p.position[1] = settings->value("camera." + scene + ".pos_y").toFloat();
            p.position[2] = settings->value("camera." + scene + ".pos_z").toFloat();

            p.orientation[0] = settings->value("camera." + scene + ".ori_q1").toFloat();
            p.orientation[1] = settings->value("camera." + scene + ".ori_q2").toFloat();
            p.orientation[2] = settings->value("camera." + scene + ".ori_q3").toFloat();
            p.orientation[3] = settings->value("camera." + scene + ".ori_q4").toFloat();

            p.focalDistance = settings->value("camera." + scene + ".focalDistance").toFloat();

            initialCameraPoses[scene] = p;
        }

        // clear initialized views and reset node to apply loaded settings
        if (viewer)
        {
            auto l = viewer->getScopedLock();

            selectedViews.clear();
            setNode(cb3DViewers->currentIndex());
        }
    }

    void Viewer3DWidget::saveSettings(QSettings* settings)
    {
        for (auto& scene : sceneMap.toStdMap())
        {
            SoSearchAction a;
            a.setType(SoCamera::getClassTypeId(), true);
            a.setInterest(SoSearchAction::FIRST);
            a.apply(scene.second.node);

            if (!a.getPath())
            {
                continue;
            }

            SoCamera* cameraNode = (SoCamera*)a.getPath()->getTail();
            QString sceneName = scene.first;

            settings->setValue("camera." + sceneName + ".pos_x", cameraNode->position.getValue().getValue()[0]);
            settings->setValue("camera." + sceneName + ".pos_y", cameraNode->position.getValue().getValue()[1]);
            settings->setValue("camera." + sceneName + ".pos_z", cameraNode->position.getValue().getValue()[2]);

            settings->setValue("camera." + sceneName + ".ori_q1", cameraNode->orientation.getValue().getValue()[0]);
            settings->setValue("camera." + sceneName + ".ori_q2", cameraNode->orientation.getValue().getValue()[1]);
            settings->setValue("camera." + sceneName + ".ori_q3", cameraNode->orientation.getValue().getValue()[2]);
            settings->setValue("camera." + sceneName + ".ori_q4", cameraNode->orientation.getValue().getValue()[3]);

            settings->setValue("camera." + sceneName + ".focalDistance", cameraNode->focalDistance.getValue());
        }
    }

    void Viewer3DWidget::setMainWindow(QMainWindow* mainWindow)
    {
        ArmarXWidgetController::setMainWindow(mainWindow);
        ArmarXMainWindow* guiMainWindow = qobject_cast<ArmarXMainWindow*>(getMainWindow());
        connect(guiMainWindow, SIGNAL(updateSceneList(QMap<QString, Viewer3DInfo>)), this, SLOT(sceneListUpdated(QMap<QString, Viewer3DInfo>)));
        sceneListUpdated(guiMainWindow->viewer3DMap);
    }

    void Viewer3DWidget::postDocking()
    {
        ui->setupUi(getWidget());
        getWidget()->setContentsMargins(1, 1, 1, 1);
        getWidget()->setFocusPolicy(Qt::WheelFocus);

        QGridLayout* grid = new QGridLayout();
        grid->setContentsMargins(0, 0, 0, 0);
        getWidget()->setLayout(grid);
        getWidget()->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);

        QWidget* view1 = new QWidget(getWidget());
        view1->setMinimumSize(100, 100);
        view1->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);

        viewer.reset(new CoinViewer(view1, "", TRUE, SoQtExaminerViewer::BUILD_POPUP));
        if (mutex3D)
        {
            viewer->setMutex(mutex3D);
        }

        // setup
        viewer->setBackgroundColor(SbColor(1.0f, 1.0f, 1.0f));
        viewer->setAccumulationBuffer(false);

        emptyNode = new SoSeparator();
        emptyNode->ref();
        viewer->setSceneGraph(emptyNode);

        viewer->setFeedbackVisibility(true);
        viewer->setGLRenderAction(new SoLineHighlightRenderAction());
        viewer->viewAll();

        // show everything
        viewer->show();

        cb3DViewers = new QComboBox(getWidget());
        label = new QLabel("Scene:");
        sceneConfigDialogButton = new QPushButton(QIcon(":/icons/configure-3.png"), "", getWidget());
        sceneConfigDialogButton->setToolTip("Configure Scene");
        grid->addWidget(view1, 0, 0, 1, 3);
        grid->addWidget(label, 1, 0);
        grid->addWidget(cb3DViewers, 1, 1);
        grid->addWidget(sceneConfigDialogButton, 1, 2);
        grid->setColumnMinimumWidth(2, 20);
        sceneConfigDialogButton->setEnabled(false);
        connect(cb3DViewers, SIGNAL(activated(int)), this, SLOT(select3DView(int)));
        connect(cb3DViewers, SIGNAL(currentIndexChanged(int)), this, SLOT(select3DView(int)));
        connect(sceneConfigDialogButton, &QPushButton::pressed, this, [this]()
        {
            int index = cb3DViewers->currentIndex();
            if (index >= 0 && index < cb3DViewers->count())
            {
                QString sceneName = cb3DViewers->itemText(index);
                auto sceneConfigDialog = sceneMap[sceneName].sceneConfigDialog;

                if (sceneConfigDialog)
                {
                    sceneConfigDialog->open();
                }
            }
        });

        // Create config dialog
        configDialog = new QDialog(ArmarXWidgetController::getMainWindow());
        configDialogUi = new Ui_ViewerWidgetConfigDialog;
        configDialogUi->setupUi(configDialog);

        connect(configDialogUi->buttonBox, SIGNAL(accepted()), this, SLOT(configDialogApplySettings()));
        connect(configDialogUi->buttonBox->button(QDialogButtonBox::Save), SIGNAL(clicked()), this, SLOT(configDialogSaveSettings()));
        connect(configDialogUi->pushButtonPickColor, SIGNAL(pressed()), this, SLOT(configDialogPickColor()));


        configDialogLoadSettings();


        ArmarXMainWindow* guiMainWindow = qobject_cast<ArmarXMainWindow*>(ArmarXWidgetController::getMainWindow());
        if (guiMainWindow)
        {
            sceneListUpdated(guiMainWindow->viewer3DMap);
        }
    }

    void Viewer3DWidget::setMutex3D(RecursiveMutexPtr const& mutex3D)
    {
        this->mutex3D = mutex3D;
        if (viewer)
        {
            viewer->setMutex(mutex3D);
        }
    }

    QPointer<QWidget> Viewer3DWidget::getCustomTitlebarWidget(QWidget* parent)
    {
        if (customToolbar)
        {
            if (parent != customToolbar->parent())
            {
                customToolbar->setParent(parent);
            }

            return customToolbar;
        }

        customToolbar = new QToolBar(parent);
        customToolbar->setIconSize(QSize(16, 16));
        customToolbar->addAction(QIcon(":/icons/configure-3.png"), "Configure", this, SLOT(configDialogOpen()));

        viewingModeAction = customToolbar->addAction(QIcon(":/icons/hand.svg"), "Viewing Mode (V)", this, SLOT(toggleViewingMode()));
        viewingModeAction->setShortcut(QKeySequence(Qt::Key_V));
        viewingModeAction->setShortcutContext(Qt::WidgetWithChildrenShortcut);
        getWidget()->addAction(viewingModeAction);
        viewingModeAction->setCheckable(true);
        viewingModeAction->setChecked(true);

        QAction* viewAllAction = new QAction(QIcon(":icons/zoom-original-2.png"), "View All (A)", getWidget());
        customToolbar->addAction(viewAllAction);
        viewAllAction->setShortcut(QKeySequence(tr("A")));
        viewAllAction->setShortcutContext(Qt::WidgetWithChildrenShortcut);
        connect(viewAllAction, SIGNAL(triggered()), this, SLOT(viewAll()));
        getWidget()->addAction(viewAllAction);

        return customToolbar;
    }

    void Viewer3DWidget::select3DView(int index)
    {
        //    ARMARX_INFO << "Selecting 3d viewer entry " << index;
        if (index >= 0 && index < cb3DViewers->count())
        {
            QString sceneName = cb3DViewers->itemText(index);
            auto sceneConfigDialog = sceneMap[sceneName].sceneConfigDialog;

            sceneConfigDialogButton->setEnabled(sceneConfigDialog != nullptr);
        }
        else
        {
            sceneConfigDialogButton->setEnabled(false);
        }
        setNode(index);
    }

    void Viewer3DWidget::sceneListUpdated(QMap<QString, Viewer3DInfo> sceneMap)
    {
        if (!viewer)
        {
            return;
        }

        auto l = viewer->getScopedLock();
        //    ARMARX_INFO << "scene map size: " << sceneMap.size();
        this->sceneMap = sceneMap;
        int oldIndex = cb3DViewers->currentIndex();

        if (oldIndex < 0)
        {
            oldIndex = 0;
        }

        cb3DViewers->clear();
        QMap<QString, Viewer3DInfo> ::iterator it = sceneMap.begin();
        std::vector<SoNode*> newSelectedList;

        for (; it != sceneMap.end(); it++)
        {
            cb3DViewers->addItem(it.key());

            if (find(selectedViews.begin(), selectedViews.end(), it.value().node) != selectedViews.end())
            {
                newSelectedList.push_back(it.value().node);
            }
        }

        selectedViews = newSelectedList;

        if (oldIndex < cb3DViewers->count())
        {
            select3DView(oldIndex);
            //        cb3DViewers->setCurrentIndex(oldIndex);
        }

        //    ARMARX_INFO << "SceneList updated";
    }

    void Viewer3DWidget::configDialogPickColor(QColor color)
    {
        if (!color.isValid())
        {
            color = QColorDialog::getColor();
        }

        if (color.isValid())
        {
            QPalette p;
            p.setColor(QPalette::Button, color);
            configDialogUi->pushButtonPickColor->setPalette(p);
        }
    }

    void Viewer3DWidget::toggleViewingMode()
    {
        viewer->setViewing(!viewer->isViewing());
        viewingModeAction->setChecked(viewer->isViewing());

        ARMARX_INFO << "Viewing mode " << (viewer->isViewing() ? "enabled" : "disabled");
    }

    void Viewer3DWidget::viewAll()
    {
        if (viewer)
        {
            viewer->viewAll();
        }
    }

    void Viewer3DWidget::configDialogOpen()
    {
        SbColor c = viewer->getBackgroundColor();
        configDialogPickColor(QColor(255 * c[0], 255 * c[1], 255 * c[2]));

        configDialog->show();
    }

    void Viewer3DWidget::configDialogApplySettings()
    {
        int numPasses = 0;

        if (configDialogUi->checkBoxMultipass->isChecked())
        {
            numPasses = pow(2, (configDialogUi->comboBoxMultipass->currentIndex() + 1));
        }
        viewer->setAccumulationBuffer(false);
        viewer->setAntialiasing((numPasses > 0), numPasses);
        ARMARX_INFO << "Multipass Anti Aliasing: " << numPasses << "x";

        QColor color = configDialogUi->pushButtonPickColor->palette().color(QPalette::Button);
        viewer->setBackgroundColor(SbColor(color.red() / 255.0, color.green() / 255.0, color.blue() / 255.0));
        ARMARX_INFO << "Background Color: (" << color.red() << ", " << color.green() << ", " << color.blue() << ")";

        if (configDialogUi->checkBoxTransparency->isChecked())
        {
            switch (configDialogUi->comboBoxTransparencyType->currentIndex())
            {
                case 0:
                    viewer->setTransparencyType(SoGLRenderAction::BLEND);
                    break;

                case 1:
                    viewer->setTransparencyType(SoGLRenderAction::SORTED_OBJECT_BLEND);
                    break;

                case 2:
                    viewer->setTransparencyType(SoGLRenderAction::SORTED_LAYERS_BLEND);
                    break;

                default:
                    ARMARX_WARNING << "Unknown transparency type set";
                    break;
            }
        }
        else
        {
            viewer->setAlphaChannel(false);
            viewer->setTransparencyType(SoGLRenderAction::NONE);
            ARMARX_INFO << "Transparency disabled";
        }
    }

    void Viewer3DWidget::configDialogSaveSettings()
    {
        if (!configDialogUi->checkBoxTransparency->isChecked())
        {
            settings.setValue(VIEWER_SETTINGS_TRANSPARENCY_TYPE, -1);
        }
        else
        {
            settings.setValue(VIEWER_SETTINGS_TRANSPARENCY_TYPE, configDialogUi->comboBoxTransparencyType->currentIndex());
        }

        if (!configDialogUi->checkBoxMultipass->isChecked())
        {
            settings.setValue(VIEWER_SETTINGS_AA, -1);
        }
        else
        {
            settings.setValue(VIEWER_SETTINGS_AA, configDialogUi->comboBoxMultipass->currentIndex());
        }

        settings.setValue(VIEWER_SETTINGS_BACKGROUND_COLOR, configDialogUi->pushButtonPickColor->palette().color(QPalette::Button));
    }

    void Viewer3DWidget::configDialogLoadSettings()
    {
        int transparencyType = settings.value(VIEWER_SETTINGS_TRANSPARENCY_TYPE, 1).toInt();
        configDialogUi->checkBoxTransparency->setChecked(transparencyType != -1);
        configDialogUi->comboBoxTransparencyType->setCurrentIndex(transparencyType);

        int antiAliasing = settings.value(VIEWER_SETTINGS_AA, -1).toInt();
        configDialogUi->checkBoxMultipass->setChecked(antiAliasing != -1);
        configDialogUi->comboBoxMultipass->setCurrentIndex(antiAliasing);

        QColor backgroundColor = settings.value(VIEWER_SETTINGS_BACKGROUND_COLOR, QColor(255, 255, 255)).value<QColor>();
        if (backgroundColor.isValid())
        {
            QPalette p;
            p.setColor(QPalette::Button, backgroundColor);
            configDialogUi->pushButtonPickColor->setPalette(p);
        }

        configDialogApplySettings();
    }

    void Viewer3DWidget::onInitComponent()
    {
    }

    void Viewer3DWidget::onConnectComponent()
    {
    }

    void Viewer3DWidget::onLockWidget()
    {
        cb3DViewers->setVisible(false);
        label->setVisible(false);
        sceneConfigDialogButton->setVisible(false);
    }

    void Viewer3DWidget::onUnlockWidget()
    {
        cb3DViewers->setVisible(true);
        label->setVisible(true);
        sceneConfigDialogButton->setVisible(true);
    }
}
