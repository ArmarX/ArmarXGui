/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once



#include <QDialog>


class QLabel;
class QLineEdit;
class QDialogButtonBox;
class QGridLayout;

namespace armarx
{
    class ArmarXMainWindow;

    /**
     * \brief The WidgetNameDialog class
     */
    class WidgetNameDialog : public QDialog
    {
        Q_OBJECT
    public:
        WidgetNameDialog(QString widgetName, ArmarXMainWindow* parent = 0);

        QString getWidgetName() const;

        ArmarXMainWindow* parent;
        QGridLayout* layout;
        QLabel* labelWidgetName;
        QLineEdit* editWidgetName;
        QDialogButtonBox* buttonBox;
    public slots:
        bool checkWidgetName(QString name);
    private:

    } ;
}

