/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// Coin3D & SoQt
#include <Inventor/nodes/SoNode.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>

#include <ArmarXCore/interface/core/ManagedIceObjectDefinitions.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXWidgetController.h>

#include <QWidget>


class QLabel;
class QToolButton;
class QGridLayout;
class QDockWidget;
class QToolBar;



namespace armarx
{
    class ArmarXMainWindow;
    /**
     * \brief The StatusDockWidgetTitleBar class
     */
    class StatusDockWidgetTitleBar :
        public QWidget
    {
        Q_OBJECT
    public:
        StatusDockWidgetTitleBar(QWidget* parent = 0, ArmarXMainWindow* mainWindow = 0);



        // QWidget interface
    public:
        void addCustomWidget(QWidget* widget);
        QSize sizeHint() const override;
        QSize minimumSizeHint() const override;

        // QObject interface
    public:
        bool event(QEvent* event) override;

    public slots:
        void changeFloatingStatus();
        void changeStatus(ManagedIceObjectState state, QStringList dependencies);
        void openChangeTitleDialog(bool);
    private:
        QDockWidget* dockWidget;
        ArmarXMainWindow* mainWindow;
        QGridLayout* layout;
        QLabel* title;
        QToolBar* editTitleBar;
        QToolButton* closeButton;
        QToolButton* undockButton;
        QToolButton* hideButton;
        QLabel* statusIcon;
        QPixmap imageOffline;
        QPixmap imageIdle;
    };

}

