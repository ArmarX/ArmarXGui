/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/PluginCache.h>

// Qt
#include <QMainWindow>
#include <QDir>
#include <QDockWidget>
#include <QAction>
#include <QPluginLoader>

// Coin3D & SoQt
#include <Inventor/nodes/SoNode.h>
#include <Inventor/nodes/SoSeparator.h>

// ArmarX
#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/services/tasks/RunningTask.h>

#include <QSplashScreen>

#include "Widgets/ViewerWidget.h"
#include "Widgets/EmergencyStopWidget.h"

class QToolButton;
class QLabel;
class QScrollArea;
class QSignalMapper;
class QListWidgetItem;
class QActionGroup;
class QLineEdit;

namespace Ui
{
    class ArmarXMainWindow;
}

// project
namespace armarx
{
    class GuiUseCaseSelector;
    class AddArmarXWidgetAction;

    class ArmarXDockWidget;

    struct Viewer3DInfo
    {
        SoNode* node;
        QPointer<QDialog> sceneConfigDialog;
    };

    /**
     * \brief The ArmarXMainWindow class
     */
    class ArmarXMainWindow :
        public QMainWindow,
        virtual public armarx::Logging
    {
        Q_OBJECT

    public:
        ArmarXMainWindow(const armarx::ManagedIceObjectRegistryInterfacePtr& registry, const std::vector<std::string>& packages, const QString& configToLoad, bool disablePreloading);
        ~ArmarXMainWindow() override;

        /**
         * loads a plugin with the given file path
         */
        void loadPlugin(QString filePath);

        /**
        * emits the closeRequest signal
        */
        void closeEvent(QCloseEvent* event) override;

        void updateRecentlyOpenedFileList();
        void addToRecentlyOpenedFileList(QString configFilepath);

        /**
         * @brief getDefaultPackageNames returns the names of all packages which are searched for plugins when the ArmarXGuiApp is started.
         * @return vector of strings containing the names of the default packages
         */
        std::vector<std::string> getDefaultPackageNames();

        /**
         * Provides access to a viewer widget, if existent
         */
        Viewer3DWidgetPtr getViewerWidget();

        /*!
         * \brief Provides access to the coin3d mutex. Use this mutex to protect access to the 3d scene.
         * \return
         */
        std::shared_ptr<std::recursive_mutex> getMutex3D();

        bool existsWidget(const QString& widgetName);
        QString getLibraryPathFromPackage(const QString& packageName);

        QStringList getFavoriteWidgets();

    public slots:
        void loadPluginsFromPackage(const QString& packageName);
        void loadGuiConfig(QString configFilepath = "", bool showAsOpenGuiConfig = true);
        void appendFileToWindowTitle(const QString& filepath = "");

    signals:
        /**
        * emitted, when the main window should be closed
        */
        void closeRequest();


        void updateSceneList(QMap<QString, Viewer3DInfo>);
    private slots:
        void closeAllWidgets();
        void closeAllWidgetsWithDialog();
        void saveGuiConfig();
        void saveGuiConfigAs();
        void updateStatusOfOpenWidgets();
        void enterFullscreenMode();
        void leaveFullscreenMode();
        void toggleWidgetLock();


        /**
        * shows a file dialog for loading a gui plugin
        */
        void pluginDialog();


        //         void listIndexes(const QModelIndexList & indexes);
        ArmarXWidgetControllerPtr createArmarXWidget(QString widgetName, QString customInstanceName, QSettings* settings = NULL);
        ArmarXWidgetControllerPtr addArmarXWidget(ArmarXWidgetControllerPtr newWidgetController, bool createViewerWidget = true);
        void addArmarXWidgetCanceled(ArmarXWidgetControllerPtr newWidgetController);
        void removeArmarXWidget(QObject* widget);
        void removeViewerWidget(QObject* widget);

        void on_actionClear_tip_blacklist_triggered();

        void on_actionAbout_triggered();

        void on_actionLoadLastConfig_toggled(bool toggled);

        void on_actionArmarX_Documentation_triggered();

        void on_actionOpen_Use_Case_triggered();

        void on_actionClear_plugin_cache_triggered();

        void updateOpenWidgetButtonStatus(QString widgetName);

        void openWidgetButtonClicked();

        void onContextMenuFavoritesRequested(const QPoint& pos);
        void onNumberOfMaxFavoritesChanged(int i);

        void updatefavoriteActions();
    private:
        /**
         * @brief updates the menu entry with the available widgets.
         */
        void updateAvailableWidgetList();
        void updateOpenWidgetList();

        /**
         * Add the (optional) icon button to start ArViz Godot to the main widgets if ArViz Godot is available.
         */
        void addArVizGodotIcon();

        /**
        * sets a SoQtExamine rViewer up. This ExaminerViewer is contained by the class member QWidget* view
        */
        Viewer3DWidgetPtr setupViewerWidget();

        /**
        * loads all gui plugins in home/armarx recursively
        */
        void loadPlugins(const QString& pluginDir, bool searchRecursive = true);


        void loadPlugins(const QSet<QString>& pluginDirs, bool searchRecursive = true);
        void loadPlugins(const QStringList& fileNames);

        QAction* addActionToToolBar(QAction* action, bool allowText);

        PluginCache pluginCache;
        QDir pluginsDir;
        QStringList loadedPackages;
        std::vector<QPluginLoader*> pluginsToLoad;
        QList<QDir> directoriesToCheck;

        ::Ui::ArmarXMainWindow* ui;

        armarx::ManagedIceObjectRegistryInterfacePtr registry;

        std::vector<ArmarXGuiInterface*> plugins;
        QStringList loadedPluginFilepaths;

        QList<QWidget*> widgetsList;
        //        WidgetCreatorMap availableWidgets;

        QMap<QString, Viewer3DInfo> viewer3DMap;

        QSignalMapper* signalMapper;
        QActionGroup* layoutActions;
        using OpenWidgetMap = QMap<QString, QPair<QDockWidget*, ArmarXWidgetControllerPtr> >;
        OpenWidgetMap listOpenWidgets;
        std::vector<ArmarXWidgetControllerPtr> pendingWidgets;
        QListWidgetItem* viewerItem;

        QPointer<TipDialog> tipDialog;
        const int widgetUsageHistoryLength = 100;


        std::vector<std::string> defaultPackageNames;


        // Emergency Stop
        EmergencyStopWidgetPtr emergencyStopWidget;

        QAction* leaveFullScreenActionF11;
        QAction* leaveFullScreenActionEsc;

        bool widgetsLocked;

    private:
        QPointer<QDockWidget> getBiggestOpenWidget();
        QMenu* getCategoryMenu(const std::string& categoryString, QMenu* menu, QIcon widgetIcon);
        QSettings mainSettings;
        QString configFilepath;
        QString guiWindowBaseName;
        QTimer* connectionStatusTimer;
        QSplashScreen* splashSceen;
        QString splashscreenPrefix;
        GuiUseCaseSelector* guiUseCaseSelector;
        QStringList mainWidgets;
        QToolButton* openWidgetButton;
        QLineEdit* searchField;
        AddArmarXWidgetAction* addWidgetAction;
        std::shared_ptr<std::recursive_mutex> mutex3d;
        QMap<QString, QAction*> actionList;

        QLabel* favoritesLabel;
        std::vector<QAction*> favoriteActions;
        QStringList favoriteActionWidgetNames;

        friend class WidgetNameDialog;
        friend class Viewer3DWidget;
    };

    /////////////////////////////////////////////////////////////////////////
    //// Additional Helper Classes
    /////////////////////////////////////////////////////////////////////////

    class WidgetNameDialog;

    /**
     * \brief The ArmarXDockWidget class
     */
    class ArmarXDockWidget :
        public QDockWidget
    {
    public:
        ArmarXDockWidget(QString name, ArmarXWidgetControllerPtr controller);
        ~ArmarXDockWidget() override;
        void setArmarXWidget(QWidget* widget);
        void closeEvent(QCloseEvent* event) override;

        void lockWidget();
        void unlockWidget();
    private:
        ArmarXWidgetControllerPtr controller;
        QScrollArea* scrollArea;

        QWidget* savedTitleBar;
    };

    class AddArmarXWidgetAction :
        public QAction
    {
        Q_OBJECT
    public:
        AddArmarXWidgetAction(QString widgetName, QObject* parent = 0, ArmarXMainWindow* mainGui = 0);
        ~AddArmarXWidgetAction() override;

    public slots:
        void triggered(QString widgetName = QString());
        void addArmarXWidget();
        void dialogAccepted();
    signals:
        void clicked(QString widgetName, QString customInstanceName);
        void accepted();
    private:
        ArmarXMainWindow* mainGui;
        WidgetNameDialog* dialog;
    };

    class OpenRecentlyOpenedFileAction :
        public QAction
    {
        Q_OBJECT
    public:
        OpenRecentlyOpenedFileAction(QString text, QObject* parent = 0, ArmarXMainWindow* mainGui = 0);
    public slots :
        void openFile();
    private:
        ArmarXMainWindow* mainGui;
    };



}

