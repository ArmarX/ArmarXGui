include(${ArmarXGui_CMAKE_DIR}/legacy/ArmarXQt4Qt5DualSupport.cmake)

add_definitions(-DQT_DISABLE_DEPRECATED_BEFORE=0x040800)

set(CMAKE_GLOBAL_AUTOGEN_TARGET ON)
set(CMAKE_GLOBAL_AUTORCC_TARGET ON)

find_package(Qt5Script QUIET)
find_package(Qt5OpenGL QUIET)
find_package(Qt5Designer QUIET)
find_package(Qt5Network QUIET)

#https://forum.qt.io/topic/81252/cmake-qt5_wrap_ui-issue
# > Since QT5_WRAP_UI macro is delivered as part of Qt5Widget packacge it is necessery
# > to make a call to find_package (Qt5Widget) before issuing QT5_WRAP_UI call.
find_package(Qt5 COMPONENTS Widgets QUIET)
if(NOT Qt5_FOUND)
    message(WARNING "Qt5 NOT FOUND")
endif()
find_package(Qt5LinguistTools QUIET)

string(REGEX REPLACE "/[^/]+$" "" QT_EXECUTABLE_PRE "${QT_MOC_EXECUTABLE}")

function(armarx_gui_library PLUGIN_NAME SOURCES QT_MOC_HDRS QT_UIS QT_RESOURCES COMPONENT_LIBS)
    if(NOT "${ARMARX_PROJECT_NAME}" STREQUAL "ArmarXGui")
        armarx_build_if(ArmarXGui_FOUND "ArmarXGui not available")
        if(NOT ArmarXGui_FOUND)
            return()
        endif()
    endif()

    armarx_find_qt_base("QtCore;QtGui;QtOpenGL;QtXml;QtScript;QtDesigner" REQUIRED)


    if(NOT "ArmarXGuiBase" STREQUAL "${PLUGIN_NAME}")
        if(COMPONENT_LIBS)
            list(APPEND COMPONENT_LIBS ArmarXCore ArmarXGuiBase ${QT_LIBRARIES})
        else()
            set(COMPONENT_LIBS ArmarXCore ArmarXGuiBase ${QT_LIBRARIES})
        endif()
    endif()

    list(APPEND SOURCES ${QT_RESOURCES})
    list(APPEND HEADERS ${QT_MOC_HDRS})
    list(APPEND HEADERS ${QT_UIS})

    set(GENERATE_BASE_DIR "${CMAKE_CURRENT_BINARY_DIR}/${PLUGIN_NAME}_autogen/include")

    install(DIRECTORY "${GENERATE_BASE_DIR}" DESTINATION "include" COMPONENT headers)
    printtarget("${HEADERS}" "${SOURCES}" "${QT_RESOURCES}" "${COMPONENT_LIBS}")

    armarx_add_library("${PLUGIN_NAME}"
         "${SOURCES}"
         "${HEADERS}"
         "${COMPONENT_LIBS}"
    )
    target_include_directories("${PLUGIN_NAME}" PUBLIC $<BUILD_INTERFACE:${GENERATE_BASE_DIR}> )
    target_compile_definitions("${PLUGIN_NAME}" PUBLIC -DQ_COMPILER_INITIALIZER_LISTS)

    set_target_properties("${PLUGIN_NAME}" PROPERTIES AUTOMOC ON)
    set_target_properties("${PLUGIN_NAME}" PROPERTIES AUTOUIC ON)
    set_target_properties("${PLUGIN_NAME}" PROPERTIES AUTORCC ON)
    set_target_properties("${PLUGIN_NAME}" PROPERTIES AUTOUIC_SEARCH_PATHS ${${ARMARX_PROJECT_NAME}_SOURCE_DIR}/source)

    file(RELATIVE_PATH CORRECT_INCLUDE "${CMAKE_SOURCE_DIR}/source" ${CMAKE_CURRENT_SOURCE_DIR})

    add_custom_command(
        TARGET ${PLUGIN_NAME}
        POST_BUILD
        COMMAND ${CMAKE_COMMAND}
        ARGS
            "-DGLOBAL_INCLUDE=${ARMARX_PROJECT_INCLUDE_PATH}"
            "-DDIR=${CMAKE_CURRENT_BINARY_DIR}/${PLUGIN_NAME}_autogen/include/"
            "-DCORRECT_INCLUDE=${CORRECT_INCLUDE}"
            "-DPROJECT_SOURCE_DIR=${CMAKE_SOURCE_DIR}/source"
            "-P" "${ArmarXGui_CMAKE_DIR}/check_qt_moc_includes.cmake"
        COMMENT "Checking qt moc includes"
    )

    if(${VERBOSE})
        message(STATUS "        Gui Library Directories:")
        printlist("              " "${ArmarXGui_LIBRARY_DIRS}")
        message(STATUS "        Include Directories: ")
        printlist("              " "${INCLUDE_DIRECTORIES}")
    endif()
endfunction()

function(armarx_gui_plugin PLUGIN_NAME SOURCES QT_MOC_HDRS QT_UIS QT_RESOURCES COMPONENT_LIBS)
    list(APPEND SOURCES ${QT_RESOURCES})
    list(APPEND HEADERS ${QT_MOC_HDRS})
    list(APPEND HEADERS ${QT_UIS})

    set(GENERATE_BASE_DIR "${CMAKE_CURRENT_BINARY_DIR}/${PLUGIN_NAME}_autogen/include")

    #gui plugin name
    # Make sure to remove only the last 'GuiPlugin' otherwise you cannot name your plugins '*GuiPlugin'
    string(REGEX REPLACE "GuiPlugin$" "" ARMARX_GUI_PLUGIN_PREFIX "${ARMARX_TARGET_NAME}")

    #autogen header if none was supplied
    set(tmp_headers ${HEADERS})
    set(tmp_regex "(^|\\.*/)${ARMARX_GUI_PLUGIN_PREFIX}GuiPlugin\\.h")
    list(FILTER tmp_headers INCLUDE REGEX "${tmp_regex}")
    list(REMOVE_DUPLICATES tmp_headers)
    list(LENGTH tmp_headers tmp_headers_len)

    if("0" STREQUAL "${tmp_headers_len}")
        ########################################################################
        #get rel path for subdir
        file(RELATIVE_PATH subdir "${PROJECT_SOURCECODE_DIR}" "${CMAKE_CURRENT_SOURCE_DIR}")
        ########################################################################
        #get the widget controller header
        set(tmp_headers ${HEADERS})
        set(tmp_regex "(^|\\.*/)${ARMARX_GUI_PLUGIN_PREFIX}WidgetController\\.h")
        list(FILTER tmp_headers INCLUDE REGEX "${tmp_regex}")
        list(REMOVE_DUPLICATES tmp_headers)
        list(LENGTH tmp_headers tmp_headers_len)
        if(NOT "1" STREQUAL "${tmp_headers_len}")
            message(FATAL_ERROR "Failed to auto generate the GuiPlugin headers! Error: Can't find the widget controller '${tmp_regex}'")
        endif()
        list(GET tmp_headers 0 tmp_hdr)
        set(ARMARX_GUI_PLUGIN_WIDGET_CONTROLLER_HEDER "${subdir}/${tmp_hdr}")
        ########################################################################
        #generate and add files
        set(outfile "${GENERATE_BASE_DIR}/${subdir}/${ARMARX_TARGET_NAME}")
        foreach(suff h cpp)
        configure_file(
            "${ArmarXCore_TEMPLATES_DIR}/GuiPluginTemplate/GuiPlugin.tmp.${suff}"
            "${outfile}.${suff}"
            @ONLY
        )
        endforeach()
        list(APPEND SOURCES "${outfile}.cpp")
        list(APPEND HEADERS "${outfile}.h")
    endif()

    armarx_gui_library(
        "${PLUGIN_NAME}"
        "${SOURCES}"
        "${HEADERS}"
        ""
        "${QT_RESOURCES}"
        "${COMPONENT_LIBS}")
endfunction()
