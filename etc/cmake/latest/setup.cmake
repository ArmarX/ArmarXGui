set(CMAKE_GLOBAL_AUTOGEN_TARGET ON)
set(CMAKE_GLOBAL_AUTORCC_TARGET ON)

find_package(Qt5Script QUIET)
find_package(Qt5OpenGL QUIET)
find_package(Qt5Designer QUIET)
find_package(Qt5Network QUIET)

#https://forum.qt.io/topic/81252/cmake-qt5_wrap_ui-issue
# > Since QT5_WRAP_UI macro is delivered as part of Qt5Widget packacge it is necessery
# > to make a call to find_package (Qt5Widget) before issuing QT5_WRAP_UI call.
find_package(Qt5 COMPONENTS Widgets QUIET)
if(NOT Qt5_FOUND)
    message(WARNING "Qt5 NOT FOUND")
endif()
find_package(Qt5LinguistTools QUIET)

string(REGEX REPLACE "/[^/]+$" "" QT_EXECUTABLE_PRE "${QT_MOC_EXECUTABLE}")

# Public CMake API.
include(${ArmarXGui_CMAKE_SCRIPTS_DIR}/add_qt_targets.cmake)

