function(armarx_add_qt_plugin TARGET)
    # Parse arguments.
    set(single_param WIDGET_CONTROLLER)
    set(flag_param MANUAL_QT_PLUGIN_SOURCES)
    set(multi_param SOURCES HEADERS UI_FILES RESOURCE_FILES DEPENDENCIES DEPENDENCIES_LEGACY)
    cmake_parse_arguments(PARSE_ARGV 1 AX "${flag_param}" "${single_param}" "${multi_param}")
    if(DEFINED AX_UNPARSED_ARGUMENTS)
        message(FATAL_ERROR "${TARGET}: Unknown arguments `${AX_UNPARSED_ARGUMENTS}`.")
    endif()

    # Variables modified within this scope.
    set(SOURCES "${AX_SOURCES}")
    set(HEADERS "${AX_HEADERS}")

    # Auto-generate plugin headers and sources if requested. If not (i.e., if
    # MANUAL_QT_PLUGIN_SOURCES is set, this function is not different to
    # `armarx_add_qt_library`, except that the intention of the user is clearer.
    if(NOT ${AX_MANUAL_QT_PLUGIN_SOURCES})
        if(NOT AX_WIDGET_CONTROLLER)
            message(FATAL_ERROR "${TARGET}: Qt plugins must have a registered widget controller in WIDGET_CONTROLLER to generate Qt plugin source files. You can also provide custom sources in SOURCES and HEADERS and enable MANUAL_QT_PLUGIN_SOURCES.")
        endif()

        # Extract controller name from namespace(s).
        string(REPLACE "::" ";" WIDGET_CONTROLLER_NS_LIST "${AX_WIDGET_CONTROLLER}")
        list(POP_BACK WIDGET_CONTROLLER_NS_LIST WIDGET_CONTROLLER_NAME)
        string(REPLACE ";" "::" WIDGET_CONTROLLER_NS "${WIDGET_CONTROLLER_NS_LIST}")

        # Get the widget controller header.
        set(HEADERS_CP ${HEADERS})
        set(HEADERS_FILTER_REGEX "(^|\\.*/)${WIDGET_CONTROLLER_NAME}\\.h")
        list(FILTER HEADERS_CP INCLUDE REGEX "${HEADERS_FILTER_REGEX}")
        list(REMOVE_DUPLICATES HEADERS_CP)
        list(LENGTH HEADERS_CP HEADERS_CP_LEN)
        if(${HEADERS_CP_LEN} EQUAL 0)
            message(FATAL_ERROR "${TARGET}: Failed to auto generate the Qt plugin sources.  Cannot find header `${WIDGET_CONTROLLER_NAME}.h` of WIDGET_CONTROLLER `${AX_WIDGET_CONTROLLER}` in supplied HEADERS.")
        endif()
        list(GET HEADERS_CP 0 WIDGET_CONTROLLER_HEADER_INCLUDE)

        # Get relative path for the include.
        file(RELATIVE_PATH INCLUDE_PATH "${PROJECT_SOURCECODE_DIR}" "${CMAKE_CURRENT_SOURCE_DIR}")

        # Variables for template configuration.
        set(QT_PLUGIN_NAMESPACE "${WIDGET_CONTROLLER_NS}")
        set(QT_WIDGET_CONTROLLER_INCLUDE "${INCLUDE_PATH}/${WIDGET_CONTROLLER_HEADER_INCLUDE}")
        set(QT_WIDGET_CONTROLLER_FQ_NAME "${AX_WIDGET_CONTROLLER}")

        # Generate Qt plugin files.
        set(OUT_FILE "${CMAKE_CURRENT_BINARY_DIR}/QtPlugin")
        foreach(EXT h cpp)
            set(QT_PLUGIN_SOURCE_FILE "${ArmarXGui_TEMPLATES_DIR}/qt_plugin/QtPlugin.in.${EXT}")

            configure_file(
                "${QT_PLUGIN_SOURCE_FILE}"
                "${OUT_FILE}.${EXT}"
                USE_SOURCE_PERMISSIONS
                @ONLY
            )
        endforeach()

        # Add generated files to SOURCES and HEADERS.
        list(APPEND SOURCES "${OUT_FILE}.cpp")
        list(APPEND HEADERS "${OUT_FILE}.h")
    else()
        if(AX_WIDGET_CONTROLLER)
            message(WARNING "${TARGET}: Not generating sources for WIDGET_CONTROLLER `${AX_WIDGET_CONTROLLERS}` (MANUAL_QT_PLUGIN_SOURCES set, assuming user provided custom sources). You may remove WIDGET_CONTROLLER to get rid of this warning.")
        endif()
    endif()

    # Define the actual library with auto-generated files appended.
    armarx_add_qt_library(${TARGET}
        SOURCES ${SOURCES}
        HEADERS ${HEADERS}
        UI_FILES ${AX_UI_FILES}
        RESOURCE_FILES ${AX_RESOURCE_FILES}
        DEPENDENCIES ${AX_DEPENDENCIES}
        DEPENDENCIES_LEGACY ${AX_DEPENDENCIES_LEGACY}
    )
endfunction()


function(armarx_add_qt_library TARGET)
    # Parse arguments.
    set(single_param)
    set(flag_param PLUGIN)
    set(multi_param SOURCES HEADERS UI_FILES RESOURCE_FILES DEPENDENCIES DEPENDENCIES_LEGACY)
    cmake_parse_arguments(PARSE_ARGV 1 AX "${flag_param}" "${single_param}" "${multi_param}")
    if(DEFINED AX_UNPARSED_ARGUMENTS)
        message(FATAL_ERROR "${TARGET}: Unknown arguments `${AX_UNPARSED_ARGUMENTS}`.")
    endif()

    qt5_add_resources(RESOURCE_FILES ${AX_RESOURCE_FILES})

    # Variables modified within this scope.
    set(SOURCES ${AX_SOURCES} ${RESOURCE_FILES})
    set(HEADERS ${AX_HEADERS} ${AX_UI_FILES})
    set(DEPENDENCIES ${AX_DEPENDENCIES})

    # TODO: Remove this.
    if(NOT "${TARGET}" STREQUAL "ArmarXGuiBase")
        list(APPEND DEPENDENCIES ArmarXGuiBase)
    endif()

    set(GENERATE_BASE_DIR "${CMAKE_CURRENT_BINARY_DIR}/${TARGET}_autogen/include")

    install(DIRECTORY "${GENERATE_BASE_DIR}" DESTINATION "include" COMPONENT headers)

    armarx_add_library(${TARGET}
        SOURCES ${SOURCES}
        HEADERS ${HEADERS}
        DEPENDENCIES ${DEPENDENCIES}
        DEPENDENCIES_LEGACY ${AX_DEPENDENCIES_LEGACY}
    )

    target_include_directories(${TARGET} PUBLIC $<BUILD_INTERFACE:${GENERATE_BASE_DIR}>)
    target_compile_definitions(${TARGET}
        PUBLIC
            Q_COMPILER_INITIALIZER_LISTS
            QT_DISABLE_DEPRECATED_BEFORE=0x040800)

    set_target_properties(${TARGET} PROPERTIES AUTOMOC ON)
    set_target_properties(${TARGET} PROPERTIES AUTOUIC ON)
    set_target_properties(${TARGET} PROPERTIES AUTORCC ON)
    set_target_properties(${TARGET} PROPERTIES AUTOUIC_SEARCH_PATHS ${${ARMARX_PROJECT_NAME}_SOURCE_DIR}/source)

    file(RELATIVE_PATH CORRECT_INCLUDE "${CMAKE_SOURCE_DIR}/source" ${CMAKE_CURRENT_SOURCE_DIR})

    add_custom_command(
        TARGET ${TARGET}
        POST_BUILD
        COMMAND ${CMAKE_COMMAND}
        ARGS
            "-DGLOBAL_INCLUDE=${ARMARX_PROJECT_INCLUDE_PATH}"
            "-DDIR=${CMAKE_CURRENT_BINARY_DIR}/${TARGET}_autogen/include/"
            "-DCORRECT_INCLUDE=${CORRECT_INCLUDE}"
            "-DPROJECT_SOURCE_DIR=${CMAKE_SOURCE_DIR}/source"
            "-P" "${ArmarXGui_CMAKE_DIR}/check_qt_moc_includes.cmake"
        COMMENT "Checking Qt MOC includes."
    )

    # Add Qt targets to all_generate and generated files.
    add_dependencies(all_generate ${TARGET}_autogen)
    file(GLOB_RECURSE QT_GENERATED_FILES "${CMAKE_CURRENT_BINARY_DIR}/${TARGET}_autogen/*")
    target_sources(generated_files PRIVATE ${QT_GENERATED_FILES})
endfunction()
