// This file was auto generated from `@QT_PLUGIN_SOURCE_FILE@`.

#pragma once

#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

namespace @QT_PLUGIN_NAMESPACE@
{
    class ARMARXCOMPONENT_IMPORT_EXPORT QtPlugin:
            public ::armarx::ArmarXGuiPlugin
    {
        Q_OBJECT
        Q_INTERFACES(ArmarXGuiInterface)
        Q_PLUGIN_METADATA(IID "ArmarXGuiInterface/1.00")

    public:
        QtPlugin();
    };
}
