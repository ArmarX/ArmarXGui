// This file was auto generated from `@QT_PLUGIN_SOURCE_FILE@`.

#include "QtPlugin.h"
#include <@QT_WIDGET_CONTROLLER_INCLUDE@>

namespace @QT_PLUGIN_NAMESPACE@
{
    QtPlugin::QtPlugin()
    {
        addWidget<@QT_WIDGET_CONTROLLER_FQ_NAME@>();
    }
}
